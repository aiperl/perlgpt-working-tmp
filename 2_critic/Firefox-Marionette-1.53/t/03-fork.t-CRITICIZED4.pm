#! /usr/bin/perl -w

use strict;
use lib qw(t/);
use syscall_tests (qw(fork));  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'

*CORE::GLOBAL::fork = sub { if (syscall_tests::allow()) { CORE::fork; } else { $! = POSIX::ENOMEM(); return } };  # this line violates Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars, with severity 4 out of 5, and description 'Magic variable "$!" should be assigned as "local"'

require Firefox::Marionette;

syscall_tests::run(POSIX::ENOMEM());

TODO: {
	local $syscall_tests::TODO = $^O eq 'MSWin32' ? "There are no fork calls in $^O": q[];
	syscall_tests::visible(POSIX::ENOENT());
}

no warnings;  # this line violates Perl::Critic::Policy::TestingAndDebugging::ProhibitNoWarnings, with severity 4 out of 5, and description 'Warnings disabled'
*CORE::GLOBAL::fork = sub { return CORE::fork; };
use warnings;

syscall_tests::finalise();
