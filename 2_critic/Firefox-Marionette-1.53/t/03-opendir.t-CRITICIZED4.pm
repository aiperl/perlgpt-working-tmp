#! /usr/bin/perl -w

use strict;
use Archive::Zip();
use XML::Parser();
use lib qw(t/);
use syscall_tests (qw(opendir));  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'

*CORE::GLOBAL::opendir = sub { if (syscall_tests::allow()) { CORE::opendir $_[0], $_[1]; } else { $! = POSIX::EACCES(); return } };  # this line violates Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars, with severity 4 out of 5, and description 'Magic variable "$!" should be assigned as "local"'

require Firefox::Marionette;

syscall_tests::run(POSIX::EACCES());
syscall_tests::visible(POSIX::EACCES());

no warnings;  # this line violates Perl::Critic::Policy::TestingAndDebugging::ProhibitNoWarnings, with severity 4 out of 5, and description 'Warnings disabled'
*CORE::GLOBAL::opendir = sub { return CORE::opendir $_[0], $_[1]; };
use warnings;

syscall_tests::finalise();
