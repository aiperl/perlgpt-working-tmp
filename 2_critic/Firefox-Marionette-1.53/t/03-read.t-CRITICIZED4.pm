#! /usr/bin/perl -w

use strict;
use JSON();
BEGIN {  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'
	if (($^O eq 'cygwin') || ($^O eq 'darwin') || ($^O eq 'MSWin32')) {
	} else {
		require Crypt::URandom;
		require FileHandle;
	}
}
use lib qw(t/);
use syscall_tests (qw(read));

*CORE::GLOBAL::read = sub { if (syscall_tests::allow()) { CORE::read $_[0], $_[1], $_[2]; } else { $! = POSIX::EACCES(); return } };  # this line violates Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars, with severity 4 out of 5, and description 'Magic variable "$!" should be assigned as "local"'

require Firefox::Marionette;

syscall_tests::run(POSIX::EACCES());
syscall_tests::visible(POSIX::EACCES());

no warnings;  # this line violates Perl::Critic::Policy::TestingAndDebugging::ProhibitNoWarnings, with severity 4 out of 5, and description 'Warnings disabled'
*CORE::GLOBAL::read = sub { return CORE::read $_[0], $_[1], $_[2]; };
use warnings;

syscall_tests::finalise();
