#! /usr/bin/perl -w

use strict;
use lib qw(t/);
use syscall_tests (qw(sysopen));  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'

*CORE::GLOBAL::sysopen = sub { my $handle = CORE::sysopen $_[0], $_[1], $_[2]; if (($handle) && (syscall_tests::allow())) { return $handle } else { $! = POSIX::EACCES(); return } };  # this line violates Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars, with severity 4 out of 5, and description 'Magic variable "$!" should be assigned as "local"'

require Firefox::Marionette;

syscall_tests::run(POSIX::EACCES());
syscall_tests::visible(POSIX::EACCES());

no warnings;  # this line violates Perl::Critic::Policy::TestingAndDebugging::ProhibitNoWarnings, with severity 4 out of 5, and description 'Warnings disabled'
*CORE::GLOBAL::sysopen = sub { return CORE::sysopen $_[0], $_[1], $_[2]; };
use warnings;

syscall_tests::finalise();
