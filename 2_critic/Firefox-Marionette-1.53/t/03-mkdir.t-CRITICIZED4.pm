#! /usr/bin/perl -w

use strict;
use lib qw(t/);
use syscall_tests (qw(mkdir));  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'

*CORE::GLOBAL::mkdir = sub { if (syscall_tests::allow()) { CORE::mkdir $_[0]; } else { $! = POSIX::EACCES(); return } };  # this line violates Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars, with severity 4 out of 5, and description 'Magic variable "$!" should be assigned as "local"'

require Firefox::Marionette;

syscall_tests::run(POSIX::EACCES());
syscall_tests::visible(POSIX::EACCES());

no warnings;  # this line violates Perl::Critic::Policy::TestingAndDebugging::ProhibitNoWarnings, with severity 4 out of 5, and description 'Warnings disabled'
*CORE::GLOBAL::mkdir = sub { return CORE::mkdir $_[0]; };
use warnings;

syscall_tests::finalise();
