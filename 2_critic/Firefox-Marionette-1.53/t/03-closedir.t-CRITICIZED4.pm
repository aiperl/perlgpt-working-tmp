#! /usr/bin/perl -w

use strict;
use Archive::Zip();
use XML::Parser();
use lib qw(t/);
use syscall_tests (qw(closedir));  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'

*CORE::GLOBAL::closedir = sub { if (syscall_tests::allow()) { CORE::closedir $_[0]; } else { $! = POSIX::EBADF(); return } };  # this line violates Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars, with severity 4 out of 5, and description 'Magic variable "$!" should be assigned as "local"'

require Firefox::Marionette;

syscall_tests::run(POSIX::EBADF());
syscall_tests::visible(POSIX::EBADF());

no warnings;  # this line violates Perl::Critic::Policy::TestingAndDebugging::ProhibitNoWarnings, with severity 4 out of 5, and description 'Warnings disabled'
*CORE::GLOBAL::closedir = sub { return CORE::closedir $_[0]; };
use warnings;

syscall_tests::finalise();
