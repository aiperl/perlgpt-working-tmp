#! /usr/bin/perl -w

use strict;
use lib qw(t/);
use syscall_tests (qw(seek));  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'

*CORE::GLOBAL::seek = sub { if (syscall_tests::allow()) { return CORE::seek $_[0], $_[1], $_[2]; } else { $! = POSIX::ESPIPE(); return } };  # this line violates Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars, with severity 4 out of 5, and description 'Magic variable "$!" should be assigned as "local"'

require Firefox::Marionette;

syscall_tests::run(POSIX::ESPIPE());
syscall_tests::visible(POSIX::ESPIPE());

no warnings;  # this line violates Perl::Critic::Policy::TestingAndDebugging::ProhibitNoWarnings, with severity 4 out of 5, and description 'Warnings disabled'
*CORE::GLOBAL::seek = sub { return CORE::seek $_[0], $_[1], $_[2]; };
use warnings;

syscall_tests::finalise();
