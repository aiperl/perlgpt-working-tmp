$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Special block name used as label',
                   '_element_class' => 'PPI::Token::Label',
                   '_explanation' => 'Use a label that cannot be confused with BEGIN, END, CHECK, INIT, or UNITCHECK blocks',
                   '_filename' => undef,
                   '_location' => [
                                    20,
                                    1,
                                    1,
                                    20,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ControlStructures::ProhibitLabelsWithSpecialBlockNames',
                   '_severity' => 4,
                   '_source' => 'BEGIN: {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "eval"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       161
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    31,
                                    22,
                                    22,
                                    31,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval',
                   '_severity' => 5,
                   '_source' => 'eval \'Devel::Cover::get_coverage()\''
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'One-argument "select" used',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       224
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    33,
                                    13,
                                    13,
                                    33,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::ProhibitOneArgSelect',
                   '_severity' => 4,
                   '_source' => 'my $oldfh = select STDOUT;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$|" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => [
                                       81,
                                       82
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    33,
                                    31,
                                    31,
                                    33,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$| = 1;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'One-argument "select" used',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    33,
                                    36,
                                    36,
                                    33,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::ProhibitOneArgSelect',
                   '_severity' => 4,
                   '_source' => 'select $oldfh;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'One-argument "select" used',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    34,
                                    10,
                                    10,
                                    34,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::ProhibitOneArgSelect',
                   '_severity' => 4,
                   '_source' => '$oldfh = select STDERR;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$|" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    34,
                                    28,
                                    28,
                                    34,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$| = 1;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'One-argument "select" used',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    34,
                                    33,
                                    33,
                                    34,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::ProhibitOneArgSelect',
                   '_severity' => 4,
                   '_source' => 'select $oldfh;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$ENV" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    89,
                                    27,
                                    27,
                                    89,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$ENV{HOME} = $correct;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$SIG" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    113,
                                    11,
                                    11,
                                    113,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$SIG{INT} = sub { $terminated = 1; die "Caught an INT signal"; };'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$SIG" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    114,
                                    12,
                                    12,
                                    114,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$SIG{TERM} = sub { $terminated = 1; die "Caught a TERM signal"; };'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_check_navigator_attributes" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       197
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    116,
                                    1,
                                    1,
                                    116,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _check_navigator_attributes {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$ENV" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    883,
                                    32,
                                    32,
                                    883,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$ENV{FIREFOX_NO_NETWORK} = 1;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       169
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    1245,
                                    8,
                                    8,
                                    1245,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => '!grep /^set_window_rect$/, $capabilities->enumerate()'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1252,
                                    8,
                                    8,
                                    1252,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => '!grep /^unhandled_prompt_behavior$/, $capabilities->enumerate()'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1259,
                                    8,
                                    8,
                                    1259,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => '!grep /^moz_shutdown_timeout$/, $capabilities->enumerate()'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1266,
                                    8,
                                    8,
                                    1266,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => '!grep /^strict_file_interactability$/, $capabilities->enumerate()'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1273,
                                    8,
                                    8,
                                    1273,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => '!grep /^page_load_strategy$/, $capabilities->enumerate()'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1280,
                                    8,
                                    8,
                                    1280,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => '!grep /^accept_insecure_certs$/, $capabilities->enumerate()'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1287,
                                    8,
                                    8,
                                    1287,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => '!grep /^moz_webdriver_click$/, $capabilities->enumerate()'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1294,
                                    8,
                                    8,
                                    1294,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => '!grep /^moz_use_non_spec_compliant_pointer_origin$/, $capabilities->enumerate()'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1302,
                                    8,
                                    8,
                                    1302,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => '!grep /^moz_accessibility_checks$/, $capabilities->enumerate()'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "eval"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1398,
                                    6,
                                    6,
                                    1398,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval',
                   '_severity' => 5,
                   '_source' => 'eval \'Devel::Cover::set_coverage("none")\' if $is_covering;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$0" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1402,
                                    10,
                                    10,
                                    1402,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$0 = "[Test HTTP Proxy for " . getppid . "]";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Stricture disabled',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => [
                                       429
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    2127,
                                    4,
                                    4,
                                    2127,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::ProhibitNoStrict',
                   '_severity' => 5,
                   '_source' => 'no strict;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2147,
                                    7,
                                    7,
                                    2147,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => '!grep /^accept_insecure_certs$/, $capabilities->enumerate()'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "eval"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2612,
                                    16,
                                    16,
                                    2612,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval',
                   '_severity' => 5,
                   '_source' => 'my $result = eval "return Firefox::Marionette::Bookmark::$name();";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Stricture disabled',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2613,
                                    3,
                                    3,
                                    2613,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::ProhibitNoStrict',
                   '_severity' => 5,
                   '_source' => 'no strict;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "eval"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2618,
                                    16,
                                    16,
                                    2618,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval',
                   '_severity' => 5,
                   '_source' => 'my $result = eval "return Firefox::Marionette::Bookmark::$name();";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Stricture disabled',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2619,
                                    3,
                                    3,
                                    2619,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::ProhibitNoStrict',
                   '_severity' => 5,
                   '_source' => 'no strict;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2669,
                                    6,
                                    6,
                                    2669,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => 'grep /^accept_insecure_certs$/, $capabilities->enumerate()'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3069,
                                    7,
                                    7,
                                    3069,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => '!grep /^accept_insecure_certs$/, $capabilities->enumerate()'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4308,
                                    8,
                                    8,
                                    4308,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => '!grep /^page_load_strategy$/, $capabilities->enumerate()'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4316,
                                    8,
                                    8,
                                    4316,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => '!grep /^accept_insecure_certs$/, $capabilities->enumerate()'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4323,
                                    8,
                                    8,
                                    4323,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => '!grep /^moz_process_id$/, $capabilities->enumerate()'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4330,
                                    8,
                                    8,
                                    4330,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => '!grep /^moz_build_id$/, $capabilities->enumerate()'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4339,
                                    8,
                                    8,
                                    4339,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => '!grep /^moz_use_non_spec_compliant_pointer_origin$/, $capabilities->enumerate()'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4346,
                                    8,
                                    8,
                                    4346,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => '!grep /^moz_accessibility_checks$/, $capabilities->enumerate()'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4370,
                                    9,
                                    9,
                                    4370,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => '!grep /^moz_webdriver_click$/, $capabilities->enumerate()'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "eval"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4587,
                                    5,
                                    5,
                                    4587,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval',
                   '_severity' => 5,
                   '_source' => 'eval \'Devel::Cover::set_coverage("none")\' if $is_covering;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$0" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4591,
                                    9,
                                    9,
                                    4591,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$0 = "[Test HTTP Content Server for " . getppid . "]";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "eval"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4732,
                                    6,
                                    6,
                                    4732,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval',
                   '_severity' => 5,
                   '_source' => 'eval \'Devel::Cover::set_coverage("none")\' if $is_covering;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$0" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4736,
                                    10,
                                    10,
                                    4736,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$0 = "[Test HTTP Links and Images Server for " . getppid . "]";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4874,
                                    8,
                                    8,
                                    4874,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => '!grep /^page_load_strategy$/, $capabilities->enumerate()'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4881,
                                    8,
                                    8,
                                    4881,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => '!grep /^accept_insecure_certs$/, $capabilities->enumerate()'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4888,
                                    8,
                                    8,
                                    4888,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => '!grep /^moz_use_non_spec_compliant_pointer_origin$/, $capabilities->enumerate()'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4895,
                                    8,
                                    8,
                                    4895,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => '!grep /^moz_webdriver_click$/, $capabilities->enumerate()'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4902,
                                    8,
                                    8,
                                    4902,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => '!grep /^moz_accessibility_checks$/, $capabilities->enumerate()'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$ENV" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5124,
                                    19,
                                    19,
                                    5124,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$ENV{http_proxy} = \'http://localhost:\' . $proxyHttpPort;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$ENV" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5125,
                                    20,
                                    20,
                                    5125,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$ENV{https_proxy} = \'http://localhost:\' . $proxyHttpsPort;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$ENV" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5126,
                                    18,
                                    18,
                                    5126,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$ENV{ftp_proxy} = \'ftp://localhost:\' . $proxyFtpPort;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5258,
                                    35,
                                    35,
                                    5258,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => '!grep /^moz_process_id$/, $capabilities->enumerate()'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5409,
                                    8,
                                    8,
                                    5409,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => '!grep /^page_load_strategy$/, $capabilities->enumerate()'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "eval"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5499,
                                    1,
                                    1,
                                    5499,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval',
                   '_severity' => 5,
                   '_source' => 'eval "no warnings; sub File::Temp::newdir { \\$! = POSIX::EACCES(); return; } use warnings;";'
                 }, 'Perl::Critic::Violation' )
        ];
1;
