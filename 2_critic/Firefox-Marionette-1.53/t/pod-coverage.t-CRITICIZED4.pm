#!perl -T

use Test::More;
eval "use Test::Pod::Coverage 1.04";  # this line violates Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval, with severity 5 out of 5, and description 'Expression form of "eval"'  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict, with severity 5 out of 5, and description 'Code before strictures are enabled'  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'
plan skip_all => "Test::Pod::Coverage 1.04 required for testing POD coverage" if $@;
all_pod_coverage_ok({ trustme => [
		 qr/^BY_(ID|NAME|CLASS|TAG|SELECTOR|LINK|PARTIAL|XPATH)$/,
		 qr/^(find_elements?|page_source|send_keys)$/,
		 qr/^(active_frame|switch_to_shadow_root)$/,
		 qr/^(chrome_window_handle|chrome_window_handles|current_chrome_window_handle)$/,
		 qr/^(download)$/,
		 qr/^(ftp)$/,
		 qr/^(xvfb)$/,
		 qr/^(TO_JSON)$/,
		 qr/^(list.*)$/,
		 qr/^(accept_dialog)$/,
		 qr/^(find_by_.*)$/,
			 ] });
