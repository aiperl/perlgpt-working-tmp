$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Magic variable "$|" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => [
                                       81,
                                       82
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    17,
                                    20,
                                    20,
                                    17,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$| = 1;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'One-argument "select" used',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       224
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    40,
                                    12,
                                    12,
                                    40,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::ProhibitOneArgSelect',
                   '_severity' => 4,
                   '_source' => 'my $old = select $client;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$|" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    40,
                                    31,
                                    31,
                                    40,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$| = 1;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'One-argument "select" used',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    40,
                                    36,
                                    36,
                                    40,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::ProhibitOneArgSelect',
                   '_severity' => 4,
                   '_source' => 'select $old;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_send_response_body" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       197
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    83,
                                    1,
                                    1,
                                    83,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _send_response_body {'
                 }, 'Perl::Critic::Violation' )
        ];
1;
