use strict;  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'
use warnings;

use English;
use Error::Pure::Utils qw(clean);
use Mo::utils qw(check_code);
use Test::More 'tests' => 5;
use Test::NoWarnings;

# Test.
my $self = {
	'key' => sub {},
};
my $ret = check_code($self, 'key');
is($ret, undef, 'Right code value.');

# Test.
$self = {
	'key' => 'bad',
};
eval {
	check_code($self, 'key');
};
is($EVAL_ERROR, "Parameter 'key' must be a code.\n",
	"Parameter 'key' must be a code.");
clean();

# Test.
$self = {};
$ret = check_code($self, 'key');
is($ret, undef, 'Right not exist key.');

# Test.
$self = {
	'key' => undef,
};
$ret = check_code($self, 'key');
is($ret, undef, 'Right undefined value.');  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'
