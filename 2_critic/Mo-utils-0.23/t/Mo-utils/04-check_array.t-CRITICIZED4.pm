use strict;  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'
use warnings;

use English;
use Error::Pure::Utils qw(clean err_msg_hr);
use Mo::utils qw(check_array);
use Test::MockObject;
use Test::More 'tests' => 9;
use Test::NoWarnings;

# Test.
my $self = {
	'key' => 'foo',
};
eval {
	check_array($self, 'key');
};
is($EVAL_ERROR, "Parameter 'key' must be a array.\n",
	"Parameter 'key' must be a array.");
my $err_msg_hr = err_msg_hr();
is($err_msg_hr->{'Reference'}, '', 'Test error parameter (Reference: ).');
is($err_msg_hr->{'Value'}, 'foo', 'Test error parameter (Value: foo).');
clean();

# Test.
$self = {
	'key' => ['foo'],
};
my $ret = check_array($self, 'key');
is($ret, undef, 'Right structure.');

# Test.
$self = {};
$ret = check_array($self, 'key');
is($ret, undef, 'Right not exist key.');

# Test.
$self = {
	'key' => undef,
};
eval {
	check_array($self, 'key');
};
is($EVAL_ERROR, "Parameter 'key' must be a array.\n",
	"Parameter 'key' must be a array.");
$err_msg_hr = err_msg_hr();
is($err_msg_hr->{'Reference'}, '', 'Test error parameter (Reference: ).');
is($err_msg_hr->{'Value'}, 'undef', 'Test error parameter (Value: undef).');
clean();  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'
