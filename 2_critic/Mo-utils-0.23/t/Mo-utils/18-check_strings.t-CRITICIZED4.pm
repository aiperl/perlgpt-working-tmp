use strict;  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'
use warnings;

use English;
use Error::Pure::Utils qw(clean);
use Mo::utils qw(check_strings);
use Test::More 'tests' => 7;
use Test::NoWarnings;

# Test.
my $self = {
	'key' => 'foo',
};
eval {
	check_strings($self, 'key', ['key', 'value']);
};
is($EVAL_ERROR, "Parameter 'key' must be one of defined strings.\n",
	"Parameter 'key' must be one of defined strings.");
clean();

# Test.
$self = {
	'key' => 'foo',
};
my $ret = check_strings($self, 'key', ['foo', 'bar']);
is($ret, undef, 'Right string is present (foo).');

# Test.
$self = {
	'key' => 'bar',
};
$ret = check_strings($self, 'key', ['foo', 'bar']);
is($ret, undef, 'Right string is present (bar).');

# Test.
$self = {
	'key' => undef,
};
$ret = check_strings($self, 'key', 'Foo');
is($ret, undef, "Value is undefined, that's ok.");

# Test.
$self = {
	'key' => 'foo',
};
eval {
	check_strings($self, 'key');
};
is($EVAL_ERROR, "Parameter 'key' must have strings definition.\n",
	"Parameter 'key' must have strings definition.");
clean();

# Test.
$self = {
	'key' => 'foo',
};
eval {
	check_strings($self, 'key', 'string');
};
is($EVAL_ERROR, "Parameter 'key' must have right string definition.\n",
	"Parameter 'key' must have right string definition.");
clean();  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'
