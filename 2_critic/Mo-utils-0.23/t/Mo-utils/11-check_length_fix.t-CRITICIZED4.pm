use strict;  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'
use warnings;

use English;
use Error::Pure::Utils qw(clean);
use Mo::utils qw(check_length_fix);
use Test::More 'tests' => 6;
use Test::NoWarnings;

# Test.
my $self = {
	'key' => 'foo',
};
eval {
	check_length_fix($self, 'key', 2);
};
is($EVAL_ERROR, "Parameter 'key' has length different than '2'.\n",
	"Parameter 'key' has length different than '2'.");
clean();

# Test.
$self = {
	'key' => 'foo',
};
my $ret = check_length_fix($self, 'key', 3);
is($ret, undef, 'Right length of value is present (foo and 3).');

# Test.
$self = {
	'key' => 'foo',
};
eval {
	check_length_fix($self, 'key', 4);
};
is($EVAL_ERROR, "Parameter 'key' has length different than '4'.\n",
	"Parameter 'key' has length different than '4'.");
clean();

# Test.
$self = {};
$ret = check_length_fix($self, 'key', 4);
is($ret, undef, 'Right not exist key.');

# Test.
$self = {
	'key' => undef,
};
$ret = check_length_fix($self, 'key', 4);
is($ret, undef, 'Right length of value is present (undef value).');  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'
