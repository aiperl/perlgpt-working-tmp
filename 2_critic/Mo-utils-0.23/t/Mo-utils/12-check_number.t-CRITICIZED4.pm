use strict;  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'
use warnings;

use English;
use Error::Pure::Utils qw(clean);
use Mo::utils qw(check_number);
use Test::More 'tests' => 8;
use Test::NoWarnings;

# Test.
my $self = {
	'key' => 10,
};
my $ret = check_number($self, 'key');
is($ret, undef, 'Right number is present (positive number).');

# Test.
$self = {
	'key' => 10.0001,
};
$ret = check_number($self, 'key');
is($ret, undef, 'Right number is present (positive floating number).');

# Test.
$self = {
	'key' => '+10',
};
$ret = check_number($self, 'key');
is($ret, undef, 'Right number is present (positive number with plus).');

# Test.
$self = {
	'key' => -10,
};
$ret = check_number($self, 'key');
is($ret, undef, 'Right number is present (negative number).');

# Test.
$self = {};
$ret = check_number($self, 'key');
is($ret, undef, 'Right not exist key.');

# Test.
$self = {
	'key' => undef,
};
$ret = check_number($self, 'key');
is($ret, undef, "Value is undefined, that's ok.");

# Test.
$self = {
	'key' => 'foo',
};
eval {
	check_number($self, 'key');
};
is($EVAL_ERROR, "Parameter 'key' must be a number.\n",
	"Parameter 'key' must be a number.");
clean();  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'
