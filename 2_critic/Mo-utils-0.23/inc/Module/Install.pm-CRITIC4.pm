$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Code before warnings are enabled',
                   '_element_class' => 'PPI::Statement::Scheduled',
                   '_explanation' => [
                                       431
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    27,
                                    1,
                                    1,
                                    26,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings',
                   '_severity' => 4,
                   '_source' => 'BEGIN {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       178
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    44,
                                    1,
                                    1,
                                    43,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub import {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "eval"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       161
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    78,
                                    2,
                                    2,
                                    77,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval',
                   '_severity' => 5,
                   '_source' => 'eval "use Win32::UTCFileTime" if $^O eq \'MSWin32\' && $] >= 5.006;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => '"require" statement with library name as string',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => 'Use a bareword instead',
                   '_filename' => undef,
                   '_location' => [
                                    134,
                                    3,
                                    3,
                                    133,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::RequireBarewordIncludes',
                   '_severity' => 5,
                   '_source' => 'require "$self->{path}/$self->{dispatch}.pm";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "autoload" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       197
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    156,
                                    1,
                                    1,
                                    155,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub autoload {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "preload" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    197,
                                    1,
                                    1,
                                    196,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub preload {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "new" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    230,
                                    1,
                                    1,
                                    229,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub new {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    267,
                                    1,
                                    1,
                                    266,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub call {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "load" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    274,
                                    1,
                                    1,
                                    273,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub load {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "load_extensions" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    296,
                                    1,
                                    1,
                                    295,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub load_extensions {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Mixed high and low-precedence booleans',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => [
                                       70
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    300,
                                    18,
                                    18,
                                    299,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators',
                   '_severity' => 4,
                   '_source' => '! ref $_ and lc $_ eq lc $self->{prefix}'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "find_extensions" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    323,
                                    1,
                                    1,
                                    322,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub find_extensions {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    378,
                                    1,
                                    1,
                                    377,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _read {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Bareword file handle opened',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       202,
                                       204
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    380,
                                    2,
                                    2,
                                    379,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::ProhibitBarewordFileHandles',
                   '_severity' => 5,
                   '_source' => 'open( FH, \'<\', $_[0] ) or die "open($_[0]): $!";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Close filehandles as soon as possible after opening them',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       209
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    380,
                                    2,
                                    2,
                                    379,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::RequireBriefOpen',
                   '_severity' => 4,
                   '_source' => 'open( FH, \'<\', $_[0] ) or die "open($_[0]): $!";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    387,
                                    1,
                                    1,
                                    386,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _readperl {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    395,
                                    1,
                                    1,
                                    394,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _readpod {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    406,
                                    1,
                                    1,
                                    405,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _write {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_write" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    406,
                                    1,
                                    1,
                                    405,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _write {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Bareword file handle opened',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    408,
                                    2,
                                    2,
                                    407,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::ProhibitBarewordFileHandles',
                   '_severity' => 5,
                   '_source' => 'open( FH, \'>\', $_[0] ) or die "open($_[0]): $!";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Close filehandles as soon as possible after opening them',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[14]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    408,
                                    2,
                                    2,
                                    407,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::RequireBriefOpen',
                   '_severity' => 4,
                   '_source' => 'open( FH, \'>\', $_[0] ) or die "open($_[0]): $!";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    434,
                                    1,
                                    1,
                                    433,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _cmp {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_cmp" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    434,
                                    1,
                                    1,
                                    433,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _cmp {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    439,
                                    1,
                                    1,
                                    438,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _CLASS {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_CLASS" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    439,
                                    1,
                                    1,
                                    438,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _CLASS {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Mixed high and low-precedence booleans',
                   '_element_class' => 'PPI::Statement::Expression',
                   '_explanation' => $VAR1->[10]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    441,
                                    3,
                                    3,
                                    440,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators',
                   '_severity' => 4,
                   '_source' => 'defined $_[0]'
                 }, 'Perl::Critic::Violation' )
        ];
1;
