#line 1
package Module::Install::Base;

use strict 'vars';
use vars qw{$VERSION};
BEGIN {  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'
	$VERSION = '1.21';
}

# Suspend handler for "redefined" warnings
BEGIN {
	my $w = $SIG{__WARN__};
	$SIG{__WARN__} = sub { $w };  # this line violates Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars, with severity 4 out of 5, and description 'Magic variable "$SIG" should be assigned as "local"'
}

#line 42

sub new {  # this line violates Perl::Critic::Policy::Subroutines::RequireArgUnpacking, with severity 4 out of 5, and description 'Always unpack @_ first'  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "new" does not end with "return"'
	my $class = shift;
	unless ( defined &{"${class}::call"} ) {
		*{"${class}::call"} = sub { shift->_top->call(@_) };
	}
	unless ( defined &{"${class}::load"} ) {
		*{"${class}::load"} = sub { shift->_top->load(@_) };
	}
	bless { @_ }, $class;
}

#line 61

sub AUTOLOAD {
	local $@;
	my $func = eval { shift->_top->autoload } or return;
	goto &$func;
}

#line 75

sub _top {  # this line violates Perl::Critic::Policy::Subroutines::RequireArgUnpacking, with severity 4 out of 5, and description 'Always unpack @_ first'  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "_top" does not end with "return"'
	$_[0]->{_top};
}

#line 90

sub admin {  # this line violates Perl::Critic::Policy::Subroutines::RequireArgUnpacking, with severity 4 out of 5, and description 'Always unpack @_ first'  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "admin" does not end with "return"'
	$_[0]->_top->{admin}
	or
	Module::Install::Base::FakeAdmin->new;
}

#line 106

sub is_admin {  # this line violates Perl::Critic::Policy::Subroutines::RequireArgUnpacking, with severity 4 out of 5, and description 'Always unpack @_ first'  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "is_admin" does not end with "return"'
	! $_[0]->admin->isa('Module::Install::Base::FakeAdmin');
}

sub DESTROY {}

package Module::Install::Base::FakeAdmin;  # this line violates Perl::Critic::Policy::Modules::ProhibitMultiplePackages, with severity 4 out of 5, and description 'Multiple "package" declarations', and explanation 'Limit to one per file'

use vars qw{$VERSION};
BEGIN {
	$VERSION = $Module::Install::Base::VERSION;
}

my $fake;

sub new {  # this line violates Perl::Critic::Policy::Subroutines::RequireArgUnpacking, with severity 4 out of 5, and description 'Always unpack @_ first'  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "new" does not end with "return"'
	$fake ||= bless(\@_, $_[0]);
}

sub AUTOLOAD {}

sub DESTROY {}

# Restore warning handler
BEGIN {
	$SIG{__WARN__} = $SIG{__WARN__}->();  # this line violates Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars, with severity 4 out of 5, and description 'Magic variable "$SIG" should be assigned as "local"'
}

1;

#line 159
