$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Code before strictures are enabled',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => [
                                       429
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    4,
                                    1,
                                    1,
                                    4,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict',
                   '_severity' => 5,
                   '_source' => '$dmb = new Date::Manip::Base;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Code before warnings are enabled',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => [
                                       431
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    4,
                                    1,
                                    1,
                                    4,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings',
                   '_severity' => 4,
                   '_source' => '$dmb = new Date::Manip::Base;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "new" called using indirect syntax',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       349
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    4,
                                    8,
                                    8,
                                    4,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Objects::ProhibitIndirectSyntax',
                   '_severity' => 4,
                   '_source' => '$dmb = new Date::Manip::Base;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Loop iterator is not lexical',
                   '_element_class' => 'PPI::Statement::Compound',
                   '_explanation' => [
                                       108
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    23,
                                    1,
                                    1,
                                    23,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLexicalLoopIterators',
                   '_severity' => 5,
                   '_source' => 'foreach $y (1970..2000) {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Loop iterator is not lexical',
                   '_element_class' => 'PPI::Statement::Compound',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    24,
                                    4,
                                    4,
                                    24,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLexicalLoopIterators',
                   '_severity' => 5,
                   '_source' => 'foreach $m (1..12) {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Loop iterator is not lexical',
                   '_element_class' => 'PPI::Statement::Compound',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    27,
                                    7,
                                    7,
                                    27,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLexicalLoopIterators',
                   '_severity' => 5,
                   '_source' => 'foreach $d (15..$dmb->days_in_month($y,$m)) {'
                 }, 'Perl::Critic::Violation' )
        ];
1;
