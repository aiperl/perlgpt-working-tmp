#!/usr/bin/perl

BEGIN {  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict, with severity 5 out of 5, and description 'Code before strictures are enabled'  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'
   $Date::Manip::Backend = 'DM5';
}
use Date::Manip;

@in = `cat parse-1.in`;
chomp(@in);

foreach $in (@in) {  # this line violates Perl::Critic::Policy::Variables::RequireLexicalLoopIterators, with severity 5 out of 5, and description 'Loop iterator is not lexical'
   ParseDateString($in);
}

# Local Variables:
# mode: cperl
# indent-tabs-mode: nil
# cperl-indent-level: 3
# cperl-continued-statement-offset: 2
# cperl-continued-brace-offset: 0
# cperl-brace-offset: 0
# cperl-brace-imaginary-offset: 0
# cperl-label-offset: 0
# End:
