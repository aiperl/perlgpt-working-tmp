#!/usr/bin/perl

$a1  = '(?<a>\d)';  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict, with severity 5 out of 5, and description 'Code before strictures are enabled'  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'

$a2  = $a1;
#$a2 = '(?<b>\d)';

$b   = '\d';  # this line violates Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars, with severity 4 out of 5, and description 'Magic variable "$b" should be assigned as "local"'

$rx  = qr/(?:${a}${b}|${a2}:${b})/;

#$string = "12";
$string = "1:2";

while (1) {
   $string =~ $rx;
   $tmp = $+{a};
}
