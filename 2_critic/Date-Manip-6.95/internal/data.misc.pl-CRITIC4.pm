$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Code before strictures are enabled',
                   '_element_class' => 'PPI::Statement::Variable',
                   '_explanation' => [
                                       429
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    20,
                                    1,
                                    1,
                                    20,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict',
                   '_severity' => 5,
                   '_source' => 'my($keep_years,$test_years);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Code before warnings are enabled',
                   '_element_class' => 'PPI::Statement::Variable',
                   '_explanation' => [
                                       431
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    20,
                                    1,
                                    1,
                                    20,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings',
                   '_severity' => 4,
                   '_source' => 'my($keep_years,$test_years);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => [
                                       68,
                                       71
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    53,
                                    18,
                                    18,
                                    53,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$_,1'
                 }, 'Perl::Critic::Violation' )
        ];
1;
