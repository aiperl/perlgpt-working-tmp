package Date::Manip;
# Copyright (c) 2010-2024 Sullivan Beck.  All rights reserved.
# This program is free software; you can redistribute it and/or modify it
# under the same terms as Perl itself.

###########################################################################
###########################################################################

use warnings;
use strict;
use Exporter;

our $VERSION;
$VERSION='6.95';

our (@ISA,@EXPORT);  # this line violates Perl::Critic::Policy::Modules::ProhibitAutomaticExportation, with severity 4 out of 5, and description 'Symbols are exported by default', and explanation 'Use '@EXPORT_OK' or '%EXPORT_TAGS' instead'

my $backend;

if ((exists $ENV{'DATE_MANIP'}  &&  $ENV{'DATE_MANIP'} eq 'DM5') ||
    (defined $Date::Manip::Backend  &&  $Date::Manip::Backend eq 'DM5')) {
   $backend = 'Date::Manip::DM5';

} elsif ($] >= 5.010) {
   $backend = 'Date::Manip::DM6';

} else {
   $backend = 'Date::Manip::DM5';
}

my $backend_exp = $backend . "::EXPORT";

my $flag = eval "require $backend; $backend->import(); return 'loaded';";  # this line violates Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval, with severity 5 out of 5, and description 'Expression form of "eval"'
if (! $flag) {
   die "ERROR LOADING MODULE: $backend";
}

{
   no strict 'refs';  # this line violates Perl::Critic::Policy::TestingAndDebugging::ProhibitNoStrict, with severity 5 out of 5, and description 'Stricture disabled'
   @EXPORT = @{ $backend_exp };
}

unshift (@ISA, $backend);

1;
# Local Variables:
# mode: cperl
# indent-tabs-mode: nil
# cperl-indent-level: 3
# cperl-continued-statement-offset: 2
# cperl-continued-brace-offset: 0
# cperl-brace-offset: 0
# cperl-brace-imaginary-offset: 0
# cperl-label-offset: 0
# End:
