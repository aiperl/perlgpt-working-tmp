$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Code before strictures are enabled',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => [
                                       429
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    15,
                                    1,
                                    1,
                                    15,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict',
                   '_severity' => 5,
                   '_source' => '@ISA = (\'Date::Manip::Obj\');'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Code before warnings are enabled',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => [
                                       431
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    15,
                                    1,
                                    1,
                                    15,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings',
                   '_severity' => 4,
                   '_source' => '@ISA = (\'Date::Manip::Obj\');'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_init" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       197
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    43,
                                    1,
                                    1,
                                    43,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _init {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Integer with leading zeros: "0001"',
                   '_element_class' => 'PPI::Token::Number::Octal',
                   '_explanation' => [
                                       58
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    141,
                                    27,
                                    27,
                                    141,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitLeadingZeros',
                   '_severity' => 5,
                   '_source' => '0001,02,01,00,00,00'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Integer with leading zeros: "02"',
                   '_element_class' => 'PPI::Token::Number::Octal',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    141,
                                    32,
                                    32,
                                    141,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitLeadingZeros',
                   '_severity' => 5,
                   '_source' => '0001,02,01,00,00,00'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Integer with leading zeros: "01"',
                   '_element_class' => 'PPI::Token::Number::Octal',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    141,
                                    35,
                                    35,
                                    141,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitLeadingZeros',
                   '_severity' => 5,
                   '_source' => '0001,02,01,00,00,00'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_init_dates" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    151,
                                    1,
                                    1,
                                    151,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _init_dates {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_init_args" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    165,
                                    1,
                                    1,
                                    165,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _init_args {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Negative array index should be used',
                   '_element_class' => 'PPI::Structure::Subscript',
                   '_explanation' => [
                                       88
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    372,
                                    14,
                                    14,
                                    372,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireNegativeIndices',
                   '_severity' => 4,
                   '_source' => '$int[$#int] = 1;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Negative array index should be used',
                   '_element_class' => 'PPI::Structure::Subscript',
                   '_explanation' => $VAR1->[8]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    383,
                                    16,
                                    16,
                                    383,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireNegativeIndices',
                   '_severity' => 4,
                   '_source' => '$int[$#int] == 0'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "start" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    791,
                                    1,
                                    1,
                                    791,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub start {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "end" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    800,
                                    1,
                                    1,
                                    800,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub end {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "basedate" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    808,
                                    1,
                                    1,
                                    808,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub basedate {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "nth" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    848,
                                    1,
                                    1,
                                    848,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub nth {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine name is a homonym for builtin function next',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       177
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    891,
                                    1,
                                    1,
                                    891,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::ProhibitBuiltinHomonyms',
                   '_severity' => 4,
                   '_source' => 'sub next {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "next" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    891,
                                    1,
                                    1,
                                    891,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub next {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "prev" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    943,
                                    1,
                                    1,
                                    943,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub prev {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_actual_base" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1189,
                                    1,
                                    1,
                                    1189,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _actual_base {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_nth_interval" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1854,
                                    1,
                                    1,
                                    1854,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _nth_interval {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => '"return" statement with explicit "undef"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       199
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    2226,
                                    7,
                                    7,
                                    2226,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::ProhibitExplicitReturnUndef',
                   '_severity' => 5,
                   '_source' => 'return undef  if (! defined $last  ||'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => '"return" statement with explicit "undef"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[19]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2412,
                                    10,
                                    10,
                                    2412,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::ProhibitExplicitReturnUndef',
                   '_severity' => 5,
                   '_source' => 'return undef  if ($first > $last);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => '"return" statement with explicit "undef"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[19]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2427,
                                    10,
                                    10,
                                    2427,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::ProhibitExplicitReturnUndef',
                   '_severity' => 5,
                   '_source' => 'return undef  if ($first > $last);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => '"return" statement with explicit "undef"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[19]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2438,
                                    4,
                                    4,
                                    2438,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::ProhibitExplicitReturnUndef',
                   '_severity' => 5,
                   '_source' => 'return undef  if (! defined $last  ||'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_field_add_values" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2695,
                                    1,
                                    1,
                                    2695,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _field_add_values {'
                 }, 'Perl::Critic::Violation' )
        ];
1;
