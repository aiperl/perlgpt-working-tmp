$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Negative array index should be used',
                   '_element_class' => 'PPI::Structure::Subscript',
                   '_explanation' => [
                                       88
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    61,
                                    28,
                                    28,
                                    61,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireNegativeIndices',
                   '_severity' => 4,
                   '_source' => '$args[$#args]'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "new" called using indirect syntax',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       349
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    109,
                                    15,
                                    15,
                                    109,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Objects::ProhibitIndirectSyntax',
                   '_severity' => 4,
                   '_source' => '$tz   = new Date::Manip::TZ $base  if ($tz);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "new" called using indirect syntax',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    137,
                                    18,
                                    18,
                                    137,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Objects::ProhibitIndirectSyntax',
                   '_severity' => 4,
                   '_source' => '$base = new Date::Manip::Base;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "new" called using indirect syntax',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    144,
                                    19,
                                    19,
                                    144,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Objects::ProhibitIndirectSyntax',
                   '_severity' => 4,
                   '_source' => '$tz = new Date::Manip::TZ $base;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "new" called using indirect syntax',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    146,
                                    19,
                                    19,
                                    146,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Objects::ProhibitIndirectSyntax',
                   '_severity' => 4,
                   '_source' => '$tz = new Date::Manip::TZ;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Negative array index should be used',
                   '_element_class' => 'PPI::Structure::Subscript',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    225,
                                    20,
                                    20,
                                    225,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireNegativeIndices',
                   '_severity' => 4,
                   '_source' => '$args[$#args]'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "new" called using indirect syntax',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    235,
                                    11,
                                    11,
                                    235,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Objects::ProhibitIndirectSyntax',
                   '_severity' => 4,
                   '_source' => 'return new Date::Manip::Date @args;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "new" called using indirect syntax',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    240,
                                    11,
                                    11,
                                    240,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Objects::ProhibitIndirectSyntax',
                   '_severity' => 4,
                   '_source' => 'return new Date::Manip::Delta @args;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "new" called using indirect syntax',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    245,
                                    11,
                                    11,
                                    245,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Objects::ProhibitIndirectSyntax',
                   '_severity' => 4,
                   '_source' => 'return new Date::Manip::Recur @args;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => '"return" statement with explicit "undef"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       199
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    252,
                                    7,
                                    7,
                                    252,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::ProhibitExplicitReturnUndef',
                   '_severity' => 5,
                   '_source' => 'return undef;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => '"return" statement with explicit "undef"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[9]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    267,
                                    7,
                                    7,
                                    267,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::ProhibitExplicitReturnUndef',
                   '_severity' => 5,
                   '_source' => 'return undef;'
                 }, 'Perl::Critic::Violation' )
        ];
1;
