$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Symbols are exported by default',
                   '_element_class' => 'PPI::Statement::Variable',
                   '_explanation' => 'Use \'@EXPORT_OK\' or \'%EXPORT_TAGS\' instead',
                   '_filename' => undef,
                   '_location' => [
                                    9,
                                    1,
                                    1,
                                    9,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::ProhibitAutomaticExportation',
                   '_severity' => 4,
                   '_source' => 'our (@ISA,@EXPORT);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Code before strictures are enabled',
                   '_element_class' => 'PPI::Statement::Variable',
                   '_explanation' => [
                                       429
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    9,
                                    1,
                                    1,
                                    9,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict',
                   '_severity' => 5,
                   '_source' => 'our (@ISA,@EXPORT);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Code before warnings are enabled',
                   '_element_class' => 'PPI::Statement::Variable',
                   '_explanation' => [
                                       431
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    9,
                                    1,
                                    1,
                                    9,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings',
                   '_severity' => 4,
                   '_source' => 'our (@ISA,@EXPORT);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "new" called using indirect syntax',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       349
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    65,
                                    11,
                                    11,
                                    65,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Objects::ProhibitIndirectSyntax',
                   '_severity' => 4,
                   '_source' => '$dateUT = new Date::Manip::Date;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "new" called using indirect syntax',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    68,
                                    11,
                                    11,
                                    68,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Objects::ProhibitIndirectSyntax',
                   '_severity' => 4,
                   '_source' => '$date   = new Date::Manip::Date;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       178
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    153,
                                    1,
                                    1,
                                    153,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub ParseDateDelta {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Loop iterator is not lexical',
                   '_element_class' => 'PPI::Statement::Compound',
                   '_explanation' => [
                                       108
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    720,
                                    10,
                                    10,
                                    720,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLexicalLoopIterators',
                   '_severity' => 5,
                   '_source' => 'foreach $flag (@n) {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => '"return" statement with explicit "undef"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       199
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    878,
                                    4,
                                    4,
                                    878,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::ProhibitExplicitReturnUndef',
                   '_severity' => 5,
                   '_source' => 'return undef  if ($err);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => '"return" statement with explicit "undef"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    891,
                                    4,
                                    4,
                                    891,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::ProhibitExplicitReturnUndef',
                   '_severity' => 5,
                   '_source' => 'return undef  if ($err);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => '"return" statement with explicit "undef"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    893,
                                    4,
                                    4,
                                    893,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::ProhibitExplicitReturnUndef',
                   '_severity' => 5,
                   '_source' => 'return undef  if ($err);'
                 }, 'Perl::Critic::Violation' )
        ];
1;
