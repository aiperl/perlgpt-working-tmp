$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Code before strictures are enabled',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => [
                                       429
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    10,
                                    1,
                                    1,
                                    10,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict',
                   '_severity' => 5,
                   '_source' => 'warnings::warnif(\'deprecated\', \'Date::Manip::DM5 is deprecated and will be removed from the Date::Manip package starting in version 7.00\');'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Symbols are exported by default',
                   '_element_class' => 'PPI::Statement::Variable',
                   '_explanation' => 'Use \'@EXPORT_OK\' or \'%EXPORT_TAGS\' instead',
                   '_filename' => undef,
                   '_location' => [
                                    12,
                                    1,
                                    1,
                                    12,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::ProhibitAutomaticExportation',
                   '_severity' => 4,
                   '_source' => 'our($OS,%Lang,%Holiday,%Events,%Curr,%Cnf,%Zone,$VERSION,@ISA,@EXPORT);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Mixed high and low-precedence booleans',
                   '_element_class' => 'PPI::Statement::Expression',
                   '_explanation' => [
                                       70
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    16,
                                    21,
                                    21,
                                    16,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators',
                   '_severity' => 4,
                   '_source' => 'defined $^O and'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Mixed high and low-precedence booleans',
                   '_element_class' => 'PPI::Statement::Expression',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    20,
                                    21,
                                    21,
                                    20,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators',
                   '_severity' => 4,
                   '_source' => 'defined $ENV{OS} and'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "eval"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       161
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    43,
                                    52,
                                    52,
                                    43,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval',
                   '_severity' => 5,
                   '_source' => 'eval("#" . substr($^X, 0, 0));'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Stricture disabled',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => [
                                       429
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    253,
                                    1,
                                    1,
                                    253,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::ProhibitNoStrict',
                   '_severity' => 5,
                   '_source' => 'no strict "vars";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       178
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    265,
                                    1,
                                    1,
                                    265,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub Date_Init {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => [
                                       68,
                                       71
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    676,
                                    20,
                                    20,
                                    676,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$_,1'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    692,
                                    1,
                                    1,
                                    692,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub ParseDateString {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Mixed high and low-precedence booleans',
                   '_element_class' => 'PPI::Statement::Expression',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    701,
                                    20,
                                    20,
                                    701,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators',
                   '_severity' => 4,
                   '_source' => '! $Curr{"InitDone"}  or  $Cnf{"UpdateCurrTZ"}'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Mixed high and low-precedence booleans',
                   '_element_class' => 'PPI::Statement::Expression',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1027,
                                    35,
                                    35,
                                    1027,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators',
                   '_severity' => 4,
                   '_source' => '! defined $y  or  length($y)<4'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1346,
                                    1,
                                    1,
                                    1346,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub ParseDate {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "ParseDate" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       197
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    1346,
                                    1,
                                    1,
                                    1346,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub ParseDate {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "@a" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => [
                                       81,
                                       82
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    1350,
                                    5,
                                    5,
                                    1350,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '@a=@_;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "@a" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1374,
                                    5,
                                    5,
                                    1374,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '@a=@args;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1404,
                                    1,
                                    1,
                                    1404,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub DateCalc {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "DateCalc" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1404,
                                    1,
                                    1,
                                    1404,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub DateCalc {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1489,
                                    1,
                                    1,
                                    1489,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub ParseDateDelta {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "@a" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1493,
                                    5,
                                    5,
                                    1493,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '@a=@_;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "@a" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1515,
                                    5,
                                    5,
                                    1515,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '@a=@args;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1688,
                                    1,
                                    1,
                                    1688,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub UnixDate {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1868,
                                    1,
                                    1,
                                    1868,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub Delta_Format {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2011,
                                    1,
                                    1,
                                    2011,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub ParseRecur {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2877,
                                    1,
                                    1,
                                    2877,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub Date_GetPrev {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2942,
                                    1,
                                    1,
                                    2942,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub Date_GetNext {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3008,
                                    1,
                                    1,
                                    3008,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub Date_IsHoliday {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "Date_IsHoliday" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3008,
                                    1,
                                    1,
                                    3008,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub Date_IsHoliday {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => '"return" statement with explicit "undef"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       199
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    3013,
                                    3,
                                    3,
                                    3013,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::ProhibitExplicitReturnUndef',
                   '_severity' => 5,
                   '_source' => 'return undef  if (! $date);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => '"return" statement with explicit "undef"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[27]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3019,
                                    3,
                                    3,
                                    3019,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::ProhibitExplicitReturnUndef',
                   '_severity' => 5,
                   '_source' => 'return undef  if (! exists $Holiday{"dates"}{$y}{$date});'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3025,
                                    1,
                                    1,
                                    3025,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub Events_List {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => '"return" statement with explicit "undef"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[27]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3033,
                                    33,
                                    33,
                                    3033,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::ProhibitExplicitReturnUndef',
                   '_severity' => 5,
                   '_source' => 'warn "Invalid date $args[0]", return undef  if (! $date0);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => '"return" statement with explicit "undef"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[27]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3041,
                                    37,
                                    37,
                                    3041,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::ProhibitExplicitReturnUndef',
                   '_severity' => 5,
                   '_source' => 'warn "Invalid date $args[1]\\n", return undef  if (! $date1);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => '"return" statement with explicit "undef"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[27]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3093,
                                    3,
                                    3,
                                    3093,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::ProhibitExplicitReturnUndef',
                   '_severity' => 5,
                   '_source' => 'return undef;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3100,
                                    1,
                                    1,
                                    3100,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub Date_SetTime {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "Date_SetTime" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3100,
                                    1,
                                    1,
                                    3100,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub Date_SetTime {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3119,
                                    1,
                                    1,
                                    3119,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub Date_SetDateField {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3166,
                                    1,
                                    1,
                                    3166,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub Date_DaysInMonth {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3175,
                                    1,
                                    1,
                                    3175,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub Date_DayOfWeek {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3189,
                                    1,
                                    1,
                                    3189,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub Date_SecsSince1970 {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3200,
                                    1,
                                    1,
                                    3200,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub Date_SecsSince1970GMT {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "Date_SecsSince1970GMT" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3200,
                                    1,
                                    1,
                                    3200,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub Date_SecsSince1970GMT {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3221,
                                    1,
                                    1,
                                    3221,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub Date_DaysSince1BC {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3253,
                                    1,
                                    1,
                                    3253,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub Date_DayOfYear {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3264,
                                    1,
                                    1,
                                    3264,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub Date_DaysInYear {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3272,
                                    1,
                                    1,
                                    3272,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub Date_WeekOfYear {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3297,
                                    1,
                                    1,
                                    3297,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub Date_LeapYear {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3307,
                                    1,
                                    1,
                                    3307,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub Date_DaySuffix {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3314,
                                    1,
                                    1,
                                    3314,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub Date_ConvTZ {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Mixed high and low-precedence booleans',
                   '_element_class' => 'PPI::Statement::Expression',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3334,
                                    25,
                                    25,
                                    3334,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators',
                   '_severity' => 4,
                   '_source' => '$Cnf{"ConvTZ"} eq "IGNORE" or ! $Cnf{"ConvTZ"}'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "new" called using indirect syntax',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       349
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    3501,
                                    9,
                                    9,
                                    3501,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Objects::ProhibitIndirectSyntax',
                   '_severity' => 4,
                   '_source' => '$in=new IO::File;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "new" called using indirect syntax',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[49]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3514,
                                    9,
                                    9,
                                    3514,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Objects::ProhibitIndirectSyntax',
                   '_severity' => 4,
                   '_source' => '$in=new IO::File;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Mixed high and low-precedence booleans',
                   '_element_class' => 'PPI::Statement::Expression',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3555,
                                    17,
                                    17,
                                    3555,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators',
                   '_severity' => 4,
                   '_source' => '! defined $Zone{"n2o"}{lc($std)} or'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3571,
                                    1,
                                    1,
                                    3571,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub Date_IsWorkDay {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "Date_IsWorkDay" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3571,
                                    1,
                                    1,
                                    3571,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub Date_IsWorkDay {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3607,
                                    1,
                                    1,
                                    3607,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub Date_NextWorkDay {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3649,
                                    1,
                                    1,
                                    3649,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub Date_PrevWorkDay {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3683,
                                    1,
                                    1,
                                    3683,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub Date_NearestWorkDay {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "Date_NearestWorkDay" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3683,
                                    1,
                                    1,
                                    3683,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub Date_NearestWorkDay {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$a" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3701,
                                    5,
                                    5,
                                    3701,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$a=$b=$date;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$b" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3701,
                                    8,
                                    8,
                                    3701,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$a=$b=$date;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$a" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3704,
                                    7,
                                    7,
                                    3704,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$a=_DateCalc_DateDelta($a,$dela,\\$err);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$b" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3706,
                                    7,
                                    7,
                                    3706,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$b=_DateCalc_DateDelta($b,$delb,\\$err);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3713,
                                    1,
                                    1,
                                    3713,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub Date_NthDayOfYear {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "Date_NthDayOfYear" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3713,
                                    1,
                                    1,
                                    3713,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub Date_NthDayOfYear {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Mixed high and low-precedence booleans',
                   '_element_class' => 'PPI::Statement::Expression',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3718,
                                    18,
                                    18,
                                    3718,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators',
                   '_severity' => 4,
                   '_source' => '! defined $n  or  $n eq ""'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3772,
                                    1,
                                    1,
                                    3772,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_InitHash {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Date_InitHash" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3772,
                                    1,
                                    1,
                                    3772,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Date_InitHash {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3834,
                                    1,
                                    1,
                                    3834,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_InitLists {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Date_InitLists" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3834,
                                    1,
                                    1,
                                    3834,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Date_InitLists {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3916,
                                    1,
                                    1,
                                    3916,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_InitStrings {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Date_InitStrings" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3916,
                                    1,
                                    1,
                                    3916,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Date_InitStrings {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4017,
                                    1,
                                    1,
                                    4017,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Regexp {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4078,
                                    20,
                                    20,
                                    4078,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$_,$i++'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4080,
                                    20,
                                    20,
                                    4080,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$i++,$_'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4103,
                                    1,
                                    1,
                                    4103,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Delta_Normalize {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Mixed high and low-precedence booleans',
                   '_element_class' => 'PPI::Statement::Expression',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4194,
                                    18,
                                    18,
                                    4194,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators',
                   '_severity' => 4,
                   '_source' => '$sign1 eq $sign2  and  ! $Cnf{"DeltaSigns"}'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4207,
                                    1,
                                    1,
                                    4207,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Delta_Split {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Delta_Split" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4207,
                                    1,
                                    1,
                                    4207,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Delta_Split {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4225,
                                    1,
                                    1,
                                    4225,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_ParseTime {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Date_ParseTime" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4225,
                                    1,
                                    1,
                                    4225,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Date_ParseTime {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4244,
                                    1,
                                    1,
                                    4244,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Join {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Date_Join" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4244,
                                    1,
                                    1,
                                    4244,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Join {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4278,
                                    1,
                                    1,
                                    4278,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _CheckTime {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4310,
                                    1,
                                    1,
                                    4310,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Recur_Split {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4338,
                                    1,
                                    1,
                                    4338,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Split {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Date_Recur_WoM" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4421,
                                    1,
                                    1,
                                    4421,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Recur_WoM {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Date_Recur" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4509,
                                    1,
                                    1,
                                    4509,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Recur {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Date_RecurSetTime" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4545,
                                    1,
                                    1,
                                    4545,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Date_RecurSetTime {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4605,
                                    1,
                                    1,
                                    4605,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _DateCalc_DateDate {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_DateCalc_DateDate" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4605,
                                    1,
                                    1,
                                    4605,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _DateCalc_DateDate {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Mixed high and low-precedence booleans',
                   '_element_class' => 'PPI::Statement::Expression',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4768,
                                    7,
                                    7,
                                    4768,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators',
                   '_severity' => 4,
                   '_source' => '$mode==2 || $mode==3  and  $d1 != $d2'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4791,
                                    1,
                                    1,
                                    4791,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _DateCalc_DeltaDelta {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4809,
                                    1,
                                    1,
                                    4809,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _DateCalc_DateDelta {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_DateCalc_DateDelta" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4809,
                                    1,
                                    1,
                                    4809,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _DateCalc_DateDelta {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Mixed high and low-precedence booleans',
                   '_element_class' => 'PPI::Statement::Expression',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4873,
                                    11,
                                    11,
                                    4873,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators',
                   '_severity' => 4,
                   '_source' => '$h>$h2  or  $h==$h2 && $mn>$m2'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Mixed high and low-precedence booleans',
                   '_element_class' => 'PPI::Statement::Expression',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4880,
                                    16,
                                    16,
                                    4880,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators',
                   '_severity' => 4,
                   '_source' => '$h<$h1  or  $h==$h1 && $mn<$m1'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4963,
                                    1,
                                    1,
                                    4963,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_UpdateHolidays {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Date_UpdateHolidays" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    4963,
                                    1,
                                    1,
                                    4963,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Date_UpdateHolidays {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5007,
                                    1,
                                    1,
                                    5007,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_SetConfigVariable {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5012,
                                    3,
                                    3,
                                    5012,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$Cnf{"PathSep"}=$val,          return  if ($var =~ /^PathSep$/i);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5013,
                                    3,
                                    3,
                                    5013,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$Cnf{"PersonalCnf"}=$val,      return  if ($var =~ /^PersonalCnf$/i);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5014,
                                    3,
                                    3,
                                    5014,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$Cnf{"PersonalCnfPath"}=$val,  return  if ($var =~ /^PersonalCnfPath$/i);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5015,
                                    3,
                                    3,
                                    5015,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => 'EraseHolidays(),              return  if ($var =~ /^EraseHolidays$/i);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5016,
                                    3,
                                    3,
                                    5016,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$Cnf{"IgnoreGlobalCnf"}=1,     return  if ($var =~ /^IgnoreGlobalCnf$/i);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5017,
                                    3,
                                    3,
                                    5017,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$Cnf{"GlobalCnf"}=$val,        return  if ($var =~ /^GlobalCnf$/i);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5019,
                                    3,
                                    3,
                                    5019,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$Curr{"InitLang"}=1,'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5021,
                                    3,
                                    3,
                                    5021,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$Cnf{"DateFormat"}=$val,       return  if ($var =~ /^DateFormat$/i);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5022,
                                    3,
                                    3,
                                    5022,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$Cnf{"TZ"}=$val,               return  if ($var =~ /^TZ$/i);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5023,
                                    3,
                                    3,
                                    5023,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$Cnf{"ConvTZ"}=$val,           return  if ($var =~ /^ConvTZ$/i);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5024,
                                    3,
                                    3,
                                    5024,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$Cnf{"Internal"}=$val,         return  if ($var =~ /^Internal$/i);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5025,
                                    3,
                                    3,
                                    5025,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$Cnf{"FirstDay"}=$val,         return  if ($var =~ /^FirstDay$/i);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5026,
                                    3,
                                    3,
                                    5026,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$Cnf{"WorkWeekBeg"}=$val,      return  if ($var =~ /^WorkWeekBeg$/i);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5027,
                                    3,
                                    3,
                                    5027,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$Cnf{"WorkWeekEnd"}=$val,      return  if ($var =~ /^WorkWeekEnd$/i);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5028,
                                    3,
                                    3,
                                    5028,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$Cnf{"WorkDayBeg"}=$val,'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5030,
                                    3,
                                    3,
                                    5030,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$Cnf{"WorkDayEnd"}=$val,'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5032,
                                    3,
                                    3,
                                    5032,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$Cnf{"WorkDay24Hr"}=$val,'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5034,
                                    3,
                                    3,
                                    5034,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$Cnf{"DeltaSigns"}=$val,       return  if ($var =~ /^DeltaSigns$/i);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5035,
                                    3,
                                    3,
                                    5035,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$Cnf{"Jan1Week1"}=$val,        return  if ($var =~ /^Jan1Week1$/i);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5036,
                                    3,
                                    3,
                                    5036,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$Cnf{"YYtoYYYY"}=$val,         return  if ($var =~ /^YYtoYYYY$/i);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5037,
                                    3,
                                    3,
                                    5037,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$Cnf{"UpdateCurrTZ"}=$val,     return  if ($var =~ /^UpdateCurrTZ$/i);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5038,
                                    3,
                                    3,
                                    5038,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$Cnf{"IntCharSet"}=$val,       return  if ($var =~ /^IntCharSet$/i);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5039,
                                    3,
                                    3,
                                    5039,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$Curr{"DebugVal"}=$val,        return  if ($var =~ /^Debug$/i);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5040,
                                    3,
                                    3,
                                    5040,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$Cnf{"TomorrowFirst"}=$val,    return  if ($var =~ /^TomorrowFirst$/i);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5041,
                                    3,
                                    3,
                                    5041,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$Cnf{"ForceDate"}=$val,        return  if ($var =~ /^ForceDate$/i);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5042,
                                    3,
                                    3,
                                    5042,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$Cnf{"TodayIsMidnight"}=$val,  return  if ($var =~ /^TodayIsMidnight$/i);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "EraseHolidays" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5047,
                                    1,
                                    1,
                                    5047,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub EraseHolidays {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5064,
                                    1,
                                    1,
                                    5064,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Events_Calc {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => '"return" statement with explicit "undef"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[27]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5071,
                                    3,
                                    3,
                                    5071,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::ProhibitExplicitReturnUndef',
                   '_severity' => 5,
                   '_source' => 'return undef  if (! $date0);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5148,
                                    7,
                                    7,
                                    5148,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => 'push(@tmp2,$tmp), next  if ($tmp =~ /^!/);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5168,
                                    1,
                                    1,
                                    5168,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Events_ParseRaw {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Events_ParseRaw" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5168,
                                    1,
                                    1,
                                    5168,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Events_ParseRaw {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5283,
                                    1,
                                    1,
                                    5283,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_InitFile {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Date_InitFile" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5283,
                                    1,
                                    1,
                                    5283,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Date_InitFile {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "new" called using indirect syntax',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[49]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5286,
                                    11,
                                    11,
                                    5286,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Objects::ProhibitIndirectSyntax',
                   '_severity' => 4,
                   '_source' => 'my($in)=new IO::File;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5340,
                                    1,
                                    1,
                                    5340,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_TimeCheck {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5398,
                                    1,
                                    1,
                                    5398,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_DateCheck {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5407,
                                    3,
                                    3,
                                    5407,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$$m=1, $$d=1  if (defined $$y and ! defined $$m and ! defined $$d);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Mixed high and low-precedence booleans',
                   '_element_class' => 'PPI::Statement::Expression',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5407,
                                    21,
                                    21,
                                    5407,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators',
                   '_severity' => 4,
                   '_source' => 'defined $$y and ! defined $$m and ! defined $$d'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5442,
                                    1,
                                    1,
                                    5442,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_FixYear {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Date_FixYear" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5442,
                                    1,
                                    1,
                                    5442,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Date_FixYear {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Mixed high and low-precedence booleans',
                   '_element_class' => 'PPI::Statement::Expression',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5446,
                                    19,
                                    19,
                                    5446,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators',
                   '_severity' => 4,
                   '_source' => '! defined $y  or  ! $y'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5487,
                                    1,
                                    1,
                                    5487,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_NthWeekOfYear {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Date_NthWeekOfYear" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5487,
                                    1,
                                    1,
                                    5487,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Date_NthWeekOfYear {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Mixed high and low-precedence booleans',
                   '_element_class' => 'PPI::Statement::Expression',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5491,
                                    22,
                                    22,
                                    5491,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators',
                   '_severity' => 4,
                   '_source' => '! defined $y  or  ! $y'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Mixed high and low-precedence booleans',
                   '_element_class' => 'PPI::Statement::Expression',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5492,
                                    18,
                                    18,
                                    5492,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators',
                   '_severity' => 4,
                   '_source' => '! defined $n  or  $n eq ""'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Char_8Bit" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5534,
                                    1,
                                    1,
                                    5534,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Char_8Bit {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5843,
                                    1,
                                    1,
                                    5843,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_English {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Date_Init_English" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5843,
                                    1,
                                    1,
                                    5843,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_English {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5915,
                                    1,
                                    1,
                                    5915,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_Italian {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Date_Init_Italian" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5915,
                                    1,
                                    1,
                                    5915,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_Italian {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5986,
                                    1,
                                    1,
                                    5986,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_French {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Date_Init_French" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    5986,
                                    1,
                                    1,
                                    5986,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_French {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    6063,
                                    1,
                                    1,
                                    6063,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_Romanian {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Date_Init_Romanian" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    6063,
                                    1,
                                    1,
                                    6063,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_Romanian {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    6180,
                                    1,
                                    1,
                                    6180,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_Swedish {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Date_Init_Swedish" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    6180,
                                    1,
                                    1,
                                    6180,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_Swedish {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    6264,
                                    1,
                                    1,
                                    6264,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_German {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Date_Init_German" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    6264,
                                    1,
                                    1,
                                    6264,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_German {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    6353,
                                    1,
                                    1,
                                    6353,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_Dutch {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Date_Init_Dutch" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    6353,
                                    1,
                                    1,
                                    6353,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_Dutch {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    6445,
                                    1,
                                    1,
                                    6445,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_Polish {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Date_Init_Polish" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    6445,
                                    1,
                                    1,
                                    6445,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_Polish {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    6544,
                                    1,
                                    1,
                                    6544,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_Spanish {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Date_Init_Spanish" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    6544,
                                    1,
                                    1,
                                    6544,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_Spanish {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    6627,
                                    1,
                                    1,
                                    6627,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_Portuguese {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Date_Init_Portuguese" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    6627,
                                    1,
                                    1,
                                    6627,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_Portuguese {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    6723,
                                    1,
                                    1,
                                    6723,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_Russian {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Date_Init_Russian" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    6723,
                                    1,
                                    1,
                                    6723,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_Russian {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    6911,
                                    1,
                                    1,
                                    6911,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_Turkish {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Date_Init_Turkish" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    6911,
                                    1,
                                    1,
                                    6911,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_Turkish {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    7022,
                                    1,
                                    1,
                                    7022,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_Danish {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Date_Init_Danish" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    7022,
                                    1,
                                    1,
                                    7022,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_Danish {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    7101,
                                    1,
                                    1,
                                    7101,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_Catalan {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_Date_Init_Catalan" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    7101,
                                    1,
                                    1,
                                    7101,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _Date_Init_Catalan {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_ModuloAddition" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    7207,
                                    1,
                                    1,
                                    7207,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _ModuloAddition {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Mixed high and low-precedence booleans',
                   '_element_class' => 'PPI::Statement::Expression',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    7240,
                                    17,
                                    17,
                                    7240,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators',
                   '_severity' => 4,
                   '_source' => '! defined $N  or'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_ExpandTilde" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    7274,
                                    1,
                                    1,
                                    7274,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _ExpandTilde {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Mixed high and low-precedence booleans',
                   '_element_class' => 'PPI::Statement::Expression',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    7336,
                                    17,
                                    17,
                                    7336,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators',
                   '_severity' => 4,
                   '_source' => '! defined $file or ! $file'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Mixed high and low-precedence booleans',
                   '_element_class' => 'PPI::Statement::Expression',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    7412,
                                    9,
                                    9,
                                    7412,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators',
                   '_severity' => 4,
                   '_source' => '! defined $mode  or  _CheckFilePath($_,$mode)'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_ReturnList" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[12]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    7454,
                                    1,
                                    1,
                                    7454,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _ReturnList {'
                 }, 'Perl::Critic::Violation' )
        ];
1;
