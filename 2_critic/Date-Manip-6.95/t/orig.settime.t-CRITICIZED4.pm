#!/usr/bin/perl

use warnings;
use strict;
use Test::Inter;
$::ti = new Test::Inter $0;  # this line violates Perl::Critic::Policy::Objects::ProhibitIndirectSyntax, with severity 4 out of 5, and description 'Subroutine "new" called using indirect syntax'
require "tests.pl";  # this line violates Perl::Critic::Policy::Modules::RequireBarewordIncludes, with severity 5 out of 5, and description '"require" statement with library name as string', and explanation 'Use a bareword instead'

sub test {  # this line violates Perl::Critic::Policy::Subroutines::RequireArgUnpacking, with severity 4 out of 5, and description 'Always unpack @_ first'
   return Date_SetTime(@_);
}

Date_Init("ForceDate=1997-03-08-12:30:00,America/New_York");

my $tests="

'Jan 1, 1996 at 10:30' 12:40       => 1996010112:40:00

1996010110:30:40       12:40:50    => 1996010112:40:50

1996010110:30:40       12:40       => 1996010112:40:00

1996010110:30:40       12 40       => 1996010112:40:00

1996010110:30:40       12 40 50    => 1996010112:40:50

";

$::ti->tests(func  => \&test,
             tests => $tests);
$::ti->done_testing();

#Local Variables:
#mode: cperl
#indent-tabs-mode: nil
#cperl-indent-level: 3
#cperl-continued-statement-offset: 2
#cperl-continued-brace-offset: 0
#cperl-brace-offset: 0
#cperl-brace-imaginary-offset: 0
#cperl-label-offset: 0
#End:
