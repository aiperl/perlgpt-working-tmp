#!/usr/bin/perl

binmode(STDOUT,':utf8');  # this line violates Perl::Critic::Policy::InputOutput::RequireEncodingWithUTF8Layer, with severity 5 out of 5, and description 'I/O layer ":utf8" used', and explanation 'Use ":encoding(UTF-8)" to get strict validation'  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict, with severity 5 out of 5, and description 'Code before strictures are enabled'  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'
binmode(STDERR,':utf8');  # this line violates Perl::Critic::Policy::InputOutput::RequireEncodingWithUTF8Layer, with severity 5 out of 5, and description 'I/O layer ":utf8" used', and explanation 'Use ":encoding(UTF-8)" to get strict validation'
use warnings;
use strict;
use Test::Inter;
$::ti = new Test::Inter $0;  # this line violates Perl::Critic::Policy::Objects::ProhibitIndirectSyntax, with severity 4 out of 5, and description 'Subroutine "new" called using indirect syntax'
require "tests.pl";  # this line violates Perl::Critic::Policy::Modules::RequireBarewordIncludes, with severity 5 out of 5, and description '"require" statement with library name as string', and explanation 'Use a bareword instead'

our $obj = new Date::Manip::Date;  # this line violates Perl::Critic::Policy::Objects::ProhibitIndirectSyntax, with severity 4 out of 5, and description 'Subroutine "new" called using indirect syntax'
$obj->config("forcedate","1997-03-08-12:30:00,America/New_York");
$obj->config("language","Russian","dateformat","nonUS");

sub test {
   my(@test)=@_;
   if ($test[0] eq "config") {
      shift(@test);
      $obj->config(@test);
      return ();
   }

   my $err = $obj->parse(@test);
   if ($err) {
      return $obj->err();
   } else {
      my $d1 = $obj->value();
      return $d1;
   }
}

my $tests="

'\xd1\xc5\xc3\xce\xc4\xcd\xdf' => '1997030800:00:00'

'\xe7\xe0\xe2\xf2\xf0\xe0' => '1997030900:00:00'

'2 \xcc\xc0\xdf 2012' => 2012050200:00:00

'2 \xec\xe0\xff 2012' => 2012050200:00:00

";

$::ti->tests(func  => \&test,
             tests => $tests);
$::ti->done_testing();

# Local Variables:
# mode: cperl
# indent-tabs-mode: nil
# cperl-indent-level: 3
# cperl-continued-statement-offset: 2
# cperl-continued-brace-offset: 0
# cperl-brace-offset: 0
# cperl-brace-imaginary-offset: 0
# cperl-label-offset: 0
# End:
