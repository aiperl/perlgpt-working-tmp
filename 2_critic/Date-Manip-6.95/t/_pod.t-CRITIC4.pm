$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Bareword file handle opened',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       202,
                                       204
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    51,
                                    4,
                                    4,
                                    51,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::ProhibitBarewordFileHandles',
                   '_severity' => 5,
                   '_source' => 'open(IN,"$testdir/_pod.ign");'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Two-argument "open" used',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       207
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    51,
                                    4,
                                    4,
                                    51,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::ProhibitTwoArgOpen',
                   '_severity' => 5,
                   '_source' => 'open(IN,"$testdir/_pod.ign");'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Bareword file handle opened',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    59,
                                    4,
                                    4,
                                    59,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::ProhibitBarewordFileHandles',
                   '_severity' => 5,
                   '_source' => 'open(IN,"$testdir/_pod.dirs");'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Two-argument "open" used',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    59,
                                    4,
                                    4,
                                    59,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::ProhibitTwoArgOpen',
                   '_severity' => 5,
                   '_source' => 'open(IN,"$testdir/_pod.dirs");'
                 }, 'Perl::Critic::Violation' )
        ];
1;
