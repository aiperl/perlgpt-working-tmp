$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'I/O layer ":utf8" used',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => 'Use ":encoding(UTF-8)" to get strict validation',
                   '_filename' => undef,
                   '_location' => [
                                    3,
                                    1,
                                    1,
                                    3,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::RequireEncodingWithUTF8Layer',
                   '_severity' => 5,
                   '_source' => 'binmode(STDOUT,\':utf8\');'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Code before strictures are enabled',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => [
                                       429
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    3,
                                    1,
                                    1,
                                    3,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict',
                   '_severity' => 5,
                   '_source' => 'binmode(STDOUT,\':utf8\');'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Code before warnings are enabled',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => [
                                       431
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    3,
                                    1,
                                    1,
                                    3,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings',
                   '_severity' => 4,
                   '_source' => 'binmode(STDOUT,\':utf8\');'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'I/O layer ":utf8" used',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => 'Use ":encoding(UTF-8)" to get strict validation',
                   '_filename' => undef,
                   '_location' => [
                                    4,
                                    1,
                                    1,
                                    4,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::RequireEncodingWithUTF8Layer',
                   '_severity' => 5,
                   '_source' => 'binmode(STDERR,\':utf8\');'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "new" called using indirect syntax',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       349
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    8,
                                    9,
                                    9,
                                    8,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Objects::ProhibitIndirectSyntax',
                   '_severity' => 4,
                   '_source' => '$::ti = new Test::Inter $0;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => '"require" statement with library name as string',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => 'Use a bareword instead',
                   '_filename' => undef,
                   '_location' => [
                                    9,
                                    1,
                                    1,
                                    9,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::RequireBarewordIncludes',
                   '_severity' => 5,
                   '_source' => 'require "tests.pl";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "new" called using indirect syntax',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    11,
                                    12,
                                    12,
                                    11,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Objects::ProhibitIndirectSyntax',
                   '_severity' => 4,
                   '_source' => 'our $obj = new Date::Manip::Date;'
                 }, 'Perl::Critic::Violation' )
        ];
1;
