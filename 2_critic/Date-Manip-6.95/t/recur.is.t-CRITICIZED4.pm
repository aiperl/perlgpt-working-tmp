#!/usr/bin/perl

use warnings;
use strict;
use Test::Inter;
$::ti = new Test::Inter $0;  # this line violates Perl::Critic::Policy::Objects::ProhibitIndirectSyntax, with severity 4 out of 5, and description 'Subroutine "new" called using indirect syntax'
require "tests.pl";  # this line violates Perl::Critic::Policy::Modules::RequireBarewordIncludes, with severity 5 out of 5, and description '"require" statement with library name as string', and explanation 'Use a bareword instead'

my $obj = new Date::Manip::Recur;  # this line violates Perl::Critic::Policy::Objects::ProhibitIndirectSyntax, with severity 4 out of 5, and description 'Subroutine "new" called using indirect syntax'

sub test {
   my ($test)=@_;
   if ($test eq 'date') {
      return $obj->is_date();
   } elsif ($test eq 'delta') {
      return $obj->is_delta();
   } elsif ($test eq 'recur') {
      return $obj->is_recur();
   }
}

my $tests="

date => 0

delta => 0

recur => 1

";

$::ti->tests(func  => \&test,
             tests => $tests);
$::ti->done_testing();

#Local Variables:
#mode: cperl
#indent-tabs-mode: nil
#cperl-indent-level: 3
#cperl-continued-statement-offset: 2
#cperl-continued-brace-offset: 0
#cperl-brace-offset: 0
#cperl-brace-imaginary-offset: 0
#cperl-label-offset: 0
#End:
