$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Subroutine "new" called using indirect syntax',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       349
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    6,
                                    9,
                                    9,
                                    6,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Objects::ProhibitIndirectSyntax',
                   '_severity' => 4,
                   '_source' => '$::ti = new Test::Inter $0;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => '"require" statement with library name as string',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => 'Use a bareword instead',
                   '_filename' => undef,
                   '_location' => [
                                    7,
                                    1,
                                    1,
                                    7,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::RequireBarewordIncludes',
                   '_severity' => 5,
                   '_source' => 'require "tests.pl";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       178
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    9,
                                    1,
                                    1,
                                    9,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub test {'
                 }, 'Perl::Critic::Violation' )
        ];
1;
