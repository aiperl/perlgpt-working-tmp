#!/usr/bin/perl

use warnings;
use strict;
use Test::Inter;
$::ti = new Test::Inter $0;  # this line violates Perl::Critic::Policy::Objects::ProhibitIndirectSyntax, with severity 4 out of 5, and description 'Subroutine "new" called using indirect syntax'
require "tests.pl";  # this line violates Perl::Critic::Policy::Modules::RequireBarewordIncludes, with severity 5 out of 5, and description '"require" statement with library name as string', and explanation 'Use a bareword instead'

our $obj = new Date::Manip::Date;  # this line violates Perl::Critic::Policy::Objects::ProhibitIndirectSyntax, with severity 4 out of 5, and description 'Subroutine "new" called using indirect syntax'
$obj->config("forcedate","now,America/New_York");
$obj->config("printable",2);

sub test {
   my(@test)=@_;
   my $err = $obj->set(@test);
   if ($err) {
      return $obj->err();
   } else {
      my $ret = $obj->value();
      return $ret;
   }
}

my $tests="

date [ 1996 1 1 12 0 0 ]  => 1996-01-01-12:00:00

date [ 1996 13 1 12 0 0 ] => '[set] Invalid date argument'

";

$::ti->tests(func  => \&test,
             tests => $tests);
$::ti->done_testing();

1;
#Local Variables:
#mode: cperl
#indent-tabs-mode: nil
#cperl-indent-level: 3
#cperl-continued-statement-offset: 2
#cperl-continued-brace-offset: 0
#cperl-brace-offset: 0
#cperl-brace-imaginary-offset: 0
#cperl-label-offset: 0
#End:
