$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Subroutine "new" called using indirect syntax',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       349
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    17,
                                    15,
                                    15,
                                    17,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Objects::ProhibitIndirectSyntax',
                   '_severity' => 4,
                   '_source' => '$ti      = new Test::Inter $0;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Bareword file handle opened',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       202,
                                       204
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    65,
                                    4,
                                    4,
                                    65,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::ProhibitBarewordFileHandles',
                   '_severity' => 5,
                   '_source' => 'open(IN,"$testdir/_version.ign");'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Two-argument "open" used',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       207
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    65,
                                    4,
                                    4,
                                    65,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::ProhibitTwoArgOpen',
                   '_severity' => 5,
                   '_source' => 'open(IN,"$testdir/_version.ign");'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "new" called using indirect syntax',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    71,
                                    15,
                                    15,
                                    71,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Objects::ProhibitIndirectSyntax',
                   '_severity' => 4,
                   '_source' => 'my $in      = new IO::File;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       169
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    87,
                                    14,
                                    14,
                                    87,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => 'my @v   = grep /^\\$VERSION\\s*=\\s*[\'"]\\d+\\.\\d+[\'"];$/, @tmp;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => '"require" statement with library name as string',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => 'Use a bareword instead',
                   '_filename' => undef,
                   '_location' => [
                                    134,
                                    13,
                                    13,
                                    134,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::RequireBarewordIncludes',
                   '_severity' => 5,
                   '_source' => 'require "./$f";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "eval"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       161
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    137,
                                    18,
                                    18,
                                    137,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval',
                   '_severity' => 5,
                   '_source' => 'my $v = eval "${package}::version()";'
                 }, 'Perl::Critic::Violation' )
        ];
1;
