$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Code before strictures are enabled',
                   '_element_class' => 'PPI::Statement::Variable',
                   '_explanation' => [
                                       429
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    10,
                                    1,
                                    1,
                                    10,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict',
                   '_severity' => 5,
                   '_source' => 'my $HAS_LVALUE;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Code before warnings are enabled',
                   '_element_class' => 'PPI::Statement::Variable',
                   '_explanation' => [
                                       431
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    10,
                                    1,
                                    1,
                                    10,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings',
                   '_severity' => 4,
                   '_source' => 'my $HAS_LVALUE;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "test_generator" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       197
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    16,
                                    1,
                                    1,
                                    16,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub test_generator {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Multiple "package" declarations',
                   '_element_class' => 'PPI::Statement::Package',
                   '_explanation' => 'Limit to one per file',
                   '_filename' => undef,
                   '_location' => [
                                    70,
                                    9,
                                    9,
                                    70,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::ProhibitMultiplePackages',
                   '_severity' => 4,
                   '_source' => 'package My::Test::LValue::2;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Multiple "package" declarations',
                   '_element_class' => 'PPI::Statement::Package',
                   '_explanation' => 'Limit to one per file',
                   '_filename' => undef,
                   '_location' => [
                                    90,
                                    13,
                                    13,
                                    90,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::ProhibitMultiplePackages',
                   '_severity' => 4,
                   '_source' => 'package My::Test::LValue::3;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Multiple "package" declarations',
                   '_element_class' => 'PPI::Statement::Package',
                   '_explanation' => 'Limit to one per file',
                   '_filename' => undef,
                   '_location' => [
                                    101,
                                    13,
                                    13,
                                    101,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::ProhibitMultiplePackages',
                   '_severity' => 4,
                   '_source' => 'package My::Test::LValue::4;'
                 }, 'Perl::Critic::Violation' )
        ];
1;
