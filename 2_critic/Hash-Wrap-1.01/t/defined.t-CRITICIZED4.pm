#! perl

use Test2::V0;

use Scalar::Util 'blessed';

use Hash::Wrap ( {  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict, with severity 5 out of 5, and description 'Code before strictures are enabled'  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'
        -as      => 'wrap_as_defined',
        -defined => 1
    },
    {
        -as      => 'wrap_as_foo',
        -defined => 'foo'
    },
);


sub my_hash { ( a => 1, b => undef ); }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "my_hash" does not end with "return"'

subtest "default" => sub {
    my %hash = my_hash;
    my $obj  = wrap_as_defined \%hash;

    is( $obj->a, 1,     'retrieve value' );
    is( $obj->b, undef, 'retrieve another value' );

    ok( $obj->defined( 'a' ),  "a defined" );
    ok( !$obj->defined( 'b' ), "existant undefined is not defined" );
    ok( !$obj->defined( 'd' ), "non-existant is not defined" );
    ok( !exists $hash{d},      "defined doesn't autovivify" );
};

subtest "rename" => sub {
    my %hash = my_hash;
    my $obj  = wrap_as_foo \%hash;
    is( $obj->a, 1, 'retrieve value' );

    ok( $obj->foo( 'a' ),  "a foo" );
    ok( !$obj->foo( 'b' ), "existant unfoo is not foo" );
    ok( !$obj->foo( 'd' ), "non-existant is not foo" );
    ok( !exists $hash{d},  "foo doesn't autovivify" );
};

done_testing;
