#! perl

use Test2::V0;
use Test2::API qw/ context /;

use Scalar::Util 'blessed';

sub test_generator {  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "test_generator" does not end with "return"'  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict, with severity 5 out of 5, and description 'Code before strictures are enabled'  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'

    my ( $generator ) = @_;

    my $ctx = context();

    my %hash = ( a => 1, b => 2 );

    my $obj = $generator->( \%hash );

    is( $obj->a, 1, 'retrieve value' );
    is( $obj->b, 2, 'retrieve another value' );

    $hash{a} = 2;
    is( $obj->a, 2, 'object scalar not independent of hash' );


    is( $obj->c, undef, 'unknown attribute' );

    $hash{c} = 4;
    is( $obj->c, 4, 'retrieve value added through hash' );

    delete $obj->{c};
    is( $obj->c, undef, 'retrieve deleted attribute' );

    $obj->a( 22 );
    is( $obj->a,  22, 'setter' );
    is( $hash{a}, 22, 'setter reflected in hash' );

    $ctx->release;
}

use Hash::Wrap ( { -as => 'undefined', -undef => 1 } );

subtest 'default' => sub {

    test_generator( \&undefined );

};

use Hash::Wrap ( {
    -as    => 'undefined_created_class',
    -undef => 1,
    -class => 'My::CreatedClass::Lvalue',
} );

subtest 'create class' => sub {

    test_generator( \&undefined_created_class );
};

done_testing;
