#! perl

use Test2::V0;

{  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict, with severity 5 out of 5, and description 'Code before strictures are enabled'  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'
    package P1;

    use Hash::Wrap;
}

my %hash = ( a => 1, b => 2 );

my $q = P1::wrap_hash( \%hash );

ok( lives { $q->a }, 'use Hash::Wrap;' ) or note $@;


{
    package P2;  # this line violates Perl::Critic::Policy::Modules::ProhibitMultiplePackages, with severity 4 out of 5, and description 'Multiple "package" declarations', and explanation 'Limit to one per file'

    use Hash::Wrap ();
}

like(
    dies { P2::wrap_hash() },
    qr{undefined subroutine.*at t/import.t}i,
    q[don't export]
);

done_testing;
