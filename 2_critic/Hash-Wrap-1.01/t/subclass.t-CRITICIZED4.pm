#! perl


use Test2::V0;

{  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict, with severity 5 out of 5, and description 'Code before strictures are enabled'  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'
    package Hashed;

    use Hash::Wrap ( { -base => 1 } );

}

{
    package Hashed::Potatoes;  # this line violates Perl::Critic::Policy::Modules::ProhibitMultiplePackages, with severity 4 out of 5, and description 'Multiple "package" declarations', and explanation 'Limit to one per file'

    our @ISA = qw[ Hashed ];

    sub foo { 30 }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "foo" does not end with "return"'
}

my $obj = Hashed::Potatoes->new( { foo => 10, bar => 20 } );

is( $obj->foo, 30, "method" );

SKIP: {

    my $bar;
    my $accessor_exists = ok( lives { $bar = $obj->bar }, "accessor exists" );

    unless ( $accessor_exists ) {
        note $@;
        skip( "accessor tests as accessors are broken\n" );

        is( $bar, 20, "accessor value" );

        $obj->{fries} = 40;

        is( $obj->fries, 40, "new accessor" );

        like(
            dies { $obj->cakes },
            qr/can't locate object method.*subclass.t/i,
            "bad element"
        );

    }
}

done_testing;
