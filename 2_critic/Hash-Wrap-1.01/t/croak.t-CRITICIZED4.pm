#! perl

use Test2::V0;

use Hash::Wrap;

my $obj = wrap_hash( {} );  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict, with severity 5 out of 5, and description 'Code before strictures are enabled'  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'

like(
    dies { $obj->foo },
    qr{t/croak.t}, "croak message has correct call frame",
);


done_testing;
