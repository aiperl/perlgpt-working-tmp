#! perl

use Test2::V0;

use Scalar::Util 'blessed';

use Hash::Wrap ( {  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict, with severity 5 out of 5, and description 'Code before strictures are enabled'  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'
        -as     => 'wrap_as_exists',
        -exists => 1
    },
    {
        -as     => 'wrap_as_foo',
        -exists => 'foo'
    },
);


sub my_hash { ( a => 1, b => 2, c => [9] ); }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "my_hash" does not end with "return"'

subtest "default" => sub {
    my %hash = my_hash;
    my $obj  = wrap_as_exists \%hash;

    is( $obj->a, 1,   'retrieve value' );
    is( $obj->b, 2,   'retrieve another value' );
    is( $obj->c, [9], 'retrieve another value' );

    ok( $obj->exists( 'a' ),  "a exists" );
    ok( !$obj->exists( 'd' ), "d does not exist" );
    ok( !exists $hash{d},     "exists doesn't autovivify" );
};

subtest "rename" => sub {
    my %hash = my_hash;
    my $obj  = wrap_as_foo \%hash;

    is( $obj->a, 1,   'retrieve value' );
    is( $obj->b, 2,   'retrieve another value' );
    is( $obj->c, [9], 'retrieve another value' );

    ok( $obj->foo( 'a' ),  "a exists" );
    ok( !$obj->foo( 'd' ), "d does not exist" );
};

done_testing;
