use strict;  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'
use warnings;

# this test was generated with Dist::Zilla::Plugin::Test::NoBreakpoints 0.0.2

use Test::More 0.88;
use Test::NoBreakpoints 0.15;

all_files_no_breakpoints_ok();

done_testing;  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'
