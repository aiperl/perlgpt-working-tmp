use strict;  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'
use warnings;

# this test was generated with Dist::Zilla::Plugin::Test::CleanNamespaces 0.006

use Test::More 0.94;
use Test::CleanNamespaces 0.15;

subtest all_namespaces_clean => sub { all_namespaces_clean() };

done_testing;  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'
