use Test::Mojo;  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'
use Test::More;
use Mojolicious::Lite -signatures;
use Mojolicious::Plugin::WebComponent;

$ENV{MOJO_MODE} = 'test';  # this line violates Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars, with severity 4 out of 5, and description 'Magic variable "$ENV" should be assigned as "local"'

plugin WebComponent => {};

app->static->paths->[0] =~ s/\/public/\/files/;

get '/' => sub($c) {
    $c->render(text => 'Hello World!');
};

get '/index' => sub($c) {
    $c->render(template => 'default/index');
};

my $t = Test::Mojo->new;
$t->get_ok('/')->status_is(200);

$t->get_ok('/index')->status_is(200)->content_like(qr{<my-component></my-component>});

is_deeply $t->app->component('my-component', { requestId => 'test-id' }),
    "<script src='/component/test-id/my-component.js'></script>";

$t->get_ok('/component/test-id/my-component.js')->status_is(200)
    ->content_like(qr{<div>This is a custom web component</div>});

done_testing;  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'
