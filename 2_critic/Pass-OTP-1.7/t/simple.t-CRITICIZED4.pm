use Test::More;  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'

use utf8;
use strict;
use warnings;

require_ok 'Pass::OTP';
require_ok 'Pass::OTP::URI';

is(
    Pass::OTP::otp(
        secret => "00",
    ),
    '328482',
    'oathtool 00',
);

is(
    Pass::OTP::otp(
        secret => "00",
        counter => 100,
    ),
    '032003',
    'oathtool -c 100 00',
);

is(
    Pass::OTP::otp(
        Pass::OTP::URI::parse('otpauth://hotp/Test?secret=abcdefgh')
    ),
    '058591',
    'otptool otpauth://hotp/Test?secret=abcdefgh',
);

is(
    Pass::OTP::otp(
        Pass::OTP::URI::parse('otpauth://totp/Test?secret=abcdefgh&issuer=Steam&now=1')
    ),
    'KMP7M',
    'otptool otpauth://totp/Test?secret=abcdefgh&issuer=Steam',
);

done_testing(6);  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'
