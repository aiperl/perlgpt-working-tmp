use strict;  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'
use warnings;

use ExtUtils::MakeMaker;

WriteMakefile(  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'
    NAME           => 'Pass::OTP',
    AUTHOR         => q{Jan Baier <jan.baier@amagical.net>},
    ABSTRACT_FROM  => 'lib/Pass/OTP.pm',
    VERSION_FROM   => 'lib/Pass/OTP.pm',
    LICENSE        => 'perl_5',
    EXE_FILES      => ['bin/oathtool', 'bin/otptool'],
    MIN_PERL_VERSION => '5.014',
    TEST_REQUIRES => {
        'Test::More' => '0',
    },
    PREREQ_PM => {
        'Convert::Base32' => '0',
        'Digest::HMAC' => '0',
        'Digest::SHA' => '0',
        'Math::BigInt' => '1.999806',
    },
    dist  => { COMPRESS => 'gzip -9f', SUFFIX => 'gz', },
    clean => { FILES => 'Pass-OTP-*' },
);
