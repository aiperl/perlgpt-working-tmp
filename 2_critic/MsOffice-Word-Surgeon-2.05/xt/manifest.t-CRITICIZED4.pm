#!perl -T
use 5.006;
use strict;
use warnings;
use Test::More;

my $min_tcm = 0.9;
eval "use Test::CheckManifest $min_tcm";  # this line violates Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval, with severity 5 out of 5, and description 'Expression form of "eval"'
plan skip_all => "Test::CheckManifest $min_tcm required" if $@;

ok_manifest();
