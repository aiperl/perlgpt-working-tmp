#!perl -T
use 5.006;
use strict;
use warnings;
use Test::More;

# Ensure a recent version of Test::Pod
my $min_tp = 1.22;
eval "use Test::Pod $min_tp";  # this line violates Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval, with severity 5 out of 5, and description 'Expression form of "eval"'
plan skip_all => "Test::Pod $min_tp required for testing POD" if $@;

all_pod_files_ok();
