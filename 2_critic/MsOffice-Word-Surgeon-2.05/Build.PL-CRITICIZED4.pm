use strict;  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'
use warnings;
use Module::Build;
Module::Build->VERSION('0.4004');

my $builder = Module::Build->new(
    module_name         => 'MsOffice::Word::Surgeon',
    license             => 'artistic_2',
    dist_author         => q{DAMI <dami@cpan.org>},
    dist_version_from   => 'lib/MsOffice/Word/Surgeon.pm',
    release_status      => 'stable',
    configure_requires => {
        'Module::Build' => '0.4004',
    },
    test_requires => {
        'Test::More' => '0',
    },
    requires => {
      perl                        => '5.24.0',
      'Archive::Zip'              => undef,
      'Carp::Clan'                => undef,
      'Encode'                    => undef,
      'List::Util'                => undef,
      'Moose'                     => undef,
      'MooseX::StrictConstructor' => undef,
      'XML::LibXML'               => undef,
      'namespace::clean'          => undef,
      'POSIX'                     => undef,
      'Exporter'                  => undef,
    },
    add_to_cleanup     => [ 'MsOffice-Word-Surgeon-*' ],
    meta_merge => {
      resources => {
        repository => 'https://github.com/damil/MsOffice-Word-Surgeon',
       }
     },
);


$builder->add_build_element('docx');
$builder->create_build_script();  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'
