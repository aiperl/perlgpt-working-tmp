$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Symbols are exported by default',
                   '_element_class' => 'PPI::Statement::Variable',
                   '_explanation' => 'Use \'@EXPORT_OK\' or \'%EXPORT_TAGS\' instead',
                   '_filename' => undef,
                   '_location' => [
                                    7,
                                    1,
                                    1,
                                    7,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::ProhibitAutomaticExportation',
                   '_severity' => 4,
                   '_source' => 'our @EXPORT = qw/maybe_preserve_spaces is_at_run_level decode_entities encode_entities/;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       178
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    28,
                                    1,
                                    1,
                                    28,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub decode_entities { $_[0] =~ s{&($entity_names);}{$entities{$1}               }eg; }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "decode_entities" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       197
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    28,
                                    1,
                                    1,
                                    28,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub decode_entities { $_[0] =~ s{&($entity_names);}{$entities{$1}               }eg; }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    29,
                                    1,
                                    1,
                                    29,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub encode_entities { $_[0] =~ s{($entity_chars)}  {\'&\'.$entity_for_char{$1}.\';\'}eg; }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "encode_entities" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    29,
                                    1,
                                    1,
                                    29,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub encode_entities { $_[0] =~ s{($entity_chars)}  {\'&\'.$entity_for_char{$1}.\';\'}eg; }'
                 }, 'Perl::Critic::Violation' )
        ];
1;
