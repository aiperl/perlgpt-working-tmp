
BEGIN {  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict, with severity 5 out of 5, and description 'Code before strictures are enabled'  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'
  unless ($ENV{AUTHOR_TESTING}) {
    print qq{1..0 # SKIP these tests are for testing by the author\n};
    exit
  }
}

use strict;
use warnings;

use Test::More;
use Test::MinimumVersion;
all_minimum_version_from_metayml_ok();  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'
