use strict;  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'
use warnings;

use English qw(-no_match_vars);
use File::Object;
use Test::More 'tests' => 2;
use Test::NoWarnings;

# Test.
SKIP: {  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'
	if ($PERL_VERSION lt v5.8.0) {
		skip 'Perl version lesser then 5.8.0.', 1;
	}
	require Test::Pod;
	Test::Pod::pod_file_ok(File::Object->new->up(2)->file('Item.pm')->s);
};
