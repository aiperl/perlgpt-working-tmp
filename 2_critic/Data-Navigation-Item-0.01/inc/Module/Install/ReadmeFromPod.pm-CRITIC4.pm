$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Subroutine prototypes used',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       194
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    17,
                                    5,
                                    5,
                                    16,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::ProhibitSubroutinePrototypes',
                   '_severity' => 5,
                   '_source' => 'sub capture(&;@);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       178
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    39,
                                    1,
                                    1,
                                    47,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub readme_from {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Module does not end with "1;"',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => 'Must end with a recognizable true value',
                   '_filename' => undef,
                   '_location' => [
                                    179,
                                    1,
                                    1,
                                    187,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::RequireEndWithOne',
                   '_severity' => 4,
                   '_source' => '\'Readme!\';'
                 }, 'Perl::Critic::Violation' )
        ];
1;
