$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Subroutine "Func1" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       197
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    5,
                                    1,
                                    1,
                                    5,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub Func1 { "My-Func1" }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Code before strictures are enabled',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       429
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    5,
                                    1,
                                    1,
                                    5,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict',
                   '_severity' => 5,
                   '_source' => 'sub Func1 { "My-Func1" }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Code before warnings are enabled',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       431
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    5,
                                    1,
                                    1,
                                    5,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings',
                   '_severity' => 4,
                   '_source' => 'sub Func1 { "My-Func1" }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "Func2" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    6,
                                    1,
                                    1,
                                    6,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub Func2 { "My-Func2" }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Multiple "package" declarations',
                   '_element_class' => 'PPI::Statement::Package',
                   '_explanation' => 'Limit to one per file',
                   '_filename' => undef,
                   '_location' => [
                                    9,
                                    1,
                                    1,
                                    9,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::ProhibitMultiplePackages',
                   '_severity' => 4,
                   '_source' => 'package MyTest;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_setup_testmode_debugger" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    47,
                                    1,
                                    1,
                                    47,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _setup_testmode_debugger {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Close filehandles as soon as possible after opening them',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       209
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    76,
                                    5,
                                    5,
                                    76,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::RequireBriefOpen',
                   '_severity' => 4,
                   '_source' => 'open my $NULL, ">", "/dev/null" or die $!;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_define_expected_vars" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    82,
                                    1,
                                    1,
                                    82,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _define_expected_vars {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_define_help_stdout" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    180,
                                    1,
                                    1,
                                    180,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _define_help_stdout {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_define_test_cases" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    195,
                                    1,
                                    1,
                                    195,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _define_test_cases {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "init_case" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    783,
                                    1,
                                    1,
                                    783,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub init_case {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_run_case" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    795,
                                    1,
                                    1,
                                    795,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _run_case {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "eval"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       161
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    815,
                                    24,
                                    24,
                                    815,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval',
                   '_severity' => 5,
                   '_source' => '$eval_return = eval $step_return // "";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Mixed high and low-precedence booleans',
                   '_element_class' => 'PPI::Statement::Expression',
                   '_explanation' => [
                                       70
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    865,
                                    30,
                                    30,
                                    865,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators',
                   '_severity' => 4,
                   '_source' => '$fail and !$case->{todo}'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_test_repl_vars" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    880,
                                    1,
                                    1,
                                    880,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _test_repl_vars {'
                 }, 'Perl::Critic::Violation' )
        ];
1;
