$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Expression form of "eval"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       161
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    43,
                                    52,
                                    52,
                                    43,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval',
                   '_severity' => 5,
                   '_source' => 'plan skip_all => "REST::Neo4p unavailable" unless eval "require REST::Neo4p; 1";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "eval"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    44,
                                    56,
                                    56,
                                    44,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval',
                   '_severity' => 5,
                   '_source' => 'plan skip_all => "REST::Neo4p version too old" unless eval "REST::Neo4p->VERSION(\'0.4003\')";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Warnings disabled',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => [
                                       431
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    48,
                                    3,
                                    3,
                                    48,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::ProhibitNoWarnings',
                   '_severity' => 4,
                   '_source' => 'no warnings \'once\';'
                 }, 'Perl::Critic::Violation' )
        ];
1;
