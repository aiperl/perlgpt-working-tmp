$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Expression form of "eval"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       161
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    41,
                                    54,
                                    54,
                                    41,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval',
                   '_severity' => 5,
                   '_source' => 'plan skip_all => "(REST::Neo4p unavailable)" unless eval "require REST::Neo4p; 1";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "eval"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    49,
                                    66,
                                    66,
                                    49,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval',
                   '_severity' => 5,
                   '_source' => 'plan skip_all => "(Neo4j::Cypher::Abstract unavailable)" unless eval "require Neo4j::Cypher::Abstract; 1";'
                 }, 'Perl::Critic::Violation' )
        ];
1;
