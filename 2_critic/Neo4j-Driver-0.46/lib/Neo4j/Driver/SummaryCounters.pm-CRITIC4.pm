$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Code not contained in explicit package',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => 'Violates encapsulation',
                   '_filename' => undef,
                   '_location' => [
                                    1,
                                    1,
                                    1,
                                    1,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::RequireExplicitPackage',
                   '_severity' => 4,
                   '_source' => 'use 5.010;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Don\'t turn off strict for large blocks of code',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => [
                                       433
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    29,
                                    1,
                                    1,
                                    29,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::ProhibitProlongedStrictureOverride',
                   '_severity' => 4,
                   '_source' => 'no strict \'refs\';'
                 }, 'Perl::Critic::Violation' )
        ];
1;
