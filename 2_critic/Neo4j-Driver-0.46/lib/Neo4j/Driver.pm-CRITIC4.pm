$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Code not contained in explicit package',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => 'Violates encapsulation',
                   '_filename' => undef,
                   '_location' => [
                                    1,
                                    1,
                                    1,
                                    1,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::RequireExplicitPackage',
                   '_severity' => 4,
                   '_source' => 'use 5.010;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_check_uri" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       197
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    71,
                                    1,
                                    1,
                                    71,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _check_uri {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_fix_neo4j_uri" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    108,
                                    1,
                                    1,
                                    108,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _fix_neo4j_uri {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       169
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    143,
                                    50,
                                    50,
                                    143,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => 'croak "Unsupported config option: $key" unless grep m/^$key$/, keys %OPTIONS;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "grep"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    203,
                                    34,
                                    34,
                                    203,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockGrep',
                   '_severity' => 4,
                   '_source' => 'push @unsupported, $key unless grep m/^$key$/, @$supported;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine name is a homonym for builtin function close',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       177
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    222,
                                    1,
                                    1,
                                    222,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::ProhibitBuiltinHomonyms',
                   '_severity' => 4,
                   '_source' => 'sub close {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "close" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    222,
                                    1,
                                    1,
                                    222,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub close {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Multiple "package" declarations',
                   '_element_class' => 'PPI::Statement::Package',
                   '_explanation' => 'Limit to one per file',
                   '_filename' => undef,
                   '_location' => [
                                    230,
                                    1,
                                    1,
                                    230,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::ProhibitMultiplePackages',
                   '_severity' => 4,
                   '_source' => 'package # private'
                 }, 'Perl::Critic::Violation' )
        ];
1;
