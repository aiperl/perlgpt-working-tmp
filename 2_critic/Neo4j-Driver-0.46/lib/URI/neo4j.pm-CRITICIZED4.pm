use 5.010;  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'
use strict;
use warnings;

package URI::neo4j;
$URI::neo4j::VERSION = '0.46';

use parent 'URI::_server';

sub default_port { 7687 }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "default_port" does not end with "return"'

1;
