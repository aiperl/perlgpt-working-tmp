$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Magic variable "$INC" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => [
                                       81,
                                       82
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    237,
                                    25,
                                    25,
                                    237,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$INC{\'Neo4j/Bolt.pm\'} = \'\';'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$INC" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    246,
                                    25,
                                    25,
                                    246,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$INC{\'Neo4j/Bolt.pm\'} = \'\';'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$a" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    335,
                                    5,
                                    5,
                                    335,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$a = { scheme => \'basic\', principal => \'foo\', credentials => \'\' };'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$a" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    338,
                                    5,
                                    5,
                                    338,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$a = { scheme => \'basic\', principal => \'\', credentials => \'bar\' };'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$a" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    354,
                                    5,
                                    5,
                                    354,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$a = { scheme => \'basic\', principal => "bar\\x{c4}\\x{80}\\x{ff}bar", credentials => \'\' };'
                 }, 'Perl::Critic::Violation' )
        ];
1;
