$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       178
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    33,
                                    1,
                                    1,
                                    33,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub response_for { $mock_plugin->response_for(undef, @_) }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "response_for" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       197
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    33,
                                    1,
                                    1,
                                    33,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub response_for { $mock_plugin->response_for(undef, @_) }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "@a" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => [
                                       81,
                                       82
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    51,
                                    16,
                                    16,
                                    51,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '@a = $tx->_run_multiple(@q)'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "@a" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    60,
                                    6,
                                    6,
                                    60,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '@a = $tx->_run_multiple([\'\'], [\'RETURN 23\']);'
                 }, 'Perl::Critic::Violation' )
        ];
1;
