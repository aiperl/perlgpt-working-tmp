use strict;  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'
use warnings;
package Neo4j_Test::NetModulePlugin;

use parent 'Neo4j::Driver::Plugin';

sub new {  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "new" does not end with "return"'
	my ($class, $net_module) = @_;
	bless \$net_module, $class;
}

sub register {  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "register" does not end with "return"'
	my ($self, $manager) = @_;
	my $net_module = $$self;
	
	$manager->add_handler(
		http_adapter_factory => sub {
			my ($continue, $driver) = @_;
			return $net_module->new($driver);
		},
	);
}


1;

__END__

This is a tiny wrapper that basically simulates the old net_module
config option using the new plug-in API. It can be used like this:

my $net_module = 'Local::MyOldNetModule';
$driver->plugin( Neo4j_Test::NetModulePlugin->new($net_module) );
