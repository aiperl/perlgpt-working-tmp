$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       178
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    14,
                                    1,
                                    1,
                                    14,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub new { bless [\\(my $b = undef), @_], shift }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "new" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       197
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    14,
                                    1,
                                    1,
                                    14,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub new { bless [\\(my $b = undef), @_], shift }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine name is a homonym for builtin function connect',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       177
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    15,
                                    1,
                                    1,
                                    15,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::ProhibitBuiltinHomonyms',
                   '_severity' => 4,
                   '_source' => 'sub connect { &new }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "connect" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    15,
                                    1,
                                    1,
                                    15,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub connect { &new }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "connect_tls" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    16,
                                    1,
                                    1,
                                    16,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub connect_tls { &new }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "connected" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    19,
                                    1,
                                    1,
                                    19,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub connected { 1 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "errnum" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    20,
                                    1,
                                    1,
                                    20,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub errnum { 0 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "errmsg" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    21,
                                    1,
                                    1,
                                    21,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub errmsg { undef }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "server_id" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    23,
                                    1,
                                    1,
                                    23,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub server_id { __PACKAGE__ }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "run_query" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    24,
                                    1,
                                    1,
                                    24,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub run_query { my $b = shift; ${$b->[0]} = 0; $b }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "field_names" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    40,
                                    1,
                                    1,
                                    40,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub field_names { 0..9 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "fetch_next" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    41,
                                    1,
                                    1,
                                    41,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub fetch_next { my $b = shift; return if ${$b->[0]}; ${$b->[0]} = 1; @row }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "update_counts" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    42,
                                    1,
                                    1,
                                    42,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub update_counts { {} }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "success" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    43,
                                    1,
                                    1,
                                    43,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub success { 1 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "failure" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    44,
                                    1,
                                    1,
                                    44,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub failure { 0 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Multiple "package" declarations',
                   '_element_class' => 'PPI::Statement::Package',
                   '_explanation' => 'Limit to one per file',
                   '_filename' => undef,
                   '_location' => [
                                    46,
                                    1,
                                    1,
                                    46,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::ProhibitMultiplePackages',
                   '_severity' => 4,
                   '_source' => 'package Local::Bolt::Txn;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Multiple "package" declarations',
                   '_element_class' => 'PPI::Statement::Package',
                   '_explanation' => 'Limit to one per file',
                   '_filename' => undef,
                   '_location' => [
                                    51,
                                    1,
                                    1,
                                    51,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::ProhibitMultiplePackages',
                   '_severity' => 4,
                   '_source' => 'package Local::Bolt::Failure;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "success" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    53,
                                    1,
                                    1,
                                    53,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub success { 0 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "failure" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    54,
                                    1,
                                    1,
                                    54,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub failure { 1 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "errnum" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    55,
                                    1,
                                    1,
                                    55,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub errnum { -22 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "server_errcode" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    56,
                                    1,
                                    1,
                                    56,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub server_errcode { "oops" }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "server_errmsg" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    57,
                                    1,
                                    1,
                                    57,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub server_errmsg { "" }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "protocol_version" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    58,
                                    1,
                                    1,
                                    58,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub protocol_version { 0 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "result_handlers" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    59,
                                    1,
                                    1,
                                    59,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub result_handlers { qw(Neo4j::Driver::Result::Bolt) }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Multiple "package" declarations',
                   '_element_class' => 'PPI::Statement::Package',
                   '_explanation' => 'Limit to one per file',
                   '_filename' => undef,
                   '_location' => [
                                    61,
                                    1,
                                    1,
                                    61,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::ProhibitMultiplePackages',
                   '_severity' => 4,
                   '_source' => 'package Local::Bolt::CxnFailure;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "connected" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    63,
                                    1,
                                    1,
                                    63,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub connected { 0 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "client_errnum" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    64,
                                    1,
                                    1,
                                    64,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub client_errnum { -13 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "client_errmsg" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    65,
                                    1,
                                    1,
                                    65,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub client_errmsg { "all wrong" }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Multiple "package" declarations',
                   '_element_class' => 'PPI::Statement::Package',
                   '_explanation' => 'Limit to one per file',
                   '_filename' => undef,
                   '_location' => [
                                    67,
                                    1,
                                    1,
                                    67,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::ProhibitMultiplePackages',
                   '_severity' => 4,
                   '_source' => 'package Local::Bolt::StreamFailure;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "success" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    69,
                                    1,
                                    1,
                                    69,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub success { ${shift->[0]} ? 0 : 1 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "failure" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    70,
                                    1,
                                    1,
                                    70,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub failure { ${shift->[0]} ? 1 : 0 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "client_errnum" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    71,
                                    1,
                                    1,
                                    71,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub client_errnum { ${shift->[0]} ? -666 : 0 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "client_errmsg" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    72,
                                    1,
                                    1,
                                    72,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub client_errmsg { "" }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Multiple "package" declarations',
                   '_element_class' => 'PPI::Statement::Package',
                   '_explanation' => 'Limit to one per file',
                   '_filename' => undef,
                   '_location' => [
                                    74,
                                    1,
                                    1,
                                    74,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::ProhibitMultiplePackages',
                   '_severity' => 4,
                   '_source' => 'package Local::Bolt::FailureRef;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    75,
                                    1,
                                    1,
                                    75,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub new { bless $_[1], shift }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "new" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    75,
                                    1,
                                    1,
                                    75,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub new { bless $_[1], shift }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "server_errcode" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    76,
                                    1,
                                    1,
                                    76,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub server_errcode { shift->{server_errcode} }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "server_errmsg" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    77,
                                    1,
                                    1,
                                    77,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub server_errmsg { shift->{server_errmsg} }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "client_errnum" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    78,
                                    1,
                                    1,
                                    78,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub client_errnum { shift->{client_errnum} // 0 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "client_errmsg" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    79,
                                    1,
                                    1,
                                    79,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub client_errmsg { shift->{client_errmsg} }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "errnum" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    80,
                                    1,
                                    1,
                                    80,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub errnum { shift->{errnum} // 0 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "errmsg" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    81,
                                    1,
                                    1,
                                    81,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub errmsg { shift->{errmsg} }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    82,
                                    1,
                                    1,
                                    82,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub reset_cxn { $_[0]->{$_} = $_[0]->{"reset_$_"} for qw( errnum errmsg ); }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "reset_cxn" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    82,
                                    1,
                                    1,
                                    82,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub reset_cxn { $_[0]->{$_} = $_[0]->{"reset_$_"} for qw( errnum errmsg ); }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_bolt_error" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    83,
                                    1,
                                    1,
                                    83,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _bolt_error { &Neo4j::Driver::Net::Bolt::_bolt_error }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Multiple "package" declarations',
                   '_element_class' => 'PPI::Statement::Package',
                   '_explanation' => 'Limit to one per file',
                   '_filename' => undef,
                   '_location' => [
                                    85,
                                    1,
                                    1,
                                    85,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::ProhibitMultiplePackages',
                   '_severity' => 4,
                   '_source' => 'package main;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    95,
                                    1,
                                    1,
                                    95,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub new_session {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "@a" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => [
                                       81,
                                       82
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    117,
                                    31,
                                    31,
                                    117,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '@a = $s->run(\'\')'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Warnings disabled',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => [
                                       431
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    201,
                                    3,
                                    3,
                                    201,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::ProhibitNoWarnings',
                   '_severity' => 4,
                   '_source' => 'no warnings \'deprecated\';'
                 }, 'Perl::Critic::Violation' )
        ];
1;
