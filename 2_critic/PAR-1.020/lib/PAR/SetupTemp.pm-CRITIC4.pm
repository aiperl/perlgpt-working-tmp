$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Code before strictures are enabled',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => [
                                       429
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    2,
                                    1,
                                    1,
                                    2,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict',
                   '_severity' => 5,
                   '_source' => '$PAR::SetupTemp::VERSION = \'1.002\';'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Code before warnings are enabled',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => [
                                       431
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    2,
                                    1,
                                    1,
                                    2,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings',
                   '_severity' => 4,
                   '_source' => '$PAR::SetupTemp::VERSION = \'1.002\';'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "set_par_temp_env" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       197
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    42,
                                    1,
                                    1,
                                    42,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub set_par_temp_env {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Mixed high and low-precedence booleans',
                   '_element_class' => 'PPI::Statement::Expression',
                   '_explanation' => [
                                       70
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    55,
                                    11,
                                    11,
                                    55,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators',
                   '_severity' => 4,
                   '_source' => '!$ENV{PAR_CLEAN} and my $mtime = (stat($PAR::SetupProgname::Progname))[9]'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Two-argument "open" used',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       207
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    59,
                                    24,
                                    24,
                                    59,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::ProhibitTwoArgOpen',
                   '_severity' => 5,
                   '_source' => '$ctx and open(my $fh, "<$PAR::SetupProgname::Progname")'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$ENV" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => [
                                       81,
                                       82
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    71,
                                    27,
                                    27,
                                    71,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$ENV{PAR_CLEAN} = 1;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$ENV" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[5]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    75,
                                    22,
                                    22,
                                    75,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$ENV{PAR_TEMP} = $stmpdir;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "map"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       169
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    96,
                                    6,
                                    6,
                                    96,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockMap',
                   '_severity' => 4,
                   '_source' => 'map $ENV{$_}, qw( PAR_TMPDIR TMPDIR TEMPDIR TEMP TMP )'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Integer with leading zeros: "0777"',
                   '_element_class' => 'PPI::Token::Number::Octal',
                   '_explanation' => [
                                       58
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    119,
                                    26,
                                    26,
                                    119,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitLeadingZeros',
                   '_severity' => 5,
                   '_source' => '$st[2] & 0777'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Integer with leading zeros: "0700"',
                   '_element_class' => 'PPI::Token::Number::Octal',
                   '_explanation' => $VAR1->[8]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    119,
                                    35,
                                    35,
                                    119,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitLeadingZeros',
                   '_severity' => 5,
                   '_source' => '            || ($st[2] & 0777) != 0700'
                 }, 'Perl::Critic::Violation' )
        ];
1;
