$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Code before strictures are enabled',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => [
                                       429
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    2,
                                    1,
                                    1,
                                    2,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict',
                   '_severity' => 5,
                   '_source' => '$PAR::SetupProgname::VERSION = \'1.002\';'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Code before warnings are enabled',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => [
                                       431
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    2,
                                    1,
                                    1,
                                    2,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings',
                   '_severity' => 4,
                   '_source' => '$PAR::SetupProgname::VERSION = \'1.002\';'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "set_progname" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       197
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    31,
                                    1,
                                    1,
                                    31,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub set_progname {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Mixed high and low-precedence booleans',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => [
                                       70
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    39,
                                    9,
                                    9,
                                    39,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators',
                   '_severity' => 4,
                   '_source' => '( () = File::Spec->splitdir($Progname) ) > 1 or !$ENV{PAR_PROGNAME}'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Two-argument "open" used',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       207
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    40,
                                    13,
                                    13,
                                    40,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::ProhibitTwoArgOpen',
                   '_severity' => 5,
                   '_source' => 'open my $fh, $Progname'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Close filehandles as soon as possible after opening them',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       209
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    40,
                                    13,
                                    13,
                                    40,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::RequireBriefOpen',
                   '_severity' => 4,
                   '_source' => 'open my $fh, $Progname'
                 }, 'Perl::Critic::Violation' )
        ];
1;
