$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Code before strictures are enabled',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => [
                                       429
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    2,
                                    1,
                                    1,
                                    2,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict',
                   '_severity' => 5,
                   '_source' => '$PAR::VERSION = \'1.020\';'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Code before warnings are enabled',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => [
                                       431
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    2,
                                    1,
                                    1,
                                    2,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings',
                   '_severity' => 4,
                   '_source' => '$PAR::VERSION = \'1.020\';'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "eval"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       161
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    15,
                                    9,
                                    9,
                                    15,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval',
                   '_severity' => 5,
                   '_source' => 'eval \'require prefork\''
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       178
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    345,
                                    1,
                                    1,
                                    345,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub import {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "import" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       197
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    345,
                                    1,
                                    1,
                                    345,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub import {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Mixed high and low-precedence booleans',
                   '_element_class' => 'PPI::Statement::Expression',
                   '_explanation' => [
                                       70
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    400,
                                    13,
                                    13,
                                    400,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators',
                   '_severity' => 4,
                   '_source' => '$progname and !$member'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Mixed high and low-precedence booleans',
                   '_element_class' => 'PPI::Statement::Expression',
                   '_explanation' => $VAR1->[5]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    413,
                                    13,
                                    13,
                                    413,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators',
                   '_severity' => 4,
                   '_source' => 'defined $ARGV[0] and !$member'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "eval"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    524,
                                    5,
                                    5,
                                    524,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval',
                   '_severity' => 5,
                   '_source' => 'eval "require PAR::Repository::Client; 1;";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "eval"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    525,
                                    19,
                                    19,
                                    525,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval',
                   '_severity' => 5,
                   '_source' => '$@ or not eval PAR::Repository::Client->VERSION >= 0.04'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "eval"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    529,
                                    33,
                                    33,
                                    529,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval',
                   '_severity' => 5,
                   '_source' => '$opt->{upgrade} and not eval PAR::Repository::Client->VERSION >= 0.22'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "eval"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    533,
                                    38,
                                    38,
                                    533,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval',
                   '_severity' => 5,
                   '_source' => '$opt->{dependencies} and not eval PAR::Repository::Client->VERSION >= 0.23'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    567,
                                    1,
                                    1,
                                    567,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _first_member {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_run_member_from_par" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    596,
                                    1,
                                    1,
                                    596,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _run_member_from_par {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$ENV" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => [
                                       81,
                                       82
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    609,
                                    17,
                                    17,
                                    609,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$ENV{PAR_0} = $filename;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_run_member" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    617,
                                    1,
                                    1,
                                    617,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _run_member {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$INC" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    638,
                                    49,
                                    49,
                                    638,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$INC{$_[1]} = $filename;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$ENV" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    640,
                                    17,
                                    17,
                                    640,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$ENV{PAR_0} = $filename;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_run_external_file" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    648,
                                    1,
                                    1,
                                    648,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _run_external_file {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$ENV" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    663,
                                    17,
                                    17,
                                    663,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$ENV{PAR_0} = $filename;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Close filehandles as soon as possible after opening them',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       209
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    704,
                                    9,
                                    9,
                                    704,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::RequireBriefOpen',
                   '_severity' => 4,
                   '_source' => 'open my $lock, ">", $inc_lock or die qq[can\'t open "$inc_lock": $!];'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Warnings disabled',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => [
                                       431
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    790,
                                    17,
                                    17,
                                    790,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::ProhibitNoWarnings',
                   '_severity' => 4,
                   '_source' => 'no warnings;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$INC" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    863,
                                    21,
                                    21,
                                    863,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$INC{$file} = $LastTempFile if (lc($file) =~ /^(?!tk).*\\.pm$/);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "reload_libs" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    870,
                                    1,
                                    1,
                                    870,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub reload_libs {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    913,
                                    1,
                                    1,
                                    913,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub unpar {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Integer with leading zeros: "0777"',
                   '_element_class' => 'PPI::Token::Number::Octal',
                   '_explanation' => [
                                       58
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    934,
                                    36,
                                    36,
                                    934,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitLeadingZeros',
                   '_severity' => 5,
                   '_source' => 'mkdir $ENV{PAR_CACHE}, 0777;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$ENV" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    936,
                                    33,
                                    33,
                                    936,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$ENV{PAR_CACHE} = File::Spec->catdir(File::Spec->tmpdir, \'par\');'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Integer with leading zeros: "0777"',
                   '_element_class' => 'PPI::Token::Number::Octal',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    937,
                                    40,
                                    40,
                                    937,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitLeadingZeros',
                   '_severity' => 5,
                   '_source' => 'mkdir $ENV{PAR_CACHE}, 0777;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Close filehandles as soon as possible after opening them',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[19]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    982,
                                    13,
                                    13,
                                    982,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::RequireBriefOpen',
                   '_severity' => 4,
                   '_source' => 'open my $fh, \'<\', $par;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$ENV" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[13]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1056,
                                    27,
                                    27,
                                    1056,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$ENV{$key} = $tempdir;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Mixed high and low-precedence booleans',
                   '_element_class' => 'PPI::Statement::Expression',
                   '_explanation' => $VAR1->[5]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1091,
                                    9,
                                    9,
                                    1091,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators',
                   '_severity' => 4,
                   '_source' => '$ENV{PAR_CLEAN} or !defined $name'
                 }, 'Perl::Critic::Violation' )
        ];
1;
