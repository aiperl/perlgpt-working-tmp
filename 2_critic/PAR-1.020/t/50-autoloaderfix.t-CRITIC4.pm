$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Code before strictures are enabled',
                   '_element_class' => 'PPI::Statement::Scheduled',
                   '_explanation' => [
                                       429
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    6,
                                    1,
                                    1,
                                    6,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict',
                   '_severity' => 5,
                   '_source' => 'BEGIN { $ENV{PAR_TMPDIR} = File::Temp::tempdir(TMPDIR => 1, CLEANUP => 1); }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Code before warnings are enabled',
                   '_element_class' => 'PPI::Statement::Scheduled',
                   '_explanation' => [
                                       431
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    6,
                                    1,
                                    1,
                                    6,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings',
                   '_severity' => 4,
                   '_source' => 'BEGIN { $ENV{PAR_TMPDIR} = File::Temp::tempdir(TMPDIR => 1, CLEANUP => 1); }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$ENV" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => [
                                       81,
                                       82
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    6,
                                    26,
                                    26,
                                    6,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$ENV{PAR_TMPDIR} = File::Temp::tempdir(TMPDIR => 1, CLEANUP => 1);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$|" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    8,
                                    3,
                                    3,
                                    8,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$|=1;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       178
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    17,
                                    1,
                                    1,
                                    17,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub new {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Multiple "package" declarations',
                   '_element_class' => 'PPI::Statement::Package',
                   '_explanation' => 'Limit to one per file',
                   '_filename' => undef,
                   '_location' => [
                                    21,
                                    1,
                                    1,
                                    21,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::ProhibitMultiplePackages',
                   '_severity' => 4,
                   '_source' => 'package main;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$INC" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    23,
                                    16,
                                    16,
                                    23,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$INC{"Bar.pm"} = $0;'
                 }, 'Perl::Critic::Violation' )
        ];
1;
