#!/usr/bin/perl
# Problem doesn't manifest if Test::More is in effect?
# What the hell?

use File::Temp ();
BEGIN { $ENV{PAR_TMPDIR} = File::Temp::tempdir(TMPDIR => 1, CLEANUP => 1); }  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict, with severity 5 out of 5, and description 'Code before strictures are enabled'  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'  # this line violates Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars, with severity 4 out of 5, and description 'Magic variable "$ENV" should be assigned as "local"'

$|=1;  # this line violates Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars, with severity 4 out of 5, and description 'Magic variable "$|" should be assigned as "local"'
print "1..1\n";
use PAR;

package Bar;
use AutoLoader 'AUTOLOAD';
# Can't use strict and warnings because in case of the
# erroneous recursion, we'll require ourselves and get a
# "subroutine redefined" error which doesn't matter.
sub new {  # this line violates Perl::Critic::Policy::Subroutines::RequireArgUnpacking, with severity 4 out of 5, and description 'Always unpack @_ first'
    return bless {} => $_[0];
}

package main;  # this line violates Perl::Critic::Policy::Modules::ProhibitMultiplePackages, with severity 4 out of 5, and description 'Multiple "package" declarations', and explanation 'Limit to one per file'

$INC{"Bar.pm"} = $0; # <--  # this line violates Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars, with severity 4 out of 5, and description 'Magic variable "$INC" should be assigned as "local"'
{
    my $p = Bar->new();
} # <-- looping while looking for Bar::DESTROY

print "ok 1 - AutoLoader works\n";
