package Hello;

# This package (and file) is used by 40-par-hashref.t
use strict;
sub hello {  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'
    return();
}

1;
