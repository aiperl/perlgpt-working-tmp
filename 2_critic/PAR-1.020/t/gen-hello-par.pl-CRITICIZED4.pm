use strict;  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'
use warnings;
use Archive::Zip qw( :ERROR_CODES );

my $zip = Archive::Zip->new();
exit($zip->addTree("t/data", "") == AZ_OK  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'
     && $zip->writeToFileNamed("t/hello.par") == AZ_OK ? 0 : 1);
