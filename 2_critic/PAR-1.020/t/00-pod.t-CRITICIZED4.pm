use strict;  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'
use warnings;
use Test::More;
plan skip_all => "Set environment variable PERL_TEST_POD=1 to test POD"
  if not $ENV{PERL_TEST_POD};
eval "use Test::Pod 1.00";  # this line violates Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval, with severity 5 out of 5, and description 'Expression form of "eval"'
plan skip_all => "Test::Pod 1.00 required for testing POD"
  if $@;
all_pod_files_ok();  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'
