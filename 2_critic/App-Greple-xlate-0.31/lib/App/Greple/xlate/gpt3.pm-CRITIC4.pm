$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Code before strictures are enabled',
                   '_element_class' => 'PPI::Statement::Variable',
                   '_explanation' => [
                                       429
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    3,
                                    1,
                                    1,
                                    3,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict',
                   '_severity' => 5,
                   '_source' => 'our $VERSION = "0.31";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Code before warnings are enabled',
                   '_element_class' => 'PPI::Statement::Variable',
                   '_explanation' => [
                                       431
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    3,
                                    1,
                                    1,
                                    3,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings',
                   '_severity' => 4,
                   '_source' => 'our $VERSION = "0.31";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Augmented assignment operator \'//=\' used in declaration',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => 'Use simple assignment when initializing variables',
                   '_filename' => undef,
                   '_location' => [
                                    17,
                                    16,
                                    16,
                                    17,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::ProhibitAugmentedAssignmentInDeclaration',
                   '_severity' => 4,
                   '_source' => 'our $lang_from //= \'ORIGINAL\';'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Augmented assignment operator \'//=\' used in declaration',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => 'Use simple assignment when initializing variables',
                   '_filename' => undef,
                   '_location' => [
                                    18,
                                    16,
                                    16,
                                    18,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::ProhibitAugmentedAssignmentInDeclaration',
                   '_severity' => 4,
                   '_source' => 'our $lang_to   //= \'JA\';'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "initialize" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       197
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    31,
                                    1,
                                    1,
                                    31,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub initialize {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "gpty" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    36,
                                    1,
                                    1,
                                    36,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub gpty {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Use IO::Interactive::is_interactive() instead of -t',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => [
                                       218
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    52,
                                    2,
                                    2,
                                    52,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::ProhibitInteractiveTest',
                   '_severity' => 5,
                   '_source' => '	-t => $param->{temp},'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       178
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    60,
                                    1,
                                    1,
                                    60,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _progress {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_progress" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    60,
                                    1,
                                    1,
                                    60,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _progress {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    64,
                                    1,
                                    1,
                                    64,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub xlate_each {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "xlate_each" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    64,
                                    1,
                                    1,
                                    64,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub xlate_each {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "map"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       169
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    67,
                                    26,
                                    26,
                                    67,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockMap',
                   '_severity' => 4,
                   '_source' => '"From:\\n", map s/^/\\t< /mgr, @_'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "map"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[11]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    70,
                                    24,
                                    24,
                                    70,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockMap',
                   '_severity' => 4,
                   '_source' => '"To:\\n", map s/^/\\t> /mgr, @out'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[7]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    77,
                                    1,
                                    1,
                                    77,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub xlate {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "xlate" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    77,
                                    1,
                                    1,
                                    77,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub xlate {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "map"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[11]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    81,
                                    38,
                                    38,
                                    81,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::RequireBlockMap',
                   '_severity' => 4,
                   '_source' => 'my @len = grep { $_ > $max } map length, @from'
                 }, 'Perl::Critic::Violation' )
        ];
1;
