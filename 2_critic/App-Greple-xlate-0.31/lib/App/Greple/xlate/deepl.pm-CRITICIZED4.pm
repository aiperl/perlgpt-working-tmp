package App::Greple::xlate::deepl;

our $VERSION = "0.31";  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict, with severity 5 out of 5, and description 'Code before strictures are enabled'  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'

use v5.14;
use warnings;
use Encode;
use Data::Dumper;

use List::Util qw(sum);
use App::cdif::Command;

use App::Greple::xlate qw(opt);
use App::Greple::xlate::Lang qw(%LANGNAME);

our $lang_from //= 'ORIGINAL';  # this line violates Perl::Critic::Policy::Variables::ProhibitAugmentedAssignmentInDeclaration, with severity 4 out of 5, and description 'Augmented assignment operator '//=' used in declaration', and explanation 'Use simple assignment when initializing variables'
our $lang_to   //= 'JA';  # this line violates Perl::Critic::Policy::Variables::ProhibitAugmentedAssignmentInDeclaration, with severity 4 out of 5, and description 'Augmented assignment operator '//=' used in declaration', and explanation 'Use simple assignment when initializing variables'
our $auth_key;
our $method = 'deepl';

my %param = (
    deepl     => { max => 128 * 1024, sub => \&deepl },
    clipboard => { max => 5000,       sub => \&clipboard },
    );

sub deepl {  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "deepl" does not end with "return"'
    state $deepl = App::cdif::Command->new;
    state $command = [ 'deepl', 'text',
		       '--to' => $lang_to,
		       $auth_key ? ('--auth-key' => $auth_key) : () ];
    $deepl->command([@$command, +shift])->update->data;
}

sub clipboard {
    require Clipboard and import Clipboard unless state $called++;
    my $from = shift;
    my $length = length $from;
    Clipboard->copy($from);
    STDERR->printflush(
	"$length characters stored in the clipboard.\n",
	"Translate it to \"$lang_to\" and clip again.\n",
	"Then hit enter: ");
    if (open my $fh, "/dev/tty" or die) {  # this line violates Perl::Critic::Policy::InputOutput::ProhibitTwoArgOpen, with severity 5 out of 5, and description 'Two-argument "open" used'  # this line violates Perl::Critic::Policy::InputOutput::RequireBriefOpen, with severity 4 out of 5, and description 'Close filehandles as soon as possible after opening them'
	my $answer = <$fh>;
    }
    my $to = Clipboard->paste;
    $to = decode('utf8', $to) if not utf8::is_utf8($_);
    return $to;
}

sub _progress {  # this line violates Perl::Critic::Policy::Subroutines::RequireArgUnpacking, with severity 4 out of 5, and description 'Always unpack @_ first'  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "_progress" does not end with "return"'
    print STDERR @_ if opt('progress');
}

sub xlate_each {  # this line violates Perl::Critic::Policy::Subroutines::RequireArgUnpacking, with severity 4 out of 5, and description 'Always unpack @_ first'  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "xlate_each" does not end with "return"'
    my $call = $param{$method}->{sub} // die;
    my @count = map { int tr/\n/\n/ } @_;
    _progress("From:\n", map s/^/\t< /mgr, @_);  # this line violates Perl::Critic::Policy::BuiltinFunctions::RequireBlockMap, with severity 4 out of 5, and description 'Expression form of "map"'
    my $to = $call->(join '', @_);
    my @out = $to =~ /.*\n/g;
    _progress("To:\n", map s/^/\t> /mgr, @out);  # this line violates Perl::Critic::Policy::BuiltinFunctions::RequireBlockMap, with severity 4 out of 5, and description 'Expression form of "map"'
    if (@out < sum @count) {
	die "Unexpected response:\n\n$to\n";
    }
    map { join '', splice @out, 0, $_ } @count;
}

sub xlate {  # this line violates Perl::Critic::Policy::Subroutines::RequireArgUnpacking, with severity 4 out of 5, and description 'Always unpack @_ first'  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "xlate" does not end with "return"'
    my @from = map { /\n\z/ ? $_ : "$_\n" } @_;
    my @to;
    my $max = $App::Greple::xlate::max_length || $param{$method}->{max} // die;
    while (@from) {
	my @tmp;
	my $len = 0;
	while (@from) {
	    my $next = length $from[0];
	    last if $len + $next > $max;
	    $len += $next;
	    push @tmp, shift @from;
	}
	push @to, xlate_each @tmp;
    }
    @to;
}

1;

__DATA__

option default -Mxlate --xlate-engine=deepl
