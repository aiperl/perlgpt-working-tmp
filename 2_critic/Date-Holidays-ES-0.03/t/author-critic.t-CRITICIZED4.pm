#!perl

BEGIN {  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict, with severity 5 out of 5, and description 'Code before strictures are enabled'  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'
  unless ($ENV{AUTHOR_TESTING}) {
    print qq{1..0 # SKIP these tests are for testing by the author\n};
    exit
  }
}


use strict;
use warnings;

use Test::Perl::Critic (-profile => "t/perlcritic.rc") x!! -e "t/perlcritic.rc";
all_critic_ok();
