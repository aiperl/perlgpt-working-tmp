use strict;  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'
use ExtUtils::MakeMaker;

my $is_devel_host = defined $ENV{USER} && $ENV{USER} eq 'eserte' && ($^O =~ /bsd/i || $ENV{PERL_RELEASE_READY}) && -f "../../perl.release.mk";  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'
my $eumm_recent_enough = $ExtUtils::MakeMaker::VERSION >= 6.54;

if (!$eumm_recent_enough) {
    *MY::dist_core = sub {
	<<'EOF';
dist :
	$(NOECHO) $(ECHO) "Sorry, use a newer EUMM!"

EOF
    };
}

WriteMakefile(
    NAME              => 'CPAN::Plugin::Sysdeps',
    VERSION_FROM      => 'lib/CPAN/Plugin/Sysdeps.pm',
    PREREQ_PM         => {
	'List::Util' => 0,
	'Test::More' => 0,
    },
    EXE_FILES         => ['script/cpan-sysdeps'],
    LICENSE	      => 'perl',
    ABSTRACT          => 'CPAN.pm plugin for installing external dependencies',
    AUTHOR            => 'Slaven Rezic <srezic@cpan.org>',
    MIN_PERL_VERSION  => 5.006,

    ($eumm_recent_enough
     ? (META_ADD => { resources  => { repository => 'git://github.com/eserte/cpan-plugin-sysdeps.git' } }) : ()),

);

sub MY::postamble {  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "MY::postamble" does not end with "return"'
    my $postamble = '';

    if ($is_devel_host) {
	$postamble .= <<'EOF';

PERL_TEST_DISTRIBUTION_CHANGES=yes

.include "../../perl.release.mk"
.include "../../perl.git.mk"

EOF
    }

    $postamble;
}
