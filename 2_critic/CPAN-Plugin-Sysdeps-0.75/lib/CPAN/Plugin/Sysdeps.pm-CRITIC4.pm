$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Pragma "constant" used',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => [
                                       55
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    12,
                                    1,
                                    1,
                                    12,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitConstantPragma',
                   '_severity' => 4,
                   '_source' => 'use constant SUPPORTED_NUMERICAL_OPS => [\'<\',\'<=\',\'==\',\'>\',\'>=\'];'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Pragma "constant" used',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    13,
                                    1,
                                    1,
                                    13,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitConstantPragma',
                   '_severity' => 4,
                   '_source' => 'use constant SUPPORTED_NUMERICAL_OPS_RX => do {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "new" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       197
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    20,
                                    1,
                                    1,
                                    20,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub new {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Close filehandles as soon as possible after opening them',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       209
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    134,
                                    6,
                                    6,
                                    134,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::RequireBriefOpen',
                   '_severity' => 4,
                   '_source' => 'open my $fh, \'<\', $mapping'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "eval"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       161
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    138,
                                    21,
                                    21,
                                    138,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval',
                   '_severity' => 5,
                   '_source' => 'push @mapping, eval $buf;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "eval"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    141,
                                    6,
                                    6,
                                    141,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval',
                   '_severity' => 5,
                   '_source' => 'eval "require $mapping";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "post_get" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    167,
                                    1,
                                    1,
                                    167,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub post_get {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_detect_linux_distribution" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    192,
                                    1,
                                    1,
                                    192,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _detect_linux_distribution {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_detect_linux_distribution_os_release" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    202,
                                    1,
                                    1,
                                    202,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _detect_linux_distribution_os_release {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Close filehandles as soon as possible after opening them',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    205,
                                    6,
                                    6,
                                    205,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::RequireBriefOpen',
                   '_severity' => 4,
                   '_source' => 'open my $fh, \'<\', $candidate_file'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_detect_linux_distribution_lsb_release" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    225,
                                    1,
                                    1,
                                    225,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _detect_linux_distribution_lsb_release {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Close filehandles as soon as possible after opening them',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    228,
                                    5,
                                    5,
                                    228,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::RequireBriefOpen',
                   '_severity' => 4,
                   '_source' => 'open my $fh, \'-|\', @cmd'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Close filehandles as soon as possible after opening them',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    248,
                                    9,
                                    9,
                                    248,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::RequireBriefOpen',
                   '_severity' => 4,
                   '_source' => 'open my $fh, \'<\', \'/etc/redhat-release\''
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Close filehandles as soon as possible after opening them',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    254,
                                    9,
                                    9,
                                    254,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::RequireBriefOpen',
                   '_severity' => 4,
                   '_source' => 'open my $fh, \'<\', \'/etc/issue\''
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_is_linux_debian_like" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    292,
                                    1,
                                    1,
                                    292,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _is_linux_debian_like {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_is_linux_fedora_like" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    297,
                                    1,
                                    1,
                                    297,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _is_linux_fedora_like {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_is_apt_installer" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    302,
                                    1,
                                    1,
                                    302,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _is_apt_installer { shift->{installer} =~m{^(apt-get|aptitude)$} }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_win32_run_elevated" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    305,
                                    1,
                                    1,
                                    305,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _win32_run_elevated {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Don\'t modify $_ in list functions',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       114
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    308,
                                    26,
                                    26,
                                    308,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ControlStructures::ProhibitMutatingListFunctions',
                   '_severity' => 5,
                   '_source' => 'my $args = join " ", map { if(/[ "]/) { s!"!\\\\"!g; qq{"$_"} } else { $_ }} @args;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       178
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    316,
                                    1,
                                    1,
                                    316,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub _debug {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_debug" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    316,
                                    1,
                                    1,
                                    316,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _debug {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_map_cpandist" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    331,
                                    1,
                                    1,
                                    331,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _map_cpandist {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Warnings disabled',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => [
                                       431
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    336,
                                    2,
                                    2,
                                    336,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::ProhibitNoWarnings',
                   '_severity' => 4,
                   '_source' => 'no warnings \'once\';'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Warnings disabled',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => $VAR1->[22]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    350,
                                    2,
                                    2,
                                    350,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::ProhibitNoWarnings',
                   '_severity' => 4,
                   '_source' => 'no warnings \'uninitialized\';'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "eval"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    362,
                                    13,
                                    13,
                                    362,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval',
                   '_severity' => 5,
                   '_source' => 'my $res = eval $code;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_find_missing_deb_packages" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    452,
                                    1,
                                    1,
                                    452,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _find_missing_deb_packages {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_find_missing_rpm_packages" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    487,
                                    1,
                                    1,
                                    487,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _find_missing_rpm_packages {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Close filehandles as soon as possible after opening them',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    498,
                                    2,
                                    2,
                                    498,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::RequireBriefOpen',
                   '_severity' => 4,
                   '_source' => 'open my $fh, \'-|\', @cmd'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_find_missing_freebsd_pkg_packages" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    514,
                                    1,
                                    1,
                                    514,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _find_missing_freebsd_pkg_packages {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_find_missing_openbsd_pkg_packages" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    530,
                                    1,
                                    1,
                                    530,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _find_missing_openbsd_pkg_packages {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_find_missing_homebrew_packages" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    562,
                                    1,
                                    1,
                                    562,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _find_missing_homebrew_packages {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_find_missing_chocolatey_packages" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    586,
                                    1,
                                    1,
                                    586,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _find_missing_chocolatey_packages {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_filter_uninstalled_packages" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    601,
                                    1,
                                    1,
                                    601,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _filter_uninstalled_packages {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_install_packages_commands" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    639,
                                    1,
                                    1,
                                    639,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _install_packages_commands {'
                 }, 'Perl::Critic::Violation' )
        ];
1;
