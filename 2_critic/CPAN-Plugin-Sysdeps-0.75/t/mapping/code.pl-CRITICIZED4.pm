return  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict, with severity 5 out of 5, and description 'Code before strictures are enabled'  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'
    ([cpandist => qr{^(Cairo-\d|Prima-Cairo-\d)}, # XXX base id or full dist name with author?
      sub {
	  my($self, $dist) = @_;
	  if ($dist->base_id =~ m{^(Cairo-\d|Prima-Cairo-\d)}) {
	      if ($^O eq 'freebsd') {
		  return { package => 'cairo' };
	      } elsif ($^O eq 'linux' && $self->{linuxdistro} =~ m{^(debian|ubuntu|linuxmint)$}) {
		  return { package => 'libcairo2-dev' };
	      }
	  }
      }],
    );
