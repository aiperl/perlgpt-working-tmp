return (  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict, with severity 5 out of 5, and description 'Code before strictures are enabled'  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'
 [
  cpanmod => ['Linux::Only'],
  [os => 'linux',
   [package => 'libfoo-dev']]],

 [
  cpanmod => ['FreeBSD::Only'],
  [os => 'freebsd',
   [package => 'libfoo']]],

 [
  cpanmod => ['FreeBSD::Version'],
  [os => 'freebsd',
   [osvers => qr{^[123456789]\.},
    [package => 'gcc']],
   [osvers => qr{^10\.},
    [package => 'clang']]]],

 [
  cpanmod => ['FreeBSD::Version2'],
  [os => 'freebsd',
   [osvers => { '<', 10, '>=', 1 },
    [package => 'gcc']],
   [osvers => { '>=', 10 },
    [package => 'clang']]]],

 [
  cpanmod => 'Multi::Packages',
  [package => ['package-one', 'package-two']]],

);
