use strict;  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'
use warnings;
use FindBin;
use lib "$FindBin::RealBin/lib";
use TestUtil;

use Test::More;
use CPAN::Plugin::Sysdeps ();

plan 'no_plan';

sub maybe_shift_sudo ($) {  # this line violates Perl::Critic::Policy::Subroutines::ProhibitSubroutinePrototypes, with severity 5 out of 5, and description 'Subroutine prototypes used'  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "maybe_shift_sudo" does not end with "return"'
    my $cmds_ref = shift;
    if ($< != 0) {
	local $Test::Builder::Level = $Test::Builder::Level + 1;
	is $cmds_ref->[0], 'sudo';
	shift @{ $cmds_ref };
    }
}

if ($^O ne 'MSWin32') {  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'
    {
	for my $debinst (qw(apt-get aptitude)) {
	    my $p = CPAN::Plugin::Sysdeps->new($debinst);
	    my @cmds = $p->_install_packages_commands(qw(libfoo libbar));
	    is scalar(@cmds), 2;
	    is_deeply [ @{$cmds[0]}[0,1] ], [qw(sh -c)];
	    like $cmds[0][-1], qr{^echo.*Install package.*libfoo libbar.*read.*yn};
	    maybe_shift_sudo $cmds[-1];
	    is_deeply $cmds[-1], [$debinst, qw(install libfoo libbar)];
	}
    }

    {
	my $p = CPAN::Plugin::Sysdeps->new('batch', 'apt-get');
	my @cmds = $p->_install_packages_commands(qw(libfoo libbar));
	is scalar(@cmds), 1;
	maybe_shift_sudo $cmds[-1];
	is_deeply $cmds[-1], [qw(apt-get -y install libfoo libbar)];
    }

    {
	my $p = CPAN::Plugin::Sysdeps->new('interactive', 'apt-get');
	my @cmds = $p->_install_packages_commands(qw(libfoo libbar));
	is scalar(@cmds), 2;
	maybe_shift_sudo $cmds[-1];
	is_deeply $cmds[-1], [qw(apt-get install libfoo libbar)];
    }

    {
	my $p = CPAN::Plugin::Sysdeps->new('pkg');
	my @cmds = $p->_install_packages_commands(qw(libfoo libbar));
	is scalar(@cmds), 1;
	maybe_shift_sudo $cmds[0];
	is $cmds[-1][0], 'pkg';
	is $cmds[-1][-3], 'install';
	is $cmds[-1][-2], 'libfoo';
	is $cmds[-1][-1], 'libbar';
    }

    {
	my @warnings;
	local $SIG{__WARN__} = sub { push @warnings, @_ };
	my $p = CPAN::Plugin::Sysdeps->new('pkg_add');
	my @cmds = $p->_install_packages_commands(qw(libfoo libbar));
	is scalar(@cmds), 1;
	maybe_shift_sudo $cmds[-1];
	is_deeply $cmds[-1], [qw(pkg_add libfoo libbar)];
	like $warnings[0], qr{batch=0 NYI for pkg_add}, 'expected warning';
	is @warnings, 1, 'only one warning';
    }

    {
	my $p = CPAN::Plugin::Sysdeps->new('homebrew');
	my @cmds = $p->_install_packages_commands(qw(libfoo libbar));
	is scalar(@cmds), 2;
	is_deeply $cmds[-1], [qw(brew install libfoo libbar)];
    }

    {
	my $p = CPAN::Plugin::Sysdeps->new('yum');
	my @cmds = $p->_install_packages_commands(qw(libfoo libbar));
	is scalar(@cmds), 1;
	maybe_shift_sudo $cmds[-1];
	is_deeply $cmds[-1], [qw(yum install libfoo libbar)];
    }

    {
	my $p = CPAN::Plugin::Sysdeps->new('dnf');
	my @cmds = $p->_install_packages_commands(qw(libfoo libbar));
	is scalar(@cmds), 1;
	maybe_shift_sudo $cmds[-1];
	is_deeply $cmds[-1], [qw(dnf install libfoo libbar)];
    }
} else {
    {
	my $p = CPAN::Plugin::Sysdeps->new('chocolatey');
	my @cmds = $p->_install_packages_commands(qw(libfoo libbar));
	is scalar(@cmds), 1;
	like $cmds[-1][0], qr{^powershell .*Start-Process 'chocolatey'.*'install libfoo libbar'};
    }
}
