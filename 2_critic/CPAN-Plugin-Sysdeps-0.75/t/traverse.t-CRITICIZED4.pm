use strict;  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'
use warnings;
use FindBin;
use lib "$FindBin::RealBin/lib";
use TestUtil;

use Test::More 'no_plan';
use CPAN::Plugin::Sysdeps ();
require_CPAN_Distribution;

my @warnings;
$SIG{__WARN__} = sub { push @warnings, @_ };  # this line violates Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars, with severity 4 out of 5, and description 'Magic variable "$SIG" should be assigned as "local"'

my $cpandist = CPAN::Distribution->new(
				       ID => 'X/XX/XXX/DummyDoesNotExist-1.0.tar.gz',
				       CONTAINSMODS => { DummyDoesNotExist => undef },
				      );

{
    my $p = CPAN::Plugin::Sysdeps->new('apt-get', 'batch', 'dryrun');
    local $CPAN::Plugin::Sysdeps::TRAVERSE_ONLY = 1;
    $p->post_get($cpandist);
    pass 'traverse only did not fail';
}

{
    my $p = CPAN::Plugin::Sysdeps->new('apt-get', 'batch', 'dryrun', "mapping=$FindBin::RealBin/mapping/fail_likelinuxdistro.pl");
    local $CPAN::Plugin::Sysdeps::TRAVERSE_ONLY = 1;
    eval { $p->post_get($cpandist) };
    like $@, qr{'like' matches only for };
}

is_deeply \@warnings, [], 'no warnings';  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'
