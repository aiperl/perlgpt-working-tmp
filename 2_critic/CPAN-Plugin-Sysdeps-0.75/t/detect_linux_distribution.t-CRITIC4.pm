$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Code not contained in explicit package',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => 'Violates encapsulation',
                   '_filename' => undef,
                   '_location' => [
                                    1,
                                    1,
                                    1,
                                    1,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::RequireExplicitPackage',
                   '_severity' => 4,
                   '_source' => 'use strict;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine prototypes used',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       194
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    15,
                                    1,
                                    1,
                                    15,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::ProhibitSubroutinePrototypes',
                   '_severity' => 5,
                   '_source' => 'sub os_release_test ($$$$$$);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Warnings disabled',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => [
                                       431
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    221,
                                    6,
                                    6,
                                    221,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::ProhibitNoWarnings',
                   '_severity' => 4,
                   '_source' => 'no warnings \'uninitialized\';'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine prototypes used',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    235,
                                    1,
                                    1,
                                    235,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::ProhibitSubroutinePrototypes',
                   '_severity' => 5,
                   '_source' => 'sub os_release_test ($$$$$$) {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "os_release_test" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       197
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    235,
                                    1,
                                    1,
                                    235,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub os_release_test ($$$$$$) {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Module does not end with "1;"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => 'Must end with a recognizable true value',
                   '_filename' => undef,
                   '_location' => [
                                    263,
                                    1,
                                    1,
                                    263,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::RequireEndWithOne',
                   '_severity' => 4,
                   '_source' => 'sub traverse_warnings_test {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "traverse_warnings_test" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    263,
                                    1,
                                    1,
                                    263,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub traverse_warnings_test {'
                 }, 'Perl::Critic::Violation' )
        ];
1;
