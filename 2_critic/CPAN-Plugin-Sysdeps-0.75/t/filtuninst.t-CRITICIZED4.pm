use strict;  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'
use warnings;
use FindBin;
use lib "$FindBin::RealBin/lib";
use TestUtil;

use Test::More;
use CPAN::Plugin::Sysdeps ();

my $p = eval { CPAN::Plugin::Sysdeps->new('dryrun') };
plan skip_all => "Construction failed: $@", 1 if !$p;
skip_on_darwin_without_homebrew;
skip_on_os('darwin', 'cannot use dummy packages for testing'); # in some installations homebrew fails with a stacktrace if a package is unknown
skip_on_os('openbsd', 'cannot use dummy packages for testing');
plan 'no_plan';

isa_ok $p, 'CPAN::Plugin::Sysdeps';

if ($p->{installer} =~ m{^(apt-get|pkg|pkg_add|homebrew|chocolatey|yum|dnf)$}) {  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'
    {
	my @packages = $p->_filter_uninstalled_packages(qw(libdoesnotexist1 libdoesnotexist2));
	is_deeply \@packages, [qw(libdoesnotexist1 libdoesnotexist2)];
    }
    {
	my @packages = $p->_filter_uninstalled_packages('libdoesnotexist1 | libdoesnotexist2');
	is_deeply \@packages, [qw(libdoesnotexist1)];
    }
}
