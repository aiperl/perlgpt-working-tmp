package
    TestUtil;

use strict;
use warnings;

require Exporter;
use vars qw(@ISA @EXPORT);  # this line violates Perl::Critic::Policy::Modules::ProhibitAutomaticExportation, with severity 4 out of 5, and description 'Symbols are exported by default', and explanation 'Use '@EXPORT_OK' or '%EXPORT_TAGS' instead'
@ISA = qw(Exporter);
@EXPORT = qw(require_CPAN_Distribution skip_on_darwin_without_homebrew skip_on_os);

sub require_CPAN_Distribution () {  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "require_CPAN_Distribution" does not end with "return"'
    if (!eval { require CPAN::Distribution; 1 }) {
	require CPAN;
    }
}

sub skip_on_darwin_without_homebrew () {  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "skip_on_darwin_without_homebrew" does not end with "return"'
    if ($^O eq 'darwin' && !-x '/usr/local/bin/brew') {
	Test::More::plan(skip_all => 'No homebrew installed here');
    }
}

sub skip_on_os ($;$) {  # this line violates Perl::Critic::Policy::Subroutines::ProhibitSubroutinePrototypes, with severity 5 out of 5, and description 'Subroutine prototypes used'  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "skip_on_os" does not end with "return"'
    my($os, $message) = @_;
    if ($^O eq $os) {
	$message ||= "Does not work on $os";
	Test::More::plan(skip_all => $message);
    }
}

1;
