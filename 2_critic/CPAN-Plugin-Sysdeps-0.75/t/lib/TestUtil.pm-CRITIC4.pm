$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Symbols are exported by default',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => 'Use \'@EXPORT_OK\' or \'%EXPORT_TAGS\' instead',
                   '_filename' => undef,
                   '_location' => [
                                    8,
                                    1,
                                    1,
                                    8,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::ProhibitAutomaticExportation',
                   '_severity' => 4,
                   '_source' => 'use vars qw(@ISA @EXPORT);'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "require_CPAN_Distribution" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       197
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    12,
                                    1,
                                    1,
                                    12,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub require_CPAN_Distribution () {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "skip_on_darwin_without_homebrew" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    18,
                                    1,
                                    1,
                                    18,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub skip_on_darwin_without_homebrew () {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine prototypes used',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       194
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    24,
                                    1,
                                    1,
                                    24,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::ProhibitSubroutinePrototypes',
                   '_severity' => 5,
                   '_source' => 'sub skip_on_os ($;$) {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "skip_on_os" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[1]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    24,
                                    1,
                                    1,
                                    24,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub skip_on_os ($;$) {'
                 }, 'Perl::Critic::Violation' )
        ];
1;
