$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Pragma "constant" used',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => [
                                       55
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    18,
                                    1,
                                    1,
                                    18,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitConstantPragma',
                   '_severity' => 4,
                   '_source' => 'use constant PATH_SEP	=> qr/ $Config{path_sep} /smx;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Pragma "constant" used',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    19,
                                    1,
                                    1,
                                    19,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitConstantPragma',
                   '_severity' => 4,
                   '_source' => 'use constant MANPATH_APPEND	=> qr/ (?<= $Config{path_sep} \\z ) /smx;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Pragma "constant" used',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    20,
                                    1,
                                    1,
                                    20,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitConstantPragma',
                   '_severity' => 4,
                   '_source' => 'use constant MANPATH_EMBED	=>'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Pragma "constant" used',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    22,
                                    1,
                                    1,
                                    22,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitConstantPragma',
                   '_severity' => 4,
                   '_source' => 'use constant MANPATH_PREPEND	=> qr/ \\A (?= $Config{path_sep} ) /smx;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Pragma "constant" used',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    23,
                                    1,
                                    1,
                                    23,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitConstantPragma',
                   '_severity' => 4,
                   '_source' => 'use constant MAN_CONF	=> {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Use IO::Interactive::is_interactive() instead of -t',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => [
                                       218
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    32,
                                    12,
                                    12,
                                    32,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::ProhibitInteractiveTest',
                   '_severity' => 5,
                   '_source' => '1	=> ! -t STDOUT,'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Warnings disabled',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => [
                                       431
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    120,
                                    2,
                                    2,
                                    120,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::ProhibitNoWarnings',
                   '_severity' => 4,
                   '_source' => 'no warnings qw{ once };'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Close filehandles as soon as possible after opening them',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       209
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    160,
                                    2,
                                    2,
                                    160,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::RequireBriefOpen',
                   '_severity' => 4,
                   '_source' => 'open my $fh, \'<\', $man_conf'
                 }, 'Perl::Critic::Violation' )
        ];
1;
