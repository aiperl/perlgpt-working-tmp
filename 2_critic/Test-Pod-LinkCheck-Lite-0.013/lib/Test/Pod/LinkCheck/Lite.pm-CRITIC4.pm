$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Pragma "constant" used',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => [
                                       55
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    36,
                                    1,
                                    1,
                                    36,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitConstantPragma',
                   '_severity' => 4,
                   '_source' => 'use constant ON_DARWIN		=> \'darwin\' eq $^O;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Pragma "constant" used',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    37,
                                    1,
                                    1,
                                    37,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitConstantPragma',
                   '_severity' => 4,
                   '_source' => 'use constant ON_VMS		=> \'VMS\' eq $^O;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Pragma "constant" used',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    45,
                                    1,
                                    1,
                                    45,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitConstantPragma',
                   '_severity' => 4,
                   '_source' => 'use constant ARRAY_REF	=> ref [];'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Pragma "constant" used',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    46,
                                    1,
                                    1,
                                    46,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitConstantPragma',
                   '_severity' => 4,
                   '_source' => 'use constant CODE_REF	=> ref sub {};'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Pragma "constant" used',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    47,
                                    1,
                                    1,
                                    47,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitConstantPragma',
                   '_severity' => 4,
                   '_source' => 'use constant HASH_REF	=> ref {};'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Pragma "constant" used',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    48,
                                    1,
                                    1,
                                    48,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitConstantPragma',
                   '_severity' => 4,
                   '_source' => 'use constant NON_REF	=> ref 0;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Pragma "constant" used',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    49,
                                    1,
                                    1,
                                    49,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitConstantPragma',
                   '_severity' => 4,
                   '_source' => 'use constant REGEXP_REF	=> ref qr<x>smx;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Pragma "constant" used',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    50,
                                    1,
                                    1,
                                    50,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitConstantPragma',
                   '_severity' => 4,
                   '_source' => 'use constant SCALAR_REF	=> ref \\0;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Pragma "constant" used',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    56,
                                    1,
                                    1,
                                    56,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitConstantPragma',
                   '_severity' => 4,
                   '_source' => 'use constant NEED_MAN_FIX	=> Pod::Simple->VERSION lt \'3.24\';'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Pragma "constant" used',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    58,
                                    1,
                                    1,
                                    58,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitConstantPragma',
                   '_severity' => 4,
                   '_source' => 'use constant ALLOW_REDIRECT_TO_INDEX => sub {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Pragma "constant" used',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    71,
                                    1,
                                    1,
                                    71,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitConstantPragma',
                   '_severity' => 4,
                   '_source' => 'use constant MAYBE_IGNORE_GITHUB	=> sub {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Pragma "constant" used',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    87,
                                    1,
                                    1,
                                    87,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitConstantPragma',
                   '_severity' => 4,
                   '_source' => 'use constant USER_AGENT_CLASS	=> \'HTTP::Tiny\';'
                 }, 'Perl::Critic::Violation' )
        ];
1;
