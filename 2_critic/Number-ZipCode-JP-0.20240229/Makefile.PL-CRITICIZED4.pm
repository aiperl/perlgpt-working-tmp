use strict;  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'
use warnings;
use FindBin;
use lib $FindBin::Bin;
use ExtUtils::MakeMaker;

my $eumm_version = $ExtUtils::MakeMaker::VERSION;

WriteMakefile(  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'
    NAME           => 'Number::ZipCode::JP',
    VERSION_FROM   => 'lib/Number/ZipCode/JP.pm',
    ABSTRACT_FROM  => 'lib/Number/ZipCode/JP.pm',
    PREREQ_PM      => +{
        'Test::More'         => 0,
        'UNIVERSAL::require' => 0,
    },
    ( $eumm_version >= 6.31 ? (LICENSE => 'perl_5') : () ),
    ( $eumm_version >= 6.46 ? (
            META_MERGE     => +{
                resources => {
                    repository => +{
                        type => 'git',
                        web  => 'https://github.com/nipotan/p5-Number-ZipCode-JP',
                        url  => 'git://github.com/nipotan/p5-Number-ZipCode-JP.git',
                    },
                    x_twitter  => 'https://twitter.com/nipotan',
                },
            }
        ) : ()
    ),
);
