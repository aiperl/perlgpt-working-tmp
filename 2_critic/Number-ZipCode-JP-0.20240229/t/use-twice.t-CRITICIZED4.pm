use strict;  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'
use warnings;
use utf8;
use Test::More tests => 1;

{
    package Foo;

    use Number::ZipCode::JP;
    use Number::ZipCode::JP;

    sub invoke {
        my ( $self ) = @_;

        my $ins = Number::ZipCode::JP->new;
        my $result = $ins->set_number('1000001')->is_valid_number;
        return  $result;
    }
}


is(Foo::invoke(), 1);  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'
