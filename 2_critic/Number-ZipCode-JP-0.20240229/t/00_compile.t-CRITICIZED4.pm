use strict;  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'
use Test::More tests => 4;

BEGIN {  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'
    require_ok('Number::ZipCode::JP');
    ok(scalar(keys %Number::ZipCode::JP::ZIP_TABLE) == 0, 'not imported');
    use_ok 'Number::ZipCode::JP';
    ok(scalar(keys %Number::ZipCode::JP::ZIP_TABLE), 'imported');
}
