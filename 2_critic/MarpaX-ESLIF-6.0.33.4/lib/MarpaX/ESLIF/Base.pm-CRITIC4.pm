$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Code not contained in explicit package',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => 'Violates encapsulation',
                   '_filename' => undef,
                   '_location' => [
                                    1,
                                    1,
                                    1,
                                    1,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::RequireExplicitPackage',
                   '_severity' => 4,
                   '_source' => 'use strict;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       178
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    49,
                                    1,
                                    1,
                                    49,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub new {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => [
                                       68,
                                       71
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    92,
                                    9,
                                    9,
                                    92,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$MULTITONS{$engine} = $self, $MULTITONS_FOR_GLOBAL_DESTRUCTION{$engine} = { engine => $engine, class => $class, dispose => $dispose }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_destroy" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       197
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    99,
                                    1,
                                    1,
                                    99,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _destroy {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    134,
                                    9,
                                    9,
                                    134,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => 'delete $MULTITONS{$engine}, delete $MULTITONS_FOR_GLOBAL_DESTRUCTION{$engine}'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_global_destroy" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    138,
                                    1,
                                    1,
                                    138,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _global_destroy {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "_clone" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    165,
                                    1,
                                    1,
                                    165,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub _clone {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Comma used to separate statements',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    193,
                                    9,
                                    9,
                                    193,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::ValuesAndExpressions::ProhibitCommaSeparatedStatements',
                   '_severity' => 4,
                   '_source' => '$MULTITONS{$engine} = $self, $MULTITONS_FOR_GLOBAL_DESTRUCTION{$engine} = { engine => $engine, class => $self->{class}, dispose => $self->{dispose} }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "CLONE" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[3]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    204,
                                    1,
                                    1,
                                    204,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub CLONE {'
                 }, 'Perl::Critic::Violation' )
        ];
1;
