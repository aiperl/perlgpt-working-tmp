package MyRecognizerInterface;
use strict;
use diagnostics;
use Log::Any qw/$log/;

sub new                    { my ($pkg, $string) = @_; bless { string => $string }, $pkg }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "new" does not end with "return"'  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'
sub read                   { 1 }  # this line violates Perl::Critic::Policy::Subroutines::ProhibitBuiltinHomonyms, with severity 4 out of 5, and description 'Subroutine name is a homonym for builtin function read'  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "read" does not end with "return"'
sub isEof                  { 1 }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "isEof" does not end with "return"'
sub isCharacterStream      { 1 }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "isCharacterStream" does not end with "return"'
sub encoding               { }
sub data                   { $_[0]->{string} }  # this line violates Perl::Critic::Policy::Subroutines::RequireArgUnpacking, with severity 4 out of 5, and description 'Always unpack @_ first'  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "data" does not end with "return"'
sub isWithDisableThreshold { 0 }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "isWithDisableThreshold" does not end with "return"'
sub isWithExhaustion       { 0 }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "isWithExhaustion" does not end with "return"'
sub isWithNewline          { 1 }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "isWithNewline" does not end with "return"'
sub isWithTrack            { 1 }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "isWithTrack" does not end with "return"'

package MyValueInterface;  # this line violates Perl::Critic::Policy::Modules::ProhibitMultiplePackages, with severity 4 out of 5, and description 'Multiple "package" declarations', and explanation 'Limit to one per file'
use strict;
use diagnostics;

sub new                { my ($pkg) = @_; bless { result => undef }, $pkg }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "new" does not end with "return"'
sub isWithHighRankOnly { 1 }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "isWithHighRankOnly" does not end with "return"'
sub isWithOrderByRank  { 1 }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "isWithOrderByRank" does not end with "return"'
sub isWithAmbiguous    { 0 }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "isWithAmbiguous" does not end with "return"'
sub isWithNull         { 0 }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "isWithNull" does not end with "return"'
sub maxParses          { 0 }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "maxParses" does not end with "return"'
sub getResult          { $_[0]->{result} }  # this line violates Perl::Critic::Policy::Subroutines::RequireArgUnpacking, with severity 4 out of 5, and description 'Always unpack @_ first'  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "getResult" does not end with "return"'
sub setResult          { $_[0]->{result} = $_[1] }  # this line violates Perl::Critic::Policy::Subroutines::RequireArgUnpacking, with severity 4 out of 5, and description 'Always unpack @_ first'  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "setResult" does not end with "return"'

package main;  # this line violates Perl::Critic::Policy::Modules::ProhibitMultiplePackages, with severity 4 out of 5, and description 'Multiple "package" declarations', and explanation 'Limit to one per file'
use strict;
use warnings FATAL => 'all';
use Test::More tests => 16;
use Log::Any qw/$log/;
use Log::Any::Adapter 'Stdout';

BEGIN { require_ok('MarpaX::ESLIF') };

my $eslif = MarpaX::ESLIF->new($log);
isa_ok($eslif, 'MarpaX::ESLIF');

my $dsl = q{
X ::= 'X'
};

my $target       = "This is a string\nsubject";
my $input        = $target . " followed by anything";

my $stringSymbol = MarpaX::ESLIF::Symbol->new($eslif, type => 'string', pattern => "'$target'");
isa_ok($stringSymbol, 'MarpaX::ESLIF::Symbol');
my $regexSymbol  = MarpaX::ESLIF::Symbol->new($eslif, type => 'regex', pattern => "a.*\nsubject", modifiers => 'A');
isa_ok($regexSymbol, 'MarpaX::ESLIF::Symbol');
my $metaSymbol  = MarpaX::ESLIF::Symbol->new($eslif, type => 'meta', grammar => MarpaX::ESLIF::Grammar->new($eslif, "<anything up to newline> ::= <ANYTHING UP TO NEWLINE>\n<ANYTHING UP TO NEWLINE> ~ /[^\\n]*/"), symbol => 'ANYTHING UP TO NEWLINE');
isa_ok($regexSymbol, 'MarpaX::ESLIF::Symbol');
my $substitutionSymbol  = MarpaX::ESLIF::Symbol->new($eslif, type => 'regex', pattern => "a(.*)\nsubject", modifiers => 'A', substitutionPattern => '"$1"');
isa_ok($substitutionSymbol, 'MarpaX::ESLIF::Symbol');

my $match;

$match = $stringSymbol->try($input) // '';
ok($match eq $target, "String try");
$match = $regexSymbol->try($input) // '';
ok($match eq "a string\nsubject", "Regex try");
$match = $metaSymbol->try($input) // '';
ok($match eq "This is a string", "Meta try");
$match = $substitutionSymbol->try($input) // '';
ok($match eq " string", "Substitution try");

my $recognizerInterface = MyRecognizerInterface->new($input);
my $eslifGrammar = MarpaX::ESLIF::Grammar->new($eslif, $dsl);
isa_ok($eslifGrammar, 'MarpaX::ESLIF::Grammar');
my $eslifRecognizer = MarpaX::ESLIF::Recognizer->new($eslifGrammar, $recognizerInterface);
isa_ok($eslifRecognizer, 'MarpaX::ESLIF::Recognizer');

$match = $eslifRecognizer->symbolTry($stringSymbol) // '';
ok($match eq $target, "Recognizer string try");
$match = $eslifRecognizer->symbolTry($regexSymbol) // '';
ok($match eq "a string\nsubject", "Recognizer regex try");
$match = $eslifRecognizer->symbolTry($metaSymbol) // '';
ok($match eq "This is a string", "Recognizer meta try");
$match = $eslifRecognizer->symbolTry($substitutionSymbol) // '';
ok($match eq " string", "Recognizer substitution try");

exit 0;  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'
