$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Code before warnings are enabled',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       431
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    5,
                                    1,
                                    1,
                                    5,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings',
                   '_severity' => 4,
                   '_source' => 'sub new {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Close filehandles as soon as possible after opening them',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       209
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    7,
                                    5,
                                    5,
                                    7,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::RequireBriefOpen',
                   '_severity' => 4,
                   '_source' => 'open my $fh, "<", \\$string;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine name is a homonym for builtin function read',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       177
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    12,
                                    1,
                                    1,
                                    12,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::ProhibitBuiltinHomonyms',
                   '_severity' => 4,
                   '_source' => 'sub read {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Multiple "package" declarations',
                   '_element_class' => 'PPI::Statement::Package',
                   '_explanation' => 'Limit to one per file',
                   '_filename' => undef,
                   '_location' => [
                                    91,
                                    1,
                                    1,
                                    91,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::ProhibitMultiplePackages',
                   '_severity' => 4,
                   '_source' => 'package MyValue;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "DESTROY" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       197
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    108,
                                    1,
                                    1,
                                    108,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub DESTROY {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "trace_local_variables" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    114,
                                    1,
                                    1,
                                    114,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub trace_local_variables {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Warnings disabled',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => [
                                       431
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    117,
                                    5,
                                    5,
                                    117,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::ProhibitNoWarnings',
                   '_severity' => 4,
                   '_source' => 'no warnings \'once\';'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "trace_rule_property" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    125,
                                    1,
                                    1,
                                    125,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub trace_rule_property {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Multiple "package" declarations',
                   '_element_class' => 'PPI::Statement::Package',
                   '_explanation' => 'Limit to one per file',
                   '_filename' => undef,
                   '_location' => [
                                    275,
                                    1,
                                    1,
                                    275,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::ProhibitMultiplePackages',
                   '_severity' => 4,
                   '_source' => 'package main;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "showRecognizerInput" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1650,
                                    1,
                                    1,
                                    1650,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub showRecognizerInput {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "showEvents" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1657,
                                    1,
                                    1,
                                    1657,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub showEvents {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "showNameExpected" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1663,
                                    1,
                                    1,
                                    1663,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub showNameExpected {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "changeEventState" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1686,
                                    1,
                                    1,
                                    1686,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub changeEventState {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "showLastCompletion" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1692,
                                    1,
                                    1,
                                    1692,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub showLastCompletion {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "showLocation" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1711,
                                    1,
                                    1,
                                    1711,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub showLocation {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "showInputLength" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1727,
                                    1,
                                    1,
                                    1727,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub showInputLength {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "showInput" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1738,
                                    1,
                                    1,
                                    1738,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub showInput {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "showError" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1749,
                                    1,
                                    1,
                                    1749,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub showError {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "doDiscardTry" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1780,
                                    1,
                                    1,
                                    1780,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub doDiscardTry {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "doNameTry" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1799,
                                    1,
                                    1,
                                    1799,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub doNameTry {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "doCmpProperties" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[4]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1815,
                                    1,
                                    1,
                                    1815,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub doCmpProperties {'
                 }, 'Perl::Critic::Violation' )
        ];
1;
