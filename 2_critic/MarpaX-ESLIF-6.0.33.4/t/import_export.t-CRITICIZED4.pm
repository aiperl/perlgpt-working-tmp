package MyRecognizerInterface;
use strict;
use diagnostics;

sub new                    { my ($pkg, $characterStream) = @_; bless { characterStream => $characterStream }, $pkg }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "new" does not end with "return"'  # this line violates Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings, with severity 4 out of 5, and description 'Code before warnings are enabled'
sub read                   { 1 }  # this line violates Perl::Critic::Policy::Subroutines::ProhibitBuiltinHomonyms, with severity 4 out of 5, and description 'Subroutine name is a homonym for builtin function read'  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "read" does not end with "return"'
sub isEof                  { 1 }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "isEof" does not end with "return"'
sub isCharacterStream      { $_[0]->{characterStream} // 1 }  # this line violates Perl::Critic::Policy::Subroutines::RequireArgUnpacking, with severity 4 out of 5, and description 'Always unpack @_ first'  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "isCharacterStream" does not end with "return"'
sub encoding               { }
sub data                   { " " } # One byte  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "data" does not end with "return"'
sub isWithDisableThreshold { 0 }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "isWithDisableThreshold" does not end with "return"'
sub isWithExhaustion       { 0 }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "isWithExhaustion" does not end with "return"'
sub isWithNewline          { 1 }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "isWithNewline" does not end with "return"'
sub isWithTrack            { 1 }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "isWithTrack" does not end with "return"'

package MyValueInterface;  # this line violates Perl::Critic::Policy::Modules::ProhibitMultiplePackages, with severity 4 out of 5, and description 'Multiple "package" declarations', and explanation 'Limit to one per file'
use strict;
use diagnostics;
use Data::Dumper;

sub new                { my ($pkg) = @_; bless { result => undef }, $pkg }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "new" does not end with "return"'
sub isWithHighRankOnly { 1 }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "isWithHighRankOnly" does not end with "return"'
sub isWithOrderByRank  { 1 }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "isWithOrderByRank" does not end with "return"'
sub isWithAmbiguous    { 0 }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "isWithAmbiguous" does not end with "return"'
sub isWithNull         { 0 }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "isWithNull" does not end with "return"'
sub maxParses          { 0 }  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "maxParses" does not end with "return"'
sub getResult          { $_[0]->{result} }  # this line violates Perl::Critic::Policy::Subroutines::RequireArgUnpacking, with severity 4 out of 5, and description 'Always unpack @_ first'  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "getResult" does not end with "return"'
sub setResult          { $_[0]->{result} = $_[1] }  # this line violates Perl::Critic::Policy::Subroutines::RequireArgUnpacking, with severity 4 out of 5, and description 'Always unpack @_ first'  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "setResult" does not end with "return"'
sub perl_proxy         { print "  perl_proxy received value: " . Dumper($_[1]); $_[1] }  # this line violates Perl::Critic::Policy::Subroutines::RequireArgUnpacking, with severity 4 out of 5, and description 'Always unpack @_ first'  # this line violates Perl::Critic::Policy::Subroutines::RequireFinalReturn, with severity 4 out of 5, and description 'Subroutine "perl_proxy" does not end with "return"'

package main;  # this line violates Perl::Critic::Policy::Modules::ProhibitMultiplePackages, with severity 4 out of 5, and description 'Multiple "package" declarations', and explanation 'Limit to one per file'
use strict;
use warnings FATAL => 'all';
use Test::More tests => 31; # require_ok + scalar(@input)
use Test::More::UTF8;
use Test::Deep qw/cmp_details deep_diag/;
use Log::Any qw/$log/;
use Log::Any::Adapter 'Stdout';
use Math::BigInt;
use Math::BigFloat;
use Encode qw/ encode :fallbacks /;
use utf8;
use Safe::Isa;
use open qw( :utf8 :std );

BEGIN { require_ok('MarpaX::ESLIF') }
#
# Our input depends on MarpaX::ESLIF, and Test::More says it has to be
# in another BEGIN block, after the one that is loading/requiring our module
#
my @input;
BEGIN {
    @input =
        (
         undef,
         "XXX",
         "\xa0\xa1",
         ["\xa0\xa1", 0],
         ["\xf0\x28\x8c\x28", 0],
         "Ḽơᶉëᶆ ȋṕšᶙṁ ḍỡḽǭᵳ ʂǐť ӓṁệẗ, ĉṓɲṩḙċťᶒțûɾ ấɖḯƥĭṩčįɳġ ḝłįʈ, șếᶑ ᶁⱺ ẽḭŭŝḿꝋď ṫĕᶆᶈṓɍ ỉñḉīḑȋᵭṵńť ṷŧ ḹẩḇőꝛế éȶ đꝍꞎôꝛȇ ᵯáꞡᶇā ąⱡîɋṹẵ.",
         ["Ḽơᶉëᶆ ȋṕšᶙṁ ḍỡḽǭᵳ ʂǐť ӓṁệẗ, ĉṓɲṩḙċťᶒțûɾ ấɖḯƥĭṩčįɳġ ḝłįʈ, șếᶑ ᶁⱺ ẽḭŭŝḿꝋď ṫĕᶆᶈṓɍ ỉñḉīḑȋᵭṵńť ṷŧ ḹẩḇőꝛế éȶ đꝍꞎôꝛȇ ᵯáꞡᶇā ąⱡîɋṹẵ.", 0],
         ["Ḽơᶉëᶆ ȋṕšᶙṁ ḍỡḽǭᵳ ʂǐť ӓṁệẗ, ĉṓɲṩḙċťᶒțûɾ ấɖḯƥĭṩčįɳġ ḝłįʈ, șếᶑ ᶁⱺ ẽḭŭŝḿꝋď ṫĕᶆᶈṓɍ ỉñḉīḑȋᵭṵńť ṷŧ ḹẩḇőꝛế éȶ đꝍꞎôꝛȇ ᵯáꞡᶇā ąⱡîɋṹẵ.", 1],
         0,
         1,
         -32768,
         32767,
         -32769,
         32768,
         2.34,
         1.6e+308,
         Math::BigFloat->new("6.78E+9"),
         Math::BigInt->new("6.78E+9"),
         [[1, undef, 2], 0],
         "",
         $MarpaX::ESLIF::true,
         $MarpaX::ESLIF::false,
         { one => "one", two => "two", perltrue => 1, true => $MarpaX::ESLIF::true, false => $MarpaX::ESLIF::false, 'else' => 'again', 'undef' => undef },
         { element1 => { one => "one", two => "two", perltrue => 1, true => $MarpaX::ESLIF::true, false => $MarpaX::ESLIF::false, 'else' => 'again', 'undef' => undef },
           element2 => { one => "one", two => "two", perltrue => 1, true => $MarpaX::ESLIF::true, false => $MarpaX::ESLIF::false, 'else' => 'again', 'undef' => undef }},
         MarpaX::ESLIF::String->new("XXXḼơᶉëᶆYYY", 'UTF-8'),
         MarpaX::ESLIF::String->new("", 'UTF-8'),
         MarpaX::ESLIF::String->new(encode('UTF-16LE', my $s = "XXXḼơᶉëᶆYYY", FB_CROAK), 'UTF-16LE'),
         MarpaX::ESLIF::String->new("", 'UTF-16'),
         'one',
         'two'
        );
}

my $grammar = q{
event ^perl_input = predicted perl_input

perl_output ::= lua_proxy  action => perl_proxy
lua_proxy   ::= perl_input action => ::lua->lua_proxy
perl_input  ::= PERL_INPUT action => perl_proxy
PERL_INPUT    ~ [^\s\S]

<luascript>
  function table_print (tt, indent, done)
    done = done or {}
    indent = indent or 0
    if type(tt) == "table" then
      for key, value in pairs (tt) do
        io.write(string.rep (" ", indent)) -- indent it
        if type (value) == "table" and not done [value] then
          done [value] = true
          io.write(string.format("  [%s] => table\n", tostring(key)));
          io.write(string.rep (" ", indent+4)) -- indent it
          io.write("(\n");
          table_print (value, indent + 7, done)
          io.write(string.rep (" ", indent+4)) -- indent it
          io.write(")\n");
        else
          if type(value) == 'string' then
            io.write(string.format("  [%s] => %s (type: %s, encoding: %s, length: %d bytes)\n", tostring (key), tostring(value), type(value), tostring(value:encoding()), string.len(value)))
          else
            io.write(string.format("  [%s] => %s (type: %s)\n", tostring (key), tostring(value), type(value)))
          end
        end
      end
    else
      io.write(tostring(tt) .. "\n")
    end
  end
  io.stdout:setvbuf('no')

  function lua_proxy(value)
    print('  lua_proxy received value of type: '..type(value))
    if type(value) == 'string' then
      print('  lua_proxy value: '..tostring(value)..', encoding: '..tostring(value:encoding())..', length: '..string.len(value)..' bytes')
    else
      print('  lua_proxy value: '..tostring(value))
      if type(value) == 'table' then
        table_print(value)
      end
    end
    return value
  end
</luascript>
};

my $eslif = MarpaX::ESLIF->new($log);
my $eslifGrammar = MarpaX::ESLIF::Grammar->new($eslif, $grammar);

foreach my $inputArray (@input) {
    my ($input, $characterStream) = (ref($inputArray) || '') eq 'ARRAY' ? @{$inputArray} : ($inputArray, 1);
    my $eslifRecognizerInterface = MyRecognizerInterface->new($characterStream);
    my $eslifRecognizer = MarpaX::ESLIF::Recognizer->new($eslifGrammar, $eslifRecognizerInterface);
    $eslifRecognizer->scan(1); # Initial events
    $eslifRecognizer->alternativeRead('PERL_INPUT', $input, 1, 1);
    my $eslifValueInterface = MyValueInterface->new();
    my $eslifValue = MarpaX::ESLIF::Value->new($eslifRecognizer, $eslifValueInterface);
    $eslifValue->value();
    my $value = $eslifValueInterface->getResult;
    my $original_input = $input;
    if ($original_input->$_isa('MarpaX::ESLIF::String') && $original_input->encoding eq 'UTF-8') {
        #
        # marpaESLIF will always reinject UTF-8 strings as true PV scalars for performance
        #
        $input = "$input";
        $value = "$value" if defined($value);
    }
    my ($ok, $stack) = cmp_details($value, $input);
    diag(deep_diag($stack)) unless (ok($ok, "import/export of " . (ref($original_input) ? ref($original_input) : (defined($original_input) ? ((length($original_input) > 0) ? "$original_input" : '<empty string>') : 'undef'))));
}

done_testing();

1;
