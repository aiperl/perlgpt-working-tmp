$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Subroutine "new" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       197
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    9,
                                    1,
                                    1,
                                    9,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub new                    { my ($pkg, $string) = @_; bless { string => $string }, $pkg }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Code before warnings are enabled',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       431
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    9,
                                    1,
                                    1,
                                    9,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings',
                   '_severity' => 4,
                   '_source' => 'sub new                    { my ($pkg, $string) = @_; bless { string => $string }, $pkg }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine name is a homonym for builtin function read',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       177
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    10,
                                    1,
                                    1,
                                    10,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::ProhibitBuiltinHomonyms',
                   '_severity' => 4,
                   '_source' => 'sub read                   { 1 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "read" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    10,
                                    1,
                                    1,
                                    10,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub read                   { 1 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "isEof" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    11,
                                    1,
                                    1,
                                    11,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub isEof                  { 1 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "isCharacterStream" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    12,
                                    1,
                                    1,
                                    12,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub isCharacterStream      { 1 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       178
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    14,
                                    1,
                                    1,
                                    14,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub data                   { $_[0]->{string} }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "data" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    14,
                                    1,
                                    1,
                                    14,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub data                   { $_[0]->{string} }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "isWithDisableThreshold" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    15,
                                    1,
                                    1,
                                    15,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub isWithDisableThreshold { 0 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "isWithExhaustion" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    16,
                                    1,
                                    1,
                                    16,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub isWithExhaustion       { 0 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "isWithNewline" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    17,
                                    1,
                                    1,
                                    17,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub isWithNewline          { 1 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "isWithTrack" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    18,
                                    1,
                                    1,
                                    18,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub isWithTrack            { 1 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    19,
                                    1,
                                    1,
                                    19,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub if_action {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    27,
                                    1,
                                    1,
                                    27,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub event_action {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    35,
                                    1,
                                    1,
                                    35,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub regex_action {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    43,
                                    1,
                                    1,
                                    43,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub generator_action {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "setRecognizer" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    51,
                                    1,
                                    1,
                                    51,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub setRecognizer          { my ($self, $recognizer) = @_; $log->debugf(\'setRecognizer: %s\', $recognizer); $self->{recognizer} = $recognizer; }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "getRecognizer" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    52,
                                    1,
                                    1,
                                    52,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub getRecognizer          { my ($self) = shift; $self->{recognizer} }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Multiple "package" declarations',
                   '_element_class' => 'PPI::Statement::Package',
                   '_explanation' => 'Limit to one per file',
                   '_filename' => undef,
                   '_location' => [
                                    54,
                                    1,
                                    1,
                                    54,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::ProhibitMultiplePackages',
                   '_severity' => 4,
                   '_source' => 'package MyValueInterface;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "new" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    59,
                                    1,
                                    1,
                                    59,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub new                { my ($pkg) = @_; bless { result => undef }, $pkg }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "isWithHighRankOnly" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    60,
                                    1,
                                    1,
                                    60,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub isWithHighRankOnly { 1 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "isWithOrderByRank" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    61,
                                    1,
                                    1,
                                    61,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub isWithOrderByRank  { 1 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "isWithAmbiguous" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    62,
                                    1,
                                    1,
                                    62,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub isWithAmbiguous    { 0 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "isWithNull" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    63,
                                    1,
                                    1,
                                    63,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub isWithNull         { 0 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "maxParses" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    64,
                                    1,
                                    1,
                                    64,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub maxParses          { 0 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    65,
                                    1,
                                    1,
                                    65,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub getResult          { $_[0]->{result} }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "getResult" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    65,
                                    1,
                                    1,
                                    65,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub getResult          { $_[0]->{result} }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    66,
                                    1,
                                    1,
                                    66,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub setResult          { $_[0]->{result} = $_[1] }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "setResult" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    66,
                                    1,
                                    1,
                                    66,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub setResult          { $_[0]->{result} = $_[1] }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[6]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    68,
                                    1,
                                    1,
                                    68,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub do_members {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Multiple "package" declarations',
                   '_element_class' => 'PPI::Statement::Package',
                   '_explanation' => 'Limit to one per file',
                   '_filename' => undef,
                   '_location' => [
                                    77,
                                    1,
                                    1,
                                    77,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::ProhibitMultiplePackages',
                   '_severity' => 4,
                   '_source' => 'package main;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Module does not end with "1;"',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => 'Must end with a recognizable true value',
                   '_filename' => undef,
                   '_location' => [
                                    399,
                                    1,
                                    1,
                                    399,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::RequireEndWithOne',
                   '_severity' => 4,
                   '_source' => 'done_testing();'
                 }, 'Perl::Critic::Violation' )
        ];
1;
