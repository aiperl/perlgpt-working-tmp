$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Code before warnings are enabled',
                   '_element_class' => 'PPI::Statement::Variable',
                   '_explanation' => [
                                       431
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    20,
                                    1,
                                    1,
                                    20,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings',
                   '_severity' => 4,
                   '_source' => 'my $have_cppguess;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Expression form of "eval"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       161
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    31,
                                    22,
                                    22,
                                    31,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval',
                   '_severity' => 5,
                   '_source' => '$have_cppguess = eval \'use ExtUtils::CppGuess 0.26; 1;\';'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$|" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => [
                                       81,
                                       82
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    60,
                                    4,
                                    4,
                                    60,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$| = 1;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$ENV" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    75,
                                    10,
                                    10,
                                    75,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$ENV{CC} = $cbuilder_config{cc} // \'cc\';'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$ENV" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    78,
                                    11,
                                    11,
                                    78,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$ENV{CXX} = $cbuilder_config{cxx} // $ENV{CC};'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$ENV" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    79,
                                    16,
                                    16,
                                    79,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$ENV{CXXFLAGS} = $cbuilder_config{cxxflags} // $cbuilder_config{ccflags} // \'\';'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$ENV" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    80,
                                    10,
                                    10,
                                    80,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$ENV{LD} = $cbuilder_config{ld} // $ENV{CC};'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$ENV" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    151,
                                    23,
                                    23,
                                    151,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$ENV{CXX} = $cxx;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$ENV" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    177,
                                    23,
                                    23,
                                    177,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$ENV{CXX} = $cxx_guess;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$ENV" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    228,
                                    23,
                                    23,
                                    228,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$ENV{CXX} = $cxx;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$ENV" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    240,
                                    23,
                                    23,
                                    240,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$ENV{CXX} = $cxx;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$ENV" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    248,
                                    19,
                                    19,
                                    248,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$ENV{CXX} = $ENV{CC};'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       178
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    1008,
                                    1,
                                    1,
                                    1008,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub try_compile {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Warnings disabled',
                   '_element_class' => 'PPI::Statement::Include',
                   '_explanation' => [
                                       431
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    1009,
                                    5,
                                    5,
                                    1009,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::ProhibitNoWarnings',
                   '_severity' => 4,
                   '_source' => 'no warnings \'once\';'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Two-argument "open" used',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       207
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    1031,
                                    5,
                                    5,
                                    1031,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::ProhibitTwoArgOpen',
                   '_severity' => 5,
                   '_source' => 'open(my $oldout, ">&STDOUT") or die "Can\'t dup STDOUT: $!";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Close filehandles as soon as possible after opening them',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       209
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    1031,
                                    5,
                                    5,
                                    1031,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::RequireBriefOpen',
                   '_severity' => 4,
                   '_source' => 'open(my $oldout, ">&STDOUT") or die "Can\'t dup STDOUT: $!";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Bareword file handle opened',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       202,
                                       204
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    1032,
                                    5,
                                    5,
                                    1032,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::ProhibitBarewordFileHandles',
                   '_severity' => 5,
                   '_source' => 'open(OLDERR, ">&", \\*STDERR) or die "Can\'t dup STDERR: $!";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Close filehandles as soon as possible after opening them',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[15]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1032,
                                    5,
                                    5,
                                    1032,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::RequireBriefOpen',
                   '_severity' => 4,
                   '_source' => 'open(OLDERR, ">&", \\*STDERR) or die "Can\'t dup STDERR: $!";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Two-argument "open" used',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[14]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1035,
                                    9,
                                    9,
                                    1035,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::ProhibitTwoArgOpen',
                   '_severity' => 5,
                   '_source' => 'open(STDERR, ">&STDOUT") or die "Can\'t dup STDOUT: $!";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'One-argument "select" used',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       224
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    1036,
                                    9,
                                    9,
                                    1036,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::ProhibitOneArgSelect',
                   '_severity' => 4,
                   '_source' => 'select STDERR;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$|" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1036,
                                    27,
                                    27,
                                    1036,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$| = 1;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'One-argument "select" used',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[19]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1037,
                                    9,
                                    9,
                                    1037,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::ProhibitOneArgSelect',
                   '_severity' => 4,
                   '_source' => 'select STDOUT;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$|" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1037,
                                    27,
                                    27,
                                    1037,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$| = 1;'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Two-argument "open" used',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[14]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1124,
                                    5,
                                    5,
                                    1124,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::ProhibitTwoArgOpen',
                   '_severity' => 5,
                   '_source' => 'open(STDERR, ">&OLDERR") or die "Can\'t dup OLDERR: $!";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_math" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       197
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    1164,
                                    1,
                                    1,
                                    1164,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_math {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_ebcdic" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1185,
                                    1,
                                    1,
                                    1185,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_ebcdic {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_inline" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1212,
                                    1,
                                    1,
                                    1212,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_inline {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_forceinline" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1244,
                                    1,
                                    1,
                                    1244,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_forceinline {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_va_copy" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1273,
                                    1,
                                    1,
                                    1273,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_va_copy {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_vsnprintf" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1317,
                                    1,
                                    1,
                                    1317,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_vsnprintf {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_fileno" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1359,
                                    1,
                                    1,
                                    1359,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_fileno {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_localtime_r" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1389,
                                    1,
                                    1,
                                    1389,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_localtime_r {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_write" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1421,
                                    1,
                                    1,
                                    1421,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_write {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_log2" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1461,
                                    1,
                                    1,
                                    1461,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_log2 {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_log2f" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1491,
                                    1,
                                    1,
                                    1491,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_log2f {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_strtold" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1564,
                                    1,
                                    1,
                                    1564,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_strtold {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_strtod" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1593,
                                    1,
                                    1,
                                    1593,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_strtod {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_strtof" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1622,
                                    1,
                                    1,
                                    1622,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_strtof {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_strtoll" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1913,
                                    1,
                                    1,
                                    1913,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_strtoll {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_strtoull" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1951,
                                    1,
                                    1,
                                    1951,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_strtoull {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_fpclassify" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    1989,
                                    1,
                                    1,
                                    1989,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_fpclassify {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_const" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2061,
                                    1,
                                    1,
                                    2061,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_const {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_c99_modifiers" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2087,
                                    1,
                                    1,
                                    2087,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_c99_modifiers {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_restrict" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2138,
                                    1,
                                    1,
                                    2138,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_restrict {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_builtin_expect" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2177,
                                    1,
                                    1,
                                    2177,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_builtin_expect {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_signbit" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2221,
                                    1,
                                    1,
                                    2221,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_signbit {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_copysign" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2253,
                                    1,
                                    1,
                                    2253,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_copysign {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_copysignf" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2286,
                                    1,
                                    1,
                                    2286,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_copysignf {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_copysignl" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2319,
                                    1,
                                    1,
                                    2319,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_copysignl {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_nl_langinfo" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2352,
                                    1,
                                    1,
                                    2352,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_nl_langinfo {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_gnu_source" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2443,
                                    1,
                                    1,
                                    2443,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_gnu_source {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "configure_file" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2761,
                                    1,
                                    1,
                                    2761,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub configure_file {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Close filehandles as soon as possible after opening them',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[15]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2780,
                                    5,
                                    5,
                                    2780,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::RequireBriefOpen',
                   '_severity' => 4,
                   '_source' => 'open(my $fhout, \'>\', $out) || die "Cannot open for writing $out, $!";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Close filehandles as soon as possible after opening them',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[15]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2785,
                                    5,
                                    5,
                                    2785,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::RequireBriefOpen',
                   '_severity' => 4,
                   '_source' => 'open(my $fhin, \'<\', $in) || die "Cannot open for reading $in, $!";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "process_genericStack" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2810,
                                    1,
                                    1,
                                    2810,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub process_genericStack {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "process_genericHash" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2825,
                                    1,
                                    1,
                                    2825,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub process_genericHash {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "process_genericSparseArray" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2840,
                                    1,
                                    1,
                                    2840,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub process_genericSparseArray {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "process_genericLogger" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2855,
                                    1,
                                    1,
                                    2855,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub process_genericLogger {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "process_tconv" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2898,
                                    1,
                                    1,
                                    2898,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub process_tconv {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "process_marpaWrapper" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    2976,
                                    1,
                                    1,
                                    2976,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub process_marpaWrapper {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "process_marpaESLIF" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3033,
                                    1,
                                    1,
                                    3033,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub process_marpaESLIF {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "process_libiconv" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3152,
                                    1,
                                    1,
                                    3152,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub process_libiconv {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Close filehandles as soon as possible after opening them',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[15]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3169,
                                    5,
                                    5,
                                    3169,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::RequireBriefOpen',
                   '_severity' => 4,
                   '_source' => 'open(my $config_h_in_fd, \'<\', $config_h_in) || die "Cannot open $config_h_in, $!";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Close filehandles as soon as possible after opening them',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[15]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3170,
                                    5,
                                    5,
                                    3170,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::RequireBriefOpen',
                   '_severity' => 4,
                   '_source' => 'open(my $config_h_in_cmake_fd, \'>\', $config_h_in_cmake) || die "Cannot open $config_h_in_cmake, $!";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Close filehandles as soon as possible after opening them',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[15]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3202,
                                    5,
                                    5,
                                    3202,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::RequireBriefOpen',
                   '_severity' => 4,
                   '_source' => 'open(my $iconv_h_in_fd, \'<\', $iconv_h_in) || die "Cannot open $iconv_h_in, $!";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Close filehandles as soon as possible after opening them',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[15]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3203,
                                    5,
                                    5,
                                    3203,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::RequireBriefOpen',
                   '_severity' => 4,
                   '_source' => 'open(my $iconv_h_in_cmake_fd, \'>\', $iconv_h_in_cmake) || die "Cannot open $iconv_h_in_cmake, $!";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "process_cchardet" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3263,
                                    1,
                                    1,
                                    3263,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub process_cchardet {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Close filehandles as soon as possible after opening them',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[15]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3294,
                                    5,
                                    5,
                                    3294,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::RequireBriefOpen',
                   '_severity' => 4,
                   '_source' => 'open(my $prmem_h_fd, \'>\', $prmem_h) || die "Cannot open $prmem_h, $!";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "process_dlfcn_win32" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3343,
                                    1,
                                    1,
                                    3343,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub process_dlfcn_win32 {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "process_pcre2" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3370,
                                    1,
                                    1,
                                    3370,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub process_pcre2 {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "process_luaunpanic" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3492,
                                    1,
                                    1,
                                    3492,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub process_luaunpanic {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "process_marpaESLIFLua" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3512,
                                    1,
                                    1,
                                    3512,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub process_marpaESLIFLua {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "generate_export_h" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3549,
                                    1,
                                    1,
                                    3549,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub generate_export_h {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Close filehandles as soon as possible after opening them',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => $VAR1->[15]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3564,
                                    5,
                                    5,
                                    3564,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::InputOutput::RequireBriefOpen',
                   '_severity' => 4,
                   '_source' => 'open(my $fh, \'>\', $export) || die "Cannot open $export, $!";'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "check_big_endian" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3583,
                                    1,
                                    1,
                                    3583,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub check_big_endian {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "build_object" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[24]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3619,
                                    1,
                                    1,
                                    3619,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub build_object {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$b" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3635,
                                    16,
                                    16,
                                    3635,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$b = ExtUtils::CBuilder->new(config => { optimize => $optimize});'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$b" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3637,
                                    16,
                                    16,
                                    3637,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$b = ExtUtils::CBuilder->new();'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Magic variable "$b" should be assigned as "local"',
                   '_element_class' => 'PPI::Token::Operator',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    3662,
                                    12,
                                    12,
                                    3662,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Variables::RequireLocalizedPunctuationVars',
                   '_severity' => 4,
                   '_source' => '$b = ExtUtils::CBuilder->new();'
                 }, 'Perl::Critic::Violation' )
        ];
1;
