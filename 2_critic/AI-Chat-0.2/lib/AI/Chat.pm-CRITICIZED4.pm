package AI::Chat;

use strict;
use warnings;

use Carp;
use HTTP::Tiny;
use JSON::PP;

our $VERSION = '0.2';
$VERSION = eval $VERSION;  # this line violates Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval, with severity 5 out of 5, and description 'Expression form of "eval"'

my $http = HTTP::Tiny->new;

# Create Chat object
sub new {  # this line violates Perl::Critic::Policy::Subroutines::RequireArgUnpacking, with severity 4 out of 5, and description 'Always unpack @_ first'
    my $class = shift;
    my %attr  = @_;

    $attr{'error'}      = '';

    $attr{'api'}        = 'OpenAI' unless $attr{'api'};
    $attr{'error'}      = 'Invalid API' unless $attr{'api'} eq 'OpenAI';
    $attr{'error'}      = 'API Key missing' unless $attr{'key'};

    $attr{'model'}      = 'gpt-3.5-turbo-0125' unless $attr{'model'};

    return bless \%attr, $class;
}

# Define endpoints for APIs
my %url    = (
    'OpenAI' => 'https://api.openai.com/v1/chat/completions',
);

# Define HTTP Headers for APIs
my %header = (
    'OpenAI' => &_get_header_openai,
);

# Returns true if last operation was success
sub success {
    my $self = shift;
    return !$self->{'error'};
}

# Returns error if last operation failed
sub error {
    my $self = shift;
    return $self->{'error'};
}

# Header for calling OpenAI
sub _get_header_openai {
    my $self = shift;
    $self->{'key'} = '' unless defined $self->{'key'};
    return {
         'Authorization' => 'Bearer ' . $self->{'key'},
         'Content-type'  => 'application/json'
     };
 }
 
 # Get a reply from a single prompt
 sub prompt {  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'
     my ($self, $prompt, $temperature) = @_;
     
     $self->{'error'} = '';
     unless ($prompt) {
         $self->{'error'} = "Missing prompt calling 'prompt' method";
         return undef;  # this line violates Perl::Critic::Policy::Subroutines::ProhibitExplicitReturnUndef, with severity 5 out of 5, and description '"return" statement with explicit "undef"'
     }

    $temperature = 1.0 unless $temperature;

    my @messages;
    push @messages, {
        role    => 'system',
        content => $self->{'role'},
    } if $self->{'role'};
    push @messages, {
        role    => 'user',
        content => $prompt,
    };

    my $response = $http->post($url{$self->{'api'}}, {
         'headers' => {
             'Authorization' => 'Bearer ' . $self->{'key'},
             'Content-type'  => 'application/json'
         },
         content => encode_json {
             model     => $self->{'model'},
             messages  => [ @messages ],
             temperature    => $temperature,
         }
     });
     if ($response->{'content'} =~ 'invalid_api_key') {
         croak 'Incorrect API Key - check your API Key is correct';
     }
     
     if ($self->{'debug'} and !$response->{'success'}) {  # this line violates Perl::Critic::Policy::ValuesAndExpressions::ProhibitMixedBooleanOperators, with severity 4 out of 5, and description 'Mixed high and low-precedence booleans'
         croak $response if $self->{'debug'} eq 'verbose';
         croak $response->{'content'};
     }

     my $reply = decode_json($response->{'content'});
     
     return $reply->{'choices'}[0]->{'message'}->{'content'};
}

__END__

=head1 NAME

AI::Chat - Interact with AI Chat APIs

=head1 VERSION

Version 0.2

=head1 SYNOPSIS

  use AI::Chat;

  my $chat  = AI::Chat->new(
      key   => 'your-api-key',
      api   => 'OpenAI',
      model => 'gpt-3.5-turbo-0125',
  );

  my $reply = $chat->prompt("What is the meaning of life?");
  print $reply;

=head1 DESCRIPTION

This module provides a simple interface for interacting with AI Chat APIs,
currently supporting OpenAI.

The AI chat agent can be given a I<role> and then passed I<prompts>.  It will
reply to the prompts in natural language.  Being AI, the responses are
non-deterministic, that is, the same prompt will result in diferent responses
on different occasions.

Further control of the creativity of the responses is possible by specifying
at optional I<temperature> parameter.

=head1 API KEYS

A free OpenAI API can be obtained from L<https://platform.openai.com/account/api-keys>

=head1 MODELS

Although the API Key is free, each use incurs a cost.  This is dependent on the
number of tokens in the prompt and the reply.  Different models have different costs.
The default model C<gpt-3.5-turbo-0125> is the lowest cost of the useful models and
is a good place to start using this module.

See also L<https://platform.openai.com/docs/models/overview>

=head1 METHODS

=head2 new

  my $chat = AI::Chat->new(%params);

Creates a new AI::Chat object.

=head3 Parameters

=over 4

=item key

C<required> Your API key for the chosen service.

=item api

The API to use (currently only 'OpenAI' is supported).

=item model

The language model to use (default: 'gpt-3.5-turbo-0125').

See L<https://platform.openai.com/docs/models/overview>

=item role

The role to use for the bot in conversations.

This tells the bot what it's purpose when answering prompts.

For example: "You are a world class copywriter famed for
creating content that is immediately engaging with a
lighthearted, storytelling style".

=item debug

Used for testing.  If set to any true value, the prompt method
will return details of the error encountered instead of C<undef>

=back

=head2 prompt

  my $reply = $chat->prompt($prompt, $temperature);

Sends a prompt to the AI Chat API and returns the response.

=head3 Parameters

=over 4

=item prompt

C<required> The prompt to send to the AI.

=item temperature

The creativity level of the response (default: 1.0).

Temperature ranges from 0 to 2.  The higher the temperature,
the more creative the bot will be in it's responses.

=back

=head2 success

  my $success = $chat->success();

Returns true if the last operation was successful.

=head2 error

  my $error = $chat->error();

Returns the error message if the last operation failed.

=head1 SEE ALSO

L<https://openai.com> - OpenAI official website

=head1 AUTHOR

Ian Boddison <ian at boddison.com>

=head1 BUGS

Please report any bugs or feature requests to C<bug-ai-chat at rt.cpan.org>, or through
the web interface at L<https://rt.cpan.org/NoAuth/ReportBug.html?Queue=bug-ai-chat>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc AI::Chat

You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<https://rt.cpan.org/NoAuth/Bugs.html?Dist=AI-Chat>

=item * Search CPAN

L<https://metacpan.org/release/AI::Chat>

=back

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2024 by Ian Boddison

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

