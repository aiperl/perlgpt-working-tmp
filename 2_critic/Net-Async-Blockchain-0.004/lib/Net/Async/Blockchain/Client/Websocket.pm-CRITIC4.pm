$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Subroutine "endpoint" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       197
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    82,
                                    1,
                                    1,
                                    82,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub endpoint : method { shift->{endpoint} }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "latest_subscription" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    90,
                                    1,
                                    1,
                                    90,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub latest_subscription : method { shift->{latest_subscription} }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Subroutine "configure" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[0]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    156,
                                    1,
                                    1,
                                    156,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub configure {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Module does not end with "1;"',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => 'Must end with a recognizable true value',
                   '_filename' => undef,
                   '_location' => [
                                    182,
                                    1,
                                    1,
                                    182,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::RequireEndWithOne',
                   '_severity' => 4,
                   '_source' => 'async sub _request {'
                 }, 'Perl::Critic::Violation' )
        ];
1;
