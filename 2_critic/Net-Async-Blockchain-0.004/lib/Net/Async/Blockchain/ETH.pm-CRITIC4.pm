$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Subroutine "subscription_id" does not end with "return"',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       197
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    58,
                                    1,
                                    1,
                                    58,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireFinalReturn',
                   '_severity' => 4,
                   '_source' => 'sub subscription_id { shift->{subscription_id} }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Module does not end with "1;"',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => 'Must end with a recognizable true value',
                   '_filename' => undef,
                   '_location' => [
                                    103,
                                    1,
                                    1,
                                    103,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Modules::RequireEndWithOne',
                   '_severity' => 4,
                   '_source' => 'async sub subscribe {'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => '"return" statement with explicit "undef"',
                   '_element_class' => 'PPI::Token::Word',
                   '_explanation' => [
                                       199
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    125,
                                    17,
                                    17,
                                    125,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::ProhibitExplicitReturnUndef',
                   '_severity' => 5,
                   '_source' => 'return undef unless $response->{params} && $response->{params}->{subscription};'
                 }, 'Perl::Critic::Violation' )
        ];
1;
