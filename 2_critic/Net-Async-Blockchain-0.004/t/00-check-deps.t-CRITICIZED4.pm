use strict;  # this line violates Perl::Critic::Policy::Modules::RequireExplicitPackage, with severity 4 out of 5, and description 'Code not contained in explicit package', and explanation 'Violates encapsulation'
use warnings;

# this test was generated with Dist::Zilla::Plugin::Test::CheckDeps 0.014

use Test::More 0.94;
use Test::CheckDeps 0.010;

check_dependencies('classic');

if (0) {
    BAIL_OUT("Missing dependencies") if !Test::More->builder->is_passing;
}

done_testing;  # this line violates Perl::Critic::Policy::Modules::RequireEndWithOne, with severity 4 out of 5, and description 'Module does not end with "1;"', and explanation 'Must end with a recognizable true value'
