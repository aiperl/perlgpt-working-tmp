#!/usr/bin/perl
use strict; use warnings;

use Test::More;

eval "use Test::Pod::Spelling::CommonMistakes";  # this line violates Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval, with severity 5 out of 5, and description 'Expression form of "eval"'
if ( $@ ) {
    plan skip_all => 'Test::Pod::Spelling::CommonMistakes required for testing POD';
} else {
    all_pod_files_ok();
}
