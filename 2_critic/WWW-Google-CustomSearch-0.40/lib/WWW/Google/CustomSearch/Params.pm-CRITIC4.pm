$distribution_file_critic_violations = $VAR1 = [
          bless( {
                   '_description' => 'Code before strictures are enabled',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => [
                                       429
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    3,
                                    1,
                                    1,
                                    3,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict',
                   '_severity' => 5,
                   '_source' => '$WWW::Google::CustomSearch::Params::VERSION   = \'0.40\';'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Code before warnings are enabled',
                   '_element_class' => 'PPI::Statement',
                   '_explanation' => [
                                       431
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    3,
                                    1,
                                    1,
                                    3,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::TestingAndDebugging::RequireUseWarnings',
                   '_severity' => 4,
                   '_source' => '$WWW::Google::CustomSearch::Params::VERSION   = \'0.40\';'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => [
                                       178
                                     ],
                   '_filename' => undef,
                   '_location' => [
                                    158,
                                    1,
                                    1,
                                    158,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub check_int                { return defined ($_[0]) && $_[0] =~ /^\\d+$/                      }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    159,
                                    1,
                                    1,
                                    159,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub check_str                { return !check_int($_[0])                                        }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    160,
                                    1,
                                    1,
                                    160,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub check_language           { return exists($LANGUAGE->{lc($_[0])})                           }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    161,
                                    1,
                                    1,
                                    161,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub check_country_collection { return exists($COUNTRY_COLLECTION->{lc($_[0])})                 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    162,
                                    1,
                                    1,
                                    162,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub check_file_type          { return exists($FILE_TYPE->{lc($_[0])})                          }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    163,
                                    1,
                                    1,
                                    163,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub check_country_code       { return exists($COUNTRY_CODE->{lc($_[0])})                       }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    164,
                                    1,
                                    1,
                                    164,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub check_interface_language { return exists($INTERFACE_LANGUAGE->{lc($_[0])})                 }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    165,
                                    1,
                                    1,
                                    165,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub check_color_type         { return exists($COLOR_TYPE->{lc($_[0])})                         }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    166,
                                    1,
                                    1,
                                    166,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub check_dominant_color     { return exists($DOMINANT_COLOR->{lc($_[0])})                     }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    167,
                                    1,
                                    1,
                                    167,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub check_image_size         { return exists($IMAGE_SIZE->{lc($_[0])})                         }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    168,
                                    1,
                                    1,
                                    168,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub check_image_type         { return exists($IMAGE_TYPE->{lc($_[0])})                         }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    169,
                                    1,
                                    1,
                                    169,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub check_rights             { return exists($RIGHTS->{lc($_[0])})                             }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    170,
                                    1,
                                    1,
                                    170,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub check_search_type        { return exists($SEARCH_TYPE->{lc($_[0])})                        }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    171,
                                    1,
                                    1,
                                    171,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub check_search_filter      { return exists($SEARCH_FILTER->{lc($_[0])})                      }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    172,
                                    1,
                                    1,
                                    172,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub check_safety_level       { return exists($SAFETY_LEVEL->{lc($_[0])})                       }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    173,
                                    1,
                                    1,
                                    173,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub check_date_restrict      { return ($_[0] =~ /^[d|w|m|y]\\d+$/i)                             }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    174,
                                    1,
                                    1,
                                    174,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub check_start_index        { return ($_[0] =~ /^\\d+$/) && ($_[0] >= 1)                       }'
                 }, 'Perl::Critic::Violation' ),
          bless( {
                   '_description' => 'Always unpack @_ first',
                   '_element_class' => 'PPI::Statement::Sub',
                   '_explanation' => $VAR1->[2]{'_explanation'},
                   '_filename' => undef,
                   '_location' => [
                                    175,
                                    1,
                                    1,
                                    175,
                                    undef
                                  ],
                   '_policy' => 'Perl::Critic::Policy::Subroutines::RequireArgUnpacking',
                   '_severity' => 4,
                   '_source' => 'sub check_result_count       { return ($_[0] =~ /^\\d{1,2}$/) && ($_[0] >= 1) && ($_[0] <= 10)  }'
                 }, 'Perl::Critic::Violation' )
        ];
1;
