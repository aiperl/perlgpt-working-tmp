$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'Date::Holidays::ES' => 'Spanish holidays'
                                },
          'author' => '',
          'dir_lib' => 'lib',
          'dir_t' => 't',
          'dirs' => 4,
          'dirs_array' => [
                            'lib/Date/Holidays',
                            'lib/Date',
                            'lib',
                            't'
                          ],
          'dist' => 'Date-Holidays-ES',
          'dynamic_config' => 0,
          'error' => {
                       'extracts_nicely' => 'expected Date-Holidays-ES but got Date-Holidays-ES-0.03'
                     },
          'extension' => 'tar.gz',
          'external_license_file' => 'LICENSE',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_build_pl' => 'Build.PL',
          'file_changelog' => 'CHANGELOG.md',
          'file_cpanfile' => 'cpanfile',
          'file_dist_ini' => 'dist.ini',
          'file_license' => 'LICENSE',
          'file_makefile_pl' => 'Makefile.PL',
          'file_manifest' => 'MANIFEST',
          'file_meta_json' => 'META.json',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README',
          'files' => 21,
          'files_array' => [
                             'Build.PL',
                             'CHANGELOG.md',
                             'Date-Holidays-ES.code-workspace',
                             'INSTALL',
                             'LICENSE',
                             'MANIFEST',
                             'META.json',
                             'META.yml',
                             'Makefile.PL',
                             'README',
                             'cpanfile',
                             'dist.ini',
                             'lib/Date/Holidays/ES.pm',
                             't/00-compile.t',
                             't/00.use.t',
                             't/01.es.t',
                             't/author-critic.t',
                             't/author-pod-coverage.t',
                             't/author-pod-syntax.t',
                             't/release-kwalitee.t',
                             't/release-meta-json.t'
                           ],
          'files_hash' => {
                            'Build.PL' => {
                                            'mtime' => 1709474159,
                                            'requires' => {
                                                            'Module::Build' => '0.30',
                                                            'strict' => '0',
                                                            'warnings' => '0'
                                                          },
                                            'size' => 1275
                                          },
                            'CHANGELOG.md' => {
                                                'mtime' => 1709474159,
                                                'size' => 940
                                              },
                            'Date-Holidays-ES.code-workspace' => {
                                                                   'mtime' => 1709474159,
                                                                   'size' => 633
                                                                 },
                            'INSTALL' => {
                                           'mtime' => 1709474159,
                                           'size' => 2513
                                         },
                            'LICENSE' => {
                                           'mtime' => 1709474159,
                                           'size' => 19741
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1709474159,
                                            'size' => 383
                                          },
                            'META.json' => {
                                             'mtime' => 1709474159,
                                             'size' => 2145
                                           },
                            'META.yml' => {
                                            'mtime' => 1709474159,
                                            'size' => 1024
                                          },
                            'Makefile.PL' => {
                                               'mtime' => 1709474159,
                                               'requires' => {
                                                               'ExtUtils::MakeMaker' => '0',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 1437
                                             },
                            'README' => {
                                          'mtime' => 1709474159,
                                          'size' => 355
                                        },
                            'cpanfile' => {
                                            'mtime' => 1709474159,
                                            'size' => 167
                                          },
                            'dist.ini' => {
                                            'mtime' => 1709474159,
                                            'size' => 2512
                                          },
                            'lib/Date/Holidays/ES.pm' => {
                                                           'license' => 'Perl_5',
                                                           'module' => 'Date::Holidays::ES',
                                                           'mtime' => 1709474159,
                                                           'recommends' => {
                                                                             'DateTime' => '0'
                                                                           },
                                                           'requires' => {
                                                                           'Carp' => '0',
                                                                           'Date::Easter' => '0',
                                                                           'Date::Holidays::Super' => '0',
                                                                           'Time::JulianDay' => '0',
                                                                           'base' => '0',
                                                                           'strict' => '0',
                                                                           'utf8' => '0',
                                                                           'warnings' => '0'
                                                                         },
                                                           'size' => 5090
                                                         },
                            't/00-compile.t' => {
                                                  'mtime' => 1709474159,
                                                  'requires' => {
                                                                  'File::Spec' => '0',
                                                                  'IO::Handle' => '0',
                                                                  'IPC::Open3' => '0',
                                                                  'Test::More' => '0',
                                                                  'perl' => '5.006',
                                                                  'strict' => '0',
                                                                  'warnings' => '0'
                                                                },
                                                  'size' => 1235,
                                                  'suggests' => {
                                                                  'blib' => '1.01'
                                                                }
                                                },
                            't/00.use.t' => {
                                              'mtime' => 1709474159,
                                              'requires' => {
                                                              'Test::More' => '0',
                                                              'strict' => '0',
                                                              'warnings' => '0'
                                                            },
                                              'size' => 136
                                            },
                            't/01.es.t' => {
                                             'mtime' => 1709474159,
                                             'requires' => {
                                                             'Test::More' => '0.88',
                                                             'strict' => '0',
                                                             'warnings' => '0'
                                                           },
                                             'size' => 1173,
                                             'suggests' => {
                                                             'Date::Holidays' => '0'
                                                           }
                                           },
                            't/author-critic.t' => {
                                                     'mtime' => 1709474159,
                                                     'recommends' => {
                                                                       'Test::Perl::Critic' => '0',
                                                                       'strict' => '0',
                                                                       'warnings' => '0'
                                                                     },
                                                     'size' => 263
                                                   },
                            't/author-pod-coverage.t' => {
                                                           'mtime' => 1709474159,
                                                           'recommends' => {
                                                                             'Pod::Coverage::TrustPod' => '0',
                                                                             'Test::Pod::Coverage' => '1.08',
                                                                             'strict' => '0',
                                                                             'warnings' => '0'
                                                                           },
                                                           'size' => 375
                                                         },
                            't/author-pod-syntax.t' => {
                                                         'mtime' => 1709474159,
                                                         'recommends' => {
                                                                           'Test::More' => '0',
                                                                           'Test::Pod' => '1.41',
                                                                           'strict' => '0',
                                                                           'warnings' => '0'
                                                                         },
                                                         'size' => 300
                                                       },
                            't/release-kwalitee.t' => {
                                                        'mtime' => 1709474159,
                                                        'recommends' => {
                                                                          'Test::Kwalitee' => '1.21',
                                                                          'Test::More' => '0.88',
                                                                          'strict' => '0',
                                                                          'warnings' => '0'
                                                                        },
                                                        'size' => 324
                                                      },
                            't/release-meta-json.t' => {
                                                         'mtime' => 1709474159,
                                                         'recommends' => {
                                                                           'Test::CPAN::Meta::JSON' => '0'
                                                                         },
                                                         'size' => 187
                                                       }
                          },
          'got_prereq_from' => 'META.yml',
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 1,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 1,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 32,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 0,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 1,
                          'meta_yml_has_repository_resource' => 1,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 1,
                          'use_warnings' => 1
                        },
          'latest_mtime' => 1709474159,
          'license' => 'perl defined in META.yml defined in LICENSE',
          'license_file' => 'lib/Date/Holidays/ES.pm',
          'license_from_yaml' => 'perl',
          'license_in_pod' => 1,
          'license_type' => 'Perl_5',
          'licenses' => {
                          'Perl_5' => [
                                        'lib/Date/Holidays/ES.pm'
                                      ]
                        },
          'manifest_matches_dist' => 1,
          'meta_json' => {
                           'abstract' => 'Spanish holidays',
                           'author' => [
                                         'Jonas B. <jonasbn@cpan.org>'
                                       ],
                           'dynamic_config' => 0,
                           'generated_by' => 'Dist::Zilla version 6.031, CPAN::Meta::Converter version 2.150010',
                           'license' => [
                                          'perl_5'
                                        ],
                           'meta-spec' => {
                                            'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                            'version' => 2
                                          },
                           'name' => 'Date-Holidays-ES',
                           'prereqs' => {
                                          'build' => {
                                                       'requires' => {
                                                                       'Module::Build' => '0.30',
                                                                       'Test::More' => '0.4234'
                                                                     }
                                                     },
                                          'configure' => {
                                                           'requires' => {
                                                                           'ExtUtils::MakeMaker' => '0',
                                                                           'Module::Build' => '0.30'
                                                                         }
                                                         },
                                          'develop' => {
                                                         'requires' => {
                                                                         'Pod::Coverage::TrustPod' => '0',
                                                                         'Test::CPAN::Meta::JSON' => '0.16',
                                                                         'Test::Kwalitee' => '1.21',
                                                                         'Test::Perl::Critic' => '0',
                                                                         'Test::Pod' => '1.41',
                                                                         'Test::Pod::Coverage' => '1.08'
                                                                       }
                                                       },
                                          'runtime' => {
                                                         'requires' => {
                                                                         'Date::Easter' => '0',
                                                                         'Date::Holidays::Super' => '0',
                                                                         'DateTime' => '0',
                                                                         'Time::JulianDay' => '0'
                                                                       }
                                                       },
                                          'test' => {
                                                      'requires' => {
                                                                      'File::Spec' => '0',
                                                                      'IO::Handle' => '0',
                                                                      'IPC::Open3' => '0',
                                                                      'Test::More' => '0.4234'
                                                                    }
                                                    }
                                        },
                           'provides' => {
                                           'Date::Holidays::ES' => {
                                                                     'file' => 'lib/Date/Holidays/ES.pm',
                                                                     'version' => '0.03'
                                                                   }
                                         },
                           'release_status' => 'stable',
                           'resources' => {
                                            'bugtracker' => {
                                                              'web' => 'https://github.com/jonasbn/Date-Holidays-ES/issues'
                                                            },
                                            'homepage' => 'https://github.com/jonasbn/Date-Holidays-ES',
                                            'repository' => {
                                                              'type' => 'git',
                                                              'url' => 'https://github.com/jonasbn/Date-Holidays-ES.git',
                                                              'web' => 'https://github.com/jonasbn/Date-Holidays-ES'
                                                            }
                                          },
                           'version' => '0.03',
                           'x_generated_by_perl' => 'v5.38.0',
                           'x_serialization_backend' => 'Cpanel::JSON::XS version 4.37',
                           'x_spdx_expression' => 'Artistic-1.0-Perl OR GPL-1.0-or-later'
                         },
          'meta_json_is_parsable' => 1,
          'meta_json_spec_version' => 2,
          'meta_yml' => {
                          'abstract' => 'Spanish holidays',
                          'author' => [
                                        'Jonas B. <jonasbn@cpan.org>'
                                      ],
                          'build_requires' => {
                                                'File::Spec' => '0',
                                                'IO::Handle' => '0',
                                                'IPC::Open3' => '0',
                                                'Module::Build' => '0.30',
                                                'Test::More' => '0.4234'
                                              },
                          'configure_requires' => {
                                                    'ExtUtils::MakeMaker' => '0',
                                                    'Module::Build' => '0.30'
                                                  },
                          'dynamic_config' => '0',
                          'generated_by' => 'Dist::Zilla version 6.031, CPAN::Meta::Converter version 2.150010',
                          'license' => 'perl',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'Date-Holidays-ES',
                          'provides' => {
                                          'Date::Holidays::ES' => {
                                                                    'file' => 'lib/Date/Holidays/ES.pm',
                                                                    'version' => '0.03'
                                                                  }
                                        },
                          'requires' => {
                                          'Date::Easter' => '0',
                                          'Date::Holidays::Super' => '0',
                                          'DateTime' => '0',
                                          'Time::JulianDay' => '0'
                                        },
                          'resources' => {
                                           'bugtracker' => 'https://github.com/jonasbn/Date-Holidays-ES/issues',
                                           'homepage' => 'https://github.com/jonasbn/Date-Holidays-ES',
                                           'repository' => 'https://github.com/jonasbn/Date-Holidays-ES.git'
                                         },
                          'version' => '0.03',
                          'x_generated_by_perl' => 'v5.38.0',
                          'x_serialization_backend' => 'YAML::Tiny version 1.74',
                          'x_spdx_expression' => 'Artistic-1.0-Perl OR GPL-1.0-or-later'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/Date/Holidays/ES.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Date::Holidays::ES'
                         }
                       ],
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/Date-Holidays-ES.tar.gz',
          'prereq' => [
                        {
                          'is_prereq' => 1,
                          'requires' => 'Date::Easter',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Date::Holidays::Super',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'DateTime',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'File::Spec',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'IO::Handle',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'IPC::Open3',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Module::Build',
                          'type' => 'build_requires',
                          'version' => '0.30'
                        },
                        {
                          'requires' => 'Module::Build',
                          'type' => 'configure_requires',
                          'version' => '0.30'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'build_requires',
                          'version' => '0.4234'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Time::JulianDay',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        }
                      ],
          'released' => 1709583461,
          'size_packed' => 14272,
          'size_unpacked' => 42208,
          'test_files' => [
                            't/00-compile.t',
                            't/00.use.t',
                            't/01.es.t',
                            't/author-critic.t',
                            't/author-pod-coverage.t',
                            't/author-pod-syntax.t',
                            't/release-kwalitee.t',
                            't/release-meta-json.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'requires' => {
                                                       'ExtUtils::MakeMaker' => '0',
                                                       'Module::Build' => '0.30',
                                                       'strict' => '0',
                                                       'warnings' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'recommends' => {
                                                       'DateTime' => '0'
                                                     },
                                     'requires' => {
                                                     'Carp' => '0',
                                                     'Date::Easter' => '0',
                                                     'Date::Holidays::Super' => '0',
                                                     'Time::JulianDay' => '0',
                                                     'base' => '0',
                                                     'strict' => '0',
                                                     'utf8' => '0',
                                                     'warnings' => '0'
                                                   }
                                   },
                      'test' => {
                                  'recommends' => {
                                                    'Pod::Coverage::TrustPod' => '0',
                                                    'Test::CPAN::Meta::JSON' => '0',
                                                    'Test::Kwalitee' => '1.21',
                                                    'Test::More' => '0.88',
                                                    'Test::Perl::Critic' => '0',
                                                    'Test::Pod' => '1.41',
                                                    'Test::Pod::Coverage' => '1.08',
                                                    'strict' => '0',
                                                    'warnings' => '0'
                                                  },
                                  'requires' => {
                                                  'File::Spec' => '0',
                                                  'IO::Handle' => '0',
                                                  'IPC::Open3' => '0',
                                                  'Test::More' => '0.88',
                                                  'perl' => '5.006',
                                                  'strict' => '0',
                                                  'warnings' => '0'
                                                },
                                  'suggests' => {
                                                  'Date::Holidays' => '0',
                                                  'blib' => '1.01'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'Date-Holidays-ES'
        };
1;
