$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'Mojolicious::Plugin::WebComponent' => 'An effort to make creating and using custom web components easier'
                                },
          'author' => '',
          'dir_lib' => 'lib',
          'dir_t' => 't',
          'dirs' => 12,
          'dirs_array' => [
                            'lib/Mojolicious/Plugin',
                            'lib/Mojolicious',
                            'lib',
                            't/files/assets/js/components',
                            't/files/assets/js',
                            't/files/assets',
                            't/files',
                            't/templates/components',
                            't/templates/default',
                            't/templates/layouts',
                            't/templates',
                            't'
                          ],
          'dist' => 'Mojolicious-Plugin-WebComponent',
          'dynamic_config' => 0,
          'error' => {
                       'extracts_nicely' => 'expected Mojolicious-Plugin-WebComponent but got Mojolicious-Plugin-WebComponent-0.06'
                     },
          'extension' => 'tar.gz',
          'external_license_file' => 'LICENSE',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_build_pl' => 'Build.PL',
          'file_changelog' => 'Changes',
          'file_cpanfile' => 'cpanfile',
          'file_license' => 'LICENSE',
          'file_manifest' => 'MANIFEST',
          'file_meta_json' => 'META.json',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README.md',
          'files' => 10,
          'files_array' => [
                             'Build.PL',
                             'Changes',
                             'LICENSE',
                             'MANIFEST',
                             'META.json',
                             'META.yml',
                             'README.md',
                             'cpanfile',
                             'lib/Mojolicious/Plugin/WebComponent.pm',
                             'minil.toml'
                           ],
          'files_hash' => {
                            'Build.PL' => {
                                            'mtime' => 1709300874,
                                            'requires' => {
                                                            'Module::Build::Tiny' => '0.035',
                                                            'perl' => '5.008_001',
                                                            'strict' => '0'
                                                          },
                                            'size' => 301
                                          },
                            'Changes' => {
                                           'mtime' => 1709300874,
                                           'size' => 748
                                         },
                            'LICENSE' => {
                                           'mtime' => 1709300874,
                                           'size' => 18418
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1709300874,
                                            'size' => 340
                                          },
                            'META.json' => {
                                             'mtime' => 1709300874,
                                             'size' => 1979
                                           },
                            'META.yml' => {
                                            'mtime' => 1709300874,
                                            'size' => 1113
                                          },
                            'README.md' => {
                                             'mtime' => 1709300874,
                                             'size' => 12136
                                           },
                            'cpanfile' => {
                                            'mtime' => 1709300874,
                                            'size' => 25
                                          },
                            'lib/Mojolicious/Plugin/WebComponent.pm' => {
                                                                          'license' => 'Perl_5',
                                                                          'module' => 'Mojolicious::Plugin::WebComponent',
                                                                          'mtime' => 1709300874,
                                                                          'requires' => {
                                                                                          'Carp' => '0',
                                                                                          'Data::Dumper' => '0',
                                                                                          'Mojo::Base' => '0',
                                                                                          'Mojolicious::Plugin' => '0',
                                                                                          'perl' => 'v5.20.0'
                                                                                        },
                                                                          'size' => 15774
                                                                        },
                            'minil.toml' => {
                                              'mtime' => 1709300874,
                                              'size' => 134
                                            },
                            't/00_compile.t' => {
                                                  'mtime' => 1709300874,
                                                  'no_index' => 1,
                                                  'requires' => {
                                                                  'Test::More' => '0.88',
                                                                  'strict' => '0',
                                                                  'warnings' => '0'
                                                                },
                                                  'size' => 179
                                                },
                            't/files/assets/js/components/my-component.js' => {
                                                                                'mtime' => 1709300874,
                                                                                'no_index' => 1,
                                                                                'size' => 1051
                                                                              },
                            't/pod-coverage.t' => {
                                                    'mtime' => 1709300874,
                                                    'no_index' => 1,
                                                    'requires' => {
                                                                    'Test::More' => '0',
                                                                    'strict' => '0',
                                                                    'warnings' => '0'
                                                                  },
                                                    'size' => 646,
                                                    'suggests' => {
                                                                    'Pod::Coverage' => '0',
                                                                    'Test::Pod::Coverage' => '0'
                                                                  }
                                                  },
                            't/pod.t' => {
                                           'mtime' => 1709300874,
                                           'no_index' => 1,
                                           'requires' => {
                                                           'Test::More' => '0',
                                                           'strict' => '0',
                                                           'warnings' => '0'
                                                         },
                                           'size' => 324,
                                           'suggests' => {
                                                           'Test::Pod' => '0'
                                                         }
                                         },
                            't/register.t' => {
                                                'mtime' => 1709300874,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'Mojolicious::Lite' => '0',
                                                                'Mojolicious::Plugin::WebComponent' => '0',
                                                                'Test::Mojo' => '0',
                                                                'Test::More' => '0.88'
                                                              },
                                                'size' => 786
                                              },
                            't/templates/components/my-component.html.ep' => {
                                                                               'mtime' => 1709300874,
                                                                               'no_index' => 1,
                                                                               'size' => 87
                                                                             },
                            't/templates/default/index.html.ep' => {
                                                                     'mtime' => 1709300874,
                                                                     'no_index' => 1,
                                                                     'size' => 129
                                                                   },
                            't/templates/layouts/default_layout.html.ep' => {
                                                                              'mtime' => 1709300874,
                                                                              'no_index' => 1,
                                                                              'size' => 376
                                                                            }
                          },
          'got_prereq_from' => 'META.yml',
          'ignored_files_array' => [
                                     't/00_compile.t',
                                     't/files/assets/js/components/my-component.js',
                                     't/pod-coverage.t',
                                     't/pod.t',
                                     't/register.t',
                                     't/templates/components/my-component.html.ep',
                                     't/templates/default/index.html.ep',
                                     't/templates/layouts/default_layout.html.ep'
                                   ],
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 1,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 1,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 33,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 1,
                          'meta_yml_has_repository_resource' => 1,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 1,
                          'use_warnings' => 1
                        },
          'latest_mtime' => 1709300874,
          'license' => 'perl defined in META.yml defined in LICENSE',
          'license_file' => 'lib/Mojolicious/Plugin/WebComponent.pm',
          'license_from_yaml' => 'perl',
          'license_in_pod' => 1,
          'license_type' => 'Perl_5',
          'licenses' => {
                          'Perl_5' => [
                                        'lib/Mojolicious/Plugin/WebComponent.pm'
                                      ]
                        },
          'manifest_matches_dist' => 1,
          'meta_json' => {
                           'abstract' => 'An effort to make creating and using custom web components easier',
                           'author' => [
                                         'Russell Shingleton <reshingleton@gmail.com>'
                                       ],
                           'dynamic_config' => 0,
                           'generated_by' => 'Minilla/v3.1.23',
                           'license' => [
                                          'perl_5'
                                        ],
                           'meta-spec' => {
                                            'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                            'version' => '2'
                                          },
                           'name' => 'Mojolicious-Plugin-WebComponent',
                           'no_index' => {
                                           'directory' => [
                                                            't',
                                                            'xt',
                                                            'inc',
                                                            'share',
                                                            'eg',
                                                            'examples',
                                                            'author',
                                                            'builder'
                                                          ]
                                         },
                           'prereqs' => {
                                          'configure' => {
                                                           'requires' => {
                                                                           'Module::Build::Tiny' => '0.035'
                                                                         }
                                                         },
                                          'develop' => {
                                                         'requires' => {
                                                                         'Test::CPAN::Meta' => '0',
                                                                         'Test::MinimumVersion::Fast' => '0.04',
                                                                         'Test::PAUSE::Permissions' => '0.07',
                                                                         'Test::Pod' => '1.41',
                                                                         'Test::Spellunker' => 'v0.2.7'
                                                                       }
                                                       },
                                          'runtime' => {
                                                         'requires' => {
                                                                         'Mojolicious' => '0',
                                                                         'perl' => 'v5.20.0'
                                                                       }
                                                       }
                                        },
                           'provides' => {
                                           'Mojolicious::Plugin::WebComponent' => {
                                                                                    'file' => 'lib/Mojolicious/Plugin/WebComponent.pm',
                                                                                    'version' => '0.06'
                                                                                  }
                                         },
                           'release_status' => 'stable',
                           'resources' => {
                                            'bugtracker' => {
                                                              'web' => 'https://github.com/rshingleton/Mojolicious-Plugin-WebComponent/issues'
                                                            },
                                            'homepage' => 'https://github.com/rshingleton/Mojolicious-Plugin-WebComponent',
                                            'repository' => {
                                                              'type' => 'git',
                                                              'url' => 'https://github.com/rshingleton/Mojolicious-Plugin-WebComponent.git',
                                                              'web' => 'https://github.com/rshingleton/Mojolicious-Plugin-WebComponent'
                                                            }
                                          },
                           'version' => '0.06',
                           'x_contributors' => [
                                                 'shingler <shingler@oclc.org>'
                                               ],
                           'x_serialization_backend' => 'JSON::PP version 4.07',
                           'x_static_install' => 1
                         },
          'meta_json_is_parsable' => 1,
          'meta_json_spec_version' => 2,
          'meta_yml' => {
                          'abstract' => 'An effort to make creating and using custom web components easier',
                          'author' => [
                                        'Russell Shingleton <reshingleton@gmail.com>'
                                      ],
                          'build_requires' => {},
                          'configure_requires' => {
                                                    'Module::Build::Tiny' => '0.035'
                                                  },
                          'dynamic_config' => '0',
                          'generated_by' => 'Minilla/v3.1.23, CPAN::Meta::Converter version 2.150010',
                          'license' => 'perl',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'Mojolicious-Plugin-WebComponent',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'share',
                                                           'eg',
                                                           'examples',
                                                           'author',
                                                           'builder'
                                                         ]
                                        },
                          'provides' => {
                                          'Mojolicious::Plugin::WebComponent' => {
                                                                                   'file' => 'lib/Mojolicious/Plugin/WebComponent.pm',
                                                                                   'version' => '0.06'
                                                                                 }
                                        },
                          'requires' => {
                                          'Mojolicious' => '0',
                                          'perl' => 'v5.20.0'
                                        },
                          'resources' => {
                                           'bugtracker' => 'https://github.com/rshingleton/Mojolicious-Plugin-WebComponent/issues',
                                           'homepage' => 'https://github.com/rshingleton/Mojolicious-Plugin-WebComponent',
                                           'repository' => 'https://github.com/rshingleton/Mojolicious-Plugin-WebComponent.git'
                                         },
                          'version' => '0.06',
                          'x_contributors' => [
                                                'shingler <shingler@oclc.org>'
                                              ],
                          'x_serialization_backend' => 'CPAN::Meta::YAML version 0.018',
                          'x_static_install' => '1'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/Mojolicious/Plugin/WebComponent.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Mojolicious::Plugin::WebComponent'
                         }
                       ],
          'no_index' => '^author/;^builder/;^eg/;^examples/;^inc/;^share/;^t/;^xt/',
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/Mojolicious-Plugin-WebComponent.tar.gz',
          'prereq' => [
                        {
                          'requires' => 'Module::Build::Tiny',
                          'type' => 'configure_requires',
                          'version' => '0.035'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Mojolicious',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => 'v5.20.0'
                        }
                      ],
          'released' => 1709583483,
          'size_packed' => 15223,
          'size_unpacked' => 54546,
          'test_files' => [
                            't/00_compile.t',
                            't/pod-coverage.t',
                            't/pod.t',
                            't/register.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'requires' => {
                                                       'Module::Build::Tiny' => '0.035',
                                                       'perl' => '5.008_001',
                                                       'strict' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'requires' => {
                                                     'Carp' => '0',
                                                     'Data::Dumper' => '0',
                                                     'Mojo::Base' => '0',
                                                     'Mojolicious::Plugin' => '0',
                                                     'perl' => 'v5.20.0'
                                                   }
                                   },
                      'test' => {
                                  'requires' => {
                                                  'Mojolicious::Lite' => '0',
                                                  'Test::Mojo' => '0',
                                                  'Test::More' => '0.88',
                                                  'strict' => '0',
                                                  'warnings' => '0'
                                                },
                                  'suggests' => {
                                                  'Pod::Coverage' => '0',
                                                  'Test::Pod' => '0',
                                                  'Test::Pod::Coverage' => '0'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'Mojolicious-Plugin-WebComponent'
        };
1;
