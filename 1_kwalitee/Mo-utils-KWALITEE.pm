$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'Mo::utils' => 'Mo utilities.'
                                },
          'author' => '',
          'dir_t' => 't',
          'dir_xt' => 'xt',
          'dirs' => 8,
          'dirs_array' => [
                            'examples',
                            'inc/Module/Install',
                            'inc/Module',
                            'inc',
                            't/Mo-utils',
                            't',
                            'xt/Mo-utils',
                            'xt'
                          ],
          'dist' => 'Mo-utils',
          'dynamic_config' => 1,
          'error' => {
                       'extracts_nicely' => 'expected Mo-utils but got Mo-utils-0.23'
                     },
          'extension' => 'tar.gz',
          'external_license_file' => 'LICENSE',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_changelog' => 'Changes',
          'file_license' => 'LICENSE',
          'file_makefile_pl' => 'Makefile.PL',
          'file_manifest' => 'MANIFEST',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README',
          'file_signature' => 'SIGNATURE',
          'files' => 8,
          'files_array' => [
                             'Changes',
                             'LICENSE',
                             'MANIFEST',
                             'META.yml',
                             'Makefile.PL',
                             'README',
                             'SIGNATURE',
                             'utils.pm'
                           ],
          'files_hash' => {
                            'Changes' => {
                                           'mtime' => 1709549552,
                                           'size' => 3303
                                         },
                            'LICENSE' => {
                                           'mtime' => 1704553041,
                                           'size' => 1304
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1709549280,
                                            'size' => 2010
                                          },
                            'META.yml' => {
                                            'mtime' => 1709549284,
                                            'size' => 923
                                          },
                            'Makefile.PL' => {
                                               'mtime' => 1707690567,
                                               'requires' => {
                                                               'inc::Module::Install' => '0',
                                                               'lib' => '0',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 1102
                                             },
                            'README' => {
                                          'mtime' => 1709549284,
                                          'size' => 19724
                                        },
                            'SIGNATURE' => {
                                             'mtime' => 1709549555,
                                             'size' => 8217
                                           },
                            'examples/check_angle_fail.pl' => {
                                                                'mtime' => 1709549277,
                                                                'no_index' => 1,
                                                                'size' => 312
                                                              },
                            'examples/check_angle_ok.pl' => {
                                                              'mtime' => 1709549277,
                                                              'no_index' => 1,
                                                              'size' => 189
                                                            },
                            'examples/check_array_fail.pl' => {
                                                                'mtime' => 1709549277,
                                                                'no_index' => 1,
                                                                'size' => 295
                                                              },
                            'examples/check_array_object_fail.pl' => {
                                                                       'mtime' => 1709549277,
                                                                       'no_index' => 1,
                                                                       'size' => 373
                                                                     },
                            'examples/check_array_object_ok.pl' => {
                                                                     'mtime' => 1709549277,
                                                                     'no_index' => 1,
                                                                     'size' => 300
                                                                   },
                            'examples/check_array_ok.pl' => {
                                                              'mtime' => 1709549277,
                                                              'no_index' => 1,
                                                              'size' => 192
                                                            },
                            'examples/check_array_required_fail.pl' => {
                                                                         'mtime' => 1709549277,
                                                                         'no_index' => 1,
                                                                         'size' => 333
                                                                       },
                            'examples/check_array_required_ok.pl' => {
                                                                       'mtime' => 1709549277,
                                                                       'no_index' => 1,
                                                                       'size' => 212
                                                                     },
                            'examples/check_bool_fail.pl' => {
                                                               'mtime' => 1709549277,
                                                               'no_index' => 1,
                                                               'size' => 298
                                                             },
                            'examples/check_bool_ok.pl' => {
                                                             'mtime' => 1709549277,
                                                             'no_index' => 1,
                                                             'size' => 206
                                                           },
                            'examples/check_code_fail.pl' => {
                                                               'mtime' => 1709549277,
                                                               'no_index' => 1,
                                                               'size' => 292
                                                             },
                            'examples/check_code_ok.pl' => {
                                                             'mtime' => 1709549277,
                                                             'no_index' => 1,
                                                             'size' => 211
                                                           },
                            'examples/check_isa_fail.pl' => {
                                                              'mtime' => 1709549277,
                                                              'no_index' => 1,
                                                              'size' => 315
                                                            },
                            'examples/check_isa_ok.pl' => {
                                                            'mtime' => 1709549277,
                                                            'no_index' => 1,
                                                            'size' => 244
                                                          },
                            'examples/check_length_fail.pl' => {
                                                                 'mtime' => 1709549277,
                                                                 'no_index' => 1,
                                                                 'size' => 296
                                                               },
                            'examples/check_length_fix_fail.pl' => {
                                                                     'mtime' => 1709549277,
                                                                     'no_index' => 1,
                                                                     'size' => 306
                                                                   },
                            'examples/check_length_fix_ok.pl' => {
                                                                   'mtime' => 1709549277,
                                                                   'no_index' => 1,
                                                                   'size' => 239
                                                                 },
                            'examples/check_length_ok.pl' => {
                                                               'mtime' => 1709549277,
                                                               'no_index' => 1,
                                                               'size' => 231
                                                             },
                            'examples/check_number_fail.pl' => {
                                                                 'mtime' => 1709549277,
                                                                 'no_index' => 1,
                                                                 'size' => 282
                                                               },
                            'examples/check_number_of_items_fail.pl' => {
                                                                          'mtime' => 1709549277,
                                                                          'no_index' => 1,
                                                                          'size' => 750
                                                                        },
                            'examples/check_number_of_items_ok.pl' => {
                                                                        'mtime' => 1709549277,
                                                                        'no_index' => 1,
                                                                        'size' => 686
                                                                      },
                            'examples/check_number_ok.pl' => {
                                                               'mtime' => 1709549277,
                                                               'no_index' => 1,
                                                               'size' => 191
                                                             },
                            'examples/check_number_range_fail.pl' => {
                                                                       'mtime' => 1709549295,
                                                                       'no_index' => 1,
                                                                       'size' => 317
                                                                     },
                            'examples/check_number_range_ok.pl' => {
                                                                     'mtime' => 1709549277,
                                                                     'no_index' => 1,
                                                                     'size' => 210
                                                                   },
                            'examples/check_regexp_fail.pl' => {
                                                                 'mtime' => 1709549278,
                                                                 'no_index' => 1,
                                                                 'size' => 382
                                                               },
                            'examples/check_regexp_ok.pl' => {
                                                               'mtime' => 1709549278,
                                                               'no_index' => 1,
                                                               'size' => 242
                                                             },
                            'examples/check_required_fail.pl' => {
                                                                   'mtime' => 1709549278,
                                                                   'no_index' => 1,
                                                                   'size' => 291
                                                                 },
                            'examples/check_required_ok.pl' => {
                                                                 'mtime' => 1709549278,
                                                                 'no_index' => 1,
                                                                 'size' => 191
                                                               },
                            'examples/check_string_begin_fail.pl' => {
                                                                       'mtime' => 1709549278,
                                                                       'no_index' => 1,
                                                                       'size' => 368
                                                                     },
                            'examples/check_string_begin_ok.pl' => {
                                                                     'mtime' => 1709549278,
                                                                     'no_index' => 1,
                                                                     'size' => 246
                                                                   },
                            'examples/check_strings_fail.pl' => {
                                                                  'mtime' => 1709549278,
                                                                  'no_index' => 1,
                                                                  'size' => 333
                                                                },
                            'examples/check_strings_ok.pl' => {
                                                                'mtime' => 1709549278,
                                                                'no_index' => 1,
                                                                'size' => 214
                                                              },
                            'inc/Module/Install.pm' => {
                                                         'mtime' => 1709549283,
                                                         'no_index' => 1,
                                                         'size' => 11877
                                                       },
                            'inc/Module/Install/AuthorRequires.pm' => {
                                                                        'mtime' => 1709549284,
                                                                        'no_index' => 1,
                                                                        'size' => 601
                                                                      },
                            'inc/Module/Install/AuthorTests.pm' => {
                                                                     'mtime' => 1709549284,
                                                                     'no_index' => 1,
                                                                     'size' => 1165
                                                                   },
                            'inc/Module/Install/Base.pm' => {
                                                              'mtime' => 1709549284,
                                                              'no_index' => 1,
                                                              'size' => 1127
                                                            },
                            'inc/Module/Install/Can.pm' => {
                                                             'mtime' => 1709549284,
                                                             'no_index' => 1,
                                                             'size' => 3333
                                                           },
                            'inc/Module/Install/Fetch.pm' => {
                                                               'mtime' => 1709549284,
                                                               'no_index' => 1,
                                                               'size' => 2455
                                                             },
                            'inc/Module/Install/Makefile.pm' => {
                                                                  'mtime' => 1709549284,
                                                                  'no_index' => 1,
                                                                  'size' => 12063
                                                                },
                            'inc/Module/Install/Metadata.pm' => {
                                                                  'mtime' => 1709549284,
                                                                  'no_index' => 1,
                                                                  'size' => 18207
                                                                },
                            'inc/Module/Install/ReadmeFromPod.pm' => {
                                                                       'mtime' => 1709549284,
                                                                       'no_index' => 1,
                                                                       'size' => 4212
                                                                     },
                            'inc/Module/Install/Win32.pm' => {
                                                               'mtime' => 1709549284,
                                                               'no_index' => 1,
                                                               'size' => 1795
                                                             },
                            'inc/Module/Install/WriteAll.pm' => {
                                                                  'mtime' => 1709549284,
                                                                  'no_index' => 1,
                                                                  'size' => 1278
                                                                },
                            't/Mo-utils/01-use.t' => {
                                                       'mtime' => 1607364903,
                                                       'no_index' => 1,
                                                       'requires' => {
                                                                       'Test::More' => '0',
                                                                       'Test::NoWarnings' => '0',
                                                                       'strict' => '0',
                                                                       'warnings' => '0'
                                                                     },
                                                       'size' => 155
                                                     },
                            't/Mo-utils/02-version.t' => {
                                                           'mtime' => 1707690568,
                                                           'no_index' => 1,
                                                           'requires' => {
                                                                           'Mo::utils' => '0',
                                                                           'Test::More' => '0',
                                                                           'Test::NoWarnings' => '0',
                                                                           'strict' => '0',
                                                                           'warnings' => '0'
                                                                         },
                                                           'size' => 145
                                                         },
                            't/Mo-utils/03-check_angle.t' => {
                                                               'mtime' => 1706951058,
                                                               'no_index' => 1,
                                                               'requires' => {
                                                                               'English' => '0',
                                                                               'Error::Pure::Utils' => '0',
                                                                               'Mo::utils' => '0',
                                                                               'Test::More' => '0',
                                                                               'Test::NoWarnings' => '0',
                                                                               'strict' => '0',
                                                                               'warnings' => '0'
                                                                             },
                                                               'size' => 1818
                                                             },
                            't/Mo-utils/04-check_array.t' => {
                                                               'mtime' => 1706950901,
                                                               'no_index' => 1,
                                                               'requires' => {
                                                                               'English' => '0',
                                                                               'Error::Pure::Utils' => '0',
                                                                               'Mo::utils' => '0',
                                                                               'Test::MockObject' => '0',
                                                                               'Test::More' => '0',
                                                                               'Test::NoWarnings' => '0',
                                                                               'strict' => '0',
                                                                               'warnings' => '0'
                                                                             },
                                                               'size' => 1118
                                                             },
                            't/Mo-utils/05-check_array_object.t' => {
                                                                      'mtime' => 1706957185,
                                                                      'no_index' => 1,
                                                                      'requires' => {
                                                                                      'English' => '0',
                                                                                      'Error::Pure::Utils' => '0',
                                                                                      'Mo::utils' => '0',
                                                                                      'Test::MockObject' => '0',
                                                                                      'Test::More' => '0',
                                                                                      'Test::NoWarnings' => '0',
                                                                                      'strict' => '0',
                                                                                      'warnings' => '0'
                                                                                    },
                                                                      'size' => 1651
                                                                    },
                            't/Mo-utils/06-check_array_required.t' => {
                                                                        'mtime' => 1699788026,
                                                                        'no_index' => 1,
                                                                        'requires' => {
                                                                                        'English' => '0',
                                                                                        'Error::Pure::Utils' => '0',
                                                                                        'Mo::utils' => '0',
                                                                                        'Test::More' => '0',
                                                                                        'Test::NoWarnings' => '0',
                                                                                        'strict' => '0',
                                                                                        'warnings' => '0'
                                                                                      },
                                                                        'size' => 1060
                                                                      },
                            't/Mo-utils/07-check_bool.t' => {
                                                              'mtime' => 1699788026,
                                                              'no_index' => 1,
                                                              'requires' => {
                                                                              'English' => '0',
                                                                              'Error::Pure::Utils' => '0',
                                                                              'Mo::utils' => '0',
                                                                              'Test::More' => '0',
                                                                              'Test::NoWarnings' => '0',
                                                                              'strict' => '0',
                                                                              'warnings' => '0'
                                                                            },
                                                              'size' => 953
                                                            },
                            't/Mo-utils/08-check_code.t' => {
                                                              'mtime' => 1699788026,
                                                              'no_index' => 1,
                                                              'requires' => {
                                                                              'English' => '0',
                                                                              'Error::Pure::Utils' => '0',
                                                                              'Mo::utils' => '0',
                                                                              'Test::More' => '0',
                                                                              'Test::NoWarnings' => '0',
                                                                              'strict' => '0',
                                                                              'warnings' => '0'
                                                                            },
                                                              'size' => 657
                                                            },
                            't/Mo-utils/09-check_isa.t' => {
                                                             'mtime' => 1699788026,
                                                             'no_index' => 1,
                                                             'requires' => {
                                                                             'English' => '0',
                                                                             'Error::Pure::Utils' => '0',
                                                                             'Mo::utils' => '0',
                                                                             'Test::MockObject' => '0',
                                                                             'Test::More' => '0',
                                                                             'Test::NoWarnings' => '0',
                                                                             'strict' => '0',
                                                                             'warnings' => '0'
                                                                           },
                                                             'size' => 1921
                                                           },
                            't/Mo-utils/10-check_length.t' => {
                                                                'mtime' => 1699788026,
                                                                'no_index' => 1,
                                                                'requires' => {
                                                                                'English' => '0',
                                                                                'Error::Pure::Utils' => '0',
                                                                                'Mo::utils' => '0',
                                                                                'Test::More' => '0',
                                                                                'Test::NoWarnings' => '0',
                                                                                'strict' => '0',
                                                                                'warnings' => '0'
                                                                              },
                                                                'size' => 840
                                                              },
                            't/Mo-utils/11-check_length_fix.t' => {
                                                                    'mtime' => 1707690475,
                                                                    'no_index' => 1,
                                                                    'requires' => {
                                                                                    'English' => '0',
                                                                                    'Error::Pure::Utils' => '0',
                                                                                    'Mo::utils' => '0',
                                                                                    'Test::More' => '0',
                                                                                    'Test::NoWarnings' => '0',
                                                                                    'strict' => '0',
                                                                                    'warnings' => '0'
                                                                                  },
                                                                    'size' => 995
                                                                  },
                            't/Mo-utils/12-check_number.t' => {
                                                                'mtime' => 1709548690,
                                                                'no_index' => 1,
                                                                'requires' => {
                                                                                'English' => '0',
                                                                                'Error::Pure::Utils' => '0',
                                                                                'Mo::utils' => '0',
                                                                                'Test::More' => '0',
                                                                                'Test::NoWarnings' => '0',
                                                                                'strict' => '0',
                                                                                'warnings' => '0'
                                                                              },
                                                                'size' => 1131
                                                              },
                            't/Mo-utils/13-check_number_of_items.t' => {
                                                                         'mtime' => 1699788026,
                                                                         'no_index' => 1,
                                                                         'requires' => {
                                                                                         'English' => '0',
                                                                                         'Error::Pure::Utils' => '0',
                                                                                         'Mo::utils' => '0',
                                                                                         'Test::MockObject' => '0',
                                                                                         'Test::More' => '0',
                                                                                         'Test::NoWarnings' => '0',
                                                                                         'strict' => '0',
                                                                                         'warnings' => '0'
                                                                                       },
                                                                         'size' => 1088
                                                                       },
                            't/Mo-utils/14-check_number_range.t' => {
                                                                      'mtime' => 1709549356,
                                                                      'no_index' => 1,
                                                                      'requires' => {
                                                                                      'English' => '0',
                                                                                      'Error::Pure::Utils' => '0',
                                                                                      'Mo::utils' => '0',
                                                                                      'Test::More' => '0',
                                                                                      'Test::NoWarnings' => '0',
                                                                                      'strict' => '0',
                                                                                      'warnings' => '0'
                                                                                    },
                                                                      'size' => 1197
                                                                    },
                            't/Mo-utils/15-check_regexp.t' => {
                                                                'mtime' => 1699788026,
                                                                'no_index' => 1,
                                                                'requires' => {
                                                                                'English' => '0',
                                                                                'Error::Pure::Utils' => '0',
                                                                                'Mo::utils' => '0',
                                                                                'Test::More' => '0',
                                                                                'Test::NoWarnings' => '0',
                                                                                'strict' => '0',
                                                                                'warnings' => '0'
                                                                              },
                                                                'size' => 1018
                                                              },
                            't/Mo-utils/16-check_required.t' => {
                                                                  'mtime' => 1699788026,
                                                                  'no_index' => 1,
                                                                  'requires' => {
                                                                                  'English' => '0',
                                                                                  'Error::Pure::Utils' => '0',
                                                                                  'Mo::utils' => '0',
                                                                                  'Test::More' => '0',
                                                                                  'Test::NoWarnings' => '0',
                                                                                  'strict' => '0',
                                                                                  'warnings' => '0'
                                                                                },
                                                                  'size' => 635
                                                                },
                            't/Mo-utils/17-check_string_begin.t' => {
                                                                      'mtime' => 1699788026,
                                                                      'no_index' => 1,
                                                                      'requires' => {
                                                                                      'English' => '0',
                                                                                      'Error::Pure::Utils' => '0',
                                                                                      'Mo::utils' => '0',
                                                                                      'Test::More' => '0',
                                                                                      'Test::NoWarnings' => '0',
                                                                                      'strict' => '0',
                                                                                      'warnings' => '0'
                                                                                    },
                                                                      'size' => 1010
                                                                    },
                            't/Mo-utils/18-check_strings.t' => {
                                                                 'mtime' => 1699788026,
                                                                 'no_index' => 1,
                                                                 'requires' => {
                                                                                 'English' => '0',
                                                                                 'Error::Pure::Utils' => '0',
                                                                                 'Mo::utils' => '0',
                                                                                 'Test::More' => '0',
                                                                                 'Test::NoWarnings' => '0',
                                                                                 'strict' => '0',
                                                                                 'warnings' => '0'
                                                                               },
                                                                 'size' => 1241
                                                               },
                            'utils.pm' => {
                                            'license' => 'BSD',
                                            'module' => 'Mo::utils',
                                            'mtime' => 1709549541,
                                            'requires' => {
                                                            'Error::Pure' => '0',
                                                            'Exporter' => '0',
                                                            'List::Util' => '0',
                                                            'Readonly' => '0',
                                                            'Scalar::Util' => '0',
                                                            'base' => '0',
                                                            'strict' => '0',
                                                            'warnings' => '0'
                                                          },
                                            'size' => 24794
                                          },
                            'xt/Mo-utils/01-pod_coverage.t' => {
                                                                 'mtime' => 1607364903,
                                                                 'no_index' => 1,
                                                                 'size' => 151
                                                               },
                            'xt/Mo-utils/02-pod.t' => {
                                                        'mtime' => 1607364903,
                                                        'no_index' => 1,
                                                        'size' => 320
                                                      }
                          },
          'got_prereq_from' => 'META.yml',
          'ignored_files_array' => [
                                     'examples/check_angle_fail.pl',
                                     'examples/check_angle_ok.pl',
                                     'examples/check_array_fail.pl',
                                     'examples/check_array_object_fail.pl',
                                     'examples/check_array_object_ok.pl',
                                     'examples/check_array_ok.pl',
                                     'examples/check_array_required_fail.pl',
                                     'examples/check_array_required_ok.pl',
                                     'examples/check_bool_fail.pl',
                                     'examples/check_bool_ok.pl',
                                     'examples/check_code_fail.pl',
                                     'examples/check_code_ok.pl',
                                     'examples/check_isa_fail.pl',
                                     'examples/check_isa_ok.pl',
                                     'examples/check_length_fail.pl',
                                     'examples/check_length_fix_fail.pl',
                                     'examples/check_length_fix_ok.pl',
                                     'examples/check_length_ok.pl',
                                     'examples/check_number_fail.pl',
                                     'examples/check_number_of_items_fail.pl',
                                     'examples/check_number_of_items_ok.pl',
                                     'examples/check_number_ok.pl',
                                     'examples/check_number_range_fail.pl',
                                     'examples/check_number_range_ok.pl',
                                     'examples/check_regexp_fail.pl',
                                     'examples/check_regexp_ok.pl',
                                     'examples/check_required_fail.pl',
                                     'examples/check_required_ok.pl',
                                     'examples/check_string_begin_fail.pl',
                                     'examples/check_string_begin_ok.pl',
                                     'examples/check_strings_fail.pl',
                                     'examples/check_strings_ok.pl',
                                     'inc/Module/Install.pm',
                                     'inc/Module/Install/AuthorRequires.pm',
                                     'inc/Module/Install/AuthorTests.pm',
                                     'inc/Module/Install/Base.pm',
                                     'inc/Module/Install/Can.pm',
                                     'inc/Module/Install/Fetch.pm',
                                     'inc/Module/Install/Makefile.pm',
                                     'inc/Module/Install/Metadata.pm',
                                     'inc/Module/Install/ReadmeFromPod.pm',
                                     'inc/Module/Install/Win32.pm',
                                     'inc/Module/Install/WriteAll.pm',
                                     't/Mo-utils/01-use.t',
                                     't/Mo-utils/02-version.t',
                                     't/Mo-utils/03-check_angle.t',
                                     't/Mo-utils/04-check_array.t',
                                     't/Mo-utils/05-check_array_object.t',
                                     't/Mo-utils/06-check_array_required.t',
                                     't/Mo-utils/07-check_bool.t',
                                     't/Mo-utils/08-check_code.t',
                                     't/Mo-utils/09-check_isa.t',
                                     't/Mo-utils/10-check_length.t',
                                     't/Mo-utils/11-check_length_fix.t',
                                     't/Mo-utils/12-check_number.t',
                                     't/Mo-utils/13-check_number_of_items.t',
                                     't/Mo-utils/14-check_number_range.t',
                                     't/Mo-utils/15-check_regexp.t',
                                     't/Mo-utils/16-check_required.t',
                                     't/Mo-utils/17-check_string_begin.t',
                                     't/Mo-utils/18-check_strings.t',
                                     'xt/Mo-utils/01-pod_coverage.t',
                                     'xt/Mo-utils/02-pod.t'
                                   ],
          'included_modules' => [
                                  'Module::Install',
                                  'Module::Install::AuthorRequires',
                                  'Module::Install::AuthorTests',
                                  'Module::Install::Base',
                                  'Module::Install::Can',
                                  'Module::Install::Fetch',
                                  'Module::Install::Makefile',
                                  'Module::Install::Metadata',
                                  'Module::Install::ReadmeFromPod',
                                  'Module::Install::Win32',
                                  'Module::Install::WriteAll'
                                ],
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 0,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 1,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 31,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 0,
                          'meta_yml_has_repository_resource' => 1,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 1,
                          'use_warnings' => 1
                        },
          'latest_mtime' => 1709549555,
          'license' => 'bsd defined in META.yml defined in LICENSE',
          'license_file' => 'utils.pm',
          'license_from_yaml' => 'bsd',
          'license_in_pod' => 1,
          'license_type' => 'BSD',
          'licenses' => {
                          'BSD' => [
                                     'utils.pm'
                                   ]
                        },
          'manifest_matches_dist' => 1,
          'meta_yml' => {
                          'abstract' => 'Mo utilities.',
                          'author' => [
                                        'Michal Josef Spacek <skim@cpan.org>'
                                      ],
                          'build_requires' => {
                                                'English' => '0',
                                                'Error::Pure::Utils' => '0',
                                                'ExtUtils::MakeMaker' => '6.59',
                                                'Test::MockObject' => '0',
                                                'Test::More' => '0',
                                                'Test::NoWarnings' => '0'
                                              },
                          'configure_requires' => {
                                                    'ExtUtils::MakeMaker' => '6.59'
                                                  },
                          'distribution_type' => 'module',
                          'dynamic_config' => '1',
                          'generated_by' => 'Module::Install version 1.21',
                          'license' => 'bsd',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'Mo-utils',
                          'no_index' => {
                                          'directory' => [
                                                           'examples',
                                                           'inc',
                                                           't',
                                                           'xt'
                                                         ]
                                        },
                          'requires' => {
                                          'Error::Pure' => '0.15',
                                          'Exporter' => '0',
                                          'List::Util' => '1.33',
                                          'Readonly' => '0',
                                          'Scalar::Util' => '0',
                                          'perl' => '5.6.2'
                                        },
                          'resources' => {
                                           'bugtracker' => 'https://github.com/michal-josef-spacek/Mo-utils/issues',
                                           'homepage' => 'https://github.com/michal-josef-spacek/Mo-utils',
                                           'license' => 'http://opensource.org/licenses/bsd-license.php',
                                           'repository' => 'git://github.com/michal-josef-spacek/Mo-utils'
                                         },
                          'version' => '0.23'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'module_install' => {
                                'version' => '1.21'
                              },
          'modules' => [
                         {
                           'file' => 'utils.pm',
                           'in_basedir' => 1,
                           'in_lib' => 0,
                           'module' => 'Mo::utils'
                         }
                       ],
          'no_index' => '^examples/;^inc/;^t/;^xt/',
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/Mo-utils.tar.gz',
          'prereq' => [
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'English',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Error::Pure',
                          'type' => 'runtime_requires',
                          'version' => '0.15'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Error::Pure::Utils',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Exporter',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'build_requires',
                          'version' => '6.59'
                        },
                        {
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'configure_requires',
                          'version' => '6.59'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'List::Util',
                          'type' => 'runtime_requires',
                          'version' => '1.33'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Readonly',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Scalar::Util',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::MockObject',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::NoWarnings',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => '5.6.2'
                        }
                      ],
          'released' => 1709583447,
          'size_packed' => 37762,
          'size_unpacked' => 148141,
          'test_files' => [
                            't/Mo-utils/01-use.t',
                            't/Mo-utils/02-version.t',
                            't/Mo-utils/03-check_angle.t',
                            't/Mo-utils/04-check_array.t',
                            't/Mo-utils/05-check_array_object.t',
                            't/Mo-utils/06-check_array_required.t',
                            't/Mo-utils/07-check_bool.t',
                            't/Mo-utils/08-check_code.t',
                            't/Mo-utils/09-check_isa.t',
                            't/Mo-utils/10-check_length.t',
                            't/Mo-utils/11-check_length_fix.t',
                            't/Mo-utils/12-check_number.t',
                            't/Mo-utils/13-check_number_of_items.t',
                            't/Mo-utils/14-check_number_range.t',
                            't/Mo-utils/15-check_regexp.t',
                            't/Mo-utils/16-check_required.t',
                            't/Mo-utils/17-check_string_begin.t',
                            't/Mo-utils/18-check_strings.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'requires' => {
                                                       'lib' => '0',
                                                       'strict' => '0',
                                                       'warnings' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'requires' => {
                                                     'Error::Pure' => '0',
                                                     'Exporter' => '0',
                                                     'List::Util' => '0',
                                                     'Readonly' => '0',
                                                     'Scalar::Util' => '0',
                                                     'base' => '0',
                                                     'strict' => '0',
                                                     'warnings' => '0'
                                                   }
                                   },
                      'test' => {
                                  'requires' => {
                                                  'English' => '0',
                                                  'Error::Pure::Utils' => '0',
                                                  'Test::MockObject' => '0',
                                                  'Test::More' => '0',
                                                  'Test::NoWarnings' => '0',
                                                  'strict' => '0',
                                                  'warnings' => '0'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'Mo-utils'
        };
1;
