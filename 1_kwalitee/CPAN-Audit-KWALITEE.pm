$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'CPAN::Audit' => 'Audit CPAN distributions for known vulnerabilities',
                                  'CPAN::Audit::Filter' => 'manage the reports / CVEs to ignore',
                                  'CPAN::Audit::Freshness' => 'check freshness of CPAN::Audit::DB',
                                  'cpan-audit' => 'Audit CPAN modules'
                                },
          'author' => '',
          'dir_lib' => 'lib',
          'dir_t' => 't',
          'dirs' => 19,
          'dirs_array' => [
                            'images',
                            'lib/CPAN/Audit/Discover',
                            'lib/CPAN/Audit',
                            'lib/CPAN',
                            'lib',
                            'script',
                            't/cli',
                            't/data/carton',
                            't/data/cpanfiles',
                            't/data/installed/perl5/lib/perl5',
                            't/data/installed/perl5/lib',
                            't/data/installed/perl5',
                            't/data/installed',
                            't/data/queried_modules',
                            't/data',
                            't/discover',
                            't/lib',
                            't',
                            'util'
                          ],
          'dist' => 'CPAN-Audit',
          'dynamic_config' => 1,
          'error' => {
                       'extracts_nicely' => 'expected CPAN-Audit but got CPAN-Audit-20240302.001',
                       'use_strict' => 'CPAN::Audit::FreshnessCheck',
                       'use_warnings' => 'CPAN::Audit::FreshnessCheck'
                     },
          'extension' => 'tar.gz',
          'external_license_file' => 'LICENSE',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_changelog' => 'Changes',
          'file_cpanfile' => 'cpanfile',
          'file_license' => 'LICENSE',
          'file_makefile_pl' => 'Makefile.PL',
          'file_manifest' => 'MANIFEST',
          'file_manifest_skip' => 'MANIFEST.SKIP',
          'file_meta_json' => 'META.json',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README.md',
          'files' => 27,
          'files_array' => [
                             'CONTRIBUTING.md',
                             'Changes',
                             'GPG_README.md',
                             'LICENSE',
                             'MANIFEST',
                             'MANIFEST.SKIP',
                             'META.json',
                             'META.yml',
                             'Makefile.PL',
                             'README.md',
                             'cpanfile',
                             'images/bdfoycpanorg-gpg-key-selfie.jpeg.gpg',
                             'images/briandfoy-gpg-key-selfie.jpeg',
                             'images/briandfoypoboxcom-gpg-key-selfie.jpeg.gpg',
                             'lib/CPAN/Audit/DB.pm',
                             'lib/CPAN/Audit/DB.pm.gpg',
                             'lib/CPAN/Audit/Discover/Cpanfile.pm',
                             'lib/CPAN/Audit/Discover/CpanfileSnapshot.pm',
                             'lib/CPAN/Audit/Discover.pm',
                             'lib/CPAN/Audit/Filter.pm',
                             'lib/CPAN/Audit/FreshnessCheck.pm',
                             'lib/CPAN/Audit/Installed.pm',
                             'lib/CPAN/Audit/Query.pm',
                             'lib/CPAN/Audit/Version.pm',
                             'lib/CPAN/Audit.pm',
                             'script/cpan-audit',
                             'util/generate'
                           ],
          'files_hash' => {
                            'CONTRIBUTING.md' => {
                                                   'mtime' => 1709426507,
                                                   'size' => 766
                                                 },
                            'Changes' => {
                                           'mtime' => 1709426507,
                                           'size' => 8134
                                         },
                            'GPG_README.md' => {
                                                 'mtime' => 1709426507,
                                                 'size' => 2740
                                               },
                            'LICENSE' => {
                                           'mtime' => 1709426507,
                                           'size' => 18394
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1709426508,
                                            'size' => 1126
                                          },
                            'MANIFEST.SKIP' => {
                                                 'mtime' => 1709426507,
                                                 'size' => 1211
                                               },
                            'META.json' => {
                                             'mtime' => 1709426508,
                                             'size' => 1866
                                           },
                            'META.yml' => {
                                            'mtime' => 1709426508,
                                            'size' => 1068
                                          },
                            'Makefile.PL' => {
                                               'mtime' => 1709426507,
                                               'noes' => {
                                                           'warnings' => '0'
                                                         },
                                               'recommends' => {
                                                                 'File::Spec' => '0'
                                                               },
                                               'requires' => {
                                                               'File::Spec::Functions' => '0',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 3828,
                                               'suggests' => {
                                                               'Test::Manifest' => '1.21'
                                                             }
                                             },
                            'README.md' => {
                                             'mtime' => 1709426507,
                                             'size' => 3690
                                           },
                            'cpanfile' => {
                                            'mtime' => 1709426507,
                                            'size' => 631
                                          },
                            'images/bdfoycpanorg-gpg-key-selfie.jpeg.gpg' => {
                                                                               'mtime' => 1709426507,
                                                                               'size' => 833
                                                                             },
                            'images/briandfoy-gpg-key-selfie.jpeg' => {
                                                                        'mtime' => 1709426507,
                                                                        'size' => 335314
                                                                      },
                            'images/briandfoypoboxcom-gpg-key-selfie.jpeg.gpg' => {
                                                                                    'mtime' => 1709426507,
                                                                                    'size' => 833
                                                                                  },
                            'lib/CPAN/Audit.pm' => {
                                                     'license' => 'Perl_5',
                                                     'module' => 'CPAN::Audit',
                                                     'mtime' => 1709426507,
                                                     'requires' => {
                                                                     'CPAN::Audit::DB' => '0',
                                                                     'CPAN::Audit::Discover' => '0',
                                                                     'CPAN::Audit::Filter' => '0',
                                                                     'CPAN::Audit::Installed' => '0',
                                                                     'CPAN::Audit::Query' => '0',
                                                                     'CPAN::Audit::Version' => '0',
                                                                     'Carp' => '0',
                                                                     'Module::CoreList' => '0',
                                                                     'perl' => 'v5.10.1',
                                                                     'strict' => '0',
                                                                     'version' => '0',
                                                                     'warnings' => '0'
                                                                   },
                                                     'size' => 7553
                                                   },
                            'lib/CPAN/Audit/DB.pm' => {
                                                        'module' => 'CPAN::Audit::DB',
                                                        'mtime' => 1709426507,
                                                        'requires' => {
                                                                        'strict' => '0',
                                                                        'warnings' => '0'
                                                                      },
                                                        'size' => 5405172
                                                      },
                            'lib/CPAN/Audit/DB.pm.gpg' => {
                                                            'mtime' => 1709426507,
                                                            'size' => 833
                                                          },
                            'lib/CPAN/Audit/Discover.pm' => {
                                                              'module' => 'CPAN::Audit::Discover',
                                                              'mtime' => 1709426507,
                                                              'requires' => {
                                                                              'CPAN::Audit::Discover::Cpanfile' => '0',
                                                                              'CPAN::Audit::Discover::CpanfileSnapshot' => '0',
                                                                              'strict' => '0',
                                                                              'warnings' => '0'
                                                                            },
                                                              'size' => 578
                                                            },
                            'lib/CPAN/Audit/Discover/Cpanfile.pm' => {
                                                                       'module' => 'CPAN::Audit::Discover::Cpanfile',
                                                                       'mtime' => 1709426507,
                                                                       'requires' => {
                                                                                       'Module::CPANfile' => '0',
                                                                                       'strict' => '0',
                                                                                       'warnings' => '0'
                                                                                     },
                                                                       'size' => 752
                                                                     },
                            'lib/CPAN/Audit/Discover/CpanfileSnapshot.pm' => {
                                                                               'module' => 'CPAN::Audit::Discover::CpanfileSnapshot',
                                                                               'mtime' => 1709426507,
                                                                               'requires' => {
                                                                                               'CPAN::DistnameInfo' => '0',
                                                                                               'strict' => '0',
                                                                                               'warnings' => '0'
                                                                                             },
                                                                               'size' => 650
                                                                             },
                            'lib/CPAN/Audit/Filter.pm' => {
                                                            'license' => 'Perl_5',
                                                            'module' => 'CPAN::Audit::Filter',
                                                            'mtime' => 1709426507,
                                                            'requires' => {
                                                                            'perl' => 'v5.10.0',
                                                                            'strict' => '0',
                                                                            'warnings' => '0'
                                                                          },
                                                            'size' => 1962
                                                          },
                            'lib/CPAN/Audit/FreshnessCheck.pm' => {
                                                                    'license' => 'Artistic_2_0',
                                                                    'module' => 'CPAN::Audit::FreshnessCheck',
                                                                    'mtime' => 1709426507,
                                                                    'recommends' => {
                                                                                      'CPAN::Audit::DB' => '0',
                                                                                      'Time::Piece' => '0'
                                                                                    },
                                                                    'requires' => {
                                                                                    'perl' => 'v5.10.0'
                                                                                  },
                                                                    'size' => 1809
                                                                  },
                            'lib/CPAN/Audit/Installed.pm' => {
                                                               'module' => 'CPAN::Audit::Installed',
                                                               'mtime' => 1709426507,
                                                               'recommends' => {
                                                                                 'Module::Extract::VERSION' => '0'
                                                                               },
                                                               'requires' => {
                                                                               'Cwd' => '0',
                                                                               'File::Find' => '0',
                                                                               'strict' => '0',
                                                                               'warnings' => '0'
                                                                             },
                                                               'size' => 1687
                                                             },
                            'lib/CPAN/Audit/Query.pm' => {
                                                           'module' => 'CPAN::Audit::Query',
                                                           'mtime' => 1709426507,
                                                           'requires' => {
                                                                           'CPAN::Audit::Version' => '0',
                                                                           'strict' => '0',
                                                                           'warnings' => '0'
                                                                         },
                                                           'size' => 1468
                                                         },
                            'lib/CPAN/Audit/Version.pm' => {
                                                             'module' => 'CPAN::Audit::Version',
                                                             'mtime' => 1709426507,
                                                             'noes' => {
                                                                         'warnings' => '0'
                                                                       },
                                                             'requires' => {
                                                                             'strict' => '0',
                                                                             'version' => '0',
                                                                             'warnings' => '0'
                                                                           },
                                                             'size' => 1365
                                                           },
                            'script/cpan-audit' => {
                                                     'license' => 'Perl_5',
                                                     'mtime' => 1709426507,
                                                     'size' => 11515
                                                   },
                            't/cli.t' => {
                                           'mtime' => 1709426507,
                                           'no_index' => 1,
                                           'noes' => {
                                                       'warnings' => '0'
                                                     },
                                           'requires' => {
                                                           'Test::More' => '0.88',
                                                           'TestCommand' => '0',
                                                           'lib' => '0',
                                                           'strict' => '0',
                                                           'warnings' => '0'
                                                         },
                                           'size' => 1595
                                         },
                            't/cli/deps.t' => {
                                                'mtime' => 1709426507,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'Test::More' => '0.88',
                                                                'TestCommand' => '0',
                                                                'lib' => '0',
                                                                'strict' => '0',
                                                                'warnings' => '0'
                                                              },
                                                'size' => 479
                                              },
                            't/cli/installed.t' => {
                                                     'mtime' => 1709426507,
                                                     'no_index' => 1,
                                                     'requires' => {
                                                                     'Test::More' => '0.88',
                                                                     'TestCommand' => '0',
                                                                     'lib' => '0',
                                                                     'strict' => '0',
                                                                     'warnings' => '0'
                                                                   },
                                                     'size' => 313
                                                   },
                            't/cli/module.t' => {
                                                  'mtime' => 1709426507,
                                                  'no_index' => 1,
                                                  'requires' => {
                                                                  'Data::Dumper' => '0',
                                                                  'Test::More' => '0.88',
                                                                  'TestCommand' => '0',
                                                                  'lib' => '0',
                                                                  'strict' => '0',
                                                                  'warnings' => '0'
                                                                },
                                                  'size' => 1966
                                                },
                            't/cli/modules.t' => {
                                                   'mtime' => 1709426507,
                                                   'no_index' => 1,
                                                   'requires' => {
                                                                   'Test::More' => '0.88',
                                                                   'TestCommand' => '0',
                                                                   'lib' => '0',
                                                                   'strict' => '0',
                                                                   'warnings' => '0'
                                                                 },
                                                   'size' => 2847
                                                 },
                            't/cli/release.t' => {
                                                   'mtime' => 1709426507,
                                                   'no_index' => 1,
                                                   'requires' => {
                                                                   'Test::More' => '0.88',
                                                                   'TestCommand' => '0',
                                                                   'lib' => '0',
                                                                   'strict' => '0',
                                                                   'warnings' => '0'
                                                                 },
                                                   'size' => 1431
                                                 },
                            't/cli/show.t' => {
                                                'mtime' => 1709426507,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'Test::More' => '0.88',
                                                                'TestCommand' => '0',
                                                                'lib' => '0',
                                                                'strict' => '0',
                                                                'warnings' => '0'
                                                              },
                                                'size' => 763
                                              },
                            't/data/carton/cpanfile.snapshot' => {
                                                                   'mtime' => 1709426507,
                                                                   'no_index' => 1,
                                                                   'size' => 626
                                                                 },
                            't/data/cpanfiles/cpanfile' => {
                                                             'mtime' => 1709426507,
                                                             'no_index' => 1,
                                                             'size' => 54
                                                           },
                            't/data/excludes' => {
                                                   'mtime' => 1709426507,
                                                   'no_index' => 1,
                                                   'size' => 158
                                                 },
                            't/data/installed/perl5/lib/perl5/Catalyst.pm' => {
                                                                                'mtime' => 1709426507,
                                                                                'no_index' => 1,
                                                                                'size' => 100
                                                                              },
                            't/data/modules_excludes' => {
                                                           'mtime' => 1709426507,
                                                           'no_index' => 1,
                                                           'size' => 185
                                                         },
                            't/data/queried_modules/cpanfile' => {
                                                                   'mtime' => 1709426507,
                                                                   'no_index' => 1,
                                                                   'size' => 73
                                                                 },
                            't/discover/cpanfile.t' => {
                                                         'mtime' => 1709426507,
                                                         'no_index' => 1,
                                                         'requires' => {
                                                                         'CPAN::Audit::Discover::Cpanfile' => '0',
                                                                         'Test::More' => '0.88',
                                                                         'strict' => '0',
                                                                         'warnings' => '0'
                                                                       },
                                                         'size' => 374
                                                       },
                            't/discover/cpanfile_snapshot.t' => {
                                                                  'mtime' => 1709426507,
                                                                  'no_index' => 1,
                                                                  'requires' => {
                                                                                  'CPAN::Audit::Discover::CpanfileSnapshot' => '0',
                                                                                  'Test::More' => '0.88',
                                                                                  'strict' => '0',
                                                                                  'warnings' => '0'
                                                                                },
                                                                  'size' => 512
                                                                },
                            't/excludes.t' => {
                                                'mtime' => 1709426507,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'Test::More' => '0.88',
                                                                'perl' => 'v5.10.0'
                                                              },
                                                'size' => 1346
                                              },
                            't/installed.t' => {
                                                 'mtime' => 1709426507,
                                                 'no_index' => 1,
                                                 'requires' => {
                                                                 'CPAN::Audit::Installed' => '0',
                                                                 'Test::More' => '0.88',
                                                                 'strict' => '0',
                                                                 'warnings' => '0'
                                                               },
                                                 'size' => 767
                                               },
                            't/json.t' => {
                                            'mtime' => 1709426507,
                                            'no_index' => 1,
                                            'noes' => {
                                                        'warnings' => '0'
                                                      },
                                            'requires' => {
                                                            'CPAN::Audit::DB' => '0',
                                                            'Capture::Tiny' => '0',
                                                            'JSON' => '0',
                                                            'Test::More' => '0.88',
                                                            'lib' => '0',
                                                            'strict' => '0',
                                                            'warnings' => '0'
                                                          },
                                            'size' => 2607
                                          },
                            't/lib/TestCommand.pm' => {
                                                        'mtime' => 1709426507,
                                                        'no_index' => 1,
                                                        'requires' => {
                                                                        'Capture::Tiny' => '0',
                                                                        'strict' => '0',
                                                                        'warnings' => '0'
                                                                      },
                                                        'size' => 460
                                                      },
                            't/queried_modules.t' => {
                                                       'mtime' => 1709426507,
                                                       'no_index' => 1,
                                                       'noes' => {
                                                                   'warnings' => '0'
                                                                 },
                                                       'requires' => {
                                                                       'CPAN::Audit::DB' => '0',
                                                                       'Capture::Tiny' => '0',
                                                                       'JSON' => '0',
                                                                       'Test::More' => '0.88',
                                                                       'lib' => '0',
                                                                       'strict' => '0',
                                                                       'warnings' => '0'
                                                                     },
                                                       'size' => 4425
                                                     },
                            't/query.t' => {
                                             'mtime' => 1709426507,
                                             'no_index' => 1,
                                             'requires' => {
                                                             'CPAN::Audit::Query' => '0',
                                                             'Test::More' => '0.88',
                                                             'strict' => '0',
                                                             'warnings' => '0'
                                                           },
                                             'size' => 1332
                                           },
                            't/test_manifest' => {
                                                   'mtime' => 1709426507,
                                                   'no_index' => 1,
                                                   'size' => 197
                                                 },
                            't/version.t' => {
                                               'mtime' => 1709426507,
                                               'no_index' => 1,
                                               'requires' => {
                                                               'CPAN::Audit::Version' => '0',
                                                               'Test::More' => '0.88',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 1826
                                             },
                            'util/generate' => {
                                                 'mtime' => 1709426507,
                                                 'size' => 10019
                                               }
                          },
          'got_prereq_from' => 'META.yml',
          'ignored_files_array' => [
                                     't/cli.t',
                                     't/cli/deps.t',
                                     't/cli/installed.t',
                                     't/cli/module.t',
                                     't/cli/modules.t',
                                     't/cli/release.t',
                                     't/cli/show.t',
                                     't/data/carton/cpanfile.snapshot',
                                     't/data/cpanfiles/cpanfile',
                                     't/data/excludes',
                                     't/data/installed/perl5/lib/perl5/Catalyst.pm',
                                     't/data/modules_excludes',
                                     't/data/queried_modules/cpanfile',
                                     't/discover/cpanfile.t',
                                     't/discover/cpanfile_snapshot.t',
                                     't/excludes.t',
                                     't/installed.t',
                                     't/json.t',
                                     't/lib/TestCommand.pm',
                                     't/queried_modules.t',
                                     't/query.t',
                                     't/test_manifest',
                                     't/version.t'
                                   ],
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 1,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 1,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 30,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 0,
                          'meta_yml_has_repository_resource' => 1,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 0,
                          'use_warnings' => 0
                        },
          'latest_mtime' => 1709426508,
          'license' => 'perl defined in META.yml defined in LICENSE',
          'license_from_yaml' => 'perl',
          'license_in_pod' => 1,
          'licenses' => {
                          'Artistic_2_0' => [
                                              'lib/CPAN/Audit/FreshnessCheck.pm'
                                            ],
                          'Perl_5' => [
                                        'lib/CPAN/Audit.pm',
                                        'lib/CPAN/Audit/Filter.pm',
                                        'script/cpan-audit'
                                      ]
                        },
          'manifest_matches_dist' => 1,
          'meta_json' => {
                           'abstract' => 'Audit CPAN distributions for known vulnerabilities',
                           'author' => [
                                         'Viacheslav Tykhanovskyi <viacheslav.t@gmail.com>'
                                       ],
                           'dynamic_config' => 1,
                           'generated_by' => 'ExtUtils::MakeMaker version 7.70, CPAN::Meta::Converter version 2.150010',
                           'license' => [
                                          'perl_5'
                                        ],
                           'meta-spec' => {
                                            'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                            'version' => 2
                                          },
                           'name' => 'CPAN-Audit',
                           'no_index' => {
                                           'directory' => [
                                                            't',
                                                            'inc'
                                                          ]
                                         },
                           'prereqs' => {
                                          'build' => {
                                                       'requires' => {
                                                                       'YAML::Tiny' => '0'
                                                                     }
                                                     },
                                          'configure' => {
                                                           'requires' => {
                                                                           'ExtUtils::MakeMaker' => '6.64',
                                                                           'File::Spec::Functions' => '0'
                                                                         }
                                                         },
                                          'runtime' => {
                                                         'requires' => {
                                                                         'CPAN::DistnameInfo' => '0',
                                                                         'IO::Interactive' => '0',
                                                                         'JSON' => '0',
                                                                         'Module::CPANfile' => '0',
                                                                         'Module::CoreList' => '5.20181020',
                                                                         'Module::Extract::VERSION' => '0',
                                                                         'PerlIO::gzip' => '0',
                                                                         'Pod::Usage' => '1.69',
                                                                         'perl' => '5.010'
                                                                       }
                                                       },
                                          'test' => {
                                                      'requires' => {
                                                                      'Capture::Tiny' => '0',
                                                                      'File::Temp' => '0',
                                                                      'HTTP::Tiny' => '0',
                                                                      'Test::More' => '0.98'
                                                                    }
                                                    }
                                        },
                           'release_status' => 'stable',
                           'resources' => {
                                            'bugtracker' => {
                                                              'web' => 'https://github.com/briandfoy/cpan-audit/issues'
                                                            },
                                            'homepage' => 'https://github.com/briandfoy/cpan-audit',
                                            'repository' => {
                                                              'type' => 'git',
                                                              'url' => 'https://github.com/briandfoy/cpan-audit',
                                                              'web' => 'https://github.com/briandfoy/cpan-audit'
                                                            }
                                          },
                           'version' => '20240302.001',
                           'x_serialization_backend' => 'JSON::PP version 4.16'
                         },
          'meta_json_is_parsable' => 1,
          'meta_json_spec_version' => 2,
          'meta_yml' => {
                          'abstract' => 'Audit CPAN distributions for known vulnerabilities',
                          'author' => [
                                        'Viacheslav Tykhanovskyi <viacheslav.t@gmail.com>'
                                      ],
                          'build_requires' => {
                                                'Capture::Tiny' => '0',
                                                'File::Temp' => '0',
                                                'HTTP::Tiny' => '0',
                                                'Test::More' => '0.98',
                                                'YAML::Tiny' => '0'
                                              },
                          'configure_requires' => {
                                                    'ExtUtils::MakeMaker' => '6.64',
                                                    'File::Spec::Functions' => '0'
                                                  },
                          'dynamic_config' => '1',
                          'generated_by' => 'ExtUtils::MakeMaker version 7.70, CPAN::Meta::Converter version 2.150010',
                          'license' => 'perl',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'CPAN-Audit',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc'
                                                         ]
                                        },
                          'requires' => {
                                          'CPAN::DistnameInfo' => '0',
                                          'IO::Interactive' => '0',
                                          'JSON' => '0',
                                          'Module::CPANfile' => '0',
                                          'Module::CoreList' => '5.20181020',
                                          'Module::Extract::VERSION' => '0',
                                          'PerlIO::gzip' => '0',
                                          'Pod::Usage' => '1.69',
                                          'perl' => '5.010'
                                        },
                          'resources' => {
                                           'bugtracker' => 'https://github.com/briandfoy/cpan-audit/issues',
                                           'homepage' => 'https://github.com/briandfoy/cpan-audit',
                                           'repository' => 'https://github.com/briandfoy/cpan-audit'
                                         },
                          'version' => '20240302.001',
                          'x_serialization_backend' => 'CPAN::Meta::YAML version 0.018'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/CPAN/Audit.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'CPAN::Audit'
                         },
                         {
                           'file' => 'lib/CPAN/Audit/DB.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'CPAN::Audit::DB'
                         },
                         {
                           'file' => 'lib/CPAN/Audit/Discover.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'CPAN::Audit::Discover'
                         },
                         {
                           'file' => 'lib/CPAN/Audit/Discover/Cpanfile.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'CPAN::Audit::Discover::Cpanfile'
                         },
                         {
                           'file' => 'lib/CPAN/Audit/Discover/CpanfileSnapshot.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'CPAN::Audit::Discover::CpanfileSnapshot'
                         },
                         {
                           'file' => 'lib/CPAN/Audit/Filter.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'CPAN::Audit::Filter'
                         },
                         {
                           'file' => 'lib/CPAN/Audit/FreshnessCheck.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'CPAN::Audit::FreshnessCheck'
                         },
                         {
                           'file' => 'lib/CPAN/Audit/Installed.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'CPAN::Audit::Installed'
                         },
                         {
                           'file' => 'lib/CPAN/Audit/Query.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'CPAN::Audit::Query'
                         },
                         {
                           'file' => 'lib/CPAN/Audit/Version.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'CPAN::Audit::Version'
                         }
                       ],
          'no_index' => '^inc/;^t/',
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/CPAN-Audit.tar.gz',
          'prereq' => [
                        {
                          'is_prereq' => 1,
                          'requires' => 'CPAN::DistnameInfo',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Capture::Tiny',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'configure_requires',
                          'version' => '6.64'
                        },
                        {
                          'requires' => 'File::Spec::Functions',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'File::Temp',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'HTTP::Tiny',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'IO::Interactive',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'JSON',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Module::CPANfile',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Module::CoreList',
                          'type' => 'runtime_requires',
                          'version' => '5.20181020'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Module::Extract::VERSION',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'PerlIO::gzip',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Pod::Usage',
                          'type' => 'runtime_requires',
                          'version' => '1.69'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'build_requires',
                          'version' => '0.98'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'YAML::Tiny',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => '5.010'
                        }
                      ],
          'released' => 1709583469,
          'size_packed' => 702493,
          'size_unpacked' => 5850233,
          'test_files' => [
                            't/cli.t',
                            't/cli/deps.t',
                            't/cli/installed.t',
                            't/cli/module.t',
                            't/cli/modules.t',
                            't/cli/release.t',
                            't/cli/show.t',
                            't/discover/cpanfile.t',
                            't/discover/cpanfile_snapshot.t',
                            't/excludes.t',
                            't/installed.t',
                            't/json.t',
                            't/queried_modules.t',
                            't/query.t',
                            't/version.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'noes' => {
                                                   'warnings' => '0'
                                                 },
                                       'recommends' => {
                                                         'File::Spec' => '0'
                                                       },
                                       'requires' => {
                                                       'File::Spec::Functions' => '0',
                                                       'strict' => '0',
                                                       'warnings' => '0'
                                                     },
                                       'suggests' => {
                                                       'Test::Manifest' => '1.21'
                                                     }
                                     },
                      'runtime' => {
                                     'noes' => {
                                                 'warnings' => '0'
                                               },
                                     'recommends' => {
                                                       'Module::Extract::VERSION' => '0',
                                                       'Time::Piece' => '0'
                                                     },
                                     'requires' => {
                                                     'CPAN::DistnameInfo' => '0',
                                                     'Carp' => '0',
                                                     'Cwd' => '0',
                                                     'File::Find' => '0',
                                                     'Module::CPANfile' => '0',
                                                     'Module::CoreList' => '0',
                                                     'perl' => 'v5.10.1',
                                                     'strict' => '0',
                                                     'version' => '0',
                                                     'warnings' => '0'
                                                   }
                                   },
                      'test' => {
                                  'noes' => {
                                              'warnings' => '0'
                                            },
                                  'requires' => {
                                                  'Capture::Tiny' => '0',
                                                  'Data::Dumper' => '0',
                                                  'JSON' => '0',
                                                  'Test::More' => '0.88',
                                                  'lib' => '0',
                                                  'perl' => 'v5.10.0',
                                                  'strict' => '0',
                                                  'warnings' => '0'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'CPAN-Audit'
        };
1;
