$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'Test::Pod::LinkCheck::Lite' => 'Test POD links'
                                },
          'author' => '',
          'dir_lib' => 'lib',
          'dir_t' => 't',
          'dir_xt' => 'xt',
          'dirs' => 23,
          'dirs_array' => [
                            'LICENSES',
                            'eg',
                            'inc/Mock/HTTP',
                            'inc/Mock',
                            'inc/My/Module',
                            'inc/My',
                            'inc',
                            'lib/Test/Pod/LinkCheck',
                            'lib/Test/Pod',
                            'lib/Test',
                            'lib',
                            't/data/_cpan',
                            't/data/_http',
                            't/data/_lib/My/Test',
                            't/data/_lib/My',
                            't/data/_lib',
                            't/data/not_ok',
                            't/data/pod_ok',
                            't/data/script',
                            't/data',
                            't',
                            'xt/author',
                            'xt'
                          ],
          'dist' => 'Test-Pod-LinkCheck-Lite',
          'dynamic_config' => 1,
          'error' => {
                       'extracts_nicely' => 'expected Test-Pod-LinkCheck-Lite but got Test-Pod-LinkCheck-Lite-0.013'
                     },
          'extension' => 'tar.gz',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_build_pl' => 'Build.PL',
          'file_changelog' => 'Changes',
          'file_makefile_pl' => 'Makefile.PL',
          'file_manifest' => 'MANIFEST',
          'file_meta_json' => 'META.json',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README',
          'files' => 13,
          'files_array' => [
                             'Build.PL',
                             'Changes',
                             'LICENSES/Artistic',
                             'LICENSES/Copying',
                             'MANIFEST',
                             'META.json',
                             'META.yml',
                             'Makefile.PL',
                             'README',
                             'eg/README',
                             'eg/manpath.PL',
                             'eg/test-pod-links',
                             'lib/Test/Pod/LinkCheck/Lite.pm'
                           ],
          'files_hash' => {
                            'Build.PL' => {
                                            'mtime' => 1709473077,
                                            'requires' => {
                                                            'Module::Build' => '0',
                                                            'My::Module::Build' => '0',
                                                            'My::Module::Meta' => '0',
                                                            'lib' => '0',
                                                            'perl' => '5.008',
                                                            'strict' => '0',
                                                            'warnings' => '0'
                                                          },
                                            'size' => 1004
                                          },
                            'Changes' => {
                                           'mtime' => 1709473077,
                                           'size' => 3629
                                         },
                            'LICENSES/Artistic' => {
                                                     'mtime' => 1709473077,
                                                     'size' => 6111
                                                   },
                            'LICENSES/Copying' => {
                                                    'mtime' => 1709473077,
                                                    'size' => 12632
                                                  },
                            'MANIFEST' => {
                                            'mtime' => 1709473077,
                                            'size' => 1441
                                          },
                            'META.json' => {
                                             'mtime' => 1709473077,
                                             'size' => 2214
                                           },
                            'META.yml' => {
                                            'mtime' => 1709473077,
                                            'size' => 1266
                                          },
                            'Makefile.PL' => {
                                               'mtime' => 1709473077,
                                               'requires' => {
                                                               'ExtUtils::MakeMaker' => '0',
                                                               'My::Module::Meta' => '0',
                                                               'lib' => '0',
                                                               'perl' => '5.008',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 3145
                                             },
                            'README' => {
                                          'mtime' => 1709473077,
                                          'size' => 1600
                                        },
                            'eg/README' => {
                                             'mtime' => 1709473077,
                                             'size' => 510
                                           },
                            'eg/manpath.PL' => {
                                                 'license' => 'Perl_5',
                                                 'mtime' => 1709473077,
                                                 'noes' => {
                                                             'warnings' => '0'
                                                           },
                                                 'recommends' => {
                                                                   'Data::Dumper' => '0'
                                                                 },
                                                 'requires' => {
                                                                 'Config' => '0',
                                                                 'Cwd' => '0',
                                                                 'File::Glob' => '0',
                                                                 'File::Spec' => '0',
                                                                 'Getopt::Long' => '2.33',
                                                                 'List::Util' => '0',
                                                                 'Pod::Usage' => '0',
                                                                 'constant' => '0',
                                                                 'perl' => '5.008',
                                                                 'strict' => '0',
                                                                 'warnings' => '0'
                                                               },
                                                 'size' => 10870
                                               },
                            'eg/test-pod-links' => {
                                                     'mtime' => 1709473077,
                                                     'size' => 4515
                                                   },
                            'inc/Mock/HTTP/Tiny.pm' => {
                                                         'mtime' => 1709473077,
                                                         'no_index' => 1,
                                                         'size' => 5669
                                                       },
                            'inc/My/Module/Build.pm' => {
                                                          'mtime' => 1709473077,
                                                          'no_index' => 1,
                                                          'size' => 4337
                                                        },
                            'inc/My/Module/Meta.pm' => {
                                                         'mtime' => 1709473077,
                                                         'no_index' => 1,
                                                         'size' => 8419
                                                       },
                            'lib/Test/Pod/LinkCheck/Lite.pm' => {
                                                                  'license' => 'Perl_5',
                                                                  'module' => 'Test::Pod::LinkCheck::Lite',
                                                                  'mtime' => 1709473077,
                                                                  'requires' => {
                                                                                  'B::Keywords' => '0',
                                                                                  'Carp' => '0',
                                                                                  'Exporter' => '0',
                                                                                  'File::Find' => '0',
                                                                                  'File::Spec' => '0',
                                                                                  'HTTP::Tiny' => '0',
                                                                                  'IPC::Cmd' => '0',
                                                                                  'Module::Load::Conditional' => '0',
                                                                                  'Pod::Perldoc' => '0',
                                                                                  'Pod::Simple' => '0',
                                                                                  'Pod::Simple::LinkSection' => '0',
                                                                                  'Pod::Simple::PullParser' => '0',
                                                                                  'Storable' => '0',
                                                                                  'Test::Builder' => '0',
                                                                                  'constant' => '0',
                                                                                  'perl' => '5.008',
                                                                                  'strict' => '0',
                                                                                  'utf8' => '0',
                                                                                  'warnings' => '0'
                                                                                },
                                                                  'size' => 47483
                                                                },
                            't/basic.t' => {
                                             'mtime' => 1709473077,
                                             'no_index' => 1,
                                             'requires' => {
                                                             'Test::More' => '0.88',
                                                             'strict' => '0',
                                                             'warnings' => '0'
                                                           },
                                             'size' => 258
                                           },
                            't/data/_cpan/Metadata' => {
                                                         'mtime' => 1709473077,
                                                         'no_index' => 1,
                                                         'size' => 135
                                                       },
                            't/data/_http/status' => {
                                                       'mtime' => 1709473077,
                                                       'no_index' => 1,
                                                       'size' => 682
                                                     },
                            't/data/_lib/My/Test/Module.pm' => {
                                                                 'mtime' => 1709473077,
                                                                 'no_index' => 1,
                                                                 'size' => 1091
                                                               },
                            't/data/_lib/mu.pod' => {
                                                      'mtime' => 1709473077,
                                                      'no_index' => 1,
                                                      'size' => 596
                                                    },
                            't/data/not_ok/bug_other_distros.pod' => {
                                                                       'mtime' => 1709473077,
                                                                       'no_index' => 1,
                                                                       'size' => 461
                                                                     },
                            't/data/not_ok/external_installed_bad_section.pod' => {
                                                                                    'mtime' => 1709473077,
                                                                                    'no_index' => 1,
                                                                                    'size' => 160
                                                                                  },
                            't/data/not_ok/external_installed_section.pod' => {
                                                                                'mtime' => 1709473077,
                                                                                'no_index' => 1,
                                                                                'size' => 155
                                                                              },
                            't/data/not_ok/github_error.pod' => {
                                                                  'mtime' => 1709473077,
                                                                  'no_index' => 1,
                                                                  'size' => 174
                                                                },
                            't/data/not_ok/internal_error.pod' => {
                                                                    'mtime' => 1709473077,
                                                                    'no_index' => 1,
                                                                    'size' => 165
                                                                  },
                            't/data/not_ok/link_off_the_map.pod' => {
                                                                      'mtime' => 1709473077,
                                                                      'no_index' => 1,
                                                                      'size' => 265
                                                                    },
                            't/data/not_ok/man_bad.pod' => {
                                                             'mtime' => 1709473077,
                                                             'no_index' => 1,
                                                             'size' => 519
                                                           },
                            't/data/not_ok/redirect.pod' => {
                                                              'mtime' => 1709473077,
                                                              'no_index' => 1,
                                                              'size' => 191
                                                            },
                            't/data/not_ok/redirect_no_path.pod' => {
                                                                      'mtime' => 1709473077,
                                                                      'no_index' => 1,
                                                                      'size' => 309
                                                                    },
                            't/data/not_ok/server_error.pod' => {
                                                                  'mtime' => 1709473077,
                                                                  'no_index' => 1,
                                                                  'size' => 176
                                                                },
                            't/data/pod_ok/bug_leading_format_code.pod' => {
                                                                             'mtime' => 1709473077,
                                                                             'no_index' => 1,
                                                                             'size' => 273
                                                                           },
                            't/data/pod_ok/bug_line_break.pod' => {
                                                                    'mtime' => 1709473077,
                                                                    'no_index' => 1,
                                                                    'size' => 215
                                                                  },
                            't/data/pod_ok/bug_recursion.pod' => {
                                                                   'mtime' => 1709473077,
                                                                   'no_index' => 1,
                                                                   'size' => 214
                                                                 },
                            't/data/pod_ok/empty.pod' => {
                                                           'mtime' => 1709473077,
                                                           'no_index' => 1,
                                                           'size' => 0
                                                         },
                            't/data/pod_ok/external_builtin.pod' => {
                                                                      'mtime' => 1709473077,
                                                                      'no_index' => 1,
                                                                      'size' => 715
                                                                    },
                            't/data/pod_ok/external_installed.pod' => {
                                                                        'mtime' => 1709473077,
                                                                        'no_index' => 1,
                                                                        'size' => 123
                                                                      },
                            't/data/pod_ok/external_installed_pod.pod' => {
                                                                            'mtime' => 1709473077,
                                                                            'no_index' => 1,
                                                                            'size' => 124
                                                                          },
                            't/data/pod_ok/external_uninstalled.pod' => {
                                                                          'mtime' => 1709473077,
                                                                          'no_index' => 1,
                                                                          'size' => 156
                                                                        },
                            't/data/pod_ok/internal.pod' => {
                                                              'mtime' => 1709473077,
                                                              'no_index' => 1,
                                                              'size' => 329
                                                            },
                            't/data/pod_ok/man.pod' => {
                                                         'mtime' => 1709473077,
                                                         'no_index' => 1,
                                                         'size' => 49
                                                       },
                            't/data/pod_ok/no_links.pod' => {
                                                              'mtime' => 1709473077,
                                                              'no_index' => 1,
                                                              'size' => 81
                                                            },
                            't/data/pod_ok/url_links.pod' => {
                                                               'mtime' => 1709473077,
                                                               'no_index' => 1,
                                                               'size' => 150
                                                             },
                            't/data/script/hello' => {
                                                       'mtime' => 1709473077,
                                                       'no_index' => 1,
                                                       'size' => 1149
                                                     },
                            't/pod_file_ok.t' => {
                                                   'mtime' => 1709473077,
                                                   'no_index' => 1,
                                                   'noes' => {
                                                               'warnings' => '0'
                                                             },
                                                   'requires' => {
                                                                   'IPC::Cmd' => '0',
                                                                   'Test::More' => '0.88',
                                                                   'Test::Pod::LinkCheck::Lite' => '0',
                                                                   'constant' => '0',
                                                                   'perl' => '5.008',
                                                                   'strict' => '0',
                                                                   'warnings' => '0'
                                                                 },
                                                   'size' => 12809
                                                 },
                            'xt/author/basic.t' => {
                                                     'mtime' => 1709473077,
                                                     'no_index' => 1,
                                                     'size' => 365
                                                   },
                            'xt/author/changes.t' => {
                                                       'mtime' => 1709473077,
                                                       'no_index' => 1,
                                                       'size' => 386
                                                     },
                            'xt/author/critic.t' => {
                                                      'mtime' => 1709473077,
                                                      'no_index' => 1,
                                                      'size' => 560
                                                    },
                            'xt/author/executable.t' => {
                                                          'mtime' => 1709473077,
                                                          'no_index' => 1,
                                                          'size' => 655
                                                        },
                            'xt/author/kwalitee.t' => {
                                                        'mtime' => 1709473077,
                                                        'no_index' => 1,
                                                        'size' => 346
                                                      },
                            'xt/author/manifest.t' => {
                                                        'mtime' => 1709473077,
                                                        'no_index' => 1,
                                                        'size' => 403
                                                      },
                            'xt/author/minimum_perl.t' => {
                                                            'mtime' => 1709473077,
                                                            'no_index' => 1,
                                                            'size' => 1430
                                                          },
                            'xt/author/no_forbidden_symbols.t' => {
                                                                    'mtime' => 1709473077,
                                                                    'no_index' => 1,
                                                                    'size' => 971
                                                                  },
                            'xt/author/perlcriticrc' => {
                                                          'mtime' => 1709473077,
                                                          'no_index' => 1,
                                                          'size' => 1868
                                                        },
                            'xt/author/perlpod.t' => {
                                                       'mtime' => 1709473077,
                                                       'no_index' => 1,
                                                       'size' => 680
                                                     },
                            'xt/author/pod.t' => {
                                                   'mtime' => 1709473077,
                                                   'no_index' => 1,
                                                   'size' => 325
                                                 },
                            'xt/author/pod_coverage.t' => {
                                                            'mtime' => 1709473077,
                                                            'no_index' => 1,
                                                            'size' => 448
                                                          },
                            'xt/author/pod_links.t' => {
                                                         'mtime' => 1709473077,
                                                         'no_index' => 1,
                                                         'size' => 465
                                                       },
                            'xt/author/pod_spelling.t' => {
                                                            'mtime' => 1709473077,
                                                            'no_index' => 1,
                                                            'size' => 414
                                                          },
                            'xt/author/prereq.t' => {
                                                      'mtime' => 1709473077,
                                                      'no_index' => 1,
                                                      'size' => 332
                                                    }
                          },
          'got_prereq_from' => 'META.yml',
          'ignored_files_array' => [
                                     'inc/Mock/HTTP/Tiny.pm',
                                     'inc/My/Module/Build.pm',
                                     'inc/My/Module/Meta.pm',
                                     't/basic.t',
                                     't/data/_cpan/Metadata',
                                     't/data/_http/status',
                                     't/data/_lib/My/Test/Module.pm',
                                     't/data/_lib/mu.pod',
                                     't/data/not_ok/bug_other_distros.pod',
                                     't/data/not_ok/external_installed_bad_section.pod',
                                     't/data/not_ok/external_installed_section.pod',
                                     't/data/not_ok/github_error.pod',
                                     't/data/not_ok/internal_error.pod',
                                     't/data/not_ok/link_off_the_map.pod',
                                     't/data/not_ok/man_bad.pod',
                                     't/data/not_ok/redirect.pod',
                                     't/data/not_ok/redirect_no_path.pod',
                                     't/data/not_ok/server_error.pod',
                                     't/data/pod_ok/bug_leading_format_code.pod',
                                     't/data/pod_ok/bug_line_break.pod',
                                     't/data/pod_ok/bug_recursion.pod',
                                     't/data/pod_ok/empty.pod',
                                     't/data/pod_ok/external_builtin.pod',
                                     't/data/pod_ok/external_installed.pod',
                                     't/data/pod_ok/external_installed_pod.pod',
                                     't/data/pod_ok/external_uninstalled.pod',
                                     't/data/pod_ok/internal.pod',
                                     't/data/pod_ok/man.pod',
                                     't/data/pod_ok/no_links.pod',
                                     't/data/pod_ok/url_links.pod',
                                     't/data/script/hello',
                                     't/pod_file_ok.t',
                                     'xt/author/basic.t',
                                     'xt/author/changes.t',
                                     'xt/author/critic.t',
                                     'xt/author/executable.t',
                                     'xt/author/kwalitee.t',
                                     'xt/author/manifest.t',
                                     'xt/author/minimum_perl.t',
                                     'xt/author/no_forbidden_symbols.t',
                                     'xt/author/perlcriticrc',
                                     'xt/author/perlpod.t',
                                     'xt/author/pod.t',
                                     'xt/author/pod_coverage.t',
                                     'xt/author/pod_links.t',
                                     'xt/author/pod_spelling.t',
                                     'xt/author/prereq.t'
                                   ],
          'included_modules' => [
                                  'Mock::HTTP::Tiny',
                                  'My::Module::Build',
                                  'My::Module::Meta'
                                ],
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 1,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 0,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 32,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 1,
                          'meta_yml_has_repository_resource' => 1,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 1,
                          'use_warnings' => 1
                        },
          'latest_mtime' => 1709473077,
          'license' => 'perl defined in META.yml',
          'license_file' => 'eg/manpath.PL,lib/Test/Pod/LinkCheck/Lite.pm',
          'license_from_yaml' => 'perl',
          'license_in_pod' => 1,
          'license_type' => 'Perl_5',
          'licenses' => {
                          'Perl_5' => [
                                        'eg/manpath.PL',
                                        'lib/Test/Pod/LinkCheck/Lite.pm'
                                      ]
                        },
          'manifest_matches_dist' => 1,
          'meta_json' => {
                           'abstract' => 'Test POD links',
                           'author' => [
                                         'Thomas R. Wyant, III F<wyant at cpan dot org>'
                                       ],
                           'dynamic_config' => 1,
                           'generated_by' => 'Module::Build version 0.4234',
                           'license' => [
                                          'perl_5'
                                        ],
                           'meta-spec' => {
                                            'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                            'version' => 2
                                          },
                           'name' => 'Test-Pod-LinkCheck-Lite',
                           'no_index' => {
                                           'directory' => [
                                                            'inc',
                                                            't',
                                                            'xt'
                                                          ],
                                           'package' => [
                                                          'My_Parser'
                                                        ]
                                         },
                           'prereqs' => {
                                          'build' => {
                                                       'requires' => {
                                                                       'Test::More' => '0.88'
                                                                     }
                                                     },
                                          'configure' => {
                                                           'requires' => {
                                                                           'lib' => '0',
                                                                           'strict' => '0',
                                                                           'warnings' => '0'
                                                                         }
                                                         },
                                          'runtime' => {
                                                         'requires' => {
                                                                         'B::Keywords' => '0',
                                                                         'Carp' => '0',
                                                                         'Exporter' => '0',
                                                                         'File::Find' => '0',
                                                                         'File::Spec' => '0',
                                                                         'HTTP::Tiny' => '0',
                                                                         'IPC::Cmd' => '0',
                                                                         'Module::Load::Conditional' => '0',
                                                                         'Pod::Perldoc' => '0',
                                                                         'Pod::Simple' => '0',
                                                                         'Pod::Simple::LinkSection' => '0',
                                                                         'Pod::Simple::PullParser' => '0',
                                                                         'Storable' => '0',
                                                                         'Test::Builder' => '0',
                                                                         'constant' => '0',
                                                                         'perl' => '5.008',
                                                                         'strict' => '0',
                                                                         'utf8' => '0',
                                                                         'warnings' => '0'
                                                                       }
                                                       }
                                        },
                           'provides' => {
                                           'Test::Pod::LinkCheck::Lite' => {
                                                                             'file' => 'lib/Test/Pod/LinkCheck/Lite.pm',
                                                                             'version' => '0.013'
                                                                           }
                                         },
                           'release_status' => 'stable',
                           'resources' => {
                                            'bugtracker' => {
                                                              'mailto' => 'wyant@cpan.org',
                                                              'web' => 'https://rt.cpan.org/Public/Dist/Display.html?Name=Test-Pod-LinkCheck-Lite'
                                                            },
                                            'license' => [
                                                           'http://dev.perl.org/licenses/'
                                                         ],
                                            'repository' => {
                                                              'type' => 'git',
                                                              'url' => 'git://github.com/trwyant/perl-Test-Pod-LinkCheck-Lite.git',
                                                              'web' => 'https://github.com/trwyant/perl-Test-Pod-LinkCheck-Lite'
                                                            }
                                          },
                           'version' => '0.013',
                           'x_serialization_backend' => 'JSON::PP version 4.16'
                         },
          'meta_json_is_parsable' => 1,
          'meta_json_spec_version' => 2,
          'meta_yml' => {
                          'abstract' => 'Test POD links',
                          'author' => [
                                        'Thomas R. Wyant, III F<wyant at cpan dot org>'
                                      ],
                          'build_requires' => {
                                                'Test::More' => '0.88'
                                              },
                          'configure_requires' => {
                                                    'lib' => '0',
                                                    'strict' => '0',
                                                    'warnings' => '0'
                                                  },
                          'dynamic_config' => '1',
                          'generated_by' => 'Module::Build version 0.4234, CPAN::Meta::Converter version 2.150010',
                          'license' => 'perl',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'Test-Pod-LinkCheck-Lite',
                          'no_index' => {
                                          'directory' => [
                                                           'inc',
                                                           't',
                                                           'xt'
                                                         ],
                                          'package' => [
                                                         'My_Parser'
                                                       ]
                                        },
                          'provides' => {
                                          'Test::Pod::LinkCheck::Lite' => {
                                                                            'file' => 'lib/Test/Pod/LinkCheck/Lite.pm',
                                                                            'version' => '0.013'
                                                                          }
                                        },
                          'requires' => {
                                          'B::Keywords' => '0',
                                          'Carp' => '0',
                                          'Exporter' => '0',
                                          'File::Find' => '0',
                                          'File::Spec' => '0',
                                          'HTTP::Tiny' => '0',
                                          'IPC::Cmd' => '0',
                                          'Module::Load::Conditional' => '0',
                                          'Pod::Perldoc' => '0',
                                          'Pod::Simple' => '0',
                                          'Pod::Simple::LinkSection' => '0',
                                          'Pod::Simple::PullParser' => '0',
                                          'Storable' => '0',
                                          'Test::Builder' => '0',
                                          'constant' => '0',
                                          'perl' => '5.008',
                                          'strict' => '0',
                                          'utf8' => '0',
                                          'warnings' => '0'
                                        },
                          'resources' => {
                                           'bugtracker' => 'https://rt.cpan.org/Public/Dist/Display.html?Name=Test-Pod-LinkCheck-Lite',
                                           'license' => 'http://dev.perl.org/licenses/',
                                           'repository' => 'git://github.com/trwyant/perl-Test-Pod-LinkCheck-Lite.git'
                                         },
                          'version' => '0.013',
                          'x_serialization_backend' => 'CPAN::Meta::YAML version 0.018'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/Test/Pod/LinkCheck/Lite.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Test::Pod::LinkCheck::Lite'
                         }
                       ],
          'no_index' => '^inc/;^t/;^xt/',
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/Test-Pod-LinkCheck-Lite.tar.gz',
          'prereq' => [
                        {
                          'is_prereq' => 1,
                          'requires' => 'B::Keywords',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Carp',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Exporter',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'File::Find',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'File::Spec',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'HTTP::Tiny',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'IPC::Cmd',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Module::Load::Conditional',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Pod::Perldoc',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Pod::Simple',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Pod::Simple::LinkSection',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Pod::Simple::PullParser',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Storable',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Test::Builder',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'build_requires',
                          'version' => '0.88'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'constant',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'lib',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => '5.008'
                        },
                        {
                          'requires' => 'strict',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'strict',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'utf8',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'warnings',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'warnings',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        }
                      ],
          'released' => 1709583462,
          'size_packed' => 56928,
          'size_unpacked' => 146217,
          'test_files' => [
                            't/basic.t',
                            't/pod_file_ok.t'
                          ],
          'uses' => {
                      'build' => {
                                   'noes' => {
                                               'warnings' => '0'
                                             },
                                   'recommends' => {
                                                     'Data::Dumper' => '0'
                                                   },
                                   'requires' => {
                                                   'Config' => '0',
                                                   'Cwd' => '0',
                                                   'File::Glob' => '0',
                                                   'File::Spec' => '0',
                                                   'Getopt::Long' => '2.33',
                                                   'List::Util' => '0',
                                                   'Pod::Usage' => '0',
                                                   'constant' => '0',
                                                   'perl' => '5.008',
                                                   'strict' => '0',
                                                   'warnings' => '0'
                                                 }
                                 },
                      'configure' => {
                                       'requires' => {
                                                       'ExtUtils::MakeMaker' => '0',
                                                       'Module::Build' => '0',
                                                       'My::Module::Build' => '0',
                                                       'My::Module::Meta' => '0',
                                                       'lib' => '0',
                                                       'perl' => '5.008',
                                                       'strict' => '0',
                                                       'warnings' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'requires' => {
                                                     'B::Keywords' => '0',
                                                     'Carp' => '0',
                                                     'Exporter' => '0',
                                                     'File::Find' => '0',
                                                     'File::Spec' => '0',
                                                     'HTTP::Tiny' => '0',
                                                     'IPC::Cmd' => '0',
                                                     'Module::Load::Conditional' => '0',
                                                     'Pod::Perldoc' => '0',
                                                     'Pod::Simple' => '0',
                                                     'Pod::Simple::LinkSection' => '0',
                                                     'Pod::Simple::PullParser' => '0',
                                                     'Storable' => '0',
                                                     'Test::Builder' => '0',
                                                     'constant' => '0',
                                                     'perl' => '5.008',
                                                     'strict' => '0',
                                                     'utf8' => '0',
                                                     'warnings' => '0'
                                                   }
                                   },
                      'test' => {
                                  'noes' => {
                                              'warnings' => '0'
                                            },
                                  'requires' => {
                                                  'IPC::Cmd' => '0',
                                                  'Test::More' => '0.88',
                                                  'constant' => '0',
                                                  'perl' => '5.008',
                                                  'strict' => '0',
                                                  'warnings' => '0'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'Test-Pod-LinkCheck-Lite'
        };
1;
