$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'Music::Duration::Partition' => 'Partition a musical duration into rhythmic phrases'
                                },
          'author' => '',
          'dir_lib' => 'lib',
          'dir_t' => 't',
          'dirs' => 7,
          'dirs_array' => [
                            'eg',
                            'lib/Music/Duration/Partition/Tutorial',
                            'lib/Music/Duration/Partition',
                            'lib/Music/Duration',
                            'lib/Music',
                            'lib',
                            't'
                          ],
          'dist' => 'Music-Duration-Partition',
          'dynamic_config' => 0,
          'error' => {
                       'extracts_nicely' => 'expected Music-Duration-Partition but got Music-Duration-Partition-0.0820'
                     },
          'extension' => 'tar.gz',
          'external_license_file' => 'LICENSE',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_changelog' => 'Changes',
          'file_dist_ini' => 'dist.ini',
          'file_license' => 'LICENSE',
          'file_makefile_pl' => 'Makefile.PL',
          'file_manifest' => 'MANIFEST',
          'file_meta_json' => 'META.json',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README,README.md',
          'files' => 29,
          'files_array' => [
                             'Changes',
                             'LICENSE',
                             'MANIFEST',
                             'META.json',
                             'META.yml',
                             'Makefile.PL',
                             'README',
                             'README.md',
                             'dist.ini',
                             'eg/drum-fills',
                             'eg/drum-fills-advanced',
                             'eg/figured-bass',
                             'eg/grouping',
                             'eg/motif',
                             'eg/possibles',
                             'eg/seven-eight',
                             'eg/shuffle',
                             'eg/three-eight',
                             'lib/Music/Duration/Partition/Tutorial/Advanced.pod',
                             'lib/Music/Duration/Partition/Tutorial/Quickstart.pod',
                             'lib/Music/Duration/Partition.pm',
                             't/00-compile.t',
                             't/01-methods.t',
                             't/author-eol.t',
                             't/author-no-tabs.t',
                             't/author-pod-coverage.t',
                             't/author-pod-spell.t',
                             't/author-pod-syntax.t',
                             't/author-synopsis.t'
                           ],
          'files_hash' => {
                            'Changes' => {
                                           'mtime' => 1709278854,
                                           'size' => 5881
                                         },
                            'LICENSE' => {
                                           'mtime' => 1709278854,
                                           'size' => 8893
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1709278854,
                                            'size' => 578
                                          },
                            'META.json' => {
                                             'mtime' => 1709278854,
                                             'size' => 15814
                                           },
                            'META.yml' => {
                                            'mtime' => 1709278854,
                                            'size' => 9628
                                          },
                            'Makefile.PL' => {
                                               'mtime' => 1709278854,
                                               'requires' => {
                                                               'ExtUtils::MakeMaker' => '0',
                                                               'perl' => '5.006',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 1620
                                             },
                            'README' => {
                                          'mtime' => 1709278854,
                                          'size' => 400
                                        },
                            'README.md' => {
                                             'mtime' => 1709278854,
                                             'size' => 148
                                           },
                            'dist.ini' => {
                                            'mtime' => 1709278854,
                                            'size' => 1088
                                          },
                            'eg/drum-fills' => {
                                                 'mtime' => 1709278854,
                                                 'size' => 2454
                                               },
                            'eg/drum-fills-advanced' => {
                                                          'mtime' => 1709278854,
                                                          'size' => 4864
                                                        },
                            'eg/figured-bass' => {
                                                   'mtime' => 1709278854,
                                                   'size' => 2031
                                                 },
                            'eg/grouping' => {
                                               'mtime' => 1709278854,
                                               'size' => 610
                                             },
                            'eg/motif' => {
                                            'mtime' => 1709278854,
                                            'size' => 1609
                                          },
                            'eg/possibles' => {
                                                'mtime' => 1709278854,
                                                'size' => 1651
                                              },
                            'eg/seven-eight' => {
                                                  'mtime' => 1709278854,
                                                  'size' => 2956
                                                },
                            'eg/shuffle' => {
                                              'mtime' => 1709278854,
                                              'size' => 1251
                                            },
                            'eg/three-eight' => {
                                                  'mtime' => 1709278854,
                                                  'size' => 2470
                                                },
                            'lib/Music/Duration/Partition.pm' => {
                                                                   'license' => 'Perl_5',
                                                                   'module' => 'Music::Duration::Partition',
                                                                   'mtime' => 1709278854,
                                                                   'requires' => {
                                                                                   'List::Util' => '0',
                                                                                   'MIDI::Simple' => '0',
                                                                                   'Math::Random::Discrete' => '0',
                                                                                   'Moo' => '0',
                                                                                   'constant' => '0',
                                                                                   'namespace::clean' => '0',
                                                                                   'strictures' => '2'
                                                                                 },
                                                                   'size' => 8535
                                                                 },
                            'lib/Music/Duration/Partition/Tutorial/Advanced.pod' => {
                                                                                      'license' => 'Perl_5',
                                                                                      'mtime' => 1709278854,
                                                                                      'size' => 6114
                                                                                    },
                            'lib/Music/Duration/Partition/Tutorial/Quickstart.pod' => {
                                                                                        'license' => 'Perl_5',
                                                                                        'mtime' => 1709278854,
                                                                                        'size' => 2571
                                                                                      },
                            't/00-compile.t' => {
                                                  'mtime' => 1709278854,
                                                  'requires' => {
                                                                  'File::Spec' => '0',
                                                                  'IO::Handle' => '0',
                                                                  'IPC::Open3' => '0',
                                                                  'Test::More' => '0',
                                                                  'perl' => '5.006',
                                                                  'strict' => '0',
                                                                  'warnings' => '0'
                                                                },
                                                  'size' => 1449,
                                                  'suggests' => {
                                                                  'blib' => '1.01'
                                                                }
                                                },
                            't/01-methods.t' => {
                                                  'mtime' => 1709278854,
                                                  'requires' => {
                                                                  'Test::Exception' => '0',
                                                                  'Test::More' => '0.88',
                                                                  'strict' => '0',
                                                                  'warnings' => '0'
                                                                },
                                                  'size' => 2754
                                                },
                            't/author-eol.t' => {
                                                  'mtime' => 1709278854,
                                                  'recommends' => {
                                                                    'Test::EOL' => '0',
                                                                    'Test::More' => '0.88',
                                                                    'strict' => '0',
                                                                    'warnings' => '0'
                                                                  },
                                                  'size' => 556
                                                },
                            't/author-no-tabs.t' => {
                                                      'mtime' => 1709278854,
                                                      'recommends' => {
                                                                        'Test::More' => '0.88',
                                                                        'Test::NoTabs' => '0',
                                                                        'strict' => '0',
                                                                        'warnings' => '0'
                                                                      },
                                                      'size' => 530
                                                    },
                            't/author-pod-coverage.t' => {
                                                           'mtime' => 1709278854,
                                                           'recommends' => {
                                                                             'Pod::Coverage::TrustPod' => '0',
                                                                             'Test::Pod::Coverage' => '1.08',
                                                                             'strict' => '0',
                                                                             'warnings' => '0'
                                                                           },
                                                           'size' => 375
                                                         },
                            't/author-pod-spell.t' => {
                                                        'mtime' => 1709278854,
                                                        'recommends' => {
                                                                          'Pod::Wordlist' => '0',
                                                                          'Test::More' => '0',
                                                                          'Test::Spelling' => '0.12',
                                                                          'strict' => '0',
                                                                          'warnings' => '0'
                                                                        },
                                                        'size' => 515
                                                      },
                            't/author-pod-syntax.t' => {
                                                         'mtime' => 1709278854,
                                                         'recommends' => {
                                                                           'Test::More' => '0',
                                                                           'Test::Pod' => '1.41',
                                                                           'strict' => '0',
                                                                           'warnings' => '0'
                                                                         },
                                                         'size' => 300
                                                       },
                            't/author-synopsis.t' => {
                                                       'mtime' => 1709278854,
                                                       'recommends' => {
                                                                         'Test::Synopsis' => '0'
                                                                       },
                                                       'size' => 178
                                                     }
                          },
          'got_prereq_from' => 'META.yml',
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 1,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 1,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 32,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 0,
                          'meta_yml_has_repository_resource' => 1,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 1,
                          'use_warnings' => 1
                        },
          'latest_mtime' => 1709278854,
          'license' => 'perl defined in META.yml defined in LICENSE',
          'license_file' => 'lib/Music/Duration/Partition.pm,lib/Music/Duration/Partition/Tutorial/Advanced.pod,lib/Music/Duration/Partition/Tutorial/Quickstart.pod',
          'license_from_yaml' => 'perl',
          'license_in_pod' => 1,
          'license_type' => 'Perl_5',
          'licenses' => {
                          'Perl_5' => [
                                        'lib/Music/Duration/Partition.pm',
                                        'lib/Music/Duration/Partition/Tutorial/Advanced.pod',
                                        'lib/Music/Duration/Partition/Tutorial/Quickstart.pod'
                                      ]
                        },
          'manifest_matches_dist' => 1,
          'meta_json' => {
                           'abstract' => 'Partition a musical duration into rhythmic phrases',
                           'author' => [
                                         'Gene Boggs <gene@cpan.org>'
                                       ],
                           'dynamic_config' => 0,
                           'generated_by' => 'Dist::Zilla version 6.031, CPAN::Meta::Converter version 2.150010',
                           'license' => [
                                          'perl_5'
                                        ],
                           'meta-spec' => {
                                            'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                            'version' => 2
                                          },
                           'name' => 'Music-Duration-Partition',
                           'prereqs' => {
                                          'configure' => {
                                                           'requires' => {
                                                                           'ExtUtils::MakeMaker' => '0',
                                                                           'perl' => '5.006'
                                                                         }
                                                         },
                                          'develop' => {
                                                         'requires' => {
                                                                         'Pod::Coverage::TrustPod' => '0',
                                                                         'Test::EOL' => '0',
                                                                         'Test::More' => '0.88',
                                                                         'Test::NoTabs' => '0',
                                                                         'Test::Pod' => '1.41',
                                                                         'Test::Pod::Coverage' => '1.08',
                                                                         'Test::Spelling' => '0.12',
                                                                         'Test::Synopsis' => '0'
                                                                       }
                                                       },
                                          'runtime' => {
                                                         'requires' => {
                                                                         'List::Util' => '0',
                                                                         'MIDI::Simple' => '0',
                                                                         'Math::Random::Discrete' => '0',
                                                                         'Moo' => '2',
                                                                         'constant' => '0',
                                                                         'namespace::clean' => '0',
                                                                         'perl' => '5.006',
                                                                         'strictures' => '2'
                                                                       },
                                                         'suggests' => {
                                                                         'Data::Dumper::Compact' => '0',
                                                                         'MIDI::Drummer::Tiny' => '0',
                                                                         'MIDI::Util' => '0',
                                                                         'Music::Scales' => '0',
                                                                         'Music::VoiceGen' => '0',
                                                                         'Music::Voss' => '0'
                                                                       }
                                                       },
                                          'test' => {
                                                      'requires' => {
                                                                      'File::Spec' => '0',
                                                                      'IO::Handle' => '0',
                                                                      'IPC::Open3' => '0',
                                                                      'Test::Exception' => '0',
                                                                      'Test::More' => '0',
                                                                      'perl' => '5.006',
                                                                      'strict' => '0',
                                                                      'warnings' => '0'
                                                                    }
                                                    }
                                        },
                           'release_status' => 'stable',
                           'resources' => {
                                            'homepage' => 'https://github.com/ology/Music-Duration-Partition',
                                            'repository' => {
                                                              'type' => 'git',
                                                              'url' => 'https://github.com/ology/Music-Duration-Partition.git',
                                                              'web' => 'https://github.com/ology/Music-Duration-Partition'
                                                            }
                                          },
                           'version' => '0.0820',
                           'x_Dist_Zilla' => {
                                               'perl' => {
                                                           'version' => '5.032001'
                                                         },
                                               'plugins' => [
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::VersionFromModule',
                                                                'name' => 'VersionFromModule',
                                                                'version' => '0.08'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::MinimumPerl',
                                                                'name' => 'MinimumPerl',
                                                                'version' => '1.006'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::AutoPrereqs',
                                                                'name' => 'AutoPrereqs',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::CheckChangesHasContent',
                                                                'name' => 'CheckChangesHasContent',
                                                                'version' => '0.011'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::NextRelease',
                                                                'name' => 'NextRelease',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Authority',
                                                                'name' => 'Authority',
                                                                'version' => '1.009'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::PkgVersion',
                                                                'name' => 'PkgVersion',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::PodWeaver',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::PodWeaver' => {
                                                                                                                    'finder' => [
                                                                                                                                  ':InstallModules',
                                                                                                                                  ':PerlExecFiles'
                                                                                                                                ],
                                                                                                                    'plugins' => [
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Plugin::EnsurePod5',
                                                                                                                                     'name' => '@CorePrep/EnsurePod5',
                                                                                                                                     'version' => '4.017'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Plugin::H1Nester',
                                                                                                                                     'name' => '@CorePrep/H1Nester',
                                                                                                                                     'version' => '4.017'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Plugin::SingleEncoding',
                                                                                                                                     'name' => '@Default/SingleEncoding',
                                                                                                                                     'version' => '4.017'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Name',
                                                                                                                                     'name' => '@Default/Name',
                                                                                                                                     'version' => '4.017'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Version',
                                                                                                                                     'name' => '@Default/Version',
                                                                                                                                     'version' => '4.017'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Region',
                                                                                                                                     'name' => '@Default/prelude',
                                                                                                                                     'version' => '4.017'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Generic',
                                                                                                                                     'name' => 'SYNOPSIS',
                                                                                                                                     'version' => '4.017'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Generic',
                                                                                                                                     'name' => 'DESCRIPTION',
                                                                                                                                     'version' => '4.017'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Generic',
                                                                                                                                     'name' => 'OVERVIEW',
                                                                                                                                     'version' => '4.017'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Collect',
                                                                                                                                     'name' => 'ATTRIBUTES',
                                                                                                                                     'version' => '4.017'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Collect',
                                                                                                                                     'name' => 'METHODS',
                                                                                                                                     'version' => '4.017'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Collect',
                                                                                                                                     'name' => 'FUNCTIONS',
                                                                                                                                     'version' => '4.017'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Leftovers',
                                                                                                                                     'name' => '@Default/Leftovers',
                                                                                                                                     'version' => '4.017'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Region',
                                                                                                                                     'name' => '@Default/postlude',
                                                                                                                                     'version' => '4.017'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Authors',
                                                                                                                                     'name' => '@Default/Authors',
                                                                                                                                     'version' => '4.017'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Legal',
                                                                                                                                     'name' => '@Default/Legal',
                                                                                                                                     'version' => '4.017'
                                                                                                                                   }
                                                                                                                                 ]
                                                                                                                  }
                                                                            },
                                                                'name' => 'PodWeaver',
                                                                'version' => '4.010'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::PodCoverageTests',
                                                                'name' => 'PodCoverageTests',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::PodSyntaxTests',
                                                                'name' => 'PodSyntaxTests',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Test::NoTabs',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::Test::NoTabs' => {
                                                                                                                       'filename' => 'xt/author/no-tabs.t',
                                                                                                                       'finder' => [
                                                                                                                                     ':InstallModules',
                                                                                                                                     ':ExecFiles',
                                                                                                                                     ':TestFiles'
                                                                                                                                   ]
                                                                                                                     }
                                                                            },
                                                                'name' => 'Test::NoTabs',
                                                                'version' => '0.15'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Test::EOL',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::Test::EOL' => {
                                                                                                                    'filename' => 'xt/author/eol.t',
                                                                                                                    'finder' => [
                                                                                                                                  ':ExecFiles',
                                                                                                                                  ':InstallModules',
                                                                                                                                  ':TestFiles'
                                                                                                                                ],
                                                                                                                    'trailing_whitespace' => 1
                                                                                                                  }
                                                                            },
                                                                'name' => 'Test::EOL',
                                                                'version' => '0.19'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Test::Compile',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::Test::Compile' => {
                                                                                                                        'bail_out_on_fail' => 0,
                                                                                                                        'fail_on_warning' => 'author',
                                                                                                                        'fake_home' => 0,
                                                                                                                        'filename' => 't/00-compile.t',
                                                                                                                        'module_finder' => [
                                                                                                                                             ':InstallModules'
                                                                                                                                           ],
                                                                                                                        'needs_display' => 0,
                                                                                                                        'phase' => 'test',
                                                                                                                        'script_finder' => [
                                                                                                                                             ':PerlExecFiles'
                                                                                                                                           ],
                                                                                                                        'skips' => [],
                                                                                                                        'switch' => []
                                                                                                                      }
                                                                            },
                                                                'name' => 'Test::Compile',
                                                                'version' => '2.058'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Test::Synopsis',
                                                                'name' => 'Test::Synopsis',
                                                                'version' => '2.000007'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::MetaConfig',
                                                                'name' => 'MetaConfig',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::MetaJSON',
                                                                'name' => 'MetaJSON',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::GithubMeta',
                                                                'name' => 'GithubMeta',
                                                                'version' => '0.58'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Test::PodSpelling',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::Test::PodSpelling' => {
                                                                                                                            'directories' => [
                                                                                                                                               'bin',
                                                                                                                                               'lib'
                                                                                                                                             ],
                                                                                                                            'spell_cmd' => 'aspell list',
                                                                                                                            'stopwords' => [
                                                                                                                                             'Instantiation',
                                                                                                                                             'VLC',
                                                                                                                                             'Voss',
                                                                                                                                             'durations',
                                                                                                                                             'hn',
                                                                                                                                             'homebrew',
                                                                                                                                             'qn',
                                                                                                                                             'th',
                                                                                                                                             'wn'
                                                                                                                                           ],
                                                                                                                            'wordlist' => 'Pod::Wordlist'
                                                                                                                          }
                                                                            },
                                                                'name' => 'Test::PodSpelling',
                                                                'version' => '2.007005'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Prereqs',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::Prereqs' => {
                                                                                                                  'phase' => 'runtime',
                                                                                                                  'type' => 'requires'
                                                                                                                }
                                                                            },
                                                                'name' => 'Prereqs',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Prereqs',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::Prereqs' => {
                                                                                                                  'phase' => 'runtime',
                                                                                                                  'type' => 'suggests'
                                                                                                                }
                                                                            },
                                                                'name' => 'Suggests',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::GatherDir',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::GatherDir' => {
                                                                                                                    'exclude_filename' => [],
                                                                                                                    'exclude_match' => [
                                                                                                                                         '\\.mid',
                                                                                                                                         'site'
                                                                                                                                       ],
                                                                                                                    'follow_symlinks' => 0,
                                                                                                                    'include_dotfiles' => 0,
                                                                                                                    'prefix' => '',
                                                                                                                    'prune_directory' => [],
                                                                                                                    'root' => '.'
                                                                                                                  }
                                                                            },
                                                                'name' => 'GatherDir',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::PruneCruft',
                                                                'name' => 'PruneCruft',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::ManifestSkip',
                                                                'name' => 'ManifestSkip',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::MetaYAML',
                                                                'name' => 'MetaYAML',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Readme',
                                                                'name' => 'Readme',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::ExtraTests',
                                                                'name' => 'ExtraTests',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::ExecDir',
                                                                'name' => 'ExecDir',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::ShareDir',
                                                                'name' => 'ShareDir',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::MakeMaker',
                                                                'config' => {
                                                                              'Dist::Zilla::Role::TestRunner' => {
                                                                                                                   'default_jobs' => 1
                                                                                                                 }
                                                                            },
                                                                'name' => 'MakeMaker',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Manifest',
                                                                'name' => 'Manifest',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::TestRelease',
                                                                'name' => 'TestRelease',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::ConfirmRelease',
                                                                'name' => 'ConfirmRelease',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::UploadToCPAN',
                                                                'name' => 'UploadToCPAN',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::CopyFilesFromBuild',
                                                                'name' => 'CopyFilesFromBuild',
                                                                'version' => '0.170880'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':InstallModules',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':IncModules',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':TestFiles',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':ExtraTestFiles',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':ExecFiles',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':PerlExecFiles',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':ShareFiles',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':MainModule',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':AllFiles',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':NoFiles',
                                                                'version' => '6.031'
                                                              }
                                                            ],
                                               'zilla' => {
                                                            'class' => 'Dist::Zilla::Dist::Builder',
                                                            'config' => {
                                                                          'is_trial' => 0
                                                                        },
                                                            'version' => '6.031'
                                                          }
                                             },
                           'x_authority' => 'cpan:GENE',
                           'x_generated_by_perl' => 'v5.32.1',
                           'x_serialization_backend' => 'Cpanel::JSON::XS version 4.26',
                           'x_spdx_expression' => 'Artistic-1.0-Perl OR GPL-1.0-or-later'
                         },
          'meta_json_is_parsable' => 1,
          'meta_json_spec_version' => 2,
          'meta_yml' => {
                          'abstract' => 'Partition a musical duration into rhythmic phrases',
                          'author' => [
                                        'Gene Boggs <gene@cpan.org>'
                                      ],
                          'build_requires' => {
                                                'File::Spec' => '0',
                                                'IO::Handle' => '0',
                                                'IPC::Open3' => '0',
                                                'Test::Exception' => '0',
                                                'Test::More' => '0',
                                                'perl' => '5.006',
                                                'strict' => '0',
                                                'warnings' => '0'
                                              },
                          'configure_requires' => {
                                                    'ExtUtils::MakeMaker' => '0',
                                                    'perl' => '5.006'
                                                  },
                          'dynamic_config' => '0',
                          'generated_by' => 'Dist::Zilla version 6.031, CPAN::Meta::Converter version 2.150010',
                          'license' => 'perl',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'Music-Duration-Partition',
                          'requires' => {
                                          'List::Util' => '0',
                                          'MIDI::Simple' => '0',
                                          'Math::Random::Discrete' => '0',
                                          'Moo' => '2',
                                          'constant' => '0',
                                          'namespace::clean' => '0',
                                          'perl' => '5.006',
                                          'strictures' => '2'
                                        },
                          'resources' => {
                                           'homepage' => 'https://github.com/ology/Music-Duration-Partition',
                                           'repository' => 'https://github.com/ology/Music-Duration-Partition.git'
                                         },
                          'version' => '0.0820',
                          'x_Dist_Zilla' => {
                                              'perl' => {
                                                          'version' => '5.032001'
                                                        },
                                              'plugins' => [
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::VersionFromModule',
                                                               'name' => 'VersionFromModule',
                                                               'version' => '0.08'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MinimumPerl',
                                                               'name' => 'MinimumPerl',
                                                               'version' => '1.006'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::AutoPrereqs',
                                                               'name' => 'AutoPrereqs',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::CheckChangesHasContent',
                                                               'name' => 'CheckChangesHasContent',
                                                               'version' => '0.011'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::NextRelease',
                                                               'name' => 'NextRelease',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Authority',
                                                               'name' => 'Authority',
                                                               'version' => '1.009'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PkgVersion',
                                                               'name' => 'PkgVersion',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PodWeaver',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::PodWeaver' => {
                                                                                                                   'finder' => [
                                                                                                                                 ':InstallModules',
                                                                                                                                 ':PerlExecFiles'
                                                                                                                               ],
                                                                                                                   'plugins' => [
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Plugin::EnsurePod5',
                                                                                                                                    'name' => '@CorePrep/EnsurePod5',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Plugin::H1Nester',
                                                                                                                                    'name' => '@CorePrep/H1Nester',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Plugin::SingleEncoding',
                                                                                                                                    'name' => '@Default/SingleEncoding',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Name',
                                                                                                                                    'name' => '@Default/Name',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Version',
                                                                                                                                    'name' => '@Default/Version',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Region',
                                                                                                                                    'name' => '@Default/prelude',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Generic',
                                                                                                                                    'name' => 'SYNOPSIS',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Generic',
                                                                                                                                    'name' => 'DESCRIPTION',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Generic',
                                                                                                                                    'name' => 'OVERVIEW',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Collect',
                                                                                                                                    'name' => 'ATTRIBUTES',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Collect',
                                                                                                                                    'name' => 'METHODS',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Collect',
                                                                                                                                    'name' => 'FUNCTIONS',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Leftovers',
                                                                                                                                    'name' => '@Default/Leftovers',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Region',
                                                                                                                                    'name' => '@Default/postlude',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Authors',
                                                                                                                                    'name' => '@Default/Authors',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Legal',
                                                                                                                                    'name' => '@Default/Legal',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  }
                                                                                                                                ]
                                                                                                                 }
                                                                           },
                                                               'name' => 'PodWeaver',
                                                               'version' => '4.010'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PodCoverageTests',
                                                               'name' => 'PodCoverageTests',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PodSyntaxTests',
                                                               'name' => 'PodSyntaxTests',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::NoTabs',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Test::NoTabs' => {
                                                                                                                      'filename' => 'xt/author/no-tabs.t',
                                                                                                                      'finder' => [
                                                                                                                                    ':InstallModules',
                                                                                                                                    ':ExecFiles',
                                                                                                                                    ':TestFiles'
                                                                                                                                  ]
                                                                                                                    }
                                                                           },
                                                               'name' => 'Test::NoTabs',
                                                               'version' => '0.15'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::EOL',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Test::EOL' => {
                                                                                                                   'filename' => 'xt/author/eol.t',
                                                                                                                   'finder' => [
                                                                                                                                 ':ExecFiles',
                                                                                                                                 ':InstallModules',
                                                                                                                                 ':TestFiles'
                                                                                                                               ],
                                                                                                                   'trailing_whitespace' => '1'
                                                                                                                 }
                                                                           },
                                                               'name' => 'Test::EOL',
                                                               'version' => '0.19'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::Compile',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Test::Compile' => {
                                                                                                                       'bail_out_on_fail' => '0',
                                                                                                                       'fail_on_warning' => 'author',
                                                                                                                       'fake_home' => '0',
                                                                                                                       'filename' => 't/00-compile.t',
                                                                                                                       'module_finder' => [
                                                                                                                                            ':InstallModules'
                                                                                                                                          ],
                                                                                                                       'needs_display' => '0',
                                                                                                                       'phase' => 'test',
                                                                                                                       'script_finder' => [
                                                                                                                                            ':PerlExecFiles'
                                                                                                                                          ],
                                                                                                                       'skips' => [],
                                                                                                                       'switch' => []
                                                                                                                     }
                                                                           },
                                                               'name' => 'Test::Compile',
                                                               'version' => '2.058'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::Synopsis',
                                                               'name' => 'Test::Synopsis',
                                                               'version' => '2.000007'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MetaConfig',
                                                               'name' => 'MetaConfig',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MetaJSON',
                                                               'name' => 'MetaJSON',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::GithubMeta',
                                                               'name' => 'GithubMeta',
                                                               'version' => '0.58'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::PodSpelling',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Test::PodSpelling' => {
                                                                                                                           'directories' => [
                                                                                                                                              'bin',
                                                                                                                                              'lib'
                                                                                                                                            ],
                                                                                                                           'spell_cmd' => 'aspell list',
                                                                                                                           'stopwords' => [
                                                                                                                                            'Instantiation',
                                                                                                                                            'VLC',
                                                                                                                                            'Voss',
                                                                                                                                            'durations',
                                                                                                                                            'hn',
                                                                                                                                            'homebrew',
                                                                                                                                            'qn',
                                                                                                                                            'th',
                                                                                                                                            'wn'
                                                                                                                                          ],
                                                                                                                           'wordlist' => 'Pod::Wordlist'
                                                                                                                         }
                                                                           },
                                                               'name' => 'Test::PodSpelling',
                                                               'version' => '2.007005'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Prereqs',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Prereqs' => {
                                                                                                                 'phase' => 'runtime',
                                                                                                                 'type' => 'requires'
                                                                                                               }
                                                                           },
                                                               'name' => 'Prereqs',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Prereqs',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Prereqs' => {
                                                                                                                 'phase' => 'runtime',
                                                                                                                 'type' => 'suggests'
                                                                                                               }
                                                                           },
                                                               'name' => 'Suggests',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::GatherDir',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::GatherDir' => {
                                                                                                                   'exclude_filename' => [],
                                                                                                                   'exclude_match' => [
                                                                                                                                        '\\.mid',
                                                                                                                                        'site'
                                                                                                                                      ],
                                                                                                                   'follow_symlinks' => '0',
                                                                                                                   'include_dotfiles' => '0',
                                                                                                                   'prefix' => '',
                                                                                                                   'prune_directory' => [],
                                                                                                                   'root' => '.'
                                                                                                                 }
                                                                           },
                                                               'name' => 'GatherDir',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PruneCruft',
                                                               'name' => 'PruneCruft',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::ManifestSkip',
                                                               'name' => 'ManifestSkip',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MetaYAML',
                                                               'name' => 'MetaYAML',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Readme',
                                                               'name' => 'Readme',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::ExtraTests',
                                                               'name' => 'ExtraTests',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::ExecDir',
                                                               'name' => 'ExecDir',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::ShareDir',
                                                               'name' => 'ShareDir',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MakeMaker',
                                                               'config' => {
                                                                             'Dist::Zilla::Role::TestRunner' => {
                                                                                                                  'default_jobs' => '1'
                                                                                                                }
                                                                           },
                                                               'name' => 'MakeMaker',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Manifest',
                                                               'name' => 'Manifest',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::TestRelease',
                                                               'name' => 'TestRelease',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::ConfirmRelease',
                                                               'name' => 'ConfirmRelease',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::UploadToCPAN',
                                                               'name' => 'UploadToCPAN',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::CopyFilesFromBuild',
                                                               'name' => 'CopyFilesFromBuild',
                                                               'version' => '0.170880'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':InstallModules',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':IncModules',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':TestFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':ExtraTestFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':ExecFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':PerlExecFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':ShareFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':MainModule',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':AllFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':NoFiles',
                                                               'version' => '6.031'
                                                             }
                                                           ],
                                              'zilla' => {
                                                           'class' => 'Dist::Zilla::Dist::Builder',
                                                           'config' => {
                                                                         'is_trial' => '0'
                                                                       },
                                                           'version' => '6.031'
                                                         }
                                            },
                          'x_authority' => 'cpan:GENE',
                          'x_generated_by_perl' => 'v5.32.1',
                          'x_serialization_backend' => 'YAML::Tiny version 1.73',
                          'x_spdx_expression' => 'Artistic-1.0-Perl OR GPL-1.0-or-later'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/Music/Duration/Partition.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Music::Duration::Partition'
                         }
                       ],
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/Music-Duration-Partition.tar.gz',
          'prereq' => [
                        {
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'File::Spec',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'IO::Handle',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'IPC::Open3',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'List::Util',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'MIDI::Simple',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Math::Random::Discrete',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Moo',
                          'type' => 'runtime_requires',
                          'version' => '2'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::Exception',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'constant',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'namespace::clean',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'build_requires',
                          'version' => '5.006'
                        },
                        {
                          'requires' => 'perl',
                          'type' => 'configure_requires',
                          'version' => '5.006'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => '5.006'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'strict',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'strictures',
                          'type' => 'runtime_requires',
                          'version' => '2'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'warnings',
                          'type' => 'build_requires',
                          'version' => '0'
                        }
                      ],
          'released' => 1709583489,
          'size_packed' => 21162,
          'size_unpacked' => 87823,
          'test_files' => [
                            't/00-compile.t',
                            't/01-methods.t',
                            't/author-eol.t',
                            't/author-no-tabs.t',
                            't/author-pod-coverage.t',
                            't/author-pod-spell.t',
                            't/author-pod-syntax.t',
                            't/author-synopsis.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'requires' => {
                                                       'ExtUtils::MakeMaker' => '0',
                                                       'perl' => '5.006',
                                                       'strict' => '0',
                                                       'warnings' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'requires' => {
                                                     'List::Util' => '0',
                                                     'MIDI::Simple' => '0',
                                                     'Math::Random::Discrete' => '0',
                                                     'Moo' => '0',
                                                     'constant' => '0',
                                                     'namespace::clean' => '0',
                                                     'strictures' => '2'
                                                   }
                                   },
                      'test' => {
                                  'recommends' => {
                                                    'Pod::Coverage::TrustPod' => '0',
                                                    'Pod::Wordlist' => '0',
                                                    'Test::EOL' => '0',
                                                    'Test::More' => '0.88',
                                                    'Test::NoTabs' => '0',
                                                    'Test::Pod' => '1.41',
                                                    'Test::Pod::Coverage' => '1.08',
                                                    'Test::Spelling' => '0.12',
                                                    'Test::Synopsis' => '0',
                                                    'strict' => '0',
                                                    'warnings' => '0'
                                                  },
                                  'requires' => {
                                                  'File::Spec' => '0',
                                                  'IO::Handle' => '0',
                                                  'IPC::Open3' => '0',
                                                  'Test::Exception' => '0',
                                                  'Test::More' => '0.88',
                                                  'perl' => '5.006',
                                                  'strict' => '0',
                                                  'warnings' => '0'
                                                },
                                  'suggests' => {
                                                  'blib' => '1.01'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'Music-Duration-Partition'
        };
1;
