$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'Runtime::Debugger' => 'Easy to use REPL with existing lexical support and DWIM tab completion.'
                                },
          'author' => '',
          'dir_lib' => 'lib',
          'dir_t' => 't',
          'dirs' => 3,
          'dirs_array' => [
                            'lib/Runtime',
                            'lib',
                            't'
                          ],
          'dist' => 'Runtime-Debugger',
          'dynamic_config' => 1,
          'error' => {
                       'extracts_nicely' => 'expected Runtime-Debugger but got Runtime-Debugger-0.14'
                     },
          'extension' => 'tar.gz',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_build_pl' => 'Build.PL',
          'file_changelog' => 'Changes',
          'file_manifest' => 'MANIFEST',
          'file_manifest_skip' => 'MANIFEST.SKIP',
          'file_meta_json' => 'META.json',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README',
          'files' => 13,
          'files_array' => [
                             'Build.PL',
                             'Changes',
                             'MANIFEST',
                             'MANIFEST.SKIP',
                             'META.json',
                             'META.yml',
                             'README',
                             'lib/Runtime/Debugger.pm',
                             't/00-load.t',
                             't/01-readline.t',
                             't/manifest.t',
                             't/pod-coverage.t',
                             't/pod.t'
                           ],
          'files_hash' => {
                            'Build.PL' => {
                                            'mtime' => 1709560115,
                                            'requires' => {
                                                            'Module::Build' => '0.4004',
                                                            'perl' => '5.006',
                                                            'strict' => '0',
                                                            'warnings' => '0'
                                                          },
                                            'size' => 1769
                                          },
                            'Changes' => {
                                           'mtime' => 1709560115,
                                           'size' => 1588
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1709560115,
                                            'size' => 177
                                          },
                            'MANIFEST.SKIP' => {
                                                 'mtime' => 1709560115,
                                                 'size' => 1637
                                               },
                            'META.json' => {
                                             'mtime' => 1709560115,
                                             'size' => 1451
                                           },
                            'META.yml' => {
                                            'mtime' => 1709560115,
                                            'size' => 940
                                          },
                            'README' => {
                                          'mtime' => 1709560115,
                                          'size' => 1580
                                        },
                            'lib/Runtime/Debugger.pm' => {
                                                           'license' => 'Artistic_2_0',
                                                           'module' => 'Runtime::Debugger',
                                                           'mtime' => 1709560115,
                                                           'noes' => {
                                                                       'strict' => '0'
                                                                     },
                                                           'requires' => {
                                                                           'Class::Tiny' => '0',
                                                                           'Data::Dumper' => '0',
                                                                           'Data::Printer' => '0',
                                                                           'Exporter' => '0',
                                                                           'Filter::Simple' => '0',
                                                                           'PadWalker' => '0',
                                                                           'Scalar::Util' => '0',
                                                                           'Term::ANSIColor' => '0',
                                                                           'Term::ReadLine' => '0',
                                                                           'feature' => '0',
                                                                           'parent' => '0',
                                                                           'perl' => '5.012',
                                                                           'strict' => '0',
                                                                           'subs' => '0',
                                                                           'warnings' => '0'
                                                                         },
                                                           'size' => 25788
                                                         },
                            't/00-load.t' => {
                                               'mtime' => 1709560115,
                                               'requires' => {
                                                               'Test::More' => '0',
                                                               'perl' => '5.006',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 382
                                             },
                            't/01-readline.t' => {
                                                   'mtime' => 1709560115,
                                                   'requires' => {
                                                                   'Runtime::Debugger' => '0',
                                                                   'Term::ANSIColor' => '0',
                                                                   'Test::More' => '0',
                                                                   'feature' => '0',
                                                                   'perl' => '5.006',
                                                                   'strict' => '0',
                                                                   'warnings' => '0'
                                                                 },
                                                   'size' => 28663
                                                 },
                            't/manifest.t' => {
                                                'mtime' => 1709560115,
                                                'requires' => {
                                                                'Test::More' => '0',
                                                                'perl' => '5.006',
                                                                'strict' => '0',
                                                                'warnings' => '0'
                                                              },
                                                'size' => 309,
                                                'suggests' => {
                                                                'Test::CheckManifest' => '0'
                                                              }
                                              },
                            't/pod-coverage.t' => {
                                                    'mtime' => 1709560115,
                                                    'requires' => {
                                                                    'Test::More' => '0',
                                                                    'perl' => '5.006',
                                                                    'strict' => '0',
                                                                    'warnings' => '0'
                                                                  },
                                                    'size' => 675,
                                                    'suggests' => {
                                                                    'Pod::Coverage' => '0',
                                                                    'Test::Pod::Coverage' => '0'
                                                                  }
                                                  },
                            't/pod.t' => {
                                           'mtime' => 1709560115,
                                           'requires' => {
                                                           'Test::More' => '0',
                                                           'perl' => '5.006',
                                                           'strict' => '0',
                                                           'warnings' => '0'
                                                         },
                                           'size' => 347,
                                           'suggests' => {
                                                           'Test::Pod' => '0'
                                                         }
                                         }
                          },
          'got_prereq_from' => 'META.yml',
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 1,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 0,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 32,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 1,
                          'meta_yml_has_repository_resource' => 1,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 1,
                          'use_warnings' => 1
                        },
          'latest_mtime' => 1709560115,
          'license' => 'artistic_2 defined in META.yml',
          'license_file' => 'lib/Runtime/Debugger.pm',
          'license_from_yaml' => 'artistic_2',
          'license_in_pod' => 1,
          'license_type' => 'Artistic_2_0',
          'licenses' => {
                          'Artistic_2_0' => [
                                              'lib/Runtime/Debugger.pm'
                                            ]
                        },
          'manifest_matches_dist' => 1,
          'meta_json' => {
                           'abstract' => 'Easy to use REPL with existing lexical support and DWIM tab completion.',
                           'author' => [
                                         'Tim Potapov <tim.potapov[AT]gmail.com>'
                                       ],
                           'dynamic_config' => 1,
                           'generated_by' => 'Module::Build version 0.4234',
                           'license' => [
                                          'artistic_2'
                                        ],
                           'meta-spec' => {
                                            'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                            'version' => 2
                                          },
                           'name' => 'Runtime-Debugger',
                           'prereqs' => {
                                          'configure' => {
                                                           'requires' => {
                                                                           'Module::Build' => '0.4004'
                                                                         }
                                                         },
                                          'runtime' => {
                                                         'requires' => {
                                                                         'Class::Tiny' => '1.008',
                                                                         'Data::Printer' => '1.002001',
                                                                         'PadWalker' => '2.5',
                                                                         'Term::ReadLine::Gnu' => '1.44',
                                                                         'perl' => '5.014'
                                                                       }
                                                       },
                                          'test' => {
                                                      'requires' => {
                                                                      'Test::More' => '0'
                                                                    }
                                                    }
                                        },
                           'provides' => {
                                           'Runtime::Debugger' => {
                                                                    'file' => 'lib/Runtime/Debugger.pm',
                                                                    'version' => '0.14'
                                                                  }
                                         },
                           'release_status' => 'stable',
                           'resources' => {
                                            'bugtracker' => {
                                                              'web' => 'https://github.com/poti1/runtime-debugger/issues'
                                                            },
                                            'license' => [
                                                           'http://opensource.org/licenses/artistic-license-2.0.php'
                                                         ],
                                            'repository' => {
                                                              'url' => 'https://github.com/poti1/runtime-debugger'
                                                            }
                                          },
                           'version' => '0.14',
                           'x_serialization_backend' => 'JSON::PP version 4.04'
                         },
          'meta_json_is_parsable' => 1,
          'meta_json_spec_version' => 2,
          'meta_yml' => {
                          'abstract' => 'Easy to use REPL with existing lexical support and DWIM tab completion.',
                          'author' => [
                                        'Tim Potapov <tim.potapov[AT]gmail.com>'
                                      ],
                          'build_requires' => {
                                                'Test::More' => '0'
                                              },
                          'configure_requires' => {
                                                    'Module::Build' => '0.4004'
                                                  },
                          'dynamic_config' => '1',
                          'generated_by' => 'Module::Build version 0.4234, CPAN::Meta::Converter version 2.150010',
                          'license' => 'artistic_2',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'Runtime-Debugger',
                          'provides' => {
                                          'Runtime::Debugger' => {
                                                                   'file' => 'lib/Runtime/Debugger.pm',
                                                                   'version' => '0.14'
                                                                 }
                                        },
                          'requires' => {
                                          'Class::Tiny' => '1.008',
                                          'Data::Printer' => '1.002001',
                                          'PadWalker' => '2.5',
                                          'Term::ReadLine::Gnu' => '1.44',
                                          'perl' => '5.014'
                                        },
                          'resources' => {
                                           'bugtracker' => 'https://github.com/poti1/runtime-debugger/issues',
                                           'license' => 'http://opensource.org/licenses/artistic-license-2.0.php',
                                           'repository' => 'https://github.com/poti1/runtime-debugger'
                                         },
                          'version' => '0.14',
                          'x_serialization_backend' => 'CPAN::Meta::YAML version 0.018'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/Runtime/Debugger.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Runtime::Debugger'
                         }
                       ],
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/Runtime-Debugger.tar.gz',
          'prereq' => [
                        {
                          'is_prereq' => 1,
                          'requires' => 'Class::Tiny',
                          'type' => 'runtime_requires',
                          'version' => '1.008'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Data::Printer',
                          'type' => 'runtime_requires',
                          'version' => '1.002001'
                        },
                        {
                          'requires' => 'Module::Build',
                          'type' => 'configure_requires',
                          'version' => '0.4004'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'PadWalker',
                          'type' => 'runtime_requires',
                          'version' => '2.5'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Term::ReadLine::Gnu',
                          'type' => 'runtime_requires',
                          'version' => '1.44'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => '5.014'
                        }
                      ],
          'released' => 1709583447,
          'size_packed' => 19788,
          'size_unpacked' => 65306,
          'test_files' => [
                            't/00-load.t',
                            't/01-readline.t',
                            't/manifest.t',
                            't/pod-coverage.t',
                            't/pod.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'requires' => {
                                                       'Module::Build' => '0.4004',
                                                       'perl' => '5.006',
                                                       'strict' => '0',
                                                       'warnings' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'noes' => {
                                                 'strict' => '0'
                                               },
                                     'requires' => {
                                                     'Class::Tiny' => '0',
                                                     'Data::Dumper' => '0',
                                                     'Data::Printer' => '0',
                                                     'Exporter' => '0',
                                                     'Filter::Simple' => '0',
                                                     'PadWalker' => '0',
                                                     'Scalar::Util' => '0',
                                                     'Term::ANSIColor' => '0',
                                                     'Term::ReadLine' => '0',
                                                     'feature' => '0',
                                                     'parent' => '0',
                                                     'perl' => '5.012',
                                                     'strict' => '0',
                                                     'subs' => '0',
                                                     'warnings' => '0'
                                                   }
                                   },
                      'test' => {
                                  'requires' => {
                                                  'Term::ANSIColor' => '0',
                                                  'Test::More' => '0',
                                                  'feature' => '0',
                                                  'perl' => '5.006',
                                                  'strict' => '0',
                                                  'warnings' => '0'
                                                },
                                  'suggests' => {
                                                  'Pod::Coverage' => '0',
                                                  'Test::CheckManifest' => '0',
                                                  'Test::Pod' => '0',
                                                  'Test::Pod::Coverage' => '0'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'Runtime-Debugger'
        };
1;
