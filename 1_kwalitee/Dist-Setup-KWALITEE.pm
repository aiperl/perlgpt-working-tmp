$distribution_kwalitee = $VAR1 = {
          'author' => '',
          'base_dirs' => [
                           'data'
                         ],
          'dir_lib' => 'lib',
          'dir_t' => 't,data/t',
          'dirs' => 9,
          'dirs_array' => [
                            'data/dot_devcontainer',
                            'data/dot_github/workflows',
                            'data/dot_github',
                            'data/t',
                            'data',
                            'lib/Dist',
                            'lib',
                            'script',
                            't'
                          ],
          'dist' => 'Dist-Setup',
          'dynamic_config' => 0,
          'error' => {
                       'extracts_nicely' => 'expected Dist-Setup but got Dist-Setup-0.10'
                     },
          'extension' => 'tar.gz',
          'external_license_file' => 'LICENSE',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_changelog' => 'Changes,data/Changes.raw,data/Changes.raw.cond',
          'file_cpanfile' => 'cpanfile,data/cpanfile',
          'file_license' => 'LICENSE',
          'file_makefile_pl' => 'Makefile.PL,data/Makefile.PL',
          'file_manifest' => 'MANIFEST',
          'file_manifest_skip' => 'data/MANIFEST.SKIP',
          'file_meta_json' => 'META.json',
          'file_meta_yml' => 'META.yml',
          'files' => 32,
          'files_array' => [
                             'Changes',
                             'LICENSE',
                             'MANIFEST',
                             'META.json',
                             'META.yml',
                             'Makefile.PL',
                             'cpanfile',
                             'data/Changes.raw',
                             'data/Changes.raw.cond',
                             'data/LICENSE.raw',
                             'data/MANIFEST.SKIP',
                             'data/Makefile.PL',
                             'data/cpanfile',
                             'data/dist_setup.conf',
                             'data/dot_devcontainer/.cond',
                             'data/dot_devcontainer/Dockerfile',
                             'data/dot_devcontainer/devcontainer.json.raw',
                             'data/dot_devcontainer/update_content.sh',
                             'data/dot_github/.cond',
                             'data/dot_github/workflows/ci.yml',
                             'data/dot_gitignore',
                             'data/dot_perlcriticrc',
                             'data/dot_perltidyrc',
                             'data/t/000-load.t',
                             'data/t/001-cpanfile.t',
                             'data/t/001-perlcritic.t',
                             'data/t/001-podsyntax.t',
                             'data/t/001-spelling.t',
                             'data/tt_footer',
                             'data/tt_header',
                             'lib/Dist/Setup.pm',
                             'script/perl_dist_setup'
                           ],
          'files_hash' => {
                            'Changes' => {
                                           'mtime' => 1709333777,
                                           'size' => 1708
                                         },
                            'LICENSE' => {
                                           'mtime' => 1709333667,
                                           'size' => 1053
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1709333837,
                                            'size' => 851
                                          },
                            'META.json' => {
                                             'mtime' => 1709333837,
                                             'size' => 1936
                                           },
                            'META.yml' => {
                                            'mtime' => 1709333836,
                                            'size' => 931
                                          },
                            'Makefile.PL' => {
                                               'mtime' => 1709333667,
                                               'requires' => {
                                                               'ExtUtils::MakeMaker::CPANfile' => '0',
                                                               'File::ShareDir::Install' => '0',
                                                               'perl' => '5.026',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 2781
                                             },
                            'cpanfile' => {
                                            'mtime' => 1709333667,
                                            'size' => 962
                                          },
                            'data/Changes.raw' => {
                                                    'mtime' => 1706357534,
                                                    'size' => 158
                                                  },
                            'data/Changes.raw.cond' => {
                                                         'mtime' => 1706357034,
                                                         'size' => 26
                                                       },
                            'data/LICENSE.raw' => {
                                                    'mtime' => 1703626415,
                                                    'size' => 1073
                                                  },
                            'data/MANIFEST.SKIP' => {
                                                      'mtime' => 1708250319,
                                                      'size' => 489
                                                    },
                            'data/Makefile.PL' => {
                                                    'mtime' => 1708251460,
                                                    'requires' => {
                                                                    'ExtUtils::MakeMaker::CPANfile' => '0',
                                                                    'File::ShareDir::Install' => '0',
                                                                    'strict' => '0',
                                                                    'warnings' => '0'
                                                                  },
                                                    'size' => 2776
                                                  },
                            'data/cpanfile' => {
                                                 'mtime' => 1709333645,
                                                 'size' => 688
                                               },
                            'data/dist_setup.conf' => {
                                                        'mtime' => 1706305692,
                                                        'size' => 1351
                                                      },
                            'data/dot_devcontainer/.cond' => {
                                                               'mtime' => 1705012260,
                                                               'size' => 33
                                                             },
                            'data/dot_devcontainer/Dockerfile' => {
                                                                    'mtime' => 1706397306,
                                                                    'size' => 611
                                                                  },
                            'data/dot_devcontainer/devcontainer.json.raw' => {
                                                                               'mtime' => 1705009243,
                                                                               'size' => 110
                                                                             },
                            'data/dot_devcontainer/update_content.sh' => {
                                                                           'mtime' => 1706301580,
                                                                           'size' => 496
                                                                         },
                            'data/dot_github/.cond' => {
                                                         'mtime' => 1705011711,
                                                         'size' => 23
                                                       },
                            'data/dot_github/workflows/ci.yml' => {
                                                                    'mtime' => 1706824194,
                                                                    'size' => 3207
                                                                  },
                            'data/dot_gitignore' => {
                                                      'mtime' => 1707069533,
                                                      'size' => 162
                                                    },
                            'data/dot_perlcriticrc' => {
                                                         'mtime' => 1706910084,
                                                         'size' => 2132
                                                       },
                            'data/dot_perltidyrc' => {
                                                       'mtime' => 1706821073,
                                                       'size' => 3400
                                                     },
                            'data/t/000-load.t' => {
                                                     'mtime' => 1706824816,
                                                     'size' => 352
                                                   },
                            'data/t/001-cpanfile.t' => {
                                                         'mtime' => 1706951295,
                                                         'size' => 216
                                                       },
                            'data/t/001-perlcritic.t' => {
                                                           'mtime' => 1708250387,
                                                           'size' => 792
                                                         },
                            'data/t/001-podsyntax.t' => {
                                                          'mtime' => 1708250398,
                                                          'size' => 950
                                                        },
                            'data/t/001-spelling.t' => {
                                                         'mtime' => 1708250402,
                                                         'size' => 2792
                                                       },
                            'data/tt_footer' => {
                                                  'mtime' => 1706308054,
                                                  'size' => 41
                                                },
                            'data/tt_header' => {
                                                  'mtime' => 1706308054,
                                                  'size' => 136
                                                },
                            'lib/Dist/Setup.pm' => {
                                                     'module' => 'Dist::Setup',
                                                     'mtime' => 1709333730,
                                                     'requires' => {
                                                                     'Eval::Safe' => '0',
                                                                     'File::Basename' => '0',
                                                                     'File::Copy' => '0',
                                                                     'File::Find' => '0',
                                                                     'File::Spec::Functions' => '0',
                                                                     'Template' => '0',
                                                                     'Time::localtime' => '0',
                                                                     'feature' => '0',
                                                                     'strict' => '0',
                                                                     'utf8' => '0',
                                                                     'version' => '0.77',
                                                                     'warnings' => '0'
                                                                   },
                                                     'size' => 4047
                                                   },
                            'script/perl_dist_setup' => {
                                                          'license' => 'MIT',
                                                          'mtime' => 1707168345,
                                                          'size' => 9578
                                                        },
                            't/000-load.t' => {
                                                'mtime' => 1709333667,
                                                'no_index' => 1,
                                                'noes' => {
                                                            'warnings' => '0'
                                                          },
                                                'requires' => {
                                                                'Test2::V0' => '0',
                                                                'strict' => '0',
                                                                'warnings' => '0'
                                                              },
                                                'size' => 538,
                                                'suggests' => {
                                                                'Dist::Setup' => '0'
                                                              }
                                              },
                            't/001-cpanfile.t' => {
                                                    'mtime' => 1709333667,
                                                    'no_index' => 1,
                                                    'requires' => {
                                                                    'Test2::V0' => '0',
                                                                    'Test::CPANfile' => '0',
                                                                    'strict' => '0',
                                                                    'warnings' => '0'
                                                                  },
                                                    'size' => 382
                                                  },
                            't/001-podsyntax.t' => {
                                                     'mtime' => 1709333667,
                                                     'no_index' => 1,
                                                     'requires' => {
                                                                     'English' => '0',
                                                                     'FindBin' => '0',
                                                                     'Readonly' => '0',
                                                                     'Test2::V0' => '0',
                                                                     'strict' => '0',
                                                                     'warnings' => '0'
                                                                   },
                                                     'size' => 1132,
                                                     'suggests' => {
                                                                     'Test::Pod' => '0'
                                                                   }
                                                   }
                          },
          'got_prereq_from' => 'META.yml',
          'ignored_files_array' => [
                                     't/000-load.t',
                                     't/001-cpanfile.t',
                                     't/001-podsyntax.t'
                                   ],
          'kwalitee' => {
                          'has_abstract_in_pod' => 0,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 1,
                          'has_meta_yml' => 1,
                          'has_readme' => 0,
                          'has_separate_license_file' => 1,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 30,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 0,
                          'meta_yml_has_repository_resource' => 1,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 1,
                          'use_warnings' => 1
                        },
          'latest_mtime' => 1709333837,
          'license' => 'mit defined in META.yml defined in LICENSE',
          'license_file' => 'script/perl_dist_setup',
          'license_from_yaml' => 'mit',
          'license_in_pod' => 1,
          'license_type' => 'MIT',
          'licenses' => {
                          'MIT' => [
                                     'script/perl_dist_setup'
                                   ]
                        },
          'manifest_matches_dist' => 1,
          'meta_json' => {
                           'abstract' => 'Simple opinionated tool to set up a Perl distribution directory.',
                           'author' => [
                                         'Mathias Kende <mathias@cpan.org>'
                                       ],
                           'dynamic_config' => 0,
                           'generated_by' => 'ExtUtils::MakeMaker version 7.44, CPAN::Meta::Converter version 2.150010',
                           'license' => [
                                          'mit'
                                        ],
                           'meta-spec' => {
                                            'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                            'version' => 2
                                          },
                           'name' => 'Dist-Setup',
                           'no_index' => {
                                           'directory' => [
                                                            't',
                                                            'inc',
                                                            'local',
                                                            'vendor'
                                                          ]
                                         },
                           'prereqs' => {
                                          'build' => {
                                                       'requires' => {
                                                                       'ExtUtils::MakeMaker' => '0'
                                                                     }
                                                     },
                                          'configure' => {
                                                           'requires' => {
                                                                           'ExtUtils::MakeMaker::CPANfile' => '0.09',
                                                                           'File::ShareDir::Install' => '0',
                                                                           'perl' => 'v5.26.0'
                                                                         }
                                                         },
                                          'develop' => {
                                                         'recommends' => {
                                                                           'Devel::Cover' => '0'
                                                                         }
                                                       },
                                          'runtime' => {
                                                         'requires' => {
                                                                         'Eval::Safe' => '0',
                                                                         'File::ShareDir' => '0',
                                                                         'Template' => '0',
                                                                         'perl' => '5.026'
                                                                       }
                                                       },
                                          'test' => {
                                                      'recommends' => {
                                                                        'Test::Pod' => '1.22'
                                                                      },
                                                      'requires' => {
                                                                      'Readonly' => '0',
                                                                      'Test2::V0' => '0',
                                                                      'Test::CPANfile' => '0',
                                                                      'Test::More' => '0'
                                                                    },
                                                      'suggests' => {
                                                                      'IPC::Run3' => '0',
                                                                      'Perl::Tidy' => '20220613',
                                                                      'Test2::Tools::PerlCritic' => '0'
                                                                    }
                                                    }
                                        },
                           'release_status' => 'stable',
                           'resources' => {
                                            'bugtracker' => {
                                                              'web' => 'https://github.com/mkende/perl_dist_setup/issues'
                                                            },
                                            'repository' => {
                                                              'type' => 'git',
                                                              'web' => 'https://github.com/mkende/perl_dist_setup'
                                                            }
                                          },
                           'version' => '0.10',
                           'x_serialization_backend' => 'JSON::PP version 4.04'
                         },
          'meta_json_is_parsable' => 1,
          'meta_json_spec_version' => 2,
          'meta_yml' => {
                          'abstract' => 'Simple opinionated tool to set up a Perl distribution directory.',
                          'author' => [
                                        'Mathias Kende <mathias@cpan.org>'
                                      ],
                          'build_requires' => {
                                                'ExtUtils::MakeMaker' => '0',
                                                'Readonly' => '0',
                                                'Test2::V0' => '0',
                                                'Test::CPANfile' => '0',
                                                'Test::More' => '0'
                                              },
                          'configure_requires' => {
                                                    'ExtUtils::MakeMaker::CPANfile' => '0.09',
                                                    'File::ShareDir::Install' => '0',
                                                    'perl' => 'v5.26.0'
                                                  },
                          'dynamic_config' => '0',
                          'generated_by' => 'ExtUtils::MakeMaker version 7.44, CPAN::Meta::Converter version 2.150010',
                          'license' => 'mit',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'Dist-Setup',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc',
                                                           'local',
                                                           'vendor'
                                                         ]
                                        },
                          'requires' => {
                                          'Eval::Safe' => '0',
                                          'File::ShareDir' => '0',
                                          'Template' => '0',
                                          'perl' => '5.026'
                                        },
                          'resources' => {
                                           'bugtracker' => 'https://github.com/mkende/perl_dist_setup/issues',
                                           'repository' => 'https://github.com/mkende/perl_dist_setup'
                                         },
                          'version' => '0.10',
                          'x_serialization_backend' => 'CPAN::Meta::YAML version 0.018'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/Dist/Setup.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Dist::Setup'
                         }
                       ],
          'no_index' => '^inc/;^local/;^t/;^vendor/',
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/Dist-Setup.tar.gz',
          'prereq' => [
                        {
                          'is_prereq' => 1,
                          'requires' => 'Eval::Safe',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'ExtUtils::MakeMaker::CPANfile',
                          'type' => 'configure_requires',
                          'version' => '0.09'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'File::ShareDir',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'File::ShareDir::Install',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Readonly',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Template',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test2::V0',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::CPANfile',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'perl',
                          'type' => 'configure_requires',
                          'version' => 'v5.26.0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => '5.026'
                        }
                      ],
          'released' => 1709583477,
          'size_packed' => 16958,
          'size_unpacked' => 47913,
          'test_files' => [
                            't/000-load.t',
                            't/001-cpanfile.t',
                            't/001-podsyntax.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'requires' => {
                                                       'ExtUtils::MakeMaker::CPANfile' => '0',
                                                       'File::ShareDir::Install' => '0',
                                                       'perl' => '5.026',
                                                       'strict' => '0',
                                                       'warnings' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'requires' => {
                                                     'Eval::Safe' => '0',
                                                     'File::Basename' => '0',
                                                     'File::Copy' => '0',
                                                     'File::Find' => '0',
                                                     'File::Spec::Functions' => '0',
                                                     'Template' => '0',
                                                     'Time::localtime' => '0',
                                                     'feature' => '0',
                                                     'strict' => '0',
                                                     'utf8' => '0',
                                                     'version' => '0.77',
                                                     'warnings' => '0'
                                                   }
                                   },
                      'test' => {
                                  'noes' => {
                                              'warnings' => '0'
                                            },
                                  'requires' => {
                                                  'English' => '0',
                                                  'FindBin' => '0',
                                                  'Readonly' => '0',
                                                  'Test2::V0' => '0',
                                                  'Test::CPANfile' => '0',
                                                  'strict' => '0',
                                                  'warnings' => '0'
                                                },
                                  'suggests' => {
                                                  'Test::Pod' => '0'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'Dist-Setup'
        };
1;
