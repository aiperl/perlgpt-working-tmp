$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'Business::ISBN::Data' => 'data pack for Business::ISBN'
                                },
          'author' => '',
          'dir_lib' => 'lib',
          'dir_t' => 't',
          'dir_xt' => 'xt',
          'dirs' => 6,
          'dirs_array' => [
                            'examples',
                            'lib/Business/ISBN',
                            'lib/Business',
                            'lib',
                            't',
                            'xt'
                          ],
          'dist' => 'Business-ISBN-Data',
          'dynamic_config' => 1,
          'error' => {
                       'extracts_nicely' => 'expected Business-ISBN-Data but got Business-ISBN-Data-20240302.001',
                       'use_strict' => 'Business::ISBN::Data',
                       'use_warnings' => 'Business::ISBN::Data'
                     },
          'extension' => 'tar.gz',
          'external_license_file' => 'LICENSE',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_changelog' => 'Changes',
          'file_license' => 'LICENSE',
          'file_makefile_pl' => 'Makefile.PL',
          'file_manifest' => 'MANIFEST',
          'file_manifest_skip' => 'MANIFEST.SKIP',
          'file_meta_json' => 'META.json',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README.pod',
          'files' => 15,
          'files_array' => [
                             'Changes',
                             'INSTALL.SKIP',
                             'LICENSE',
                             'MANIFEST',
                             'MANIFEST.SKIP',
                             'META.json',
                             'META.yml',
                             'Makefile.PL',
                             'README.pod',
                             'examples/README',
                             'examples/make_default_data.pl',
                             'lib/Business/ISBN/Data.pm',
                             'lib/Business/ISBN/RangeMessage.url',
                             'lib/Business/ISBN/RangeMessage.xml',
                             'xt/changes.t'
                           ],
          'files_hash' => {
                            'Changes' => {
                                           'mtime' => 1709422410,
                                           'size' => 6829
                                         },
                            'INSTALL.SKIP' => {
                                                'mtime' => 1709422410,
                                                'size' => 110
                                              },
                            'LICENSE' => {
                                           'mtime' => 1709422410,
                                           'size' => 9187
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1709422411,
                                            'size' => 552
                                          },
                            'MANIFEST.SKIP' => {
                                                 'mtime' => 1709422410,
                                                 'size' => 1215
                                               },
                            'META.json' => {
                                             'mtime' => 1709422411,
                                             'size' => 1532
                                           },
                            'META.yml' => {
                                            'mtime' => 1709422411,
                                            'size' => 863
                                          },
                            'Makefile.PL' => {
                                               'mtime' => 1709422410,
                                               'noes' => {
                                                           'warnings' => '0'
                                                         },
                                               'recommends' => {
                                                                 'File::Spec' => '0'
                                                               },
                                               'requires' => {
                                                               'File::Spec::Functions' => '0',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 2765,
                                               'suggests' => {
                                                               'Test::Manifest' => '1.21'
                                                             }
                                             },
                            'README.pod' => {
                                              'mtime' => 1709422410,
                                              'size' => 4740
                                            },
                            'examples/README' => {
                                                   'mtime' => 1709422410,
                                                   'size' => 69
                                                 },
                            'examples/make_default_data.pl' => {
                                                                 'license' => 'Artistic_2_0',
                                                                 'mtime' => 1709422410,
                                                                 'size' => 4969
                                                               },
                            'lib/Business/ISBN/Data.pm' => {
                                                             'license' => 'Artistic_2_0',
                                                             'module' => 'Business::ISBN::Data',
                                                             'mtime' => 1709422410,
                                                             'requires' => {
                                                                             'perl' => '5.008',
                                                                             'utf8' => '0'
                                                                           },
                                                             'size' => 43382
                                                           },
                            'lib/Business/ISBN/RangeMessage.url' => {
                                                                      'mtime' => 1709422410,
                                                                      'size' => 59
                                                                    },
                            'lib/Business/ISBN/RangeMessage.xml' => {
                                                                      'mtime' => 1709422410,
                                                                      'size' => 204585
                                                                    },
                            't/business-isbn.t' => {
                                                     'mtime' => 1709422410,
                                                     'no_index' => 1,
                                                     'requires' => {
                                                                     'File::Basename' => '0',
                                                                     'Test::More' => '0.88'
                                                                   },
                                                     'size' => 747
                                                   },
                            't/check_data_structure.t' => {
                                                            'mtime' => 1709422410,
                                                            'no_index' => 1,
                                                            'requires' => {
                                                                            'File::Spec::Functions' => '0',
                                                                            'Test::More' => '0.88'
                                                                          },
                                                            'size' => 2979
                                                          },
                            't/default_data.t' => {
                                                    'mtime' => 1709422410,
                                                    'no_index' => 1,
                                                    'requires' => {
                                                                    'Data::Dumper' => '0',
                                                                    'File::Spec::Functions' => '0',
                                                                    'Test::More' => '0.88'
                                                                  },
                                                    'size' => 1436,
                                                    'suggests' => {
                                                                    'Business::ISBN' => '3.006'
                                                                  }
                                                  },
                            't/get_data.t' => {
                                                'mtime' => 1709422410,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'File::Basename' => '0',
                                                                'Test::More' => '0.88'
                                                              },
                                                'size' => 947
                                              },
                            't/load.t' => {
                                            'mtime' => 1709422410,
                                            'no_index' => 1,
                                            'requires' => {
                                                            'Test::More' => '0.88'
                                                          },
                                            'size' => 293
                                          },
                            't/parse.t' => {
                                             'mtime' => 1709422410,
                                             'no_index' => 1,
                                             'requires' => {
                                                             'File::Basename' => '0',
                                                             'Test::More' => '0.88'
                                                           },
                                             'size' => 544
                                           },
                            't/pod.t' => {
                                           'mtime' => 1709422410,
                                           'no_index' => 1,
                                           'requires' => {
                                                           'Test::More' => '0'
                                                         },
                                           'size' => 129,
                                           'suggests' => {
                                                           'Test::Pod' => '1.00'
                                                         }
                                         },
                            't/pod_coverage.t' => {
                                                    'mtime' => 1709422410,
                                                    'no_index' => 1,
                                                    'requires' => {
                                                                    'Test::More' => '0'
                                                                  },
                                                    'size' => 201,
                                                    'suggests' => {
                                                                    'Test::Pod::Coverage' => '0'
                                                                  }
                                                  },
                            't/test_manifest' => {
                                                   'mtime' => 1709422410,
                                                   'no_index' => 1,
                                                   'size' => 66
                                                 },
                            'xt/changes.t' => {
                                                'mtime' => 1709422410,
                                                'size' => 131
                                              }
                          },
          'got_prereq_from' => 'META.yml',
          'ignored_files_array' => [
                                     't/business-isbn.t',
                                     't/check_data_structure.t',
                                     't/default_data.t',
                                     't/get_data.t',
                                     't/load.t',
                                     't/parse.t',
                                     't/pod.t',
                                     't/pod_coverage.t',
                                     't/test_manifest'
                                   ],
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 1,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 1,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 30,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 0,
                          'meta_yml_has_repository_resource' => 1,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 0,
                          'use_warnings' => 0
                        },
          'latest_mtime' => 1709422411,
          'license' => 'artistic_2 defined in META.yml defined in LICENSE',
          'license_file' => 'examples/make_default_data.pl,lib/Business/ISBN/Data.pm',
          'license_from_yaml' => 'artistic_2',
          'license_in_pod' => 1,
          'license_type' => 'Artistic_2_0',
          'licenses' => {
                          'Artistic_2_0' => [
                                              'examples/make_default_data.pl',
                                              'lib/Business/ISBN/Data.pm'
                                            ]
                        },
          'manifest_matches_dist' => 1,
          'meta_json' => {
                           'abstract' => 'data pack for Business::ISBN',
                           'author' => [
                                         'brian d foy <briandfoy@pobox.com>'
                                       ],
                           'dynamic_config' => 1,
                           'generated_by' => 'ExtUtils::MakeMaker version 7.70, CPAN::Meta::Converter version 2.150010',
                           'license' => [
                                          'artistic_2'
                                        ],
                           'meta-spec' => {
                                            'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                            'version' => 2
                                          },
                           'name' => 'Business-ISBN-Data',
                           'no_index' => {
                                           'directory' => [
                                                            't',
                                                            'inc'
                                                          ]
                                         },
                           'prereqs' => {
                                          'build' => {
                                                       'requires' => {}
                                                     },
                                          'configure' => {
                                                           'requires' => {
                                                                           'ExtUtils::MakeMaker' => '6.64',
                                                                           'File::Spec::Functions' => '0'
                                                                         }
                                                         },
                                          'runtime' => {
                                                         'requires' => {
                                                                         'Carp' => '0',
                                                                         'File::Basename' => '0',
                                                                         'File::Spec::Functions' => '0',
                                                                         'perl' => '5.008'
                                                                       }
                                                       },
                                          'test' => {
                                                      'requires' => {
                                                                      'Test::More' => '1'
                                                                    }
                                                    }
                                        },
                           'release_status' => 'stable',
                           'resources' => {
                                            'bugtracker' => {
                                                              'web' => 'https://github.com/briandfoy/business-isbn-data/issues'
                                                            },
                                            'homepage' => 'https://github.com/briandfoy/business-isbn-data',
                                            'repository' => {
                                                              'type' => 'git',
                                                              'url' => 'https://github.com/briandfoy/business-isbn-data',
                                                              'web' => 'https://github.com/briandfoy/business-isbn-data'
                                                            }
                                          },
                           'version' => '20240302.001',
                           'x_serialization_backend' => 'JSON::PP version 4.16'
                         },
          'meta_json_is_parsable' => 1,
          'meta_json_spec_version' => 2,
          'meta_yml' => {
                          'abstract' => 'data pack for Business::ISBN',
                          'author' => [
                                        'brian d foy <briandfoy@pobox.com>'
                                      ],
                          'build_requires' => {
                                                'Test::More' => '1'
                                              },
                          'configure_requires' => {
                                                    'ExtUtils::MakeMaker' => '6.64',
                                                    'File::Spec::Functions' => '0'
                                                  },
                          'dynamic_config' => '1',
                          'generated_by' => 'ExtUtils::MakeMaker version 7.70, CPAN::Meta::Converter version 2.150010',
                          'license' => 'artistic_2',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'Business-ISBN-Data',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc'
                                                         ]
                                        },
                          'requires' => {
                                          'Carp' => '0',
                                          'File::Basename' => '0',
                                          'File::Spec::Functions' => '0',
                                          'perl' => '5.008'
                                        },
                          'resources' => {
                                           'bugtracker' => 'https://github.com/briandfoy/business-isbn-data/issues',
                                           'homepage' => 'https://github.com/briandfoy/business-isbn-data',
                                           'repository' => 'https://github.com/briandfoy/business-isbn-data'
                                         },
                          'version' => '20240302.001',
                          'x_serialization_backend' => 'CPAN::Meta::YAML version 0.018'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/Business/ISBN/Data.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Business::ISBN::Data'
                         }
                       ],
          'no_index' => '^inc/;^t/',
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/Business-ISBN-Data.tar.gz',
          'prereq' => [
                        {
                          'is_prereq' => 1,
                          'requires' => 'Carp',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'configure_requires',
                          'version' => '6.64'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'File::Basename',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'File::Spec::Functions',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'File::Spec::Functions',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'build_requires',
                          'version' => '1'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => '5.008'
                        }
                      ],
          'released' => 1709583472,
          'size_packed' => 34005,
          'size_unpacked' => 288330,
          'test_files' => [
                            't/business-isbn.t',
                            't/check_data_structure.t',
                            't/default_data.t',
                            't/get_data.t',
                            't/load.t',
                            't/parse.t',
                            't/pod.t',
                            't/pod_coverage.t'
                          ],
          'unknown_license_texts' => {
                                       'README.pod' => '=head2 Copyright and License

You should have received a I<LICENSE> file, but the license is also noted
in the module files. About the only thing you can\'t do is pretend that
you wrote code that you didn\'t.

'
                                     },
          'uses' => {
                      'configure' => {
                                       'noes' => {
                                                   'warnings' => '0'
                                                 },
                                       'recommends' => {
                                                         'File::Spec' => '0'
                                                       },
                                       'requires' => {
                                                       'File::Spec::Functions' => '0',
                                                       'strict' => '0',
                                                       'warnings' => '0'
                                                     },
                                       'suggests' => {
                                                       'Test::Manifest' => '1.21'
                                                     }
                                     },
                      'runtime' => {
                                     'requires' => {
                                                     'perl' => '5.008',
                                                     'utf8' => '0'
                                                   }
                                   },
                      'test' => {
                                  'requires' => {
                                                  'Data::Dumper' => '0',
                                                  'File::Basename' => '0',
                                                  'File::Spec::Functions' => '0',
                                                  'Test::More' => '0.88'
                                                },
                                  'suggests' => {
                                                  'Business::ISBN' => '3.006',
                                                  'Test::Pod' => '1.00',
                                                  'Test::Pod::Coverage' => '0'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'Business-ISBN-Data'
        };
1;
