$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'AI::Chat' => 'Interact with AI Chat APIs'
                                },
          'author' => '',
          'dir_lib' => 'lib',
          'dir_t' => 't',
          'dirs' => 3,
          'dirs_array' => [
                            'lib/AI',
                            'lib',
                            't'
                          ],
          'dist' => 'AI-Chat',
          'dynamic_config' => 1,
          'error' => {
                       'extracts_nicely' => 'expected AI-Chat but got AI-Chat-0.2'
                     },
          'extension' => 'tar.gz',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_changelog' => 'Changes',
          'file_makefile_pl' => 'Makefile.PL',
          'file_manifest' => 'MANIFEST',
          'file_meta_json' => 'META.json',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README',
          'files' => 7,
          'files_array' => [
                             'Changes',
                             'MANIFEST',
                             'META.json',
                             'META.yml',
                             'Makefile.PL',
                             'README',
                             'lib/AI/Chat.pm'
                           ],
          'files_hash' => {
                            'Changes' => {
                                           'mtime' => 1709500374,
                                           'size' => 239
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1709500417,
                                            'size' => 327
                                          },
                            'META.json' => {
                                             'mtime' => 1709500417,
                                             'size' => 1105
                                           },
                            'META.yml' => {
                                            'mtime' => 1709500414,
                                            'size' => 615
                                          },
                            'Makefile.PL' => {
                                               'mtime' => 1709416780,
                                               'requires' => {
                                                               'ExtUtils::MakeMaker' => '0',
                                                               'perl' => '5.010',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 1764
                                             },
                            'README' => {
                                          'mtime' => 1709416869,
                                          'size' => 1261
                                        },
                            'lib/AI/Chat.pm' => {
                                                  'license' => 'Perl_5',
                                                  'module' => 'AI::Chat',
                                                  'mtime' => 1709499995,
                                                  'requires' => {
                                                                  'Carp' => '0',
                                                                  'HTTP::Tiny' => '0',
                                                                  'JSON::PP' => '0',
                                                                  'strict' => '0',
                                                                  'warnings' => '0'
                                                                },
                                                  'size' => 6495
                                                },
                            't/00-load.t' => {
                                               'mtime' => 1709416383,
                                               'no_index' => 1,
                                               'requires' => {
                                                               'Test::More' => '0',
                                                               'perl' => '5.006',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 213
                                             },
                            't/01-openai.t' => {
                                                 'mtime' => 1709417225,
                                                 'no_index' => 1,
                                                 'requires' => {
                                                                 'AI::Chat' => '0',
                                                                 'Test::More' => '0.88',
                                                                 'perl' => '5.006',
                                                                 'strict' => '0',
                                                                 'warnings' => '0'
                                                               },
                                                 'size' => 708
                                               },
                            't/manifest.t' => {
                                                'mtime' => 1709416383,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'Test::More' => '0',
                                                                'perl' => '5.006',
                                                                'strict' => '0',
                                                                'warnings' => '0'
                                                              },
                                                'size' => 324,
                                                'suggests' => {
                                                                'Test::CheckManifest' => '0'
                                                              }
                                              },
                            't/pod-coverage.t' => {
                                                    'mtime' => 1709416383,
                                                    'no_index' => 1,
                                                    'requires' => {
                                                                    'Test::More' => '0',
                                                                    'perl' => '5.006',
                                                                    'strict' => '0',
                                                                    'warnings' => '0'
                                                                  },
                                                    'size' => 701,
                                                    'suggests' => {
                                                                    'Pod::Coverage' => '0',
                                                                    'Test::Pod::Coverage' => '0'
                                                                  }
                                                  },
                            't/pod.t' => {
                                           'mtime' => 1709416383,
                                           'no_index' => 1,
                                           'requires' => {
                                                           'Test::More' => '0',
                                                           'perl' => '5.006',
                                                           'strict' => '0',
                                                           'warnings' => '0'
                                                         },
                                           'size' => 363,
                                           'suggests' => {
                                                           'Test::Pod' => '0'
                                                         }
                                         },
                            't/version.t' => {
                                               'mtime' => 1709417288,
                                               'no_index' => 1,
                                               'requires' => {
                                                               'AI::Chat' => '0',
                                                               'Test::More' => '0.88',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 619
                                             }
                          },
          'got_prereq_from' => 'META.yml',
          'ignored_files_array' => [
                                     't/00-load.t',
                                     't/01-openai.t',
                                     't/manifest.t',
                                     't/pod-coverage.t',
                                     't/pod.t',
                                     't/version.t'
                                   ],
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 1,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 0,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 30,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 0,
                          'meta_yml_has_repository_resource' => 0,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 1,
                          'use_warnings' => 1
                        },
          'latest_mtime' => 1709500417,
          'license' => 'perl defined in META.yml',
          'license_file' => 'lib/AI/Chat.pm',
          'license_from_yaml' => 'perl',
          'license_in_pod' => 1,
          'license_type' => 'Perl_5',
          'licenses' => {
                          'Perl_5' => [
                                        'lib/AI/Chat.pm'
                                      ]
                        },
          'manifest_matches_dist' => 1,
          'meta_json' => {
                           'abstract' => 'Interact with AI Chat APIs',
                           'author' => [
                                         'Ian Boddison <bod@cpan.org>'
                                       ],
                           'dynamic_config' => 1,
                           'generated_by' => 'ExtUtils::MakeMaker version 7.58, CPAN::Meta::Converter version 2.150010',
                           'license' => [
                                          'perl_5'
                                        ],
                           'meta-spec' => {
                                            'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                            'version' => 2
                                          },
                           'name' => 'AI-Chat',
                           'no_index' => {
                                           'directory' => [
                                                            't',
                                                            'inc'
                                                          ]
                                         },
                           'prereqs' => {
                                          'build' => {
                                                       'requires' => {
                                                                       'ExtUtils::MakeMaker' => '0'
                                                                     }
                                                     },
                                          'configure' => {
                                                           'requires' => {
                                                                           'ExtUtils::MakeMaker' => '0'
                                                                         }
                                                         },
                                          'runtime' => {
                                                         'requires' => {
                                                                         'Carp' => '1.50',
                                                                         'HTTP::Tiny' => '0.014',
                                                                         'JSON::PP' => '2.00',
                                                                         'perl' => '5.010'
                                                                       }
                                                       },
                                          'test' => {
                                                      'requires' => {
                                                                      'Test::More' => '0'
                                                                    }
                                                    }
                                        },
                           'release_status' => 'stable',
                           'version' => '0.2',
                           'x_serialization_backend' => 'JSON::PP version 4.06'
                         },
          'meta_json_is_parsable' => 1,
          'meta_json_spec_version' => 2,
          'meta_yml' => {
                          'abstract' => 'Interact with AI Chat APIs',
                          'author' => [
                                        'Ian Boddison <bod@cpan.org>'
                                      ],
                          'build_requires' => {
                                                'ExtUtils::MakeMaker' => '0',
                                                'Test::More' => '0'
                                              },
                          'configure_requires' => {
                                                    'ExtUtils::MakeMaker' => '0'
                                                  },
                          'dynamic_config' => '1',
                          'generated_by' => 'ExtUtils::MakeMaker version 7.58, CPAN::Meta::Converter version 2.150010',
                          'license' => 'perl',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'AI-Chat',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc'
                                                         ]
                                        },
                          'requires' => {
                                          'Carp' => '1.50',
                                          'HTTP::Tiny' => '0.014',
                                          'JSON::PP' => '2.00',
                                          'perl' => '5.010'
                                        },
                          'version' => '0.2',
                          'x_serialization_backend' => 'CPAN::Meta::YAML version 0.018'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/AI/Chat.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'AI::Chat'
                         }
                       ],
          'no_index' => '^inc/;^t/',
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/AI-Chat.tar.gz',
          'prereq' => [
                        {
                          'is_prereq' => 1,
                          'requires' => 'Carp',
                          'type' => 'runtime_requires',
                          'version' => '1.50'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'HTTP::Tiny',
                          'type' => 'runtime_requires',
                          'version' => '0.014'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'JSON::PP',
                          'type' => 'runtime_requires',
                          'version' => '2.00'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => '5.010'
                        }
                      ],
          'released' => 1709583457,
          'size_packed' => 5773,
          'size_unpacked' => 14734,
          'test_files' => [
                            't/00-load.t',
                            't/01-openai.t',
                            't/manifest.t',
                            't/pod-coverage.t',
                            't/pod.t',
                            't/version.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'requires' => {
                                                       'ExtUtils::MakeMaker' => '0',
                                                       'perl' => '5.010',
                                                       'strict' => '0',
                                                       'warnings' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'requires' => {
                                                     'Carp' => '0',
                                                     'HTTP::Tiny' => '0',
                                                     'JSON::PP' => '0',
                                                     'strict' => '0',
                                                     'warnings' => '0'
                                                   }
                                   },
                      'test' => {
                                  'requires' => {
                                                  'Test::More' => '0.88',
                                                  'perl' => '5.006',
                                                  'strict' => '0',
                                                  'warnings' => '0'
                                                },
                                  'suggests' => {
                                                  'Pod::Coverage' => '0',
                                                  'Test::CheckManifest' => '0',
                                                  'Test::Pod' => '0',
                                                  'Test::Pod::Coverage' => '0'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'AI-Chat'
        };
1;
