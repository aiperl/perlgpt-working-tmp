$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'Hash::Wrap' => 'create on-the-fly objects from hashes'
                                },
          'author' => '',
          'dir_lib' => 'lib',
          'dir_t' => 't',
          'dir_xt' => 'xt',
          'dirs' => 8,
          'dirs_array' => [
                            'dev',
                            'lib/Hash',
                            'lib',
                            't',
                            'templates',
                            'xt/author',
                            'xt/release',
                            'xt'
                          ],
          'dist' => 'Hash-Wrap',
          'dynamic_config' => 0,
          'error' => {
                       'extracts_nicely' => 'expected Hash-Wrap but got Hash-Wrap-1.01'
                     },
          'extension' => 'tar.gz',
          'external_license_file' => 'LICENSE',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_build_pl' => 'Build.PL',
          'file_changelog' => 'Changes',
          'file_dist_ini' => 'dist.ini',
          'file_license' => 'LICENSE',
          'file_makefile_pl' => 'Makefile.PL',
          'file_manifest' => 'MANIFEST',
          'file_meta_json' => 'META.json',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README',
          'files' => 18,
          'files_array' => [
                             'Build.PL',
                             'CONTRIBUTING.md',
                             'Changes',
                             'LICENSE',
                             'MANIFEST',
                             'META.json',
                             'META.yml',
                             'Makefile.PL',
                             'README',
                             'dev/pre-commit.sh',
                             'dist.ini',
                             'hgrc',
                             'lib/Hash/Wrap.pm',
                             'perlcritic.rc',
                             'perltidy.rc',
                             'templates/CONTRIBUTING.md',
                             'tidyall.ini',
                             'weaver.ini'
                           ],
          'files_hash' => {
                            'Build.PL' => {
                                            'mtime' => 1709308134,
                                            'requires' => {
                                                            'Module::Build::Tiny' => '0.034',
                                                            'perl' => '5.010',
                                                            'strict' => '0',
                                                            'warnings' => '0'
                                                          },
                                            'size' => 174
                                          },
                            'CONTRIBUTING.md' => {
                                                   'mtime' => 1709308134,
                                                   'size' => 3854
                                                 },
                            'Changes' => {
                                           'mtime' => 1709308134,
                                           'size' => 5608
                                         },
                            'LICENSE' => {
                                           'mtime' => 1709308134,
                                           'size' => 35322
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1709308134,
                                            'size' => 901
                                          },
                            'META.json' => {
                                             'mtime' => 1709308134,
                                             'size' => 5120
                                           },
                            'META.yml' => {
                                            'mtime' => 1709308134,
                                            'size' => 973
                                          },
                            'Makefile.PL' => {
                                               'mtime' => 1709308134,
                                               'requires' => {
                                                               'ExtUtils::MakeMaker' => '0',
                                                               'perl' => '5.010',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 1269
                                             },
                            'README' => {
                                          'mtime' => 1709308134,
                                          'size' => 1776
                                        },
                            'dev/pre-commit.sh' => {
                                                     'mtime' => 1709308134,
                                                     'size' => 140
                                                   },
                            'dist.ini' => {
                                            'mtime' => 1709308134,
                                            'size' => 2741
                                          },
                            'hgrc' => {
                                        'mtime' => 1709308134,
                                        'size' => 120
                                      },
                            'lib/Hash/Wrap.pm' => {
                                                    'license' => 'GPL_3',
                                                    'module' => 'Hash::Wrap',
                                                    'mtime' => 1709308134,
                                                    'noes' => {
                                                                'strict' => '0'
                                                              },
                                                    'recommends' => {
                                                                      'Carp' => '0'
                                                                    },
                                                    'requires' => {
                                                                    'Digest::MD5' => '0',
                                                                    'Scalar::Util' => '0',
                                                                    'constant' => '0',
                                                                    'perl' => '5.01000',
                                                                    'strict' => '0',
                                                                    'warnings' => '0'
                                                                  },
                                                    'size' => 40887
                                                  },
                            'perlcritic.rc' => {
                                                 'mtime' => 1709308134,
                                                 'size' => 184
                                               },
                            'perltidy.rc' => {
                                               'mtime' => 1709308134,
                                               'size' => 64
                                             },
                            't/00-compile.t' => {
                                                  'mtime' => 1709308134,
                                                  'no_index' => 1,
                                                  'requires' => {
                                                                  'File::Spec' => '0',
                                                                  'IO::Handle' => '0',
                                                                  'IPC::Open3' => '0',
                                                                  'Test::More' => '0',
                                                                  'perl' => '5.006',
                                                                  'strict' => '0',
                                                                  'warnings' => '0'
                                                                },
                                                  'size' => 1434,
                                                  'suggests' => {
                                                                  'blib' => '1.01'
                                                                }
                                                },
                            't/00-report-prereqs.dd' => {
                                                          'mtime' => 1709308134,
                                                          'no_index' => 1,
                                                          'size' => 6078
                                                        },
                            't/00-report-prereqs.t' => {
                                                         'mtime' => 1709308134,
                                                         'no_index' => 1,
                                                         'requires' => {
                                                                         'ExtUtils::MakeMaker' => '0',
                                                                         'File::Spec' => '0',
                                                                         'Test::More' => '0',
                                                                         'strict' => '0',
                                                                         'warnings' => '0'
                                                                       },
                                                         'size' => 6017
                                                       },
                            't/api.t' => {
                                           'mtime' => 1709308134,
                                           'no_index' => 1,
                                           'noes' => {
                                                       'warnings' => '0'
                                                     },
                                           'requires' => {
                                                           'Hash::Wrap' => '0',
                                                           'Test2::V0' => '0',
                                                           'if' => '0'
                                                         },
                                           'size' => 2272
                                         },
                            't/as_return.t' => {
                                                 'mtime' => 1709308134,
                                                 'no_index' => 1,
                                                 'requires' => {
                                                                 'Hash::Wrap' => '0',
                                                                 'Test2::V0' => '0'
                                                               },
                                                 'size' => 1025
                                               },
                            't/as_scalar_ref.t' => {
                                                     'mtime' => 1709308134,
                                                     'no_index' => 1,
                                                     'requires' => {
                                                                     'Hash::Wrap' => '0',
                                                                     'Test2::V0' => '0'
                                                                   },
                                                     'size' => 812
                                                   },
                            't/basic.t' => {
                                             'mtime' => 1709308134,
                                             'no_index' => 1,
                                             'noes' => {
                                                         'strict' => '0'
                                                       },
                                             'recommends' => {
                                                               'Storable' => '0'
                                                             },
                                             'requires' => {
                                                             'Hash::Wrap' => '0',
                                                             'Scalar::Util' => '0',
                                                             'Test2::V0' => '0'
                                                           },
                                             'size' => 4204
                                           },
                            't/croak.t' => {
                                             'mtime' => 1709308134,
                                             'no_index' => 1,
                                             'requires' => {
                                                             'Hash::Wrap' => '0',
                                                             'Test2::V0' => '0'
                                                           },
                                             'size' => 178
                                           },
                            't/defined.t' => {
                                               'mtime' => 1709308134,
                                               'no_index' => 1,
                                               'requires' => {
                                                               'Hash::Wrap' => '0',
                                                               'Scalar::Util' => '0',
                                                               'Test2::V0' => '0'
                                                             },
                                               'size' => 1047
                                             },
                            't/exists.t' => {
                                              'mtime' => 1709308134,
                                              'no_index' => 1,
                                              'requires' => {
                                                              'Hash::Wrap' => '0',
                                                              'Scalar::Util' => '0',
                                                              'Test2::V0' => '0'
                                                            },
                                              'size' => 993
                                            },
                            't/immutable.t' => {
                                                 'mtime' => 1709308134,
                                                 'no_index' => 1,
                                                 'requires' => {
                                                                 'Hash::Wrap' => '0',
                                                                 'Scalar::Util' => '0',
                                                                 'Test2::V0' => '0'
                                                               },
                                                 'size' => 1085
                                               },
                            't/import.t' => {
                                              'mtime' => 1709308134,
                                              'no_index' => 1,
                                              'requires' => {
                                                              'Hash::Wrap' => '0',
                                                              'Test2::V0' => '0'
                                                            },
                                              'size' => 353
                                            },
                            't/lockkeys.t' => {
                                                'mtime' => 1709308134,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'Hash::Wrap' => '0',
                                                                'Scalar::Util' => '0',
                                                                'Test2::V0' => '0'
                                                              },
                                                'size' => 2042
                                              },
                            't/lvalue.t' => {
                                              'mtime' => 1709308134,
                                              'no_index' => 1,
                                              'requires' => {
                                                              'Hash::Wrap' => '0',
                                                              'Scalar::Util' => '0',
                                                              'Test2::API' => '0',
                                                              'Test2::V0' => '0'
                                                            },
                                              'size' => 2054
                                            },
                            't/lvalue_undef.t' => {
                                                    'mtime' => 1709308134,
                                                    'no_index' => 1,
                                                    'recommends' => {
                                                                      'Hash::Wrap' => '0'
                                                                    },
                                                    'requires' => {
                                                                    'Scalar::Util' => '0',
                                                                    'Test2::API' => '0',
                                                                    'Test2::V0' => '0',
                                                                    'if' => '0'
                                                                  },
                                                    'size' => 1341
                                                  },
                            't/methods.t' => {
                                               'mtime' => 1709308134,
                                               'no_index' => 1,
                                               'requires' => {
                                                               'Hash::Wrap' => '0',
                                                               'Test2::V0' => '0'
                                                             },
                                               'size' => 1489
                                             },
                            't/predicate.t' => {
                                                 'mtime' => 1709308134,
                                                 'no_index' => 1,
                                                 'requires' => {
                                                                 'Hash::Wrap' => '0',
                                                                 'Scalar::Util' => '0',
                                                                 'Test2::V0' => '0'
                                                               },
                                                 'size' => 336
                                               },
                            't/recurse.t' => {
                                               'mtime' => 1709308134,
                                               'no_index' => 1,
                                               'requires' => {
                                                               'Hash::Util' => '0',
                                                               'Hash::Wrap' => '0',
                                                               'Scalar::Util' => '0',
                                                               'Test2::V0' => '0'
                                                             },
                                               'size' => 8642
                                             },
                            't/subclass.t' => {
                                                'mtime' => 1709308134,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'Hash::Wrap' => '0',
                                                                'Test2::V0' => '0'
                                                              },
                                                'size' => 771
                                              },
                            't/undef.t' => {
                                             'mtime' => 1709308134,
                                             'no_index' => 1,
                                             'requires' => {
                                                             'Hash::Wrap' => '0',
                                                             'Scalar::Util' => '0',
                                                             'Test2::API' => '0',
                                                             'Test2::V0' => '0'
                                                           },
                                             'size' => 1091
                                           },
                            'templates/CONTRIBUTING.md' => {
                                                             'mtime' => 1709308134,
                                                             'size' => 4891
                                                           },
                            'tidyall.ini' => {
                                               'mtime' => 1709308134,
                                               'size' => 126
                                             },
                            'weaver.ini' => {
                                              'mtime' => 1709308134,
                                              'size' => 880
                                            },
                            'xt/author/clean-namespaces.t' => {
                                                                'mtime' => 1709308134,
                                                                'no_index' => 1,
                                                                'size' => 241
                                                              },
                            'xt/author/critic.t' => {
                                                      'mtime' => 1709308134,
                                                      'no_index' => 1,
                                                      'size' => 129
                                                    },
                            'xt/author/no-breakpoints.t' => {
                                                              'mtime' => 1709308134,
                                                              'no_index' => 1,
                                                              'size' => 204
                                                            },
                            'xt/author/no-tabs.t' => {
                                                       'mtime' => 1709308134,
                                                       'no_index' => 1,
                                                       'size' => 633
                                                     },
                            'xt/author/pod-coverage.t' => {
                                                            'mtime' => 1709308134,
                                                            'no_index' => 1,
                                                            'size' => 245
                                                          },
                            'xt/author/pod-spell.t' => {
                                                         'mtime' => 1709308134,
                                                         'no_index' => 1,
                                                         'size' => 308
                                                       },
                            'xt/author/pod-syntax.t' => {
                                                          'mtime' => 1709308134,
                                                          'no_index' => 1,
                                                          'size' => 170
                                                        },
                            'xt/author/test-version.t' => {
                                                            'mtime' => 1709308134,
                                                            'no_index' => 1,
                                                            'size' => 415
                                                          },
                            'xt/release/cpan-changes.t' => {
                                                             'mtime' => 1709308134,
                                                             'no_index' => 1,
                                                             'size' => 228
                                                           },
                            'xt/release/dist-manifest.t' => {
                                                              'mtime' => 1709308134,
                                                              'no_index' => 1,
                                                              'size' => 82
                                                            },
                            'xt/release/fixme.t' => {
                                                      'mtime' => 1709308134,
                                                      'no_index' => 1,
                                                      'size' => 126
                                                    },
                            'xt/release/meta-json.t' => {
                                                          'mtime' => 1709308134,
                                                          'no_index' => 1,
                                                          'size' => 52
                                                        },
                            'xt/release/trailing-space.t' => {
                                                               'mtime' => 1709308134,
                                                               'no_index' => 1,
                                                               'size' => 540
                                                             },
                            'xt/release/unused-vars.t' => {
                                                            'mtime' => 1709308134,
                                                            'no_index' => 1,
                                                            'size' => 98
                                                          }
                          },
          'got_prereq_from' => 'META.yml',
          'ignored_files_array' => [
                                     't/00-compile.t',
                                     't/00-report-prereqs.dd',
                                     't/00-report-prereqs.t',
                                     't/api.t',
                                     't/as_return.t',
                                     't/as_scalar_ref.t',
                                     't/basic.t',
                                     't/croak.t',
                                     't/defined.t',
                                     't/exists.t',
                                     't/immutable.t',
                                     't/import.t',
                                     't/lockkeys.t',
                                     't/lvalue.t',
                                     't/lvalue_undef.t',
                                     't/methods.t',
                                     't/predicate.t',
                                     't/recurse.t',
                                     't/subclass.t',
                                     't/undef.t',
                                     'xt/author/clean-namespaces.t',
                                     'xt/author/critic.t',
                                     'xt/author/no-breakpoints.t',
                                     'xt/author/no-tabs.t',
                                     'xt/author/pod-coverage.t',
                                     'xt/author/pod-spell.t',
                                     'xt/author/pod-syntax.t',
                                     'xt/author/test-version.t',
                                     'xt/release/cpan-changes.t',
                                     'xt/release/dist-manifest.t',
                                     'xt/release/fixme.t',
                                     'xt/release/meta-json.t',
                                     'xt/release/trailing-space.t',
                                     'xt/release/unused-vars.t'
                                   ],
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 1,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 1,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 33,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 1,
                          'meta_yml_has_repository_resource' => 1,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 1,
                          'use_warnings' => 1
                        },
          'latest_mtime' => 1709308134,
          'license' => 'gpl defined in META.yml defined in LICENSE',
          'license_file' => 'lib/Hash/Wrap.pm',
          'license_from_yaml' => 'gpl',
          'license_in_pod' => 1,
          'license_type' => 'GPL_3',
          'licenses' => {
                          'GPL_3' => [
                                       'lib/Hash/Wrap.pm'
                                     ]
                        },
          'manifest_matches_dist' => 1,
          'meta_json' => {
                           'abstract' => 'create on-the-fly objects from hashes',
                           'author' => [
                                         'Diab Jerius <djerius@cpan.org>'
                                       ],
                           'dynamic_config' => 0,
                           'generated_by' => 'Dist::Zilla version 6.030, CPAN::Meta::Converter version 2.150010',
                           'license' => [
                                          'gpl_3'
                                        ],
                           'meta-spec' => {
                                            'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                            'version' => 2
                                          },
                           'name' => 'Hash-Wrap',
                           'no_index' => {
                                           'directory' => [
                                                            'eg',
                                                            'examples',
                                                            'inc',
                                                            'share',
                                                            't',
                                                            'xt'
                                                          ]
                                         },
                           'prereqs' => {
                                          'configure' => {
                                                           'requires' => {
                                                                           'Module::Build::Tiny' => '0.034'
                                                                         }
                                                         },
                                          'develop' => {
                                                         'requires' => {
                                                                         'Dist::Zilla' => '5',
                                                                         'Dist::Zilla::Plugin::BumpVersionAfterRelease' => '0',
                                                                         'Dist::Zilla::Plugin::CheckMetaResources' => '0',
                                                                         'Dist::Zilla::Plugin::CopyFilesFromRelease' => '0',
                                                                         'Dist::Zilla::Plugin::EnsureChangesHasContent' => '0',
                                                                         'Dist::Zilla::Plugin::EnsurePrereqsInstalled' => '0',
                                                                         'Dist::Zilla::Plugin::GatherDir' => '0',
                                                                         'Dist::Zilla::Plugin::GatherDir::Template' => '0',
                                                                         'Dist::Zilla::Plugin::InsertCopyright' => '0',
                                                                         'Dist::Zilla::Plugin::InsertExample' => '0',
                                                                         'Dist::Zilla::Plugin::MetaJSON' => '0',
                                                                         'Dist::Zilla::Plugin::MetaNoIndex' => '0',
                                                                         'Dist::Zilla::Plugin::MetaProvides::Package' => '0',
                                                                         'Dist::Zilla::Plugin::MetaResources' => '0',
                                                                         'Dist::Zilla::Plugin::ModuleBuildTiny' => '0',
                                                                         'Dist::Zilla::Plugin::NextRelease' => '0',
                                                                         'Dist::Zilla::Plugin::PodCoverageTests' => '0',
                                                                         'Dist::Zilla::Plugin::PodSyntaxTests' => '0',
                                                                         'Dist::Zilla::Plugin::PodWeaver' => '0',
                                                                         'Dist::Zilla::Plugin::Prereqs' => '0',
                                                                         'Dist::Zilla::Plugin::Prereqs::AuthorDeps' => '0',
                                                                         'Dist::Zilla::Plugin::Readme::Brief' => '0',
                                                                         'Dist::Zilla::Plugin::ReadmeAnyFromPod' => '0',
                                                                         'Dist::Zilla::Plugin::Regenerate' => '0',
                                                                         'Dist::Zilla::Plugin::RewriteVersion' => '0',
                                                                         'Dist::Zilla::Plugin::RunExtraTests' => '0',
                                                                         'Dist::Zilla::Plugin::Test::CPAN::Changes' => '0',
                                                                         'Dist::Zilla::Plugin::Test::CPAN::Meta::JSON' => '0',
                                                                         'Dist::Zilla::Plugin::Test::CleanNamespaces' => '0',
                                                                         'Dist::Zilla::Plugin::Test::Compile' => '0',
                                                                         'Dist::Zilla::Plugin::Test::DistManifest' => '0',
                                                                         'Dist::Zilla::Plugin::Test::Fixme' => '0',
                                                                         'Dist::Zilla::Plugin::Test::NoBreakpoints' => '0',
                                                                         'Dist::Zilla::Plugin::Test::NoTabs' => '0',
                                                                         'Dist::Zilla::Plugin::Test::Perl::Critic' => '0',
                                                                         'Dist::Zilla::Plugin::Test::PodSpelling' => '0',
                                                                         'Dist::Zilla::Plugin::Test::ReportPrereqs' => '0',
                                                                         'Dist::Zilla::Plugin::Test::TrailingSpace' => '0',
                                                                         'Dist::Zilla::Plugin::Test::UnusedVars' => '0',
                                                                         'Dist::Zilla::Plugin::Test::Version' => '0',
                                                                         'Dist::Zilla::PluginBundle::Basic' => '0',
                                                                         'Dist::Zilla::PluginBundle::Filter' => '0',
                                                                         'Pod::Coverage::TrustPod' => '0',
                                                                         'Pod::Weaver::Plugin::StopWords' => '0',
                                                                         'Pod::Weaver::Section::Contributors' => '0',
                                                                         'Pod::Weaver::Section::GenerateSection' => '0',
                                                                         'Software::License::GPL_3' => '0',
                                                                         'Test::CPAN::Changes' => '0.19',
                                                                         'Test::CPAN::Meta::JSON' => '0.16',
                                                                         'Test::CleanNamespaces' => '0.15',
                                                                         'Test::DistManifest' => '0',
                                                                         'Test::More' => '0.88',
                                                                         'Test::NoBreakpoints' => '0.15',
                                                                         'Test::NoTabs' => '0',
                                                                         'Test::Perl::Critic' => '0',
                                                                         'Test::Pod' => '1.41',
                                                                         'Test::Pod::Coverage' => '1.08',
                                                                         'Test::Spelling' => '0.12',
                                                                         'Test::TrailingSpace' => '0.0203',
                                                                         'Test::Vars' => '0.015',
                                                                         'Test::Version' => '1'
                                                                       }
                                                       },
                                          'runtime' => {
                                                         'requires' => {
                                                                         'Scalar::Util' => '0',
                                                                         'perl' => '5.010'
                                                                       }
                                                       },
                                          'test' => {
                                                      'recommends' => {
                                                                        'CPAN::Meta' => '2.120900'
                                                                      },
                                                      'requires' => {
                                                                      'ExtUtils::MakeMaker' => '0',
                                                                      'File::Spec' => '0',
                                                                      'IO::Handle' => '0',
                                                                      'IPC::Open3' => '0',
                                                                      'Test2::V0' => '0',
                                                                      'Test::More' => '0'
                                                                    }
                                                    }
                                        },
                           'provides' => {
                                           'Hash::Wrap' => {
                                                             'file' => 'lib/Hash/Wrap.pm',
                                                             'version' => '1.01'
                                                           }
                                         },
                           'release_status' => 'stable',
                           'resources' => {
                                            'bugtracker' => {
                                                              'mailto' => 'bug-hash-wrap@rt.cpan.org',
                                                              'web' => 'https://rt.cpan.org/Public/Dist/Display.html?Name=Hash-Wrap'
                                                            },
                                            'repository' => {
                                                              'url' => 'https://gitlab.com/djerius/hash-wrap.git',
                                                              'web' => 'https://gitlab.com/djerius/hash-wrap'
                                                            }
                                          },
                           'version' => '1.01',
                           'x_generated_by_perl' => 'v5.28.3',
                           'x_serialization_backend' => 'Cpanel::JSON::XS version 4.36',
                           'x_spdx_expression' => 'GPL-3.0-only'
                         },
          'meta_json_is_parsable' => 1,
          'meta_json_spec_version' => 2,
          'meta_yml' => {
                          'abstract' => 'create on-the-fly objects from hashes',
                          'author' => [
                                        'Diab Jerius <djerius@cpan.org>'
                                      ],
                          'build_requires' => {
                                                'ExtUtils::MakeMaker' => '0',
                                                'File::Spec' => '0',
                                                'IO::Handle' => '0',
                                                'IPC::Open3' => '0',
                                                'Test2::V0' => '0',
                                                'Test::More' => '0'
                                              },
                          'configure_requires' => {
                                                    'Module::Build::Tiny' => '0.034'
                                                  },
                          'dynamic_config' => '0',
                          'generated_by' => 'Dist::Zilla version 6.030, CPAN::Meta::Converter version 2.150010',
                          'license' => 'gpl',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'Hash-Wrap',
                          'no_index' => {
                                          'directory' => [
                                                           'eg',
                                                           'examples',
                                                           'inc',
                                                           'share',
                                                           't',
                                                           'xt'
                                                         ]
                                        },
                          'provides' => {
                                          'Hash::Wrap' => {
                                                            'file' => 'lib/Hash/Wrap.pm',
                                                            'version' => '1.01'
                                                          }
                                        },
                          'requires' => {
                                          'Scalar::Util' => '0',
                                          'perl' => '5.010'
                                        },
                          'resources' => {
                                           'bugtracker' => 'https://rt.cpan.org/Public/Dist/Display.html?Name=Hash-Wrap',
                                           'repository' => 'https://gitlab.com/djerius/hash-wrap.git'
                                         },
                          'version' => '1.01',
                          'x_generated_by_perl' => 'v5.28.3',
                          'x_serialization_backend' => 'YAML::Tiny version 1.74',
                          'x_spdx_expression' => 'GPL-3.0-only'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/Hash/Wrap.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Hash::Wrap'
                         }
                       ],
          'no_index' => '^eg/;^examples/;^inc/;^share/;^t/;^xt/',
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/Hash-Wrap.tar.gz',
          'prereq' => [
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'File::Spec',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'IO::Handle',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'IPC::Open3',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'Module::Build::Tiny',
                          'type' => 'configure_requires',
                          'version' => '0.034'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Scalar::Util',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test2::V0',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => '5.010'
                        }
                      ],
          'released' => 1709583481,
          'size_packed' => 45637,
          'size_unpacked' => 151765,
          'test_files' => [
                            't/00-compile.t',
                            't/00-report-prereqs.t',
                            't/api.t',
                            't/as_return.t',
                            't/as_scalar_ref.t',
                            't/basic.t',
                            't/croak.t',
                            't/defined.t',
                            't/exists.t',
                            't/immutable.t',
                            't/import.t',
                            't/lockkeys.t',
                            't/lvalue.t',
                            't/lvalue_undef.t',
                            't/methods.t',
                            't/predicate.t',
                            't/recurse.t',
                            't/subclass.t',
                            't/undef.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'requires' => {
                                                       'ExtUtils::MakeMaker' => '0',
                                                       'Module::Build::Tiny' => '0.034',
                                                       'perl' => '5.010',
                                                       'strict' => '0',
                                                       'warnings' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'noes' => {
                                                 'strict' => '0'
                                               },
                                     'recommends' => {
                                                       'Carp' => '0'
                                                     },
                                     'requires' => {
                                                     'Digest::MD5' => '0',
                                                     'Scalar::Util' => '0',
                                                     'constant' => '0',
                                                     'perl' => '5.01000',
                                                     'strict' => '0',
                                                     'warnings' => '0'
                                                   }
                                   },
                      'test' => {
                                  'noes' => {
                                              'strict' => '0',
                                              'warnings' => '0'
                                            },
                                  'recommends' => {
                                                    'Storable' => '0'
                                                  },
                                  'requires' => {
                                                  'ExtUtils::MakeMaker' => '0',
                                                  'File::Spec' => '0',
                                                  'Hash::Util' => '0',
                                                  'IO::Handle' => '0',
                                                  'IPC::Open3' => '0',
                                                  'Scalar::Util' => '0',
                                                  'Test2::API' => '0',
                                                  'Test2::V0' => '0',
                                                  'Test::More' => '0',
                                                  'if' => '0',
                                                  'perl' => '5.006',
                                                  'strict' => '0',
                                                  'warnings' => '0'
                                                },
                                  'suggests' => {
                                                  'blib' => '1.01'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'Hash-Wrap'
        };
1;
