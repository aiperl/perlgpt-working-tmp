$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'App::Greple::xlate' => 'translation support module for greple',
                                  'xlate' => 'translate CLI front-end for App::Greple::xlate module'
                                },
          'author' => '',
          'dir_lib' => 'lib',
          'dir_t' => 't',
          'dirs' => 7,
          'dirs_array' => [
                            'lib/App/Greple/xlate',
                            'lib/App/Greple',
                            'lib/App',
                            'lib',
                            'script',
                            'share',
                            't'
                          ],
          'dist' => 'App-Greple-xlate',
          'dynamic_config' => 0,
          'error' => {
                       'extracts_nicely' => 'expected App-Greple-xlate but got App-Greple-xlate-0.31'
                     },
          'extension' => 'tar.gz',
          'external_license_file' => 'LICENSE',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_build_pl' => 'Build.PL',
          'file_changelog' => 'Changes',
          'file_cpanfile' => 'cpanfile',
          'file_license' => 'LICENSE',
          'file_manifest' => 'MANIFEST',
          'file_manifest_skip' => 'MANIFEST.SKIP',
          'file_meta_json' => 'META.json',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README.deepl-DE.md,README.deepl-FR.md,README.deepl-JA.md,README.deepl-KO.md,README.deepl-RU.md,README.deepl-ZH.md,README.gpt3-DE.md,README.gpt3-FR.md,README.gpt3-JA.md,README.gpt3-KO.md,README.gpt3-RU.md,README.gpt3-ZH.md,README.gpt4-JA.md,README.md',
          'files' => 30,
          'files_array' => [
                             'Build.PL',
                             'Changes',
                             'LICENSE',
                             'MANIFEST',
                             'MANIFEST.SKIP',
                             'META.json',
                             'META.yml',
                             'README.deepl-DE.md',
                             'README.deepl-FR.md',
                             'README.deepl-JA.md',
                             'README.deepl-KO.md',
                             'README.deepl-RU.md',
                             'README.deepl-ZH.md',
                             'README.gpt3-DE.md',
                             'README.gpt3-FR.md',
                             'README.gpt3-JA.md',
                             'README.gpt3-KO.md',
                             'README.gpt3-RU.md',
                             'README.gpt3-ZH.md',
                             'README.gpt4-JA.md',
                             'README.md',
                             'cpanfile',
                             'lib/App/Greple/xlate/Cache.pm',
                             'lib/App/Greple/xlate/Lang.pm',
                             'lib/App/Greple/xlate/deepl.pm',
                             'lib/App/Greple/xlate/gpt3.pm',
                             'lib/App/Greple/xlate/gpt4.pm',
                             'lib/App/Greple/xlate.pm',
                             'minil.toml',
                             'script/xlate'
                           ],
          'files_hash' => {
                            'Build.PL' => {
                                            'mtime' => 1709289231,
                                            'requires' => {
                                                            'Module::Build::Tiny' => '0.035',
                                                            'perl' => '5.008_001',
                                                            'strict' => '0'
                                                          },
                                            'size' => 301
                                          },
                            'Changes' => {
                                           'mtime' => 1709289231,
                                           'size' => 3391
                                         },
                            'LICENSE' => {
                                           'mtime' => 1709289231,
                                           'size' => 18418
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1709289231,
                                            'size' => 565
                                          },
                            'MANIFEST.SKIP' => {
                                                 'mtime' => 1709289231,
                                                 'size' => 21
                                               },
                            'META.json' => {
                                             'mtime' => 1709289231,
                                             'size' => 3018
                                           },
                            'META.yml' => {
                                            'mtime' => 1709289231,
                                            'size' => 1625
                                          },
                            'README.deepl-DE.md' => {
                                                      'mtime' => 1709289231,
                                                      'size' => 12200
                                                    },
                            'README.deepl-FR.md' => {
                                                      'mtime' => 1709289231,
                                                      'size' => 12105
                                                    },
                            'README.deepl-JA.md' => {
                                                      'mtime' => 1709289231,
                                                      'size' => 13384
                                                    },
                            'README.deepl-KO.md' => {
                                                      'mtime' => 1709289231,
                                                      'size' => 12178
                                                    },
                            'README.deepl-RU.md' => {
                                                      'mtime' => 1709289231,
                                                      'size' => 15833
                                                    },
                            'README.deepl-ZH.md' => {
                                                      'mtime' => 1709289231,
                                                      'size' => 10459
                                                    },
                            'README.gpt3-DE.md' => {
                                                     'mtime' => 1709289231,
                                                     'size' => 11925
                                                   },
                            'README.gpt3-FR.md' => {
                                                     'mtime' => 1709289231,
                                                     'size' => 11945
                                                   },
                            'README.gpt3-JA.md' => {
                                                     'mtime' => 1709289231,
                                                     'size' => 13909
                                                   },
                            'README.gpt3-KO.md' => {
                                                     'mtime' => 1709289231,
                                                     'size' => 12219
                                                   },
                            'README.gpt3-RU.md' => {
                                                     'mtime' => 1709289231,
                                                     'size' => 15571
                                                   },
                            'README.gpt3-ZH.md' => {
                                                     'mtime' => 1709289231,
                                                     'size' => 10283
                                                   },
                            'README.gpt4-JA.md' => {
                                                     'mtime' => 1709289231,
                                                     'size' => 13706
                                                   },
                            'README.md' => {
                                             'mtime' => 1709289231,
                                             'size' => 11251
                                           },
                            'cpanfile' => {
                                            'mtime' => 1709289231,
                                            'size' => 532
                                          },
                            'lib/App/Greple/xlate.pm' => {
                                                           'license' => 'Perl_5',
                                                           'module' => 'App::Greple::xlate',
                                                           'mtime' => 1709289231,
                                                           'noes' => {
                                                                       'strict' => '0'
                                                                     },
                                                           'recommends' => {
                                                                             'App::Greple::xlate::Cache' => '0'
                                                                           },
                                                           'requires' => {
                                                                           'App::cdif::Command' => '0',
                                                                           'Data::Dumper' => '0',
                                                                           'Exporter' => '0',
                                                                           'Hash::Util' => '0',
                                                                           'Text::ANSI::Fold' => '0',
                                                                           'Unicode::EastAsianWidth' => '0',
                                                                           'perl' => 'v5.14.0',
                                                                           'warnings' => '0'
                                                                         },
                                                           'size' => 16039
                                                         },
                            'lib/App/Greple/xlate/Cache.pm' => {
                                                                 'module' => 'App::Greple::xlate::Cache',
                                                                 'mtime' => 1709289231,
                                                                 'noes' => {
                                                                             'strict' => '0'
                                                                           },
                                                                 'requires' => {
                                                                                 'Data::Dumper' => '0',
                                                                                 'Hash::Util' => '0',
                                                                                 'JSON' => '0',
                                                                                 'List::Util' => '0',
                                                                                 'perl' => 'v5.14.0',
                                                                                 'warnings' => '0'
                                                                               },
                                                                 'size' => 2640
                                                               },
                            'lib/App/Greple/xlate/Lang.pm' => {
                                                                'module' => 'App::Greple::xlate::Lang',
                                                                'mtime' => 1709289231,
                                                                'requires' => {
                                                                                'Exporter' => '0',
                                                                                'perl' => 'v5.14.0',
                                                                                'warnings' => '0'
                                                                              },
                                                                'size' => 1036
                                                              },
                            'lib/App/Greple/xlate/deepl.pm' => {
                                                                 'module' => 'App::Greple::xlate::deepl',
                                                                 'mtime' => 1709289231,
                                                                 'recommends' => {
                                                                                   'Clipboard' => '0'
                                                                                 },
                                                                 'requires' => {
                                                                                 'App::Greple::xlate' => '0',
                                                                                 'App::Greple::xlate::Lang' => '0',
                                                                                 'App::cdif::Command' => '0',
                                                                                 'Data::Dumper' => '0',
                                                                                 'Encode' => '0',
                                                                                 'List::Util' => '0',
                                                                                 'perl' => 'v5.14.0',
                                                                                 'warnings' => '0'
                                                                               },
                                                                 'size' => 2087
                                                               },
                            'lib/App/Greple/xlate/gpt3.pm' => {
                                                                'module' => 'App::Greple::xlate::gpt3',
                                                                'mtime' => 1709289231,
                                                                'requires' => {
                                                                                'App::Greple::xlate' => '0',
                                                                                'App::Greple::xlate::Lang' => '0',
                                                                                'App::cdif::Command' => '0',
                                                                                'Data::Dumper' => '0',
                                                                                'Encode' => '0',
                                                                                'List::Util' => '0',
                                                                                'perl' => 'v5.14.0',
                                                                                'utf8' => '0',
                                                                                'warnings' => '0'
                                                                              },
                                                                'size' => 2481
                                                              },
                            'lib/App/Greple/xlate/gpt4.pm' => {
                                                                'module' => 'App::Greple::xlate::gpt4',
                                                                'mtime' => 1709289231,
                                                                'requires' => {
                                                                                'App::Greple::xlate' => '0',
                                                                                'App::Greple::xlate::Lang' => '0',
                                                                                'App::cdif::Command' => '0',
                                                                                'Data::Dumper' => '0',
                                                                                'Encode' => '0',
                                                                                'List::Util' => '0',
                                                                                'perl' => 'v5.14.0',
                                                                                'utf8' => '0',
                                                                                'warnings' => '0'
                                                                              },
                                                                'size' => 2481
                                                              },
                            'minil.toml' => {
                                              'mtime' => 1709289231,
                                              'size' => 418
                                            },
                            'script/xlate' => {
                                                'mtime' => 1709289231,
                                                'size' => 7032
                                              },
                            'share/XLATE.mk' => {
                                                  'mtime' => 1709289231,
                                                  'no_index' => 1,
                                                  'size' => 2284
                                                },
                            'share/xlate.el' => {
                                                  'mtime' => 1709289231,
                                                  'no_index' => 1,
                                                  'size' => 1641
                                                },
                            't/00_compile.t' => {
                                                  'mtime' => 1709289231,
                                                  'no_index' => 1,
                                                  'requires' => {
                                                                  'Test::More' => '0.98',
                                                                  'strict' => '0'
                                                                },
                                                  'size' => 212
                                                }
                          },
          'got_prereq_from' => 'META.yml',
          'ignored_files_array' => [
                                     'share/XLATE.mk',
                                     'share/xlate.el',
                                     't/00_compile.t'
                                   ],
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 1,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 1,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 33,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 1,
                          'meta_yml_has_repository_resource' => 1,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 1,
                          'use_warnings' => 1
                        },
          'latest_mtime' => 1709289231,
          'license' => 'perl defined in META.yml defined in LICENSE',
          'license_file' => 'lib/App/Greple/xlate.pm',
          'license_from_yaml' => 'perl',
          'license_in_pod' => 1,
          'license_type' => 'Perl_5',
          'licenses' => {
                          'Perl_5' => [
                                        'lib/App/Greple/xlate.pm'
                                      ]
                        },
          'manifest_matches_dist' => 1,
          'meta_json' => {
                           'abstract' => 'translation support module for greple',
                           'author' => [
                                         'Kazumasa Utashiro'
                                       ],
                           'dynamic_config' => 0,
                           'generated_by' => 'Minilla/v3.1.23',
                           'license' => [
                                          'perl_5'
                                        ],
                           'meta-spec' => {
                                            'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                            'version' => '2'
                                          },
                           'name' => 'App-Greple-xlate',
                           'no_index' => {
                                           'directory' => [
                                                            't',
                                                            'xt',
                                                            'inc',
                                                            'share',
                                                            'eg',
                                                            'examples',
                                                            'author',
                                                            'builder'
                                                          ]
                                         },
                           'prereqs' => {
                                          'configure' => {
                                                           'requires' => {
                                                                           'Module::Build::Tiny' => '0.035'
                                                                         }
                                                         },
                                          'develop' => {
                                                         'recommends' => {
                                                                           'App::Greple::subst::desumasu' => '0',
                                                                           'App::Greple::xp' => '0.04',
                                                                           'Pod::Markdown' => '0'
                                                                         },
                                                         'requires' => {
                                                                         'Test::CPAN::Meta' => '0',
                                                                         'Test::MinimumVersion::Fast' => '0.04',
                                                                         'Test::PAUSE::Permissions' => '0.07',
                                                                         'Test::Pod' => '1.41',
                                                                         'Test::Spellunker' => 'v0.2.7'
                                                                       }
                                                       },
                                          'runtime' => {
                                                         'requires' => {
                                                                         'App::Greple' => '9.0902',
                                                                         'App::Greple::msdoc' => '1.05',
                                                                         'App::optex::textconv' => '1.04',
                                                                         'App::sdif' => '4.29',
                                                                         'Clipboard' => '0',
                                                                         'File::Share' => '0',
                                                                         'Hash::Util' => '0',
                                                                         'JSON' => '0',
                                                                         'List::Util' => '1.56',
                                                                         'Text::ANSI::Fold' => '2.2104',
                                                                         'perl' => 'v5.14.0'
                                                                       }
                                                       },
                                          'test' => {
                                                      'requires' => {
                                                                      'Test::More' => '0.98'
                                                                    }
                                                    }
                                        },
                           'provides' => {
                                           'App::Greple::xlate' => {
                                                                     'file' => 'lib/App/Greple/xlate.pm',
                                                                     'version' => '0.31'
                                                                   },
                                           'App::Greple::xlate::Cache' => {
                                                                            'file' => 'lib/App/Greple/xlate/Cache.pm'
                                                                          },
                                           'App::Greple::xlate::Lang' => {
                                                                           'file' => 'lib/App/Greple/xlate/Lang.pm'
                                                                         },
                                           'App::Greple::xlate::deepl' => {
                                                                            'file' => 'lib/App/Greple/xlate/deepl.pm',
                                                                            'version' => '0.31'
                                                                          },
                                           'App::Greple::xlate::gpt3' => {
                                                                           'file' => 'lib/App/Greple/xlate/gpt3.pm',
                                                                           'version' => '0.31'
                                                                         },
                                           'App::Greple::xlate::gpt4' => {
                                                                           'file' => 'lib/App/Greple/xlate/gpt4.pm',
                                                                           'version' => '0.31'
                                                                         }
                                         },
                           'release_status' => 'stable',
                           'resources' => {
                                            'bugtracker' => {
                                                              'web' => 'https://github.com/kaz-utashiro/App-Greple-xlate/issues'
                                                            },
                                            'homepage' => 'https://github.com/kaz-utashiro/App-Greple-xlate',
                                            'repository' => {
                                                              'type' => 'git',
                                                              'url' => 'https://github.com/kaz-utashiro/App-Greple-xlate.git',
                                                              'web' => 'https://github.com/kaz-utashiro/App-Greple-xlate'
                                                            }
                                          },
                           'version' => '0.31',
                           'x_authority' => 'cpan:UTASHIRO',
                           'x_contributors' => [
                                                 'Kazumasa Utashiro <kaz@utashiro.com>'
                                               ],
                           'x_serialization_backend' => 'JSON::PP version 4.12',
                           'x_static_install' => 1
                         },
          'meta_json_is_parsable' => 1,
          'meta_json_spec_version' => 2,
          'meta_yml' => {
                          'abstract' => 'translation support module for greple',
                          'author' => [
                                        'Kazumasa Utashiro'
                                      ],
                          'build_requires' => {
                                                'Test::More' => '0.98'
                                              },
                          'configure_requires' => {
                                                    'Module::Build::Tiny' => '0.035'
                                                  },
                          'dynamic_config' => '0',
                          'generated_by' => 'Minilla/v3.1.23, CPAN::Meta::Converter version 2.150010',
                          'license' => 'perl',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'App-Greple-xlate',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'share',
                                                           'eg',
                                                           'examples',
                                                           'author',
                                                           'builder'
                                                         ]
                                        },
                          'provides' => {
                                          'App::Greple::xlate' => {
                                                                    'file' => 'lib/App/Greple/xlate.pm',
                                                                    'version' => '0.31'
                                                                  },
                                          'App::Greple::xlate::Cache' => {
                                                                           'file' => 'lib/App/Greple/xlate/Cache.pm'
                                                                         },
                                          'App::Greple::xlate::Lang' => {
                                                                          'file' => 'lib/App/Greple/xlate/Lang.pm'
                                                                        },
                                          'App::Greple::xlate::deepl' => {
                                                                           'file' => 'lib/App/Greple/xlate/deepl.pm',
                                                                           'version' => '0.31'
                                                                         },
                                          'App::Greple::xlate::gpt3' => {
                                                                          'file' => 'lib/App/Greple/xlate/gpt3.pm',
                                                                          'version' => '0.31'
                                                                        },
                                          'App::Greple::xlate::gpt4' => {
                                                                          'file' => 'lib/App/Greple/xlate/gpt4.pm',
                                                                          'version' => '0.31'
                                                                        }
                                        },
                          'requires' => {
                                          'App::Greple' => '9.0902',
                                          'App::Greple::msdoc' => '1.05',
                                          'App::optex::textconv' => '1.04',
                                          'App::sdif' => '4.29',
                                          'Clipboard' => '0',
                                          'File::Share' => '0',
                                          'Hash::Util' => '0',
                                          'JSON' => '0',
                                          'List::Util' => '1.56',
                                          'Text::ANSI::Fold' => '2.2104',
                                          'perl' => 'v5.14.0'
                                        },
                          'resources' => {
                                           'bugtracker' => 'https://github.com/kaz-utashiro/App-Greple-xlate/issues',
                                           'homepage' => 'https://github.com/kaz-utashiro/App-Greple-xlate',
                                           'repository' => 'https://github.com/kaz-utashiro/App-Greple-xlate.git'
                                         },
                          'version' => '0.31',
                          'x_authority' => 'cpan:UTASHIRO',
                          'x_contributors' => [
                                                'Kazumasa Utashiro <kaz@utashiro.com>'
                                              ],
                          'x_serialization_backend' => 'CPAN::Meta::YAML version 0.018',
                          'x_static_install' => '1'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/App/Greple/xlate.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'App::Greple::xlate'
                         },
                         {
                           'file' => 'lib/App/Greple/xlate/Cache.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'App::Greple::xlate::Cache'
                         },
                         {
                           'file' => 'lib/App/Greple/xlate/Lang.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'App::Greple::xlate::Lang'
                         },
                         {
                           'file' => 'lib/App/Greple/xlate/deepl.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'App::Greple::xlate::deepl'
                         },
                         {
                           'file' => 'lib/App/Greple/xlate/gpt3.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'App::Greple::xlate::gpt3'
                         },
                         {
                           'file' => 'lib/App/Greple/xlate/gpt4.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'App::Greple::xlate::gpt4'
                         }
                       ],
          'no_index' => '^author/;^builder/;^eg/;^examples/;^inc/;^share/;^t/;^xt/',
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/App-Greple-xlate.tar.gz',
          'prereq' => [
                        {
                          'is_prereq' => 1,
                          'requires' => 'App::Greple',
                          'type' => 'runtime_requires',
                          'version' => '9.0902'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'App::Greple::msdoc',
                          'type' => 'runtime_requires',
                          'version' => '1.05'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'App::optex::textconv',
                          'type' => 'runtime_requires',
                          'version' => '1.04'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'App::sdif',
                          'type' => 'runtime_requires',
                          'version' => '4.29'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Clipboard',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'File::Share',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Hash::Util',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'JSON',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'List::Util',
                          'type' => 'runtime_requires',
                          'version' => '1.56'
                        },
                        {
                          'requires' => 'Module::Build::Tiny',
                          'type' => 'configure_requires',
                          'version' => '0.035'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'build_requires',
                          'version' => '0.98'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Text::ANSI::Fold',
                          'type' => 'runtime_requires',
                          'version' => '2.2104'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => 'v5.14.0'
                        }
                      ],
          'released' => 1709583488,
          'size_packed' => 63038,
          'size_unpacked' => 243190,
          'test_files' => [
                            't/00_compile.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'requires' => {
                                                       'Module::Build::Tiny' => '0.035',
                                                       'perl' => '5.008_001',
                                                       'strict' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'noes' => {
                                                 'strict' => '0'
                                               },
                                     'recommends' => {
                                                       'Clipboard' => '0'
                                                     },
                                     'requires' => {
                                                     'App::cdif::Command' => '0',
                                                     'Data::Dumper' => '0',
                                                     'Encode' => '0',
                                                     'Exporter' => '0',
                                                     'Hash::Util' => '0',
                                                     'JSON' => '0',
                                                     'List::Util' => '0',
                                                     'Text::ANSI::Fold' => '0',
                                                     'Unicode::EastAsianWidth' => '0',
                                                     'perl' => 'v5.14.0',
                                                     'utf8' => '0',
                                                     'warnings' => '0'
                                                   }
                                   },
                      'test' => {
                                  'requires' => {
                                                  'Test::More' => '0.98',
                                                  'strict' => '0'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'App-Greple-xlate'
        };
1;
