$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'Net::Silverpeak::Orchestrator' => 'Silverpeak Orchestrator REST API client library'
                                },
          'author' => '',
          'dir_lib' => 'lib',
          'dir_t' => 't',
          'dirs' => 4,
          'dirs_array' => [
                            'lib/Net/Silverpeak',
                            'lib/Net',
                            'lib',
                            't'
                          ],
          'dist' => 'Net-Silverpeak-Orchestrator',
          'dynamic_config' => 0,
          'error' => {
                       'extracts_nicely' => 'expected Net-Silverpeak-Orchestrator but got Net-Silverpeak-Orchestrator-0.010000'
                     },
          'extension' => 'tar.gz',
          'external_license_file' => 'LICENSE',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_changelog' => 'Changes',
          'file_dist_ini' => 'dist.ini',
          'file_license' => 'LICENSE',
          'file_makefile_pl' => 'Makefile.PL',
          'file_manifest' => 'MANIFEST',
          'file_meta_json' => 'META.json',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README',
          'files' => 30,
          'files_array' => [
                             'Changes',
                             'LICENSE',
                             'MANIFEST',
                             'META.json',
                             'META.yml',
                             'Makefile.PL',
                             'README',
                             'dist.ini',
                             'lib/Net/Silverpeak/Orchestrator.pm',
                             't/00-compile.t',
                             't/api.t',
                             't/api_key_authentication.t',
                             't/author-critic.t',
                             't/author-distmeta.t',
                             't/author-eol.t',
                             't/author-minimum-version.t',
                             't/author-mojibake.t',
                             't/author-no-tabs.t',
                             't/author-pod-coverage.t',
                             't/author-pod-linkcheck.t',
                             't/author-pod-syntax.t',
                             't/author-portability.t',
                             't/author-synopsis.t',
                             't/author-test-version.t',
                             't/perlcriticrc',
                             't/release-cpan-changes.t',
                             't/release-dist-manifest.t',
                             't/release-kwalitee.t',
                             't/release-meta-json.t',
                             't/release-unused-vars.t'
                           ],
          'files_hash' => {
                            'Changes' => {
                                           'mtime' => 1709558898,
                                           'size' => 2596
                                         },
                            'LICENSE' => {
                                           'mtime' => 1709558898,
                                           'size' => 19756
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1709558898,
                                            'size' => 628
                                          },
                            'META.json' => {
                                             'mtime' => 1709558898,
                                             'size' => 20189
                                           },
                            'META.yml' => {
                                            'mtime' => 1709558898,
                                            'size' => 12491
                                          },
                            'Makefile.PL' => {
                                               'mtime' => 1709558898,
                                               'requires' => {
                                                               'ExtUtils::MakeMaker' => '0',
                                                               'perl' => '5.024000',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 1701
                                             },
                            'README' => {
                                          'mtime' => 1709558898,
                                          'size' => 406
                                        },
                            'dist.ini' => {
                                            'mtime' => 1709558898,
                                            'size' => 879
                                          },
                            'lib/Net/Silverpeak/Orchestrator.pm' => {
                                                                      'license' => 'Perl_5',
                                                                      'module' => 'Net::Silverpeak::Orchestrator',
                                                                      'mtime' => 1709558898,
                                                                      'noes' => {
                                                                                  'warnings' => '0'
                                                                                },
                                                                      'recommends' => {
                                                                                        'HTTP::Thin' => '0'
                                                                                      },
                                                                      'requires' => {
                                                                                      'Carp' => '0',
                                                                                      'HTTP::CookieJar' => '0',
                                                                                      'List::Util' => '0',
                                                                                      'Moo' => '0',
                                                                                      'Role::REST::Client' => '0',
                                                                                      'Types::Standard' => '0',
                                                                                      'feature' => '0',
                                                                                      'perl' => '5.024'
                                                                                    },
                                                                      'size' => 18710
                                                                    },
                            't/00-compile.t' => {
                                                  'mtime' => 1709558898,
                                                  'requires' => {
                                                                  'File::Spec' => '0',
                                                                  'IO::Handle' => '0',
                                                                  'IPC::Open3' => '0',
                                                                  'Test::More' => '0',
                                                                  'perl' => '5.006',
                                                                  'strict' => '0',
                                                                  'warnings' => '0'
                                                                },
                                                  'size' => 1452,
                                                  'suggests' => {
                                                                  'blib' => '1.01'
                                                                }
                                                },
                            't/api.t' => {
                                           'mtime' => 1709558898,
                                           'requires' => {
                                                           'List::Util' => '0',
                                                           'Net::Silverpeak::Orchestrator' => '0',
                                                           'Test2::Tools::Compare' => '0',
                                                           'Test2::Tools::Subtest' => '0',
                                                           'Test2::V0' => '0'
                                                         },
                                           'size' => 22446
                                         },
                            't/api_key_authentication.t' => {
                                                              'mtime' => 1709558898,
                                                              'requires' => {
                                                                              'Net::Silverpeak::Orchestrator' => '0',
                                                                              'Test2::V0' => '0'
                                                                            },
                                                              'size' => 789
                                                            },
                            't/author-critic.t' => {
                                                     'mtime' => 1709558898,
                                                     'recommends' => {
                                                                       'Test::Perl::Critic' => '0',
                                                                       'strict' => '0',
                                                                       'warnings' => '0'
                                                                     },
                                                     'size' => 261
                                                   },
                            't/author-distmeta.t' => {
                                                       'mtime' => 1709558898,
                                                       'recommends' => {
                                                                         'Test::CPAN::Meta' => '0',
                                                                         'strict' => '0',
                                                                         'warnings' => '0'
                                                                       },
                                                       'size' => 277
                                                     },
                            't/author-eol.t' => {
                                                  'mtime' => 1709558898,
                                                  'recommends' => {
                                                                    'Test::EOL' => '0',
                                                                    'Test::More' => '0.88',
                                                                    'strict' => '0',
                                                                    'warnings' => '0'
                                                                  },
                                                  'size' => 984
                                                },
                            't/author-minimum-version.t' => {
                                                              'mtime' => 1709558898,
                                                              'recommends' => {
                                                                                'Test::MinimumVersion' => '0',
                                                                                'Test::More' => '0',
                                                                                'strict' => '0',
                                                                                'warnings' => '0'
                                                                              },
                                                              'size' => 238
                                                            },
                            't/author-mojibake.t' => {
                                                       'mtime' => 1709558898,
                                                       'recommends' => {
                                                                         'Test::Mojibake' => '0',
                                                                         'Test::More' => '0',
                                                                         'strict' => '0',
                                                                         'warnings' => '0'
                                                                       },
                                                       'size' => 235
                                                     },
                            't/author-no-tabs.t' => {
                                                      'mtime' => 1709558898,
                                                      'recommends' => {
                                                                        'Test::More' => '0.88',
                                                                        'Test::NoTabs' => '0',
                                                                        'strict' => '0',
                                                                        'warnings' => '0'
                                                                      },
                                                      'size' => 958
                                                    },
                            't/author-pod-coverage.t' => {
                                                           'mtime' => 1709558898,
                                                           'recommends' => {
                                                                             'Pod::Coverage::TrustPod' => '0',
                                                                             'Test::Pod::Coverage' => '1.08',
                                                                             'strict' => '0',
                                                                             'warnings' => '0'
                                                                           },
                                                           'size' => 375
                                                         },
                            't/author-pod-linkcheck.t' => {
                                                            'mtime' => 1709558898,
                                                            'recommends' => {
                                                                              'Test::More' => '0',
                                                                              'strict' => '0',
                                                                              'warnings' => '0'
                                                                            },
                                                            'size' => 481,
                                                            'suggests' => {
                                                                            'Test::Pod::LinkCheck' => '0'
                                                                          }
                                                          },
                            't/author-pod-syntax.t' => {
                                                         'mtime' => 1709558898,
                                                         'recommends' => {
                                                                           'Test::More' => '0',
                                                                           'Test::Pod' => '1.41',
                                                                           'strict' => '0',
                                                                           'warnings' => '0'
                                                                         },
                                                         'size' => 300
                                                       },
                            't/author-portability.t' => {
                                                          'mtime' => 1709558898,
                                                          'recommends' => {
                                                                            'Test::More' => '0',
                                                                            'Test::Portability::Files' => '0',
                                                                            'strict' => '0',
                                                                            'warnings' => '0'
                                                                          },
                                                          'size' => 218
                                                        },
                            't/author-synopsis.t' => {
                                                       'mtime' => 1709558898,
                                                       'recommends' => {
                                                                         'Test::Synopsis' => '0'
                                                                       },
                                                       'size' => 178
                                                     },
                            't/author-test-version.t' => {
                                                           'mtime' => 1709558898,
                                                           'recommends' => {
                                                                             'Test::More' => '0.88',
                                                                             'Test::Version' => '0',
                                                                             'strict' => '0',
                                                                             'warnings' => '0'
                                                                           },
                                                           'size' => 545
                                                         },
                            't/perlcriticrc' => {
                                                  'mtime' => 1709558898,
                                                  'size' => 90
                                                },
                            't/release-cpan-changes.t' => {
                                                            'mtime' => 1709558898,
                                                            'recommends' => {
                                                                              'Test::CPAN::Changes' => '0',
                                                                              'Test::More' => '0.96',
                                                                              'strict' => '0',
                                                                              'warnings' => '0'
                                                                            },
                                                            'size' => 363
                                                          },
                            't/release-dist-manifest.t' => {
                                                             'mtime' => 1709558898,
                                                             'recommends' => {
                                                                               'Test::DistManifest' => '0',
                                                                               'Test::More' => '0',
                                                                               'strict' => '0',
                                                                               'warnings' => '0'
                                                                             },
                                                             'size' => 217
                                                           },
                            't/release-kwalitee.t' => {
                                                        'mtime' => 1709558898,
                                                        'recommends' => {
                                                                          'Test::Kwalitee' => '1.21',
                                                                          'Test::More' => '0.88',
                                                                          'strict' => '0',
                                                                          'warnings' => '0'
                                                                        },
                                                        'size' => 324
                                                      },
                            't/release-meta-json.t' => {
                                                         'mtime' => 1709558898,
                                                         'recommends' => {
                                                                           'Test::CPAN::Meta::JSON' => '0'
                                                                         },
                                                         'size' => 187
                                                       },
                            't/release-unused-vars.t' => {
                                                           'mtime' => 1709558898,
                                                           'recommends' => {
                                                                             'Test::More' => '0.96',
                                                                             'Test::Vars' => '0'
                                                                           },
                                                           'size' => 233
                                                         }
                          },
          'got_prereq_from' => 'META.yml',
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 1,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 1,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 32,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 0,
                          'meta_yml_has_repository_resource' => 1,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 1,
                          'use_warnings' => 1
                        },
          'latest_mtime' => 1709558898,
          'license' => 'perl defined in META.yml defined in LICENSE',
          'license_file' => 'lib/Net/Silverpeak/Orchestrator.pm',
          'license_from_yaml' => 'perl',
          'license_in_pod' => 1,
          'license_type' => 'Perl_5',
          'licenses' => {
                          'Perl_5' => [
                                        'lib/Net/Silverpeak/Orchestrator.pm'
                                      ]
                        },
          'manifest_matches_dist' => 1,
          'meta_json' => {
                           'abstract' => 'Silverpeak Orchestrator REST API client library',
                           'author' => [
                                         'Alexander Hartmaier <abraxxa@cpan.org>'
                                       ],
                           'dynamic_config' => 0,
                           'generated_by' => 'Dist::Zilla version 6.031, CPAN::Meta::Converter version 2.150010',
                           'license' => [
                                          'perl_5'
                                        ],
                           'meta-spec' => {
                                            'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                            'version' => 2
                                          },
                           'name' => 'Net-Silverpeak-Orchestrator',
                           'prereqs' => {
                                          'configure' => {
                                                           'requires' => {
                                                                           'ExtUtils::MakeMaker' => '0'
                                                                         }
                                                         },
                                          'develop' => {
                                                         'requires' => {
                                                                         'Pod::Coverage::TrustPod' => '0',
                                                                         'Test::CPAN::Changes' => '0.19',
                                                                         'Test::CPAN::Meta' => '0',
                                                                         'Test::CPAN::Meta::JSON' => '0.16',
                                                                         'Test::DistManifest' => '0',
                                                                         'Test::EOL' => '0',
                                                                         'Test::Kwalitee' => '1.21',
                                                                         'Test::MinimumVersion' => '0',
                                                                         'Test::Mojibake' => '0',
                                                                         'Test::More' => '0.88',
                                                                         'Test::NoTabs' => '0',
                                                                         'Test::Perl::Critic' => '0',
                                                                         'Test::Pod' => '1.41',
                                                                         'Test::Pod::Coverage' => '1.08',
                                                                         'Test::Pod::LinkCheck' => '0',
                                                                         'Test::Portability::Files' => '0',
                                                                         'Test::Synopsis' => '0',
                                                                         'Test::Vars' => '0',
                                                                         'Test::Version' => '1'
                                                                       }
                                                       },
                                          'runtime' => {
                                                         'requires' => {
                                                                         'Carp::Clan' => '6.07',
                                                                         'HTTP::CookieJar' => '0.010',
                                                                         'HTTP::Tiny' => '0.088',
                                                                         'JSON' => '2.97001',
                                                                         'List::Util' => '1.55',
                                                                         'Moo' => '2.003004',
                                                                         'Role::REST::Client' => '0.23',
                                                                         'Type::Tiny' => '1.004002',
                                                                         'perl' => '5.024000'
                                                                       }
                                                       },
                                          'test' => {
                                                      'requires' => {
                                                                      'File::Spec' => '0',
                                                                      'IO::Handle' => '0',
                                                                      'IPC::Open3' => '0',
                                                                      'Test2::V0' => '0.000135',
                                                                      'Test::More' => '0'
                                                                    }
                                                    }
                                        },
                           'release_status' => 'stable',
                           'resources' => {
                                            'bugtracker' => {
                                                              'web' => 'https://github.com/abraxxa/Net-Silverpeak-Orchestrator/issues'
                                                            },
                                            'repository' => {
                                                              'type' => 'git',
                                                              'url' => 'https://github.com/abraxxa/Net-Silverpeak-Orchestrator.git',
                                                              'web' => 'https://github.com/abraxxa/Net-Silverpeak-Orchestrator'
                                                            }
                                          },
                           'version' => '0.010000',
                           'x_Dist_Zilla' => {
                                               'perl' => {
                                                           'version' => '5.038002'
                                                         },
                                               'plugins' => [
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::GatherDir',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::GatherDir' => {
                                                                                                                    'exclude_filename' => [],
                                                                                                                    'exclude_match' => [],
                                                                                                                    'follow_symlinks' => 0,
                                                                                                                    'include_dotfiles' => 0,
                                                                                                                    'prefix' => '',
                                                                                                                    'prune_directory' => [],
                                                                                                                    'root' => '.'
                                                                                                                  }
                                                                            },
                                                                'name' => '@Basic/GatherDir',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::PruneCruft',
                                                                'name' => '@Basic/PruneCruft',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::ManifestSkip',
                                                                'name' => '@Basic/ManifestSkip',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::MetaYAML',
                                                                'name' => '@Basic/MetaYAML',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::License',
                                                                'name' => '@Basic/License',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Readme',
                                                                'name' => '@Basic/Readme',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::ExtraTests',
                                                                'name' => '@Basic/ExtraTests',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::ExecDir',
                                                                'name' => '@Basic/ExecDir',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::ShareDir',
                                                                'name' => '@Basic/ShareDir',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::MakeMaker',
                                                                'config' => {
                                                                              'Dist::Zilla::Role::TestRunner' => {
                                                                                                                   'default_jobs' => 1
                                                                                                                 }
                                                                            },
                                                                'name' => '@Basic/MakeMaker',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Manifest',
                                                                'name' => '@Basic/Manifest',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::TestRelease',
                                                                'name' => '@Basic/TestRelease',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::ConfirmRelease',
                                                                'name' => '@Basic/ConfirmRelease',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::UploadToCPAN',
                                                                'name' => '@Basic/UploadToCPAN',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::PodWeaver',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::PodWeaver' => {
                                                                                                                    'finder' => [
                                                                                                                                  ':InstallModules',
                                                                                                                                  ':PerlExecFiles'
                                                                                                                                ],
                                                                                                                    'plugins' => [
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Plugin::EnsurePod5',
                                                                                                                                     'name' => '@CorePrep/EnsurePod5',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Plugin::H1Nester',
                                                                                                                                     'name' => '@CorePrep/H1Nester',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Plugin::SingleEncoding',
                                                                                                                                     'name' => '@Default/SingleEncoding',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Name',
                                                                                                                                     'name' => '@Default/Name',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Version',
                                                                                                                                     'name' => '@Default/Version',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Region',
                                                                                                                                     'name' => '@Default/prelude',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Generic',
                                                                                                                                     'name' => 'SYNOPSIS',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Generic',
                                                                                                                                     'name' => 'DESCRIPTION',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Generic',
                                                                                                                                     'name' => 'OVERVIEW',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Collect',
                                                                                                                                     'name' => 'ATTRIBUTES',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Collect',
                                                                                                                                     'name' => 'METHODS',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Collect',
                                                                                                                                     'name' => 'FUNCTIONS',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Leftovers',
                                                                                                                                     'name' => '@Default/Leftovers',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Region',
                                                                                                                                     'name' => '@Default/postlude',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Authors',
                                                                                                                                     'name' => '@Default/Authors',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Legal',
                                                                                                                                     'name' => '@Default/Legal',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   }
                                                                                                                                 ]
                                                                                                                  }
                                                                            },
                                                                'name' => 'PodWeaver',
                                                                'version' => '4.010'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::PkgVersion',
                                                                'name' => 'PkgVersion',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::NextRelease',
                                                                'name' => 'NextRelease',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::MetaConfig',
                                                                'name' => 'MetaConfig',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::MetaJSON',
                                                                'name' => 'MetaJSON',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::MetaResources',
                                                                'name' => 'MetaResources',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Test::CPAN::Changes',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::Test::CPAN::Changes' => {
                                                                                                                              'changelog' => 'Changes'
                                                                                                                            }
                                                                            },
                                                                'name' => '@TestingMania/Test::CPAN::Changes',
                                                                'version' => '0.012'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::PodCoverageTests',
                                                                'name' => '@TestingMania/PodCoverageTests',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Test::Synopsis',
                                                                'name' => '@TestingMania/Test::Synopsis',
                                                                'version' => '2.000007'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Test::Version',
                                                                'name' => '@TestingMania/Test::Version',
                                                                'version' => '1.09'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Test::CPAN::Meta::JSON',
                                                                'name' => '@TestingMania/Test::CPAN::Meta::JSON',
                                                                'version' => '0.004'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Test::Kwalitee',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::Test::Kwalitee' => {
                                                                                                                         'filename' => 'xt/release/kwalitee.t',
                                                                                                                         'skiptest' => []
                                                                                                                       }
                                                                            },
                                                                'name' => '@TestingMania/Test::Kwalitee',
                                                                'version' => '2.12'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Test::Portability',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::Test::Portability' => {
                                                                                                                            'options' => ''
                                                                                                                          }
                                                                            },
                                                                'name' => '@TestingMania/Test::Portability',
                                                                'version' => '2.001001'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Test::Perl::Critic',
                                                                'name' => '@TestingMania/Test::Perl::Critic',
                                                                'version' => '3.001'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::MetaTests',
                                                                'name' => '@TestingMania/MetaTests',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Test::Pod::LinkCheck',
                                                                'name' => '@TestingMania/Test::Pod::LinkCheck',
                                                                'version' => '1.004'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Test::DistManifest',
                                                                'name' => '@TestingMania/Test::DistManifest',
                                                                'version' => '2.000006'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Test::MinimumVersion',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::Test::MinimumVersion' => {
                                                                                                                               'max_target_perl' => undef
                                                                                                                             }
                                                                            },
                                                                'name' => '@TestingMania/Test::MinimumVersion',
                                                                'version' => '2.000010'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::PodSyntaxTests',
                                                                'name' => '@TestingMania/PodSyntaxTests',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::MojibakeTests',
                                                                'name' => '@TestingMania/MojibakeTests',
                                                                'version' => '0.8'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Test::Compile',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::Test::Compile' => {
                                                                                                                        'bail_out_on_fail' => 0,
                                                                                                                        'fail_on_warning' => 'author',
                                                                                                                        'fake_home' => 0,
                                                                                                                        'filename' => 't/00-compile.t',
                                                                                                                        'module_finder' => [
                                                                                                                                             ':InstallModules'
                                                                                                                                           ],
                                                                                                                        'needs_display' => 0,
                                                                                                                        'phase' => 'test',
                                                                                                                        'script_finder' => [
                                                                                                                                             ':PerlExecFiles'
                                                                                                                                           ],
                                                                                                                        'skips' => [],
                                                                                                                        'switch' => []
                                                                                                                      }
                                                                            },
                                                                'name' => '@TestingMania/Test::Compile',
                                                                'version' => '2.058'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Test::NoTabs',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::Test::NoTabs' => {
                                                                                                                       'filename' => 'xt/author/no-tabs.t',
                                                                                                                       'finder' => [
                                                                                                                                     ':InstallModules',
                                                                                                                                     ':ExecFiles',
                                                                                                                                     ':TestFiles'
                                                                                                                                   ]
                                                                                                                     }
                                                                            },
                                                                'name' => '@TestingMania/Test::NoTabs',
                                                                'version' => '0.15'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Test::UnusedVars',
                                                                'name' => '@TestingMania/Test::UnusedVars',
                                                                'version' => '2.001001'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Test::EOL',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::Test::EOL' => {
                                                                                                                    'filename' => 'xt/author/eol.t',
                                                                                                                    'finder' => [
                                                                                                                                  ':ExecFiles',
                                                                                                                                  ':InstallModules',
                                                                                                                                  ':TestFiles'
                                                                                                                                ],
                                                                                                                    'trailing_whitespace' => 1
                                                                                                                  }
                                                                            },
                                                                'name' => '@TestingMania/Test::EOL',
                                                                'version' => '0.19'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Git::Check',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::Git::Check' => {
                                                                                                                     'untracked_files' => 'die'
                                                                                                                   },
                                                                              'Dist::Zilla::Role::Git::DirtyFiles' => {
                                                                                                                        'allow_dirty' => [
                                                                                                                                           'Changes',
                                                                                                                                           'dist.ini'
                                                                                                                                         ],
                                                                                                                        'allow_dirty_match' => [],
                                                                                                                        'changelog' => 'Changes'
                                                                                                                      },
                                                                              'Dist::Zilla::Role::Git::Repo' => {
                                                                                                                  'git_version' => '2.39.2',
                                                                                                                  'repo_root' => '.'
                                                                                                                }
                                                                            },
                                                                'name' => '@Git/Check',
                                                                'version' => '2.049'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Git::Commit',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::Git::Commit' => {
                                                                                                                      'add_files_in' => [],
                                                                                                                      'commit_msg' => 'version %v%n%n%c',
                                                                                                                      'signoff' => 0
                                                                                                                    },
                                                                              'Dist::Zilla::Role::Git::DirtyFiles' => {
                                                                                                                        'allow_dirty' => [
                                                                                                                                           'Changes',
                                                                                                                                           'dist.ini'
                                                                                                                                         ],
                                                                                                                        'allow_dirty_match' => [],
                                                                                                                        'changelog' => 'Changes'
                                                                                                                      },
                                                                              'Dist::Zilla::Role::Git::Repo' => {
                                                                                                                  'git_version' => '2.39.2',
                                                                                                                  'repo_root' => '.'
                                                                                                                },
                                                                              'Dist::Zilla::Role::Git::StringFormatter' => {
                                                                                                                             'time_zone' => 'local'
                                                                                                                           }
                                                                            },
                                                                'name' => '@Git/Commit',
                                                                'version' => '2.049'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Git::Tag',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::Git::Tag' => {
                                                                                                                   'branch' => undef,
                                                                                                                   'changelog' => 'Changes',
                                                                                                                   'signed' => 0,
                                                                                                                   'tag' => '0.010000',
                                                                                                                   'tag_format' => '%v',
                                                                                                                   'tag_message' => '%v'
                                                                                                                 },
                                                                              'Dist::Zilla::Role::Git::Repo' => {
                                                                                                                  'git_version' => '2.39.2',
                                                                                                                  'repo_root' => '.'
                                                                                                                },
                                                                              'Dist::Zilla::Role::Git::StringFormatter' => {
                                                                                                                             'time_zone' => 'local'
                                                                                                                           }
                                                                            },
                                                                'name' => '@Git/Tag',
                                                                'version' => '2.049'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Git::Push',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::Git::Push' => {
                                                                                                                    'push_to' => [
                                                                                                                                   'origin'
                                                                                                                                 ],
                                                                                                                    'remotes_must_exist' => 1
                                                                                                                  },
                                                                              'Dist::Zilla::Role::Git::Repo' => {
                                                                                                                  'git_version' => '2.39.2',
                                                                                                                  'repo_root' => '.'
                                                                                                                }
                                                                            },
                                                                'name' => '@Git/Push',
                                                                'version' => '2.049'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Prereqs',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::Prereqs' => {
                                                                                                                  'phase' => 'runtime',
                                                                                                                  'type' => 'requires'
                                                                                                                }
                                                                            },
                                                                'name' => 'Prereqs',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Prereqs',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::Prereqs' => {
                                                                                                                  'phase' => 'test',
                                                                                                                  'type' => 'requires'
                                                                                                                }
                                                                            },
                                                                'name' => 'TestRequires',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':InstallModules',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':IncModules',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':TestFiles',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':ExtraTestFiles',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':ExecFiles',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':PerlExecFiles',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':ShareFiles',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':MainModule',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':AllFiles',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':NoFiles',
                                                                'version' => '6.031'
                                                              }
                                                            ],
                                               'zilla' => {
                                                            'class' => 'Dist::Zilla::Dist::Builder',
                                                            'config' => {
                                                                          'is_trial' => 0
                                                                        },
                                                            'version' => '6.031'
                                                          }
                                             },
                           'x_generated_by_perl' => 'v5.38.2',
                           'x_serialization_backend' => 'Cpanel::JSON::XS version 4.37',
                           'x_spdx_expression' => 'Artistic-1.0-Perl OR GPL-1.0-or-later'
                         },
          'meta_json_is_parsable' => 1,
          'meta_json_spec_version' => 2,
          'meta_yml' => {
                          'abstract' => 'Silverpeak Orchestrator REST API client library',
                          'author' => [
                                        'Alexander Hartmaier <abraxxa@cpan.org>'
                                      ],
                          'build_requires' => {
                                                'File::Spec' => '0',
                                                'IO::Handle' => '0',
                                                'IPC::Open3' => '0',
                                                'Test2::V0' => '0.000135',
                                                'Test::More' => '0'
                                              },
                          'configure_requires' => {
                                                    'ExtUtils::MakeMaker' => '0'
                                                  },
                          'dynamic_config' => '0',
                          'generated_by' => 'Dist::Zilla version 6.031, CPAN::Meta::Converter version 2.150010',
                          'license' => 'perl',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'Net-Silverpeak-Orchestrator',
                          'requires' => {
                                          'Carp::Clan' => '6.07',
                                          'HTTP::CookieJar' => '0.010',
                                          'HTTP::Tiny' => '0.088',
                                          'JSON' => '2.97001',
                                          'List::Util' => '1.55',
                                          'Moo' => '2.003004',
                                          'Role::REST::Client' => '0.23',
                                          'Type::Tiny' => '1.004002',
                                          'perl' => '5.024000'
                                        },
                          'resources' => {
                                           'bugtracker' => 'https://github.com/abraxxa/Net-Silverpeak-Orchestrator/issues',
                                           'repository' => 'https://github.com/abraxxa/Net-Silverpeak-Orchestrator.git'
                                         },
                          'version' => '0.010000',
                          'x_Dist_Zilla' => {
                                              'perl' => {
                                                          'version' => '5.038002'
                                                        },
                                              'plugins' => [
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::GatherDir',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::GatherDir' => {
                                                                                                                   'exclude_filename' => [],
                                                                                                                   'exclude_match' => [],
                                                                                                                   'follow_symlinks' => '0',
                                                                                                                   'include_dotfiles' => '0',
                                                                                                                   'prefix' => '',
                                                                                                                   'prune_directory' => [],
                                                                                                                   'root' => '.'
                                                                                                                 }
                                                                           },
                                                               'name' => '@Basic/GatherDir',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PruneCruft',
                                                               'name' => '@Basic/PruneCruft',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::ManifestSkip',
                                                               'name' => '@Basic/ManifestSkip',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MetaYAML',
                                                               'name' => '@Basic/MetaYAML',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::License',
                                                               'name' => '@Basic/License',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Readme',
                                                               'name' => '@Basic/Readme',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::ExtraTests',
                                                               'name' => '@Basic/ExtraTests',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::ExecDir',
                                                               'name' => '@Basic/ExecDir',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::ShareDir',
                                                               'name' => '@Basic/ShareDir',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MakeMaker',
                                                               'config' => {
                                                                             'Dist::Zilla::Role::TestRunner' => {
                                                                                                                  'default_jobs' => '1'
                                                                                                                }
                                                                           },
                                                               'name' => '@Basic/MakeMaker',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Manifest',
                                                               'name' => '@Basic/Manifest',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::TestRelease',
                                                               'name' => '@Basic/TestRelease',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::ConfirmRelease',
                                                               'name' => '@Basic/ConfirmRelease',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::UploadToCPAN',
                                                               'name' => '@Basic/UploadToCPAN',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PodWeaver',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::PodWeaver' => {
                                                                                                                   'finder' => [
                                                                                                                                 ':InstallModules',
                                                                                                                                 ':PerlExecFiles'
                                                                                                                               ],
                                                                                                                   'plugins' => [
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Plugin::EnsurePod5',
                                                                                                                                    'name' => '@CorePrep/EnsurePod5',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Plugin::H1Nester',
                                                                                                                                    'name' => '@CorePrep/H1Nester',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Plugin::SingleEncoding',
                                                                                                                                    'name' => '@Default/SingleEncoding',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Name',
                                                                                                                                    'name' => '@Default/Name',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Version',
                                                                                                                                    'name' => '@Default/Version',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Region',
                                                                                                                                    'name' => '@Default/prelude',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Generic',
                                                                                                                                    'name' => 'SYNOPSIS',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Generic',
                                                                                                                                    'name' => 'DESCRIPTION',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Generic',
                                                                                                                                    'name' => 'OVERVIEW',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Collect',
                                                                                                                                    'name' => 'ATTRIBUTES',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Collect',
                                                                                                                                    'name' => 'METHODS',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Collect',
                                                                                                                                    'name' => 'FUNCTIONS',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Leftovers',
                                                                                                                                    'name' => '@Default/Leftovers',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Region',
                                                                                                                                    'name' => '@Default/postlude',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Authors',
                                                                                                                                    'name' => '@Default/Authors',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Legal',
                                                                                                                                    'name' => '@Default/Legal',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  }
                                                                                                                                ]
                                                                                                                 }
                                                                           },
                                                               'name' => 'PodWeaver',
                                                               'version' => '4.010'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PkgVersion',
                                                               'name' => 'PkgVersion',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::NextRelease',
                                                               'name' => 'NextRelease',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MetaConfig',
                                                               'name' => 'MetaConfig',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MetaJSON',
                                                               'name' => 'MetaJSON',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MetaResources',
                                                               'name' => 'MetaResources',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::CPAN::Changes',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Test::CPAN::Changes' => {
                                                                                                                             'changelog' => 'Changes'
                                                                                                                           }
                                                                           },
                                                               'name' => '@TestingMania/Test::CPAN::Changes',
                                                               'version' => '0.012'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PodCoverageTests',
                                                               'name' => '@TestingMania/PodCoverageTests',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::Synopsis',
                                                               'name' => '@TestingMania/Test::Synopsis',
                                                               'version' => '2.000007'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::Version',
                                                               'name' => '@TestingMania/Test::Version',
                                                               'version' => '1.09'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::CPAN::Meta::JSON',
                                                               'name' => '@TestingMania/Test::CPAN::Meta::JSON',
                                                               'version' => '0.004'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::Kwalitee',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Test::Kwalitee' => {
                                                                                                                        'filename' => 'xt/release/kwalitee.t',
                                                                                                                        'skiptest' => []
                                                                                                                      }
                                                                           },
                                                               'name' => '@TestingMania/Test::Kwalitee',
                                                               'version' => '2.12'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::Portability',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Test::Portability' => {
                                                                                                                           'options' => ''
                                                                                                                         }
                                                                           },
                                                               'name' => '@TestingMania/Test::Portability',
                                                               'version' => '2.001001'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::Perl::Critic',
                                                               'name' => '@TestingMania/Test::Perl::Critic',
                                                               'version' => '3.001'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MetaTests',
                                                               'name' => '@TestingMania/MetaTests',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::Pod::LinkCheck',
                                                               'name' => '@TestingMania/Test::Pod::LinkCheck',
                                                               'version' => '1.004'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::DistManifest',
                                                               'name' => '@TestingMania/Test::DistManifest',
                                                               'version' => '2.000006'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::MinimumVersion',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Test::MinimumVersion' => {
                                                                                                                              'max_target_perl' => undef
                                                                                                                            }
                                                                           },
                                                               'name' => '@TestingMania/Test::MinimumVersion',
                                                               'version' => '2.000010'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PodSyntaxTests',
                                                               'name' => '@TestingMania/PodSyntaxTests',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MojibakeTests',
                                                               'name' => '@TestingMania/MojibakeTests',
                                                               'version' => '0.8'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::Compile',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Test::Compile' => {
                                                                                                                       'bail_out_on_fail' => '0',
                                                                                                                       'fail_on_warning' => 'author',
                                                                                                                       'fake_home' => '0',
                                                                                                                       'filename' => 't/00-compile.t',
                                                                                                                       'module_finder' => [
                                                                                                                                            ':InstallModules'
                                                                                                                                          ],
                                                                                                                       'needs_display' => '0',
                                                                                                                       'phase' => 'test',
                                                                                                                       'script_finder' => [
                                                                                                                                            ':PerlExecFiles'
                                                                                                                                          ],
                                                                                                                       'skips' => [],
                                                                                                                       'switch' => []
                                                                                                                     }
                                                                           },
                                                               'name' => '@TestingMania/Test::Compile',
                                                               'version' => '2.058'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::NoTabs',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Test::NoTabs' => {
                                                                                                                      'filename' => 'xt/author/no-tabs.t',
                                                                                                                      'finder' => [
                                                                                                                                    ':InstallModules',
                                                                                                                                    ':ExecFiles',
                                                                                                                                    ':TestFiles'
                                                                                                                                  ]
                                                                                                                    }
                                                                           },
                                                               'name' => '@TestingMania/Test::NoTabs',
                                                               'version' => '0.15'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::UnusedVars',
                                                               'name' => '@TestingMania/Test::UnusedVars',
                                                               'version' => '2.001001'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::EOL',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Test::EOL' => {
                                                                                                                   'filename' => 'xt/author/eol.t',
                                                                                                                   'finder' => [
                                                                                                                                 ':ExecFiles',
                                                                                                                                 ':InstallModules',
                                                                                                                                 ':TestFiles'
                                                                                                                               ],
                                                                                                                   'trailing_whitespace' => '1'
                                                                                                                 }
                                                                           },
                                                               'name' => '@TestingMania/Test::EOL',
                                                               'version' => '0.19'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Git::Check',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Git::Check' => {
                                                                                                                    'untracked_files' => 'die'
                                                                                                                  },
                                                                             'Dist::Zilla::Role::Git::DirtyFiles' => {
                                                                                                                       'allow_dirty' => [
                                                                                                                                          'Changes',
                                                                                                                                          'dist.ini'
                                                                                                                                        ],
                                                                                                                       'allow_dirty_match' => [],
                                                                                                                       'changelog' => 'Changes'
                                                                                                                     },
                                                                             'Dist::Zilla::Role::Git::Repo' => {
                                                                                                                 'git_version' => '2.39.2',
                                                                                                                 'repo_root' => '.'
                                                                                                               }
                                                                           },
                                                               'name' => '@Git/Check',
                                                               'version' => '2.049'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Git::Commit',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Git::Commit' => {
                                                                                                                     'add_files_in' => [],
                                                                                                                     'commit_msg' => 'version %v%n%n%c',
                                                                                                                     'signoff' => '0'
                                                                                                                   },
                                                                             'Dist::Zilla::Role::Git::DirtyFiles' => {
                                                                                                                       'allow_dirty' => [
                                                                                                                                          'Changes',
                                                                                                                                          'dist.ini'
                                                                                                                                        ],
                                                                                                                       'allow_dirty_match' => [],
                                                                                                                       'changelog' => 'Changes'
                                                                                                                     },
                                                                             'Dist::Zilla::Role::Git::Repo' => {
                                                                                                                 'git_version' => '2.39.2',
                                                                                                                 'repo_root' => '.'
                                                                                                               },
                                                                             'Dist::Zilla::Role::Git::StringFormatter' => {
                                                                                                                            'time_zone' => 'local'
                                                                                                                          }
                                                                           },
                                                               'name' => '@Git/Commit',
                                                               'version' => '2.049'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Git::Tag',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Git::Tag' => {
                                                                                                                  'branch' => undef,
                                                                                                                  'changelog' => 'Changes',
                                                                                                                  'signed' => '0',
                                                                                                                  'tag' => '0.010000',
                                                                                                                  'tag_format' => '%v',
                                                                                                                  'tag_message' => '%v'
                                                                                                                },
                                                                             'Dist::Zilla::Role::Git::Repo' => {
                                                                                                                 'git_version' => '2.39.2',
                                                                                                                 'repo_root' => '.'
                                                                                                               },
                                                                             'Dist::Zilla::Role::Git::StringFormatter' => {
                                                                                                                            'time_zone' => 'local'
                                                                                                                          }
                                                                           },
                                                               'name' => '@Git/Tag',
                                                               'version' => '2.049'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Git::Push',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Git::Push' => {
                                                                                                                   'push_to' => [
                                                                                                                                  'origin'
                                                                                                                                ],
                                                                                                                   'remotes_must_exist' => '1'
                                                                                                                 },
                                                                             'Dist::Zilla::Role::Git::Repo' => {
                                                                                                                 'git_version' => '2.39.2',
                                                                                                                 'repo_root' => '.'
                                                                                                               }
                                                                           },
                                                               'name' => '@Git/Push',
                                                               'version' => '2.049'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Prereqs',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Prereqs' => {
                                                                                                                 'phase' => 'runtime',
                                                                                                                 'type' => 'requires'
                                                                                                               }
                                                                           },
                                                               'name' => 'Prereqs',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Prereqs',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Prereqs' => {
                                                                                                                 'phase' => 'test',
                                                                                                                 'type' => 'requires'
                                                                                                               }
                                                                           },
                                                               'name' => 'TestRequires',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':InstallModules',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':IncModules',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':TestFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':ExtraTestFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':ExecFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':PerlExecFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':ShareFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':MainModule',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':AllFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':NoFiles',
                                                               'version' => '6.031'
                                                             }
                                                           ],
                                              'zilla' => {
                                                           'class' => 'Dist::Zilla::Dist::Builder',
                                                           'config' => {
                                                                         'is_trial' => '0'
                                                                       },
                                                           'version' => '6.031'
                                                         }
                                            },
                          'x_generated_by_perl' => 'v5.38.2',
                          'x_serialization_backend' => 'YAML::Tiny version 1.74',
                          'x_spdx_expression' => 'Artistic-1.0-Perl OR GPL-1.0-or-later'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/Net/Silverpeak/Orchestrator.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Net::Silverpeak::Orchestrator'
                         }
                       ],
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/Net-Silverpeak-Orchestrator.tar.gz',
          'prereq' => [
                        {
                          'is_prereq' => 1,
                          'requires' => 'Carp::Clan',
                          'type' => 'runtime_requires',
                          'version' => '6.07'
                        },
                        {
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'File::Spec',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'HTTP::CookieJar',
                          'type' => 'runtime_requires',
                          'version' => '0.010'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'HTTP::Tiny',
                          'type' => 'runtime_requires',
                          'version' => '0.088'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'IO::Handle',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'IPC::Open3',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'JSON',
                          'type' => 'runtime_requires',
                          'version' => '2.97001'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'List::Util',
                          'type' => 'runtime_requires',
                          'version' => '1.55'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Moo',
                          'type' => 'runtime_requires',
                          'version' => '2.003004'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Role::REST::Client',
                          'type' => 'runtime_requires',
                          'version' => '0.23'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test2::V0',
                          'type' => 'build_requires',
                          'version' => '0.000135'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Type::Tiny',
                          'type' => 'runtime_requires',
                          'version' => '1.004002'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => '5.024000'
                        }
                      ],
          'released' => 1709583447,
          'size_packed' => 22725,
          'size_unpacked' => 108507,
          'test_files' => [
                            't/00-compile.t',
                            't/api.t',
                            't/api_key_authentication.t',
                            't/author-critic.t',
                            't/author-distmeta.t',
                            't/author-eol.t',
                            't/author-minimum-version.t',
                            't/author-mojibake.t',
                            't/author-no-tabs.t',
                            't/author-pod-coverage.t',
                            't/author-pod-linkcheck.t',
                            't/author-pod-syntax.t',
                            't/author-portability.t',
                            't/author-synopsis.t',
                            't/author-test-version.t',
                            't/release-cpan-changes.t',
                            't/release-dist-manifest.t',
                            't/release-kwalitee.t',
                            't/release-meta-json.t',
                            't/release-unused-vars.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'requires' => {
                                                       'ExtUtils::MakeMaker' => '0',
                                                       'perl' => '5.024000',
                                                       'strict' => '0',
                                                       'warnings' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'noes' => {
                                                 'warnings' => '0'
                                               },
                                     'recommends' => {
                                                       'HTTP::Thin' => '0'
                                                     },
                                     'requires' => {
                                                     'Carp' => '0',
                                                     'HTTP::CookieJar' => '0',
                                                     'List::Util' => '0',
                                                     'Moo' => '0',
                                                     'Role::REST::Client' => '0',
                                                     'Types::Standard' => '0',
                                                     'feature' => '0',
                                                     'perl' => '5.024'
                                                   }
                                   },
                      'test' => {
                                  'recommends' => {
                                                    'Pod::Coverage::TrustPod' => '0',
                                                    'Test::CPAN::Changes' => '0',
                                                    'Test::CPAN::Meta' => '0',
                                                    'Test::CPAN::Meta::JSON' => '0',
                                                    'Test::DistManifest' => '0',
                                                    'Test::EOL' => '0',
                                                    'Test::Kwalitee' => '1.21',
                                                    'Test::MinimumVersion' => '0',
                                                    'Test::Mojibake' => '0',
                                                    'Test::More' => '0.96',
                                                    'Test::NoTabs' => '0',
                                                    'Test::Perl::Critic' => '0',
                                                    'Test::Pod' => '1.41',
                                                    'Test::Pod::Coverage' => '1.08',
                                                    'Test::Portability::Files' => '0',
                                                    'Test::Synopsis' => '0',
                                                    'Test::Vars' => '0',
                                                    'Test::Version' => '0',
                                                    'strict' => '0',
                                                    'warnings' => '0'
                                                  },
                                  'requires' => {
                                                  'File::Spec' => '0',
                                                  'IO::Handle' => '0',
                                                  'IPC::Open3' => '0',
                                                  'List::Util' => '0',
                                                  'Test2::Tools::Compare' => '0',
                                                  'Test2::Tools::Subtest' => '0',
                                                  'Test2::V0' => '0',
                                                  'Test::More' => '0',
                                                  'perl' => '5.006',
                                                  'strict' => '0',
                                                  'warnings' => '0'
                                                },
                                  'suggests' => {
                                                  'Test::Pod::LinkCheck' => '0',
                                                  'blib' => '1.01'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'Net-Silverpeak-Orchestrator'
        };
1;
