$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'Number::ZipCode::JP' => 'Validate Japanese zip-codes',
                                  'Number::ZipCode::JP::Table' => 'Regex table for all of the Japanese zip-codes'
                                },
          'author' => '',
          'dir_lib' => 'lib',
          'dir_t' => 't',
          'dir_xt' => 'xt',
          'dirs' => 7,
          'dirs_array' => [
                            'lib/Number/ZipCode/JP/Table',
                            'lib/Number/ZipCode/JP',
                            'lib/Number/ZipCode',
                            'lib/Number',
                            'lib',
                            't',
                            'xt'
                          ],
          'dist' => 'Number-ZipCode-JP',
          'dynamic_config' => 1,
          'error' => {
                       'extracts_nicely' => 'expected Number-ZipCode-JP but got Number-ZipCode-JP-0.20240229'
                     },
          'extension' => 'tar.gz',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_changelog' => 'Changes',
          'file_makefile_pl' => 'Makefile.PL',
          'file_manifest' => 'MANIFEST',
          'file_meta_json' => 'META.json',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README',
          'files' => 13,
          'files_array' => [
                             'Changes',
                             'MANIFEST',
                             'META.json',
                             'META.yml',
                             'Makefile.PL',
                             'README',
                             'lib/Number/ZipCode/JP/Table/Area.pm',
                             'lib/Number/ZipCode/JP/Table/Company.pm',
                             'lib/Number/ZipCode/JP/Table.pm',
                             'lib/Number/ZipCode/JP.pm',
                             'regexp-table-maker.pl',
                             'xt/perlcritic.t',
                             'xt/perlcriticrc'
                           ],
          'files_hash' => {
                            'Changes' => {
                                           'mtime' => 1709279832,
                                           'size' => 9415
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1709279860,
                                            'size' => 319
                                          },
                            'META.json' => {
                                             'mtime' => 1709279899,
                                             'size' => 1033
                                           },
                            'META.yml' => {
                                            'mtime' => 1709279899,
                                            'size' => 617
                                          },
                            'Makefile.PL' => {
                                               'mtime' => 1530862850,
                                               'requires' => {
                                                               'ExtUtils::MakeMaker' => '0',
                                                               'FindBin' => '0',
                                                               'lib' => '0',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 947
                                             },
                            'README' => {
                                          'mtime' => 1309856785,
                                          'size' => 546
                                        },
                            'lib/Number/ZipCode/JP.pm' => {
                                                            'license' => 'Perl_5',
                                                            'module' => 'Number::ZipCode::JP',
                                                            'mtime' => 1709279873,
                                                            'noes' => {
                                                                        'strict' => '0'
                                                                      },
                                                            'recommends' => {
                                                                              'Number::ZipCode::JP::Table' => '0'
                                                                            },
                                                            'requires' => {
                                                                            'Carp' => '0',
                                                                            'UNIVERSAL::require' => '0',
                                                                            'perl' => '5.008_001',
                                                                            'strict' => '0',
                                                                            'warnings' => '0'
                                                                          },
                                                            'size' => 5276
                                                          },
                            'lib/Number/ZipCode/JP/Table.pm' => {
                                                                  'license' => 'Perl_5',
                                                                  'module' => 'Number::ZipCode::JP::Table',
                                                                  'mtime' => 1709279851,
                                                                  'noes' => {
                                                                              'strict' => '0',
                                                                              'warnings' => '0'
                                                                            },
                                                                  'requires' => {
                                                                                  'strict' => '0',
                                                                                  'warnings' => '0'
                                                                                },
                                                                  'size' => 941
                                                                },
                            'lib/Number/ZipCode/JP/Table/Area.pm' => {
                                                                       'module' => 'Number::ZipCode::JP::Table::Area',
                                                                       'mtime' => 1709279803,
                                                                       'requires' => {
                                                                                       'strict' => '0',
                                                                                       'warnings' => '0'
                                                                                     },
                                                                       'size' => 156596
                                                                     },
                            'lib/Number/ZipCode/JP/Table/Company.pm' => {
                                                                          'module' => 'Number::ZipCode::JP::Table::Company',
                                                                          'mtime' => 1709279781,
                                                                          'requires' => {
                                                                                          'strict' => '0',
                                                                                          'warnings' => '0'
                                                                                        },
                                                                          'size' => 84522
                                                                        },
                            'regexp-table-maker.pl' => {
                                                         'mtime' => 1401702372,
                                                         'size' => 7395
                                                       },
                            't/00_compile.t' => {
                                                  'mtime' => 1309493594,
                                                  'no_index' => 1,
                                                  'requires' => {
                                                                  'Test::More' => '0',
                                                                  'strict' => '0'
                                                                },
                                                  'size' => 264
                                                },
                            't/area.t' => {
                                            'mtime' => 1709279803,
                                            'no_index' => 1,
                                            'requires' => {
                                                            'Test::More' => '0',
                                                            'strict' => '0'
                                                          },
                                            'size' => 11008523
                                          },
                            't/company.t' => {
                                               'mtime' => 1709279781,
                                               'no_index' => 1,
                                               'requires' => {
                                                               'Test::More' => '0',
                                                               'strict' => '0'
                                                             },
                                               'size' => 2013010
                                             },
                            't/pod.t' => {
                                           'mtime' => 1309493651,
                                           'no_index' => 1,
                                           'requires' => {
                                                           'Test::More' => '0'
                                                         },
                                           'size' => 129,
                                           'suggests' => {
                                                           'Test::Pod' => '1.00'
                                                         }
                                         },
                            't/use-twice.t' => {
                                                 'mtime' => 1309490927,
                                                 'no_index' => 1,
                                                 'requires' => {
                                                                 'Number::ZipCode::JP' => '0',
                                                                 'Test::More' => '0',
                                                                 'strict' => '0',
                                                                 'utf8' => '0',
                                                                 'warnings' => '0'
                                                               },
                                                 'size' => 356
                                               },
                            'xt/perlcritic.t' => {
                                                   'mtime' => 1309430505,
                                                   'size' => 177
                                                 },
                            'xt/perlcriticrc' => {
                                                   'mtime' => 1309430486,
                                                   'size' => 72
                                                 }
                          },
          'got_prereq_from' => 'META.yml',
          'ignored_files_array' => [
                                     't/00_compile.t',
                                     't/area.t',
                                     't/company.t',
                                     't/pod.t',
                                     't/use-twice.t'
                                   ],
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 1,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 0,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 29,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 0,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 0,
                          'meta_yml_has_repository_resource' => 0,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 1,
                          'use_warnings' => 1
                        },
          'latest_mtime' => 1709279899,
          'license' => 'perl defined in META.yml',
          'license_file' => 'lib/Number/ZipCode/JP.pm,lib/Number/ZipCode/JP/Table.pm',
          'license_from_yaml' => 'perl',
          'license_in_pod' => 1,
          'license_type' => 'Perl_5',
          'licenses' => {
                          'Perl_5' => [
                                        'lib/Number/ZipCode/JP.pm',
                                        'lib/Number/ZipCode/JP/Table.pm'
                                      ]
                        },
          'manifest_matches_dist' => 1,
          'meta_json' => {
                           'abstract' => 'Validate Japanese zip-codes',
                           'author' => [
                                         'unknown'
                                       ],
                           'dynamic_config' => 1,
                           'generated_by' => 'ExtUtils::MakeMaker version 7.58, CPAN::Meta::Converter version 2.150010',
                           'license' => [
                                          'perl_5'
                                        ],
                           'meta-spec' => {
                                            'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                            'version' => 2
                                          },
                           'name' => 'Number-ZipCode-JP',
                           'no_index' => {
                                           'directory' => [
                                                            't',
                                                            'inc'
                                                          ]
                                         },
                           'prereqs' => {
                                          'build' => {
                                                       'requires' => {
                                                                       'ExtUtils::MakeMaker' => '0'
                                                                     }
                                                     },
                                          'configure' => {
                                                           'requires' => {
                                                                           'ExtUtils::MakeMaker' => '0'
                                                                         }
                                                         },
                                          'runtime' => {
                                                         'requires' => {
                                                                         'Test::More' => '0',
                                                                         'UNIVERSAL::require' => '0'
                                                                       }
                                                       }
                                        },
                           'release_status' => 'stable',
                           'resources' => {
                                            'X_twitter' => 'https://twitter.com/nipotan'
                                          },
                           'version' => '0.20240229',
                           'x_serialization_backend' => 'JSON::PP version 2.97001'
                         },
          'meta_json_is_parsable' => 1,
          'meta_json_spec_version' => 2,
          'meta_yml' => {
                          'abstract' => 'Validate Japanese zip-codes',
                          'author' => [
                                        'unknown'
                                      ],
                          'build_requires' => {
                                                'ExtUtils::MakeMaker' => '0'
                                              },
                          'configure_requires' => {
                                                    'ExtUtils::MakeMaker' => '0'
                                                  },
                          'dynamic_config' => '1',
                          'generated_by' => 'ExtUtils::MakeMaker version 7.58, CPAN::Meta::Converter version 2.150010',
                          'license' => 'perl',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'Number-ZipCode-JP',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc'
                                                         ]
                                        },
                          'requires' => {
                                          'Test::More' => '0',
                                          'UNIVERSAL::require' => '0'
                                        },
                          'resources' => {
                                           'X_twitter' => 'https://twitter.com/nipotan'
                                         },
                          'version' => '0.20240229',
                          'x_serialization_backend' => 'CPAN::Meta::YAML version 0.018'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/Number/ZipCode/JP.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Number::ZipCode::JP'
                         },
                         {
                           'file' => 'lib/Number/ZipCode/JP/Table.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Number::ZipCode::JP::Table'
                         },
                         {
                           'file' => 'lib/Number/ZipCode/JP/Table/Area.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Number::ZipCode::JP::Table::Area'
                         },
                         {
                           'file' => 'lib/Number/ZipCode/JP/Table/Company.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Number::ZipCode::JP::Table::Company'
                         }
                       ],
          'no_index' => '^inc/;^t/',
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/Number-ZipCode-JP.tar.gz',
          'prereq' => [
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'UNIVERSAL::require',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        }
                      ],
          'released' => 1709583488,
          'size_packed' => 1499731,
          'size_unpacked' => 13290138,
          'test_files' => [
                            't/00_compile.t',
                            't/area.t',
                            't/company.t',
                            't/pod.t',
                            't/use-twice.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'requires' => {
                                                       'ExtUtils::MakeMaker' => '0',
                                                       'FindBin' => '0',
                                                       'lib' => '0',
                                                       'strict' => '0',
                                                       'warnings' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'noes' => {
                                                 'strict' => '0',
                                                 'warnings' => '0'
                                               },
                                     'requires' => {
                                                     'Carp' => '0',
                                                     'UNIVERSAL::require' => '0',
                                                     'perl' => '5.008_001',
                                                     'strict' => '0',
                                                     'warnings' => '0'
                                                   }
                                   },
                      'test' => {
                                  'requires' => {
                                                  'Test::More' => '0',
                                                  'strict' => '0',
                                                  'utf8' => '0',
                                                  'warnings' => '0'
                                                },
                                  'suggests' => {
                                                  'Test::Pod' => '1.00'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'Number-ZipCode-JP'
        };
1;
