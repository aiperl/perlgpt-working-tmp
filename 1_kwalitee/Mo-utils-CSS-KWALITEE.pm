$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'Mo::utils::CSS' => 'Mo CSS utilities.'
                                },
          'author' => '',
          'dir_t' => 't',
          'dir_xt' => 'xt',
          'dirs' => 8,
          'dirs_array' => [
                            'examples',
                            'inc/Module/Install',
                            'inc/Module',
                            'inc',
                            't/Mo-utils-CSS',
                            't',
                            'xt/Mo-utils-CSS',
                            'xt'
                          ],
          'dist' => 'Mo-utils-CSS',
          'dynamic_config' => 1,
          'error' => {
                       'extracts_nicely' => 'expected Mo-utils-CSS but got Mo-utils-CSS-0.05'
                     },
          'extension' => 'tar.gz',
          'external_license_file' => 'LICENSE',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_changelog' => 'Changes',
          'file_license' => 'LICENSE',
          'file_makefile_pl' => 'Makefile.PL',
          'file_manifest' => 'MANIFEST',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README',
          'file_signature' => 'SIGNATURE',
          'files' => 8,
          'files_array' => [
                             'CSS.pm',
                             'Changes',
                             'LICENSE',
                             'MANIFEST',
                             'META.yml',
                             'Makefile.PL',
                             'README',
                             'SIGNATURE'
                           ],
          'files_hash' => {
                            'CSS.pm' => {
                                          'license' => 'BSD',
                                          'module' => 'Mo::utils::CSS',
                                          'mtime' => 1706953224,
                                          'requires' => {
                                                          'Error::Pure' => '0',
                                                          'Exporter' => '0',
                                                          'Graphics::ColorNames::CSS' => '0',
                                                          'List::Util' => '1.33',
                                                          'Mo::utils' => '0.06',
                                                          'Readonly' => '0',
                                                          'base' => '0',
                                                          'strict' => '0',
                                                          'warnings' => '0'
                                                        },
                                          'size' => 10713
                                        },
                            'Changes' => {
                                           'mtime' => 1709546903,
                                           'size' => 747
                                         },
                            'LICENSE' => {
                                           'mtime' => 1704541157,
                                           'size' => 1304
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1706952845,
                                            'size' => 948
                                          },
                            'META.yml' => {
                                            'mtime' => 1706972691,
                                            'size' => 966
                                          },
                            'Makefile.PL' => {
                                               'mtime' => 1706953224,
                                               'requires' => {
                                                               'inc::Module::Install' => '0',
                                                               'lib' => '0',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 1155
                                             },
                            'README' => {
                                          'mtime' => 1706972691,
                                          'size' => 5855
                                        },
                            'SIGNATURE' => {
                                             'mtime' => 1709546905,
                                             'size' => 4563
                                           },
                            'examples/check_array_css_color_fail.pl' => {
                                                                          'mtime' => 1706953224,
                                                                          'no_index' => 1,
                                                                          'size' => 326
                                                                        },
                            'examples/check_array_css_color_ok.pl' => {
                                                                        'mtime' => 1706953224,
                                                                        'no_index' => 1,
                                                                        'size' => 413
                                                                      },
                            'examples/check_css_class_fail.pl' => {
                                                                    'mtime' => 1706953224,
                                                                    'no_index' => 1,
                                                                    'size' => 335
                                                                  },
                            'examples/check_css_class_ok.pl' => {
                                                                  'mtime' => 1706953224,
                                                                  'no_index' => 1,
                                                                  'size' => 207
                                                                },
                            'examples/check_css_color_fail.pl' => {
                                                                    'mtime' => 1706953224,
                                                                    'no_index' => 1,
                                                                    'size' => 312
                                                                  },
                            'examples/check_css_color_ok.pl' => {
                                                                  'mtime' => 1706953224,
                                                                  'no_index' => 1,
                                                                  'size' => 204
                                                                },
                            'examples/check_css_unit_fail.pl' => {
                                                                   'mtime' => 1706953224,
                                                                   'no_index' => 1,
                                                                   'size' => 311
                                                                 },
                            'examples/check_css_unit_ok.pl' => {
                                                                 'mtime' => 1706953224,
                                                                 'no_index' => 1,
                                                                 'size' => 203
                                                               },
                            'inc/Module/Install.pm' => {
                                                         'mtime' => 1706972690,
                                                         'no_index' => 1,
                                                         'size' => 11877
                                                       },
                            'inc/Module/Install/AuthorRequires.pm' => {
                                                                        'mtime' => 1706972691,
                                                                        'no_index' => 1,
                                                                        'size' => 601
                                                                      },
                            'inc/Module/Install/AuthorTests.pm' => {
                                                                     'mtime' => 1706972691,
                                                                     'no_index' => 1,
                                                                     'size' => 1165
                                                                   },
                            'inc/Module/Install/Base.pm' => {
                                                              'mtime' => 1706972691,
                                                              'no_index' => 1,
                                                              'size' => 1127
                                                            },
                            'inc/Module/Install/Can.pm' => {
                                                             'mtime' => 1706972691,
                                                             'no_index' => 1,
                                                             'size' => 3333
                                                           },
                            'inc/Module/Install/Fetch.pm' => {
                                                               'mtime' => 1706972691,
                                                               'no_index' => 1,
                                                               'size' => 2455
                                                             },
                            'inc/Module/Install/Makefile.pm' => {
                                                                  'mtime' => 1706972691,
                                                                  'no_index' => 1,
                                                                  'size' => 12063
                                                                },
                            'inc/Module/Install/Metadata.pm' => {
                                                                  'mtime' => 1706972691,
                                                                  'no_index' => 1,
                                                                  'size' => 18207
                                                                },
                            'inc/Module/Install/ReadmeFromPod.pm' => {
                                                                       'mtime' => 1706972691,
                                                                       'no_index' => 1,
                                                                       'size' => 4212
                                                                     },
                            'inc/Module/Install/Win32.pm' => {
                                                               'mtime' => 1706972691,
                                                               'no_index' => 1,
                                                               'size' => 1795
                                                             },
                            'inc/Module/Install/WriteAll.pm' => {
                                                                  'mtime' => 1706972691,
                                                                  'no_index' => 1,
                                                                  'size' => 1278
                                                                },
                            't/Mo-utils-CSS/01-use.t' => {
                                                           'mtime' => 1689483910,
                                                           'no_index' => 1,
                                                           'requires' => {
                                                                           'Test::More' => '0',
                                                                           'Test::NoWarnings' => '0',
                                                                           'strict' => '0',
                                                                           'warnings' => '0'
                                                                         },
                                                           'size' => 165
                                                         },
                            't/Mo-utils-CSS/02-version.t' => {
                                                               'mtime' => 1706953224,
                                                               'no_index' => 1,
                                                               'requires' => {
                                                                               'Mo::utils::CSS' => '0',
                                                                               'Test::More' => '0',
                                                                               'Test::NoWarnings' => '0',
                                                                               'strict' => '0',
                                                                               'warnings' => '0'
                                                                             },
                                                               'size' => 155
                                                             },
                            't/Mo-utils-CSS/03-check_array_css_color.t' => {
                                                                             'mtime' => 1706952887,
                                                                             'no_index' => 1,
                                                                             'requires' => {
                                                                                             'English' => '0',
                                                                                             'Error::Pure::Utils' => '0',
                                                                                             'Mo::utils::CSS' => '0',
                                                                                             'Test::MockObject' => '0',
                                                                                             'Test::More' => '0',
                                                                                             'Test::NoWarnings' => '0',
                                                                                             'strict' => '0',
                                                                                             'warnings' => '0'
                                                                                           },
                                                                             'size' => 1234
                                                                           },
                            't/Mo-utils-CSS/04-check_css_class.t' => {
                                                                       'mtime' => 1709546878,
                                                                       'no_index' => 1,
                                                                       'requires' => {
                                                                                       'English' => '0',
                                                                                       'Error::Pure::Utils' => '0',
                                                                                       'Mo::utils::CSS' => '0',
                                                                                       'Readonly' => '0',
                                                                                       'Test::More' => '0',
                                                                                       'Test::NoWarnings' => '0',
                                                                                       'strict' => '0',
                                                                                       'warnings' => '0'
                                                                                     },
                                                                       'size' => 1380
                                                                     },
                            't/Mo-utils-CSS/05-check_css_color.t' => {
                                                                       'mtime' => 1709546678,
                                                                       'no_index' => 1,
                                                                       'requires' => {
                                                                                       'English' => '0',
                                                                                       'Error::Pure::Utils' => '0',
                                                                                       'Mo::utils::CSS' => '0',
                                                                                       'Readonly' => '0',
                                                                                       'Test::More' => '0',
                                                                                       'Test::NoWarnings' => '0',
                                                                                       'strict' => '0',
                                                                                       'warnings' => '0'
                                                                                     },
                                                                       'size' => 9403
                                                                     },
                            't/Mo-utils-CSS/06-check_css_unit.t' => {
                                                                      'mtime' => 1706952963,
                                                                      'no_index' => 1,
                                                                      'requires' => {
                                                                                      'English' => '0',
                                                                                      'Error::Pure::Utils' => '0',
                                                                                      'Mo::utils::CSS' => '0',
                                                                                      'Readonly' => '0',
                                                                                      'Test::More' => '0',
                                                                                      'Test::NoWarnings' => '0',
                                                                                      'strict' => '0',
                                                                                      'warnings' => '0'
                                                                                    },
                                                                      'size' => 1744
                                                                    },
                            'xt/Mo-utils-CSS/01-pod_coverage.t' => {
                                                                     'mtime' => 1689483910,
                                                                     'no_index' => 1,
                                                                     'size' => 161
                                                                   },
                            'xt/Mo-utils-CSS/02-pod.t' => {
                                                            'mtime' => 1689483910,
                                                            'no_index' => 1,
                                                            'size' => 318
                                                          }
                          },
          'got_prereq_from' => 'META.yml',
          'ignored_files_array' => [
                                     'examples/check_array_css_color_fail.pl',
                                     'examples/check_array_css_color_ok.pl',
                                     'examples/check_css_class_fail.pl',
                                     'examples/check_css_class_ok.pl',
                                     'examples/check_css_color_fail.pl',
                                     'examples/check_css_color_ok.pl',
                                     'examples/check_css_unit_fail.pl',
                                     'examples/check_css_unit_ok.pl',
                                     'inc/Module/Install.pm',
                                     'inc/Module/Install/AuthorRequires.pm',
                                     'inc/Module/Install/AuthorTests.pm',
                                     'inc/Module/Install/Base.pm',
                                     'inc/Module/Install/Can.pm',
                                     'inc/Module/Install/Fetch.pm',
                                     'inc/Module/Install/Makefile.pm',
                                     'inc/Module/Install/Metadata.pm',
                                     'inc/Module/Install/ReadmeFromPod.pm',
                                     'inc/Module/Install/Win32.pm',
                                     'inc/Module/Install/WriteAll.pm',
                                     't/Mo-utils-CSS/01-use.t',
                                     't/Mo-utils-CSS/02-version.t',
                                     't/Mo-utils-CSS/03-check_array_css_color.t',
                                     't/Mo-utils-CSS/04-check_css_class.t',
                                     't/Mo-utils-CSS/05-check_css_color.t',
                                     't/Mo-utils-CSS/06-check_css_unit.t',
                                     'xt/Mo-utils-CSS/01-pod_coverage.t',
                                     'xt/Mo-utils-CSS/02-pod.t'
                                   ],
          'included_modules' => [
                                  'Module::Install',
                                  'Module::Install::AuthorRequires',
                                  'Module::Install::AuthorTests',
                                  'Module::Install::Base',
                                  'Module::Install::Can',
                                  'Module::Install::Fetch',
                                  'Module::Install::Makefile',
                                  'Module::Install::Metadata',
                                  'Module::Install::ReadmeFromPod',
                                  'Module::Install::Win32',
                                  'Module::Install::WriteAll'
                                ],
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 0,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 1,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 31,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 0,
                          'meta_yml_has_repository_resource' => 1,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 1,
                          'use_warnings' => 1
                        },
          'latest_mtime' => 1709546905,
          'license' => 'bsd defined in META.yml defined in LICENSE',
          'license_file' => 'CSS.pm',
          'license_from_yaml' => 'bsd',
          'license_in_pod' => 1,
          'license_type' => 'BSD',
          'licenses' => {
                          'BSD' => [
                                     'CSS.pm'
                                   ]
                        },
          'manifest_matches_dist' => 1,
          'meta_yml' => {
                          'abstract' => 'Mo CSS utilities.',
                          'author' => [
                                        'Michal Josef Spacek <skim@cpan.org>'
                                      ],
                          'build_requires' => {
                                                'English' => '0',
                                                'Error::Pure::Utils' => '0',
                                                'ExtUtils::MakeMaker' => '6.59',
                                                'Readonly' => '0',
                                                'Test::More' => '0',
                                                'Test::NoWarnings' => '0'
                                              },
                          'configure_requires' => {
                                                    'ExtUtils::MakeMaker' => '6.59'
                                                  },
                          'distribution_type' => 'module',
                          'dynamic_config' => '1',
                          'generated_by' => 'Module::Install version 1.21',
                          'license' => 'bsd',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'Mo-utils-CSS',
                          'no_index' => {
                                          'directory' => [
                                                           'examples',
                                                           'inc',
                                                           't',
                                                           'xt'
                                                         ]
                                        },
                          'requires' => {
                                          'Error::Pure' => '0.15',
                                          'Exporter' => '0',
                                          'Graphics::ColorNames::CSS' => '0',
                                          'List::Util' => '1.33',
                                          'Mo::utils' => '0.06',
                                          'Readonly' => '0',
                                          'perl' => '5.6.2'
                                        },
                          'resources' => {
                                           'bugtracker' => 'https://github.com/michal-josef-spacek/Mo-utils-CSS/issues',
                                           'homepage' => 'https://github.com/michal-josef-spacek/Mo-utils-CSS',
                                           'license' => 'http://opensource.org/licenses/bsd-license.php',
                                           'repository' => 'git://github.com/michal-josef-spacek/Mo-utils-CSS'
                                         },
                          'version' => '0.05'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'module_install' => {
                                'version' => '1.21'
                              },
          'modules' => [
                         {
                           'file' => 'CSS.pm',
                           'in_basedir' => 1,
                           'in_lib' => 0,
                           'module' => 'Mo::utils::CSS'
                         }
                       ],
          'no_index' => '^examples/;^inc/;^t/;^xt/',
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/Mo-utils-CSS.tar.gz',
          'prereq' => [
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'English',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Error::Pure',
                          'type' => 'runtime_requires',
                          'version' => '0.15'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Error::Pure::Utils',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Exporter',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'build_requires',
                          'version' => '6.59'
                        },
                        {
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'configure_requires',
                          'version' => '6.59'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Graphics::ColorNames::CSS',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'List::Util',
                          'type' => 'runtime_requires',
                          'version' => '1.33'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Mo::utils',
                          'type' => 'runtime_requires',
                          'version' => '0.06'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Readonly',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Readonly',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::NoWarnings',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => '5.6.2'
                        }
                      ],
          'released' => 1709583453,
          'size_packed' => 29315,
          'size_unpacked' => 101235,
          'test_files' => [
                            't/Mo-utils-CSS/01-use.t',
                            't/Mo-utils-CSS/02-version.t',
                            't/Mo-utils-CSS/03-check_array_css_color.t',
                            't/Mo-utils-CSS/04-check_css_class.t',
                            't/Mo-utils-CSS/05-check_css_color.t',
                            't/Mo-utils-CSS/06-check_css_unit.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'requires' => {
                                                       'lib' => '0',
                                                       'strict' => '0',
                                                       'warnings' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'requires' => {
                                                     'Error::Pure' => '0',
                                                     'Exporter' => '0',
                                                     'Graphics::ColorNames::CSS' => '0',
                                                     'List::Util' => '1.33',
                                                     'Mo::utils' => '0.06',
                                                     'Readonly' => '0',
                                                     'base' => '0',
                                                     'strict' => '0',
                                                     'warnings' => '0'
                                                   }
                                   },
                      'test' => {
                                  'requires' => {
                                                  'English' => '0',
                                                  'Error::Pure::Utils' => '0',
                                                  'Readonly' => '0',
                                                  'Test::MockObject' => '0',
                                                  'Test::More' => '0',
                                                  'Test::NoWarnings' => '0',
                                                  'strict' => '0',
                                                  'warnings' => '0'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'Mo-utils-CSS'
        };
1;
