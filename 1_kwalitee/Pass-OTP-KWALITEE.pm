$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'Pass::OTP' => 'Perl implementation of HOTP / TOTP algorithms',
                                  'Pass::OTP::URI' => 'Parse otpauth:// URI',
                                  'oathtool' => 'alternative Perl implementation of oathtool(1), one-time password tool',
                                  'otptool' => 'one-time password tool'
                                },
          'author' => '',
          'dir_lib' => 'lib',
          'dir_t' => 't',
          'dirs' => 5,
          'dirs_array' => [
                            'bin',
                            'lib/Pass/OTP',
                            'lib/Pass',
                            'lib',
                            't'
                          ],
          'dist' => 'Pass-OTP',
          'dynamic_config' => 1,
          'error' => {
                       'extracts_nicely' => 'expected Pass-OTP but got Pass-OTP-1.7',
                       'no_pax_headers' => './PaxHeaders/Pass-OTP-1.7,./PaxHeaders/Pass-OTP-1.7,Pass-OTP-1.7/PaxHeaders/lib,Pass-OTP-1.7/PaxHeaders/lib,Pass-OTP-1.7/lib/PaxHeaders/Pass,Pass-OTP-1.7/lib/PaxHeaders/Pass,Pass-OTP-1.7/lib/Pass/PaxHeaders/OTP,Pass-OTP-1.7/lib/Pass/PaxHeaders/OTP,Pass-OTP-1.7/lib/Pass/OTP/PaxHeaders/URI.pm,Pass-OTP-1.7/lib/Pass/OTP/PaxHeaders/URI.pm,Pass-OTP-1.7/lib/Pass/PaxHeaders/OTP.pm,Pass-OTP-1.7/lib/Pass/PaxHeaders/OTP.pm,Pass-OTP-1.7/PaxHeaders/README,Pass-OTP-1.7/PaxHeaders/README,Pass-OTP-1.7/PaxHeaders/Makefile.PL,Pass-OTP-1.7/PaxHeaders/Makefile.PL,Pass-OTP-1.7/PaxHeaders/README.pod,Pass-OTP-1.7/PaxHeaders/README.pod,Pass-OTP-1.7/PaxHeaders/Changes,Pass-OTP-1.7/PaxHeaders/Changes,Pass-OTP-1.7/PaxHeaders/t,Pass-OTP-1.7/PaxHeaders/t,Pass-OTP-1.7/t/PaxHeaders/oathtool.t,Pass-OTP-1.7/t/PaxHeaders/oathtool.t,Pass-OTP-1.7/t/PaxHeaders/totp-test-vectors.t,Pass-OTP-1.7/t/PaxHeaders/totp-test-vectors.t,Pass-OTP-1.7/t/PaxHeaders/simple.t,Pass-OTP-1.7/t/PaxHeaders/simple.t,Pass-OTP-1.7/PaxHeaders/MANIFEST,Pass-OTP-1.7/PaxHeaders/MANIFEST,Pass-OTP-1.7/PaxHeaders/.perltidyrc,Pass-OTP-1.7/PaxHeaders/.perltidyrc,Pass-OTP-1.7/PaxHeaders/bin,Pass-OTP-1.7/PaxHeaders/bin,Pass-OTP-1.7/bin/PaxHeaders/otptool,Pass-OTP-1.7/bin/PaxHeaders/otptool,Pass-OTP-1.7/bin/PaxHeaders/oathtool,Pass-OTP-1.7/bin/PaxHeaders/oathtool,Pass-OTP-1.7/PaxHeaders/META.yml,Pass-OTP-1.7/PaxHeaders/META.yml,Pass-OTP-1.7/PaxHeaders/META.json,Pass-OTP-1.7/PaxHeaders/META.json'
                     },
          'extension' => 'tar.gz',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_changelog' => 'Changes',
          'file_makefile_pl' => 'Makefile.PL',
          'file_manifest' => 'MANIFEST',
          'file_meta_json' => 'META.json',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README,README.pod',
          'files' => 12,
          'files_array' => [
                             '.perltidyrc',
                             'Changes',
                             'MANIFEST',
                             'META.json',
                             'META.yml',
                             'Makefile.PL',
                             'README',
                             'README.pod',
                             'bin/oathtool',
                             'bin/otptool',
                             'lib/Pass/OTP/URI.pm',
                             'lib/Pass/OTP.pm'
                           ],
          'files_hash' => {
                            '.perltidyrc' => {
                                               'mtime' => 1694599206,
                                               'size' => 329
                                             },
                            'Changes' => {
                                           'mtime' => 1709384622,
                                           'size' => 566
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1709384863,
                                            'size' => 355
                                          },
                            'META.json' => {
                                             'mtime' => 1709384863,
                                             'size' => 1179
                                           },
                            'META.yml' => {
                                            'mtime' => 1709384863,
                                            'size' => 675
                                          },
                            'Makefile.PL' => {
                                               'mtime' => 1694599206,
                                               'requires' => {
                                                               'ExtUtils::MakeMaker' => '0',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 685
                                             },
                            'README' => {
                                          'mtime' => 1709384535,
                                          'size' => 1694
                                        },
                            'README.pod' => {
                                              'license' => 'GPL_1,GPL_2,GPL_3',
                                              'mtime' => 1709384541,
                                              'size' => 1624
                                            },
                            'bin/oathtool' => {
                                                'license' => 'GPL_1,GPL_2,GPL_3',
                                                'mtime' => 1694599206,
                                                'size' => 1769
                                              },
                            'bin/otptool' => {
                                               'license' => 'GPL_1,GPL_2,GPL_3',
                                               'mtime' => 1694599206,
                                               'size' => 925
                                             },
                            'lib/Pass/OTP.pm' => {
                                                   'license' => 'GPL_1,GPL_2,GPL_3',
                                                   'module' => 'Pass::OTP',
                                                   'mtime' => 1709384558,
                                                   'requires' => {
                                                                   'Convert::Base32' => '0',
                                                                   'Digest::HMAC' => '0',
                                                                   'Digest::SHA' => '0',
                                                                   'Exporter' => '0',
                                                                   'Math::BigInt' => '0',
                                                                   'strict' => '0',
                                                                   'utf8' => '0',
                                                                   'warnings' => '0'
                                                                 },
                                                   'size' => 3814
                                                 },
                            'lib/Pass/OTP/URI.pm' => {
                                                       'license' => 'GPL_1,GPL_2,GPL_3',
                                                       'module' => 'Pass::OTP::URI',
                                                       'mtime' => 1709384569,
                                                       'requires' => {
                                                                       'Exporter' => '0',
                                                                       'strict' => '0',
                                                                       'utf8' => '0',
                                                                       'warnings' => '0'
                                                                     },
                                                       'size' => 1275
                                                     },
                            't/oathtool.t' => {
                                                'mtime' => 1694601028,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'Test::More' => '0.88',
                                                                'open' => '0',
                                                                'strict' => '0',
                                                                'utf8' => '0',
                                                                'warnings' => '0'
                                                              },
                                                'size' => 3205
                                              },
                            't/simple.t' => {
                                              'mtime' => 1694599206,
                                              'no_index' => 1,
                                              'requires' => {
                                                              'Test::More' => '0.88',
                                                              'strict' => '0',
                                                              'utf8' => '0',
                                                              'warnings' => '0'
                                                            },
                                              'size' => 709
                                            },
                            't/totp-test-vectors.t' => {
                                                         'mtime' => 1709384363,
                                                         'no_index' => 1,
                                                         'requires' => {
                                                                         'Test::More' => '0.88',
                                                                         'strict' => '0',
                                                                         'utf8' => '0',
                                                                         'warnings' => '0'
                                                                       },
                                                         'size' => 2197
                                                       }
                          },
          'got_prereq_from' => 'META.yml',
          'ignored_files_array' => [
                                     't/oathtool.t',
                                     't/simple.t',
                                     't/totp-test-vectors.t'
                                   ],
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 1,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 0,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 30,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 0,
                          'meta_yml_has_repository_resource' => 0,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 1,
                          'use_warnings' => 1
                        },
          'latest_mtime' => 1709384863,
          'license' => 'perl defined in META.yml',
          'license_from_yaml' => 'perl',
          'license_in_pod' => 1,
          'licenses' => {
                          'GPL_1' => [
                                       'README.pod',
                                       'bin/oathtool',
                                       'bin/otptool',
                                       'lib/Pass/OTP.pm',
                                       'lib/Pass/OTP/URI.pm'
                                     ],
                          'GPL_2' => [
                                       'README.pod',
                                       'bin/oathtool',
                                       'bin/otptool',
                                       'lib/Pass/OTP.pm',
                                       'lib/Pass/OTP/URI.pm'
                                     ],
                          'GPL_3' => [
                                       'README.pod',
                                       'bin/oathtool',
                                       'bin/otptool',
                                       'lib/Pass/OTP.pm',
                                       'lib/Pass/OTP/URI.pm'
                                     ]
                        },
          'manifest_matches_dist' => 1,
          'meta_json' => {
                           'abstract' => 'Perl implementation of HOTP / TOTP algorithms',
                           'author' => [
                                         'Jan Baier <jan.baier@amagical.net>'
                                       ],
                           'dynamic_config' => 1,
                           'generated_by' => 'ExtUtils::MakeMaker version 7.70, CPAN::Meta::Converter version 2.150010',
                           'license' => [
                                          'perl_5'
                                        ],
                           'meta-spec' => {
                                            'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                            'version' => 2
                                          },
                           'name' => 'Pass-OTP',
                           'no_index' => {
                                           'directory' => [
                                                            't',
                                                            'inc'
                                                          ]
                                         },
                           'prereqs' => {
                                          'build' => {
                                                       'requires' => {
                                                                       'ExtUtils::MakeMaker' => '0'
                                                                     }
                                                     },
                                          'configure' => {
                                                           'requires' => {
                                                                           'ExtUtils::MakeMaker' => '0'
                                                                         }
                                                         },
                                          'runtime' => {
                                                         'requires' => {
                                                                         'Convert::Base32' => '0',
                                                                         'Digest::HMAC' => '0',
                                                                         'Digest::SHA' => '0',
                                                                         'Math::BigInt' => '1.999806',
                                                                         'perl' => '5.014'
                                                                       }
                                                       },
                                          'test' => {
                                                      'requires' => {
                                                                      'Test::More' => '0'
                                                                    }
                                                    }
                                        },
                           'release_status' => 'stable',
                           'version' => '1.7',
                           'x_serialization_backend' => 'JSON::PP version 4.16'
                         },
          'meta_json_is_parsable' => 1,
          'meta_json_spec_version' => 2,
          'meta_yml' => {
                          'abstract' => 'Perl implementation of HOTP / TOTP algorithms',
                          'author' => [
                                        'Jan Baier <jan.baier@amagical.net>'
                                      ],
                          'build_requires' => {
                                                'ExtUtils::MakeMaker' => '0',
                                                'Test::More' => '0'
                                              },
                          'configure_requires' => {
                                                    'ExtUtils::MakeMaker' => '0'
                                                  },
                          'dynamic_config' => '1',
                          'generated_by' => 'ExtUtils::MakeMaker version 7.70, CPAN::Meta::Converter version 2.150010',
                          'license' => 'perl',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'Pass-OTP',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc'
                                                         ]
                                        },
                          'requires' => {
                                          'Convert::Base32' => '0',
                                          'Digest::HMAC' => '0',
                                          'Digest::SHA' => '0',
                                          'Math::BigInt' => '1.999806',
                                          'perl' => '5.014'
                                        },
                          'version' => '1.7',
                          'x_serialization_backend' => 'CPAN::Meta::YAML version 0.018'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/Pass/OTP.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Pass::OTP'
                         },
                         {
                           'file' => 'lib/Pass/OTP/URI.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Pass::OTP::URI'
                         }
                       ],
          'no_index' => '^inc/;^t/',
          'no_pax_headers' => 0,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/Pass-OTP.tar.gz',
          'prereq' => [
                        {
                          'is_prereq' => 1,
                          'requires' => 'Convert::Base32',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Digest::HMAC',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Digest::SHA',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Math::BigInt',
                          'type' => 'runtime_requires',
                          'version' => '1.999806'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => '5.014'
                        }
                      ],
          'released' => 1709583476,
          'size_packed' => 6796,
          'size_unpacked' => 21001,
          'test_files' => [
                            't/oathtool.t',
                            't/simple.t',
                            't/totp-test-vectors.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'requires' => {
                                                       'ExtUtils::MakeMaker' => '0',
                                                       'strict' => '0',
                                                       'warnings' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'requires' => {
                                                     'Convert::Base32' => '0',
                                                     'Digest::HMAC' => '0',
                                                     'Digest::SHA' => '0',
                                                     'Exporter' => '0',
                                                     'Math::BigInt' => '0',
                                                     'strict' => '0',
                                                     'utf8' => '0',
                                                     'warnings' => '0'
                                                   }
                                   },
                      'test' => {
                                  'requires' => {
                                                  'Test::More' => '0.88',
                                                  'open' => '0',
                                                  'strict' => '0',
                                                  'utf8' => '0',
                                                  'warnings' => '0'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'Pass-OTP'
        };
1;
