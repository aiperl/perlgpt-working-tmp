$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'Test::HTTP::LocalServer' => 'spawn a local HTTP server for testing'
                                },
          'author' => '',
          'dir_lib' => 'lib',
          'dir_t' => 't',
          'dir_xt' => 'xt',
          'dirs' => 8,
          'dirs_array' => [
                            '.github/workflows',
                            '.github',
                            'development',
                            'lib/Test/HTTP',
                            'lib/Test',
                            'lib',
                            't',
                            'xt'
                          ],
          'dist' => 'Test-HTTP-LocalServer',
          'dynamic_config' => 0,
          'error' => {
                       'extracts_nicely' => 'expected Test-HTTP-LocalServer but got Test-HTTP-LocalServer-0.76',
                       'use_warnings' => 'Test::HTTP::LocalServer'
                     },
          'extension' => 'tar.gz',
          'external_license_file' => 'LICENSE',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_changelog' => 'Changes',
          'file_license' => 'LICENSE',
          'file_makefile_pl' => 'Makefile.PL',
          'file_manifest' => 'MANIFEST',
          'file_manifest_skip' => 'MANIFEST.SKIP',
          'file_meta_json' => 'META.json',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README,README.mkdn',
          'files' => 30,
          'files_array' => [
                             '.gitignore',
                             'Changes',
                             'LICENSE',
                             'MANIFEST',
                             'MANIFEST.SKIP',
                             'META.json',
                             'META.yml',
                             'Makefile.PL',
                             'README',
                             'README.mkdn',
                             'development/gen-bzipbomb.pl',
                             'development/gen-gzipbomb.pl',
                             'lib/Test/HTTP/LocalServer.pm',
                             'lib/Test/HTTP/cookie-server',
                             'lib/Test/HTTP/gzip-streamed.psgi',
                             'lib/Test/HTTP/log-server',
                             'testrules.yml',
                             'xt/99-changes.t',
                             'xt/99-compile.t',
                             'xt/99-examples.t',
                             'xt/99-manifest.t',
                             'xt/99-minimumversion.t',
                             'xt/99-pod.t',
                             'xt/99-synopsis.t',
                             'xt/99-test-prerequisites.t',
                             'xt/99-todo.t',
                             'xt/99-unix-text.t',
                             'xt/99-versions.t',
                             'xt/copyright.t',
                             'xt/meta-lint.t'
                           ],
          'files_hash' => {
                            '.github/workflows/linux.yml' => {
                                                               'mtime' => 1709491504,
                                                               'size' => 565
                                                             },
                            '.github/workflows/macos.yml' => {
                                                               'mtime' => 1709491504,
                                                               'size' => 432
                                                             },
                            '.github/workflows/windows.yml' => {
                                                                 'mtime' => 1709491504,
                                                                 'size' => 612
                                                               },
                            '.gitignore' => {
                                              'mtime' => 1709491504,
                                              'size' => 145
                                            },
                            'Changes' => {
                                           'mtime' => 1709491504,
                                           'size' => 4082
                                         },
                            'LICENSE' => {
                                           'mtime' => 1709491504,
                                           'size' => 8893
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1709491504,
                                            'size' => 748
                                          },
                            'MANIFEST.SKIP' => {
                                                 'mtime' => 1709491504,
                                                 'size' => 273
                                               },
                            'META.json' => {
                                             'mtime' => 1709491506,
                                             'size' => 2141
                                           },
                            'META.yml' => {
                                            'mtime' => 1709491506,
                                            'size' => 1183
                                          },
                            'Makefile.PL' => {
                                               'mtime' => 1709491504,
                                               'requires' => {
                                                               'ExtUtils::MakeMaker' => '0',
                                                               'strict' => '0'
                                                             },
                                               'size' => 7836,
                                               'suggests' => {
                                                               'Pod::Markdown' => '0',
                                                               'heading' => '0'
                                                             }
                                             },
                            'README' => {
                                          'mtime' => 1709491504,
                                          'size' => 1217
                                        },
                            'README.mkdn' => {
                                               'mtime' => 1709491504,
                                               'size' => 6661
                                             },
                            'development/gen-bzipbomb.pl' => {
                                                               'mtime' => 1709491504,
                                                               'size' => 613
                                                             },
                            'development/gen-gzipbomb.pl' => {
                                                               'mtime' => 1709491504,
                                                               'size' => 632
                                                             },
                            'lib/Test/HTTP/LocalServer.pm' => {
                                                                'license' => 'Perl_5',
                                                                'module' => 'Test::HTTP::LocalServer',
                                                                'mtime' => 1709491504,
                                                                'noes' => {
                                                                            'strict' => '0'
                                                                          },
                                                                'recommends' => {
                                                                                  'POSIX' => '0'
                                                                                },
                                                                'requires' => {
                                                                                'Carp' => '0',
                                                                                'Cwd' => '0',
                                                                                'File::Basename' => '0',
                                                                                'File::Spec' => '0',
                                                                                'File::Temp' => '0',
                                                                                'FindBin' => '0',
                                                                                'HTTP::Daemon' => '6.05',
                                                                                'HTTP::Tiny' => '0',
                                                                                'Time::HiRes' => '0',
                                                                                'URI::URL' => '0',
                                                                                'perl' => '5.008',
                                                                                'strict' => '0',
                                                                                'vars' => '0'
                                                                              },
                                                                'size' => 12297
                                                              },
                            'lib/Test/HTTP/cookie-server' => {
                                                               'mtime' => 1709491504,
                                                               'size' => 942
                                                             },
                            'lib/Test/HTTP/gzip-streamed.psgi' => {
                                                                    'mtime' => 1709491504,
                                                                    'size' => 1173
                                                                  },
                            'lib/Test/HTTP/log-server' => {
                                                            'mtime' => 1709491504,
                                                            'size' => 15150
                                                          },
                            't/00-load.t' => {
                                               'mtime' => 1709491504,
                                               'no_index' => 1,
                                               'requires' => {
                                                               'Test::More' => '0',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 360
                                             },
                            't/01-start.t' => {
                                                'mtime' => 1709491504,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'HTTP::Tiny' => '0',
                                                                'Test::HTTP::LocalServer' => '0',
                                                                'Test::More' => '0',
                                                                'strict' => '0',
                                                                'warnings' => '0'
                                                              },
                                                'size' => 948
                                              },
                            't/02-start-stop.t' => {
                                                     'mtime' => 1709491504,
                                                     'no_index' => 1,
                                                     'requires' => {
                                                                     'Test::HTTP::LocalServer' => '0',
                                                                     'Test::More' => '0',
                                                                     'strict' => '0',
                                                                     'warnings' => '0'
                                                                   },
                                                     'size' => 481
                                                   },
                            't/03-sticky-fields.t' => {
                                                        'mtime' => 1709491504,
                                                        'no_index' => 1,
                                                        'requires' => {
                                                                        'HTTP::Tiny' => '0',
                                                                        'Test::HTTP::LocalServer' => '0',
                                                                        'Test::More' => '0',
                                                                        'Time::HiRes' => '0',
                                                                        'strict' => '0',
                                                                        'warnings' => '0'
                                                                      },
                                                        'size' => 1099
                                                      },
                            't/04-server-name.t' => {
                                                      'mtime' => 1709491504,
                                                      'no_index' => 1,
                                                      'requires' => {
                                                                      'Test::HTTP::LocalServer' => '0',
                                                                      'Test::More' => '0',
                                                                      'strict' => '0',
                                                                      'warnings' => '0'
                                                                    },
                                                      'size' => 738
                                                    },
                            't/05-basic-auth.t' => {
                                                     'mtime' => 1709491504,
                                                     'no_index' => 1,
                                                     'requires' => {
                                                                     'HTTP::Tiny' => '0',
                                                                     'Test::HTTP::LocalServer' => '0',
                                                                     'Test::More' => '0',
                                                                     'strict' => '0',
                                                                     'warnings' => '0'
                                                                   },
                                                     'size' => 1250
                                                   },
                            't/05-simplest.t' => {
                                                   'mtime' => 1709491504,
                                                   'no_index' => 1,
                                                   'requires' => {
                                                                   'Test::HTTP::LocalServer' => '0',
                                                                   'Test::More' => '0',
                                                                   'strict' => '0',
                                                                   'warnings' => '0'
                                                                 },
                                                   'size' => 200
                                                 },
                            't/06-exitcode.t' => {
                                                   'mtime' => 1709491504,
                                                   'no_index' => 1,
                                                   'requires' => {
                                                                   'Test::HTTP::LocalServer' => '0',
                                                                   'Test::More' => '0',
                                                                   'strict' => '0',
                                                                   'warnings' => '0'
                                                                 },
                                                   'size' => 390
                                                 },
                            'testrules.yml' => {
                                                 'mtime' => 1709491504,
                                                 'size' => 81
                                               },
                            'xt/99-changes.t' => {
                                                   'mtime' => 1709491504,
                                                   'size' => 735
                                                 },
                            'xt/99-compile.t' => {
                                                   'mtime' => 1709491504,
                                                   'size' => 1045
                                                 },
                            'xt/99-examples.t' => {
                                                    'mtime' => 1709491504,
                                                    'size' => 373
                                                  },
                            'xt/99-manifest.t' => {
                                                    'mtime' => 1709491504,
                                                    'size' => 1060
                                                  },
                            'xt/99-minimumversion.t' => {
                                                          'mtime' => 1709491504,
                                                          'size' => 313
                                                        },
                            'xt/99-pod.t' => {
                                               'mtime' => 1709491504,
                                               'size' => 813
                                             },
                            'xt/99-synopsis.t' => {
                                                    'mtime' => 1709491504,
                                                    'size' => 1545
                                                  },
                            'xt/99-test-prerequisites.t' => {
                                                              'mtime' => 1709491504,
                                                              'size' => 3421
                                                            },
                            'xt/99-todo.t' => {
                                                'mtime' => 1709491504,
                                                'size' => 1142
                                              },
                            'xt/99-unix-text.t' => {
                                                     'mtime' => 1709491504,
                                                     'size' => 997
                                                   },
                            'xt/99-versions.t' => {
                                                    'mtime' => 1709491504,
                                                    'size' => 1647
                                                  },
                            'xt/copyright.t' => {
                                                  'mtime' => 1709491504,
                                                  'size' => 2472
                                                },
                            'xt/meta-lint.t' => {
                                                  'mtime' => 1709491504,
                                                  'size' => 1142
                                                }
                          },
          'got_prereq_from' => 'META.yml',
          'ignored_files_array' => [
                                     't/00-load.t',
                                     't/01-start.t',
                                     't/02-start-stop.t',
                                     't/03-sticky-fields.t',
                                     't/04-server-name.t',
                                     't/05-basic-auth.t',
                                     't/05-simplest.t',
                                     't/06-exitcode.t'
                                   ],
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 1,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 1,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 31,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 0,
                          'meta_yml_has_repository_resource' => 1,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 1,
                          'use_warnings' => 0
                        },
          'latest_mtime' => 1709491506,
          'license' => 'perl defined in META.yml defined in LICENSE',
          'license_file' => 'lib/Test/HTTP/LocalServer.pm',
          'license_from_yaml' => 'perl',
          'license_in_pod' => 1,
          'license_type' => 'Perl_5',
          'licenses' => {
                          'Perl_5' => [
                                        'lib/Test/HTTP/LocalServer.pm'
                                      ]
                        },
          'manifest_matches_dist' => 1,
          'meta_json' => {
                           'abstract' => 'spawn a local HTTP server for testing',
                           'author' => [
                                         'Max Maischein <corion@cpan.org>'
                                       ],
                           'dynamic_config' => 0,
                           'generated_by' => 'ExtUtils::MakeMaker version 7.64, CPAN::Meta::Converter version 2.150010',
                           'license' => [
                                          'perl_5'
                                        ],
                           'meta-spec' => {
                                            'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                            'version' => 2
                                          },
                           'name' => 'Test-HTTP-LocalServer',
                           'no_index' => {
                                           'directory' => [
                                                            't',
                                                            'inc'
                                                          ]
                                         },
                           'prereqs' => {
                                          'build' => {
                                                       'requires' => {
                                                                       'ExtUtils::MakeMaker' => '0',
                                                                       'File::Basename' => '0',
                                                                       'File::Copy' => '0',
                                                                       'File::Find' => '0',
                                                                       'File::Path' => '0'
                                                                     }
                                                     },
                                          'configure' => {
                                                           'requires' => {
                                                                           'ExtUtils::MakeMaker' => '0'
                                                                         }
                                                         },
                                          'runtime' => {
                                                         'requires' => {
                                                                         'CGI' => '0',
                                                                         'Carp' => '0',
                                                                         'Cwd' => '0',
                                                                         'File::Basename' => '0',
                                                                         'File::Spec' => '0',
                                                                         'File::Temp' => '0',
                                                                         'Getopt::Long' => '0',
                                                                         'HTTP::Daemon' => '6.05',
                                                                         'HTTP::Request::AsCGI' => '0',
                                                                         'HTTP::Response' => '0',
                                                                         'HTTP::Tiny' => '0',
                                                                         'IO::Socket::INET' => '0',
                                                                         'IO::Socket::IP' => '0.25',
                                                                         'Socket' => '0',
                                                                         'Time::HiRes' => '0',
                                                                         'URI' => '0',
                                                                         'URI::URL' => '0',
                                                                         'perl' => '5.008'
                                                                       }
                                                       },
                                          'test' => {
                                                      'requires' => {
                                                                      'Test::More' => '0'
                                                                    }
                                                    }
                                        },
                           'release_status' => 'stable',
                           'resources' => {
                                            'bugtracker' => {
                                                              'web' => 'https://github.com/Corion/Test-HTTP-LocalServer/issues'
                                                            },
                                            'license' => [
                                                           'https://dev.perl.org/licenses/'
                                                         ],
                                            'repository' => {
                                                              'type' => 'git',
                                                              'url' => 'git://github.com/Corion/Test-HTTP-LocalServer.git',
                                                              'web' => 'https://github.com/Corion/Test-HTTP-LocalServer'
                                                            }
                                          },
                           'version' => '0.76',
                           'x_serialization_backend' => 'JSON::PP version 4.07',
                           'x_static_install' => 0
                         },
          'meta_json_is_parsable' => 1,
          'meta_json_spec_version' => 2,
          'meta_yml' => {
                          'abstract' => 'spawn a local HTTP server for testing',
                          'author' => [
                                        'Max Maischein <corion@cpan.org>'
                                      ],
                          'build_requires' => {
                                                'ExtUtils::MakeMaker' => '0',
                                                'File::Basename' => '0',
                                                'File::Copy' => '0',
                                                'File::Find' => '0',
                                                'File::Path' => '0',
                                                'Test::More' => '0'
                                              },
                          'configure_requires' => {
                                                    'ExtUtils::MakeMaker' => '0'
                                                  },
                          'dynamic_config' => '0',
                          'generated_by' => 'ExtUtils::MakeMaker version 7.64, CPAN::Meta::Converter version 2.150010',
                          'license' => 'perl',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'Test-HTTP-LocalServer',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc'
                                                         ]
                                        },
                          'requires' => {
                                          'CGI' => '0',
                                          'Carp' => '0',
                                          'Cwd' => '0',
                                          'File::Basename' => '0',
                                          'File::Spec' => '0',
                                          'File::Temp' => '0',
                                          'Getopt::Long' => '0',
                                          'HTTP::Daemon' => '6.05',
                                          'HTTP::Request::AsCGI' => '0',
                                          'HTTP::Response' => '0',
                                          'HTTP::Tiny' => '0',
                                          'IO::Socket::INET' => '0',
                                          'IO::Socket::IP' => '0.25',
                                          'Socket' => '0',
                                          'Time::HiRes' => '0',
                                          'URI' => '0',
                                          'URI::URL' => '0',
                                          'perl' => '5.008'
                                        },
                          'resources' => {
                                           'bugtracker' => 'https://github.com/Corion/Test-HTTP-LocalServer/issues',
                                           'license' => 'https://dev.perl.org/licenses/',
                                           'repository' => 'git://github.com/Corion/Test-HTTP-LocalServer.git'
                                         },
                          'version' => '0.76',
                          'x_serialization_backend' => 'CPAN::Meta::YAML version 0.018',
                          'x_static_install' => '0'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/Test/HTTP/LocalServer.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Test::HTTP::LocalServer'
                         }
                       ],
          'no_index' => '^inc/;^t/',
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/Test-HTTP-LocalServer.tar.gz',
          'prereq' => [
                        {
                          'is_prereq' => 1,
                          'requires' => 'CGI',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Carp',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Cwd',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'File::Basename',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'File::Basename',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'File::Copy',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'File::Find',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'File::Path',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'File::Spec',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'File::Temp',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Getopt::Long',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'HTTP::Daemon',
                          'type' => 'runtime_requires',
                          'version' => '6.05'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'HTTP::Request::AsCGI',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'HTTP::Response',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'HTTP::Tiny',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'IO::Socket::INET',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'IO::Socket::IP',
                          'type' => 'runtime_requires',
                          'version' => '0.25'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Socket',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Time::HiRes',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'URI',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'URI::URL',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => '5.008'
                        }
                      ],
          'released' => 1709583459,
          'size_packed' => 31255,
          'size_unpacked' => 87847,
          'test_files' => [
                            't/00-load.t',
                            't/01-start.t',
                            't/02-start-stop.t',
                            't/03-sticky-fields.t',
                            't/04-server-name.t',
                            't/05-basic-auth.t',
                            't/05-simplest.t',
                            't/06-exitcode.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'requires' => {
                                                       'ExtUtils::MakeMaker' => '0',
                                                       'strict' => '0'
                                                     },
                                       'suggests' => {
                                                       'Pod::Markdown' => '0',
                                                       'heading' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'noes' => {
                                                 'strict' => '0'
                                               },
                                     'recommends' => {
                                                       'POSIX' => '0'
                                                     },
                                     'requires' => {
                                                     'Carp' => '0',
                                                     'Cwd' => '0',
                                                     'File::Basename' => '0',
                                                     'File::Spec' => '0',
                                                     'File::Temp' => '0',
                                                     'FindBin' => '0',
                                                     'HTTP::Daemon' => '6.05',
                                                     'HTTP::Tiny' => '0',
                                                     'Time::HiRes' => '0',
                                                     'URI::URL' => '0',
                                                     'perl' => '5.008',
                                                     'strict' => '0',
                                                     'vars' => '0'
                                                   }
                                   },
                      'test' => {
                                  'requires' => {
                                                  'HTTP::Tiny' => '0',
                                                  'Test::More' => '0',
                                                  'Time::HiRes' => '0',
                                                  'strict' => '0',
                                                  'warnings' => '0'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'Test-HTTP-LocalServer'
        };
1;
