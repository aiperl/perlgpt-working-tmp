$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'PAR' => 'Perl Archive Toolkit',
                                  'PAR::Environment' => 'Index and reference of PAR environment variables',
                                  'PAR::FAQ' => 'Frequently Asked Questions about PAR',
                                  'PAR::Heavy' => 'PAR guts',
                                  'PAR::SetupProgname' => 'Setup $ENV{PAR_PROGNAME}',
                                  'PAR::SetupTemp' => 'Setup $ENV{PAR_TEMP}',
                                  'PAR::Tutorial' => 'Cross-Platform Packaging and Deployment with PAR'
                                },
          'author' => '',
          'dir_lib' => 'lib',
          'dir_t' => 't',
          'dirs' => 6,
          'dirs_array' => [
                            'lib/PAR',
                            'lib',
                            't/data/lib',
                            't/data/script',
                            't/data',
                            't'
                          ],
          'dist' => 'PAR',
          'dynamic_config' => 1,
          'error' => {
                       'extracts_nicely' => 'expected PAR but got PAR-1.020'
                     },
          'extension' => 'tar.gz',
          'external_license_file' => 'LICENSE',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_changelog' => 'Changes',
          'file_license' => 'LICENSE',
          'file_makefile_pl' => 'Makefile.PL',
          'file_manifest' => 'MANIFEST',
          'file_manifest_skip' => 'MANIFEST.SKIP',
          'file_meta_json' => 'META.json',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README',
          'files' => 16,
          'files_array' => [
                             'AUTHORS',
                             'Changes',
                             'LICENSE',
                             'MANIFEST',
                             'MANIFEST.SKIP',
                             'META.json',
                             'META.yml',
                             'Makefile.PL',
                             'README',
                             'lib/PAR/Environment.pod',
                             'lib/PAR/FAQ.pod',
                             'lib/PAR/Heavy.pm',
                             'lib/PAR/SetupProgname.pm',
                             'lib/PAR/SetupTemp.pm',
                             'lib/PAR/Tutorial.pod',
                             'lib/PAR.pm'
                           ],
          'files_hash' => {
                            'AUTHORS' => {
                                           'mtime' => 1474195501,
                                           'size' => 3853
                                         },
                            'Changes' => {
                                           'mtime' => 1709549171,
                                           'size' => 49507
                                         },
                            'LICENSE' => {
                                           'mtime' => 1475334792,
                                           'size' => 18536
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1709549258,
                                            'size' => 534
                                          },
                            'MANIFEST.SKIP' => {
                                                 'mtime' => 1492097192,
                                                 'size' => 161
                                               },
                            'META.json' => {
                                             'mtime' => 1709549258,
                                             'size' => 1544
                                           },
                            'META.yml' => {
                                            'mtime' => 1709549258,
                                            'size' => 835
                                          },
                            'Makefile.PL' => {
                                               'mtime' => 1707652632,
                                               'requires' => {
                                                               'ExtUtils::MakeMaker' => '0',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 2591
                                             },
                            'README' => {
                                          'mtime' => 1702046747,
                                          'size' => 2099
                                        },
                            'lib/PAR.pm' => {
                                              'license' => 'Perl_5',
                                              'module' => 'PAR',
                                              'mtime' => 1707655191,
                                              'noes' => {
                                                          'warnings' => '0'
                                                        },
                                              'recommends' => {
                                                                'File::Glob' => '0',
                                                                'File::Spec' => '0',
                                                                'File::Temp' => '0',
                                                                'LWP::Simple' => '0',
                                                                'PAR::Heavy' => '0'
                                                              },
                                              'requires' => {
                                                              'Archive::Zip' => '0',
                                                              'Carp' => '0',
                                                              'Config' => '0',
                                                              'Fcntl' => '0',
                                                              'PAR::SetupProgname' => '0',
                                                              'PAR::SetupTemp' => '0',
                                                              'bytes' => '0',
                                                              'perl' => '5.008009',
                                                              'strict' => '0',
                                                              'vars' => '0',
                                                              'warnings' => '0'
                                                            },
                                              'size' => 40823,
                                              'suggests' => {
                                                              'Archive::Unzip::Burst' => '0',
                                                              'File::Spec' => '0',
                                                              'PAR::Repository::Client' => '0',
                                                              'prefork' => '0'
                                                            }
                                            },
                            'lib/PAR/Environment.pod' => {
                                                           'license' => 'Perl_5',
                                                           'mtime' => 1702047530,
                                                           'size' => 8417
                                                         },
                            'lib/PAR/FAQ.pod' => {
                                                   'license' => 'Perl_5',
                                                   'mtime' => 1702047531,
                                                   'size' => 20635
                                                 },
                            'lib/PAR/Heavy.pm' => {
                                                    'license' => 'Perl_5',
                                                    'module' => 'PAR::Heavy',
                                                    'mtime' => 1702047529,
                                                    'noes' => {
                                                                'strict' => '0',
                                                                'warnings' => '0'
                                                              },
                                                    'requires' => {
                                                                    'strict' => '0',
                                                                    'warnings' => '0'
                                                                  },
                                                    'size' => 6481,
                                                    'suggests' => {
                                                                    'DynaLoader' => '0'
                                                                  }
                                                  },
                            'lib/PAR/SetupProgname.pm' => {
                                                            'license' => 'Perl_5',
                                                            'module' => 'PAR::SetupProgname',
                                                            'mtime' => 1707652656,
                                                            'recommends' => {
                                                                              'File::Spec' => '0'
                                                                            },
                                                            'requires' => {
                                                                            'Config' => '0',
                                                                            'perl' => '5.008009',
                                                                            'strict' => '0',
                                                                            'warnings' => '0'
                                                                          },
                                                            'size' => 2310
                                                          },
                            'lib/PAR/SetupTemp.pm' => {
                                                        'license' => 'Perl_5',
                                                        'module' => 'PAR::SetupTemp',
                                                        'mtime' => 1707652649,
                                                        'recommends' => {
                                                                          'Digest::SHA' => '0',
                                                                          'File::Spec' => '0'
                                                                        },
                                                        'requires' => {
                                                                        'Fcntl' => '0',
                                                                        'PAR::SetupProgname' => '0',
                                                                        'perl' => '5.008009',
                                                                        'strict' => '0',
                                                                        'warnings' => '0'
                                                                      },
                                                        'size' => 4922,
                                                        'suggests' => {
                                                                        'Digest::MD5' => '0',
                                                                        'Digest::SHA' => '0',
                                                                        'Digest::SHA1' => '0'
                                                                      }
                                                      },
                            'lib/PAR/Tutorial.pod' => {
                                                        'license' => 'Perl_5',
                                                        'mtime' => 1702047532,
                                                        'size' => 20879
                                                      },
                            't/00-pod.t' => {
                                              'mtime' => 1474195499,
                                              'no_index' => 1,
                                              'requires' => {
                                                              'Test::More' => '0',
                                                              'strict' => '0',
                                                              'warnings' => '0'
                                                            },
                                              'size' => 260,
                                              'suggests' => {
                                                              'Test::Pod' => '1.00'
                                                            }
                                            },
                            't/01-basic.t' => {
                                                'mtime' => 1474195501,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'File::Path' => '0',
                                                                'File::Spec' => '0',
                                                                'File::Temp' => '0',
                                                                'PAR' => '0',
                                                                'Test::More' => '0',
                                                                'strict' => '0',
                                                                'warnings' => '0'
                                                              },
                                                'size' => 856
                                              },
                            't/40-par-hashref.t' => {
                                                      'mtime' => 1474195501,
                                                      'no_index' => 1,
                                                      'requires' => {
                                                                      'File::Spec' => '0',
                                                                      'File::Temp' => '0',
                                                                      'FindBin' => '0',
                                                                      'Hello' => '0',
                                                                      'Test::More' => '0',
                                                                      'strict' => '0',
                                                                      'vars' => '0'
                                                                    },
                                                      'size' => 1078,
                                                      'suggests' => {
                                                                      'Data' => '0',
                                                                      'PAR' => '0'
                                                                    }
                                                    },
                            't/50-autoloaderfix.t' => {
                                                        'mtime' => 1474195501,
                                                        'no_index' => 1,
                                                        'requires' => {
                                                                        'AutoLoader' => '0',
                                                                        'File::Temp' => '0',
                                                                        'PAR' => '0'
                                                                      },
                                                        'size' => 618
                                                      },
                            't/60-cleanup.t' => {
                                                  'mtime' => 1474195501,
                                                  'no_index' => 1,
                                                  'requires' => {
                                                                  'File::Temp' => '0',
                                                                  'PAR' => '0',
                                                                  'Test::More' => '0',
                                                                  'strict' => '0',
                                                                  'warnings' => '0'
                                                                },
                                                  'size' => 652
                                                },
                            't/Hello.pm' => {
                                              'mtime' => 1474195499,
                                              'no_index' => 1,
                                              'requires' => {
                                                              'strict' => '0'
                                                            },
                                              'size' => 114
                                            },
                            't/data/lib/Data.pm' => {
                                                      'mtime' => 1480263015,
                                                      'no_index' => 1,
                                                      'size' => 84
                                                    },
                            't/data/lib/Hello.pm' => {
                                                       'mtime' => 1480263015,
                                                       'no_index' => 1,
                                                       'size' => 75
                                                     },
                            't/data/script/data.pl' => {
                                                         'mtime' => 1480263015,
                                                         'no_index' => 1,
                                                         'size' => 82
                                                       },
                            't/data/script/hello.pl' => {
                                                          'mtime' => 1480263015,
                                                          'no_index' => 1,
                                                          'size' => 72
                                                        },
                            't/data/script/nostrict.pl' => {
                                                             'mtime' => 1480263015,
                                                             'no_index' => 1,
                                                             'size' => 52
                                                           },
                            't/gen-hello-par.pl' => {
                                                      'mtime' => 1480263015,
                                                      'no_index' => 1,
                                                      'size' => 202
                                                    }
                          },
          'got_prereq_from' => 'META.yml',
          'ignored_files_array' => [
                                     't/00-pod.t',
                                     't/01-basic.t',
                                     't/40-par-hashref.t',
                                     't/50-autoloaderfix.t',
                                     't/60-cleanup.t',
                                     't/Hello.pm',
                                     't/data/lib/Data.pm',
                                     't/data/lib/Hello.pm',
                                     't/data/script/data.pl',
                                     't/data/script/hello.pl',
                                     't/data/script/nostrict.pl',
                                     't/gen-hello-par.pl'
                                   ],
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 1,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 1,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 32,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 0,
                          'meta_yml_has_repository_resource' => 1,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 1,
                          'use_warnings' => 1
                        },
          'latest_mtime' => 1709549258,
          'license' => 'perl defined in META.yml defined in LICENSE',
          'license_file' => 'lib/PAR.pm,lib/PAR/Environment.pod,lib/PAR/FAQ.pod,lib/PAR/Heavy.pm,lib/PAR/SetupProgname.pm,lib/PAR/SetupTemp.pm,lib/PAR/Tutorial.pod',
          'license_from_yaml' => 'perl',
          'license_in_pod' => 1,
          'license_type' => 'Perl_5',
          'licenses' => {
                          'Perl_5' => [
                                        'lib/PAR.pm',
                                        'lib/PAR/Environment.pod',
                                        'lib/PAR/FAQ.pod',
                                        'lib/PAR/Heavy.pm',
                                        'lib/PAR/SetupProgname.pm',
                                        'lib/PAR/SetupTemp.pm',
                                        'lib/PAR/Tutorial.pod'
                                      ]
                        },
          'manifest_matches_dist' => 1,
          'meta_json' => {
                           'abstract' => 'Perl Archive Toolkit',
                           'author' => [
                                         'Audrey Tang <cpan@audreyt.org>'
                                       ],
                           'dynamic_config' => 1,
                           'generated_by' => 'ExtUtils::MakeMaker version 7.70, CPAN::Meta::Converter version 2.150010',
                           'license' => [
                                          'perl_5'
                                        ],
                           'meta-spec' => {
                                            'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                            'version' => 2
                                          },
                           'name' => 'PAR',
                           'no_index' => {
                                           'directory' => [
                                                            't',
                                                            'inc'
                                                          ]
                                         },
                           'prereqs' => {
                                          'build' => {
                                                       'requires' => {
                                                                       'ExtUtils::MakeMaker' => '0'
                                                                     }
                                                     },
                                          'configure' => {
                                                           'requires' => {
                                                                           'ExtUtils::MakeMaker' => '0'
                                                                         }
                                                         },
                                          'runtime' => {
                                                         'requires' => {
                                                                         'Archive::Zip' => '1.00',
                                                                         'AutoLoader' => '5.66_02',
                                                                         'Compress::Zlib' => '1.30',
                                                                         'Digest::SHA' => '5.45',
                                                                         'File::Temp' => '0.05',
                                                                         'PAR::Dist' => '0.32',
                                                                         'perl' => '5.008009'
                                                                       }
                                                       },
                                          'test' => {
                                                      'requires' => {
                                                                      'Test::More' => '0'
                                                                    }
                                                    }
                                        },
                           'release_status' => 'stable',
                           'resources' => {
                                            'bugtracker' => {
                                                              'web' => 'https://github.com/rschupp/PAR/issues'
                                                            },
                                            'repository' => {
                                                              'type' => 'git',
                                                              'url' => 'git://github.com/rschupp/PAR.git',
                                                              'web' => 'https://github.com/rschupp/PAR'
                                                            },
                                            'x_MailingList' => 'mailto:par@perl.org'
                                          },
                           'version' => '1.020',
                           'x_serialization_backend' => 'JSON::PP version 4.16'
                         },
          'meta_json_is_parsable' => 1,
          'meta_json_spec_version' => 2,
          'meta_yml' => {
                          'abstract' => 'Perl Archive Toolkit',
                          'author' => [
                                        'Audrey Tang <cpan@audreyt.org>'
                                      ],
                          'build_requires' => {
                                                'ExtUtils::MakeMaker' => '0',
                                                'Test::More' => '0'
                                              },
                          'configure_requires' => {
                                                    'ExtUtils::MakeMaker' => '0'
                                                  },
                          'dynamic_config' => '1',
                          'generated_by' => 'ExtUtils::MakeMaker version 7.70, CPAN::Meta::Converter version 2.150010',
                          'license' => 'perl',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'PAR',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc'
                                                         ]
                                        },
                          'requires' => {
                                          'Archive::Zip' => '1.00',
                                          'AutoLoader' => '5.66_02',
                                          'Compress::Zlib' => '1.30',
                                          'Digest::SHA' => '5.45',
                                          'File::Temp' => '0.05',
                                          'PAR::Dist' => '0.32',
                                          'perl' => '5.008009'
                                        },
                          'resources' => {
                                           'MailingList' => 'mailto:par@perl.org',
                                           'bugtracker' => 'https://github.com/rschupp/PAR/issues',
                                           'repository' => 'git://github.com/rschupp/PAR.git'
                                         },
                          'version' => '1.020',
                          'x_serialization_backend' => 'CPAN::Meta::YAML version 0.018'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/PAR.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'PAR'
                         },
                         {
                           'file' => 'lib/PAR/Heavy.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'PAR::Heavy'
                         },
                         {
                           'file' => 'lib/PAR/SetupProgname.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'PAR::SetupProgname'
                         },
                         {
                           'file' => 'lib/PAR/SetupTemp.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'PAR::SetupTemp'
                         }
                       ],
          'no_index' => '^inc/;^t/',
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/PAR.tar.gz',
          'prereq' => [
                        {
                          'is_prereq' => 1,
                          'requires' => 'Archive::Zip',
                          'type' => 'runtime_requires',
                          'version' => '1.00'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'AutoLoader',
                          'type' => 'runtime_requires',
                          'version' => '5.66_02'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Compress::Zlib',
                          'type' => 'runtime_requires',
                          'version' => '1.30'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Digest::SHA',
                          'type' => 'runtime_requires',
                          'version' => '5.45'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'File::Temp',
                          'type' => 'runtime_requires',
                          'version' => '0.05'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'PAR::Dist',
                          'type' => 'runtime_requires',
                          'version' => '0.32'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => '5.008009'
                        }
                      ],
          'released' => 1709583452,
          'size_packed' => 66318,
          'size_unpacked' => 188272,
          'test_files' => [
                            't/00-pod.t',
                            't/01-basic.t',
                            't/40-par-hashref.t',
                            't/50-autoloaderfix.t',
                            't/60-cleanup.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'requires' => {
                                                       'ExtUtils::MakeMaker' => '0',
                                                       'strict' => '0',
                                                       'warnings' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'noes' => {
                                                 'strict' => '0',
                                                 'warnings' => '0'
                                               },
                                     'recommends' => {
                                                       'Digest::SHA' => '0',
                                                       'File::Glob' => '0',
                                                       'File::Spec' => '0',
                                                       'File::Temp' => '0',
                                                       'LWP::Simple' => '0'
                                                     },
                                     'requires' => {
                                                     'Archive::Zip' => '0',
                                                     'Carp' => '0',
                                                     'Config' => '0',
                                                     'Fcntl' => '0',
                                                     'bytes' => '0',
                                                     'perl' => '5.008009',
                                                     'strict' => '0',
                                                     'vars' => '0',
                                                     'warnings' => '0'
                                                   },
                                     'suggests' => {
                                                     'Archive::Unzip::Burst' => '0',
                                                     'Digest::MD5' => '0',
                                                     'Digest::SHA' => '0',
                                                     'Digest::SHA1' => '0',
                                                     'DynaLoader' => '0',
                                                     'File::Spec' => '0',
                                                     'PAR::Repository::Client' => '0',
                                                     'prefork' => '0'
                                                   }
                                   },
                      'test' => {
                                  'requires' => {
                                                  'AutoLoader' => '0',
                                                  'File::Path' => '0',
                                                  'File::Spec' => '0',
                                                  'File::Temp' => '0',
                                                  'FindBin' => '0',
                                                  'Test::More' => '0',
                                                  'strict' => '0',
                                                  'vars' => '0',
                                                  'warnings' => '0'
                                                },
                                  'suggests' => {
                                                  'Data' => '0',
                                                  'Test::Pod' => '1.00'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'PAR'
        };
1;
