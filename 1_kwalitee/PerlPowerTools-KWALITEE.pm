$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'PerlPowerTools' => 'BSD utilities written in pure Perl',
                                  'addbib' => 'create or extend a bibliographic database',
                                  'apply' => 'Run a command many times with different arguments',
                                  'ar' => 'create and maintain library archives',
                                  'arch' => 'display system machine type',
                                  'arithmetic' => 'improve your arithmetic skills',
                                  'asa' => 'interpret ASA/FORTRAN carriage-controls',
                                  'awk' => 'pattern-directed scanning and processing language',
                                  'banner' => 'print large banner on printer',
                                  'base64' => 'encode and decode base64 data',
                                  'basename' => 'remove directory and suffix from filenames',
                                  'bc' => 'an arbitrary precision calculator language',
                                  'bcd' => 'format input as punch cards',
                                  'cal' => 'displays a calendar and the date of easter',
                                  'cat' => 'concatenate and print files',
                                  'chgrp' => 'change group ownership of files',
                                  'ching' => 'the Book of Changes',
                                  'chmod' => 'change permissions of files',
                                  'chown' => 'change ownership of files',
                                  'clear' => 'clear the screen',
                                  'cmp' => 'compare two files',
                                  'col' => 'filter reverse line feeds from input',
                                  'colrm' => 'remove columns from a file',
                                  'comm' => 'select or reject lines common to two files',
                                  'cp' => 'copy files and/or directories',
                                  'cut' => 'select portions of each line of a file',
                                  'date' => 'display date and time',
                                  'dc' => 'an arbitrary precision calculator',
                                  'deroff' => 'strip troff, eqn and tbl sequences from text',
                                  'diff' => 'compute `intelligent\' differences between two files',
                                  'dirname' => 'print the directory name of a path',
                                  'du' => 'display disk usage statistics',
                                  'echo' => 'echo arguments',
                                  'ed' => 'text editor',
                                  'env' => 'Run a program in a modified environment',
                                  'expand' => 'convert tabs to spaces',
                                  'expr' => 'evaluate expression',
                                  'factor' => 'factor a number',
                                  'false' => 'Exit unsuccesfully',
                                  'file' => 'determine file type',
                                  'find' => 'search directory tree for files matching a pattern',
                                  'fish' => 'plays the children\'s game of Go Fish',
                                  'fmt' => 'simple text formatter',
                                  'fold' => 'wrap each input line to fit specified width',
                                  'fortune' => 'print a random, hopefully interesting, adage',
                                  'from' => 'print names of those who have sent mail',
                                  'glob' => 'find pathnames matching a pattern',
                                  'grep' => 'search for regular expressions and print',
                                  'hangman' => 'perl version of the game hangman',
                                  'head' => 'print the first lines of a file',
                                  'hexdump' => 'print input as hexadecimal',
                                  'id' => 'show user information',
                                  'install' => 'install files and directories',
                                  'join' => 'relational database operator',
                                  'kill' => 'send signals to a process',
                                  'ln' => 'create links',
                                  'lock' => 'reserves a terminal',
                                  'look' => 'find lines in a sorted list',
                                  'ls' => 'list file/directory information',
                                  'mail' => 'implementation of Berkeley mail(1)',
                                  'maze' => 'generate a maze problem',
                                  'mimedecode' => 'extract MIME attachments in uudecode-like manner',
                                  'mkdir' => 'create directories',
                                  'mkfifo' => 'make named pipes',
                                  'moo' => 'play a game of MOO',
                                  'morse' => 'translate text to morse code',
                                  'nl' => 'line numbering filter',
                                  'od' => 'dump files in octal and other formats',
                                  'par' => 'create a Perl archive of files',
                                  'paste' => 'merge corresponding or subsequent lines of files',
                                  'patch' => 'apply a diff file to an original',
                                  'pig' => 'eformatray inputway asway Igpay Atinlay',
                                  'ping' => 'probe for network hosts',
                                  'pom' => 'display the phase of the moon',
                                  'ppt' => 'reformat input as paper tape',
                                  'pr' => 'convert text files for printing',
                                  'primes' => 'generate primes',
                                  'printenv' => 'Display the environment',
                                  'printf' => 'format and print data',
                                  'pwd' => 'working directory name',
                                  'rain' => 'Let it rain',
                                  'random' => 'display lines at random, or exit with a random value',
                                  'rev' => 'reverse lines of a file',
                                  'rm' => 'remove directory entries',
                                  'rmdir' => 'remove directories',
                                  'robots' => 'fight off villainous robots',
                                  'seq' => 'print a numeric sequence',
                                  'shar' => 'create a shell archive of files',
                                  'sleep' => 'suspend execution for a number of seconds',
                                  'sort' => 'sort or merge text files',
                                  'spell' => 'scan a file for misspelled words',
                                  'split' => 'split a file into pieces',
                                  'strings' => 'extract strings',
                                  'sum' => 'display file checksums and block counts',
                                  'tac' => 'concatenate and print files in reverse',
                                  'tail' => 'display the last part of a file',
                                  'tar' => 'manipulate tape archives',
                                  'tee' => 'pipe fitting',
                                  'test' => 'condition evaluation utility',
                                  'time' => 'times the execution of a command',
                                  'touch' => 'change access and modification times of files',
                                  'tr' => 'translate or delete characters',
                                  'true' => 'Exit succesfully',
                                  'tsort' => 'topological sort',
                                  'tty' => 'return user\'s terminal name',
                                  'uname' => 'print system information',
                                  'unexpand' => 'convert spaces to tabs',
                                  'uniq' => 'report or filter out repeated lines in a file',
                                  'units' => 'conversion program',
                                  'unlink' => 'remove a file',
                                  'unpar' => 'extract files from a Perl archive',
                                  'unshar' => 'extract files from a shell archive',
                                  'uudecode' => 'decode a binary file',
                                  'uuencode' => 'encode a binary file',
                                  'wc' => 'paragraph, line, word, character, and byte counter',
                                  'what' => 'extract version information from a file',
                                  'which' => 'report full paths of commands',
                                  'whoami' => 'display effective user ID',
                                  'whois' => 'Internet domain name and network number directory service',
                                  'words' => 'find words which can be made from a string of letters',
                                  'wump' => 'play a game of "Hunt the Wumpus"',
                                  'xargs' => 'construct argument list(s) and execute utility',
                                  'yes' => 'print out a string till doomsday'
                                },
          'author' => '',
          'dir_lib' => 'lib',
          'dir_t' => 't',
          'dir_xt' => 'xt',
          'dirs' => 33,
          'dirs_array' => [
                            'bin',
                            'lib/PerlPowerTools',
                            'lib',
                            't/bc',
                            't/cat',
                            't/cp',
                            't/cut',
                            't/data/cat',
                            't/data/nl',
                            't/data/od',
                            't/data/rev',
                            't/data/sort',
                            't/data',
                            't/date',
                            't/echo',
                            't/factor',
                            't/false',
                            't/find',
                            't/lib',
                            't/mimedecode',
                            't/nl',
                            't/od',
                            't/rev',
                            't/rm',
                            't/robots',
                            't/rot13',
                            't/seq',
                            't/sort',
                            't/true',
                            't/units',
                            't/uudecode',
                            't',
                            'xt'
                          ],
          'dist' => 'PerlPowerTools',
          'dynamic_config' => 1,
          'error' => {
                       'extracts_nicely' => 'expected PerlPowerTools but got PerlPowerTools-1.044',
                       'use_strict' => 'PerlPowerTools, ppt',
                       'use_warnings' => 'PerlPowerTools, PerlPowerTools::SymbolicMode, ppt'
                     },
          'extension' => 'tar.gz',
          'external_license_file' => 'LICENSE-ARTISTIC2',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_changelog' => 'Changes',
          'file_makefile_pl' => 'Makefile.PL',
          'file_manifest' => 'MANIFEST',
          'file_manifest_skip' => 'MANIFEST.SKIP',
          'file_meta_json' => 'META.json',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README.pod',
          'files' => 142,
          'files_array' => [
                             'CONTRIBUTING.md',
                             'Changes',
                             'INSTALL.SKIP',
                             'LICENSE-ARTISTIC2',
                             'LICENSE-BSD2',
                             'LICENSE-GPL2',
                             'MANIFEST',
                             'MANIFEST.SKIP',
                             'META.json',
                             'META.yml',
                             'Makefile.PL',
                             'README.pod',
                             'bin/addbib',
                             'bin/apply',
                             'bin/ar',
                             'bin/arch',
                             'bin/arithmetic',
                             'bin/asa',
                             'bin/awk',
                             'bin/banner',
                             'bin/base64',
                             'bin/basename',
                             'bin/bc',
                             'bin/bcd',
                             'bin/cal',
                             'bin/cat',
                             'bin/chgrp',
                             'bin/ching',
                             'bin/chmod',
                             'bin/chown',
                             'bin/clear',
                             'bin/cmp',
                             'bin/col',
                             'bin/colrm',
                             'bin/comm',
                             'bin/cp',
                             'bin/cut',
                             'bin/date',
                             'bin/dc',
                             'bin/deroff',
                             'bin/diff',
                             'bin/dirname',
                             'bin/du',
                             'bin/echo',
                             'bin/ed',
                             'bin/env',
                             'bin/expand',
                             'bin/expr',
                             'bin/factor',
                             'bin/false',
                             'bin/file',
                             'bin/find',
                             'bin/fish',
                             'bin/fmt',
                             'bin/fold',
                             'bin/fortune',
                             'bin/from',
                             'bin/glob',
                             'bin/grep',
                             'bin/hangman',
                             'bin/head',
                             'bin/hexdump',
                             'bin/id',
                             'bin/install',
                             'bin/join',
                             'bin/kill',
                             'bin/ln',
                             'bin/lock',
                             'bin/look',
                             'bin/ls',
                             'bin/mail',
                             'bin/maze',
                             'bin/mimedecode',
                             'bin/mkdir',
                             'bin/mkfifo',
                             'bin/moo',
                             'bin/morse',
                             'bin/nl',
                             'bin/od',
                             'bin/par',
                             'bin/paste',
                             'bin/patch',
                             'bin/pig',
                             'bin/ping',
                             'bin/pom',
                             'bin/ppt',
                             'bin/pr',
                             'bin/primes',
                             'bin/printenv',
                             'bin/printf',
                             'bin/pwd',
                             'bin/rain',
                             'bin/random',
                             'bin/rev',
                             'bin/rm',
                             'bin/rmdir',
                             'bin/robots',
                             'bin/rot13',
                             'bin/seq',
                             'bin/shar',
                             'bin/sleep',
                             'bin/sort',
                             'bin/spell',
                             'bin/split',
                             'bin/strings',
                             'bin/sum',
                             'bin/tac',
                             'bin/tail',
                             'bin/tar',
                             'bin/tee',
                             'bin/test',
                             'bin/time',
                             'bin/touch',
                             'bin/tr',
                             'bin/true',
                             'bin/tsort',
                             'bin/tty',
                             'bin/uname',
                             'bin/unexpand',
                             'bin/uniq',
                             'bin/units',
                             'bin/unlink',
                             'bin/unpar',
                             'bin/unshar',
                             'bin/uudecode',
                             'bin/uuencode',
                             'bin/wc',
                             'bin/what',
                             'bin/which',
                             'bin/whoami',
                             'bin/whois',
                             'bin/words',
                             'bin/wump',
                             'bin/xargs',
                             'bin/yes',
                             'lib/PerlPowerTools/SymbolicMode.pm',
                             'lib/PerlPowerTools.pm',
                             'lib/ppt.pm',
                             'xt/changes.t',
                             'xt/perlcritic.t',
                             'xt/perlcriticrc',
                             'xt/pod.t'
                           ],
          'files_hash' => {
                            'CONTRIBUTING.md' => {
                                                   'mtime' => 1709457159,
                                                   'size' => 3872
                                                 },
                            'Changes' => {
                                           'mtime' => 1709457159,
                                           'size' => 22044
                                         },
                            'INSTALL.SKIP' => {
                                                'mtime' => 1709457159,
                                                'size' => 110
                                              },
                            'LICENSE-ARTISTIC2' => {
                                                     'mtime' => 1709457159,
                                                     'size' => 9144
                                                   },
                            'LICENSE-BSD2' => {
                                                'mtime' => 1709457159,
                                                'size' => 1322
                                              },
                            'LICENSE-GPL2' => {
                                                'mtime' => 1709457159,
                                                'size' => 18092
                                              },
                            'MANIFEST' => {
                                            'mtime' => 1709457165,
                                            'size' => 2079
                                          },
                            'MANIFEST.SKIP' => {
                                                 'mtime' => 1709457159,
                                                 'size' => 1579
                                               },
                            'META.json' => {
                                             'mtime' => 1709457165,
                                             'size' => 1588
                                           },
                            'META.yml' => {
                                            'mtime' => 1709457165,
                                            'size' => 879
                                          },
                            'Makefile.PL' => {
                                               'mtime' => 1709457159,
                                               'noes' => {
                                                           'warnings' => '0'
                                                         },
                                               'recommends' => {
                                                                 'File::Spec' => '0'
                                                               },
                                               'requires' => {
                                                               'File::Spec::Functions' => '0',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 5273,
                                               'suggests' => {
                                                               'Test::Manifest' => '1.21'
                                                             }
                                             },
                            'README.pod' => {
                                              'mtime' => 1709457159,
                                              'size' => 4767
                                            },
                            'bin/addbib' => {
                                              'mtime' => 1709457159,
                                              'size' => 6176
                                            },
                            'bin/apply' => {
                                             'mtime' => 1709457159,
                                             'size' => 4134
                                           },
                            'bin/ar' => {
                                          'mtime' => 1709457159,
                                          'size' => 16017
                                        },
                            'bin/arch' => {
                                            'mtime' => 1709457159,
                                            'size' => 1658
                                          },
                            'bin/arithmetic' => {
                                                  'mtime' => 1709457159,
                                                  'size' => 6143
                                                },
                            'bin/asa' => {
                                           'mtime' => 1709457159,
                                           'size' => 2326
                                         },
                            'bin/awk' => {
                                           'mtime' => 1709457159,
                                           'size' => 4595
                                         },
                            'bin/banner' => {
                                              'license' => 'BSD',
                                              'mtime' => 1709457159,
                                              'size' => 73758
                                            },
                            'bin/base64' => {
                                              'license' => 'Artistic_2_0',
                                              'mtime' => 1709457159,
                                              'size' => 3304
                                            },
                            'bin/basename' => {
                                                'mtime' => 1709457159,
                                                'size' => 2094
                                              },
                            'bin/bc' => {
                                          'license' => 'GPL_2',
                                          'mtime' => 1709457159,
                                          'size' => 88378
                                        },
                            'bin/bcd' => {
                                           'mtime' => 1709457159,
                                           'size' => 7425
                                         },
                            'bin/cal' => {
                                           'license' => 'GPL_1,GPL_2,GPL_3',
                                           'mtime' => 1709457159,
                                           'size' => 9375
                                         },
                            'bin/cat' => {
                                           'mtime' => 1709457159,
                                           'size' => 3967
                                         },
                            'bin/chgrp' => {
                                             'mtime' => 1709457159,
                                             'size' => 4873
                                           },
                            'bin/ching' => {
                                             'license' => 'BSD',
                                             'mtime' => 1709457159,
                                             'size' => 76734
                                           },
                            'bin/chmod' => {
                                             'mtime' => 1709457159,
                                             'size' => 18917
                                           },
                            'bin/chown' => {
                                             'mtime' => 1709457159,
                                             'size' => 5112
                                           },
                            'bin/clear' => {
                                             'mtime' => 1709457159,
                                             'size' => 1061
                                           },
                            'bin/cmp' => {
                                           'mtime' => 1709457159,
                                           'size' => 8002
                                         },
                            'bin/col' => {
                                           'mtime' => 1709457159,
                                           'size' => 11598
                                         },
                            'bin/colrm' => {
                                             'mtime' => 1709457159,
                                             'size' => 2139
                                           },
                            'bin/comm' => {
                                            'mtime' => 1709457159,
                                            'size' => 1878
                                          },
                            'bin/cp' => {
                                          'license' => 'Artistic_2_0',
                                          'mtime' => 1709457159,
                                          'size' => 3902
                                        },
                            'bin/cut' => {
                                           'mtime' => 1709457159,
                                           'size' => 6936
                                         },
                            'bin/date' => {
                                            'mtime' => 1709457159,
                                            'size' => 16582
                                          },
                            'bin/dc' => {
                                          'mtime' => 1709457159,
                                          'size' => 10788
                                        },
                            'bin/deroff' => {
                                              'mtime' => 1709457159,
                                              'size' => 4417
                                            },
                            'bin/diff' => {
                                            'mtime' => 1709457159,
                                            'size' => 23131
                                          },
                            'bin/dirname' => {
                                               'mtime' => 1709457159,
                                               'size' => 1750
                                             },
                            'bin/du' => {
                                          'mtime' => 1709457159,
                                          'size' => 5508
                                        },
                            'bin/echo' => {
                                            'mtime' => 1709457159,
                                            'size' => 1365
                                          },
                            'bin/ed' => {
                                          'mtime' => 1709457159,
                                          'size' => 29489
                                        },
                            'bin/env' => {
                                           'mtime' => 1709457159,
                                           'size' => 2418
                                         },
                            'bin/expand' => {
                                              'mtime' => 1709457159,
                                              'size' => 3731
                                            },
                            'bin/expr' => {
                                            'mtime' => 1709457159,
                                            'size' => 5284
                                          },
                            'bin/factor' => {
                                              'mtime' => 1709457159,
                                              'size' => 5009
                                            },
                            'bin/false' => {
                                             'mtime' => 1709457159,
                                             'size' => 1614
                                           },
                            'bin/file' => {
                                            'license' => 'BSD',
                                            'mtime' => 1709457159,
                                            'size' => 25449
                                          },
                            'bin/find' => {
                                            'mtime' => 1709457159,
                                            'size' => 5505
                                          },
                            'bin/fish' => {
                                            'license' => 'Artistic_1_0,Artistic_2_0',
                                            'mtime' => 1709457159,
                                            'size' => 8011
                                          },
                            'bin/fmt' => {
                                           'mtime' => 1709457159,
                                           'size' => 2117
                                         },
                            'bin/fold' => {
                                            'mtime' => 1709457159,
                                            'size' => 6856
                                          },
                            'bin/fortune' => {
                                               'license' => 'GPL_1,GPL_2,GPL_3',
                                               'mtime' => 1709457159,
                                               'size' => 27819
                                             },
                            'bin/from' => {
                                            'mtime' => 1709457159,
                                            'size' => 2789
                                          },
                            'bin/glob' => {
                                            'license' => 'Perl_5',
                                            'mtime' => 1709457159,
                                            'size' => 14827
                                          },
                            'bin/grep' => {
                                            'mtime' => 1709457159,
                                            'size' => 16430
                                          },
                            'bin/hangman' => {
                                               'license' => 'GPL_1,GPL_2,GPL_3',
                                               'mtime' => 1709457159,
                                               'size' => 3529
                                             },
                            'bin/head' => {
                                            'mtime' => 1709457159,
                                            'size' => 3197
                                          },
                            'bin/hexdump' => {
                                               'license' => 'Artistic_2_0',
                                               'mtime' => 1709457159,
                                               'size' => 5490
                                             },
                            'bin/id' => {
                                          'mtime' => 1709457159,
                                          'size' => 5615
                                        },
                            'bin/install' => {
                                               'mtime' => 1709457159,
                                               'size' => 17455
                                             },
                            'bin/join' => {
                                            'mtime' => 1709457159,
                                            'size' => 11505
                                          },
                            'bin/kill' => {
                                            'mtime' => 1709457159,
                                            'size' => 3530
                                          },
                            'bin/ln' => {
                                          'mtime' => 1709457159,
                                          'size' => 3519
                                        },
                            'bin/lock' => {
                                            'mtime' => 1709457159,
                                            'size' => 4484
                                          },
                            'bin/look' => {
                                            'mtime' => 1709457159,
                                            'size' => 3440
                                          },
                            'bin/ls' => {
                                          'mtime' => 1709457159,
                                          'size' => 16150
                                        },
                            'bin/mail' => {
                                            'license' => 'Artistic_1_0,Artistic_2_0',
                                            'mtime' => 1709457159,
                                            'size' => 32082
                                          },
                            'bin/maze' => {
                                            'license' => 'Perl_5',
                                            'mtime' => 1709457159,
                                            'size' => 4452
                                          },
                            'bin/mimedecode' => {
                                                  'mtime' => 1709457159,
                                                  'size' => 1743
                                                },
                            'bin/mkdir' => {
                                             'mtime' => 1709457159,
                                             'size' => 13269
                                           },
                            'bin/mkfifo' => {
                                              'mtime' => 1709457159,
                                              'size' => 2088
                                            },
                            'bin/moo' => {
                                           'mtime' => 1709457159,
                                           'size' => 3610
                                         },
                            'bin/morse' => {
                                             'mtime' => 1709457159,
                                             'size' => 6140
                                           },
                            'bin/nl' => {
                                          'license' => 'Artistic_2_0',
                                          'mtime' => 1709457159,
                                          'size' => 6103
                                        },
                            'bin/od' => {
                                          'mtime' => 1709457159,
                                          'size' => 6156
                                        },
                            'bin/par' => {
                                           'mtime' => 1709457159,
                                           'size' => 8719
                                         },
                            'bin/paste' => {
                                             'mtime' => 1709457159,
                                             'size' => 3275
                                           },
                            'bin/patch' => {
                                             'license' => 'GPL_1,GPL_2,GPL_3',
                                             'mtime' => 1709457159,
                                             'size' => 50292
                                           },
                            'bin/pig' => {
                                           'mtime' => 1709457159,
                                           'size' => 2045
                                         },
                            'bin/ping' => {
                                            'mtime' => 1709457159,
                                            'size' => 1535
                                          },
                            'bin/pom' => {
                                           'license' => 'Perl_5',
                                           'mtime' => 1709457159,
                                           'size' => 11203
                                         },
                            'bin/ppt' => {
                                           'mtime' => 1709457159,
                                           'size' => 455
                                         },
                            'bin/pr' => {
                                          'license' => 'Artistic_1_0,Artistic_2_0',
                                          'mtime' => 1709457159,
                                          'size' => 11520
                                        },
                            'bin/primes' => {
                                              'mtime' => 1709457159,
                                              'size' => 4653
                                            },
                            'bin/printenv' => {
                                                'mtime' => 1709457159,
                                                'size' => 1414
                                              },
                            'bin/printf' => {
                                              'mtime' => 1709457159,
                                              'size' => 1699
                                            },
                            'bin/pwd' => {
                                           'mtime' => 1709457159,
                                           'size' => 1245
                                         },
                            'bin/rain' => {
                                            'mtime' => 1709457159,
                                            'size' => 1047
                                          },
                            'bin/random' => {
                                              'mtime' => 1709457159,
                                              'size' => 2454
                                            },
                            'bin/rev' => {
                                           'license' => 'GPL_1,GPL_2,GPL_3',
                                           'mtime' => 1709457159,
                                           'size' => 1889
                                         },
                            'bin/rm' => {
                                          'license' => 'Artistic_2_0',
                                          'mtime' => 1709457159,
                                          'size' => 6469
                                        },
                            'bin/rmdir' => {
                                             'mtime' => 1709457159,
                                             'size' => 2306
                                           },
                            'bin/robots' => {
                                              'license' => 'Perl_5',
                                              'mtime' => 1709457159,
                                              'size' => 32614
                                            },
                            'bin/rot13' => {
                                             'mtime' => 1709457159,
                                             'size' => 303
                                           },
                            'bin/seq' => {
                                           'license' => 'Artistic_2_0',
                                           'mtime' => 1709457159,
                                           'size' => 3351
                                         },
                            'bin/shar' => {
                                            'mtime' => 1709457159,
                                            'size' => 2213
                                          },
                            'bin/sleep' => {
                                             'mtime' => 1709457159,
                                             'size' => 1592
                                           },
                            'bin/sort' => {
                                            'mtime' => 1709457159,
                                            'size' => 29384
                                          },
                            'bin/spell' => {
                                             'mtime' => 1709457159,
                                             'size' => 11127
                                           },
                            'bin/split' => {
                                             'mtime' => 1709457159,
                                             'size' => 5554
                                           },
                            'bin/strings' => {
                                               'mtime' => 1709457159,
                                               'size' => 3134
                                             },
                            'bin/sum' => {
                                           'mtime' => 1709457159,
                                           'size' => 8361
                                         },
                            'bin/tac' => {
                                           'mtime' => 1709457159,
                                           'size' => 11863
                                         },
                            'bin/tail' => {
                                            'mtime' => 1709457159,
                                            'size' => 16743
                                          },
                            'bin/tar' => {
                                           'mtime' => 1709457159,
                                           'size' => 4711
                                         },
                            'bin/tee' => {
                                           'mtime' => 1709457159,
                                           'size' => 1524
                                         },
                            'bin/test' => {
                                            'mtime' => 1709457159,
                                            'size' => 13765
                                          },
                            'bin/time' => {
                                            'mtime' => 1709457159,
                                            'size' => 1523
                                          },
                            'bin/touch' => {
                                             'mtime' => 1709457159,
                                             'size' => 5096
                                           },
                            'bin/tr' => {
                                          'mtime' => 1709457159,
                                          'size' => 4016
                                        },
                            'bin/true' => {
                                            'mtime' => 1709457159,
                                            'size' => 1608
                                          },
                            'bin/tsort' => {
                                             'mtime' => 1709457159,
                                             'size' => 2680
                                           },
                            'bin/tty' => {
                                           'mtime' => 1709457159,
                                           'size' => 305
                                         },
                            'bin/uname' => {
                                             'mtime' => 1709457159,
                                             'size' => 1481
                                           },
                            'bin/unexpand' => {
                                                'mtime' => 1709457159,
                                                'size' => 4004
                                              },
                            'bin/uniq' => {
                                            'mtime' => 1709457159,
                                            'size' => 4580
                                          },
                            'bin/units' => {
                                             'license' => 'GPL_1,GPL_2,GPL_3',
                                             'mtime' => 1709457159,
                                             'size' => 53228
                                           },
                            'bin/unlink' => {
                                              'license' => 'Artistic_2_0',
                                              'mtime' => 1709457159,
                                              'size' => 1423
                                            },
                            'bin/unpar' => {
                                             'mtime' => 1709457159,
                                             'size' => 1917
                                           },
                            'bin/unshar' => {
                                              'license' => 'Perl_5',
                                              'mtime' => 1709457159,
                                              'size' => 6968
                                            },
                            'bin/uudecode' => {
                                                'mtime' => 1709457159,
                                                'size' => 2883
                                              },
                            'bin/uuencode' => {
                                                'mtime' => 1709457159,
                                                'size' => 1608
                                              },
                            'bin/wc' => {
                                          'mtime' => 1709457159,
                                          'size' => 9734
                                        },
                            'bin/what' => {
                                            'mtime' => 1709457159,
                                            'size' => 3527
                                          },
                            'bin/which' => {
                                             'mtime' => 1709457159,
                                             'size' => 4574
                                           },
                            'bin/whoami' => {
                                              'license' => 'Artistic_2_0',
                                              'mtime' => 1709457159,
                                              'size' => 1042
                                            },
                            'bin/whois' => {
                                             'mtime' => 1709457159,
                                             'size' => 1464
                                           },
                            'bin/words' => {
                                             'mtime' => 1709457159,
                                             'size' => 3896
                                           },
                            'bin/wump' => {
                                            'mtime' => 1709457159,
                                            'size' => 13733
                                          },
                            'bin/xargs' => {
                                             'mtime' => 1709457159,
                                             'size' => 2481
                                           },
                            'bin/yes' => {
                                           'mtime' => 1709457159,
                                           'size' => 1032
                                         },
                            'lib/PerlPowerTools.pm' => {
                                                         'license' => 'Perl_5',
                                                         'module' => 'PerlPowerTools',
                                                         'mtime' => 1709457159,
                                                         'size' => 6492
                                                       },
                            'lib/PerlPowerTools/SymbolicMode.pm' => {
                                                                      'module' => 'PerlPowerTools::SymbolicMode',
                                                                      'mtime' => 1709457159,
                                                                      'requires' => {
                                                                                      'strict' => '0'
                                                                                    },
                                                                      'size' => 8318
                                                                    },
                            'lib/ppt.pm' => {
                                              'module' => 'ppt',
                                              'mtime' => 1709457159,
                                              'requires' => {
                                                              'PerlPowerTools' => '0'
                                                            },
                                              'size' => 436
                                            },
                            't/000.basic.t' => {
                                                 'mtime' => 1709457159,
                                                 'no_index' => 1,
                                                 'requires' => {
                                                                 'Test::More' => '0.88',
                                                                 'strict' => '0',
                                                                 'warnings' => '0'
                                                               },
                                                 'size' => 135
                                               },
                            't/910.meta.t' => {
                                                'mtime' => 1709457159,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'Test::More' => '0.88',
                                                                'strict' => '0',
                                                                'warnings' => '0'
                                                              },
                                                'size' => 473
                                              },
                            't/990.pod.t' => {
                                               'mtime' => 1709457159,
                                               'no_index' => 1,
                                               'requires' => {
                                                               'Test::More' => '0.88',
                                                               'Test::Pod' => '0'
                                                             },
                                               'size' => 252
                                             },
                            't/bc/input.t' => {
                                                'mtime' => 1709457159,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'File::Temp' => '0',
                                                                'Test::More' => '0.88',
                                                                'strict' => '0',
                                                                'warnings' => '0'
                                                              },
                                                'size' => 8158
                                              },
                            't/cat/cat.t' => {
                                               'mtime' => 1709457159,
                                               'no_index' => 1,
                                               'requires' => {
                                                               'Test::More' => '0.88'
                                                             },
                                               'size' => 405
                                             },
                            't/cp/gh-115-copy-into-dir.t' => {
                                                               'mtime' => 1709457159,
                                                               'no_index' => 1,
                                                               'requires' => {
                                                                               'Cwd' => '0',
                                                                               'File::Path' => '0',
                                                                               'File::Spec::Functions' => '0',
                                                                               'File::Temp' => '0',
                                                                               'Test::More' => '0.88',
                                                                               'strict' => '0',
                                                                               'warnings' => '0'
                                                                             },
                                                               'size' => 2073
                                                             },
                            't/cut/cut.t' => {
                                               'mtime' => 1709457159,
                                               'no_index' => 1,
                                               'requires' => {
                                                               'File::Spec::Functions' => '0',
                                                               'IPC::Run3' => '0',
                                                               'Test::More' => '0.88'
                                                             },
                                               'size' => 1141
                                             },
                            't/data/cat/cat-n-1.txt' => {
                                                          'mtime' => 1709457159,
                                                          'no_index' => 1,
                                                          'size' => 23
                                                        },
                            't/data/nl/nl.txt' => {
                                                    'mtime' => 1709457159,
                                                    'no_index' => 1,
                                                    'size' => 60
                                                  },
                            't/data/od/ascii.txt' => {
                                                       'mtime' => 1709457159,
                                                       'no_index' => 1,
                                                       'size' => 256
                                                     },
                            't/data/rev/reverse-this.txt' => {
                                                               'mtime' => 1709457159,
                                                               'no_index' => 1,
                                                               'size' => 19
                                                             },
                            't/data/sort/ints1.txt' => {
                                                         'mtime' => 1709457159,
                                                         'no_index' => 1,
                                                         'size' => 292
                                                       },
                            't/data/sort/letters1.txt' => {
                                                            'mtime' => 1709457159,
                                                            'no_index' => 1,
                                                            'size' => 12
                                                          },
                            't/data/sort/three-words.txt' => {
                                                               'mtime' => 1709457159,
                                                               'no_index' => 1,
                                                               'size' => 143
                                                             },
                            't/date/date.t' => {
                                                 'mtime' => 1709457159,
                                                 'no_index' => 1,
                                                 'requires' => {
                                                                 'Test::More' => '1',
                                                                 'strict' => '0',
                                                                 'warnings' => '0'
                                                               },
                                                 'size' => 164
                                               },
                            't/echo/echo.t' => {
                                                 'mtime' => 1709457159,
                                                 'no_index' => 1,
                                                 'requires' => {
                                                                 'Test::More' => '1',
                                                                 'strict' => '0',
                                                                 'warnings' => '0'
                                                               },
                                                 'size' => 1337
                                               },
                            't/factor/factor.t' => {
                                                     'mtime' => 1709457159,
                                                     'no_index' => 1,
                                                     'requires' => {
                                                                     'Test::More' => '0.95',
                                                                     'lib' => '0',
                                                                     'perl' => '5.006',
                                                                     'strict' => '0'
                                                                   },
                                                     'size' => 8846
                                                   },
                            't/false/false.t' => {
                                                   'mtime' => 1709457159,
                                                   'no_index' => 1,
                                                   'requires' => {
                                                                   'File::Spec' => '0',
                                                                   'Test::More' => '0.95'
                                                                 },
                                                   'size' => 406
                                                 },
                            't/find/find.t' => {
                                                 'mtime' => 1709457159,
                                                 'no_index' => 1,
                                                 'requires' => {
                                                                 'Config' => '0',
                                                                 'Data::Dumper' => '0',
                                                                 'File::Basename' => '0',
                                                                 'File::Path' => '0',
                                                                 'File::Spec::Functions' => '0',
                                                                 'File::Temp' => '0',
                                                                 'FindBin' => '0',
                                                                 'Test::More' => '0.88',
                                                                 'strict' => '0',
                                                                 'warnings' => '0'
                                                               },
                                                 'size' => 3733
                                               },
                            't/lib/common.pl' => {
                                                   'mtime' => 1709457159,
                                                   'no_index' => 1,
                                                   'size' => 3457
                                                 },
                            't/lib/utils.pm' => {
                                                  'mtime' => 1709457159,
                                                  'no_index' => 1,
                                                  'noes' => {
                                                              'warnings' => '0'
                                                            },
                                                  'requires' => {
                                                                  'Test::More' => '1',
                                                                  'strict' => '0',
                                                                  'warnings' => '0'
                                                                },
                                                  'size' => 680
                                                },
                            't/mimedecode/000.basic.t' => {
                                                            'mtime' => 1709457159,
                                                            'no_index' => 1,
                                                            'requires' => {
                                                                            'Test::More' => '0.88',
                                                                            'strict' => '0',
                                                                            'warnings' => '0'
                                                                          },
                                                            'size' => 291
                                                          },
                            't/nl/nl.t' => {
                                             'mtime' => 1709457159,
                                             'no_index' => 1,
                                             'requires' => {
                                                             'Test::More' => '0',
                                                             'strict' => '0',
                                                             'warnings' => '0'
                                                           },
                                             'size' => 310
                                           },
                            't/od/od.t' => {
                                             'mtime' => 1709457159,
                                             'no_index' => 1,
                                             'requires' => {
                                                             'File::Spec::Functions' => '0',
                                                             'Test::More' => '0.88'
                                                           },
                                             'size' => 4519
                                           },
                            't/rev/rev.t' => {
                                               'mtime' => 1709457159,
                                               'no_index' => 1,
                                               'requires' => {
                                                               'File::Basename' => '0',
                                                               'File::Spec::Functions' => '0',
                                                               'IPC::Run3' => '0',
                                                               'Test::More' => '0.88',
                                                               'lib' => '0',
                                                               'strict' => '0',
                                                               'utils' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 1507
                                             },
                            't/rm/process_options.t' => {
                                                          'mtime' => 1709457159,
                                                          'no_index' => 1,
                                                          'requires' => {
                                                                          'Test::More' => '0.88',
                                                                          'strict' => '0',
                                                                          'warnings' => '0'
                                                                        },
                                                          'size' => 2844
                                                        },
                            't/rm/run.t' => {
                                              'mtime' => 1709457159,
                                              'no_index' => 1,
                                              'noes' => {
                                                          'warnings' => '0'
                                                        },
                                              'requires' => {
                                                              'File::Temp' => '0',
                                                              'Test::More' => '0.88',
                                                              'constant' => '0',
                                                              'strict' => '0',
                                                              'warnings' => '0'
                                                            },
                                              'size' => 4306
                                            },
                            't/robots/000.basic.t' => {
                                                        'mtime' => 1709457159,
                                                        'no_index' => 1,
                                                        'requires' => {
                                                                        'Test::More' => '0.88',
                                                                        'strict' => '0',
                                                                        'warnings' => '0'
                                                                      },
                                                        'size' => 280,
                                                        'suggests' => {
                                                                        'Curses' => '0'
                                                                      }
                                                      },
                            't/rot13/rot13.t' => {
                                                   'mtime' => 1709457159,
                                                   'no_index' => 1,
                                                   'requires' => {
                                                                   'File::Spec' => '0',
                                                                   'Test::More' => '0.88',
                                                                   'strict' => '0',
                                                                   'warnings' => '0'
                                                                 },
                                                   'size' => 983
                                                 },
                            't/seq/seq.t' => {
                                               'mtime' => 1709457159,
                                               'no_index' => 1,
                                               'requires' => {
                                                               'File::Spec' => '0',
                                                               'Test::More' => '0.88',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 833
                                             },
                            't/sort/sort.t' => {
                                                 'mtime' => 1709457159,
                                                 'no_index' => 1,
                                                 'requires' => {
                                                                 'IPC::Run3' => '0',
                                                                 'Test::More' => '0.88',
                                                                 'strict' => '0',
                                                                 'warnings' => '0'
                                                               },
                                                 'size' => 2266
                                               },
                            't/true/true.t' => {
                                                 'mtime' => 1709457159,
                                                 'no_index' => 1,
                                                 'requires' => {
                                                                 'File::Spec' => '0',
                                                                 'Test::More' => '0.95'
                                                               },
                                                 'size' => 404
                                               },
                            't/units/units.t' => {
                                                   'mtime' => 1709457159,
                                                   'no_index' => 1,
                                                   'requires' => {
                                                                   'Test::More' => '0.88',
                                                                   'strict' => '0',
                                                                   'warnings' => '0'
                                                                 },
                                                   'size' => 2054
                                                 },
                            't/uudecode/uudecode.t' => {
                                                         'mtime' => 1709457159,
                                                         'no_index' => 1,
                                                         'requires' => {
                                                                         'Test::More' => '1',
                                                                         'strict' => '0'
                                                                       },
                                                         'size' => 3450
                                                       },
                            'xt/changes.t' => {
                                                'mtime' => 1709457159,
                                                'size' => 131
                                              },
                            'xt/perlcritic.t' => {
                                                   'mtime' => 1709457159,
                                                   'size' => 185
                                                 },
                            'xt/perlcriticrc' => {
                                                   'mtime' => 1709457159,
                                                   'size' => 975
                                                 },
                            'xt/pod.t' => {
                                            'mtime' => 1709457159,
                                            'size' => 159
                                          }
                          },
          'got_prereq_from' => 'META.yml',
          'ignored_files_array' => [
                                     't/000.basic.t',
                                     't/910.meta.t',
                                     't/990.pod.t',
                                     't/bc/input.t',
                                     't/cat/cat.t',
                                     't/cp/gh-115-copy-into-dir.t',
                                     't/cut/cut.t',
                                     't/data/cat/cat-n-1.txt',
                                     't/data/nl/nl.txt',
                                     't/data/od/ascii.txt',
                                     't/data/rev/reverse-this.txt',
                                     't/data/sort/ints1.txt',
                                     't/data/sort/letters1.txt',
                                     't/data/sort/three-words.txt',
                                     't/date/date.t',
                                     't/echo/echo.t',
                                     't/factor/factor.t',
                                     't/false/false.t',
                                     't/find/find.t',
                                     't/lib/common.pl',
                                     't/lib/utils.pm',
                                     't/mimedecode/000.basic.t',
                                     't/nl/nl.t',
                                     't/od/od.t',
                                     't/rev/rev.t',
                                     't/rm/process_options.t',
                                     't/rm/run.t',
                                     't/robots/000.basic.t',
                                     't/rot13/rot13.t',
                                     't/seq/seq.t',
                                     't/sort/sort.t',
                                     't/true/true.t',
                                     't/units/units.t',
                                     't/uudecode/uudecode.t'
                                   ],
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 1,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 1,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 30,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 0,
                          'meta_yml_has_repository_resource' => 1,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 0,
                          'use_warnings' => 0
                        },
          'latest_mtime' => 1709457165,
          'license' => 'perl defined in META.yml defined in LICENSE-ARTISTIC2',
          'license_from_yaml' => 'perl',
          'license_in_pod' => 1,
          'licenses' => {
                          'Artistic_1_0' => [
                                              'bin/fish',
                                              'bin/mail',
                                              'bin/pr'
                                            ],
                          'Artistic_2_0' => [
                                              'bin/base64',
                                              'bin/cp',
                                              'bin/fish',
                                              'bin/hexdump',
                                              'bin/mail',
                                              'bin/nl',
                                              'bin/pr',
                                              'bin/rm',
                                              'bin/seq',
                                              'bin/unlink',
                                              'bin/whoami'
                                            ],
                          'BSD' => [
                                     'bin/banner',
                                     'bin/ching',
                                     'bin/file'
                                   ],
                          'GPL_1' => [
                                       'bin/cal',
                                       'bin/fortune',
                                       'bin/hangman',
                                       'bin/patch',
                                       'bin/rev',
                                       'bin/units'
                                     ],
                          'GPL_2' => [
                                       'bin/bc',
                                       'bin/cal',
                                       'bin/fortune',
                                       'bin/hangman',
                                       'bin/patch',
                                       'bin/rev',
                                       'bin/units'
                                     ],
                          'GPL_3' => [
                                       'bin/cal',
                                       'bin/fortune',
                                       'bin/hangman',
                                       'bin/patch',
                                       'bin/rev',
                                       'bin/units'
                                     ],
                          'Perl_5' => [
                                        'bin/glob',
                                        'bin/maze',
                                        'bin/pom',
                                        'bin/robots',
                                        'bin/unshar',
                                        'lib/PerlPowerTools.pm'
                                      ]
                        },
          'manifest_matches_dist' => 1,
          'meta_json' => {
                           'abstract' => 'BSD utilities written in pure Perl',
                           'author' => [
                                         'brian d foy <briandfoy@pobox.com>'
                                       ],
                           'dynamic_config' => 1,
                           'generated_by' => 'ExtUtils::MakeMaker version 7.70, CPAN::Meta::Converter version 2.150010',
                           'license' => [
                                          'perl_5'
                                        ],
                           'meta-spec' => {
                                            'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                            'version' => 2
                                          },
                           'name' => 'PerlPowerTools',
                           'no_index' => {
                                           'directory' => [
                                                            't',
                                                            'inc'
                                                          ]
                                         },
                           'prereqs' => {
                                          'build' => {
                                                       'requires' => {}
                                                     },
                                          'configure' => {
                                                           'requires' => {
                                                                           'ExtUtils::MakeMaker' => '6.64',
                                                                           'File::Spec::Functions' => '0'
                                                                         }
                                                         },
                                          'runtime' => {
                                                         'requires' => {
                                                                         'App::a2p' => '0',
                                                                         'App::find2perl' => '0',
                                                                         'MIME::Parser' => '0',
                                                                         'perl' => '5.008'
                                                                       }
                                                       },
                                          'test' => {
                                                      'requires' => {
                                                                      'IPC::Run3' => '0',
                                                                      'Test::More' => '1',
                                                                      'Test::Pod' => '0',
                                                                      'Test::Warnings' => '0'
                                                                    }
                                                    }
                                        },
                           'release_status' => 'stable',
                           'resources' => {
                                            'bugtracker' => {
                                                              'web' => 'https://github.com/briandfoy/PerlPowerTools/issues'
                                                            },
                                            'homepage' => 'https://www.perlpowertools.com/',
                                            'repository' => {
                                                              'type' => 'git',
                                                              'url' => 'https://github.com/briandfoy/PerlPowerTools',
                                                              'web' => 'https://github.com/briandfoy/PerlPowerTools'
                                                            }
                                          },
                           'version' => '1.044',
                           'x_serialization_backend' => 'JSON::PP version 4.16'
                         },
          'meta_json_is_parsable' => 1,
          'meta_json_spec_version' => 2,
          'meta_yml' => {
                          'abstract' => 'BSD utilities written in pure Perl',
                          'author' => [
                                        'brian d foy <briandfoy@pobox.com>'
                                      ],
                          'build_requires' => {
                                                'IPC::Run3' => '0',
                                                'Test::More' => '1',
                                                'Test::Pod' => '0',
                                                'Test::Warnings' => '0'
                                              },
                          'configure_requires' => {
                                                    'ExtUtils::MakeMaker' => '6.64',
                                                    'File::Spec::Functions' => '0'
                                                  },
                          'dynamic_config' => '1',
                          'generated_by' => 'ExtUtils::MakeMaker version 7.70, CPAN::Meta::Converter version 2.150010',
                          'license' => 'perl',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'PerlPowerTools',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc'
                                                         ]
                                        },
                          'requires' => {
                                          'App::a2p' => '0',
                                          'App::find2perl' => '0',
                                          'MIME::Parser' => '0',
                                          'perl' => '5.008'
                                        },
                          'resources' => {
                                           'bugtracker' => 'https://github.com/briandfoy/PerlPowerTools/issues',
                                           'homepage' => 'https://www.perlpowertools.com/',
                                           'repository' => 'https://github.com/briandfoy/PerlPowerTools'
                                         },
                          'version' => '1.044',
                          'x_serialization_backend' => 'CPAN::Meta::YAML version 0.018'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/PerlPowerTools.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'PerlPowerTools'
                         },
                         {
                           'file' => 'lib/PerlPowerTools/SymbolicMode.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'PerlPowerTools::SymbolicMode'
                         },
                         {
                           'file' => 'lib/ppt.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'ppt'
                         }
                       ],
          'no_index' => '^inc/;^t/',
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/PerlPowerTools.tar.gz',
          'prereq' => [
                        {
                          'is_prereq' => 1,
                          'requires' => 'App::a2p',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'App::find2perl',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'configure_requires',
                          'version' => '6.64'
                        },
                        {
                          'requires' => 'File::Spec::Functions',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'IPC::Run3',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'MIME::Parser',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'build_requires',
                          'version' => '1'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::Pod',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::Warnings',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => '5.008'
                        }
                      ],
          'released' => 1709583466,
          'size_packed' => 400948,
          'size_unpacked' => 1281767,
          'test_files' => [
                            't/000.basic.t',
                            't/910.meta.t',
                            't/990.pod.t',
                            't/bc/input.t',
                            't/cat/cat.t',
                            't/cp/gh-115-copy-into-dir.t',
                            't/cut/cut.t',
                            't/date/date.t',
                            't/echo/echo.t',
                            't/factor/factor.t',
                            't/false/false.t',
                            't/find/find.t',
                            't/mimedecode/000.basic.t',
                            't/nl/nl.t',
                            't/od/od.t',
                            't/rev/rev.t',
                            't/rm/process_options.t',
                            't/rm/run.t',
                            't/robots/000.basic.t',
                            't/rot13/rot13.t',
                            't/seq/seq.t',
                            't/sort/sort.t',
                            't/true/true.t',
                            't/units/units.t',
                            't/uudecode/uudecode.t'
                          ],
          'unknown_license_texts' => {
                                       'README.pod' => '=head2 Copyright and License

You should have received a I<LICENSE> file, but the license is also noted
in the module files. About the only thing you can\'t do is pretend that
you wrote code that you didn\'t.

',
                                       'bin/addbib' => '=head1 COPYRIGHT and LICENSE

This program is copyright (c) Jeffrey S. Haemer (2004).

This program is free and open software. You may use, modify, distribute,
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others from doing the same.
',
                                       'bin/apply' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Abigail 1999.

This program is free and open software. You may use, copy, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

',
                                       'bin/ar' => '=head1 COPYRIGHT and LICENSE

This program is copyright by dkulp 1999.

This program is free and open software. You may use, copy, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

',
                                       'bin/arithmetic' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Abigail 1999.

This program is free and open software. You may use, copy, modify, distribute,
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others from doing the same.

',
                                       'bin/awk' => '=head1 COPYRIGHT and LICENSE

This program is copyright (c) Tom Christiansen 1999.

This program is free and open software. You may use, modify, distribute,
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others from doing the same.
',
                                       'bin/basename' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Abigail 1999.

This program is free and open software. You may use, copy, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

',
                                       'bin/bcd' => '=head1 COPYRIGHT

 Copyright (c) 1989, 1993
	The Regents of the University of California.  All rights reserved.

 This code is derived from software contributed to Berkeley by
 Steve Hayman of the Indiana University Computer Science Dept.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS\'\' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
',
                                       'bin/cat' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Abigail 1999.

This program is free and open software. You may use, copy, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

',
                                       'bin/chgrp' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Abigail 1999.

This program is free and open software. You may use, copy, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

',
                                       'bin/chmod' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Abigail 1999.

This program is free and open software. You may use, copy, modify, distribute,
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others from doing the same.

',
                                       'bin/chown' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Abigail 1999.

This program is free and open software. You may use, copy, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

',
                                       'bin/cmp' => '=head1 COPYRIGHT and LICENSE

This program is copyright (c) D Roland Walker 1999.

This program is free and open software. You may use, modify, distribute,
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others from doing the same.

',
                                       'bin/col' => '=head1 COPYRIGHT and LICENSE

This program is copyright 2000 by Ronald J Kimball.

This program is free and open software.  You may use, modify, or
distribute this program (and any modified variants) in any way you
wish, provided you do not restrict others from doing the same.

',
                                       'bin/cut' => '=head1 COPYRIGHT and LICENSE

This program is free and open software. You may use, copy, modify,
distribute and sell this program (and any modified variants) in any
way you wish, provided you do not restrict others to do the same.

',
                                       'bin/dirname' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Abigail 1999.

This program is free and open software. You may use, copy, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

',
                                       'bin/du' => '=head1 COPYRIGHT and LICENSE

This program is Copyright (c) by Greg Hewgill 1999.

This program is free and open software. You may use, copy, modify,
distribute, and sell this program (and any modified variants) in any
way you wish, provided you do not restrict others from doing the same.
',
                                       'bin/echo' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Randy Yarger 1999.

This program is free and open software. You may use, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

',
                                       'bin/env' => '=head1 COPYRIGHT and LICENSE

This program is copyright (c) Matthew Bafford 1999.

This program is free and open software.  You may use, modify, distribute,
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others from doing the same.


',
                                       'bin/expand' => '=head1 COPYRIGHT and LICENSE

This program is free and open software. You may use, modify, distribute,
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others from doing the same.

',
                                       'bin/factor' => '=head1 COPYRIGHT and LICENSE

This program is copyright (c) Jonathan Feinberg and Benjamin Tilly (1999).

This program is free and open software. You may use, modify, distribute,
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others from doing the same.
',
                                       'bin/false' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Abigail 1999.

This program is free and open software. You may use, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

',
                                       'bin/find' => '=head1 COPYRIGHT and LICENSE

This program is copyright (c) Gregory L. Snow 1999
(with parts "borrowed" from things copyright (c) Tom Christiansen 1999).

This program is free and open software. You may use, modify, distribute,
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others from doing the same.


',
                                       'bin/grep' => '=head1 COPYRIGHT and LICENSE

Copyright (c) 1993-1999. Tom Christiansen.

This program is free and open software. You may use, copy, modify, distribute,
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others from doing the same.

',
                                       'bin/head' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Abigail 1999.

This program is free and open software. You may use, copy, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

',
                                       'bin/install' => '=head1 COPYRIGHT and LICENSE

Copyright 1999 UAH Information Technology and Systems Center.

This program is free and open software. You may use, copy, modify,
distribute, and sell this program (and any modified variants) in any way
you wish, provided you do not restrict others from doing the same.

',
                                       'bin/join' => '=head1 COPYRIGHT and LICENSE

This program is copyright (c) Jonathan Feinberg 1999.

This program is free and open software. You may use, modify, distribute,
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others from doing the same.


',
                                       'bin/ln' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Abigail 1999.

This program is free and open software. You may use, copy, modify, distribute,
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others from doing the same.

',
                                       'bin/lock' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Aron Atkins 1999.

This program is free and open software. You may use, copy, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

',
                                       'bin/look' => '=head1 COPYRIGHT and LICENSE

This program is copyright (c) Tom Christiansen 1999.

This program is free and open software. You may use, modify, distribute,
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others from doing the same.
',
                                       'bin/ls' => '=head1 COPYRIGHT and LICENSE

This program is free and open software. You may use, modify,
distribute, and sell this program (and any modified variants) in any
way you wish, provided you do not restrict others from doing the same.

',
                                       'bin/mkdir' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Abigail 1999.

This program is free and open software. You may use, copy, modify, distribute,
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others from doing the same.

',
                                       'bin/moo' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Abigail 1999.

This program is free and open software. You may use, copy, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

',
                                       'bin/morse' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Abigail 1999.

This program is free and open software. You may use, copy, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

',
                                       'bin/od' => '=head1 COPYRIGHT and LICENSE

This program is copyright (c) Mark Kahn 1999.

This program is free and open software. You may use, modify, distribute,
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others from doing the same.
',
                                       'bin/par' => '=head1 COPYRIGHT and LICENSE

This program is copyright (c) Tim Gim Yee 1999.

This program is free and open software. You may use, modify, distribute,
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others from doing the same.

',
                                       'bin/paste' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Randy Yarger 1999.

This program is free and open software. You may use, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

',
                                       'bin/pig' => '=head1 COPYRIGHT and LICENSE

Isthay ogrampray isway opyrightcay (c) Onathanjay Einbergfay 1999.

Isthay ogrampray isway eefray andway openway oftwaresay. Ouyay aymay
useway, odifymay, istributeday, andway ellsay isthay ogrampray (andway
anyway odifiedmay ariantsvay) inway anyway ayway ouyay ishway,
ovidedpray ouyay oday otnay estrictray othersway omfray oingday ethay
amesay.

',
                                       'bin/primes' => '=head1 COPYRIGHT and LICENSE

This program is copyright (c) Jonathan Feinberg and Benjamin Tilly (1999).

This program is free and open software. You may use, modify, distribute,
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others from doing the same.

',
                                       'bin/printenv' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Randy Yarger 1999.

This program is free and open software. You may use, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

',
                                       'bin/printf' => '=head1 COPYRIGHT and LICENSE

This program is copyright (c) Tom Christiansen 1999.

This program is free and open software. You may use, modify, distribute,
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others from doing the same.
',
                                       'bin/pwd' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Kevin Meltzer 1999.

This program is free and open software. You may use, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

',
                                       'bin/rain' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Abigail 1999.

This program is free and open software. You may use, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

',
                                       'bin/random' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Abigail 1999.

This program is free and open software. You may use, copy, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

',
                                       'bin/rmdir' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Abigail 1999.

This program is free and open software. You may use, copy, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

',
                                       'bin/shar' => '=head1 COPYRIGHT

This code is released to the public domain.

',
                                       'bin/sleep' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Randy Yarger 1999.

This program is free and open software. You may use, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

',
                                       'bin/spell' => '=head1 COPYRIGHT and LICENSE

This program is copyright (c) Gregory L. Snow 1999.

This program is free and open software.  You may use, modify, or
distribute this program (with or without modifications) to your hearts
content.  However I take no responsibility for your use or misuse of this program.


',
                                       'bin/split' => '=head1 COPYRIGHT and LICENSE

This program is free and open software. You may use, copy, modify,
distribute and sell this program (and any modified variants) in any
way you wish, provided you do not restrict others to do the same.

',
                                       'bin/tac' => '=head1 COPYRIGHT and LICENSE

This program is copyright (c) Tim Gim Yee 1999.

This program is free and open software. You may use, modify, distribute,
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others from doing the same.

',
                                       'bin/tail' => '=head1 COPYRIGHT and LICENSE

This program is free and open software. You may use, modify, distribute,
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others from doing the same.

',
                                       'bin/time' => '=head1 COPYRIGHT and LICENSE

This program is copyright by dkulp 1999.

This program is free and open software. You may use, copy, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

',
                                       'bin/touch' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Abigail 1999.

This program is free and open software. You may use, copy, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

',
                                       'bin/tr' => '=head1 COPYRIGHT and LICENSE

This program is copyright (c) Tom Christiansen 1999.

This program is free and open software. You may use, modify, distribute,
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others from doing the same.
',
                                       'bin/true' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Abigail 1999.

This program is free and open software. You may use, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

',
                                       'bin/unexpand' => '=head1 COPYRIGHT and LICENSE

This program is free and open software. You may use, modify, distribute,
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others from doing the same.

',
                                       'bin/uniq' => '=head1 COPYRIGHT and LICENSE

This program is copyright (c) Jonathan Feinberg 1999.

This program is free and open software. You may use, modify, distribute,
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others from doing the same.
',
                                       'bin/unpar' => '=head1 COPYRIGHT and LICENSE

This program is copyright (c) Tim Gim Yee 1999.

This program is free and open software. You may use, modify, distribute,
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others from doing the same.

',
                                       'bin/wc' => '=head1 COPYRIGHT and LICENSE

This program is copyright (c) by Peter Prymmer 1999.

This program is free and open software.  You may use, copy, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

',
                                       'bin/what' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Ken Schumack 1999.

This program is free and open software. You may use, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others from doing the same.

',
                                       'bin/which' => '=head1 COPYRIGHT and LICENSE

This program is copyright by Abigail 1999.

This program is free and open software. You may use, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

',
                                       'bin/words' => '=head1 COPYRIGHT and LICENSE

This program is copyright 1999 by Ronald J Kimball.

This program is free and open software.  You may use, modify, or
distribute this program (and any modified variants) in any way you
wish, provided you do not restrict others from doing the same.

',
                                       'bin/wump' => '=head1 COPYRIGHT and LICENSE

This program is copyright (C) Amir Karger 1999. (Although I can\'t imagine why)

This program is free and open software. You may use, copy, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

',
                                       'bin/yes' => '=head1 COPYRIGHT and LICENSE

This program is free and open software. You may use, modify, distribute
and sell this program (and any modified variants) in any way you wish,
provided you do not restrict others to do the same.

'
                                     },
          'uses' => {
                      'configure' => {
                                       'noes' => {
                                                   'warnings' => '0'
                                                 },
                                       'recommends' => {
                                                         'File::Spec' => '0'
                                                       },
                                       'requires' => {
                                                       'File::Spec::Functions' => '0',
                                                       'strict' => '0',
                                                       'warnings' => '0'
                                                     },
                                       'suggests' => {
                                                       'Test::Manifest' => '1.21'
                                                     }
                                     },
                      'runtime' => {
                                     'requires' => {
                                                     'strict' => '0'
                                                   }
                                   },
                      'test' => {
                                  'noes' => {
                                              'warnings' => '0'
                                            },
                                  'requires' => {
                                                  'Config' => '0',
                                                  'Cwd' => '0',
                                                  'Data::Dumper' => '0',
                                                  'File::Basename' => '0',
                                                  'File::Path' => '0',
                                                  'File::Spec' => '0',
                                                  'File::Spec::Functions' => '0',
                                                  'File::Temp' => '0',
                                                  'FindBin' => '0',
                                                  'IPC::Run3' => '0',
                                                  'Test::More' => '1',
                                                  'Test::Pod' => '0',
                                                  'constant' => '0',
                                                  'lib' => '0',
                                                  'perl' => '5.006',
                                                  'strict' => '0',
                                                  'warnings' => '0'
                                                },
                                  'suggests' => {
                                                  'Curses' => '0'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'PerlPowerTools'
        };
1;
