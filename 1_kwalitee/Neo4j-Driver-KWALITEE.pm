$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'Neo4j::Driver' => 'Neo4j community graph database driver for Bolt and HTTP',
                                  'Neo4j::Driver::Config' => 'Driver configuration options',
                                  'Neo4j::Driver::Deprecations' => 'Explains features that have been deprecated, but not yet removed',
                                  'Neo4j::Driver::Net' => 'Explains the design of the networking modules',
                                  'Neo4j::Driver::Net::HTTP::LWP' => 'HTTP network adapter for libwww-perl',
                                  'Neo4j::Driver::Plugin' => 'Plug-in interface for Neo4j::Driver',
                                  'Neo4j::Driver::Record' => 'Container for Cypher result values',
                                  'Neo4j::Driver::Result' => 'Result of running a Cypher statement (a stream of records)',
                                  'Neo4j::Driver::ResultSummary' => 'Details about the result of running a statement',
                                  'Neo4j::Driver::ServerInfo' => 'Provides Neo4j server address and version',
                                  'Neo4j::Driver::Session' => 'Context of work for database interactions',
                                  'Neo4j::Driver::StatementResult' => 'DEPRECATED (renamed to Neo4j::Driver::Result)',
                                  'Neo4j::Driver::SummaryCounters' => 'Statement statistics',
                                  'Neo4j::Driver::TODO' => 'Information on planned improvements to Neo4j::Driver',
                                  'Neo4j::Driver::Transaction' => 'Logical container for an atomic unit of work',
                                  'Neo4j::Driver::Type::Bytes' => 'Represents a Neo4j byte array',
                                  'Neo4j::Driver::Type::Temporal' => 'DEPRECATED (use DateTime / Duration instead)',
                                  'Neo4j::Driver::Types' => 'Type mapping from Neo4j to Perl and vice versa'
                                },
          'author' => '',
          'dir_lib' => 'lib',
          'dir_t' => 't',
          'dir_xt' => 'xt',
          'dirs' => 14,
          'dirs_array' => [
                            'lib/Neo4j/Driver/Net/HTTP',
                            'lib/Neo4j/Driver/Net',
                            'lib/Neo4j/Driver/Result',
                            'lib/Neo4j/Driver/Type',
                            'lib/Neo4j/Driver',
                            'lib/Neo4j',
                            'lib/URI',
                            'lib',
                            't/lib/Neo4j_Test',
                            't/lib',
                            't/simulator',
                            't',
                            'xt/author',
                            'xt'
                          ],
          'dist' => 'Neo4j-Driver',
          'dynamic_config' => 0,
          'error' => {
                       'extracts_nicely' => 'expected Neo4j-Driver but got Neo4j-Driver-0.46'
                     },
          'extension' => 'tar.gz',
          'external_license_file' => 'LICENSE',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_changelog' => 'Changes',
          'file_cpanfile' => 'cpanfile',
          'file_license' => 'LICENSE',
          'file_makefile_pl' => 'Makefile.PL',
          'file_manifest' => 'MANIFEST',
          'file_meta_json' => 'META.json',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README',
          'files' => 123,
          'files_array' => [
                             'Changes',
                             'LICENSE',
                             'MANIFEST',
                             'META.json',
                             'META.yml',
                             'Makefile.PL',
                             'README',
                             'cpanfile',
                             'lib/Neo4j/Driver/Config.pod',
                             'lib/Neo4j/Driver/Deprecations.pod',
                             'lib/Neo4j/Driver/Events.pm',
                             'lib/Neo4j/Driver/Net/Bolt.pm',
                             'lib/Neo4j/Driver/Net/HTTP/LWP.pm',
                             'lib/Neo4j/Driver/Net/HTTP.pm',
                             'lib/Neo4j/Driver/Net.pod',
                             'lib/Neo4j/Driver/Plugin.pm',
                             'lib/Neo4j/Driver/Record.pm',
                             'lib/Neo4j/Driver/Result/Bolt.pm',
                             'lib/Neo4j/Driver/Result/JSON.pm',
                             'lib/Neo4j/Driver/Result/Jolt.pm',
                             'lib/Neo4j/Driver/Result/Text.pm',
                             'lib/Neo4j/Driver/Result.pm',
                             'lib/Neo4j/Driver/ResultColumns.pm',
                             'lib/Neo4j/Driver/ResultSummary.pm',
                             'lib/Neo4j/Driver/ServerInfo.pm',
                             'lib/Neo4j/Driver/Session.pm',
                             'lib/Neo4j/Driver/StatementResult.pm',
                             'lib/Neo4j/Driver/SummaryCounters.pm',
                             'lib/Neo4j/Driver/TODO.pod',
                             'lib/Neo4j/Driver/Transaction.pm',
                             'lib/Neo4j/Driver/Type/Bytes.pm',
                             'lib/Neo4j/Driver/Type/DateTime.pm',
                             'lib/Neo4j/Driver/Type/Duration.pm',
                             'lib/Neo4j/Driver/Type/Node.pm',
                             'lib/Neo4j/Driver/Type/Path.pm',
                             'lib/Neo4j/Driver/Type/Point.pm',
                             'lib/Neo4j/Driver/Type/Relationship.pm',
                             'lib/Neo4j/Driver/Type/Temporal.pm',
                             'lib/Neo4j/Driver/Types.pod',
                             'lib/Neo4j/Driver.pm',
                             'lib/URI/neo4j.pm',
                             't/bolt.t',
                             't/config.t',
                             't/deprecated.t',
                             't/experimental.t',
                             't/info.t',
                             't/jolt-mime.t',
                             't/jolt-types.t',
                             't/json-utf8.t',
                             't/net-lwp.t',
                             't/plugins.t',
                             't/record-ambiguous.t',
                             't/record.t',
                             't/session-execute.t',
                             't/session.t',
                             't/simulator/1b50ab87c090165edcff76695714a028.json',
                             't/simulator/258e20659e551c22f831022dfc85eaf3.json',
                             't/simulator/277d3b4e9542b5b288afe1334822921f.json',
                             't/simulator/36462d9ecaa934db2d00656a893d90e6.json',
                             't/simulator/3b306aea58d91ad728cb348058e3e7c5.json',
                             't/simulator/40b3874b81f18df51f7754909f8f1b37.json',
                             't/simulator/41b678d53f3d77d846b2c095b96bcf1e.json',
                             't/simulator/45e5a806b330f336f963c2860f4ae782.json',
                             't/simulator/46cd038afc0d41fae6c321db1dc25907.json',
                             't/simulator/473c6a8aeb2f66db9744274b9bd72670.json',
                             't/simulator/4826f03ad4b48d72bbf9be89655fe84b.json',
                             't/simulator/4a5a9ef3c7a3b4ccfe4ca2999ec1857c.json',
                             't/simulator/4ae098246b92d1e47643f2348172757a.json',
                             't/simulator/58e2f7fbcc39d89edef87a309e6a8473.json',
                             't/simulator/5f48aea17ef03872db49b87979c6c2ff.json',
                             't/simulator/65e077011a50f12691892fcdec4857bb.json',
                             't/simulator/690c256747c0333b23f1dfa6cd80fa48.json',
                             't/simulator/69e55ef75e82d59d5fc75c7200349e27.json',
                             't/simulator/70e75c2068c60bedcbc663f90b764537.json',
                             't/simulator/78e46089c159e63c5f15cfa49a41e8b0.json',
                             't/simulator/7a1951a7f314e5ac5acbf77414389e1b.json',
                             't/simulator/7efd7850f6cf3a9c76ce2849b2a884df.json',
                             't/simulator/7f52e5c2ca4fd46c0fa3d0fdf591c53a.json',
                             't/simulator/8611d52712e03d4111cafac619597b02.json',
                             't/simulator/88338be4e89d055564c8dfaec57aca3d.json',
                             't/simulator/8cc19434ccd37b4d2aaedabab0ab87b0.json',
                             't/simulator/900ca8317927b50dcac05e17bc33aa33.json',
                             't/simulator/9bc7a811318444f451cb294a906cb433.json',
                             't/simulator/a462aae2a270fc7e72eed97aecc65e6e.json',
                             't/simulator/a5439a4fc2d686ec7a1e7a391bde49af.json',
                             't/simulator/ad37aafc969ac5d936c359b4674862ac.json',
                             't/simulator/b198251a91a65e89eafeb9f5fe9a8f40.json',
                             't/simulator/b1a1d249e95abed861cdb99d452ff1a7.json',
                             't/simulator/b4d1522bea6dbd339ea2a04c1487be22.json',
                             't/simulator/bab18e00cb02c7e581b7f059c6cac810.json',
                             't/simulator/baccda8df4b1313033359e84b7e2f7d7.json',
                             't/simulator/bedf48911c566152c9af73670e9183a3.json',
                             't/simulator/c3f8270aa594c3ac3ecf19835c4a252d.json',
                             't/simulator/c9d365dda9414c90db5565e3d88fa5e8.json',
                             't/simulator/cc6640bda0105e9e7fbe830694d43182.json',
                             't/simulator/cc85064d581f7e7614d8156c6f14b96a.json',
                             't/simulator/cfabc137884e853a99db17e0dc661a76.json',
                             't/simulator/d2b770da4270c2bdc2461885c6ec8498.json',
                             't/simulator/d6ed6e50d94cb2c035eb1d7fc82cf3a3.json',
                             't/simulator/dbed469579eac391ac5e220ce2177634.json',
                             't/simulator/dd0964d85a298c02b26989feed9ec37d.json',
                             't/simulator/ddbc10fd5c1ee2ad98c67563ae1c01ad.json',
                             't/simulator/e78931db236d261eb4a643b02b4f4f40.json',
                             't/simulator/eb3a1958964aeb15063d12effc5aae84.json',
                             't/simulator/ee0ae03c20b92e3bd7cf52af42b187dc.json',
                             't/simulator/f2b9cd1caf727069fcf4d783c052c9cf.json',
                             't/simulator/f3e5fca687a369c9b5a99a2f08d91539.json',
                             't/simulator/f61719f9841f68ab6580606fe575cc68.json',
                             't/simulator/fbb1ee3afc0f7ab9d41ea738dcd68bd3.json',
                             't/simulator/fca8d030f65653bbe39370d7fa3c12ae.json',
                             't/simulator/ffe82ba4e04d84af8f74a4b2fc99ec99.json',
                             't/statement-result.t',
                             't/summary.t',
                             't/transaction.t',
                             't/tx-concurrent.t',
                             't/types-spatial.t',
                             't/types-temporal.t',
                             't/types.t',
                             'xt/author/compatibility.t',
                             'xt/author/cpan-distros.t',
                             'xt/author/internals.t',
                             'xt/author/minimum-version.t',
                             'xt/author/pod-syntax.t'
                           ],
          'files_hash' => {
                            'Changes' => {
                                           'mtime' => 1709469480,
                                           'size' => 10579
                                         },
                            'LICENSE' => {
                                           'mtime' => 1709469480,
                                           'size' => 8902
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1709469480,
                                            'size' => 4548
                                          },
                            'META.json' => {
                                             'mtime' => 1709469480,
                                             'size' => 6155
                                           },
                            'META.yml' => {
                                            'mtime' => 1709469480,
                                            'size' => 3653
                                          },
                            'Makefile.PL' => {
                                               'mtime' => 1709469480,
                                               'requires' => {
                                                               'ExtUtils::MakeMaker' => '0',
                                                               'perl' => '5.010',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 1804
                                             },
                            'README' => {
                                          'mtime' => 1709469480,
                                          'size' => 1336
                                        },
                            'cpanfile' => {
                                            'mtime' => 1709469480,
                                            'size' => 1383
                                          },
                            'lib/Neo4j/Driver.pm' => {
                                                       'license' => 'Artistic_2_0',
                                                       'module' => 'Neo4j::Driver',
                                                       'mtime' => 1709469480,
                                                       'requires' => {
                                                                       'Carp' => '0',
                                                                       'Neo4j::Driver::Events' => '0',
                                                                       'Neo4j::Driver::Session' => '0',
                                                                       'Neo4j::Driver::Type::Node' => '0',
                                                                       'Neo4j::Driver::Type::Path' => '0',
                                                                       'Neo4j::Driver::Type::Point' => '0',
                                                                       'Neo4j::Driver::Type::Relationship' => '0',
                                                                       'Neo4j::Driver::Type::Temporal' => '0',
                                                                       'URI' => '1.25',
                                                                       'URI::_server' => '0',
                                                                       'parent' => '0',
                                                                       'perl' => '5.010',
                                                                       'strict' => '0',
                                                                       'utf8' => '0',
                                                                       'warnings' => '0'
                                                                     },
                                                       'size' => 14901
                                                     },
                            'lib/Neo4j/Driver/Config.pod' => {
                                                               'license' => 'Artistic_2_0',
                                                               'mtime' => 1709469480,
                                                               'size' => 8292
                                                             },
                            'lib/Neo4j/Driver/Deprecations.pod' => {
                                                                     'license' => 'Artistic_2_0',
                                                                     'mtime' => 1709469480,
                                                                     'size' => 29111
                                                                   },
                            'lib/Neo4j/Driver/Events.pm' => {
                                                              'module' => 'Neo4j::Driver::Events',
                                                              'mtime' => 1709469480,
                                                              'requires' => {
                                                                              'Carp' => '0',
                                                                              'Scalar::Util' => '0',
                                                                              'perl' => '5.010',
                                                                              'strict' => '0',
                                                                              'utf8' => '0',
                                                                              'warnings' => '0'
                                                                            },
                                                              'size' => 3243
                                                            },
                            'lib/Neo4j/Driver/Net.pod' => {
                                                            'license' => 'Artistic_2_0',
                                                            'mtime' => 1709469480,
                                                            'size' => 13309
                                                          },
                            'lib/Neo4j/Driver/Net/Bolt.pm' => {
                                                                'module' => 'Neo4j::Driver::Net::Bolt',
                                                                'mtime' => 1709469480,
                                                                'requires' => {
                                                                                'Carp' => '0',
                                                                                'Neo4j::Driver::Result::Bolt' => '0',
                                                                                'Neo4j::Driver::ServerInfo' => '0',
                                                                                'Neo4j::Error' => '0',
                                                                                'Try::Tiny' => '0',
                                                                                'URI' => '1.25',
                                                                                'perl' => '5.010',
                                                                                'strict' => '0',
                                                                                'utf8' => '0',
                                                                                'warnings' => '0'
                                                                              },
                                                                'size' => 6198,
                                                                'suggests' => {
                                                                                'Neo4j::Bolt' => '0'
                                                                              }
                                                              },
                            'lib/Neo4j/Driver/Net/HTTP.pm' => {
                                                                'module' => 'Neo4j::Driver::Net::HTTP',
                                                                'mtime' => 1709469480,
                                                                'requires' => {
                                                                                'Carp' => '0',
                                                                                'Neo4j::Driver::Net::HTTP::LWP' => '0',
                                                                                'Neo4j::Driver::Result::JSON' => '0',
                                                                                'Neo4j::Driver::Result::Jolt' => '0',
                                                                                'Neo4j::Driver::Result::Text' => '0',
                                                                                'Neo4j::Driver::ServerInfo' => '0',
                                                                                'Time::Piece' => '1.20',
                                                                                'URI' => '1.31',
                                                                                'perl' => '5.010',
                                                                                'strict' => '0',
                                                                                'utf8' => '0',
                                                                                'warnings' => '0'
                                                                              },
                                                                'size' => 8068
                                                              },
                            'lib/Neo4j/Driver/Net/HTTP/LWP.pm' => {
                                                                    'license' => 'Artistic_2_0',
                                                                    'module' => 'Neo4j::Driver::Net::HTTP::LWP',
                                                                    'mtime' => 1709469480,
                                                                    'requires' => {
                                                                                    'Carp' => '0',
                                                                                    'JSON::MaybeXS' => '1.003003',
                                                                                    'LWP::UserAgent' => '6.04',
                                                                                    'URI' => '1.31',
                                                                                    'perl' => '5.010',
                                                                                    'strict' => '0',
                                                                                    'utf8' => '0',
                                                                                    'warnings' => '0'
                                                                                  },
                                                                    'size' => 6185
                                                                  },
                            'lib/Neo4j/Driver/Plugin.pm' => {
                                                              'license' => 'Artistic_2_0',
                                                              'module' => 'Neo4j::Driver::Plugin',
                                                              'mtime' => 1709469480,
                                                              'requires' => {
                                                                              'perl' => '5.010',
                                                                              'strict' => '0',
                                                                              'warnings' => '0'
                                                                            },
                                                              'size' => 18640
                                                            },
                            'lib/Neo4j/Driver/Record.pm' => {
                                                              'license' => 'Artistic_2_0',
                                                              'module' => 'Neo4j::Driver::Record',
                                                              'mtime' => 1709469480,
                                                              'noes' => {
                                                                          'if' => '0',
                                                                          'warnings' => '0'
                                                                        },
                                                              'requires' => {
                                                                              'Carp' => '0',
                                                                              'JSON::MaybeXS' => '1.003003',
                                                                              'Neo4j::Driver::ResultSummary' => '0',
                                                                              'perl' => '5.010',
                                                                              'strict' => '0',
                                                                              'utf8' => '0',
                                                                              'warnings' => '0'
                                                                            },
                                                              'size' => 5663
                                                            },
                            'lib/Neo4j/Driver/Result.pm' => {
                                                              'license' => 'Artistic_2_0',
                                                              'module' => 'Neo4j::Driver::Result',
                                                              'mtime' => 1709469480,
                                                              'noes' => {
                                                                          'if' => '0'
                                                                        },
                                                              'recommends' => {
                                                                                'JSON::PP' => '0'
                                                                              },
                                                              'requires' => {
                                                                              'Carp' => '0',
                                                                              'Neo4j::Driver::Record' => '0',
                                                                              'Neo4j::Driver::ResultColumns' => '0',
                                                                              'Neo4j::Driver::ResultSummary' => '0',
                                                                              'Neo4j::Driver::StatementResult' => '0',
                                                                              'parent' => '0',
                                                                              'perl' => '5.010',
                                                                              'strict' => '0',
                                                                              'utf8' => '0',
                                                                              'warnings' => '0'
                                                                            },
                                                              'size' => 10323
                                                            },
                            'lib/Neo4j/Driver/Result/Bolt.pm' => {
                                                                   'module' => 'Neo4j::Driver::Result::Bolt',
                                                                   'mtime' => 1709469480,
                                                                   'requires' => {
                                                                                   'Carp' => '0',
                                                                                   'List::Util' => '0',
                                                                                   'Neo4j::Driver::Net::Bolt' => '0',
                                                                                   'Neo4j::Driver::Result' => '0',
                                                                                   'parent' => '0',
                                                                                   'perl' => '5.010',
                                                                                   'strict' => '0',
                                                                                   'utf8' => '0',
                                                                                   'warnings' => '0'
                                                                                 },
                                                                   'size' => 6037
                                                                 },
                            'lib/Neo4j/Driver/Result/JSON.pm' => {
                                                                   'module' => 'Neo4j::Driver::Result::JSON',
                                                                   'mtime' => 1709469480,
                                                                   'recommends' => {
                                                                                     'Neo4j::Driver::Type::DateTime' => '0',
                                                                                     'Neo4j::Driver::Type::Duration' => '0'
                                                                                   },
                                                                   'requires' => {
                                                                                   'Carp' => '0',
                                                                                   'JSON::MaybeXS' => '1.002004',
                                                                                   'Neo4j::Driver::Result' => '0',
                                                                                   'Neo4j::Error' => '0',
                                                                                   'Try::Tiny' => '0',
                                                                                   'URI' => '1.31',
                                                                                   'parent' => '0',
                                                                                   'perl' => '5.010',
                                                                                   'strict' => '0',
                                                                                   'utf8' => '0',
                                                                                   'warnings' => '0'
                                                                                 },
                                                                   'size' => 8728
                                                                 },
                            'lib/Neo4j/Driver/Result/Jolt.pm' => {
                                                                   'module' => 'Neo4j::Driver::Result::Jolt',
                                                                   'mtime' => 1709469480,
                                                                   'recommends' => {
                                                                                     'Neo4j::Driver::Type::Bytes' => '0',
                                                                                     'Neo4j::Driver::Type::DateTime' => '0',
                                                                                     'Neo4j::Driver::Type::Duration' => '0'
                                                                                   },
                                                                   'requires' => {
                                                                                   'Carp' => '0',
                                                                                   'JSON::MaybeXS' => '1.002004',
                                                                                   'Neo4j::Driver::Result' => '0',
                                                                                   'Neo4j::Error' => '0',
                                                                                   'parent' => '0',
                                                                                   'perl' => '5.010',
                                                                                   'strict' => '0',
                                                                                   'utf8' => '0',
                                                                                   'warnings' => '0'
                                                                                 },
                                                                   'size' => 11628
                                                                 },
                            'lib/Neo4j/Driver/Result/Text.pm' => {
                                                                   'module' => 'Neo4j::Driver::Result::Text',
                                                                   'mtime' => 1709469480,
                                                                   'requires' => {
                                                                                   'Neo4j::Driver::Result' => '0',
                                                                                   'Neo4j::Error' => '0',
                                                                                   'parent' => '0',
                                                                                   'perl' => '5.010',
                                                                                   'strict' => '0',
                                                                                   'utf8' => '0',
                                                                                   'warnings' => '0'
                                                                                 },
                                                                   'size' => 2122
                                                                 },
                            'lib/Neo4j/Driver/ResultColumns.pm' => {
                                                                     'module' => 'Neo4j::Driver::ResultColumns',
                                                                     'mtime' => 1709469480,
                                                                     'requires' => {
                                                                                     'Carp' => '0',
                                                                                     'perl' => '5.010',
                                                                                     'strict' => '0',
                                                                                     'utf8' => '0',
                                                                                     'warnings' => '0'
                                                                                   },
                                                                     'size' => 2692
                                                                   },
                            'lib/Neo4j/Driver/ResultSummary.pm' => {
                                                                     'license' => 'Artistic_2_0',
                                                                     'module' => 'Neo4j::Driver::ResultSummary',
                                                                     'mtime' => 1709469480,
                                                                     'requires' => {
                                                                                     'Carp' => '0',
                                                                                     'Neo4j::Driver::SummaryCounters' => '0',
                                                                                     'perl' => '5.010',
                                                                                     'strict' => '0',
                                                                                     'utf8' => '0',
                                                                                     'warnings' => '0'
                                                                                   },
                                                                     'size' => 4715
                                                                   },
                            'lib/Neo4j/Driver/ServerInfo.pm' => {
                                                                  'license' => 'Artistic_2_0',
                                                                  'module' => 'Neo4j::Driver::ServerInfo',
                                                                  'mtime' => 1709469480,
                                                                  'requires' => {
                                                                                  'Carp' => '0',
                                                                                  'URI' => '1.25',
                                                                                  'perl' => '5.010',
                                                                                  'strict' => '0',
                                                                                  'utf8' => '0',
                                                                                  'warnings' => '0'
                                                                                },
                                                                  'size' => 4243
                                                                },
                            'lib/Neo4j/Driver/Session.pm' => {
                                                               'license' => 'Artistic_2_0',
                                                               'module' => 'Neo4j::Driver::Session',
                                                               'mtime' => 1709469480,
                                                               'requires' => {
                                                                               'Carp' => '0',
                                                                               'List::Util' => '0',
                                                                               'Neo4j::Driver::Net::Bolt' => '0',
                                                                               'Neo4j::Driver::Net::HTTP' => '0',
                                                                               'Neo4j::Driver::Transaction' => '0',
                                                                               'Neo4j::Error' => '0',
                                                                               'Scalar::Util' => '0',
                                                                               'Time::HiRes' => '0',
                                                                               'Try::Tiny' => '0',
                                                                               'URI' => '1.25',
                                                                               'parent' => '0',
                                                                               'perl' => '5.010',
                                                                               'strict' => '0',
                                                                               'utf8' => '0',
                                                                               'warnings' => '0'
                                                                             },
                                                               'size' => 11387
                                                             },
                            'lib/Neo4j/Driver/StatementResult.pm' => {
                                                                       'license' => 'Artistic_2_0',
                                                                       'module' => 'Neo4j::Driver::StatementResult',
                                                                       'mtime' => 1709469480,
                                                                       'requires' => {
                                                                                       'perl' => '5.010',
                                                                                       'strict' => '0',
                                                                                       'warnings' => '0'
                                                                                     },
                                                                       'size' => 1877
                                                                     },
                            'lib/Neo4j/Driver/SummaryCounters.pm' => {
                                                                       'license' => 'Artistic_2_0',
                                                                       'module' => 'Neo4j::Driver::SummaryCounters',
                                                                       'mtime' => 1709469480,
                                                                       'noes' => {
                                                                                   'strict' => '0'
                                                                                 },
                                                                       'requires' => {
                                                                                       'perl' => '5.010',
                                                                                       'strict' => '0',
                                                                                       'utf8' => '0',
                                                                                       'warnings' => '0'
                                                                                     },
                                                                       'size' => 4096
                                                                     },
                            'lib/Neo4j/Driver/TODO.pod' => {
                                                             'mtime' => 1709469480,
                                                             'size' => 624
                                                           },
                            'lib/Neo4j/Driver/Transaction.pm' => {
                                                                   'license' => 'Artistic_2_0',
                                                                   'module' => 'Neo4j::Driver::Transaction',
                                                                   'mtime' => 1709469480,
                                                                   'requires' => {
                                                                                   'Carp' => '0',
                                                                                   'Neo4j::Driver::Result' => '0',
                                                                                   'Scalar::Util' => '0',
                                                                                   'Try::Tiny' => '0',
                                                                                   'parent' => '0',
                                                                                   'perl' => '5.010',
                                                                                   'strict' => '0',
                                                                                   'utf8' => '0',
                                                                                   'warnings' => '0'
                                                                                 },
                                                                   'size' => 13828
                                                                 },
                            'lib/Neo4j/Driver/Type/Bytes.pm' => {
                                                                  'license' => 'Artistic_2_0',
                                                                  'module' => 'Neo4j::Driver::Type::Bytes',
                                                                  'mtime' => 1709469480,
                                                                  'requires' => {
                                                                                  'overload' => '0',
                                                                                  'parent' => '0',
                                                                                  'perl' => '5.010',
                                                                                  'strict' => '0',
                                                                                  'utf8' => '0',
                                                                                  'warnings' => '0'
                                                                                },
                                                                  'size' => 1096
                                                                },
                            'lib/Neo4j/Driver/Type/DateTime.pm' => {
                                                                     'module' => 'Neo4j::Driver::Type::DateTime',
                                                                     'mtime' => 1709469480,
                                                                     'recommends' => {
                                                                                       'Time::Piece' => '0'
                                                                                     },
                                                                     'requires' => {
                                                                                     'Neo4j::Driver::Type::Temporal' => '0',
                                                                                     'Neo4j::Types::DateTime' => '0',
                                                                                     'parent' => '0',
                                                                                     'perl' => '5.010',
                                                                                     'strict' => '0',
                                                                                     'utf8' => '0',
                                                                                     'warnings' => '0'
                                                                                   },
                                                                     'size' => 2026
                                                                   },
                            'lib/Neo4j/Driver/Type/Duration.pm' => {
                                                                     'module' => 'Neo4j::Driver::Type::Duration',
                                                                     'mtime' => 1709469480,
                                                                     'requires' => {
                                                                                     'Neo4j::Driver::Type::Temporal' => '0',
                                                                                     'Neo4j::Types::Duration' => '0',
                                                                                     'parent' => '0',
                                                                                     'perl' => '5.010',
                                                                                     'strict' => '0',
                                                                                     'utf8' => '0',
                                                                                     'warnings' => '0'
                                                                                   },
                                                                     'size' => 1531
                                                                   },
                            'lib/Neo4j/Driver/Type/Node.pm' => {
                                                                 'module' => 'Neo4j::Driver::Type::Node',
                                                                 'mtime' => 1709469480,
                                                                 'requires' => {
                                                                                 'Carp' => '0',
                                                                                 'Neo4j::Types::Node' => '0',
                                                                                 'overload' => '0',
                                                                                 'parent' => '0',
                                                                                 'perl' => '5.010',
                                                                                 'strict' => '0',
                                                                                 'utf8' => '0',
                                                                                 'warnings' => '0',
                                                                                 'warnings::register' => '0'
                                                                               },
                                                                 'size' => 2419
                                                               },
                            'lib/Neo4j/Driver/Type/Path.pm' => {
                                                                 'module' => 'Neo4j::Driver::Type::Path',
                                                                 'mtime' => 1709469480,
                                                                 'requires' => {
                                                                                 'Carp' => '0',
                                                                                 'Neo4j::Types::Path' => '0',
                                                                                 'overload' => '0',
                                                                                 'parent' => '0',
                                                                                 'perl' => '5.010',
                                                                                 'strict' => '0',
                                                                                 'utf8' => '0',
                                                                                 'warnings' => '0'
                                                                               },
                                                                 'size' => 1116
                                                               },
                            'lib/Neo4j/Driver/Type/Point.pm' => {
                                                                  'module' => 'Neo4j::Driver::Type::Point',
                                                                  'mtime' => 1709469480,
                                                                  'requires' => {
                                                                                  'Neo4j::Types::Point' => '0',
                                                                                  'parent' => '0',
                                                                                  'perl' => '5.010',
                                                                                  'strict' => '0',
                                                                                  'utf8' => '0',
                                                                                  'warnings' => '0'
                                                                                },
                                                                  'size' => 875
                                                                },
                            'lib/Neo4j/Driver/Type/Relationship.pm' => {
                                                                         'module' => 'Neo4j::Driver::Type::Relationship',
                                                                         'mtime' => 1709469480,
                                                                         'requires' => {
                                                                                         'Neo4j::Types::Relationship' => '0',
                                                                                         'overload' => '0',
                                                                                         'parent' => '0',
                                                                                         'perl' => '5.010',
                                                                                         'strict' => '0',
                                                                                         'utf8' => '0',
                                                                                         'warnings' => '0',
                                                                                         'warnings::register' => '0'
                                                                                       },
                                                                         'size' => 2866
                                                                       },
                            'lib/Neo4j/Driver/Type/Temporal.pm' => {
                                                                     'license' => 'Artistic_2_0',
                                                                     'module' => 'Neo4j::Driver::Type::Temporal',
                                                                     'mtime' => 1709469480,
                                                                     'requires' => {
                                                                                     'perl' => '5.010',
                                                                                     'strict' => '0',
                                                                                     'warnings' => '0'
                                                                                   },
                                                                     'size' => 1552
                                                                   },
                            'lib/Neo4j/Driver/Types.pod' => {
                                                              'license' => 'Artistic_2_0',
                                                              'mtime' => 1709469480,
                                                              'size' => 7465
                                                            },
                            'lib/URI/neo4j.pm' => {
                                                    'module' => 'URI::neo4j',
                                                    'mtime' => 1709469480,
                                                    'requires' => {
                                                                    'URI::_server' => '0',
                                                                    'parent' => '0',
                                                                    'perl' => '5.010',
                                                                    'strict' => '0',
                                                                    'warnings' => '0'
                                                                  },
                                                    'size' => 148
                                                  },
                            't/bolt.t' => {
                                            'mtime' => 1709469480,
                                            'noes' => {
                                                        'warnings' => '0'
                                                      },
                                            'recommends' => {
                                                              'Test::Warnings' => '0'
                                                            },
                                            'requires' => {
                                                            'JSON::PP' => '0',
                                                            'Neo4j::Driver' => '0',
                                                            'Neo4j_Test' => '0',
                                                            'Test::Exception' => '0',
                                                            'Test::More' => '0.94',
                                                            'Test::Warnings' => '0.010',
                                                            'if' => '0',
                                                            'lib' => '0',
                                                            'parent' => '0',
                                                            'strict' => '0',
                                                            'warnings' => '0'
                                                          },
                                            'size' => 9664
                                          },
                            't/config.t' => {
                                              'mtime' => 1709469480,
                                              'recommends' => {
                                                                'Test::Warnings' => '0'
                                                              },
                                              'requires' => {
                                                              'Neo4j_Test' => '0',
                                                              'Neo4j_Test::MockHTTP' => '0',
                                                              'Test::Exception' => '0',
                                                              'Test::More' => '0.94',
                                                              'Test::Warnings' => '0.010',
                                                              'Try::Tiny' => '0',
                                                              'if' => '0',
                                                              'lib' => '0',
                                                              'strict' => '0',
                                                              'warnings' => '0'
                                                            },
                                              'size' => 21127
                                            },
                            't/deprecated.t' => {
                                                  'mtime' => 1709469480,
                                                  'recommends' => {
                                                                    'Neo4j::Driver::Plugin' => '0',
                                                                    'Neo4j_Test::MockHTTP' => '0',
                                                                    'Neo4j_Test::Sim' => '0',
                                                                    'Test::Exception' => '0',
                                                                    'Test::More' => '0.94',
                                                                    'Test::Warnings' => '0.010',
                                                                    'if' => '0',
                                                                    'parent' => '0'
                                                                  },
                                                  'requires' => {
                                                                  'Neo4j_Test' => '0',
                                                                  'lib' => '0',
                                                                  'strict' => '0',
                                                                  'warnings' => '0'
                                                                },
                                                  'size' => 20785
                                                },
                            't/experimental.t' => {
                                                    'mtime' => 1709469480,
                                                    'recommends' => {
                                                                      'Neo4j::Driver' => '0',
                                                                      'Neo4j_Test::MockHTTP' => '0',
                                                                      'Test::Exception' => '0',
                                                                      'Test::More' => '0.94',
                                                                      'Test::Warnings' => '0.010',
                                                                      'if' => '0'
                                                                    },
                                                    'requires' => {
                                                                    'Neo4j_Test' => '0',
                                                                    'lib' => '0',
                                                                    'strict' => '0',
                                                                    'warnings' => '0'
                                                                  },
                                                    'size' => 3228
                                                  },
                            't/info.t' => {
                                            'mtime' => 1709469480,
                                            'recommends' => {
                                                              'Test::Warnings' => '0'
                                                            },
                                            'requires' => {
                                                            'Neo4j_Test' => '0',
                                                            'Neo4j_Test::MockHTTP' => '0',
                                                            'Test::Exception' => '0',
                                                            'Test::More' => '0.94',
                                                            'Test::Warnings' => '0.010',
                                                            'if' => '0',
                                                            'lib' => '0',
                                                            'strict' => '0',
                                                            'warnings' => '0'
                                                          },
                                            'size' => 4925
                                          },
                            't/jolt-mime.t' => {
                                                 'mtime' => 1709469480,
                                                 'recommends' => {
                                                                   'Test::Warnings' => '0'
                                                                 },
                                                 'requires' => {
                                                                 'Neo4j::Driver' => '0',
                                                                 'Neo4j_Test::EchoHTTP' => '0',
                                                                 'Neo4j_Test::MockHTTP' => '0',
                                                                 'Test::Exception' => '0',
                                                                 'Test::More' => '0.94',
                                                                 'Test::Warnings' => '0.010',
                                                                 'if' => '0',
                                                                 'lib' => '0',
                                                                 'strict' => '0',
                                                                 'warnings' => '0'
                                                               },
                                                 'size' => 7015
                                               },
                            't/jolt-types.t' => {
                                                  'mtime' => 1709469480,
                                                  'noes' => {
                                                              'warnings' => '0'
                                                            },
                                                  'recommends' => {
                                                                    'Test::Warnings' => '0'
                                                                  },
                                                  'requires' => {
                                                                  'Neo4j::Driver' => '0',
                                                                  'Neo4j::Types' => '0',
                                                                  'Neo4j_Test' => '0',
                                                                  'Neo4j_Test::MockHTTP' => '0',
                                                                  'Test::Exception' => '0',
                                                                  'Test::More' => '0.94',
                                                                  'Test::Warnings' => '0.010',
                                                                  'if' => '0',
                                                                  'lib' => '0',
                                                                  'strict' => '0',
                                                                  'warnings' => '0'
                                                                },
                                                  'size' => 20204
                                                },
                            't/json-utf8.t' => {
                                                 'mtime' => 1709469480,
                                                 'recommends' => {
                                                                   'Test::Exception' => '0',
                                                                   'Test::More' => '0.94',
                                                                   'Test::Warnings' => '0.010',
                                                                   'if' => '0'
                                                                 },
                                                 'requires' => {
                                                                 'Neo4j_Test' => '0',
                                                                 'lib' => '0',
                                                                 'strict' => '0',
                                                                 'utf8' => '0',
                                                                 'warnings' => '0'
                                                               },
                                                 'size' => 3492
                                               },
                            't/lib/Neo4j_Test.pm' => {
                                                       'mtime' => 1709469480,
                                                       'no_index' => 1,
                                                       'noes' => {
                                                                   'if' => '0'
                                                                 },
                                                       'recommends' => {
                                                                         'Scalar::Util' => '0'
                                                                       },
                                                       'requires' => {
                                                                       'Neo4j::Driver' => '0',
                                                                       'Neo4j_Test::NetModulePlugin' => '0',
                                                                       'Neo4j_Test::Sim' => '0',
                                                                       'URI' => '0',
                                                                       'strict' => '0',
                                                                       'warnings' => '0'
                                                                     },
                                                       'size' => 4039
                                                     },
                            't/lib/Neo4j_Test/EchoHTTP.pm' => {
                                                                'mtime' => 1709469480,
                                                                'no_index' => 1,
                                                                'requires' => {
                                                                                'Neo4j_Test::MockHTTP' => '0',
                                                                                'parent' => '0',
                                                                                'strict' => '0',
                                                                                'warnings' => '0'
                                                                              },
                                                                'size' => 3227
                                                              },
                            't/lib/Neo4j_Test/MockHTTP.pm' => {
                                                                'mtime' => 1709469480,
                                                                'no_index' => 1,
                                                                'requires' => {
                                                                                'JSON::MaybeXS' => '0',
                                                                                'Neo4j::Driver::Net::HTTP::LWP' => '0',
                                                                                'Neo4j::Driver::Plugin' => '0',
                                                                                'parent' => '0',
                                                                                'strict' => '0',
                                                                                'warnings' => '0'
                                                                              },
                                                                'size' => 4531
                                                              },
                            't/lib/Neo4j_Test/MockQuery.pm' => {
                                                                 'mtime' => 1709469480,
                                                                 'no_index' => 1,
                                                                 'noes' => {
                                                                             'warnings' => '0'
                                                                           },
                                                                 'requires' => {
                                                                                 'List::Util' => '0',
                                                                                 'Neo4j_Test::MockHTTP' => '0',
                                                                                 'Scalar::Util' => '0',
                                                                                 'parent' => '0',
                                                                                 'strict' => '0',
                                                                                 'warnings' => '0'
                                                                               },
                                                                 'size' => 5589
                                                               },
                            't/lib/Neo4j_Test/NetModulePlugin.pm' => {
                                                                       'mtime' => 1709469480,
                                                                       'no_index' => 1,
                                                                       'size' => 641
                                                                     },
                            't/lib/Neo4j_Test/Sim.pm' => {
                                                           'mtime' => 1709469480,
                                                           'no_index' => 1,
                                                           'size' => 5705
                                                         },
                            't/net-lwp.t' => {
                                               'mtime' => 1709469480,
                                               'recommends' => {
                                                                 'Test::Warnings' => '0'
                                                               },
                                               'requires' => {
                                                               'HTTP::Headers' => '0',
                                                               'HTTP::Response' => '0',
                                                               'JSON::MaybeXS' => '0',
                                                               'Mock::Quick' => '0',
                                                               'Neo4j::Driver::Net::HTTP::LWP' => '0',
                                                               'Test::Exception' => '0',
                                                               'Test::More' => '0.94',
                                                               'Test::Warnings' => '0.010',
                                                               'URI' => '0',
                                                               'if' => '0',
                                                               'lib' => '0',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 10620,
                                               'suggests' => {
                                                               'LWP::Protocol::https' => '0',
                                                               'Mozilla::CA' => '0'
                                                             }
                                             },
                            't/plugins.t' => {
                                               'mtime' => 1709469480,
                                               'recommends' => {
                                                                 'Test::Warnings' => '0'
                                                               },
                                               'requires' => {
                                                               'Neo4j::Driver' => '0',
                                                               'Neo4j::Driver::Events' => '0',
                                                               'Neo4j::Driver::Plugin' => '0',
                                                               'Neo4j_Test::MockHTTP' => '0',
                                                               'Test::Exception' => '0',
                                                               'Test::More' => '0.94',
                                                               'Test::Warnings' => '0.010',
                                                               'if' => '0',
                                                               'lib' => '0',
                                                               'parent' => '0',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 8926
                                             },
                            't/record-ambiguous.t' => {
                                                        'mtime' => 1709469480,
                                                        'recommends' => {
                                                                          'Test::Warnings' => '0'
                                                                        },
                                                        'requires' => {
                                                                        'Neo4j::Driver' => '0',
                                                                        'Neo4j_Test::MockHTTP' => '0',
                                                                        'Test::Exception' => '0',
                                                                        'Test::More' => '0.94',
                                                                        'Test::Warnings' => '0.010',
                                                                        'if' => '0',
                                                                        'lib' => '0',
                                                                        'strict' => '0',
                                                                        'warnings' => '0'
                                                                      },
                                                        'size' => 2302
                                                      },
                            't/record.t' => {
                                              'mtime' => 1709469480,
                                              'recommends' => {
                                                                'Neo4j::Driver::Record' => '0',
                                                                'Test::Exception' => '0',
                                                                'Test::More' => '0.94',
                                                                'Test::Warnings' => '0.010',
                                                                'if' => '0'
                                                              },
                                              'requires' => {
                                                              'Neo4j_Test' => '0',
                                                              'lib' => '0',
                                                              'strict' => '0',
                                                              'warnings' => '0'
                                                            },
                                              'size' => 2485
                                            },
                            't/session-execute.t' => {
                                                       'mtime' => 1709469480,
                                                       'recommends' => {
                                                                         'Test::Warnings' => '0'
                                                                       },
                                                       'requires' => {
                                                                       'Neo4j::Driver' => '0',
                                                                       'Neo4j::Driver::Plugin' => '0',
                                                                       'Neo4j::Error' => '0',
                                                                       'Neo4j_Test' => '0',
                                                                       'Neo4j_Test::EchoHTTP' => '0',
                                                                       'Neo4j_Test::MockQuery' => '0',
                                                                       'Scalar::Util' => '0',
                                                                       'Test::Exception' => '0',
                                                                       'Test::More' => '0.94',
                                                                       'Test::Warnings' => '0.010',
                                                                       'Time::HiRes' => '0',
                                                                       'if' => '0',
                                                                       'lib' => '0',
                                                                       'parent' => '0',
                                                                       'strict' => '0',
                                                                       'warnings' => '0'
                                                                     },
                                                       'size' => 7540
                                                     },
                            't/session.t' => {
                                               'mtime' => 1709469480,
                                               'recommends' => {
                                                                 'Test::Exception' => '0',
                                                                 'Test::More' => '0.94',
                                                                 'Test::Warnings' => '0.010',
                                                                 'if' => '0'
                                                               },
                                               'requires' => {
                                                               'Neo4j_Test' => '0',
                                                               'lib' => '0',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 3241
                                             },
                            't/simulator/1b50ab87c090165edcff76695714a028.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 241
                                                                                   },
                            't/simulator/258e20659e551c22f831022dfc85eaf3.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 218
                                                                                   },
                            't/simulator/277d3b4e9542b5b288afe1334822921f.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 612
                                                                                   },
                            't/simulator/36462d9ecaa934db2d00656a893d90e6.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 446
                                                                                   },
                            't/simulator/3b306aea58d91ad728cb348058e3e7c5.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 529
                                                                                   },
                            't/simulator/40b3874b81f18df51f7754909f8f1b37.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 430
                                                                                   },
                            't/simulator/41b678d53f3d77d846b2c095b96bcf1e.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 567
                                                                                   },
                            't/simulator/45e5a806b330f336f963c2860f4ae782.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 230
                                                                                   },
                            't/simulator/46cd038afc0d41fae6c321db1dc25907.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 241
                                                                                   },
                            't/simulator/473c6a8aeb2f66db9744274b9bd72670.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 440
                                                                                   },
                            't/simulator/4826f03ad4b48d72bbf9be89655fe84b.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 400
                                                                                   },
                            't/simulator/4a5a9ef3c7a3b4ccfe4ca2999ec1857c.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 780
                                                                                   },
                            't/simulator/4ae098246b92d1e47643f2348172757a.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 401
                                                                                   },
                            't/simulator/58e2f7fbcc39d89edef87a309e6a8473.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 1705
                                                                                   },
                            't/simulator/5f48aea17ef03872db49b87979c6c2ff.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 478
                                                                                   },
                            't/simulator/65e077011a50f12691892fcdec4857bb.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 256
                                                                                   },
                            't/simulator/690c256747c0333b23f1dfa6cd80fa48.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 404
                                                                                   },
                            't/simulator/69e55ef75e82d59d5fc75c7200349e27.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 418
                                                                                   },
                            't/simulator/70e75c2068c60bedcbc663f90b764537.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 478
                                                                                   },
                            't/simulator/78e46089c159e63c5f15cfa49a41e8b0.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 611
                                                                                   },
                            't/simulator/7a1951a7f314e5ac5acbf77414389e1b.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 214
                                                                                   },
                            't/simulator/7efd7850f6cf3a9c76ce2849b2a884df.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 2434
                                                                                   },
                            't/simulator/7f52e5c2ca4fd46c0fa3d0fdf591c53a.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 241
                                                                                   },
                            't/simulator/8611d52712e03d4111cafac619597b02.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 799
                                                                                   },
                            't/simulator/88338be4e89d055564c8dfaec57aca3d.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 404
                                                                                   },
                            't/simulator/8cc19434ccd37b4d2aaedabab0ab87b0.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 478
                                                                                   },
                            't/simulator/900ca8317927b50dcac05e17bc33aa33.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 268
                                                                                   },
                            't/simulator/9bc7a811318444f451cb294a906cb433.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 365
                                                                                   },
                            't/simulator/a462aae2a270fc7e72eed97aecc65e6e.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 478
                                                                                   },
                            't/simulator/a5439a4fc2d686ec7a1e7a391bde49af.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 440
                                                                                   },
                            't/simulator/ad37aafc969ac5d936c359b4674862ac.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 427
                                                                                   },
                            't/simulator/b198251a91a65e89eafeb9f5fe9a8f40.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 3610
                                                                                   },
                            't/simulator/b1a1d249e95abed861cdb99d452ff1a7.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 478
                                                                                   },
                            't/simulator/b4d1522bea6dbd339ea2a04c1487be22.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 1626
                                                                                   },
                            't/simulator/bab18e00cb02c7e581b7f059c6cac810.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 1162
                                                                                   },
                            't/simulator/baccda8df4b1313033359e84b7e2f7d7.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 7330
                                                                                   },
                            't/simulator/bedf48911c566152c9af73670e9183a3.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 546
                                                                                   },
                            't/simulator/c3f8270aa594c3ac3ecf19835c4a252d.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 1658
                                                                                   },
                            't/simulator/c9d365dda9414c90db5565e3d88fa5e8.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 491
                                                                                   },
                            't/simulator/cc6640bda0105e9e7fbe830694d43182.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 611
                                                                                   },
                            't/simulator/cc85064d581f7e7614d8156c6f14b96a.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 1840
                                                                                   },
                            't/simulator/cfabc137884e853a99db17e0dc661a76.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 411
                                                                                   },
                            't/simulator/d2b770da4270c2bdc2461885c6ec8498.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 216
                                                                                   },
                            't/simulator/d6ed6e50d94cb2c035eb1d7fc82cf3a3.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 418
                                                                                   },
                            't/simulator/dbed469579eac391ac5e220ce2177634.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 167
                                                                                   },
                            't/simulator/dd0964d85a298c02b26989feed9ec37d.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 478
                                                                                   },
                            't/simulator/ddbc10fd5c1ee2ad98c67563ae1c01ad.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 260
                                                                                   },
                            't/simulator/e78931db236d261eb4a643b02b4f4f40.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 578
                                                                                   },
                            't/simulator/eb3a1958964aeb15063d12effc5aae84.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 602
                                                                                   },
                            't/simulator/ee0ae03c20b92e3bd7cf52af42b187dc.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 7215
                                                                                   },
                            't/simulator/f2b9cd1caf727069fcf4d783c052c9cf.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 478
                                                                                   },
                            't/simulator/f3e5fca687a369c9b5a99a2f08d91539.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 215
                                                                                   },
                            't/simulator/f61719f9841f68ab6580606fe575cc68.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 27
                                                                                   },
                            't/simulator/fbb1ee3afc0f7ab9d41ea738dcd68bd3.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 429
                                                                                   },
                            't/simulator/fca8d030f65653bbe39370d7fa3c12ae.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 611
                                                                                   },
                            't/simulator/ffe82ba4e04d84af8f74a4b2fc99ec99.json' => {
                                                                                     'mtime' => 1709469480,
                                                                                     'size' => 418
                                                                                   },
                            't/statement-result.t' => {
                                                        'mtime' => 1709469480,
                                                        'recommends' => {
                                                                          'Neo4j::Driver' => '0',
                                                                          'Neo4j_Test::MockHTTP' => '0',
                                                                          'Test::Exception' => '0',
                                                                          'Test::More' => '0.94',
                                                                          'Test::Warnings' => '0.010',
                                                                          'if' => '0'
                                                                        },
                                                        'requires' => {
                                                                        'Neo4j_Test' => '0',
                                                                        'lib' => '0',
                                                                        'strict' => '0',
                                                                        'warnings' => '0'
                                                                      },
                                                        'size' => 8924
                                                      },
                            't/summary.t' => {
                                               'mtime' => 1709469480,
                                               'recommends' => {
                                                                 'Neo4j::Driver' => '0',
                                                                 'Neo4j_Test::MockHTTP' => '0',
                                                                 'Test::Exception' => '0',
                                                                 'Test::More' => '0.94',
                                                                 'Test::Warnings' => '0.010',
                                                                 'if' => '0'
                                                               },
                                               'requires' => {
                                                               'Neo4j_Test' => '0',
                                                               'lib' => '0',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 5940
                                             },
                            't/transaction.t' => {
                                                   'mtime' => 1709469480,
                                                   'recommends' => {
                                                                     'Test::Exception' => '0',
                                                                     'Test::More' => '0.94',
                                                                     'Test::Warnings' => '0.010',
                                                                     'URI' => '0',
                                                                     'if' => '0'
                                                                   },
                                                   'requires' => {
                                                                   'Neo4j_Test' => '0',
                                                                   'lib' => '0',
                                                                   'strict' => '0',
                                                                   'warnings' => '0'
                                                                 },
                                                   'size' => 7727
                                                 },
                            't/tx-concurrent.t' => {
                                                     'mtime' => 1709469480,
                                                     'recommends' => {
                                                                       'Test::Warnings' => '0'
                                                                     },
                                                     'requires' => {
                                                                     'Neo4j_Test' => '0',
                                                                     'Neo4j_Test::MockHTTP' => '0',
                                                                     'Test::Exception' => '0',
                                                                     'Test::More' => '0.94',
                                                                     'Test::Warnings' => '0.010',
                                                                     'if' => '0',
                                                                     'lib' => '0',
                                                                     'parent' => '0',
                                                                     'strict' => '0',
                                                                     'warnings' => '0'
                                                                   },
                                                     'size' => 13449
                                                   },
                            't/types-spatial.t' => {
                                                     'mtime' => 1709469480,
                                                     'recommends' => {
                                                                       'Test::Warnings' => '0'
                                                                     },
                                                     'requires' => {
                                                                     'Neo4j::Driver' => '0',
                                                                     'Neo4j_Test::MockQuery' => '0',
                                                                     'Test::More' => '0.94',
                                                                     'Test::Neo4j::Types' => '0',
                                                                     'Test::Warnings' => '0.010',
                                                                     'if' => '0',
                                                                     'lib' => '0',
                                                                     'strict' => '0',
                                                                     'warnings' => '0'
                                                                   },
                                                     'size' => 2408
                                                   },
                            't/types-temporal.t' => {
                                                      'mtime' => 1709469480,
                                                      'recommends' => {
                                                                        'Test::More' => '0.88',
                                                                        'Test::Warnings' => '0'
                                                                      },
                                                      'requires' => {
                                                                      'Neo4j::Driver' => '0',
                                                                      'Neo4j::Types' => '0',
                                                                      'Neo4j_Test::MockQuery' => '0',
                                                                      'Test::More' => '0.94',
                                                                      'Test::Neo4j::Types' => '0.03',
                                                                      'Test::Warnings' => '0.010',
                                                                      'Time::Piece' => '0',
                                                                      'if' => '0',
                                                                      'lib' => '0',
                                                                      'strict' => '0',
                                                                      'warnings' => '0'
                                                                    },
                                                      'size' => 9646
                                                    },
                            't/types.t' => {
                                             'mtime' => 1709469480,
                                             'recommends' => {
                                                               'JSON::PP' => '0',
                                                               'Neo4j::Types' => '0',
                                                               'Test::Exception' => '0',
                                                               'Test::More' => '0.94',
                                                               'Test::Warnings' => '0.010',
                                                               'if' => '0'
                                                             },
                                             'requires' => {
                                                             'Neo4j_Test' => '0',
                                                             'lib' => '0',
                                                             'strict' => '0',
                                                             'warnings' => '0'
                                                           },
                                             'size' => 10171,
                                             'suggests' => {
                                                             'Neo4j::Bolt' => '0.4500'
                                                           }
                                           },
                            'xt/author/compatibility.t' => {
                                                             'mtime' => 1709469480,
                                                             'size' => 1501
                                                           },
                            'xt/author/cpan-distros.t' => {
                                                            'mtime' => 1709469480,
                                                            'size' => 3288
                                                          },
                            'xt/author/internals.t' => {
                                                         'mtime' => 1709469480,
                                                         'size' => 2843
                                                       },
                            'xt/author/minimum-version.t' => {
                                                               'mtime' => 1709469480,
                                                               'size' => 106
                                                             },
                            'xt/author/pod-syntax.t' => {
                                                          'mtime' => 1709469480,
                                                          'size' => 170
                                                        }
                          },
          'got_prereq_from' => 'META.yml',
          'ignored_files_array' => [
                                     't/lib/Neo4j_Test.pm',
                                     't/lib/Neo4j_Test/EchoHTTP.pm',
                                     't/lib/Neo4j_Test/MockHTTP.pm',
                                     't/lib/Neo4j_Test/MockQuery.pm',
                                     't/lib/Neo4j_Test/NetModulePlugin.pm',
                                     't/lib/Neo4j_Test/Sim.pm'
                                   ],
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 1,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 1,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 33,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 1,
                          'meta_yml_has_repository_resource' => 1,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 1,
                          'use_warnings' => 1
                        },
          'latest_mtime' => 1709469480,
          'license' => 'artistic_2 defined in META.yml defined in LICENSE',
          'license_file' => 'lib/Neo4j/Driver.pm,lib/Neo4j/Driver/Config.pod,lib/Neo4j/Driver/Deprecations.pod,lib/Neo4j/Driver/Net.pod,lib/Neo4j/Driver/Net/HTTP/LWP.pm,lib/Neo4j/Driver/Plugin.pm,lib/Neo4j/Driver/Record.pm,lib/Neo4j/Driver/Result.pm,lib/Neo4j/Driver/ResultSummary.pm,lib/Neo4j/Driver/ServerInfo.pm,lib/Neo4j/Driver/Session.pm,lib/Neo4j/Driver/StatementResult.pm,lib/Neo4j/Driver/SummaryCounters.pm,lib/Neo4j/Driver/Transaction.pm,lib/Neo4j/Driver/Type/Bytes.pm,lib/Neo4j/Driver/Type/Temporal.pm,lib/Neo4j/Driver/Types.pod',
          'license_from_yaml' => 'artistic_2',
          'license_in_pod' => 1,
          'license_type' => 'Artistic_2_0',
          'licenses' => {
                          'Artistic_2_0' => [
                                              'lib/Neo4j/Driver.pm',
                                              'lib/Neo4j/Driver/Config.pod',
                                              'lib/Neo4j/Driver/Deprecations.pod',
                                              'lib/Neo4j/Driver/Net.pod',
                                              'lib/Neo4j/Driver/Net/HTTP/LWP.pm',
                                              'lib/Neo4j/Driver/Plugin.pm',
                                              'lib/Neo4j/Driver/Record.pm',
                                              'lib/Neo4j/Driver/Result.pm',
                                              'lib/Neo4j/Driver/ResultSummary.pm',
                                              'lib/Neo4j/Driver/ServerInfo.pm',
                                              'lib/Neo4j/Driver/Session.pm',
                                              'lib/Neo4j/Driver/StatementResult.pm',
                                              'lib/Neo4j/Driver/SummaryCounters.pm',
                                              'lib/Neo4j/Driver/Transaction.pm',
                                              'lib/Neo4j/Driver/Type/Bytes.pm',
                                              'lib/Neo4j/Driver/Type/Temporal.pm',
                                              'lib/Neo4j/Driver/Types.pod'
                                            ]
                        },
          'manifest_matches_dist' => 1,
          'meta_json' => {
                           'abstract' => 'Neo4j community graph database driver for Bolt and HTTP',
                           'author' => [
                                         'Arne Johannessen <ajnn@cpan.org>'
                                       ],
                           'dynamic_config' => 0,
                           'generated_by' => 'Dist::Zilla version 6.031, CPAN::Meta::Converter version 2.150010',
                           'license' => [
                                          'artistic_2'
                                        ],
                           'meta-spec' => {
                                            'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                            'version' => 2
                                          },
                           'name' => 'Neo4j-Driver',
                           'no_index' => {
                                           'directory' => [
                                                            't/lib'
                                                          ]
                                         },
                           'prereqs' => {
                                          'configure' => {
                                                           'requires' => {
                                                                           'ExtUtils::MakeMaker' => '0'
                                                                         }
                                                         },
                                          'develop' => {
                                                         'recommends' => {
                                                                           'Path::Tiny' => '0.011'
                                                                         },
                                                         'requires' => {
                                                                         'Test::Exception' => '0',
                                                                         'Test::MinimumVersion' => '0',
                                                                         'Test::More' => '0.94',
                                                                         'Test::Pod' => '1.41',
                                                                         'Test::Warnings' => '0.010'
                                                                       },
                                                         'suggests' => {
                                                                         'Neo4j::Cypher::Abstract' => '0',
                                                                         'REST::Neo4p' => '0'
                                                                       }
                                                       },
                                          'runtime' => {
                                                         'recommends' => {
                                                                           'Neo4j::Types' => '2.00'
                                                                         },
                                                         'requires' => {
                                                                         'JSON::MaybeXS' => '1.003003',
                                                                         'LWP::UserAgent' => '6.04',
                                                                         'Neo4j::Error' => '0',
                                                                         'Neo4j::Types' => '1.00',
                                                                         'Time::Piece' => '1.20',
                                                                         'Try::Tiny' => '0',
                                                                         'URI' => '1.31',
                                                                         'parent' => '0',
                                                                         'perl' => '5.010'
                                                                       },
                                                         'suggests' => {
                                                                         'Cpanel::JSON::XS' => '4.38',
                                                                         'JSON::PP' => '4.11',
                                                                         'LWP::Protocol::https' => '0',
                                                                         'Neo4j::Bolt' => '0.02'
                                                                       }
                                                       },
                                          'test' => {
                                                      'requires' => {
                                                                      'HTTP::Headers' => '0',
                                                                      'HTTP::Response' => '0',
                                                                      'Mock::Quick' => '0',
                                                                      'Neo4j::Types' => '1.00',
                                                                      'Test::Exception' => '0',
                                                                      'Test::More' => '0.94',
                                                                      'Test::Neo4j::Types' => '0.03',
                                                                      'Test::Warnings' => '0.010'
                                                                    }
                                                    }
                                        },
                           'provides' => {
                                           'Neo4j::Driver' => {
                                                                'file' => 'lib/Neo4j/Driver.pm',
                                                                'version' => '0.46'
                                                              },
                                           'Neo4j::Driver::Events' => {
                                                                        'file' => 'lib/Neo4j/Driver/Events.pm',
                                                                        'version' => '0.46'
                                                                      },
                                           'Neo4j::Driver::Net::Bolt' => {
                                                                           'file' => 'lib/Neo4j/Driver/Net/Bolt.pm',
                                                                           'version' => '0.46'
                                                                         },
                                           'Neo4j::Driver::Net::HTTP' => {
                                                                           'file' => 'lib/Neo4j/Driver/Net/HTTP.pm',
                                                                           'version' => '0.46'
                                                                         },
                                           'Neo4j::Driver::Net::HTTP::LWP' => {
                                                                                'file' => 'lib/Neo4j/Driver/Net/HTTP/LWP.pm',
                                                                                'version' => '0.46'
                                                                              },
                                           'Neo4j::Driver::Plugin' => {
                                                                        'file' => 'lib/Neo4j/Driver/Plugin.pm',
                                                                        'version' => '0.46'
                                                                      },
                                           'Neo4j::Driver::Record' => {
                                                                        'file' => 'lib/Neo4j/Driver/Record.pm',
                                                                        'version' => '0.46'
                                                                      },
                                           'Neo4j::Driver::Result' => {
                                                                        'file' => 'lib/Neo4j/Driver/Result.pm',
                                                                        'version' => '0.46'
                                                                      },
                                           'Neo4j::Driver::Result::Bolt' => {
                                                                              'file' => 'lib/Neo4j/Driver/Result/Bolt.pm',
                                                                              'version' => '0.46'
                                                                            },
                                           'Neo4j::Driver::Result::JSON' => {
                                                                              'file' => 'lib/Neo4j/Driver/Result/JSON.pm',
                                                                              'version' => '0.46'
                                                                            },
                                           'Neo4j::Driver::Result::Jolt' => {
                                                                              'file' => 'lib/Neo4j/Driver/Result/Jolt.pm',
                                                                              'version' => '0.46'
                                                                            },
                                           'Neo4j::Driver::Result::Text' => {
                                                                              'file' => 'lib/Neo4j/Driver/Result/Text.pm',
                                                                              'version' => '0.46'
                                                                            },
                                           'Neo4j::Driver::ResultColumns' => {
                                                                               'file' => 'lib/Neo4j/Driver/ResultColumns.pm',
                                                                               'version' => '0.46'
                                                                             },
                                           'Neo4j::Driver::ResultSummary' => {
                                                                               'file' => 'lib/Neo4j/Driver/ResultSummary.pm',
                                                                               'version' => '0.46'
                                                                             },
                                           'Neo4j::Driver::ServerInfo' => {
                                                                            'file' => 'lib/Neo4j/Driver/ServerInfo.pm',
                                                                            'version' => '0.46'
                                                                          },
                                           'Neo4j::Driver::Session' => {
                                                                         'file' => 'lib/Neo4j/Driver/Session.pm',
                                                                         'version' => '0.46'
                                                                       },
                                           'Neo4j::Driver::StatementResult' => {
                                                                                 'file' => 'lib/Neo4j/Driver/StatementResult.pm',
                                                                                 'version' => '0.46'
                                                                               },
                                           'Neo4j::Driver::SummaryCounters' => {
                                                                                 'file' => 'lib/Neo4j/Driver/SummaryCounters.pm',
                                                                                 'version' => '0.46'
                                                                               },
                                           'Neo4j::Driver::Transaction' => {
                                                                             'file' => 'lib/Neo4j/Driver/Transaction.pm',
                                                                             'version' => '0.46'
                                                                           },
                                           'Neo4j::Driver::Type::Bytes' => {
                                                                             'file' => 'lib/Neo4j/Driver/Type/Bytes.pm',
                                                                             'version' => '0.46'
                                                                           },
                                           'Neo4j::Driver::Type::DateTime' => {
                                                                                'file' => 'lib/Neo4j/Driver/Type/DateTime.pm',
                                                                                'version' => '0.46'
                                                                              },
                                           'Neo4j::Driver::Type::Duration' => {
                                                                                'file' => 'lib/Neo4j/Driver/Type/Duration.pm',
                                                                                'version' => '0.46'
                                                                              },
                                           'Neo4j::Driver::Type::Node' => {
                                                                            'file' => 'lib/Neo4j/Driver/Type/Node.pm',
                                                                            'version' => '0.46'
                                                                          },
                                           'Neo4j::Driver::Type::Path' => {
                                                                            'file' => 'lib/Neo4j/Driver/Type/Path.pm',
                                                                            'version' => '0.46'
                                                                          },
                                           'Neo4j::Driver::Type::Point' => {
                                                                             'file' => 'lib/Neo4j/Driver/Type/Point.pm',
                                                                             'version' => '0.46'
                                                                           },
                                           'Neo4j::Driver::Type::Relationship' => {
                                                                                    'file' => 'lib/Neo4j/Driver/Type/Relationship.pm',
                                                                                    'version' => '0.46'
                                                                                  },
                                           'Neo4j::Driver::Type::Temporal' => {
                                                                                'file' => 'lib/Neo4j/Driver/Type/Temporal.pm',
                                                                                'version' => '0.46'
                                                                              },
                                           'URI::neo4j' => {
                                                             'file' => 'lib/URI/neo4j.pm',
                                                             'version' => '0.46'
                                                           }
                                         },
                           'release_status' => 'stable',
                           'resources' => {
                                            'bugtracker' => {
                                                              'web' => 'https://github.com/johannessen/neo4j-driver-perl/issues'
                                                            },
                                            'repository' => {
                                                              'type' => 'git',
                                                              'url' => 'https://github.com/johannessen/neo4j-driver-perl.git',
                                                              'web' => 'https://github.com/johannessen/neo4j-driver-perl'
                                                            }
                                          },
                           'version' => '0.46',
                           'x_generated_by_perl' => 'v5.38.0',
                           'x_serialization_backend' => 'Cpanel::JSON::XS version 4.37',
                           'x_spdx_expression' => 'Artistic-2.0'
                         },
          'meta_json_is_parsable' => 1,
          'meta_json_spec_version' => 2,
          'meta_yml' => {
                          'abstract' => 'Neo4j community graph database driver for Bolt and HTTP',
                          'author' => [
                                        'Arne Johannessen <ajnn@cpan.org>'
                                      ],
                          'build_requires' => {
                                                'HTTP::Headers' => '0',
                                                'HTTP::Response' => '0',
                                                'Mock::Quick' => '0',
                                                'Neo4j::Types' => '1.00',
                                                'Test::Exception' => '0',
                                                'Test::More' => '0.94',
                                                'Test::Neo4j::Types' => '0.03',
                                                'Test::Warnings' => '0.010'
                                              },
                          'configure_requires' => {
                                                    'ExtUtils::MakeMaker' => '0'
                                                  },
                          'dynamic_config' => '0',
                          'generated_by' => 'Dist::Zilla version 6.031, CPAN::Meta::Converter version 2.150010',
                          'license' => 'artistic_2',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'Neo4j-Driver',
                          'no_index' => {
                                          'directory' => [
                                                           't/lib'
                                                         ]
                                        },
                          'provides' => {
                                          'Neo4j::Driver' => {
                                                               'file' => 'lib/Neo4j/Driver.pm',
                                                               'version' => '0.46'
                                                             },
                                          'Neo4j::Driver::Events' => {
                                                                       'file' => 'lib/Neo4j/Driver/Events.pm',
                                                                       'version' => '0.46'
                                                                     },
                                          'Neo4j::Driver::Net::Bolt' => {
                                                                          'file' => 'lib/Neo4j/Driver/Net/Bolt.pm',
                                                                          'version' => '0.46'
                                                                        },
                                          'Neo4j::Driver::Net::HTTP' => {
                                                                          'file' => 'lib/Neo4j/Driver/Net/HTTP.pm',
                                                                          'version' => '0.46'
                                                                        },
                                          'Neo4j::Driver::Net::HTTP::LWP' => {
                                                                               'file' => 'lib/Neo4j/Driver/Net/HTTP/LWP.pm',
                                                                               'version' => '0.46'
                                                                             },
                                          'Neo4j::Driver::Plugin' => {
                                                                       'file' => 'lib/Neo4j/Driver/Plugin.pm',
                                                                       'version' => '0.46'
                                                                     },
                                          'Neo4j::Driver::Record' => {
                                                                       'file' => 'lib/Neo4j/Driver/Record.pm',
                                                                       'version' => '0.46'
                                                                     },
                                          'Neo4j::Driver::Result' => {
                                                                       'file' => 'lib/Neo4j/Driver/Result.pm',
                                                                       'version' => '0.46'
                                                                     },
                                          'Neo4j::Driver::Result::Bolt' => {
                                                                             'file' => 'lib/Neo4j/Driver/Result/Bolt.pm',
                                                                             'version' => '0.46'
                                                                           },
                                          'Neo4j::Driver::Result::JSON' => {
                                                                             'file' => 'lib/Neo4j/Driver/Result/JSON.pm',
                                                                             'version' => '0.46'
                                                                           },
                                          'Neo4j::Driver::Result::Jolt' => {
                                                                             'file' => 'lib/Neo4j/Driver/Result/Jolt.pm',
                                                                             'version' => '0.46'
                                                                           },
                                          'Neo4j::Driver::Result::Text' => {
                                                                             'file' => 'lib/Neo4j/Driver/Result/Text.pm',
                                                                             'version' => '0.46'
                                                                           },
                                          'Neo4j::Driver::ResultColumns' => {
                                                                              'file' => 'lib/Neo4j/Driver/ResultColumns.pm',
                                                                              'version' => '0.46'
                                                                            },
                                          'Neo4j::Driver::ResultSummary' => {
                                                                              'file' => 'lib/Neo4j/Driver/ResultSummary.pm',
                                                                              'version' => '0.46'
                                                                            },
                                          'Neo4j::Driver::ServerInfo' => {
                                                                           'file' => 'lib/Neo4j/Driver/ServerInfo.pm',
                                                                           'version' => '0.46'
                                                                         },
                                          'Neo4j::Driver::Session' => {
                                                                        'file' => 'lib/Neo4j/Driver/Session.pm',
                                                                        'version' => '0.46'
                                                                      },
                                          'Neo4j::Driver::StatementResult' => {
                                                                                'file' => 'lib/Neo4j/Driver/StatementResult.pm',
                                                                                'version' => '0.46'
                                                                              },
                                          'Neo4j::Driver::SummaryCounters' => {
                                                                                'file' => 'lib/Neo4j/Driver/SummaryCounters.pm',
                                                                                'version' => '0.46'
                                                                              },
                                          'Neo4j::Driver::Transaction' => {
                                                                            'file' => 'lib/Neo4j/Driver/Transaction.pm',
                                                                            'version' => '0.46'
                                                                          },
                                          'Neo4j::Driver::Type::Bytes' => {
                                                                            'file' => 'lib/Neo4j/Driver/Type/Bytes.pm',
                                                                            'version' => '0.46'
                                                                          },
                                          'Neo4j::Driver::Type::DateTime' => {
                                                                               'file' => 'lib/Neo4j/Driver/Type/DateTime.pm',
                                                                               'version' => '0.46'
                                                                             },
                                          'Neo4j::Driver::Type::Duration' => {
                                                                               'file' => 'lib/Neo4j/Driver/Type/Duration.pm',
                                                                               'version' => '0.46'
                                                                             },
                                          'Neo4j::Driver::Type::Node' => {
                                                                           'file' => 'lib/Neo4j/Driver/Type/Node.pm',
                                                                           'version' => '0.46'
                                                                         },
                                          'Neo4j::Driver::Type::Path' => {
                                                                           'file' => 'lib/Neo4j/Driver/Type/Path.pm',
                                                                           'version' => '0.46'
                                                                         },
                                          'Neo4j::Driver::Type::Point' => {
                                                                            'file' => 'lib/Neo4j/Driver/Type/Point.pm',
                                                                            'version' => '0.46'
                                                                          },
                                          'Neo4j::Driver::Type::Relationship' => {
                                                                                   'file' => 'lib/Neo4j/Driver/Type/Relationship.pm',
                                                                                   'version' => '0.46'
                                                                                 },
                                          'Neo4j::Driver::Type::Temporal' => {
                                                                               'file' => 'lib/Neo4j/Driver/Type/Temporal.pm',
                                                                               'version' => '0.46'
                                                                             },
                                          'URI::neo4j' => {
                                                            'file' => 'lib/URI/neo4j.pm',
                                                            'version' => '0.46'
                                                          }
                                        },
                          'recommends' => {
                                            'Neo4j::Types' => '2.00'
                                          },
                          'requires' => {
                                          'JSON::MaybeXS' => '1.003003',
                                          'LWP::UserAgent' => '6.04',
                                          'Neo4j::Error' => '0',
                                          'Neo4j::Types' => '1.00',
                                          'Time::Piece' => '1.20',
                                          'Try::Tiny' => '0',
                                          'URI' => '1.31',
                                          'parent' => '0',
                                          'perl' => '5.010'
                                        },
                          'resources' => {
                                           'bugtracker' => 'https://github.com/johannessen/neo4j-driver-perl/issues',
                                           'repository' => 'https://github.com/johannessen/neo4j-driver-perl.git'
                                         },
                          'version' => '0.46',
                          'x_generated_by_perl' => 'v5.38.0',
                          'x_serialization_backend' => 'YAML::Tiny version 1.74',
                          'x_spdx_expression' => 'Artistic-2.0'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/Neo4j/Driver.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver'
                         },
                         {
                           'file' => 'lib/Neo4j/Driver/Events.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver::Events'
                         },
                         {
                           'file' => 'lib/Neo4j/Driver/Net/Bolt.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver::Net::Bolt'
                         },
                         {
                           'file' => 'lib/Neo4j/Driver/Net/HTTP.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver::Net::HTTP'
                         },
                         {
                           'file' => 'lib/Neo4j/Driver/Net/HTTP/LWP.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver::Net::HTTP::LWP'
                         },
                         {
                           'file' => 'lib/Neo4j/Driver/Plugin.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver::Plugin'
                         },
                         {
                           'file' => 'lib/Neo4j/Driver/Record.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver::Record'
                         },
                         {
                           'file' => 'lib/Neo4j/Driver/Result.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver::Result'
                         },
                         {
                           'file' => 'lib/Neo4j/Driver/Result/Bolt.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver::Result::Bolt'
                         },
                         {
                           'file' => 'lib/Neo4j/Driver/Result/JSON.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver::Result::JSON'
                         },
                         {
                           'file' => 'lib/Neo4j/Driver/Result/Jolt.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver::Result::Jolt'
                         },
                         {
                           'file' => 'lib/Neo4j/Driver/Result/Text.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver::Result::Text'
                         },
                         {
                           'file' => 'lib/Neo4j/Driver/ResultColumns.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver::ResultColumns'
                         },
                         {
                           'file' => 'lib/Neo4j/Driver/ResultSummary.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver::ResultSummary'
                         },
                         {
                           'file' => 'lib/Neo4j/Driver/ServerInfo.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver::ServerInfo'
                         },
                         {
                           'file' => 'lib/Neo4j/Driver/Session.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver::Session'
                         },
                         {
                           'file' => 'lib/Neo4j/Driver/StatementResult.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver::StatementResult'
                         },
                         {
                           'file' => 'lib/Neo4j/Driver/SummaryCounters.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver::SummaryCounters'
                         },
                         {
                           'file' => 'lib/Neo4j/Driver/Transaction.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver::Transaction'
                         },
                         {
                           'file' => 'lib/Neo4j/Driver/Type/Bytes.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver::Type::Bytes'
                         },
                         {
                           'file' => 'lib/Neo4j/Driver/Type/DateTime.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver::Type::DateTime'
                         },
                         {
                           'file' => 'lib/Neo4j/Driver/Type/Duration.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver::Type::Duration'
                         },
                         {
                           'file' => 'lib/Neo4j/Driver/Type/Node.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver::Type::Node'
                         },
                         {
                           'file' => 'lib/Neo4j/Driver/Type/Path.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver::Type::Path'
                         },
                         {
                           'file' => 'lib/Neo4j/Driver/Type/Point.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver::Type::Point'
                         },
                         {
                           'file' => 'lib/Neo4j/Driver/Type/Relationship.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver::Type::Relationship'
                         },
                         {
                           'file' => 'lib/Neo4j/Driver/Type/Temporal.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Neo4j::Driver::Type::Temporal'
                         },
                         {
                           'file' => 'lib/URI/neo4j.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'URI::neo4j'
                         }
                       ],
          'no_index' => '^t/lib/',
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/Neo4j-Driver.tar.gz',
          'prereq' => [
                        {
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'HTTP::Headers',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'HTTP::Response',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'JSON::MaybeXS',
                          'type' => 'runtime_requires',
                          'version' => '1.003003'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'LWP::UserAgent',
                          'type' => 'runtime_requires',
                          'version' => '6.04'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Mock::Quick',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Neo4j::Error',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Neo4j::Types',
                          'type' => 'build_requires',
                          'version' => '1.00'
                        },
                        {
                          'is_optional_prereq' => 1,
                          'requires' => 'Neo4j::Types',
                          'type' => 'runtime_recommends',
                          'version' => '2.00'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Neo4j::Types',
                          'type' => 'runtime_requires',
                          'version' => '1.00'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::Exception',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'build_requires',
                          'version' => '0.94'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::Neo4j::Types',
                          'type' => 'build_requires',
                          'version' => '0.03'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::Warnings',
                          'type' => 'build_requires',
                          'version' => '0.010'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Time::Piece',
                          'type' => 'runtime_requires',
                          'version' => '1.20'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Try::Tiny',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'URI',
                          'type' => 'runtime_requires',
                          'version' => '1.31'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'parent',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => '5.010'
                        }
                      ],
          'released' => 1709583462,
          'size_packed' => 128660,
          'size_unpacked' => 519131,
          'test_files' => [
                            't/bolt.t',
                            't/config.t',
                            't/deprecated.t',
                            't/experimental.t',
                            't/info.t',
                            't/jolt-mime.t',
                            't/jolt-types.t',
                            't/json-utf8.t',
                            't/net-lwp.t',
                            't/plugins.t',
                            't/record-ambiguous.t',
                            't/record.t',
                            't/session-execute.t',
                            't/session.t',
                            't/statement-result.t',
                            't/summary.t',
                            't/transaction.t',
                            't/tx-concurrent.t',
                            't/types-spatial.t',
                            't/types-temporal.t',
                            't/types.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'requires' => {
                                                       'ExtUtils::MakeMaker' => '0',
                                                       'perl' => '5.010',
                                                       'strict' => '0',
                                                       'warnings' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'noes' => {
                                                 'if' => '0',
                                                 'strict' => '0',
                                                 'warnings' => '0'
                                               },
                                     'recommends' => {
                                                       'JSON::PP' => '0',
                                                       'Time::Piece' => '0'
                                                     },
                                     'requires' => {
                                                     'Carp' => '0',
                                                     'JSON::MaybeXS' => '1.003003',
                                                     'LWP::UserAgent' => '6.04',
                                                     'List::Util' => '0',
                                                     'Neo4j::Error' => '0',
                                                     'Neo4j::Types::DateTime' => '0',
                                                     'Neo4j::Types::Duration' => '0',
                                                     'Neo4j::Types::Node' => '0',
                                                     'Neo4j::Types::Path' => '0',
                                                     'Neo4j::Types::Point' => '0',
                                                     'Neo4j::Types::Relationship' => '0',
                                                     'Scalar::Util' => '0',
                                                     'Time::HiRes' => '0',
                                                     'Time::Piece' => '1.20',
                                                     'Try::Tiny' => '0',
                                                     'URI' => '1.31',
                                                     'URI::_server' => '0',
                                                     'overload' => '0',
                                                     'parent' => '0',
                                                     'perl' => '5.010',
                                                     'strict' => '0',
                                                     'utf8' => '0',
                                                     'warnings' => '0',
                                                     'warnings::register' => '0'
                                                   },
                                     'suggests' => {
                                                     'Neo4j::Bolt' => '0'
                                                   }
                                   },
                      'test' => {
                                  'noes' => {
                                              'if' => '0',
                                              'warnings' => '0'
                                            },
                                  'recommends' => {
                                                    'JSON::PP' => '0',
                                                    'Neo4j::Types' => '0',
                                                    'Scalar::Util' => '0',
                                                    'Test::Exception' => '0',
                                                    'Test::More' => '0.94',
                                                    'Test::Warnings' => '0.010',
                                                    'URI' => '0',
                                                    'if' => '0',
                                                    'parent' => '0'
                                                  },
                                  'requires' => {
                                                  'HTTP::Headers' => '0',
                                                  'HTTP::Response' => '0',
                                                  'JSON::MaybeXS' => '0',
                                                  'JSON::PP' => '0',
                                                  'List::Util' => '0',
                                                  'Mock::Quick' => '0',
                                                  'Neo4j::Error' => '0',
                                                  'Neo4j::Types' => '0',
                                                  'Scalar::Util' => '0',
                                                  'Test::Exception' => '0',
                                                  'Test::More' => '0.94',
                                                  'Test::Neo4j::Types' => '0.03',
                                                  'Test::Warnings' => '0.010',
                                                  'Time::HiRes' => '0',
                                                  'Time::Piece' => '0',
                                                  'Try::Tiny' => '0',
                                                  'URI' => '0',
                                                  'if' => '0',
                                                  'lib' => '0',
                                                  'parent' => '0',
                                                  'strict' => '0',
                                                  'utf8' => '0',
                                                  'warnings' => '0'
                                                },
                                  'suggests' => {
                                                  'LWP::Protocol::https' => '0',
                                                  'Mozilla::CA' => '0',
                                                  'Neo4j::Bolt' => '0.4500'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'Neo4j-Driver'
        };
1;
