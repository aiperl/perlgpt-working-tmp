$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'MarpaX::ESLIF' => 'ESLIF is Extended ScanLess InterFace',
                                  'MarpaX::ESLIF::BNF' => 'MarpaX::ESLIF\'s BNF',
                                  'MarpaX::ESLIF::Base' => 'ESLIF base',
                                  'MarpaX::ESLIF::Bindings' => 'MarpaX::ESLIF\'s Bindings',
                                  'MarpaX::ESLIF::Event::Type' => 'ESLIF Event Types',
                                  'MarpaX::ESLIF::Grammar' => 'MarpaX::ESLIF\'s grammar',
                                  'MarpaX::ESLIF::Grammar::Properties' => 'ESLIF Grammar Properties',
                                  'MarpaX::ESLIF::Grammar::Rule::Properties' => 'ESLIF Grammar Rule Properties',
                                  'MarpaX::ESLIF::Grammar::Symbol::Properties' => 'ESLIF Grammar Symbol Properties',
                                  'MarpaX::ESLIF::Introduction' => 'MarpaX::ESLIF\'s Introduction',
                                  'MarpaX::ESLIF::JSON' => 'ESLIF\'s JSON interface',
                                  'MarpaX::ESLIF::JSON::Decoder' => 'ESLIF\'s JSON decoder interface',
                                  'MarpaX::ESLIF::JSON::Decoder::RecognizerInterface' => 'MarpaX::ESLIF::JSON Recognizer Interface',
                                  'MarpaX::ESLIF::JSON::Encoder' => 'ESLIF\'s JSON encoder interface',
                                  'MarpaX::ESLIF::Logger::Interface' => 'MarpaX::ESLIF\'s logger interface',
                                  'MarpaX::ESLIF::Logger::Level' => 'ESLIF Logger levels',
                                  'MarpaX::ESLIF::Recognizer' => 'MarpaX::ESLIF\'s recognizer',
                                  'MarpaX::ESLIF::Recognizer::Interface' => 'MarpaX::ESLIF\'s recognizer interface',
                                  'MarpaX::ESLIF::RegexCallout' => 'ESLIF Regex Callout',
                                  'MarpaX::ESLIF::Rule::PropertyBitSet' => 'ESLIF Rule Property Bit Set',
                                  'MarpaX::ESLIF::String' => 'ESLIF String is any string value with encoding attribute',
                                  'MarpaX::ESLIF::Symbol' => 'MarpaX::ESLIF\'s symbol',
                                  'MarpaX::ESLIF::Symbol::EventBitSet' => 'ESLIF Symbol Event Bit Set',
                                  'MarpaX::ESLIF::Symbol::PropertyBitSet' => 'ESLIF Symbol Property Bit Set',
                                  'MarpaX::ESLIF::Symbol::Type' => 'ESLIF Symbol Type',
                                  'MarpaX::ESLIF::Tutorial::Calculator' => 'MarpaX::ESLIF Calculator Tutorial',
                                  'MarpaX::ESLIF::Value' => 'MarpaX::ESLIF\'s value',
                                  'MarpaX::ESLIF::Value::Interface' => 'MarpaX::ESLIF\'s value interface',
                                  'MarpaX::ESLIF::Value::Type' => 'ESLIF Value Types'
                                },
          'author' => '',
          'dir_lib' => 'lib',
          'dir_t' => 't',
          'dir_xt' => 'xt',
          'dirs' => 23,
          'dirs_array' => [
                            'etc/tarballs',
                            'etc',
                            'inc/ExtUtils',
                            'inc',
                            'lib/MarpaX/ESLIF/Event',
                            'lib/MarpaX/ESLIF/Grammar/Rule',
                            'lib/MarpaX/ESLIF/Grammar/Symbol',
                            'lib/MarpaX/ESLIF/Grammar',
                            'lib/MarpaX/ESLIF/JSON/Decoder',
                            'lib/MarpaX/ESLIF/JSON',
                            'lib/MarpaX/ESLIF/Logger',
                            'lib/MarpaX/ESLIF/Recognizer',
                            'lib/MarpaX/ESLIF/Rule',
                            'lib/MarpaX/ESLIF/Symbol',
                            'lib/MarpaX/ESLIF/Tutorial',
                            'lib/MarpaX/ESLIF/Value',
                            'lib/MarpaX/ESLIF',
                            'lib/MarpaX',
                            'lib',
                            't',
                            'xt/author',
                            'xt/release',
                            'xt'
                          ],
          'dist' => 'MarpaX-ESLIF',
          'dynamic_config' => 1,
          'error' => {
                       'extracts_nicely' => 'expected MarpaX-ESLIF but got MarpaX-ESLIF-6.0.33.4'
                     },
          'extension' => 'tar.gz',
          'external_license_file' => 'LICENSE',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_changelog' => 'Changes',
          'file_cpanfile' => 'cpanfile',
          'file_dist_ini' => 'dist.ini',
          'file_license' => 'LICENSE',
          'file_makefile_pl' => 'Makefile.PL',
          'file_manifest' => 'MANIFEST',
          'file_meta_json' => 'META.json',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README.txt',
          'files' => 56,
          'files_array' => [
                             '.perlcriticrc',
                             'Changes',
                             'ESLIF.xs',
                             'LICENSE',
                             'MANIFEST',
                             'META.json',
                             'META.yml',
                             'Makefile.PL',
                             'README.txt',
                             'cpanfile',
                             'dist.ini',
                             'etc/compile_marpaESLIF.pl',
                             'etc/create_tar_gz.pl',
                             'etc/init_pause.pl',
                             'etc/inttypes.h.in',
                             'etc/stdint.h.in',
                             'etc/tarballs/dlfcn_win32-src.tar.gz',
                             'etc/tarballs/generichash-src.tar.gz',
                             'etc/tarballs/genericlogger-src.tar.gz',
                             'etc/tarballs/genericsparsearray-src.tar.gz',
                             'etc/tarballs/genericstack-src.tar.gz',
                             'etc/tarballs/luaunpanic-src.tar.gz',
                             'etc/tarballs/marpaeslif-src.tar.gz',
                             'etc/tarballs/marpaesliflua-src.tar.gz',
                             'etc/tarballs/marpawrapper-src.tar.gz',
                             'etc/tarballs/tconv-src.tar.gz',
                             'lib/MarpaX/ESLIF/BNF.pod',
                             'lib/MarpaX/ESLIF/Base.pm',
                             'lib/MarpaX/ESLIF/Bindings.pod',
                             'lib/MarpaX/ESLIF/Event/Type.pm',
                             'lib/MarpaX/ESLIF/Grammar/Properties.pm',
                             'lib/MarpaX/ESLIF/Grammar/Rule/Properties.pm',
                             'lib/MarpaX/ESLIF/Grammar/Symbol/Properties.pm',
                             'lib/MarpaX/ESLIF/Grammar.pm',
                             'lib/MarpaX/ESLIF/Introduction.pod',
                             'lib/MarpaX/ESLIF/JSON/Decoder/RecognizerInterface.pm',
                             'lib/MarpaX/ESLIF/JSON/Decoder.pm',
                             'lib/MarpaX/ESLIF/JSON/Encoder.pm',
                             'lib/MarpaX/ESLIF/JSON.pm',
                             'lib/MarpaX/ESLIF/Logger/Interface.pod',
                             'lib/MarpaX/ESLIF/Logger/Level.pm',
                             'lib/MarpaX/ESLIF/Recognizer/Interface.pod',
                             'lib/MarpaX/ESLIF/Recognizer.pm',
                             'lib/MarpaX/ESLIF/RegexCallout.pm',
                             'lib/MarpaX/ESLIF/Rule/PropertyBitSet.pm',
                             'lib/MarpaX/ESLIF/String.pm',
                             'lib/MarpaX/ESLIF/Symbol/EventBitSet.pm',
                             'lib/MarpaX/ESLIF/Symbol/PropertyBitSet.pm',
                             'lib/MarpaX/ESLIF/Symbol/Type.pm',
                             'lib/MarpaX/ESLIF/Symbol.pm',
                             'lib/MarpaX/ESLIF/Tutorial/Calculator.pod',
                             'lib/MarpaX/ESLIF/Value/Interface.pod',
                             'lib/MarpaX/ESLIF/Value/Type.pm',
                             'lib/MarpaX/ESLIF/Value.pm',
                             'lib/MarpaX/ESLIF.pm',
                             'ppport.h'
                           ],
          'files_hash' => {
                            '.perlcriticrc' => {
                                                 'mtime' => 1709273152,
                                                 'size' => 53
                                               },
                            'Changes' => {
                                           'mtime' => 1709273150,
                                           'size' => 7360
                                         },
                            'ESLIF.xs' => {
                                            'mtime' => 1709273150,
                                            'size' => 297012
                                          },
                            'LICENSE' => {
                                           'mtime' => 1709273150,
                                           'size' => 18362
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1709273152,
                                            'size' => 2180
                                          },
                            'META.json' => {
                                             'mtime' => 1709273152,
                                             'size' => 8895
                                           },
                            'META.yml' => {
                                            'mtime' => 1709273152,
                                            'size' => 4080
                                          },
                            'Makefile.PL' => {
                                               'mtime' => 1709273152,
                                               'requires' => {
                                                               'Config' => '0',
                                                               'ExtUtils::Constant' => '0',
                                                               'ExtUtils::MakeMaker' => '0',
                                                               'File::Find' => '0',
                                                               'File::Spec' => '0',
                                                               'IO::Handle' => '0',
                                                               'Probe::Perl' => '0',
                                                               'Try::Tiny' => '0',
                                                               'perl' => '5.010',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 9236
                                             },
                            'README.txt' => {
                                              'mtime' => 1709273152,
                                              'size' => 1131
                                            },
                            'cpanfile' => {
                                            'mtime' => 1709273150,
                                            'size' => 4523
                                          },
                            'dist.ini' => {
                                            'mtime' => 1709273151,
                                            'size' => 15390
                                          },
                            'etc/compile_marpaESLIF.pl' => {
                                                             'mtime' => 1709273152,
                                                             'size' => 107153
                                                           },
                            'etc/create_tar_gz.pl' => {
                                                        'mtime' => 1709273152,
                                                        'size' => 1168
                                                      },
                            'etc/init_pause.pl' => {
                                                     'mtime' => 1709273152,
                                                     'size' => 462
                                                   },
                            'etc/inttypes.h.in' => {
                                                     'mtime' => 1709273152,
                                                     'size' => 5862
                                                   },
                            'etc/stdint.h.in' => {
                                                   'mtime' => 1709273152,
                                                   'size' => 10766
                                                 },
                            'etc/tarballs/dlfcn_win32-src.tar.gz' => {
                                                                       'mtime' => 1709273163,
                                                                       'size' => 21702
                                                                     },
                            'etc/tarballs/generichash-src.tar.gz' => {
                                                                       'mtime' => 1709273163,
                                                                       'size' => 10877
                                                                     },
                            'etc/tarballs/genericlogger-src.tar.gz' => {
                                                                         'mtime' => 1709273163,
                                                                         'size' => 11830
                                                                       },
                            'etc/tarballs/genericsparsearray-src.tar.gz' => {
                                                                              'mtime' => 1709273163,
                                                                              'size' => 5405
                                                                            },
                            'etc/tarballs/genericstack-src.tar.gz' => {
                                                                        'mtime' => 1709273163,
                                                                        'size' => 13911
                                                                      },
                            'etc/tarballs/luaunpanic-src.tar.gz' => {
                                                                      'mtime' => 1709273160,
                                                                      'size' => 317416
                                                                    },
                            'etc/tarballs/marpaeslif-src.tar.gz' => {
                                                                      'mtime' => 1709273163,
                                                                      'size' => 5360879
                                                                    },
                            'etc/tarballs/marpaesliflua-src.tar.gz' => {
                                                                         'mtime' => 1709273163,
                                                                         'size' => 997653
                                                                       },
                            'etc/tarballs/marpawrapper-src.tar.gz' => {
                                                                        'mtime' => 1709273163,
                                                                        'size' => 432496
                                                                      },
                            'etc/tarballs/tconv-src.tar.gz' => {
                                                                 'mtime' => 1709273158,
                                                                 'size' => 6133425
                                                               },
                            'inc/ExtUtils/CppGuess.pm' => {
                                                            'mtime' => 1709273152,
                                                            'no_index' => 1,
                                                            'size' => 15733
                                                          },
                            'lib/MarpaX/ESLIF.pm' => {
                                                       'license' => 'Perl_5',
                                                       'module' => 'MarpaX::ESLIF',
                                                       'mtime' => 1709273152,
                                                       'requires' => {
                                                                       'Config' => '0',
                                                                       'Encode' => '0',
                                                                       'JSON::MaybeXS' => '1.004000',
                                                                       'MarpaX::ESLIF::Base' => '0',
                                                                       'MarpaX::ESLIF::Event::Type' => '0',
                                                                       'MarpaX::ESLIF::Grammar' => '0',
                                                                       'MarpaX::ESLIF::Grammar::Properties' => '0',
                                                                       'MarpaX::ESLIF::Grammar::Rule::Properties' => '0',
                                                                       'MarpaX::ESLIF::Grammar::Symbol::Properties' => '0',
                                                                       'MarpaX::ESLIF::JSON' => '0',
                                                                       'MarpaX::ESLIF::Logger::Level' => '0',
                                                                       'MarpaX::ESLIF::Recognizer' => '0',
                                                                       'MarpaX::ESLIF::RegexCallout' => '0',
                                                                       'MarpaX::ESLIF::Rule::PropertyBitSet' => '0',
                                                                       'MarpaX::ESLIF::String' => '0',
                                                                       'MarpaX::ESLIF::Symbol' => '0',
                                                                       'MarpaX::ESLIF::Symbol::EventBitSet' => '0',
                                                                       'MarpaX::ESLIF::Symbol::PropertyBitSet' => '0',
                                                                       'MarpaX::ESLIF::Symbol::Type' => '0',
                                                                       'MarpaX::ESLIF::Value' => '0',
                                                                       'MarpaX::ESLIF::Value::Type' => '0',
                                                                       'Math::BigFloat' => '0',
                                                                       'Math::BigInt' => '0',
                                                                       'XSLoader' => '0',
                                                                       'parent' => '0',
                                                                       'strict' => '0',
                                                                       'warnings' => '0'
                                                                     },
                                                       'size' => 16910
                                                     },
                            'lib/MarpaX/ESLIF/BNF.pod' => {
                                                            'license' => 'Perl_5',
                                                            'mtime' => 1709273152,
                                                            'size' => 73061
                                                          },
                            'lib/MarpaX/ESLIF/Base.pm' => {
                                                            'license' => 'Perl_5',
                                                            'module' => 'MarpaX::ESLIF::Base',
                                                            'mtime' => 1709273152,
                                                            'requires' => {
                                                                            'Carp' => '0',
                                                                            'Devel::GlobalDestruction' => '0',
                                                                            'namespace::clean' => '0',
                                                                            'strict' => '0',
                                                                            'warnings' => '0'
                                                                          },
                                                            'size' => 8146
                                                          },
                            'lib/MarpaX/ESLIF/Bindings.pod' => {
                                                                 'license' => 'Perl_5',
                                                                 'mtime' => 1709273159,
                                                                 'size' => 17095
                                                               },
                            'lib/MarpaX/ESLIF/Event/Type.pm' => {
                                                                  'license' => 'Perl_5',
                                                                  'module' => 'MarpaX::ESLIF::Event::Type',
                                                                  'mtime' => 1709273159,
                                                                  'requires' => {
                                                                                  'Carp' => '0',
                                                                                  'strict' => '0',
                                                                                  'warnings' => '0'
                                                                                },
                                                                  'size' => 2221
                                                                },
                            'lib/MarpaX/ESLIF/Grammar.pm' => {
                                                               'license' => 'Perl_5',
                                                               'module' => 'MarpaX::ESLIF::Grammar',
                                                               'mtime' => 1709273153,
                                                               'requires' => {
                                                                               'MarpaX::ESLIF::Base' => '0',
                                                                               'parent' => '0',
                                                                               'strict' => '0',
                                                                               'warnings' => '0'
                                                                             },
                                                               'size' => 10347
                                                             },
                            'lib/MarpaX/ESLIF/Grammar/Properties.pm' => {
                                                                          'license' => 'Perl_5',
                                                                          'module' => 'MarpaX::ESLIF::Grammar::Properties',
                                                                          'mtime' => 1709273163,
                                                                          'requires' => {
                                                                                          'strict' => '0',
                                                                                          'warnings' => '0'
                                                                                        },
                                                                          'size' => 7143
                                                                        },
                            'lib/MarpaX/ESLIF/Grammar/Rule/Properties.pm' => {
                                                                               'license' => 'Perl_5',
                                                                               'module' => 'MarpaX::ESLIF::Grammar::Rule::Properties',
                                                                               'mtime' => 1709273163,
                                                                               'requires' => {
                                                                                               'strict' => '0',
                                                                                               'warnings' => '0'
                                                                                             },
                                                                               'size' => 7451
                                                                             },
                            'lib/MarpaX/ESLIF/Grammar/Symbol/Properties.pm' => {
                                                                                 'license' => 'Perl_5',
                                                                                 'module' => 'MarpaX::ESLIF::Grammar::Symbol::Properties',
                                                                                 'mtime' => 1709273163,
                                                                                 'requires' => {
                                                                                                 'strict' => '0',
                                                                                                 'warnings' => '0'
                                                                                               },
                                                                                 'size' => 11817
                                                                               },
                            'lib/MarpaX/ESLIF/Introduction.pod' => {
                                                                     'license' => 'Perl_5',
                                                                     'mtime' => 1709273160,
                                                                     'size' => 24223
                                                                   },
                            'lib/MarpaX/ESLIF/JSON.pm' => {
                                                            'license' => 'Perl_5',
                                                            'module' => 'MarpaX::ESLIF::JSON',
                                                            'mtime' => 1709273152,
                                                            'requires' => {
                                                                            'MarpaX::ESLIF::JSON::Decoder' => '0',
                                                                            'MarpaX::ESLIF::JSON::Encoder' => '0',
                                                                            'strict' => '0',
                                                                            'warnings' => '0'
                                                                          },
                                                            'size' => 2589
                                                          },
                            'lib/MarpaX/ESLIF/JSON/Decoder.pm' => {
                                                                    'license' => 'Perl_5',
                                                                    'module' => 'MarpaX::ESLIF::JSON::Decoder',
                                                                    'mtime' => 1709273160,
                                                                    'requires' => {
                                                                                    'MarpaX::ESLIF::Grammar' => '0',
                                                                                    'MarpaX::ESLIF::JSON::Decoder::RecognizerInterface' => '0',
                                                                                    'parent' => '0',
                                                                                    'strict' => '0',
                                                                                    'warnings' => '0'
                                                                                  },
                                                                    'size' => 3676
                                                                  },
                            'lib/MarpaX/ESLIF/JSON/Decoder/RecognizerInterface.pm' => {
                                                                                        'license' => 'Perl_5',
                                                                                        'module' => 'MarpaX::ESLIF::JSON::Decoder::RecognizerInterface',
                                                                                        'mtime' => 1709273163,
                                                                                        'requires' => {
                                                                                                        'Carp' => '0',
                                                                                                        'strict' => '0',
                                                                                                        'warnings' => '0'
                                                                                                      },
                                                                                        'size' => 4645
                                                                                      },
                            'lib/MarpaX/ESLIF/JSON/Encoder.pm' => {
                                                                    'license' => 'Perl_5',
                                                                    'module' => 'MarpaX::ESLIF::JSON::Encoder',
                                                                    'mtime' => 1709273160,
                                                                    'requires' => {
                                                                                    'MarpaX::ESLIF::Grammar' => '0',
                                                                                    'parent' => '0',
                                                                                    'strict' => '0',
                                                                                    'warnings' => '0'
                                                                                  },
                                                                    'size' => 2263
                                                                  },
                            'lib/MarpaX/ESLIF/Logger/Interface.pod' => {
                                                                         'license' => 'Perl_5',
                                                                         'mtime' => 1709273163,
                                                                         'size' => 1610
                                                                       },
                            'lib/MarpaX/ESLIF/Logger/Level.pm' => {
                                                                    'license' => 'Perl_5',
                                                                    'module' => 'MarpaX::ESLIF::Logger::Level',
                                                                    'mtime' => 1709273160,
                                                                    'requires' => {
                                                                                    'Carp' => '0',
                                                                                    'strict' => '0',
                                                                                    'warnings' => '0'
                                                                                  },
                                                                    'size' => 1706
                                                                  },
                            'lib/MarpaX/ESLIF/Recognizer.pm' => {
                                                                  'license' => 'Perl_5',
                                                                  'module' => 'MarpaX::ESLIF::Recognizer',
                                                                  'mtime' => 1709273159,
                                                                  'noes' => {
                                                                              'warnings' => '0'
                                                                            },
                                                                  'requires' => {
                                                                                  'MarpaX::ESLIF::Base' => '0',
                                                                                  'parent' => '0',
                                                                                  'strict' => '0',
                                                                                  'warnings' => '0'
                                                                                },
                                                                  'size' => 17886
                                                                },
                            'lib/MarpaX/ESLIF/Recognizer/Interface.pod' => {
                                                                             'license' => 'Perl_5',
                                                                             'mtime' => 1709273163,
                                                                             'size' => 5149
                                                                           },
                            'lib/MarpaX/ESLIF/RegexCallout.pm' => {
                                                                    'license' => 'Perl_5',
                                                                    'module' => 'MarpaX::ESLIF::RegexCallout',
                                                                    'mtime' => 1709273160,
                                                                    'requires' => {
                                                                                    'constant' => '0',
                                                                                    'strict' => '0',
                                                                                    'warnings' => '0'
                                                                                  },
                                                                    'size' => 13220
                                                                  },
                            'lib/MarpaX/ESLIF/Rule/PropertyBitSet.pm' => {
                                                                           'license' => 'Perl_5',
                                                                           'module' => 'MarpaX::ESLIF::Rule::PropertyBitSet',
                                                                           'mtime' => 1709273163,
                                                                           'requires' => {
                                                                                           'Carp' => '0',
                                                                                           'strict' => '0',
                                                                                           'warnings' => '0'
                                                                                         },
                                                                           'size' => 1499
                                                                         },
                            'lib/MarpaX/ESLIF/String.pm' => {
                                                              'license' => 'Perl_5',
                                                              'module' => 'MarpaX::ESLIF::String',
                                                              'mtime' => 1709273153,
                                                              'requires' => {
                                                                              'Carp' => '0',
                                                                              'overload' => '0',
                                                                              'strict' => '0',
                                                                              'warnings' => '0'
                                                                            },
                                                              'size' => 3078
                                                            },
                            'lib/MarpaX/ESLIF/Symbol.pm' => {
                                                              'license' => 'Perl_5',
                                                              'module' => 'MarpaX::ESLIF::Symbol',
                                                              'mtime' => 1709273153,
                                                              'requires' => {
                                                                              'Carp' => '0',
                                                                              'MarpaX::ESLIF::Base' => '0',
                                                                              'parent' => '0',
                                                                              'strict' => '0',
                                                                              'warnings' => '0'
                                                                            },
                                                              'size' => 10512
                                                            },
                            'lib/MarpaX/ESLIF/Symbol/EventBitSet.pm' => {
                                                                          'license' => 'Perl_5',
                                                                          'module' => 'MarpaX::ESLIF::Symbol::EventBitSet',
                                                                          'mtime' => 1709273163,
                                                                          'requires' => {
                                                                                          'Carp' => '0',
                                                                                          'strict' => '0',
                                                                                          'warnings' => '0'
                                                                                        },
                                                                          'size' => 1281
                                                                        },
                            'lib/MarpaX/ESLIF/Symbol/PropertyBitSet.pm' => {
                                                                             'license' => 'Perl_5',
                                                                             'module' => 'MarpaX::ESLIF::Symbol::PropertyBitSet',
                                                                             'mtime' => 1709273163,
                                                                             'requires' => {
                                                                                             'Carp' => '0',
                                                                                             'strict' => '0',
                                                                                             'warnings' => '0'
                                                                                           },
                                                                             'size' => 1653
                                                                           },
                            'lib/MarpaX/ESLIF/Symbol/Type.pm' => {
                                                                   'license' => 'Perl_5',
                                                                   'module' => 'MarpaX::ESLIF::Symbol::Type',
                                                                   'mtime' => 1709273160,
                                                                   'requires' => {
                                                                                   'Carp' => '0',
                                                                                   'strict' => '0',
                                                                                   'warnings' => '0'
                                                                                 },
                                                                   'size' => 1045
                                                                 },
                            'lib/MarpaX/ESLIF/Tutorial/Calculator.pod' => {
                                                                            'license' => 'Perl_5',
                                                                            'mtime' => 1709273163,
                                                                            'size' => 27122
                                                                          },
                            'lib/MarpaX/ESLIF/Value.pm' => {
                                                             'license' => 'Perl_5',
                                                             'module' => 'MarpaX::ESLIF::Value',
                                                             'mtime' => 1709273153,
                                                             'requires' => {
                                                                             'MarpaX::ESLIF::Base' => '0',
                                                                             'parent' => '0',
                                                                             'strict' => '0',
                                                                             'warnings' => '0'
                                                                           },
                                                             'size' => 1899
                                                           },
                            'lib/MarpaX/ESLIF/Value/Interface.pod' => {
                                                                        'license' => 'Perl_5',
                                                                        'mtime' => 1709273163,
                                                                        'size' => 4037
                                                                      },
                            'lib/MarpaX/ESLIF/Value/Type.pm' => {
                                                                  'license' => 'Perl_5',
                                                                  'module' => 'MarpaX::ESLIF::Value::Type',
                                                                  'mtime' => 1709273159,
                                                                  'requires' => {
                                                                                  'Carp' => '0',
                                                                                  'strict' => '0',
                                                                                  'warnings' => '0'
                                                                                },
                                                                  'size' => 3108
                                                                },
                            'ppport.h' => {
                                            'mtime' => 1709273151,
                                            'size' => 559260
                                          },
                            't/00-compile.t' => {
                                                  'mtime' => 1709273152,
                                                  'no_index' => 1,
                                                  'requires' => {
                                                                  'File::Spec' => '0',
                                                                  'IO::Handle' => '0',
                                                                  'IPC::Open3' => '0',
                                                                  'Test::More' => '0',
                                                                  'perl' => '5.006',
                                                                  'strict' => '0',
                                                                  'warnings' => '0'
                                                                },
                                                  'size' => 2219,
                                                  'suggests' => {
                                                                  'blib' => '1.01'
                                                                }
                                                },
                            't/00-report-prereqs.dd' => {
                                                          'mtime' => 1709273152,
                                                          'no_index' => 1,
                                                          'size' => 7729
                                                        },
                            't/00-report-prereqs.t' => {
                                                         'mtime' => 1709273152,
                                                         'no_index' => 1,
                                                         'requires' => {
                                                                         'ExtUtils::MakeMaker' => '0',
                                                                         'File::Spec' => '0',
                                                                         'Test::More' => '0',
                                                                         'strict' => '0',
                                                                         'warnings' => '0'
                                                                       },
                                                         'size' => 6017
                                                       },
                            't/advent.t' => {
                                              'mtime' => 1709273152,
                                              'no_index' => 1,
                                              'requires' => {
                                                              'Encode' => '0',
                                                              'Log::Any' => '0',
                                                              'Log::Any::Adapter' => '0',
                                                              'Test::More' => '0.88',
                                                              'Test::More::UTF8' => '0',
                                                              'diagnostics' => '0',
                                                              'open' => '0',
                                                              'strict' => '0',
                                                              'utf8' => '0',
                                                              'warnings' => '0'
                                                            },
                                              'size' => 6941
                                            },
                            't/allluacallbacks.t' => {
                                                       'mtime' => 1709273152,
                                                       'no_index' => 1,
                                                       'requires' => {
                                                                       'Log::Any' => '0',
                                                                       'Log::Any::Adapter' => '0',
                                                                       'Test::More' => '0',
                                                                       'diagnostics' => '0',
                                                                       'strict' => '0',
                                                                       'warnings' => '0'
                                                                     },
                                                       'size' => 3920
                                                     },
                            't/import_export.t' => {
                                                     'mtime' => 1709273152,
                                                     'no_index' => 1,
                                                     'requires' => {
                                                                     'Data::Dumper' => '0',
                                                                     'Encode' => '0',
                                                                     'Log::Any' => '0',
                                                                     'Log::Any::Adapter' => '0',
                                                                     'Math::BigFloat' => '0',
                                                                     'Math::BigInt' => '0',
                                                                     'Safe::Isa' => '0',
                                                                     'Test::Deep' => '0',
                                                                     'Test::More' => '0.88',
                                                                     'Test::More::UTF8' => '0',
                                                                     'diagnostics' => '0',
                                                                     'open' => '0',
                                                                     'strict' => '0',
                                                                     'utf8' => '0',
                                                                     'warnings' => '0'
                                                                   },
                                                     'size' => 6881
                                                   },
                            't/json.t' => {
                                            'mtime' => 1709273152,
                                            'no_index' => 1,
                                            'requires' => {
                                                            'Log::Any' => '0',
                                                            'Log::Any::Adapter' => '0',
                                                            'POSIX' => '0',
                                                            'Test::More' => '0.88',
                                                            'Test::More::UTF8' => '0',
                                                            'strict' => '0',
                                                            'warnings' => '0'
                                                          },
                                            'size' => 4496
                                          },
                            't/jsonWithSharedStream.t' => {
                                                            'mtime' => 1709273152,
                                                            'no_index' => 1,
                                                            'requires' => {
                                                                            'Log::Any' => '0',
                                                                            'Log::Any::Adapter' => '0',
                                                                            'Test::More' => '0.88',
                                                                            'Test::More::UTF8' => '0',
                                                                            'diagnostics' => '0',
                                                                            'strict' => '0',
                                                                            'warnings' => '0'
                                                                          },
                                                            'size' => 14337
                                                          },
                            't/parameterizedRules.t' => {
                                                          'mtime' => 1709273152,
                                                          'no_index' => 1,
                                                          'requires' => {
                                                                          'Carp' => '0',
                                                                          'Log::Any' => '0',
                                                                          'Log::Any::Adapter' => '0',
                                                                          'Test::More' => '0',
                                                                          'diagnostics' => '0',
                                                                          'strict' => '0',
                                                                          'warnings' => '0'
                                                                        },
                                                          'size' => 3645
                                                        },
                            't/resolver.t' => {
                                                'mtime' => 1709273152,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'Encode' => '0',
                                                                'Log::Any' => '0',
                                                                'Log::Any::Adapter' => '0',
                                                                'Test::More' => '0.88',
                                                                'Test::More::UTF8' => '0',
                                                                'diagnostics' => '0',
                                                                'open' => '0',
                                                                'strict' => '0',
                                                                'utf8' => '0',
                                                                'warnings' => '0'
                                                              },
                                                'size' => 6032
                                              },
                            't/symbol.t' => {
                                              'mtime' => 1709273152,
                                              'no_index' => 1,
                                              'requires' => {
                                                              'Log::Any' => '0',
                                                              'Log::Any::Adapter' => '0',
                                                              'Test::More' => '0',
                                                              'diagnostics' => '0',
                                                              'strict' => '0',
                                                              'warnings' => '0'
                                                            },
                                              'size' => 3205
                                            },
                            't/test.t' => {
                                            'mtime' => 1709273152,
                                            'no_index' => 1,
                                            'noes' => {
                                                        'warnings' => '0'
                                                      },
                                            'requires' => {
                                                            'Carp' => '0',
                                                            'Encode' => '0',
                                                            'Log::Any' => '0',
                                                            'Log::Any::Adapter' => '0',
                                                            'Test::Deep::NoTest' => '0',
                                                            'Test::More' => '0.88',
                                                            'Try::Tiny' => '0',
                                                            'diagnostics' => '0',
                                                            'strict' => '0',
                                                            'warnings' => '0'
                                                          },
                                            'size' => 83705
                                          },
                            't/thread.t' => {
                                              'mtime' => 1709273152,
                                              'no_index' => 1,
                                              'recommends' => {
                                                                'Log::Any' => '0',
                                                                'Log::Any::Adapter' => '0',
                                                                'Test::More' => '0.88',
                                                                'constant' => '0',
                                                                'diagnostics' => '0',
                                                                'threads' => '0',
                                                                'threads::shared' => '0'
                                                              },
                                              'requires' => {
                                                              'Config' => '0',
                                                              'strict' => '0',
                                                              'warnings' => '0'
                                                            },
                                              'size' => 6656
                                            },
                            'xt/author/critic.t' => {
                                                      'mtime' => 1709273152,
                                                      'no_index' => 1,
                                                      'size' => 129
                                                    },
                            'xt/author/distmeta.t' => {
                                                        'mtime' => 1709273152,
                                                        'no_index' => 1,
                                                        'size' => 147
                                                      },
                            'xt/author/minimum-version.t' => {
                                                               'mtime' => 1709273153,
                                                               'no_index' => 1,
                                                               'size' => 108
                                                             },
                            'xt/author/mojibake.t' => {
                                                        'mtime' => 1709273152,
                                                        'no_index' => 1,
                                                        'size' => 105
                                                      },
                            'xt/author/no-tabs.t' => {
                                                       'mtime' => 1709273152,
                                                       'no_index' => 1,
                                                       'size' => 1688
                                                     },
                            'xt/author/pod-coverage.t' => {
                                                            'mtime' => 1709273152,
                                                            'no_index' => 1,
                                                            'size' => 1680
                                                          },
                            'xt/author/pod-syntax.t' => {
                                                          'mtime' => 1709273152,
                                                          'no_index' => 1,
                                                          'size' => 170
                                                        },
                            'xt/author/portability.t' => {
                                                           'mtime' => 1709273152,
                                                           'no_index' => 1,
                                                           'size' => 115
                                                         },
                            'xt/author/synopsis.t' => {
                                                        'mtime' => 1709273152,
                                                        'no_index' => 1,
                                                        'size' => 48
                                                      },
                            'xt/author/test-version.t' => {
                                                            'mtime' => 1709273152,
                                                            'no_index' => 1,
                                                            'size' => 415
                                                          },
                            'xt/release/cpan-changes.t' => {
                                                             'mtime' => 1709273152,
                                                             'no_index' => 1,
                                                             'size' => 228
                                                           },
                            'xt/release/kwalitee.t' => {
                                                         'mtime' => 1709273152,
                                                         'no_index' => 1,
                                                         'size' => 208
                                                       },
                            'xt/release/meta-json.t' => {
                                                          'mtime' => 1709273152,
                                                          'no_index' => 1,
                                                          'size' => 52
                                                        }
                          },
          'got_prereq_from' => 'META.yml',
          'ignored_files_array' => [
                                     'inc/ExtUtils/CppGuess.pm',
                                     't/00-compile.t',
                                     't/00-report-prereqs.dd',
                                     't/00-report-prereqs.t',
                                     't/advent.t',
                                     't/allluacallbacks.t',
                                     't/import_export.t',
                                     't/json.t',
                                     't/jsonWithSharedStream.t',
                                     't/parameterizedRules.t',
                                     't/resolver.t',
                                     't/symbol.t',
                                     't/test.t',
                                     't/thread.t',
                                     'xt/author/critic.t',
                                     'xt/author/distmeta.t',
                                     'xt/author/minimum-version.t',
                                     'xt/author/mojibake.t',
                                     'xt/author/no-tabs.t',
                                     'xt/author/pod-coverage.t',
                                     'xt/author/pod-syntax.t',
                                     'xt/author/portability.t',
                                     'xt/author/synopsis.t',
                                     'xt/author/test-version.t',
                                     'xt/release/cpan-changes.t',
                                     'xt/release/kwalitee.t',
                                     'xt/release/meta-json.t'
                                   ],
          'included_modules' => [
                                  'ExtUtils::CppGuess'
                                ],
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 1,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 1,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 33,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 1,
                          'meta_yml_has_repository_resource' => 1,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 1,
                          'use_warnings' => 1
                        },
          'latest_mtime' => 1709273163,
          'license' => 'perl defined in META.yml defined in LICENSE',
          'license_file' => 'lib/MarpaX/ESLIF.pm,lib/MarpaX/ESLIF/BNF.pod,lib/MarpaX/ESLIF/Base.pm,lib/MarpaX/ESLIF/Bindings.pod,lib/MarpaX/ESLIF/Event/Type.pm,lib/MarpaX/ESLIF/Grammar.pm,lib/MarpaX/ESLIF/Grammar/Properties.pm,lib/MarpaX/ESLIF/Grammar/Rule/Properties.pm,lib/MarpaX/ESLIF/Grammar/Symbol/Properties.pm,lib/MarpaX/ESLIF/Introduction.pod,lib/MarpaX/ESLIF/JSON.pm,lib/MarpaX/ESLIF/JSON/Decoder.pm,lib/MarpaX/ESLIF/JSON/Decoder/RecognizerInterface.pm,lib/MarpaX/ESLIF/JSON/Encoder.pm,lib/MarpaX/ESLIF/Logger/Interface.pod,lib/MarpaX/ESLIF/Logger/Level.pm,lib/MarpaX/ESLIF/Recognizer.pm,lib/MarpaX/ESLIF/Recognizer/Interface.pod,lib/MarpaX/ESLIF/RegexCallout.pm,lib/MarpaX/ESLIF/Rule/PropertyBitSet.pm,lib/MarpaX/ESLIF/String.pm,lib/MarpaX/ESLIF/Symbol.pm,lib/MarpaX/ESLIF/Symbol/EventBitSet.pm,lib/MarpaX/ESLIF/Symbol/PropertyBitSet.pm,lib/MarpaX/ESLIF/Symbol/Type.pm,lib/MarpaX/ESLIF/Tutorial/Calculator.pod,lib/MarpaX/ESLIF/Value.pm,lib/MarpaX/ESLIF/Value/Interface.pod,lib/MarpaX/ESLIF/Value/Type.pm',
          'license_from_yaml' => 'perl',
          'license_in_pod' => 1,
          'license_type' => 'Perl_5',
          'licenses' => {
                          'Perl_5' => [
                                        'lib/MarpaX/ESLIF.pm',
                                        'lib/MarpaX/ESLIF/BNF.pod',
                                        'lib/MarpaX/ESLIF/Base.pm',
                                        'lib/MarpaX/ESLIF/Bindings.pod',
                                        'lib/MarpaX/ESLIF/Event/Type.pm',
                                        'lib/MarpaX/ESLIF/Grammar.pm',
                                        'lib/MarpaX/ESLIF/Grammar/Properties.pm',
                                        'lib/MarpaX/ESLIF/Grammar/Rule/Properties.pm',
                                        'lib/MarpaX/ESLIF/Grammar/Symbol/Properties.pm',
                                        'lib/MarpaX/ESLIF/Introduction.pod',
                                        'lib/MarpaX/ESLIF/JSON.pm',
                                        'lib/MarpaX/ESLIF/JSON/Decoder.pm',
                                        'lib/MarpaX/ESLIF/JSON/Decoder/RecognizerInterface.pm',
                                        'lib/MarpaX/ESLIF/JSON/Encoder.pm',
                                        'lib/MarpaX/ESLIF/Logger/Interface.pod',
                                        'lib/MarpaX/ESLIF/Logger/Level.pm',
                                        'lib/MarpaX/ESLIF/Recognizer.pm',
                                        'lib/MarpaX/ESLIF/Recognizer/Interface.pod',
                                        'lib/MarpaX/ESLIF/RegexCallout.pm',
                                        'lib/MarpaX/ESLIF/Rule/PropertyBitSet.pm',
                                        'lib/MarpaX/ESLIF/String.pm',
                                        'lib/MarpaX/ESLIF/Symbol.pm',
                                        'lib/MarpaX/ESLIF/Symbol/EventBitSet.pm',
                                        'lib/MarpaX/ESLIF/Symbol/PropertyBitSet.pm',
                                        'lib/MarpaX/ESLIF/Symbol/Type.pm',
                                        'lib/MarpaX/ESLIF/Tutorial/Calculator.pod',
                                        'lib/MarpaX/ESLIF/Value.pm',
                                        'lib/MarpaX/ESLIF/Value/Interface.pod',
                                        'lib/MarpaX/ESLIF/Value/Type.pm'
                                      ]
                        },
          'manifest_matches_dist' => 1,
          'meta_json' => {
                           'abstract' => 'ESLIF is Extended ScanLess InterFace',
                           'author' => [
                                         'Jean-Damien Durand <jeandamiendurand@free.fr>'
                                       ],
                           'dynamic_config' => 1,
                           'generated_by' => 'Dist::Zilla version 6.031, CPAN::Meta::Converter version 2.150010',
                           'license' => [
                                          'perl_5'
                                        ],
                           'meta-spec' => {
                                            'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                            'version' => 2
                                          },
                           'name' => 'MarpaX-ESLIF',
                           'no_index' => {
                                           'directory' => [
                                                            'eg',
                                                            'examples',
                                                            'inc',
                                                            'share',
                                                            't',
                                                            'xt'
                                                          ]
                                         },
                           'prereqs' => {
                                          'configure' => {
                                                           'requires' => {
                                                                           'Archive::Tar' => '0',
                                                                           'Carp' => '0',
                                                                           'Config' => '0',
                                                                           'Config::AutoConf' => '0',
                                                                           'Cwd' => '0',
                                                                           'ExtUtils::CBuilder' => '0.280224',
                                                                           'ExtUtils::Constant' => '0',
                                                                           'ExtUtils::MakeMaker' => '0',
                                                                           'File::Basename' => '0',
                                                                           'File::Copy' => '0',
                                                                           'File::Find' => '0',
                                                                           'File::Path' => '0',
                                                                           'File::Spec' => '0',
                                                                           'File::Temp' => '0',
                                                                           'File::Which' => '0',
                                                                           'File::chdir' => '0',
                                                                           'IO::Handle' => '0',
                                                                           'POSIX' => '0',
                                                                           'Perl::OSType' => '0',
                                                                           'Probe::Perl' => '0',
                                                                           'Try::Tiny' => '0',
                                                                           'perl' => '5.006'
                                                                         },
                                                           'suggests' => {
                                                                           'ExtUtils::CppGuess' => '0.26'
                                                                         }
                                                         },
                                          'develop' => {
                                                         'requires' => {
                                                                         'Dist::Zilla' => '5',
                                                                         'Dist::Zilla::Plugin::Authority' => '0',
                                                                         'Dist::Zilla::Plugin::AutoMetaResources' => '0',
                                                                         'Dist::Zilla::Plugin::AutoPrereqs' => '0',
                                                                         'Dist::Zilla::Plugin::CPANFile' => '0',
                                                                         'Dist::Zilla::Plugin::ChangelogFromGit::CPAN::Changes' => '0',
                                                                         'Dist::Zilla::Plugin::DynamicPrereqs' => '0',
                                                                         'Dist::Zilla::Plugin::GatherFile' => '0',
                                                                         'Dist::Zilla::Plugin::Git::Contributors' => '0',
                                                                         'Dist::Zilla::Plugin::Git::NextVersion' => '0',
                                                                         'Dist::Zilla::Plugin::GitHub::Meta' => '0',
                                                                         'Dist::Zilla::Plugin::MetaTests' => '0',
                                                                         'Dist::Zilla::Plugin::MinimumPerl' => '0',
                                                                         'Dist::Zilla::Plugin::MojibakeTests' => '0',
                                                                         'Dist::Zilla::Plugin::OurPkgVersion' => '0',
                                                                         'Dist::Zilla::Plugin::PodWeaver' => '0',
                                                                         'Dist::Zilla::Plugin::Prereqs' => '0',
                                                                         'Dist::Zilla::Plugin::Prereqs::AuthorDeps' => '0',
                                                                         'Dist::Zilla::Plugin::Test::CPAN::Changes' => '0',
                                                                         'Dist::Zilla::Plugin::Test::CPAN::Meta::JSON' => '0',
                                                                         'Dist::Zilla::Plugin::Test::Kwalitee' => '0',
                                                                         'Dist::Zilla::Plugin::Test::MinimumVersion' => '0',
                                                                         'Dist::Zilla::Plugin::Test::NoTabs' => '0',
                                                                         'Dist::Zilla::Plugin::Test::Perl::Critic' => '0',
                                                                         'Dist::Zilla::Plugin::Test::Pod::Coverage::Configurable' => '0',
                                                                         'Dist::Zilla::Plugin::Test::Portability' => '0',
                                                                         'Dist::Zilla::Plugin::Test::Synopsis' => '0',
                                                                         'Dist::Zilla::Plugin::Test::Version' => '0',
                                                                         'Dist::Zilla::PluginBundle::Starter::Git' => '0',
                                                                         'Pod::Coverage::TrustPod' => '0',
                                                                         'Pod::Weaver::PluginBundle::RJBS' => '0',
                                                                         'Software::License::Perl_5' => '0',
                                                                         'Test::CPAN::Changes' => '0.19',
                                                                         'Test::CPAN::Meta' => '0',
                                                                         'Test::CPAN::Meta::JSON' => '0.16',
                                                                         'Test::Kwalitee' => '1.21',
                                                                         'Test::MinimumVersion' => '0',
                                                                         'Test::Mojibake' => '0',
                                                                         'Test::More' => '0.96',
                                                                         'Test::NoTabs' => '0',
                                                                         'Test::Perl::Critic' => '0',
                                                                         'Test::Pod' => '1.41',
                                                                         'Test::Pod::Coverage' => '1.08',
                                                                         'Test::Portability::Files' => '0',
                                                                         'Test::Synopsis' => '0',
                                                                         'Test::Version' => '1'
                                                                       }
                                                       },
                                          'runtime' => {
                                                         'requires' => {
                                                                         'Carp' => '0',
                                                                         'Devel::GlobalDestruction' => '0',
                                                                         'Encode' => '0',
                                                                         'JSON::MaybeXS' => '1.004000',
                                                                         'Math::BigFloat' => '0',
                                                                         'Math::BigInt' => '0',
                                                                         'XSLoader' => '0',
                                                                         'constant' => '0',
                                                                         'namespace::clean' => '0',
                                                                         'overload' => '0',
                                                                         'parent' => '0',
                                                                         'perl' => '5.010',
                                                                         'strict' => '0',
                                                                         'warnings' => '0'
                                                                       }
                                                       },
                                          'test' => {
                                                      'recommends' => {
                                                                        'CPAN::Meta' => '2.120900'
                                                                      },
                                                      'requires' => {
                                                                      'Data::Dumper' => '0',
                                                                      'ExtUtils::MakeMaker' => '0',
                                                                      'File::Spec' => '0',
                                                                      'IO::Handle' => '0',
                                                                      'IPC::Open3' => '0',
                                                                      'Log::Any' => '0',
                                                                      'Log::Any::Adapter' => '0',
                                                                      'POSIX' => '0',
                                                                      'Safe::Isa' => '0',
                                                                      'Test::Deep' => '0',
                                                                      'Test::Deep::NoTest' => '0',
                                                                      'Test::More' => '0',
                                                                      'Test::More::UTF8' => '0',
                                                                      'Try::Tiny' => '0',
                                                                      'diagnostics' => '0',
                                                                      'open' => '0',
                                                                      'perl' => '5.010',
                                                                      'threads' => '0',
                                                                      'threads::shared' => '0',
                                                                      'utf8' => '0'
                                                                    }
                                                    }
                                        },
                           'provides' => {
                                           'MarpaX::ESLIF' => {
                                                                'file' => 'lib/MarpaX/ESLIF.pm',
                                                                'version' => 'v6.0.33.4'
                                                              },
                                           'MarpaX::ESLIF::Base' => {
                                                                      'file' => 'lib/MarpaX/ESLIF/Base.pm',
                                                                      'version' => 'v6.0.33.4'
                                                                    },
                                           'MarpaX::ESLIF::Event::Type' => {
                                                                             'file' => 'lib/MarpaX/ESLIF/Event/Type.pm',
                                                                             'version' => 'v6.0.33.4'
                                                                           },
                                           'MarpaX::ESLIF::Grammar' => {
                                                                         'file' => 'lib/MarpaX/ESLIF/Grammar.pm',
                                                                         'version' => 'v6.0.33.4'
                                                                       },
                                           'MarpaX::ESLIF::Grammar::Properties' => {
                                                                                     'file' => 'lib/MarpaX/ESLIF/Grammar/Properties.pm',
                                                                                     'version' => 'v6.0.33.4'
                                                                                   },
                                           'MarpaX::ESLIF::Grammar::Rule::Properties' => {
                                                                                           'file' => 'lib/MarpaX/ESLIF/Grammar/Rule/Properties.pm',
                                                                                           'version' => 'v6.0.33.4'
                                                                                         },
                                           'MarpaX::ESLIF::Grammar::Symbol::Properties' => {
                                                                                             'file' => 'lib/MarpaX/ESLIF/Grammar/Symbol/Properties.pm',
                                                                                             'version' => 'v6.0.33.4'
                                                                                           },
                                           'MarpaX::ESLIF::JSON' => {
                                                                      'file' => 'lib/MarpaX/ESLIF/JSON.pm',
                                                                      'version' => 'v6.0.33.4'
                                                                    },
                                           'MarpaX::ESLIF::JSON::Decoder' => {
                                                                               'file' => 'lib/MarpaX/ESLIF/JSON/Decoder.pm',
                                                                               'version' => 'v6.0.33.4'
                                                                             },
                                           'MarpaX::ESLIF::JSON::Decoder::RecognizerInterface' => {
                                                                                                    'file' => 'lib/MarpaX/ESLIF/JSON/Decoder/RecognizerInterface.pm',
                                                                                                    'version' => 'v6.0.33.4'
                                                                                                  },
                                           'MarpaX::ESLIF::JSON::Encoder' => {
                                                                               'file' => 'lib/MarpaX/ESLIF/JSON/Encoder.pm',
                                                                               'version' => 'v6.0.33.4'
                                                                             },
                                           'MarpaX::ESLIF::Logger::Level' => {
                                                                               'file' => 'lib/MarpaX/ESLIF/Logger/Level.pm',
                                                                               'version' => 'v6.0.33.4'
                                                                             },
                                           'MarpaX::ESLIF::Recognizer' => {
                                                                            'file' => 'lib/MarpaX/ESLIF/Recognizer.pm',
                                                                            'version' => 'v6.0.33.4'
                                                                          },
                                           'MarpaX::ESLIF::RegexCallout' => {
                                                                              'file' => 'lib/MarpaX/ESLIF/RegexCallout.pm',
                                                                              'version' => 'v6.0.33.4'
                                                                            },
                                           'MarpaX::ESLIF::Rule::PropertyBitSet' => {
                                                                                      'file' => 'lib/MarpaX/ESLIF/Rule/PropertyBitSet.pm',
                                                                                      'version' => 'v6.0.33.4'
                                                                                    },
                                           'MarpaX::ESLIF::String' => {
                                                                        'file' => 'lib/MarpaX/ESLIF/String.pm',
                                                                        'version' => 'v6.0.33.4'
                                                                      },
                                           'MarpaX::ESLIF::Symbol' => {
                                                                        'file' => 'lib/MarpaX/ESLIF/Symbol.pm',
                                                                        'version' => 'v6.0.33.4'
                                                                      },
                                           'MarpaX::ESLIF::Symbol::EventBitSet' => {
                                                                                     'file' => 'lib/MarpaX/ESLIF/Symbol/EventBitSet.pm',
                                                                                     'version' => 'v6.0.33.4'
                                                                                   },
                                           'MarpaX::ESLIF::Symbol::PropertyBitSet' => {
                                                                                        'file' => 'lib/MarpaX/ESLIF/Symbol/PropertyBitSet.pm',
                                                                                        'version' => 'v6.0.33.4'
                                                                                      },
                                           'MarpaX::ESLIF::Symbol::Type' => {
                                                                              'file' => 'lib/MarpaX/ESLIF/Symbol/Type.pm',
                                                                              'version' => 'v6.0.33.4'
                                                                            },
                                           'MarpaX::ESLIF::Value' => {
                                                                       'file' => 'lib/MarpaX/ESLIF/Value.pm',
                                                                       'version' => 'v6.0.33.4'
                                                                     },
                                           'MarpaX::ESLIF::Value::Type' => {
                                                                             'file' => 'lib/MarpaX/ESLIF/Value/Type.pm',
                                                                             'version' => 'v6.0.33.4'
                                                                           }
                                         },
                           'release_status' => 'stable',
                           'resources' => {
                                            'bugtracker' => {
                                                              'web' => 'https://github.com/jddurand/c-marpaESLIFPerl/issues'
                                                            },
                                            'homepage' => 'https://metacpan.org/release/MarpaX-ESLIF',
                                            'repository' => {
                                                              'type' => 'git',
                                                              'url' => 'git://github.com/jddurand/c-marpaESLIFPerl.git',
                                                              'web' => 'https://github.com/jddurand/c-marpaESLIFPerl'
                                                            }
                                          },
                           'version' => '6.0.33.4',
                           'x_authority' => 'cpan:JDDPAUSE',
                           'x_generated_by_perl' => 'v5.26.1',
                           'x_serialization_backend' => 'Cpanel::JSON::XS version 4.11',
                           'x_spdx_expression' => 'Artistic-1.0-Perl OR GPL-1.0-or-later'
                         },
          'meta_json_is_parsable' => 1,
          'meta_json_spec_version' => 2,
          'meta_yml' => {
                          'abstract' => 'ESLIF is Extended ScanLess InterFace',
                          'author' => [
                                        'Jean-Damien Durand <jeandamiendurand@free.fr>'
                                      ],
                          'build_requires' => {
                                                'Data::Dumper' => '0',
                                                'ExtUtils::MakeMaker' => '0',
                                                'File::Spec' => '0',
                                                'IO::Handle' => '0',
                                                'IPC::Open3' => '0',
                                                'Log::Any' => '0',
                                                'Log::Any::Adapter' => '0',
                                                'POSIX' => '0',
                                                'Safe::Isa' => '0',
                                                'Test::Deep' => '0',
                                                'Test::Deep::NoTest' => '0',
                                                'Test::More' => '0',
                                                'Test::More::UTF8' => '0',
                                                'Try::Tiny' => '0',
                                                'diagnostics' => '0',
                                                'open' => '0',
                                                'perl' => '5.010',
                                                'threads' => '0',
                                                'threads::shared' => '0',
                                                'utf8' => '0'
                                              },
                          'configure_requires' => {
                                                    'Archive::Tar' => '0',
                                                    'Carp' => '0',
                                                    'Config' => '0',
                                                    'Config::AutoConf' => '0',
                                                    'Cwd' => '0',
                                                    'ExtUtils::CBuilder' => '0.280224',
                                                    'ExtUtils::Constant' => '0',
                                                    'ExtUtils::MakeMaker' => '0',
                                                    'File::Basename' => '0',
                                                    'File::Copy' => '0',
                                                    'File::Find' => '0',
                                                    'File::Path' => '0',
                                                    'File::Spec' => '0',
                                                    'File::Temp' => '0',
                                                    'File::Which' => '0',
                                                    'File::chdir' => '0',
                                                    'IO::Handle' => '0',
                                                    'POSIX' => '0',
                                                    'Perl::OSType' => '0',
                                                    'Probe::Perl' => '0',
                                                    'Try::Tiny' => '0',
                                                    'perl' => '5.006'
                                                  },
                          'dynamic_config' => '1',
                          'generated_by' => 'Dist::Zilla version 6.031, CPAN::Meta::Converter version 2.150010',
                          'license' => 'perl',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'MarpaX-ESLIF',
                          'no_index' => {
                                          'directory' => [
                                                           'eg',
                                                           'examples',
                                                           'inc',
                                                           'share',
                                                           't',
                                                           'xt'
                                                         ]
                                        },
                          'provides' => {
                                          'MarpaX::ESLIF' => {
                                                               'file' => 'lib/MarpaX/ESLIF.pm',
                                                               'version' => 'v6.0.33.4'
                                                             },
                                          'MarpaX::ESLIF::Base' => {
                                                                     'file' => 'lib/MarpaX/ESLIF/Base.pm',
                                                                     'version' => 'v6.0.33.4'
                                                                   },
                                          'MarpaX::ESLIF::Event::Type' => {
                                                                            'file' => 'lib/MarpaX/ESLIF/Event/Type.pm',
                                                                            'version' => 'v6.0.33.4'
                                                                          },
                                          'MarpaX::ESLIF::Grammar' => {
                                                                        'file' => 'lib/MarpaX/ESLIF/Grammar.pm',
                                                                        'version' => 'v6.0.33.4'
                                                                      },
                                          'MarpaX::ESLIF::Grammar::Properties' => {
                                                                                    'file' => 'lib/MarpaX/ESLIF/Grammar/Properties.pm',
                                                                                    'version' => 'v6.0.33.4'
                                                                                  },
                                          'MarpaX::ESLIF::Grammar::Rule::Properties' => {
                                                                                          'file' => 'lib/MarpaX/ESLIF/Grammar/Rule/Properties.pm',
                                                                                          'version' => 'v6.0.33.4'
                                                                                        },
                                          'MarpaX::ESLIF::Grammar::Symbol::Properties' => {
                                                                                            'file' => 'lib/MarpaX/ESLIF/Grammar/Symbol/Properties.pm',
                                                                                            'version' => 'v6.0.33.4'
                                                                                          },
                                          'MarpaX::ESLIF::JSON' => {
                                                                     'file' => 'lib/MarpaX/ESLIF/JSON.pm',
                                                                     'version' => 'v6.0.33.4'
                                                                   },
                                          'MarpaX::ESLIF::JSON::Decoder' => {
                                                                              'file' => 'lib/MarpaX/ESLIF/JSON/Decoder.pm',
                                                                              'version' => 'v6.0.33.4'
                                                                            },
                                          'MarpaX::ESLIF::JSON::Decoder::RecognizerInterface' => {
                                                                                                   'file' => 'lib/MarpaX/ESLIF/JSON/Decoder/RecognizerInterface.pm',
                                                                                                   'version' => 'v6.0.33.4'
                                                                                                 },
                                          'MarpaX::ESLIF::JSON::Encoder' => {
                                                                              'file' => 'lib/MarpaX/ESLIF/JSON/Encoder.pm',
                                                                              'version' => 'v6.0.33.4'
                                                                            },
                                          'MarpaX::ESLIF::Logger::Level' => {
                                                                              'file' => 'lib/MarpaX/ESLIF/Logger/Level.pm',
                                                                              'version' => 'v6.0.33.4'
                                                                            },
                                          'MarpaX::ESLIF::Recognizer' => {
                                                                           'file' => 'lib/MarpaX/ESLIF/Recognizer.pm',
                                                                           'version' => 'v6.0.33.4'
                                                                         },
                                          'MarpaX::ESLIF::RegexCallout' => {
                                                                             'file' => 'lib/MarpaX/ESLIF/RegexCallout.pm',
                                                                             'version' => 'v6.0.33.4'
                                                                           },
                                          'MarpaX::ESLIF::Rule::PropertyBitSet' => {
                                                                                     'file' => 'lib/MarpaX/ESLIF/Rule/PropertyBitSet.pm',
                                                                                     'version' => 'v6.0.33.4'
                                                                                   },
                                          'MarpaX::ESLIF::String' => {
                                                                       'file' => 'lib/MarpaX/ESLIF/String.pm',
                                                                       'version' => 'v6.0.33.4'
                                                                     },
                                          'MarpaX::ESLIF::Symbol' => {
                                                                       'file' => 'lib/MarpaX/ESLIF/Symbol.pm',
                                                                       'version' => 'v6.0.33.4'
                                                                     },
                                          'MarpaX::ESLIF::Symbol::EventBitSet' => {
                                                                                    'file' => 'lib/MarpaX/ESLIF/Symbol/EventBitSet.pm',
                                                                                    'version' => 'v6.0.33.4'
                                                                                  },
                                          'MarpaX::ESLIF::Symbol::PropertyBitSet' => {
                                                                                       'file' => 'lib/MarpaX/ESLIF/Symbol/PropertyBitSet.pm',
                                                                                       'version' => 'v6.0.33.4'
                                                                                     },
                                          'MarpaX::ESLIF::Symbol::Type' => {
                                                                             'file' => 'lib/MarpaX/ESLIF/Symbol/Type.pm',
                                                                             'version' => 'v6.0.33.4'
                                                                           },
                                          'MarpaX::ESLIF::Value' => {
                                                                      'file' => 'lib/MarpaX/ESLIF/Value.pm',
                                                                      'version' => 'v6.0.33.4'
                                                                    },
                                          'MarpaX::ESLIF::Value::Type' => {
                                                                            'file' => 'lib/MarpaX/ESLIF/Value/Type.pm',
                                                                            'version' => 'v6.0.33.4'
                                                                          }
                                        },
                          'requires' => {
                                          'Carp' => '0',
                                          'Devel::GlobalDestruction' => '0',
                                          'Encode' => '0',
                                          'JSON::MaybeXS' => '1.004000',
                                          'Math::BigFloat' => '0',
                                          'Math::BigInt' => '0',
                                          'XSLoader' => '0',
                                          'constant' => '0',
                                          'namespace::clean' => '0',
                                          'overload' => '0',
                                          'parent' => '0',
                                          'perl' => '5.010',
                                          'strict' => '0',
                                          'warnings' => '0'
                                        },
                          'resources' => {
                                           'bugtracker' => 'https://github.com/jddurand/c-marpaESLIFPerl/issues',
                                           'homepage' => 'https://metacpan.org/release/MarpaX-ESLIF',
                                           'repository' => 'git://github.com/jddurand/c-marpaESLIFPerl.git'
                                         },
                          'version' => '6.0.33.4',
                          'x_authority' => 'cpan:JDDPAUSE',
                          'x_generated_by_perl' => 'v5.26.1',
                          'x_serialization_backend' => 'YAML::Tiny version 1.73',
                          'x_spdx_expression' => 'Artistic-1.0-Perl OR GPL-1.0-or-later'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/MarpaX/ESLIF.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MarpaX::ESLIF'
                         },
                         {
                           'file' => 'lib/MarpaX/ESLIF/Base.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MarpaX::ESLIF::Base'
                         },
                         {
                           'file' => 'lib/MarpaX/ESLIF/Event/Type.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MarpaX::ESLIF::Event::Type'
                         },
                         {
                           'file' => 'lib/MarpaX/ESLIF/Grammar.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MarpaX::ESLIF::Grammar'
                         },
                         {
                           'file' => 'lib/MarpaX/ESLIF/Grammar/Properties.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MarpaX::ESLIF::Grammar::Properties'
                         },
                         {
                           'file' => 'lib/MarpaX/ESLIF/Grammar/Rule/Properties.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MarpaX::ESLIF::Grammar::Rule::Properties'
                         },
                         {
                           'file' => 'lib/MarpaX/ESLIF/Grammar/Symbol/Properties.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MarpaX::ESLIF::Grammar::Symbol::Properties'
                         },
                         {
                           'file' => 'lib/MarpaX/ESLIF/JSON.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MarpaX::ESLIF::JSON'
                         },
                         {
                           'file' => 'lib/MarpaX/ESLIF/JSON/Decoder.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MarpaX::ESLIF::JSON::Decoder'
                         },
                         {
                           'file' => 'lib/MarpaX/ESLIF/JSON/Decoder/RecognizerInterface.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MarpaX::ESLIF::JSON::Decoder::RecognizerInterface'
                         },
                         {
                           'file' => 'lib/MarpaX/ESLIF/JSON/Encoder.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MarpaX::ESLIF::JSON::Encoder'
                         },
                         {
                           'file' => 'lib/MarpaX/ESLIF/Logger/Level.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MarpaX::ESLIF::Logger::Level'
                         },
                         {
                           'file' => 'lib/MarpaX/ESLIF/Recognizer.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MarpaX::ESLIF::Recognizer'
                         },
                         {
                           'file' => 'lib/MarpaX/ESLIF/RegexCallout.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MarpaX::ESLIF::RegexCallout'
                         },
                         {
                           'file' => 'lib/MarpaX/ESLIF/Rule/PropertyBitSet.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MarpaX::ESLIF::Rule::PropertyBitSet'
                         },
                         {
                           'file' => 'lib/MarpaX/ESLIF/String.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MarpaX::ESLIF::String'
                         },
                         {
                           'file' => 'lib/MarpaX/ESLIF/Symbol.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MarpaX::ESLIF::Symbol'
                         },
                         {
                           'file' => 'lib/MarpaX/ESLIF/Symbol/EventBitSet.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MarpaX::ESLIF::Symbol::EventBitSet'
                         },
                         {
                           'file' => 'lib/MarpaX/ESLIF/Symbol/PropertyBitSet.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MarpaX::ESLIF::Symbol::PropertyBitSet'
                         },
                         {
                           'file' => 'lib/MarpaX/ESLIF/Symbol/Type.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MarpaX::ESLIF::Symbol::Type'
                         },
                         {
                           'file' => 'lib/MarpaX/ESLIF/Value.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MarpaX::ESLIF::Value'
                         },
                         {
                           'file' => 'lib/MarpaX/ESLIF/Value/Type.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MarpaX::ESLIF::Value::Type'
                         }
                       ],
          'needs_compiler' => 1,
          'no_index' => '^eg/;^examples/;^inc/;^share/;^t/;^xt/',
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/MarpaX-ESLIF.tar.gz',
          'prereq' => [
                        {
                          'requires' => 'Archive::Tar',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'Carp',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Carp',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'Config',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'Config::AutoConf',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'Cwd',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Data::Dumper',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Devel::GlobalDestruction',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Encode',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'ExtUtils::CBuilder',
                          'type' => 'configure_requires',
                          'version' => '0.280224'
                        },
                        {
                          'requires' => 'ExtUtils::Constant',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'File::Basename',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'File::Copy',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'File::Find',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'File::Path',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'File::Spec',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'File::Spec',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'File::Temp',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'File::Which',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'File::chdir',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'IO::Handle',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'IO::Handle',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'IPC::Open3',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'JSON::MaybeXS',
                          'type' => 'runtime_requires',
                          'version' => '1.004000'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Log::Any',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Log::Any::Adapter',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Math::BigFloat',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Math::BigInt',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'POSIX',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'POSIX',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'Perl::OSType',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'Probe::Perl',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Safe::Isa',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::Deep',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::Deep::NoTest',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More::UTF8',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Try::Tiny',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'Try::Tiny',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'XSLoader',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'constant',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'diagnostics',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'namespace::clean',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'open',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'overload',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'parent',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'build_requires',
                          'version' => '5.010'
                        },
                        {
                          'requires' => 'perl',
                          'type' => 'configure_requires',
                          'version' => '5.006'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => '5.010'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'strict',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'threads',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'threads::shared',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'utf8',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'warnings',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        }
                      ],
          'released' => 1709583520,
          'size_packed' => 13604470,
          'size_unpacked' => 14821488,
          'test_files' => [
                            't/00-compile.t',
                            't/00-report-prereqs.t',
                            't/advent.t',
                            't/allluacallbacks.t',
                            't/import_export.t',
                            't/json.t',
                            't/jsonWithSharedStream.t',
                            't/parameterizedRules.t',
                            't/resolver.t',
                            't/symbol.t',
                            't/test.t',
                            't/thread.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'requires' => {
                                                       'Config' => '0',
                                                       'ExtUtils::Constant' => '0',
                                                       'ExtUtils::MakeMaker' => '0',
                                                       'File::Find' => '0',
                                                       'File::Spec' => '0',
                                                       'IO::Handle' => '0',
                                                       'Probe::Perl' => '0',
                                                       'Try::Tiny' => '0',
                                                       'perl' => '5.010',
                                                       'strict' => '0',
                                                       'warnings' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'noes' => {
                                                 'warnings' => '0'
                                               },
                                     'requires' => {
                                                     'Carp' => '0',
                                                     'Config' => '0',
                                                     'Devel::GlobalDestruction' => '0',
                                                     'Encode' => '0',
                                                     'JSON::MaybeXS' => '1.004000',
                                                     'Math::BigFloat' => '0',
                                                     'Math::BigInt' => '0',
                                                     'XSLoader' => '0',
                                                     'constant' => '0',
                                                     'namespace::clean' => '0',
                                                     'overload' => '0',
                                                     'parent' => '0',
                                                     'strict' => '0',
                                                     'warnings' => '0'
                                                   }
                                   },
                      'test' => {
                                  'noes' => {
                                              'warnings' => '0'
                                            },
                                  'recommends' => {
                                                    'Log::Any' => '0',
                                                    'Log::Any::Adapter' => '0',
                                                    'Test::More' => '0.88',
                                                    'constant' => '0',
                                                    'diagnostics' => '0',
                                                    'threads' => '0',
                                                    'threads::shared' => '0'
                                                  },
                                  'requires' => {
                                                  'Carp' => '0',
                                                  'Config' => '0',
                                                  'Data::Dumper' => '0',
                                                  'Encode' => '0',
                                                  'ExtUtils::MakeMaker' => '0',
                                                  'File::Spec' => '0',
                                                  'IO::Handle' => '0',
                                                  'IPC::Open3' => '0',
                                                  'Log::Any' => '0',
                                                  'Log::Any::Adapter' => '0',
                                                  'Math::BigFloat' => '0',
                                                  'Math::BigInt' => '0',
                                                  'POSIX' => '0',
                                                  'Safe::Isa' => '0',
                                                  'Test::Deep' => '0',
                                                  'Test::Deep::NoTest' => '0',
                                                  'Test::More' => '0.88',
                                                  'Test::More::UTF8' => '0',
                                                  'Try::Tiny' => '0',
                                                  'diagnostics' => '0',
                                                  'open' => '0',
                                                  'perl' => '5.006',
                                                  'strict' => '0',
                                                  'utf8' => '0',
                                                  'warnings' => '0'
                                                },
                                  'suggests' => {
                                                  'blib' => '1.01'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'MarpaX-ESLIF'
        };
1;
