$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'Firefox::Marionette' => 'Automate the Firefox browser with the Marionette protocol',
                                  'Firefox::Marionette::Bookmark' => 'Represents a Firefox bookmark retrieved using the Marionette protocol',
                                  'Firefox::Marionette::Buttons' => 'Human readable mouse buttons for the Marionette protocol',
                                  'Firefox::Marionette::Cache' => 'Constants to describe actions on the cache',
                                  'Firefox::Marionette::Capabilities' => 'Represents Firefox Capabilities retrieved using the Marionette protocol',
                                  'Firefox::Marionette::Certificate' => 'Represents a x509 Certificate from Firefox',
                                  'Firefox::Marionette::Cookie' => 'Represents a Firefox cookie retrieved using the Marionette protocol',
                                  'Firefox::Marionette::Display' => 'Represents a display from the displays method',
                                  'Firefox::Marionette::Element' => 'Represents a Firefox element retrieved using the Marionette protocol',
                                  'Firefox::Marionette::Element::Rect' => 'Represents the box around an Element',
                                  'Firefox::Marionette::Exception' => 'Represents an base exception class for exceptions for Firefox::Marionette',
                                  'Firefox::Marionette::Exception::InsecureCertificate' => 'Represents a \'insecure certificate\' exception thrown by Firefox',
                                  'Firefox::Marionette::Exception::NoSuchAlert' => 'Represents a \'no such alert\' exception thrown by Firefox',
                                  'Firefox::Marionette::Exception::NotFound' => 'Represents a \'no such element\' exception thrown by Firefox',
                                  'Firefox::Marionette::Exception::Response' => 'Represents an exception thrown by Firefox',
                                  'Firefox::Marionette::Exception::StaleElement' => 'Represents a \'stale element reference\' exception thrown by Firefox',
                                  'Firefox::Marionette::Extension::HarExportTrigger' => 'Contains the HAR Export Trigger extension',
                                  'Firefox::Marionette::Extension::Stealth' => 'Contains the Stealth Extension',
                                  'Firefox::Marionette::GeoLocation' => 'Represents a GeoLocation for Firefox',
                                  'Firefox::Marionette::Image' => 'Represents an image from the images method',
                                  'Firefox::Marionette::Keys' => 'Human readable special keys for the Marionette protocol',
                                  'Firefox::Marionette::Link' => 'Represents a link from the links method',
                                  'Firefox::Marionette::LocalObject' => 'Parent class that represents a Firefox local object retrieved using the Marionette protocol',
                                  'Firefox::Marionette::Login' => 'Represents a login from the Firefox Password Manager',
                                  'Firefox::Marionette::Profile' => 'Represents a prefs.js Firefox Profile',
                                  'Firefox::Marionette::Proxy' => 'Represents a Proxy used by Firefox Capabilities using the Marionette protocol',
                                  'Firefox::Marionette::Response' => 'Represents a Marionette protocol response',
                                  'Firefox::Marionette::ShadowRoot' => 'Represents a Firefox shadow root retrieved using the Marionette protocol',
                                  'Firefox::Marionette::Timeouts' => 'Represents the timeouts for page loading, searching, and scripts.',
                                  'Firefox::Marionette::UpdateStatus' => 'Represents the resulting status of an Firefox update',
                                  'Firefox::Marionette::WebAuthn::Authenticator' => 'Represents a Firefox WebAuthn Authenticator',
                                  'Firefox::Marionette::WebAuthn::Credential' => 'Represents a Firefox WebAuthn Credential',
                                  'Firefox::Marionette::WebFrame' => 'Represents a Firefox web frame retrieved using the Marionette protocol',
                                  'Firefox::Marionette::WebWindow' => 'Represents a Firefox window retrieved using the Marionette protocol',
                                  'Firefox::Marionette::Window::Rect' => 'Represents the browser window\'s shape and size',
                                  'Waterfox::Marionette' => 'Automate the Waterfox browser with the Marionette protocol',
                                  'Waterfox::Marionette::Profile' => 'Represents a prefs.js Waterfox Profile'
                                },
          'author' => '',
          'dir_lib' => 'lib',
          'dir_t' => 't',
          'dirs' => 16,
          'dirs_array' => [
                            'lib/Firefox/Marionette/Element',
                            'lib/Firefox/Marionette/Exception',
                            'lib/Firefox/Marionette/Extension',
                            'lib/Firefox/Marionette/WebAuthn',
                            'lib/Firefox/Marionette/Window',
                            'lib/Firefox/Marionette',
                            'lib/Firefox',
                            'lib/Waterfox/Marionette',
                            'lib/Waterfox',
                            'lib',
                            't/addons/borderify',
                            't/addons/discogs-search',
                            't/addons',
                            't/author',
                            't/data',
                            't'
                          ],
          'dist' => 'Firefox-Marionette',
          'dynamic_config' => 1,
          'error' => {
                       'extracts_nicely' => 'expected Firefox-Marionette but got Firefox-Marionette-1.53'
                     },
          'extension' => 'tar.gz',
          'external_license_file' => 'LICENSE',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_changelog' => 'Changes',
          'file_license' => 'LICENSE',
          'file_makefile_pl' => 'Makefile.PL',
          'file_manifest' => 'MANIFEST',
          'file_meta_json' => 'META.json',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README,README.md',
          'files' => 51,
          'files_array' => [
                             'Changes',
                             'LICENSE',
                             'MANIFEST',
                             'META.json',
                             'META.yml',
                             'Makefile.PL',
                             'README',
                             'README.md',
                             'ca-bundle-for-firefox',
                             'check-firefox-certificate-authorities',
                             'firefox-passwords',
                             'lib/Firefox/Marionette/Bookmark.pm',
                             'lib/Firefox/Marionette/Buttons.pm',
                             'lib/Firefox/Marionette/Cache.pm',
                             'lib/Firefox/Marionette/Capabilities.pm',
                             'lib/Firefox/Marionette/Certificate.pm',
                             'lib/Firefox/Marionette/Cookie.pm',
                             'lib/Firefox/Marionette/Display.pm',
                             'lib/Firefox/Marionette/Element/Rect.pm',
                             'lib/Firefox/Marionette/Element.pm',
                             'lib/Firefox/Marionette/Exception/InsecureCertificate.pm',
                             'lib/Firefox/Marionette/Exception/NoSuchAlert.pm',
                             'lib/Firefox/Marionette/Exception/NotFound.pm',
                             'lib/Firefox/Marionette/Exception/Response.pm',
                             'lib/Firefox/Marionette/Exception/StaleElement.pm',
                             'lib/Firefox/Marionette/Exception.pm',
                             'lib/Firefox/Marionette/Extension/HarExportTrigger.pm',
                             'lib/Firefox/Marionette/Extension/Stealth.pm',
                             'lib/Firefox/Marionette/GeoLocation.pm',
                             'lib/Firefox/Marionette/Image.pm',
                             'lib/Firefox/Marionette/Keys.pm',
                             'lib/Firefox/Marionette/Link.pm',
                             'lib/Firefox/Marionette/LocalObject.pm',
                             'lib/Firefox/Marionette/Login.pm',
                             'lib/Firefox/Marionette/Profile.pm',
                             'lib/Firefox/Marionette/Proxy.pm',
                             'lib/Firefox/Marionette/Response.pm',
                             'lib/Firefox/Marionette/ShadowRoot.pm',
                             'lib/Firefox/Marionette/Timeouts.pm',
                             'lib/Firefox/Marionette/UpdateStatus.pm',
                             'lib/Firefox/Marionette/WebAuthn/Authenticator.pm',
                             'lib/Firefox/Marionette/WebAuthn/Credential.pm',
                             'lib/Firefox/Marionette/WebFrame.pm',
                             'lib/Firefox/Marionette/WebWindow.pm',
                             'lib/Firefox/Marionette/Window/Rect.pm',
                             'lib/Firefox/Marionette.pm',
                             'lib/Waterfox/Marionette/Profile.pm',
                             'lib/Waterfox/Marionette.pm',
                             'mozilla-head-check',
                             'setup-for-firefox-marionette-build.sh',
                             'ssh-auth-cmd-marionette'
                           ],
          'files_hash' => {
                            'Changes' => {
                                           'mtime' => 1709462618,
                                           'size' => 20098
                                         },
                            'LICENSE' => {
                                           'mtime' => 1704940980,
                                           'size' => 19984
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1709462618,
                                            'size' => 2742
                                          },
                            'META.json' => {
                                             'mtime' => 1709463130,
                                             'size' => 2795
                                           },
                            'META.yml' => {
                                            'mtime' => 1709463130,
                                            'size' => 1596
                                          },
                            'Makefile.PL' => {
                                               'mtime' => 1706946533,
                                               'noes' => {
                                                           'warnings' => '0'
                                                         },
                                               'requires' => {
                                                               'English' => '0',
                                                               'ExtUtils::MakeMaker' => '0',
                                                               'Fcntl' => '0',
                                                               'File::Spec' => '0',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 15274
                                             },
                            'README' => {
                                          'mtime' => 1709462618,
                                          'size' => 148795
                                        },
                            'README.md' => {
                                             'mtime' => 1709462618,
                                             'size' => 157457
                                           },
                            'ca-bundle-for-firefox' => {
                                                         'mtime' => 1709462618,
                                                         'size' => 7523
                                                       },
                            'check-firefox-certificate-authorities' => {
                                                                         'mtime' => 1709462618,
                                                                         'size' => 6576
                                                                       },
                            'firefox-passwords' => {
                                                     'mtime' => 1709462618,
                                                     'size' => 18376
                                                   },
                            'lib/Firefox/Marionette.pm' => {
                                                             'license' => 'Perl_5',
                                                             'module' => 'Firefox::Marionette',
                                                             'mtime' => 1709462618,
                                                             'recommends' => {
                                                                               'Firefox::Marionette::Extension::HarExportTrigger' => '0',
                                                                               'Firefox::Marionette::Extension::Stealth' => '0',
                                                                               'Win32' => '0',
                                                                               'Win32::Process' => '0',
                                                                               'Win32API::Registry' => '0'
                                                                             },
                                                             'requires' => {
                                                                             'Archive::Zip' => '0',
                                                                             'Carp' => '0',
                                                                             'Compress::Zlib' => '0',
                                                                             'Config' => '0',
                                                                             'Config::INI::Reader' => '0',
                                                                             'Crypt::URandom' => '0',
                                                                             'DirHandle' => '0',
                                                                             'English' => '0',
                                                                             'Exporter' => '0',
                                                                             'File::Find' => '0',
                                                                             'File::Path' => '0',
                                                                             'File::Spec' => '0',
                                                                             'File::Spec::Unix' => '0',
                                                                             'File::Spec::Win32' => '0',
                                                                             'File::Temp' => '0',
                                                                             'File::stat' => '0',
                                                                             'FileHandle' => '0',
                                                                             'Firefox::Marionette::Bookmark' => '0',
                                                                             'Firefox::Marionette::Cache' => '0',
                                                                             'Firefox::Marionette::Capabilities' => '0',
                                                                             'Firefox::Marionette::Certificate' => '0',
                                                                             'Firefox::Marionette::Cookie' => '0',
                                                                             'Firefox::Marionette::Display' => '0',
                                                                             'Firefox::Marionette::Element' => '0',
                                                                             'Firefox::Marionette::Element::Rect' => '0',
                                                                             'Firefox::Marionette::Exception' => '0',
                                                                             'Firefox::Marionette::Exception::Response' => '0',
                                                                             'Firefox::Marionette::GeoLocation' => '0',
                                                                             'Firefox::Marionette::Image' => '0',
                                                                             'Firefox::Marionette::Link' => '0',
                                                                             'Firefox::Marionette::Login' => '0',
                                                                             'Firefox::Marionette::Profile' => '0',
                                                                             'Firefox::Marionette::Proxy' => '0',
                                                                             'Firefox::Marionette::Response' => '0',
                                                                             'Firefox::Marionette::ShadowRoot' => '0',
                                                                             'Firefox::Marionette::Timeouts' => '0',
                                                                             'Firefox::Marionette::UpdateStatus' => '0',
                                                                             'Firefox::Marionette::WebAuthn::Authenticator' => '0',
                                                                             'Firefox::Marionette::WebAuthn::Credential' => '0',
                                                                             'Firefox::Marionette::WebFrame' => '0',
                                                                             'Firefox::Marionette::WebWindow' => '0',
                                                                             'Firefox::Marionette::Window::Rect' => '0',
                                                                             'IO::Handle' => '0',
                                                                             'IPC::Open3' => '0',
                                                                             'JSON' => '0',
                                                                             'MIME::Base64' => '0',
                                                                             'POSIX' => '0',
                                                                             'Scalar::Util' => '0',
                                                                             'Socket' => '0',
                                                                             'Symbol' => '0',
                                                                             'Text::CSV_XS' => '0',
                                                                             'Time::HiRes' => '0',
                                                                             'Time::Local' => '0',
                                                                             'URI' => '0',
                                                                             'URI::Escape' => '0',
                                                                             'Waterfox::Marionette::Profile' => '0',
                                                                             'XML::Parser' => '0',
                                                                             'parent' => '0',
                                                                             'strict' => '0',
                                                                             'warnings' => '0'
                                                                           },
                                                             'size' => 558517
                                                           },
                            'lib/Firefox/Marionette/Bookmark.pm' => {
                                                                      'license' => 'Perl_5',
                                                                      'module' => 'Firefox::Marionette::Bookmark',
                                                                      'mtime' => 1709462618,
                                                                      'requires' => {
                                                                                      'Exporter' => '0',
                                                                                      'URI::URL' => '0',
                                                                                      'URI::data' => '0',
                                                                                      'strict' => '0',
                                                                                      'warnings' => '0'
                                                                                    },
                                                                      'size' => 12932
                                                                    },
                            'lib/Firefox/Marionette/Buttons.pm' => {
                                                                     'license' => 'Perl_5',
                                                                     'module' => 'Firefox::Marionette::Buttons',
                                                                     'mtime' => 1709462618,
                                                                     'requires' => {
                                                                                     'Exporter' => '0',
                                                                                     'strict' => '0',
                                                                                     'warnings' => '0'
                                                                                   },
                                                                     'size' => 3264
                                                                   },
                            'lib/Firefox/Marionette/Cache.pm' => {
                                                                   'license' => 'Perl_5',
                                                                   'module' => 'Firefox::Marionette::Cache',
                                                                   'mtime' => 1709462618,
                                                                   'requires' => {
                                                                                   'Exporter' => '0',
                                                                                   'strict' => '0',
                                                                                   'warnings' => '0'
                                                                                 },
                                                                   'size' => 11296
                                                                 },
                            'lib/Firefox/Marionette/Capabilities.pm' => {
                                                                          'license' => 'Perl_5',
                                                                          'module' => 'Firefox::Marionette::Capabilities',
                                                                          'mtime' => 1709462618,
                                                                          'requires' => {
                                                                                          'strict' => '0',
                                                                                          'warnings' => '0'
                                                                                        },
                                                                          'size' => 13179
                                                                        },
                            'lib/Firefox/Marionette/Certificate.pm' => {
                                                                         'license' => 'Perl_5',
                                                                         'module' => 'Firefox::Marionette::Certificate',
                                                                         'mtime' => 1709462618,
                                                                         'requires' => {
                                                                                         'strict' => '0',
                                                                                         'warnings' => '0'
                                                                                       },
                                                                         'size' => 10532
                                                                       },
                            'lib/Firefox/Marionette/Cookie.pm' => {
                                                                    'license' => 'Perl_5',
                                                                    'module' => 'Firefox::Marionette::Cookie',
                                                                    'mtime' => 1709462618,
                                                                    'requires' => {
                                                                                    'strict' => '0',
                                                                                    'warnings' => '0'
                                                                                  },
                                                                    'size' => 5238
                                                                  },
                            'lib/Firefox/Marionette/Display.pm' => {
                                                                     'license' => 'Perl_5',
                                                                     'module' => 'Firefox::Marionette::Display',
                                                                     'mtime' => 1709462618,
                                                                     'requires' => {
                                                                                     'strict' => '0',
                                                                                     'warnings' => '0'
                                                                                   },
                                                                     'size' => 5550
                                                                   },
                            'lib/Firefox/Marionette/Element.pm' => {
                                                                     'license' => 'Perl_5',
                                                                     'module' => 'Firefox::Marionette::Element',
                                                                     'mtime' => 1709462618,
                                                                     'requires' => {
                                                                                     'Firefox::Marionette::LocalObject' => '0',
                                                                                     'parent' => '0',
                                                                                     'strict' => '0',
                                                                                     'warnings' => '0'
                                                                                   },
                                                                     'size' => 29811
                                                                   },
                            'lib/Firefox/Marionette/Element/Rect.pm' => {
                                                                          'license' => 'Perl_5',
                                                                          'module' => 'Firefox::Marionette::Element::Rect',
                                                                          'mtime' => 1709462618,
                                                                          'requires' => {
                                                                                          'strict' => '0',
                                                                                          'warnings' => '0'
                                                                                        },
                                                                          'size' => 3578
                                                                        },
                            'lib/Firefox/Marionette/Exception.pm' => {
                                                                       'license' => 'Perl_5',
                                                                       'module' => 'Firefox::Marionette::Exception',
                                                                       'mtime' => 1709462618,
                                                                       'requires' => {
                                                                                       'Carp' => '0',
                                                                                       'overload' => '0',
                                                                                       'strict' => '0',
                                                                                       'warnings' => '0'
                                                                                     },
                                                                       'size' => 3255
                                                                     },
                            'lib/Firefox/Marionette/Exception/InsecureCertificate.pm' => {
                                                                                           'license' => 'Perl_5',
                                                                                           'module' => 'Firefox::Marionette::Exception::InsecureCertificate',
                                                                                           'mtime' => 1709462618,
                                                                                           'requires' => {
                                                                                                           'Firefox::Marionette::Exception::Response' => '0',
                                                                                                           'parent' => '0',
                                                                                                           'strict' => '0',
                                                                                                           'warnings' => '0'
                                                                                                         },
                                                                                           'size' => 2912
                                                                                         },
                            'lib/Firefox/Marionette/Exception/NoSuchAlert.pm' => {
                                                                                   'license' => 'Perl_5',
                                                                                   'module' => 'Firefox::Marionette::Exception::NoSuchAlert',
                                                                                   'mtime' => 1709462618,
                                                                                   'requires' => {
                                                                                                   'Firefox::Marionette::Exception::Response' => '0',
                                                                                                   'parent' => '0',
                                                                                                   'strict' => '0',
                                                                                                   'warnings' => '0'
                                                                                                 },
                                                                                   'size' => 2876
                                                                                 },
                            'lib/Firefox/Marionette/Exception/NotFound.pm' => {
                                                                                'license' => 'Perl_5',
                                                                                'module' => 'Firefox::Marionette::Exception::NotFound',
                                                                                'mtime' => 1709462618,
                                                                                'requires' => {
                                                                                                'Firefox::Marionette::Exception::Response' => '0',
                                                                                                'parent' => '0',
                                                                                                'strict' => '0',
                                                                                                'warnings' => '0'
                                                                                              },
                                                                                'size' => 3042
                                                                              },
                            'lib/Firefox/Marionette/Exception/Response.pm' => {
                                                                                'license' => 'Perl_5',
                                                                                'module' => 'Firefox::Marionette::Exception::Response',
                                                                                'mtime' => 1709462618,
                                                                                'requires' => {
                                                                                                'Firefox::Marionette::Exception' => '0',
                                                                                                'parent' => '0',
                                                                                                'strict' => '0',
                                                                                                'warnings' => '0'
                                                                                              },
                                                                                'size' => 3677
                                                                              },
                            'lib/Firefox/Marionette/Exception/StaleElement.pm' => {
                                                                                    'license' => 'Perl_5',
                                                                                    'module' => 'Firefox::Marionette::Exception::StaleElement',
                                                                                    'mtime' => 1709462618,
                                                                                    'requires' => {
                                                                                                    'Firefox::Marionette::Exception::Response' => '0',
                                                                                                    'parent' => '0',
                                                                                                    'strict' => '0',
                                                                                                    'warnings' => '0'
                                                                                                  },
                                                                                    'size' => 2965
                                                                                  },
                            'lib/Firefox/Marionette/Extension/HarExportTrigger.pm' => {
                                                                                        'license' => 'Perl_5',
                                                                                        'module' => 'Firefox::Marionette::Extension::HarExportTrigger',
                                                                                        'mtime' => 1709462618,
                                                                                        'requires' => {
                                                                                                        'strict' => '0',
                                                                                                        'warnings' => '0'
                                                                                                      },
                                                                                        'size' => 46258
                                                                                      },
                            'lib/Firefox/Marionette/Extension/Stealth.pm' => {
                                                                               'license' => 'Perl_5',
                                                                               'module' => 'Firefox::Marionette::Extension::Stealth',
                                                                               'mtime' => 1709462618,
                                                                               'requires' => {
                                                                                               'Archive::Zip' => '0',
                                                                                               'strict' => '0',
                                                                                               'warnings' => '0'
                                                                                             },
                                                                               'size' => 13309
                                                                             },
                            'lib/Firefox/Marionette/GeoLocation.pm' => {
                                                                         'license' => 'Perl_5',
                                                                         'module' => 'Firefox::Marionette::GeoLocation',
                                                                         'mtime' => 1709462618,
                                                                         'requires' => {
                                                                                         'Encode' => '0',
                                                                                         'charnames' => '0',
                                                                                         'overload' => '0',
                                                                                         'strict' => '0',
                                                                                         'warnings' => '0'
                                                                                       },
                                                                         'size' => 10495
                                                                       },
                            'lib/Firefox/Marionette/Image.pm' => {
                                                                   'license' => 'Perl_5',
                                                                   'module' => 'Firefox::Marionette::Image',
                                                                   'mtime' => 1709462618,
                                                                   'requires' => {
                                                                                   'Firefox::Marionette::Element' => '0',
                                                                                   'URI::URL' => '0',
                                                                                   'parent' => '0',
                                                                                   'strict' => '0',
                                                                                   'warnings' => '0'
                                                                                 },
                                                                   'size' => 5144
                                                                 },
                            'lib/Firefox/Marionette/Keys.pm' => {
                                                                  'license' => 'Perl_5',
                                                                  'module' => 'Firefox::Marionette::Keys',
                                                                  'mtime' => 1709462618,
                                                                  'requires' => {
                                                                                  'Exporter' => '0',
                                                                                  'strict' => '0',
                                                                                  'warnings' => '0'
                                                                                },
                                                                  'size' => 8503
                                                                },
                            'lib/Firefox/Marionette/Link.pm' => {
                                                                  'license' => 'Perl_5',
                                                                  'module' => 'Firefox::Marionette::Link',
                                                                  'mtime' => 1709462618,
                                                                  'requires' => {
                                                                                  'Firefox::Marionette::Element' => '0',
                                                                                  'URI::URL' => '0',
                                                                                  'parent' => '0',
                                                                                  'strict' => '0',
                                                                                  'warnings' => '0'
                                                                                },
                                                                  'size' => 4918
                                                                },
                            'lib/Firefox/Marionette/LocalObject.pm' => {
                                                                         'license' => 'Perl_5',
                                                                         'module' => 'Firefox::Marionette::LocalObject',
                                                                         'mtime' => 1709462618,
                                                                         'requires' => {
                                                                                         'overload' => '0',
                                                                                         'strict' => '0',
                                                                                         'warnings' => '0'
                                                                                       },
                                                                         'size' => 4300
                                                                       },
                            'lib/Firefox/Marionette/Login.pm' => {
                                                                   'license' => 'Perl_5',
                                                                   'module' => 'Firefox::Marionette::Login',
                                                                   'mtime' => 1709462618,
                                                                   'requires' => {
                                                                                   'strict' => '0',
                                                                                   'warnings' => '0'
                                                                                 },
                                                                   'size' => 10594
                                                                 },
                            'lib/Firefox/Marionette/Profile.pm' => {
                                                                     'license' => 'Perl_5',
                                                                     'module' => 'Firefox::Marionette::Profile',
                                                                     'mtime' => 1709462618,
                                                                     'recommends' => {
                                                                                       'Win32' => '0'
                                                                                     },
                                                                     'requires' => {
                                                                                     'Config::INI::Reader' => '0',
                                                                                     'English' => '0',
                                                                                     'Fcntl' => '0',
                                                                                     'File::Spec' => '0',
                                                                                     'FileHandle' => '0',
                                                                                     'strict' => '0',
                                                                                     'warnings' => '0'
                                                                                   },
                                                                     'size' => 28546
                                                                   },
                            'lib/Firefox/Marionette/Proxy.pm' => {
                                                                   'license' => 'Perl_5',
                                                                   'module' => 'Firefox::Marionette::Proxy',
                                                                   'mtime' => 1709462618,
                                                                   'requires' => {
                                                                                   'strict' => '0',
                                                                                   'warnings' => '0'
                                                                                 },
                                                                   'size' => 9174
                                                                 },
                            'lib/Firefox/Marionette/Response.pm' => {
                                                                      'license' => 'Perl_5',
                                                                      'module' => 'Firefox::Marionette::Response',
                                                                      'mtime' => 1709462618,
                                                                      'requires' => {
                                                                                      'Firefox::Marionette::Exception::InsecureCertificate' => '0',
                                                                                      'Firefox::Marionette::Exception::NoSuchAlert' => '0',
                                                                                      'Firefox::Marionette::Exception::NotFound' => '0',
                                                                                      'Firefox::Marionette::Exception::Response' => '0',
                                                                                      'Firefox::Marionette::Exception::StaleElement' => '0',
                                                                                      'strict' => '0',
                                                                                      'warnings' => '0'
                                                                                    },
                                                                      'size' => 7207
                                                                    },
                            'lib/Firefox/Marionette/ShadowRoot.pm' => {
                                                                        'license' => 'Perl_5',
                                                                        'module' => 'Firefox::Marionette::ShadowRoot',
                                                                        'mtime' => 1709462618,
                                                                        'requires' => {
                                                                                        'Firefox::Marionette::LocalObject' => '0',
                                                                                        'parent' => '0',
                                                                                        'strict' => '0',
                                                                                        'warnings' => '0'
                                                                                      },
                                                                        'size' => 3246
                                                                      },
                            'lib/Firefox/Marionette/Timeouts.pm' => {
                                                                      'license' => 'Perl_5',
                                                                      'module' => 'Firefox::Marionette::Timeouts',
                                                                      'mtime' => 1709462618,
                                                                      'requires' => {
                                                                                      'strict' => '0',
                                                                                      'warnings' => '0'
                                                                                    },
                                                                      'size' => 4191
                                                                    },
                            'lib/Firefox/Marionette/UpdateStatus.pm' => {
                                                                          'license' => 'Perl_5',
                                                                          'module' => 'Firefox::Marionette::UpdateStatus',
                                                                          'mtime' => 1709462618,
                                                                          'requires' => {
                                                                                          'URI' => '0',
                                                                                          'strict' => '0',
                                                                                          'warnings' => '0'
                                                                                        },
                                                                          'size' => 12457
                                                                        },
                            'lib/Firefox/Marionette/WebAuthn/Authenticator.pm' => {
                                                                                    'license' => 'Perl_5',
                                                                                    'module' => 'Firefox::Marionette::WebAuthn::Authenticator',
                                                                                    'mtime' => 1709462618,
                                                                                    'requires' => {
                                                                                                    'strict' => '0',
                                                                                                    'warnings' => '0'
                                                                                                  },
                                                                                    'size' => 9925
                                                                                  },
                            'lib/Firefox/Marionette/WebAuthn/Credential.pm' => {
                                                                                 'license' => 'Perl_5',
                                                                                 'module' => 'Firefox::Marionette::WebAuthn::Credential',
                                                                                 'mtime' => 1709462618,
                                                                                 'requires' => {
                                                                                                 'strict' => '0',
                                                                                                 'warnings' => '0'
                                                                                               },
                                                                                 'size' => 8535
                                                                               },
                            'lib/Firefox/Marionette/WebFrame.pm' => {
                                                                      'license' => 'Perl_5',
                                                                      'module' => 'Firefox::Marionette::WebFrame',
                                                                      'mtime' => 1709462618,
                                                                      'requires' => {
                                                                                      'Firefox::Marionette::LocalObject' => '0',
                                                                                      'parent' => '0',
                                                                                      'strict' => '0',
                                                                                      'warnings' => '0'
                                                                                    },
                                                                      'size' => 2998
                                                                    },
                            'lib/Firefox/Marionette/WebWindow.pm' => {
                                                                       'license' => 'Perl_5',
                                                                       'module' => 'Firefox::Marionette::WebWindow',
                                                                       'mtime' => 1709462618,
                                                                       'requires' => {
                                                                                       'Firefox::Marionette::LocalObject' => '0',
                                                                                       'parent' => '0',
                                                                                       'strict' => '0',
                                                                                       'warnings' => '0'
                                                                                     },
                                                                       'size' => 3154
                                                                     },
                            'lib/Firefox/Marionette/Window/Rect.pm' => {
                                                                         'license' => 'Perl_5',
                                                                         'module' => 'Firefox::Marionette::Window::Rect',
                                                                         'mtime' => 1709462618,
                                                                         'requires' => {
                                                                                         'strict' => '0',
                                                                                         'warnings' => '0'
                                                                                       },
                                                                         'size' => 3882
                                                                       },
                            'lib/Waterfox/Marionette.pm' => {
                                                              'license' => 'Perl_5',
                                                              'module' => 'Waterfox::Marionette',
                                                              'mtime' => 1709462618,
                                                              'requires' => {
                                                                              'English' => '0',
                                                                              'Firefox::Marionette' => '0',
                                                                              'Waterfox::Marionette::Profile' => '0',
                                                                              'parent' => '0',
                                                                              'strict' => '0',
                                                                              'warnings' => '0'
                                                                            },
                                                              'size' => 5658
                                                            },
                            'lib/Waterfox/Marionette/Profile.pm' => {
                                                                      'license' => 'Perl_5',
                                                                      'module' => 'Waterfox::Marionette::Profile',
                                                                      'mtime' => 1709462618,
                                                                      'recommends' => {
                                                                                        'Win32' => '0'
                                                                                      },
                                                                      'requires' => {
                                                                                      'English' => '0',
                                                                                      'File::Spec' => '0',
                                                                                      'Firefox::Marionette::Profile' => '0',
                                                                                      'parent' => '0',
                                                                                      'strict' => '0',
                                                                                      'warnings' => '0'
                                                                                    },
                                                                      'size' => 7605
                                                                    },
                            'mozilla-head-check' => {
                                                      'mtime' => 1709462618,
                                                      'size' => 6082
                                                    },
                            'setup-for-firefox-marionette-build.sh' => {
                                                                         'mtime' => 1709462618,
                                                                         'size' => 9640
                                                                       },
                            'ssh-auth-cmd-marionette' => {
                                                           'mtime' => 1709462618,
                                                           'size' => 22256
                                                         },
                            't/00.load.t' => {
                                               'mtime' => 1704940981,
                                               'no_index' => 1,
                                               'requires' => {
                                                               'Test::More' => '0'
                                                             },
                                               'size' => 141
                                             },
                            't/01-marionette.t' => {
                                                     'mtime' => 1709462618,
                                                     'no_index' => 1,
                                                     'noes' => {
                                                                 'strict' => '0'
                                                               },
                                                     'recommends' => {
                                                                       'Crypt::URandom' => '0',
                                                                       'Win32::Process' => '0'
                                                                     },
                                                     'requires' => {
                                                                     'Compress::Zlib' => '0',
                                                                     'Config' => '0',
                                                                     'Cwd' => '0',
                                                                     'Digest::SHA' => '0',
                                                                     'Encode' => '0',
                                                                     'File::HomeDir' => '0',
                                                                     'Firefox::Marionette' => '0',
                                                                     'HTTP::Daemon' => '0',
                                                                     'HTTP::Response' => '0',
                                                                     'HTTP::Status' => '0',
                                                                     'IO::Socket::IP' => '0',
                                                                     'IO::Socket::SSL' => '0',
                                                                     'MIME::Base64' => '0',
                                                                     'Test::More' => '0.88',
                                                                     'Waterfox::Marionette' => '0',
                                                                     'strict' => '0',
                                                                     'warnings' => '0'
                                                                   },
                                                     'size' => 318448,
                                                     'suggests' => {
                                                                     'Firefox::Marionette::Buttons' => '0',
                                                                     'Firefox::Marionette::Keys' => '0',
                                                                     'PDF::API2' => '0',
                                                                     'idea' => '0'
                                                                   }
                                                   },
                            't/02-taint.t' => {
                                                'mtime' => 1704940981,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'File::Spec' => '0',
                                                                'Firefox::Marionette' => '0',
                                                                'Test::More' => '0.88',
                                                                'strict' => '0'
                                                              },
                                                'size' => 1930
                                              },
                            't/03-close.t' => {
                                                'mtime' => 1704940981,
                                                'no_index' => 1,
                                                'noes' => {
                                                            'warnings' => '0'
                                                          },
                                                'requires' => {
                                                                'Archive::Zip' => '0',
                                                                'Firefox::Marionette' => '0',
                                                                'IPC::Open3' => '0',
                                                                'JSON' => '0',
                                                                'XML::Parser' => '0',
                                                                'lib' => '0',
                                                                'strict' => '0',
                                                                'syscall_tests' => '0',
                                                                'warnings' => '0'
                                                              },
                                                'size' => 485
                                              },
                            't/03-closedir.t' => {
                                                   'mtime' => 1704940981,
                                                   'no_index' => 1,
                                                   'noes' => {
                                                               'warnings' => '0'
                                                             },
                                                   'requires' => {
                                                                   'Archive::Zip' => '0',
                                                                   'Firefox::Marionette' => '0',
                                                                   'XML::Parser' => '0',
                                                                   'lib' => '0',
                                                                   'strict' => '0',
                                                                   'syscall_tests' => '0',
                                                                   'warnings' => '0'
                                                                 },
                                                   'size' => 476
                                                 },
                            't/03-fork.t' => {
                                               'mtime' => 1704940981,
                                               'no_index' => 1,
                                               'noes' => {
                                                           'warnings' => '0'
                                                         },
                                               'requires' => {
                                                               'Firefox::Marionette' => '0',
                                                               'lib' => '0',
                                                               'strict' => '0',
                                                               'syscall_tests' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 508
                                             },
                            't/03-mkdir.t' => {
                                                'mtime' => 1704940981,
                                                'no_index' => 1,
                                                'noes' => {
                                                            'warnings' => '0'
                                                          },
                                                'requires' => {
                                                                'Firefox::Marionette' => '0',
                                                                'lib' => '0',
                                                                'strict' => '0',
                                                                'syscall_tests' => '0',
                                                                'warnings' => '0'
                                                              },
                                                'size' => 425
                                              },
                            't/03-opendir.t' => {
                                                  'mtime' => 1704940981,
                                                  'no_index' => 1,
                                                  'noes' => {
                                                              'warnings' => '0'
                                                            },
                                                  'requires' => {
                                                                  'Archive::Zip' => '0',
                                                                  'Firefox::Marionette' => '0',
                                                                  'XML::Parser' => '0',
                                                                  'lib' => '0',
                                                                  'strict' => '0',
                                                                  'syscall_tests' => '0',
                                                                  'warnings' => '0'
                                                                },
                                                  'size' => 488
                                                },
                            't/03-read.t' => {
                                               'mtime' => 1704940981,
                                               'no_index' => 1,
                                               'noes' => {
                                                           'warnings' => '0'
                                                         },
                                               'recommends' => {
                                                                 'Crypt::URandom' => '0',
                                                                 'FileHandle' => '0'
                                                               },
                                               'requires' => {
                                                               'Firefox::Marionette' => '0',
                                                               'JSON' => '0',
                                                               'lib' => '0',
                                                               'strict' => '0',
                                                               'syscall_tests' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 600
                                             },
                            't/03-seek.t' => {
                                               'mtime' => 1704940981,
                                               'no_index' => 1,
                                               'noes' => {
                                                           'warnings' => '0'
                                                         },
                                               'requires' => {
                                                               'Firefox::Marionette' => '0',
                                                               'lib' => '0',
                                                               'strict' => '0',
                                                               'syscall_tests' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 455
                                             },
                            't/03-stat.t' => {
                                               'mtime' => 1704940981,
                                               'no_index' => 1,
                                               'noes' => {
                                                           'warnings' => '0'
                                                         },
                                               'requires' => {
                                                               'Archive::Zip' => '0',
                                                               'Firefox::Marionette' => '0',
                                                               'lib' => '0',
                                                               'strict' => '0',
                                                               'syscall_tests' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 603
                                             },
                            't/03-sysopen.t' => {
                                                  'mtime' => 1704940981,
                                                  'no_index' => 1,
                                                  'noes' => {
                                                              'warnings' => '0'
                                                            },
                                                  'requires' => {
                                                                  'Firefox::Marionette' => '0',
                                                                  'lib' => '0',
                                                                  'strict' => '0',
                                                                  'syscall_tests' => '0',
                                                                  'warnings' => '0'
                                                                },
                                                  'size' => 506
                                                },
                            't/04-botd.t' => {
                                               'mtime' => 1709462618,
                                               'no_index' => 1,
                                               'recommends' => {
                                                                 'test_daemons' => '0'
                                                               },
                                               'requires' => {
                                                               'File::Spec' => '0',
                                                               'Firefox::Marionette' => '0',
                                                               'Test::More' => '0.88',
                                                               'lib' => '0',
                                                               'strict' => '0'
                                                             },
                                               'size' => 2567
                                             },
                            't/04-fingerprint.t' => {
                                                      'mtime' => 1709462618,
                                                      'no_index' => 1,
                                                      'recommends' => {
                                                                        'test_daemons' => '0'
                                                                      },
                                                      'requires' => {
                                                                      'File::Spec' => '0',
                                                                      'Firefox::Marionette' => '0',
                                                                      'Test::More' => '0.88',
                                                                      'lib' => '0',
                                                                      'strict' => '0'
                                                                    },
                                                      'size' => 3296
                                                    },
                            't/04-proxy.t' => {
                                                'mtime' => 1709462618,
                                                'no_index' => 1,
                                                'recommends' => {
                                                                  'test_daemons' => '0'
                                                                },
                                                'requires' => {
                                                                'Config' => '0',
                                                                'Crypt::URandom' => '0',
                                                                'File::Spec' => '0',
                                                                'Firefox::Marionette' => '0',
                                                                'MIME::Base64' => '0',
                                                                'Socket' => '0',
                                                                'Test::More' => '0.88',
                                                                'lib' => '0',
                                                                'strict' => '0'
                                                              },
                                                'size' => 13510,
                                                'suggests' => {
                                                                'web' => '0'
                                                              }
                                              },
                            't/04-webauthn.t' => {
                                                   'mtime' => 1709462618,
                                                   'no_index' => 1,
                                                   'requires' => {
                                                                   'Crypt::URandom' => '0',
                                                                   'Firefox::Marionette' => '0',
                                                                   'MIME::Base64' => '0',
                                                                   'Test::More' => '0.88',
                                                                   'strict' => '0'
                                                                 },
                                                   'size' => 12196
                                                 },
                            't/addons/borderify/borderify.js' => {
                                                                   'mtime' => 1705662693,
                                                                   'no_index' => 1,
                                                                   'size' => 96
                                                                 },
                            't/addons/borderify/manifest.json' => {
                                                                    'mtime' => 1705662693,
                                                                    'no_index' => 1,
                                                                    'size' => 427
                                                                  },
                            't/addons/discogs-search/README.md' => {
                                                                     'mtime' => 1704940981,
                                                                     'no_index' => 1,
                                                                     'size' => 497
                                                                   },
                            't/addons/discogs-search/manifest.json' => {
                                                                         'mtime' => 1704940981,
                                                                         'no_index' => 1,
                                                                         'size' => 525
                                                                       },
                            't/addons/test.xpi' => {
                                                     'mtime' => 1704940981,
                                                     'no_index' => 1,
                                                     'size' => 540
                                                   },
                            't/author/bulk_test.pl' => {
                                                         'mtime' => 1709462618,
                                                         'no_index' => 1,
                                                         'size' => 41952
                                                       },
                            't/data/1Passwordv7.csv' => {
                                                          'mtime' => 1704940981,
                                                          'no_index' => 1,
                                                          'size' => 631
                                                        },
                            't/data/1Passwordv8.1pux' => {
                                                           'mtime' => 1704940981,
                                                           'no_index' => 1,
                                                           'size' => 3607
                                                         },
                            't/data/aria.html' => {
                                                    'mtime' => 1704940981,
                                                    'no_index' => 1,
                                                    'size' => 558
                                                  },
                            't/data/bitwarden_export_org.csv' => {
                                                                   'mtime' => 1704940981,
                                                                   'no_index' => 1,
                                                                   'size' => 469
                                                                 },
                            't/data/bookmarks_chrome.html' => {
                                                                'mtime' => 1704940981,
                                                                'no_index' => 1,
                                                                'size' => 5923
                                                              },
                            't/data/bookmarks_edge.html' => {
                                                              'mtime' => 1704940981,
                                                              'no_index' => 1,
                                                              'size' => 5923
                                                            },
                            't/data/bookmarks_empty.html' => {
                                                               'mtime' => 1704940981,
                                                               'no_index' => 1,
                                                               'size' => 415
                                                             },
                            't/data/bookmarks_firefox.html' => {
                                                                 'mtime' => 1704940981,
                                                                 'no_index' => 1,
                                                                 'size' => 9278
                                                               },
                            't/data/bookmarks_firefox.json' => {
                                                                 'mtime' => 1704940981,
                                                                 'no_index' => 1,
                                                                 'size' => 3537
                                                               },
                            't/data/bookmarks_truncated.html' => {
                                                                   'mtime' => 1704940981,
                                                                   'no_index' => 1,
                                                                   'size' => 5692
                                                                 },
                            't/data/elements.html' => {
                                                        'mtime' => 1704940981,
                                                        'no_index' => 1,
                                                        'size' => 3714
                                                      },
                            't/data/iframe.html' => {
                                                      'mtime' => 1704940981,
                                                      'no_index' => 1,
                                                      'size' => 160
                                                    },
                            't/data/keepass.csv' => {
                                                      'mtime' => 1704940981,
                                                      'no_index' => 1,
                                                      'size' => 739
                                                    },
                            't/data/keepass1.xml' => {
                                                       'mtime' => 1704940981,
                                                       'no_index' => 1,
                                                       'size' => 2998
                                                     },
                            't/data/keepassxs.csv' => {
                                                        'mtime' => 1704940981,
                                                        'no_index' => 1,
                                                        'size' => 394
                                                      },
                            't/data/key4.db' => {
                                                  'mtime' => 1704940981,
                                                  'no_index' => 1,
                                                  'size' => 294912
                                                },
                            't/data/last_pass_example.csv' => {
                                                                'mtime' => 1704940981,
                                                                'no_index' => 1,
                                                                'size' => 595
                                                              },
                            't/data/logins.json' => {
                                                      'mtime' => 1704940981,
                                                      'no_index' => 1,
                                                      'size' => 642
                                                    },
                            't/data/visible.html' => {
                                                       'mtime' => 1704940981,
                                                       'no_index' => 1,
                                                       'size' => 1497
                                                     },
                            't/manifest.t' => {
                                                'mtime' => 1709462618,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'Test::More' => '0',
                                                                'perl' => '5.006',
                                                                'strict' => '0',
                                                                'warnings' => '0'
                                                              },
                                                'size' => 492,
                                                'suggests' => {
                                                                'Test::CheckManifest' => '0'
                                                              }
                                              },
                            't/pod-coverage.t' => {
                                                    'mtime' => 1704940981,
                                                    'no_index' => 1,
                                                    'requires' => {
                                                                    'Test::More' => '0'
                                                                  },
                                                    'size' => 577,
                                                    'suggests' => {
                                                                    'Test::Pod::Coverage' => '1.04'
                                                                  }
                                                  },
                            't/pod.t' => {
                                           'mtime' => 1704940981,
                                           'no_index' => 1,
                                           'requires' => {
                                                           'Test::More' => '0'
                                                         },
                                           'size' => 140,
                                           'suggests' => {
                                                           'Test::Pod' => '1.41'
                                                         }
                                         },
                            't/stub.pl' => {
                                             'mtime' => 1709462618,
                                             'no_index' => 1,
                                             'size' => 7264
                                           },
                            't/syscall_tests.pm' => {
                                                      'mtime' => 1704940981,
                                                      'no_index' => 1,
                                                      'recommends' => {
                                                                        'Win32' => '0',
                                                                        'Win32::Process' => '0',
                                                                        'Win32API::Registry' => '0'
                                                                      },
                                                      'requires' => {
                                                                      'Cwd' => '0',
                                                                      'Fcntl' => '0',
                                                                      'File::Path' => '0',
                                                                      'File::Spec' => '0',
                                                                      'Test::More' => '0.88',
                                                                      'strict' => '0',
                                                                      'warnings' => '0'
                                                                    },
                                                      'size' => 3790
                                                    },
                            't/test_daemons.pm' => {
                                                     'mtime' => 1709462618,
                                                     'no_index' => 1,
                                                     'size' => 37967
                                                   }
                          },
          'got_prereq_from' => 'META.yml',
          'ignored_files_array' => [
                                     't/00.load.t',
                                     't/01-marionette.t',
                                     't/02-taint.t',
                                     't/03-close.t',
                                     't/03-closedir.t',
                                     't/03-fork.t',
                                     't/03-mkdir.t',
                                     't/03-opendir.t',
                                     't/03-read.t',
                                     't/03-seek.t',
                                     't/03-stat.t',
                                     't/03-sysopen.t',
                                     't/04-botd.t',
                                     't/04-fingerprint.t',
                                     't/04-proxy.t',
                                     't/04-webauthn.t',
                                     't/addons/borderify/borderify.js',
                                     't/addons/borderify/manifest.json',
                                     't/addons/discogs-search/README.md',
                                     't/addons/discogs-search/manifest.json',
                                     't/addons/test.xpi',
                                     't/author/bulk_test.pl',
                                     't/data/1Passwordv7.csv',
                                     't/data/1Passwordv8.1pux',
                                     't/data/aria.html',
                                     't/data/bitwarden_export_org.csv',
                                     't/data/bookmarks_chrome.html',
                                     't/data/bookmarks_edge.html',
                                     't/data/bookmarks_empty.html',
                                     't/data/bookmarks_firefox.html',
                                     't/data/bookmarks_firefox.json',
                                     't/data/bookmarks_truncated.html',
                                     't/data/elements.html',
                                     't/data/iframe.html',
                                     't/data/keepass.csv',
                                     't/data/keepass1.xml',
                                     't/data/keepassxs.csv',
                                     't/data/key4.db',
                                     't/data/last_pass_example.csv',
                                     't/data/logins.json',
                                     't/data/visible.html',
                                     't/manifest.t',
                                     't/pod-coverage.t',
                                     't/pod.t',
                                     't/stub.pl',
                                     't/syscall_tests.pm',
                                     't/test_daemons.pm'
                                   ],
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 1,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 1,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 32,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 0,
                          'meta_yml_has_repository_resource' => 1,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 1,
                          'use_warnings' => 1
                        },
          'latest_mtime' => 1709463130,
          'license' => 'perl defined in META.yml defined in LICENSE',
          'license_file' => 'lib/Firefox/Marionette.pm,lib/Firefox/Marionette/Bookmark.pm,lib/Firefox/Marionette/Buttons.pm,lib/Firefox/Marionette/Cache.pm,lib/Firefox/Marionette/Capabilities.pm,lib/Firefox/Marionette/Certificate.pm,lib/Firefox/Marionette/Cookie.pm,lib/Firefox/Marionette/Display.pm,lib/Firefox/Marionette/Element.pm,lib/Firefox/Marionette/Element/Rect.pm,lib/Firefox/Marionette/Exception.pm,lib/Firefox/Marionette/Exception/InsecureCertificate.pm,lib/Firefox/Marionette/Exception/NoSuchAlert.pm,lib/Firefox/Marionette/Exception/NotFound.pm,lib/Firefox/Marionette/Exception/Response.pm,lib/Firefox/Marionette/Exception/StaleElement.pm,lib/Firefox/Marionette/Extension/HarExportTrigger.pm,lib/Firefox/Marionette/Extension/Stealth.pm,lib/Firefox/Marionette/GeoLocation.pm,lib/Firefox/Marionette/Image.pm,lib/Firefox/Marionette/Keys.pm,lib/Firefox/Marionette/Link.pm,lib/Firefox/Marionette/LocalObject.pm,lib/Firefox/Marionette/Login.pm,lib/Firefox/Marionette/Profile.pm,lib/Firefox/Marionette/Proxy.pm,lib/Firefox/Marionette/Response.pm,lib/Firefox/Marionette/ShadowRoot.pm,lib/Firefox/Marionette/Timeouts.pm,lib/Firefox/Marionette/UpdateStatus.pm,lib/Firefox/Marionette/WebAuthn/Authenticator.pm,lib/Firefox/Marionette/WebAuthn/Credential.pm,lib/Firefox/Marionette/WebFrame.pm,lib/Firefox/Marionette/WebWindow.pm,lib/Firefox/Marionette/Window/Rect.pm,lib/Waterfox/Marionette.pm,lib/Waterfox/Marionette/Profile.pm',
          'license_from_yaml' => 'perl',
          'license_in_pod' => 1,
          'license_type' => 'Perl_5',
          'licenses' => {
                          'Perl_5' => [
                                        'lib/Firefox/Marionette.pm',
                                        'lib/Firefox/Marionette/Bookmark.pm',
                                        'lib/Firefox/Marionette/Buttons.pm',
                                        'lib/Firefox/Marionette/Cache.pm',
                                        'lib/Firefox/Marionette/Capabilities.pm',
                                        'lib/Firefox/Marionette/Certificate.pm',
                                        'lib/Firefox/Marionette/Cookie.pm',
                                        'lib/Firefox/Marionette/Display.pm',
                                        'lib/Firefox/Marionette/Element.pm',
                                        'lib/Firefox/Marionette/Element/Rect.pm',
                                        'lib/Firefox/Marionette/Exception.pm',
                                        'lib/Firefox/Marionette/Exception/InsecureCertificate.pm',
                                        'lib/Firefox/Marionette/Exception/NoSuchAlert.pm',
                                        'lib/Firefox/Marionette/Exception/NotFound.pm',
                                        'lib/Firefox/Marionette/Exception/Response.pm',
                                        'lib/Firefox/Marionette/Exception/StaleElement.pm',
                                        'lib/Firefox/Marionette/Extension/HarExportTrigger.pm',
                                        'lib/Firefox/Marionette/Extension/Stealth.pm',
                                        'lib/Firefox/Marionette/GeoLocation.pm',
                                        'lib/Firefox/Marionette/Image.pm',
                                        'lib/Firefox/Marionette/Keys.pm',
                                        'lib/Firefox/Marionette/Link.pm',
                                        'lib/Firefox/Marionette/LocalObject.pm',
                                        'lib/Firefox/Marionette/Login.pm',
                                        'lib/Firefox/Marionette/Profile.pm',
                                        'lib/Firefox/Marionette/Proxy.pm',
                                        'lib/Firefox/Marionette/Response.pm',
                                        'lib/Firefox/Marionette/ShadowRoot.pm',
                                        'lib/Firefox/Marionette/Timeouts.pm',
                                        'lib/Firefox/Marionette/UpdateStatus.pm',
                                        'lib/Firefox/Marionette/WebAuthn/Authenticator.pm',
                                        'lib/Firefox/Marionette/WebAuthn/Credential.pm',
                                        'lib/Firefox/Marionette/WebFrame.pm',
                                        'lib/Firefox/Marionette/WebWindow.pm',
                                        'lib/Firefox/Marionette/Window/Rect.pm',
                                        'lib/Waterfox/Marionette.pm',
                                        'lib/Waterfox/Marionette/Profile.pm'
                                      ]
                        },
          'manifest_matches_dist' => 1,
          'meta_json' => {
                           'abstract' => 'Automate the Firefox browser with the Marionette protocol',
                           'author' => [
                                         'David Dick <ddick@cpan.org>'
                                       ],
                           'dynamic_config' => 1,
                           'generated_by' => 'ExtUtils::MakeMaker version 7.70, CPAN::Meta::Converter version 2.150010',
                           'license' => [
                                          'perl_5'
                                        ],
                           'meta-spec' => {
                                            'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                            'version' => 2
                                          },
                           'name' => 'Firefox-Marionette',
                           'no_index' => {
                                           'directory' => [
                                                            't',
                                                            'inc'
                                                          ]
                                         },
                           'prereqs' => {
                                          'build' => {
                                                       'requires' => {
                                                                       'Compress::Zlib' => '0',
                                                                       'Crypt::PasswdMD5' => '0',
                                                                       'Cwd' => '0',
                                                                       'Digest::SHA' => '0',
                                                                       'File::HomeDir' => '0',
                                                                       'HTTP::Daemon' => '0',
                                                                       'HTTP::Response' => '0',
                                                                       'HTTP::Status' => '0',
                                                                       'IO::Socket::IP' => '0',
                                                                       'IO::Socket::SSL' => '0',
                                                                       'PDF::API2' => '2.036',
                                                                       'Test::CheckManifest' => '0.9',
                                                                       'Test::More' => '0',
                                                                       'Test::Pod' => '1.41',
                                                                       'Test::Pod::Coverage' => '1.04'
                                                                     }
                                                     },
                                          'configure' => {
                                                           'requires' => {
                                                                           'ExtUtils::MakeMaker' => '0'
                                                                         }
                                                         },
                                          'runtime' => {
                                                         'requires' => {
                                                                         'Archive::Zip' => '0',
                                                                         'Config' => '0',
                                                                         'Config::INI::Reader' => '0',
                                                                         'Crypt::URandom' => '0',
                                                                         'DirHandle' => '0',
                                                                         'Encode' => '0',
                                                                         'English' => '0',
                                                                         'Exporter' => '0',
                                                                         'Fcntl' => '0',
                                                                         'File::Find' => '0',
                                                                         'File::Spec' => '0',
                                                                         'File::Temp' => '0',
                                                                         'FileHandle' => '0',
                                                                         'IO::Handle' => '0',
                                                                         'IPC::Open3' => '1.03',
                                                                         'JSON' => '0',
                                                                         'MIME::Base64' => '3.11',
                                                                         'POSIX' => '0',
                                                                         'Pod::Simple::Text' => '0',
                                                                         'Scalar::Util' => '0',
                                                                         'Socket' => '0',
                                                                         'Term::ReadKey' => '0',
                                                                         'Text::CSV_XS' => '1.35',
                                                                         'Time::HiRes' => '0',
                                                                         'Time::Local' => '0',
                                                                         'URI' => '1.61',
                                                                         'URI::Escape' => '0',
                                                                         'URI::URL' => '0',
                                                                         'URI::data' => '0',
                                                                         'XML::Parser' => '0',
                                                                         'overload' => '0',
                                                                         'parent' => '0',
                                                                         'perl' => '5.006'
                                                                       }
                                                       }
                                        },
                           'release_status' => 'stable',
                           'resources' => {
                                            'bugtracker' => {
                                                              'web' => 'https://github.com/david-dick/firefox-marionette/issues'
                                                            },
                                            'repository' => {
                                                              'type' => 'git',
                                                              'url' => 'https://github.com/david-dick/firefox-marionette',
                                                              'web' => 'https://github.com/david-dick/firefox-marionette'
                                                            }
                                          },
                           'version' => '1.53',
                           'x_serialization_backend' => 'JSON::PP version 4.16'
                         },
          'meta_json_is_parsable' => 1,
          'meta_json_spec_version' => 2,
          'meta_yml' => {
                          'abstract' => 'Automate the Firefox browser with the Marionette protocol',
                          'author' => [
                                        'David Dick <ddick@cpan.org>'
                                      ],
                          'build_requires' => {
                                                'Compress::Zlib' => '0',
                                                'Crypt::PasswdMD5' => '0',
                                                'Cwd' => '0',
                                                'Digest::SHA' => '0',
                                                'File::HomeDir' => '0',
                                                'HTTP::Daemon' => '0',
                                                'HTTP::Response' => '0',
                                                'HTTP::Status' => '0',
                                                'IO::Socket::IP' => '0',
                                                'IO::Socket::SSL' => '0',
                                                'PDF::API2' => '2.036',
                                                'Test::CheckManifest' => '0.9',
                                                'Test::More' => '0',
                                                'Test::Pod' => '1.41',
                                                'Test::Pod::Coverage' => '1.04'
                                              },
                          'configure_requires' => {
                                                    'ExtUtils::MakeMaker' => '0'
                                                  },
                          'dynamic_config' => '1',
                          'generated_by' => 'ExtUtils::MakeMaker version 7.70, CPAN::Meta::Converter version 2.150010',
                          'license' => 'perl',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'Firefox-Marionette',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc'
                                                         ]
                                        },
                          'requires' => {
                                          'Archive::Zip' => '0',
                                          'Config' => '0',
                                          'Config::INI::Reader' => '0',
                                          'Crypt::URandom' => '0',
                                          'DirHandle' => '0',
                                          'Encode' => '0',
                                          'English' => '0',
                                          'Exporter' => '0',
                                          'Fcntl' => '0',
                                          'File::Find' => '0',
                                          'File::Spec' => '0',
                                          'File::Temp' => '0',
                                          'FileHandle' => '0',
                                          'IO::Handle' => '0',
                                          'IPC::Open3' => '1.03',
                                          'JSON' => '0',
                                          'MIME::Base64' => '3.11',
                                          'POSIX' => '0',
                                          'Pod::Simple::Text' => '0',
                                          'Scalar::Util' => '0',
                                          'Socket' => '0',
                                          'Term::ReadKey' => '0',
                                          'Text::CSV_XS' => '1.35',
                                          'Time::HiRes' => '0',
                                          'Time::Local' => '0',
                                          'URI' => '1.61',
                                          'URI::Escape' => '0',
                                          'URI::URL' => '0',
                                          'URI::data' => '0',
                                          'XML::Parser' => '0',
                                          'overload' => '0',
                                          'parent' => '0',
                                          'perl' => '5.006'
                                        },
                          'resources' => {
                                           'bugtracker' => 'https://github.com/david-dick/firefox-marionette/issues',
                                           'repository' => 'https://github.com/david-dick/firefox-marionette'
                                         },
                          'version' => '1.53',
                          'x_serialization_backend' => 'CPAN::Meta::YAML version 0.018'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/Firefox/Marionette.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/Bookmark.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::Bookmark'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/Buttons.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::Buttons'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/Cache.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::Cache'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/Capabilities.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::Capabilities'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/Certificate.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::Certificate'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/Cookie.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::Cookie'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/Display.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::Display'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/Element.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::Element'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/Element/Rect.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::Element::Rect'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/Exception.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::Exception'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/Exception/InsecureCertificate.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::Exception::InsecureCertificate'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/Exception/NoSuchAlert.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::Exception::NoSuchAlert'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/Exception/NotFound.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::Exception::NotFound'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/Exception/Response.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::Exception::Response'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/Exception/StaleElement.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::Exception::StaleElement'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/Extension/HarExportTrigger.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::Extension::HarExportTrigger'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/Extension/Stealth.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::Extension::Stealth'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/GeoLocation.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::GeoLocation'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/Image.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::Image'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/Keys.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::Keys'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/Link.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::Link'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/LocalObject.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::LocalObject'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/Login.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::Login'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/Profile.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::Profile'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/Proxy.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::Proxy'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/Response.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::Response'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/ShadowRoot.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::ShadowRoot'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/Timeouts.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::Timeouts'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/UpdateStatus.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::UpdateStatus'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/WebAuthn/Authenticator.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::WebAuthn::Authenticator'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/WebAuthn/Credential.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::WebAuthn::Credential'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/WebFrame.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::WebFrame'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/WebWindow.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::WebWindow'
                         },
                         {
                           'file' => 'lib/Firefox/Marionette/Window/Rect.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Firefox::Marionette::Window::Rect'
                         },
                         {
                           'file' => 'lib/Waterfox/Marionette.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Waterfox::Marionette'
                         },
                         {
                           'file' => 'lib/Waterfox/Marionette/Profile.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Waterfox::Marionette::Profile'
                         }
                       ],
          'no_index' => '^inc/;^t/',
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/Firefox-Marionette.tar.gz',
          'prereq' => [
                        {
                          'is_prereq' => 1,
                          'requires' => 'Archive::Zip',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Compress::Zlib',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Config',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Config::INI::Reader',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Crypt::PasswdMD5',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Crypt::URandom',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Cwd',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Digest::SHA',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'DirHandle',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Encode',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'English',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Exporter',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Fcntl',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'File::Find',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'File::HomeDir',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'File::Spec',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'File::Temp',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'FileHandle',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'HTTP::Daemon',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'HTTP::Response',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'HTTP::Status',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'IO::Handle',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'IO::Socket::IP',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'IO::Socket::SSL',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'IPC::Open3',
                          'type' => 'runtime_requires',
                          'version' => '1.03'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'JSON',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'MIME::Base64',
                          'type' => 'runtime_requires',
                          'version' => '3.11'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'PDF::API2',
                          'type' => 'build_requires',
                          'version' => '2.036'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'POSIX',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Pod::Simple::Text',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Scalar::Util',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Socket',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Term::ReadKey',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::CheckManifest',
                          'type' => 'build_requires',
                          'version' => '0.9'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::Pod',
                          'type' => 'build_requires',
                          'version' => '1.41'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::Pod::Coverage',
                          'type' => 'build_requires',
                          'version' => '1.04'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Text::CSV_XS',
                          'type' => 'runtime_requires',
                          'version' => '1.35'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Time::HiRes',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Time::Local',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'URI',
                          'type' => 'runtime_requires',
                          'version' => '1.61'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'URI::Escape',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'URI::URL',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'URI::data',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'XML::Parser',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'overload',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'parent',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => '5.006'
                        }
                      ],
          'released' => 1709583464,
          'size_packed' => 398221,
          'size_unpacked' => 2114502,
          'test_files' => [
                            't/00.load.t',
                            't/01-marionette.t',
                            't/02-taint.t',
                            't/03-close.t',
                            't/03-closedir.t',
                            't/03-fork.t',
                            't/03-mkdir.t',
                            't/03-opendir.t',
                            't/03-read.t',
                            't/03-seek.t',
                            't/03-stat.t',
                            't/03-sysopen.t',
                            't/04-botd.t',
                            't/04-fingerprint.t',
                            't/04-proxy.t',
                            't/04-webauthn.t',
                            't/manifest.t',
                            't/pod-coverage.t',
                            't/pod.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'noes' => {
                                                   'warnings' => '0'
                                                 },
                                       'requires' => {
                                                       'English' => '0',
                                                       'ExtUtils::MakeMaker' => '0',
                                                       'Fcntl' => '0',
                                                       'File::Spec' => '0',
                                                       'strict' => '0',
                                                       'warnings' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'recommends' => {
                                                       'Win32' => '0',
                                                       'Win32::Process' => '0',
                                                       'Win32API::Registry' => '0'
                                                     },
                                     'requires' => {
                                                     'Archive::Zip' => '0',
                                                     'Carp' => '0',
                                                     'Compress::Zlib' => '0',
                                                     'Config' => '0',
                                                     'Config::INI::Reader' => '0',
                                                     'Crypt::URandom' => '0',
                                                     'DirHandle' => '0',
                                                     'Encode' => '0',
                                                     'English' => '0',
                                                     'Exporter' => '0',
                                                     'Fcntl' => '0',
                                                     'File::Find' => '0',
                                                     'File::Path' => '0',
                                                     'File::Spec' => '0',
                                                     'File::Spec::Unix' => '0',
                                                     'File::Spec::Win32' => '0',
                                                     'File::Temp' => '0',
                                                     'File::stat' => '0',
                                                     'FileHandle' => '0',
                                                     'IO::Handle' => '0',
                                                     'IPC::Open3' => '0',
                                                     'JSON' => '0',
                                                     'MIME::Base64' => '0',
                                                     'POSIX' => '0',
                                                     'Scalar::Util' => '0',
                                                     'Socket' => '0',
                                                     'Symbol' => '0',
                                                     'Text::CSV_XS' => '0',
                                                     'Time::HiRes' => '0',
                                                     'Time::Local' => '0',
                                                     'URI' => '0',
                                                     'URI::Escape' => '0',
                                                     'URI::URL' => '0',
                                                     'URI::data' => '0',
                                                     'XML::Parser' => '0',
                                                     'charnames' => '0',
                                                     'overload' => '0',
                                                     'parent' => '0',
                                                     'strict' => '0',
                                                     'warnings' => '0'
                                                   }
                                   },
                      'test' => {
                                  'noes' => {
                                              'strict' => '0',
                                              'warnings' => '0'
                                            },
                                  'recommends' => {
                                                    'Crypt::URandom' => '0',
                                                    'FileHandle' => '0',
                                                    'Win32' => '0',
                                                    'Win32::Process' => '0',
                                                    'Win32API::Registry' => '0'
                                                  },
                                  'requires' => {
                                                  'Archive::Zip' => '0',
                                                  'Compress::Zlib' => '0',
                                                  'Config' => '0',
                                                  'Crypt::URandom' => '0',
                                                  'Cwd' => '0',
                                                  'Digest::SHA' => '0',
                                                  'Encode' => '0',
                                                  'Fcntl' => '0',
                                                  'File::HomeDir' => '0',
                                                  'File::Path' => '0',
                                                  'File::Spec' => '0',
                                                  'HTTP::Daemon' => '0',
                                                  'HTTP::Response' => '0',
                                                  'HTTP::Status' => '0',
                                                  'IO::Socket::IP' => '0',
                                                  'IO::Socket::SSL' => '0',
                                                  'IPC::Open3' => '0',
                                                  'JSON' => '0',
                                                  'MIME::Base64' => '0',
                                                  'Socket' => '0',
                                                  'Test::More' => '0.88',
                                                  'XML::Parser' => '0',
                                                  'lib' => '0',
                                                  'perl' => '5.006',
                                                  'strict' => '0',
                                                  'warnings' => '0'
                                                },
                                  'suggests' => {
                                                  'PDF::API2' => '0',
                                                  'Test::CheckManifest' => '0',
                                                  'Test::Pod' => '1.41',
                                                  'Test::Pod::Coverage' => '1.04',
                                                  'idea' => '0',
                                                  'web' => '0'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'Firefox-Marionette'
        };
1;
