$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'Data::Navigation::Item' => 'Data object for navigation item.'
                                },
          'author' => '',
          'dir_t' => 't',
          'dir_xt' => 'xt',
          'dirs' => 8,
          'dirs_array' => [
                            'examples',
                            'inc/Module/Install',
                            'inc/Module',
                            'inc',
                            't/Data-Navigation-Item',
                            't',
                            'xt/Data-Navigation-Item',
                            'xt'
                          ],
          'dist' => 'Data-Navigation-Item',
          'dynamic_config' => 1,
          'error' => {
                       'extracts_nicely' => 'expected Data-Navigation-Item but got Data-Navigation-Item-0.01'
                     },
          'extension' => 'tar.gz',
          'external_license_file' => 'LICENSE',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_changelog' => 'Changes',
          'file_license' => 'LICENSE',
          'file_makefile_pl' => 'Makefile.PL',
          'file_manifest' => 'MANIFEST',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README',
          'file_signature' => 'SIGNATURE',
          'files' => 8,
          'files_array' => [
                             'Changes',
                             'Item.pm',
                             'LICENSE',
                             'MANIFEST',
                             'META.yml',
                             'Makefile.PL',
                             'README',
                             'SIGNATURE'
                           ],
          'files_hash' => {
                            'Changes' => {
                                           'mtime' => 1709553144,
                                           'size' => 49
                                         },
                            'Item.pm' => {
                                           'license' => 'BSD',
                                           'module' => 'Data::Navigation::Item',
                                           'mtime' => 1709552955,
                                           'requires' => {
                                                           'Mo' => '0',
                                                           'Mo::utils' => '0.09',
                                                           'Mo::utils::CSS' => '0.02',
                                                           'Mo::utils::URI' => '0',
                                                           'strict' => '0',
                                                           'warnings' => '0'
                                                         },
                                           'size' => 4336
                                         },
                            'LICENSE' => {
                                           'mtime' => 1707573409,
                                           'size' => 1299
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1709553148,
                                            'size' => 822
                                          },
                            'META.yml' => {
                                            'mtime' => 1709552961,
                                            'size' => 952
                                          },
                            'Makefile.PL' => {
                                               'mtime' => 1709249301,
                                               'requires' => {
                                                               'inc::Module::Install' => '0',
                                                               'lib' => '0',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 1106
                                             },
                            'README' => {
                                          'mtime' => 1709552961,
                                          'size' => 3822
                                        },
                            'SIGNATURE' => {
                                             'mtime' => 1709553150,
                                             'size' => 4149
                                           },
                            'examples/nav_item.pl' => {
                                                        'mtime' => 1709552957,
                                                        'no_index' => 1,
                                                        'size' => 655
                                                      },
                            'inc/Module/Install.pm' => {
                                                         'mtime' => 1709552960,
                                                         'no_index' => 1,
                                                         'size' => 11877
                                                       },
                            'inc/Module/Install/AuthorRequires.pm' => {
                                                                        'mtime' => 1709552961,
                                                                        'no_index' => 1,
                                                                        'size' => 601
                                                                      },
                            'inc/Module/Install/AuthorTests.pm' => {
                                                                     'mtime' => 1709552961,
                                                                     'no_index' => 1,
                                                                     'size' => 1165
                                                                   },
                            'inc/Module/Install/Base.pm' => {
                                                              'mtime' => 1709552961,
                                                              'no_index' => 1,
                                                              'size' => 1127
                                                            },
                            'inc/Module/Install/Can.pm' => {
                                                             'mtime' => 1709552961,
                                                             'no_index' => 1,
                                                             'size' => 3333
                                                           },
                            'inc/Module/Install/Fetch.pm' => {
                                                               'mtime' => 1709552961,
                                                               'no_index' => 1,
                                                               'size' => 2455
                                                             },
                            'inc/Module/Install/Makefile.pm' => {
                                                                  'mtime' => 1709552961,
                                                                  'no_index' => 1,
                                                                  'size' => 12063
                                                                },
                            'inc/Module/Install/Metadata.pm' => {
                                                                  'mtime' => 1709552961,
                                                                  'no_index' => 1,
                                                                  'size' => 18207
                                                                },
                            'inc/Module/Install/ReadmeFromPod.pm' => {
                                                                       'mtime' => 1709552961,
                                                                       'no_index' => 1,
                                                                       'size' => 4212
                                                                     },
                            'inc/Module/Install/Win32.pm' => {
                                                               'mtime' => 1709552961,
                                                               'no_index' => 1,
                                                               'size' => 1795
                                                             },
                            'inc/Module/Install/WriteAll.pm' => {
                                                                  'mtime' => 1709552961,
                                                                  'no_index' => 1,
                                                                  'size' => 1278
                                                                },
                            't/Data-Navigation-Item/01-use.t' => {
                                                                   'mtime' => 1707575958,
                                                                   'no_index' => 1,
                                                                   'requires' => {
                                                                                   'Test::More' => '0',
                                                                                   'Test::NoWarnings' => '0',
                                                                                   'strict' => '0',
                                                                                   'warnings' => '0'
                                                                                 },
                                                                   'size' => 181
                                                                 },
                            't/Data-Navigation-Item/02-version.t' => {
                                                                       'mtime' => 1707575958,
                                                                       'no_index' => 1,
                                                                       'requires' => {
                                                                                       'Data::Navigation::Item' => '0',
                                                                                       'Test::More' => '0',
                                                                                       'Test::NoWarnings' => '0',
                                                                                       'strict' => '0',
                                                                                       'warnings' => '0'
                                                                                     },
                                                                       'size' => 171
                                                                     },
                            't/Data-Navigation-Item/03-new.t' => {
                                                                   'mtime' => 1709547465,
                                                                   'no_index' => 1,
                                                                   'requires' => {
                                                                                   'Data::Navigation::Item' => '0',
                                                                                   'English' => '0',
                                                                                   'Error::Pure::Utils' => '0',
                                                                                   'Test::More' => '0',
                                                                                   'Test::NoWarnings' => '0',
                                                                                   'strict' => '0',
                                                                                   'warnings' => '0'
                                                                                 },
                                                                   'size' => 2292
                                                                 },
                            't/Data-Navigation-Item/04-class.t' => {
                                                                     'mtime' => 1709171242,
                                                                     'no_index' => 1,
                                                                     'requires' => {
                                                                                     'Data::Navigation::Item' => '0',
                                                                                     'Test::More' => '0',
                                                                                     'Test::NoWarnings' => '0',
                                                                                     'strict' => '0',
                                                                                     'warnings' => '0'
                                                                                   },
                                                                     'size' => 387
                                                                   },
                            't/Data-Navigation-Item/05-desc.t' => {
                                                                    'mtime' => 1709171283,
                                                                    'no_index' => 1,
                                                                    'requires' => {
                                                                                    'Data::Navigation::Item' => '0',
                                                                                    'Test::More' => '0',
                                                                                    'Test::NoWarnings' => '0',
                                                                                    'strict' => '0',
                                                                                    'warnings' => '0'
                                                                                  },
                                                                    'size' => 423
                                                                  },
                            't/Data-Navigation-Item/06-id.t' => {
                                                                  'mtime' => 1709249099,
                                                                  'no_index' => 1,
                                                                  'requires' => {
                                                                                  'Data::Navigation::Item' => '0',
                                                                                  'Test::More' => '0',
                                                                                  'Test::NoWarnings' => '0',
                                                                                  'strict' => '0',
                                                                                  'warnings' => '0'
                                                                                },
                                                                  'size' => 341
                                                                },
                            't/Data-Navigation-Item/07-image.t' => {
                                                                     'mtime' => 1709249138,
                                                                     'no_index' => 1,
                                                                     'requires' => {
                                                                                     'Data::Navigation::Item' => '0',
                                                                                     'Test::More' => '0',
                                                                                     'Test::NoWarnings' => '0',
                                                                                     'strict' => '0',
                                                                                     'warnings' => '0'
                                                                                   },
                                                                     'size' => 378
                                                                   },
                            't/Data-Navigation-Item/08-location.t' => {
                                                                        'mtime' => 1709249192,
                                                                        'no_index' => 1,
                                                                        'requires' => {
                                                                                        'Data::Navigation::Item' => '0',
                                                                                        'Test::More' => '0',
                                                                                        'Test::NoWarnings' => '0',
                                                                                        'strict' => '0',
                                                                                        'warnings' => '0'
                                                                                      },
                                                                        'size' => 426
                                                                      },
                            't/Data-Navigation-Item/09-title.t' => {
                                                                     'mtime' => 1709249219,
                                                                     'no_index' => 1,
                                                                     'requires' => {
                                                                                     'Data::Navigation::Item' => '0',
                                                                                     'Test::More' => '0',
                                                                                     'Test::NoWarnings' => '0',
                                                                                     'strict' => '0',
                                                                                     'warnings' => '0'
                                                                                   },
                                                                     'size' => 226
                                                                   },
                            'xt/Data-Navigation-Item/01-pod_coverage.t' => {
                                                                             'mtime' => 1707575958,
                                                                             'no_index' => 1,
                                                                             'size' => 177
                                                                           },
                            'xt/Data-Navigation-Item/02-pod.t' => {
                                                                    'mtime' => 1707575958,
                                                                    'no_index' => 1,
                                                                    'size' => 319
                                                                  }
                          },
          'got_prereq_from' => 'META.yml',
          'ignored_files_array' => [
                                     'examples/nav_item.pl',
                                     'inc/Module/Install.pm',
                                     'inc/Module/Install/AuthorRequires.pm',
                                     'inc/Module/Install/AuthorTests.pm',
                                     'inc/Module/Install/Base.pm',
                                     'inc/Module/Install/Can.pm',
                                     'inc/Module/Install/Fetch.pm',
                                     'inc/Module/Install/Makefile.pm',
                                     'inc/Module/Install/Metadata.pm',
                                     'inc/Module/Install/ReadmeFromPod.pm',
                                     'inc/Module/Install/Win32.pm',
                                     'inc/Module/Install/WriteAll.pm',
                                     't/Data-Navigation-Item/01-use.t',
                                     't/Data-Navigation-Item/02-version.t',
                                     't/Data-Navigation-Item/03-new.t',
                                     't/Data-Navigation-Item/04-class.t',
                                     't/Data-Navigation-Item/05-desc.t',
                                     't/Data-Navigation-Item/06-id.t',
                                     't/Data-Navigation-Item/07-image.t',
                                     't/Data-Navigation-Item/08-location.t',
                                     't/Data-Navigation-Item/09-title.t',
                                     'xt/Data-Navigation-Item/01-pod_coverage.t',
                                     'xt/Data-Navigation-Item/02-pod.t'
                                   ],
          'included_modules' => [
                                  'Module::Install',
                                  'Module::Install::AuthorRequires',
                                  'Module::Install::AuthorTests',
                                  'Module::Install::Base',
                                  'Module::Install::Can',
                                  'Module::Install::Fetch',
                                  'Module::Install::Makefile',
                                  'Module::Install::Metadata',
                                  'Module::Install::ReadmeFromPod',
                                  'Module::Install::Win32',
                                  'Module::Install::WriteAll'
                                ],
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 0,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 1,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 31,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 0,
                          'meta_yml_has_repository_resource' => 1,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 1,
                          'use_warnings' => 1
                        },
          'latest_mtime' => 1709553150,
          'license' => 'bsd defined in META.yml defined in LICENSE',
          'license_file' => 'Item.pm',
          'license_from_yaml' => 'bsd',
          'license_in_pod' => 1,
          'license_type' => 'BSD',
          'licenses' => {
                          'BSD' => [
                                     'Item.pm'
                                   ]
                        },
          'manifest_matches_dist' => 1,
          'meta_yml' => {
                          'abstract' => 'Data object for navigation item.',
                          'author' => [
                                        'Michal Josef Spacek <skim@cpan.org>'
                                      ],
                          'build_requires' => {
                                                'English' => '0',
                                                'Error::Pure::Utils' => '0',
                                                'ExtUtils::MakeMaker' => '6.36',
                                                'Test::More' => '0',
                                                'Test::NoWarnings' => '0'
                                              },
                          'configure_requires' => {
                                                    'ExtUtils::MakeMaker' => '6.36'
                                                  },
                          'distribution_type' => 'module',
                          'dynamic_config' => '1',
                          'generated_by' => 'Module::Install version 1.21',
                          'license' => 'bsd',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'Data-Navigation-Item',
                          'no_index' => {
                                          'directory' => [
                                                           'examples',
                                                           'inc',
                                                           't',
                                                           'xt'
                                                         ]
                                        },
                          'requires' => {
                                          'Mo' => '0',
                                          'Mo::utils' => '0.09',
                                          'Mo::utils::CSS' => '0.02',
                                          'Mo::utils::URI' => '0',
                                          'perl' => '5.8.0'
                                        },
                          'resources' => {
                                           'bugtracker' => 'https://github.com/michal-josef-spacek/Data-Navigation-Item/issues',
                                           'homepage' => 'https://github.com/michal-josef-spacek/Data-Navigation-Item',
                                           'license' => 'http://opensource.org/licenses/bsd-license.php',
                                           'repository' => 'git://github.com/michal-josef-spacek/Data-Navigation-Item'
                                         },
                          'version' => '0.01'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'module_install' => {
                                'version' => '1.21'
                              },
          'modules' => [
                         {
                           'file' => 'Item.pm',
                           'in_basedir' => 1,
                           'in_lib' => 0,
                           'module' => 'Data::Navigation::Item'
                         }
                       ],
          'no_index' => '^examples/;^inc/;^t/;^xt/',
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/Data-Navigation-Item.tar.gz',
          'prereq' => [
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'English',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Error::Pure::Utils',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'build_requires',
                          'version' => '6.36'
                        },
                        {
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'configure_requires',
                          'version' => '6.36'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Mo',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Mo::utils',
                          'type' => 'runtime_requires',
                          'version' => '0.09'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Mo::utils::CSS',
                          'type' => 'runtime_requires',
                          'version' => '0.02'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Mo::utils::URI',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::NoWarnings',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => '5.8.0'
                        }
                      ],
          'released' => 1709583447,
          'size_packed' => 26564,
          'size_unpacked' => 80624,
          'test_files' => [
                            't/Data-Navigation-Item/01-use.t',
                            't/Data-Navigation-Item/02-version.t',
                            't/Data-Navigation-Item/03-new.t',
                            't/Data-Navigation-Item/04-class.t',
                            't/Data-Navigation-Item/05-desc.t',
                            't/Data-Navigation-Item/06-id.t',
                            't/Data-Navigation-Item/07-image.t',
                            't/Data-Navigation-Item/08-location.t',
                            't/Data-Navigation-Item/09-title.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'requires' => {
                                                       'lib' => '0',
                                                       'strict' => '0',
                                                       'warnings' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'requires' => {
                                                     'Mo' => '0',
                                                     'Mo::utils' => '0.09',
                                                     'Mo::utils::CSS' => '0.02',
                                                     'Mo::utils::URI' => '0',
                                                     'strict' => '0',
                                                     'warnings' => '0'
                                                   }
                                   },
                      'test' => {
                                  'requires' => {
                                                  'English' => '0',
                                                  'Error::Pure::Utils' => '0',
                                                  'Test::More' => '0',
                                                  'Test::NoWarnings' => '0',
                                                  'strict' => '0',
                                                  'warnings' => '0'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'Data-Navigation-Item'
        };
1;
