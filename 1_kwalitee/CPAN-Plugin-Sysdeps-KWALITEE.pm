$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'CPAN::Plugin::Sysdeps' => 'a CPAN.pm plugin for installing system dependencies',
                                  'CPAN::Plugin::Sysdeps::Mapping' => 'a static mapping of CPAN modules to system packages',
                                  'cpan-sysdeps' => 'report system dependencies for CPAN modules'
                                },
          'author' => '',
          'dir_lib' => 'lib',
          'dir_t' => 't',
          'dirs' => 8,
          'dirs_array' => [
                            'lib/CPAN/Plugin/Sysdeps',
                            'lib/CPAN/Plugin',
                            'lib/CPAN',
                            'lib',
                            'script',
                            't/lib',
                            't/mapping',
                            't'
                          ],
          'dist' => 'CPAN-Plugin-Sysdeps',
          'dynamic_config' => 1,
          'error' => {
                       'extracts_nicely' => 'expected CPAN-Plugin-Sysdeps but got CPAN-Plugin-Sysdeps-0.75'
                     },
          'extension' => 'tar.gz',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_changelog' => 'Changes',
          'file_makefile_pl' => 'Makefile.PL',
          'file_manifest' => 'MANIFEST',
          'file_manifest_skip' => 'MANIFEST.SKIP',
          'file_meta_json' => 'META.json',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README.md',
          'files' => 11,
          'files_array' => [
                             'Changes',
                             'MANIFEST',
                             'MANIFEST.SKIP',
                             'META.json',
                             'META.yml',
                             'Makefile.PL',
                             'README.md',
                             'lib/CPAN/Plugin/Sysdeps/Mapping.pm',
                             'lib/CPAN/Plugin/Sysdeps.pm',
                             'script/cpan-sysdeps',
                             'script/test-sysdeps-roundtrip'
                           ],
          'files_hash' => {
                            'Changes' => {
                                           'mtime' => 1709485244,
                                           'size' => 4994
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1709485308,
                                            'size' => 544
                                          },
                            'MANIFEST.SKIP' => {
                                                 'mtime' => 1709381840,
                                                 'size' => 1025
                                               },
                            'META.json' => {
                                             'mtime' => 1709485308,
                                             'size' => 1168
                                           },
                            'META.yml' => {
                                            'mtime' => 1709485308,
                                            'size' => 691
                                          },
                            'Makefile.PL' => {
                                               'mtime' => 1514718973,
                                               'requires' => {
                                                               'ExtUtils::MakeMaker' => '0',
                                                               'strict' => '0'
                                                             },
                                               'size' => 1192
                                             },
                            'README.md' => {
                                             'mtime' => 1694870490,
                                             'size' => 1142
                                           },
                            'lib/CPAN/Plugin/Sysdeps.pm' => {
                                                              'license' => 'Perl_5',
                                                              'module' => 'CPAN::Plugin::Sysdeps',
                                                              'mtime' => 1709485252,
                                                              'noes' => {
                                                                          'warnings' => '0'
                                                                        },
                                                              'recommends' => {
                                                                                'Data::Dumper' => '0',
                                                                                'IPC::Open3' => '0',
                                                                                'Symbol' => '0'
                                                                              },
                                                              'requires' => {
                                                                              'List::Util' => '0',
                                                                              'constant' => '0',
                                                                              'strict' => '0',
                                                                              'warnings' => '0'
                                                                            },
                                                              'size' => 31304,
                                                              'suggests' => {
                                                                              'Hash::Util' => '0'
                                                                            }
                                                            },
                            'lib/CPAN/Plugin/Sysdeps/Mapping.pm' => {
                                                                      'module' => 'CPAN::Plugin::Sysdeps::Mapping',
                                                                      'mtime' => 1709485258,
                                                                      'requires' => {
                                                                                      'constant' => '0',
                                                                                      'strict' => '0',
                                                                                      'warnings' => '0'
                                                                                    },
                                                                      'size' => 111650
                                                                    },
                            'script/cpan-sysdeps' => {
                                                       'mtime' => 1540936823,
                                                       'size' => 5025
                                                     },
                            'script/test-sysdeps-roundtrip' => {
                                                                 'mtime' => 1556274809,
                                                                 'size' => 3113
                                                               },
                            't/basic.t' => {
                                             'mtime' => 1464033077,
                                             'no_index' => 1,
                                             'requires' => {
                                                             'CPAN::Plugin::Sysdeps' => '0',
                                                             'FindBin' => '0',
                                                             'Test::More' => '0',
                                                             'TestUtil' => '0',
                                                             'lib' => '0',
                                                             'strict' => '0',
                                                             'warnings' => '0'
                                                           },
                                             'size' => 537
                                           },
                            't/code.t' => {
                                            'mtime' => 1464033014,
                                            'no_index' => 1,
                                            'requires' => {
                                                            'CPAN::Plugin::Sysdeps' => '0',
                                                            'FindBin' => '0',
                                                            'Test::More' => '0',
                                                            'TestUtil' => '0',
                                                            'lib' => '0',
                                                            'strict' => '0',
                                                            'warnings' => '0'
                                                          },
                                            'size' => 581
                                          },
                            't/detect_linux_distribution.t' => {
                                                                 'mtime' => 1709484976,
                                                                 'no_index' => 1,
                                                                 'noes' => {
                                                                             'warnings' => '0'
                                                                           },
                                                                 'requires' => {
                                                                                 'CPAN::Plugin::Sysdeps' => '0',
                                                                                 'File::Temp' => '0',
                                                                                 'FindBin' => '0',
                                                                                 'Getopt::Long' => '0',
                                                                                 'List::Util' => '0',
                                                                                 'Test::More' => '0',
                                                                                 'TestUtil' => '0',
                                                                                 'lib' => '0',
                                                                                 'strict' => '0',
                                                                                 'warnings' => '0'
                                                                               },
                                                                 'size' => 10011
                                                               },
                            't/filtuninst.t' => {
                                                  'mtime' => 1708369616,
                                                  'no_index' => 1,
                                                  'requires' => {
                                                                  'CPAN::Plugin::Sysdeps' => '0',
                                                                  'FindBin' => '0',
                                                                  'Test::More' => '0',
                                                                  'TestUtil' => '0',
                                                                  'lib' => '0',
                                                                  'strict' => '0',
                                                                  'warnings' => '0'
                                                                },
                                                  'size' => 934
                                                },
                            't/instcmd.t' => {
                                               'mtime' => 1519465985,
                                               'no_index' => 1,
                                               'requires' => {
                                                               'CPAN::Plugin::Sysdeps' => '0',
                                                               'FindBin' => '0',
                                                               'Test::More' => '0',
                                                               'TestUtil' => '0',
                                                               'lib' => '0',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 2874
                                             },
                            't/lib/TestUtil.pm' => {
                                                     'mtime' => 1708369782,
                                                     'no_index' => 1,
                                                     'recommends' => {
                                                                       'CPAN' => '0'
                                                                     },
                                                     'requires' => {
                                                                     'Exporter' => '0',
                                                                     'strict' => '0',
                                                                     'vars' => '0',
                                                                     'warnings' => '0'
                                                                   },
                                                     'size' => 642,
                                                     'suggests' => {
                                                                     'CPAN::Distribution' => '0'
                                                                   }
                                                   },
                            't/mapping/code.pl' => {
                                                     'mtime' => 1478376426,
                                                     'no_index' => 1,
                                                     'size' => 430
                                                   },
                            't/mapping/fail_likelinuxdistro.pl' => {
                                                                     'mtime' => 1461614022,
                                                                     'no_index' => 1,
                                                                     'size' => 116
                                                                   },
                            't/mapping/sample.pl' => {
                                                       'mtime' => 1556274809,
                                                       'no_index' => 1,
                                                       'size' => 614
                                                     },
                            't/sample.t' => {
                                              'mtime' => 1556274809,
                                              'no_index' => 1,
                                              'requires' => {
                                                              'CPAN::Plugin::Sysdeps' => '0',
                                                              'FindBin' => '0',
                                                              'Test::More' => '0',
                                                              'TestUtil' => '0',
                                                              'lib' => '0',
                                                              'strict' => '0',
                                                              'warnings' => '0'
                                                            },
                                              'size' => 2875
                                            },
                            't/traverse.t' => {
                                                'mtime' => 1709482953,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'CPAN::Plugin::Sysdeps' => '0',
                                                                'FindBin' => '0',
                                                                'Test::More' => '0',
                                                                'TestUtil' => '0',
                                                                'lib' => '0',
                                                                'strict' => '0',
                                                                'warnings' => '0'
                                                              },
                                                'size' => 909
                                              }
                          },
          'got_prereq_from' => 'META.yml',
          'ignored_files_array' => [
                                     't/basic.t',
                                     't/code.t',
                                     't/detect_linux_distribution.t',
                                     't/filtuninst.t',
                                     't/instcmd.t',
                                     't/lib/TestUtil.pm',
                                     't/mapping/code.pl',
                                     't/mapping/fail_likelinuxdistro.pl',
                                     't/mapping/sample.pl',
                                     't/sample.t',
                                     't/traverse.t'
                                   ],
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 1,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 0,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 31,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 0,
                          'meta_yml_has_repository_resource' => 1,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 1,
                          'use_warnings' => 1
                        },
          'latest_mtime' => 1709485308,
          'license' => 'perl defined in META.yml',
          'license_file' => 'lib/CPAN/Plugin/Sysdeps.pm',
          'license_from_yaml' => 'perl',
          'license_in_pod' => 1,
          'license_type' => 'Perl_5',
          'licenses' => {
                          'Perl_5' => [
                                        'lib/CPAN/Plugin/Sysdeps.pm'
                                      ]
                        },
          'manifest_matches_dist' => 1,
          'meta_json' => {
                           'abstract' => 'CPAN.pm plugin for installing external dependencies',
                           'author' => [
                                         'Slaven Rezic <srezic@cpan.org>'
                                       ],
                           'dynamic_config' => 1,
                           'generated_by' => 'ExtUtils::MakeMaker version 7.64, CPAN::Meta::Converter version 2.150010',
                           'license' => [
                                          'perl_5'
                                        ],
                           'meta-spec' => {
                                            'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                            'version' => 2
                                          },
                           'name' => 'CPAN-Plugin-Sysdeps',
                           'no_index' => {
                                           'directory' => [
                                                            't',
                                                            'inc'
                                                          ]
                                         },
                           'prereqs' => {
                                          'build' => {
                                                       'requires' => {
                                                                       'ExtUtils::MakeMaker' => '0'
                                                                     }
                                                     },
                                          'configure' => {
                                                           'requires' => {
                                                                           'ExtUtils::MakeMaker' => '0'
                                                                         }
                                                         },
                                          'runtime' => {
                                                         'requires' => {
                                                                         'List::Util' => '0',
                                                                         'Test::More' => '0',
                                                                         'perl' => '5.006'
                                                                       }
                                                       }
                                        },
                           'release_status' => 'stable',
                           'resources' => {
                                            'repository' => {
                                                              'type' => 'git',
                                                              'url' => 'git://github.com/eserte/cpan-plugin-sysdeps.git'
                                                            }
                                          },
                           'version' => '0.75',
                           'x_serialization_backend' => 'JSON::PP version 4.07'
                         },
          'meta_json_is_parsable' => 1,
          'meta_json_spec_version' => 2,
          'meta_yml' => {
                          'abstract' => 'CPAN.pm plugin for installing external dependencies',
                          'author' => [
                                        'Slaven Rezic <srezic@cpan.org>'
                                      ],
                          'build_requires' => {
                                                'ExtUtils::MakeMaker' => '0'
                                              },
                          'configure_requires' => {
                                                    'ExtUtils::MakeMaker' => '0'
                                                  },
                          'dynamic_config' => '1',
                          'generated_by' => 'ExtUtils::MakeMaker version 7.64, CPAN::Meta::Converter version 2.150010',
                          'license' => 'perl',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'CPAN-Plugin-Sysdeps',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc'
                                                         ]
                                        },
                          'requires' => {
                                          'List::Util' => '0',
                                          'Test::More' => '0',
                                          'perl' => '5.006'
                                        },
                          'resources' => {
                                           'repository' => 'git://github.com/eserte/cpan-plugin-sysdeps.git'
                                         },
                          'version' => '0.75',
                          'x_serialization_backend' => 'CPAN::Meta::YAML version 0.018'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/CPAN/Plugin/Sysdeps.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'CPAN::Plugin::Sysdeps'
                         },
                         {
                           'file' => 'lib/CPAN/Plugin/Sysdeps/Mapping.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'CPAN::Plugin::Sysdeps::Mapping'
                         }
                       ],
          'no_index' => '^inc/;^t/',
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/CPAN-Plugin-Sysdeps.tar.gz',
          'prereq' => [
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'List::Util',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => '5.006'
                        }
                      ],
          'released' => 1709583459,
          'size_packed' => 41845,
          'size_unpacked' => 182371,
          'test_files' => [
                            't/basic.t',
                            't/code.t',
                            't/detect_linux_distribution.t',
                            't/filtuninst.t',
                            't/instcmd.t',
                            't/sample.t',
                            't/traverse.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'requires' => {
                                                       'ExtUtils::MakeMaker' => '0',
                                                       'strict' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'noes' => {
                                                 'warnings' => '0'
                                               },
                                     'recommends' => {
                                                       'Data::Dumper' => '0',
                                                       'IPC::Open3' => '0',
                                                       'Symbol' => '0'
                                                     },
                                     'requires' => {
                                                     'List::Util' => '0',
                                                     'constant' => '0',
                                                     'strict' => '0',
                                                     'warnings' => '0'
                                                   },
                                     'suggests' => {
                                                     'Hash::Util' => '0'
                                                   }
                                   },
                      'test' => {
                                  'noes' => {
                                              'warnings' => '0'
                                            },
                                  'recommends' => {
                                                    'CPAN' => '0'
                                                  },
                                  'requires' => {
                                                  'Exporter' => '0',
                                                  'File::Temp' => '0',
                                                  'FindBin' => '0',
                                                  'Getopt::Long' => '0',
                                                  'List::Util' => '0',
                                                  'Test::More' => '0',
                                                  'lib' => '0',
                                                  'strict' => '0',
                                                  'vars' => '0',
                                                  'warnings' => '0'
                                                },
                                  'suggests' => {
                                                  'CPAN::Distribution' => '0'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'CPAN-Plugin-Sysdeps'
        };
1;
