$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'WWW::Google::CustomSearch' => 'Interface to Google JSON/Atom Custom Search.',
                                  'WWW::Google::CustomSearch::Item' => 'Placeholder for Google JSON/Atom Custom Search Item.',
                                  'WWW::Google::CustomSearch::Page' => 'Placeholder for Google JSON/Atom Custom Search Page.',
                                  'WWW::Google::CustomSearch::Params' => 'Placeholders for parameters for WWW::Google::CustomSearch',
                                  'WWW::Google::CustomSearch::Request' => 'Placeholder for Google JSON/Atom Custom Search Request.',
                                  'WWW::Google::CustomSearch::Result' => 'Placeholder for Google JSON/Atom Custom Search Result.'
                                },
          'author' => '',
          'dir_lib' => 'lib',
          'dir_t' => 't',
          'dirs' => 5,
          'dirs_array' => [
                            'lib/WWW/Google/CustomSearch',
                            'lib/WWW/Google',
                            'lib/WWW',
                            'lib',
                            't'
                          ],
          'dist' => 'WWW-Google-CustomSearch',
          'dynamic_config' => 1,
          'error' => {
                       'extracts_nicely' => 'expected WWW-Google-CustomSearch but got WWW-Google-CustomSearch-0.40'
                     },
          'extension' => 'tar.gz',
          'external_license_file' => 'LICENSE',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_changelog' => 'Changes',
          'file_license' => 'LICENSE',
          'file_makefile_pl' => 'Makefile.PL',
          'file_manifest' => 'MANIFEST',
          'file_manifest_skip' => 'MANIFEST.SKIP',
          'file_meta_json' => 'META.json',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README',
          'files' => 14,
          'files_array' => [
                             'Changes',
                             'LICENSE',
                             'MANIFEST',
                             'MANIFEST.SKIP',
                             'META.json',
                             'META.yml',
                             'Makefile.PL',
                             'README',
                             'lib/WWW/Google/CustomSearch/Item.pm',
                             'lib/WWW/Google/CustomSearch/Page.pm',
                             'lib/WWW/Google/CustomSearch/Params.pm',
                             'lib/WWW/Google/CustomSearch/Request.pm',
                             'lib/WWW/Google/CustomSearch/Result.pm',
                             'lib/WWW/Google/CustomSearch.pm'
                           ],
          'files_hash' => {
                            'Changes' => {
                                           'mtime' => 1709310115,
                                           'size' => 5158
                                         },
                            'LICENSE' => {
                                           'mtime' => 1709304453,
                                           'size' => 9596
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1709310298,
                                            'size' => 529
                                          },
                            'MANIFEST.SKIP' => {
                                                 'mtime' => 1709304453,
                                                 'size' => 119
                                               },
                            'META.json' => {
                                             'mtime' => 1709310298,
                                             'size' => 2259
                                           },
                            'META.yml' => {
                                            'mtime' => 1709310295,
                                            'size' => 1423
                                          },
                            'Makefile.PL' => {
                                               'mtime' => 1709309332,
                                               'requires' => {
                                                               'ExtUtils::MakeMaker' => '0',
                                                               'perl' => '5.006',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 2156
                                             },
                            'README' => {
                                          'mtime' => 1709304453,
                                          'size' => 3363
                                        },
                            'lib/WWW/Google/CustomSearch.pm' => {
                                                                  'license' => 'Artistic_1_0,Artistic_2_0',
                                                                  'module' => 'WWW::Google::CustomSearch',
                                                                  'mtime' => 1709309282,
                                                                  'requires' => {
                                                                                  'Data::Dumper' => '0',
                                                                                  'JSON' => '0',
                                                                                  'Moo' => '0',
                                                                                  'Types::Standard' => '0',
                                                                                  'URI' => '0',
                                                                                  'WWW::Google::CustomSearch::Params' => '0',
                                                                                  'WWW::Google::CustomSearch::Result' => '0',
                                                                                  'WWW::Google::UserAgent' => '0',
                                                                                  'WWW::Google::UserAgent::DataTypes' => '0',
                                                                                  'namespace::autoclean' => '0',
                                                                                  'perl' => '5.006'
                                                                                },
                                                                  'size' => 74353
                                                                },
                            'lib/WWW/Google/CustomSearch/Item.pm' => {
                                                                       'license' => 'Artistic_1_0,Artistic_2_0',
                                                                       'module' => 'WWW::Google::CustomSearch::Item',
                                                                       'mtime' => 1709309282,
                                                                       'requires' => {
                                                                                       'Data::Dumper' => '0',
                                                                                       'Moo' => '0',
                                                                                       'namespace::autoclean' => '0',
                                                                                       'perl' => '5.006'
                                                                                     },
                                                                       'size' => 8584
                                                                     },
                            'lib/WWW/Google/CustomSearch/Page.pm' => {
                                                                       'license' => 'Artistic_1_0,Artistic_2_0',
                                                                       'module' => 'WWW::Google::CustomSearch::Page',
                                                                       'mtime' => 1709309282,
                                                                       'requires' => {
                                                                                       'Data::Dumper' => '0',
                                                                                       'Moo' => '0',
                                                                                       'namespace::autoclean' => '0',
                                                                                       'perl' => '5.006'
                                                                                     },
                                                                       'size' => 8114
                                                                     },
                            'lib/WWW/Google/CustomSearch/Params.pm' => {
                                                                         'license' => 'Artistic_1_0,Artistic_2_0',
                                                                         'module' => 'WWW::Google::CustomSearch::Params',
                                                                         'mtime' => 1709309282,
                                                                         'requires' => {
                                                                                         'Data::Dumper' => '0',
                                                                                         'Exporter' => '0',
                                                                                         'parent' => '0',
                                                                                         'perl' => '5.006',
                                                                                         'strict' => '0',
                                                                                         'warnings' => '0'
                                                                                       },
                                                                         'size' => 19500
                                                                       },
                            'lib/WWW/Google/CustomSearch/Request.pm' => {
                                                                          'license' => 'Artistic_1_0,Artistic_2_0',
                                                                          'module' => 'WWW::Google::CustomSearch::Request',
                                                                          'mtime' => 1709309282,
                                                                          'requires' => {
                                                                                          'Data::Dumper' => '0',
                                                                                          'Moo' => '0',
                                                                                          'WWW::Google::CustomSearch::Page' => '0',
                                                                                          'namespace::autoclean' => '0',
                                                                                          'perl' => '5.006'
                                                                                        },
                                                                          'size' => 4468
                                                                        },
                            'lib/WWW/Google/CustomSearch/Result.pm' => {
                                                                         'license' => 'Artistic_1_0,Artistic_2_0',
                                                                         'module' => 'WWW::Google::CustomSearch::Result',
                                                                         'mtime' => 1709309282,
                                                                         'requires' => {
                                                                                         'Data::Dumper' => '0',
                                                                                         'Moo' => '0',
                                                                                         'WWW::Google::CustomSearch::Item' => '0',
                                                                                         'WWW::Google::CustomSearch::Page' => '0',
                                                                                         'WWW::Google::CustomSearch::Request' => '0',
                                                                                         'namespace::autoclean' => '0',
                                                                                         'perl' => '5.006'
                                                                                       },
                                                                         'size' => 10234
                                                                       },
                            't/00-load.t' => {
                                               'mtime' => 1709310180,
                                               'no_index' => 1,
                                               'requires' => {
                                                               'Test::More' => '0.88',
                                                               'perl' => '5.006',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 749
                                             },
                            't/01-new.t' => {
                                              'mtime' => 1709310247,
                                              'no_index' => 1,
                                              'requires' => {
                                                              'Test::More' => '0',
                                                              'WWW::Google::CustomSearch' => '0',
                                                              'perl' => '5.006',
                                                              'strict' => '0',
                                                              'warnings' => '0'
                                                            },
                                              'size' => 3871
                                            },
                            't/02-search.t' => {
                                                 'mtime' => 1709310260,
                                                 'no_index' => 1,
                                                 'requires' => {
                                                                 'Test::More' => '0',
                                                                 'WWW::Google::CustomSearch' => '0',
                                                                 'perl' => '5.006',
                                                                 'strict' => '0',
                                                                 'warnings' => '0'
                                                               },
                                                 'size' => 360
                                               },
                            't/manifest.t' => {
                                                'mtime' => 1709310163,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'Test::More' => '0',
                                                                'perl' => '5.006',
                                                                'strict' => '0',
                                                                'warnings' => '0'
                                                              },
                                                'size' => 253,
                                                'suggests' => {
                                                                'Test::CheckManifest' => '0'
                                                              }
                                              },
                            't/meta-json.t' => {
                                                 'mtime' => 1709310217,
                                                 'no_index' => 1,
                                                 'requires' => {
                                                                 'Test::More' => '0.88',
                                                                 'WWW::Google::CustomSearch' => '0',
                                                                 'perl' => '5.006',
                                                                 'strict' => '0',
                                                                 'warnings' => '0'
                                                               },
                                                 'size' => 601,
                                                 'suggests' => {
                                                                 'Test::CPAN::Meta::JSON' => '0'
                                                               }
                                               },
                            't/meta-yml.t' => {
                                                'mtime' => 1709310227,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'Test::More' => '0.88',
                                                                'WWW::Google::CustomSearch' => '0',
                                                                'perl' => '5.006',
                                                                'strict' => '0',
                                                                'warnings' => '0'
                                                              },
                                                'size' => 585,
                                                'suggests' => {
                                                                'Test::CPAN::Meta' => '0'
                                                              }
                                              },
                            't/pod.t' => {
                                           'mtime' => 1709310205,
                                           'no_index' => 1,
                                           'requires' => {
                                                           'Test::More' => '0',
                                                           'perl' => '5.006',
                                                           'strict' => '0',
                                                           'warnings' => '0'
                                                         },
                                           'size' => 268,
                                           'suggests' => {
                                                           'Test::Pod' => '0'
                                                         }
                                         }
                          },
          'got_prereq_from' => 'META.yml',
          'ignored_files_array' => [
                                     't/00-load.t',
                                     't/01-new.t',
                                     't/02-search.t',
                                     't/manifest.t',
                                     't/meta-json.t',
                                     't/meta-yml.t',
                                     't/pod.t'
                                   ],
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 1,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 1,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 33,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 1,
                          'meta_yml_has_repository_resource' => 1,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 1,
                          'use_warnings' => 1
                        },
          'latest_mtime' => 1709310298,
          'license' => 'artistic_2 defined in META.yml defined in LICENSE',
          'license_from_yaml' => 'artistic_2',
          'license_in_pod' => 1,
          'licenses' => {
                          'Artistic_1_0' => [
                                              'lib/WWW/Google/CustomSearch.pm',
                                              'lib/WWW/Google/CustomSearch/Item.pm',
                                              'lib/WWW/Google/CustomSearch/Page.pm',
                                              'lib/WWW/Google/CustomSearch/Params.pm',
                                              'lib/WWW/Google/CustomSearch/Request.pm',
                                              'lib/WWW/Google/CustomSearch/Result.pm'
                                            ],
                          'Artistic_2_0' => [
                                              'lib/WWW/Google/CustomSearch.pm',
                                              'lib/WWW/Google/CustomSearch/Item.pm',
                                              'lib/WWW/Google/CustomSearch/Page.pm',
                                              'lib/WWW/Google/CustomSearch/Params.pm',
                                              'lib/WWW/Google/CustomSearch/Request.pm',
                                              'lib/WWW/Google/CustomSearch/Result.pm'
                                            ]
                        },
          'manifest_matches_dist' => 1,
          'meta_json' => {
                           'abstract' => 'Interface to Google JSON/Atom Custom Search.',
                           'author' => [
                                         'Mohammad S Anwar <mohammad.anwar@yahoo.com>'
                                       ],
                           'dynamic_config' => 1,
                           'generated_by' => 'ExtUtils::MakeMaker version 7.34, CPAN::Meta::Converter version 2.150010',
                           'license' => [
                                          'artistic_2'
                                        ],
                           'meta-spec' => {
                                            'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                            'version' => 2
                                          },
                           'name' => 'WWW-Google-CustomSearch',
                           'no_index' => {
                                           'directory' => [
                                                            't',
                                                            'inc'
                                                          ]
                                         },
                           'prereqs' => {
                                          'build' => {
                                                       'requires' => {
                                                                       'Test::More' => '0'
                                                                     }
                                                     },
                                          'configure' => {
                                                           'requires' => {
                                                                           'ExtUtils::MakeMaker' => '0'
                                                                         }
                                                         },
                                          'runtime' => {
                                                         'requires' => {
                                                                         'JSON' => '2.53',
                                                                         'Moo' => '2.000000',
                                                                         'Type::Tiny' => '1.000005',
                                                                         'URI' => '1.65',
                                                                         'WWW::Google::UserAgent' => '0.23',
                                                                         'namespace::autoclean' => '0.28',
                                                                         'perl' => '5.006'
                                                                       }
                                                       }
                                        },
                           'provides' => {
                                           'WWW::Google::CustomSearch' => {
                                                                            'file' => 'lib/WWW/Google/CustomSearch.pm',
                                                                            'version' => '0.40'
                                                                          },
                                           'WWW::Google::CustomSearch::Item' => {
                                                                                  'file' => 'lib/WWW/Google/CustomSearch/Item.pm',
                                                                                  'version' => '0.40'
                                                                                },
                                           'WWW::Google::CustomSearch::Page' => {
                                                                                  'file' => 'lib/WWW/Google/CustomSearch/Page.pm',
                                                                                  'version' => '0.40'
                                                                                },
                                           'WWW::Google::CustomSearch::Params' => {
                                                                                    'file' => 'lib/WWW/Google/CustomSearch/Params.pm',
                                                                                    'version' => '0.40'
                                                                                  },
                                           'WWW::Google::CustomSearch::Request' => {
                                                                                     'file' => 'lib/WWW/Google/CustomSearch/Request.pm',
                                                                                     'version' => '0.40'
                                                                                   },
                                           'WWW::Google::CustomSearch::Result' => {
                                                                                    'file' => 'lib/WWW/Google/CustomSearch/Result.pm',
                                                                                    'version' => '0.40'
                                                                                  }
                                         },
                           'release_status' => 'stable',
                           'resources' => {
                                            'repository' => {
                                                              'type' => 'git',
                                                              'url' => 'https://github.com/manwar/WWW-Google-CustomSearch.git',
                                                              'web' => 'https://github.com/manwar/WWW-Google-CustomSearch'
                                                            }
                                          },
                           'version' => '0.40',
                           'x_serialization_backend' => 'JSON::PP version 4.02'
                         },
          'meta_json_is_parsable' => 1,
          'meta_json_spec_version' => 2,
          'meta_yml' => {
                          'abstract' => 'Interface to Google JSON/Atom Custom Search.',
                          'author' => [
                                        'Mohammad S Anwar <mohammad.anwar@yahoo.com>'
                                      ],
                          'build_requires' => {
                                                'Test::More' => '0'
                                              },
                          'configure_requires' => {
                                                    'ExtUtils::MakeMaker' => '0'
                                                  },
                          'dynamic_config' => '1',
                          'generated_by' => 'ExtUtils::MakeMaker version 7.34, CPAN::Meta::Converter version 2.150010',
                          'license' => 'artistic_2',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'WWW-Google-CustomSearch',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc'
                                                         ]
                                        },
                          'provides' => {
                                          'WWW::Google::CustomSearch' => {
                                                                           'file' => 'lib/WWW/Google/CustomSearch.pm',
                                                                           'version' => '0.40'
                                                                         },
                                          'WWW::Google::CustomSearch::Item' => {
                                                                                 'file' => 'lib/WWW/Google/CustomSearch/Item.pm',
                                                                                 'version' => '0.40'
                                                                               },
                                          'WWW::Google::CustomSearch::Page' => {
                                                                                 'file' => 'lib/WWW/Google/CustomSearch/Page.pm',
                                                                                 'version' => '0.40'
                                                                               },
                                          'WWW::Google::CustomSearch::Params' => {
                                                                                   'file' => 'lib/WWW/Google/CustomSearch/Params.pm',
                                                                                   'version' => '0.40'
                                                                                 },
                                          'WWW::Google::CustomSearch::Request' => {
                                                                                    'file' => 'lib/WWW/Google/CustomSearch/Request.pm',
                                                                                    'version' => '0.40'
                                                                                  },
                                          'WWW::Google::CustomSearch::Result' => {
                                                                                   'file' => 'lib/WWW/Google/CustomSearch/Result.pm',
                                                                                   'version' => '0.40'
                                                                                 }
                                        },
                          'requires' => {
                                          'JSON' => '2.53',
                                          'Moo' => '2.000000',
                                          'Type::Tiny' => '1.000005',
                                          'URI' => '1.65',
                                          'WWW::Google::UserAgent' => '0.23',
                                          'namespace::autoclean' => '0.28',
                                          'perl' => '5.006'
                                        },
                          'resources' => {
                                           'repository' => 'https://github.com/manwar/WWW-Google-CustomSearch.git'
                                         },
                          'version' => '0.40',
                          'x_serialization_backend' => 'CPAN::Meta::YAML version 0.018'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/WWW/Google/CustomSearch.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'WWW::Google::CustomSearch'
                         },
                         {
                           'file' => 'lib/WWW/Google/CustomSearch/Item.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'WWW::Google::CustomSearch::Item'
                         },
                         {
                           'file' => 'lib/WWW/Google/CustomSearch/Page.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'WWW::Google::CustomSearch::Page'
                         },
                         {
                           'file' => 'lib/WWW/Google/CustomSearch/Params.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'WWW::Google::CustomSearch::Params'
                         },
                         {
                           'file' => 'lib/WWW/Google/CustomSearch/Request.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'WWW::Google::CustomSearch::Request'
                         },
                         {
                           'file' => 'lib/WWW/Google/CustomSearch/Result.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'WWW::Google::CustomSearch::Result'
                         }
                       ],
          'no_index' => '^inc/;^t/',
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/WWW-Google-CustomSearch.tar.gz',
          'prereq' => [
                        {
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'JSON',
                          'type' => 'runtime_requires',
                          'version' => '2.53'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Moo',
                          'type' => 'runtime_requires',
                          'version' => '2.000000'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Type::Tiny',
                          'type' => 'runtime_requires',
                          'version' => '1.000005'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'URI',
                          'type' => 'runtime_requires',
                          'version' => '1.65'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'WWW::Google::UserAgent',
                          'type' => 'runtime_requires',
                          'version' => '0.23'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'namespace::autoclean',
                          'type' => 'runtime_requires',
                          'version' => '0.28'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => '5.006'
                        }
                      ],
          'released' => 1709583479,
          'size_packed' => 27658,
          'size_unpacked' => 156543,
          'test_files' => [
                            't/00-load.t',
                            't/01-new.t',
                            't/02-search.t',
                            't/manifest.t',
                            't/meta-json.t',
                            't/meta-yml.t',
                            't/pod.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'requires' => {
                                                       'ExtUtils::MakeMaker' => '0',
                                                       'perl' => '5.006',
                                                       'strict' => '0',
                                                       'warnings' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'requires' => {
                                                     'Data::Dumper' => '0',
                                                     'Exporter' => '0',
                                                     'JSON' => '0',
                                                     'Moo' => '0',
                                                     'Types::Standard' => '0',
                                                     'URI' => '0',
                                                     'WWW::Google::UserAgent' => '0',
                                                     'WWW::Google::UserAgent::DataTypes' => '0',
                                                     'namespace::autoclean' => '0',
                                                     'parent' => '0',
                                                     'perl' => '5.006',
                                                     'strict' => '0',
                                                     'warnings' => '0'
                                                   }
                                   },
                      'test' => {
                                  'requires' => {
                                                  'Test::More' => '0.88',
                                                  'perl' => '5.006',
                                                  'strict' => '0',
                                                  'warnings' => '0'
                                                },
                                  'suggests' => {
                                                  'Test::CPAN::Meta' => '0',
                                                  'Test::CPAN::Meta::JSON' => '0',
                                                  'Test::CheckManifest' => '0',
                                                  'Test::Pod' => '0'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'WWW-Google-CustomSearch'
        };
1;
