$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'MsOffice::Word::Surgeon' => 'tamper with the guts of Microsoft docx documents, with regexes',
                                  'MsOffice::Word::Surgeon::PackagePart' => 'Operations on a single part within the ZIP package of a docx document',
                                  'MsOffice::Word::Surgeon::Revision' => 'generate XML markup for MsWord revisions',
                                  'MsOffice::Word::Surgeon::Run' => 'internal representation for a "run of text"',
                                  'MsOffice::Word::Surgeon::Text' => 'internal representation for a node of literal text',
                                  'MsOffice::Word::Surgeon::Utils' => 'utility functions for MsOffice::Word::Surgeon'
                                },
          'author' => '',
          'dir_lib' => 'lib',
          'dir_t' => 't',
          'dir_xt' => 'xt',
          'dirs' => 7,
          'dirs_array' => [
                            'lib/MsOffice/Word/Surgeon',
                            'lib/MsOffice/Word',
                            'lib/MsOffice',
                            'lib',
                            't/etc',
                            't',
                            'xt'
                          ],
          'dist' => 'MsOffice-Word-Surgeon',
          'dynamic_config' => 1,
          'error' => {
                       'extracts_nicely' => 'expected MsOffice-Word-Surgeon but got MsOffice-Word-Surgeon-2.05'
                     },
          'extension' => 'tar.gz',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_build_pl' => 'Build.PL',
          'file_changelog' => 'Changes',
          'file_manifest' => 'MANIFEST',
          'file_meta_json' => 'META.json',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README.md',
          'files' => 17,
          'files_array' => [
                             'Build.PL',
                             'Changes',
                             'MANIFEST',
                             'META.json',
                             'META.yml',
                             'README.md',
                             'lib/MsOffice/Word/Surgeon/PackagePart.pm',
                             'lib/MsOffice/Word/Surgeon/Revision.pm',
                             'lib/MsOffice/Word/Surgeon/Run.pm',
                             'lib/MsOffice/Word/Surgeon/Text.pm',
                             'lib/MsOffice/Word/Surgeon/Utils.pm',
                             'lib/MsOffice/Word/Surgeon.pm',
                             't/etc/MsOffice-Word-Surgeon.docx',
                             't/msoffice-word-surgeon.t',
                             't/reveal_bookmarks.t',
                             'xt/manifest.t',
                             'xt/pod.t'
                           ],
          'files_hash' => {
                            'Build.PL' => {
                                            'mtime' => 1707858919,
                                            'requires' => {
                                                            'Module::Build' => '0.4004',
                                                            'strict' => '0',
                                                            'warnings' => '0'
                                                          },
                                            'size' => 1262
                                          },
                            'Changes' => {
                                           'mtime' => 1709414942,
                                           'size' => 2107
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1707858445,
                                            'size' => 389
                                          },
                            'META.json' => {
                                             'mtime' => 1709417811,
                                             'size' => 2286
                                           },
                            'META.yml' => {
                                            'mtime' => 1709417811,
                                            'size' => 1512
                                          },
                            'README.md' => {
                                             'mtime' => 1651382486,
                                             'size' => 552
                                           },
                            'lib/MsOffice/Word/Surgeon.pm' => {
                                                                'license' => 'Perl_5',
                                                                'module' => 'MsOffice::Word::Surgeon',
                                                                'mtime' => 1709414972,
                                                                'requires' => {
                                                                                'Archive::Zip' => '0',
                                                                                'Carp::Clan' => '0',
                                                                                'Encode' => '0',
                                                                                'Moose' => '0',
                                                                                'MooseX::StrictConstructor' => '0',
                                                                                'MsOffice::Word::Surgeon::PackagePart' => '0',
                                                                                'MsOffice::Word::Surgeon::Revision' => '0',
                                                                                'namespace::clean' => '0',
                                                                                'perl' => 'v5.24.0'
                                                                              },
                                                                'size' => 16545
                                                              },
                            'lib/MsOffice/Word/Surgeon/PackagePart.pm' => {
                                                                            'license' => 'Perl_5',
                                                                            'module' => 'MsOffice::Word::Surgeon::PackagePart',
                                                                            'mtime' => 1709414972,
                                                                            'noes' => {
                                                                                        'warnings' => '0'
                                                                                      },
                                                                            'requires' => {
                                                                                            'Carp' => '0',
                                                                                            'Carp::Clan' => '0',
                                                                                            'List::Util' => '0',
                                                                                            'Moose' => '0',
                                                                                            'MooseX::StrictConstructor' => '0',
                                                                                            'MsOffice::Word::Surgeon::Run' => '0',
                                                                                            'MsOffice::Word::Surgeon::Text' => '0',
                                                                                            'MsOffice::Word::Surgeon::Utils' => '0',
                                                                                            'XML::LibXML' => '0',
                                                                                            'constant' => '0',
                                                                                            'namespace::clean' => '0',
                                                                                            'perl' => 'v5.24.0',
                                                                                            'strict' => '0',
                                                                                            'warnings' => '0'
                                                                                          },
                                                                            'size' => 35014
                                                                          },
                            'lib/MsOffice/Word/Surgeon/Revision.pm' => {
                                                                         'module' => 'MsOffice::Word::Surgeon::Revision',
                                                                         'mtime' => 1709414972,
                                                                         'requires' => {
                                                                                         'Carp::Clan' => '0',
                                                                                         'Moose' => '0',
                                                                                         'Moose::Util::TypeConstraints' => '0',
                                                                                         'MooseX::StrictConstructor' => '0',
                                                                                         'MsOffice::Word::Surgeon::Utils' => '0',
                                                                                         'POSIX' => '0',
                                                                                         'namespace::clean' => '0',
                                                                                         'perl' => 'v5.24.0'
                                                                                       },
                                                                         'size' => 3466
                                                                       },
                            'lib/MsOffice/Word/Surgeon/Run.pm' => {
                                                                    'license' => 'Perl_5',
                                                                    'module' => 'MsOffice::Word::Surgeon::Run',
                                                                    'mtime' => 1709414972,
                                                                    'requires' => {
                                                                                    'Carp::Clan' => '0',
                                                                                    'Moose' => '0',
                                                                                    'MooseX::StrictConstructor' => '0',
                                                                                    'MsOffice::Word::Surgeon::Utils' => '0',
                                                                                    'namespace::clean' => '0',
                                                                                    'perl' => 'v5.24.0'
                                                                                  },
                                                                    'size' => 5805
                                                                  },
                            'lib/MsOffice/Word/Surgeon/Text.pm' => {
                                                                     'license' => 'Perl_5',
                                                                     'module' => 'MsOffice::Word::Surgeon::Text',
                                                                     'mtime' => 1709414972,
                                                                     'requires' => {
                                                                                     'Carp::Clan' => '0',
                                                                                     'Moose' => '0',
                                                                                     'MooseX::StrictConstructor' => '0',
                                                                                     'MsOffice::Word::Surgeon::Utils' => '0',
                                                                                     'namespace::clean' => '0',
                                                                                     'perl' => 'v5.24.0'
                                                                                   },
                                                                     'size' => 6876
                                                                   },
                            'lib/MsOffice/Word/Surgeon/Utils.pm' => {
                                                                      'module' => 'MsOffice::Word::Surgeon::Utils',
                                                                      'mtime' => 1709414972,
                                                                      'requires' => {
                                                                                      'Carp::Clan' => '0',
                                                                                      'Exporter' => '0',
                                                                                      'strict' => '0',
                                                                                      'warnings' => '0'
                                                                                    },
                                                                      'size' => 2127
                                                                    },
                            't/etc/MsOffice-Word-Surgeon.docx' => {
                                                                    'mtime' => 1709411436,
                                                                    'size' => 37856
                                                                  },
                            't/msoffice-word-surgeon.t' => {
                                                             'mtime' => 1709410816,
                                                             'requires' => {
                                                                             'MsOffice::Word::Surgeon' => '0',
                                                                             'Test::More' => '0.88',
                                                                             'strict' => '0',
                                                                             'warnings' => '0'
                                                                           },
                                                             'size' => 2785
                                                           },
                            't/reveal_bookmarks.t' => {
                                                        'mtime' => 1707851849,
                                                        'requires' => {
                                                                        'MsOffice::Word::Surgeon' => '0',
                                                                        'Test::More' => '0.88',
                                                                        'strict' => '0',
                                                                        'warnings' => '0'
                                                                      },
                                                        'size' => 880
                                                      },
                            'xt/manifest.t' => {
                                                 'mtime' => 1572312415,
                                                 'size' => 204
                                               },
                            'xt/pod.t' => {
                                            'mtime' => 1572312415,
                                            'size' => 242
                                          }
                          },
          'got_prereq_from' => 'META.yml',
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 1,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 0,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 32,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 1,
                          'meta_yml_has_repository_resource' => 1,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 1,
                          'use_warnings' => 1
                        },
          'latest_mtime' => 1709417811,
          'license' => 'artistic_2 defined in META.yml',
          'license_file' => 'lib/MsOffice/Word/Surgeon.pm,lib/MsOffice/Word/Surgeon/PackagePart.pm,lib/MsOffice/Word/Surgeon/Run.pm,lib/MsOffice/Word/Surgeon/Text.pm',
          'license_from_yaml' => 'artistic_2',
          'license_in_pod' => 1,
          'license_type' => 'Perl_5',
          'licenses' => {
                          'Perl_5' => [
                                        'lib/MsOffice/Word/Surgeon.pm',
                                        'lib/MsOffice/Word/Surgeon/PackagePart.pm',
                                        'lib/MsOffice/Word/Surgeon/Run.pm',
                                        'lib/MsOffice/Word/Surgeon/Text.pm'
                                      ]
                        },
          'manifest_matches_dist' => 1,
          'meta_json' => {
                           'abstract' => 'tamper with the guts of Microsoft docx documents, with regexes',
                           'author' => [
                                         'DAMI <dami@cpan.org>'
                                       ],
                           'dynamic_config' => 1,
                           'generated_by' => 'Module::Build version 0.4232',
                           'license' => [
                                          'artistic_2'
                                        ],
                           'meta-spec' => {
                                            'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                            'version' => 2
                                          },
                           'name' => 'MsOffice-Word-Surgeon',
                           'prereqs' => {
                                          'configure' => {
                                                           'requires' => {
                                                                           'Module::Build' => '0.4004'
                                                                         }
                                                         },
                                          'runtime' => {
                                                         'requires' => {
                                                                         'Archive::Zip' => '0',
                                                                         'Carp::Clan' => '0',
                                                                         'Encode' => '0',
                                                                         'Exporter' => '0',
                                                                         'List::Util' => '0',
                                                                         'Moose' => '0',
                                                                         'MooseX::StrictConstructor' => '0',
                                                                         'POSIX' => '0',
                                                                         'XML::LibXML' => '0',
                                                                         'namespace::clean' => '0',
                                                                         'perl' => 'v5.24.0'
                                                                       }
                                                       },
                                          'test' => {
                                                      'requires' => {
                                                                      'Test::More' => '0'
                                                                    }
                                                    }
                                        },
                           'provides' => {
                                           'MsOffice::Word::Surgeon' => {
                                                                          'file' => 'lib/MsOffice/Word/Surgeon.pm',
                                                                          'version' => '2.05'
                                                                        },
                                           'MsOffice::Word::Surgeon::PackagePart' => {
                                                                                       'file' => 'lib/MsOffice/Word/Surgeon/PackagePart.pm',
                                                                                       'version' => '2.05'
                                                                                     },
                                           'MsOffice::Word::Surgeon::Revision' => {
                                                                                    'file' => 'lib/MsOffice/Word/Surgeon/Revision.pm',
                                                                                    'version' => '2.05'
                                                                                  },
                                           'MsOffice::Word::Surgeon::Run' => {
                                                                               'file' => 'lib/MsOffice/Word/Surgeon/Run.pm',
                                                                               'version' => '2.05'
                                                                             },
                                           'MsOffice::Word::Surgeon::Text' => {
                                                                                'file' => 'lib/MsOffice/Word/Surgeon/Text.pm',
                                                                                'version' => '2.05'
                                                                              },
                                           'MsOffice::Word::Surgeon::Utils' => {
                                                                                 'file' => 'lib/MsOffice/Word/Surgeon/Utils.pm',
                                                                                 'version' => '2.05'
                                                                               }
                                         },
                           'release_status' => 'stable',
                           'resources' => {
                                            'license' => [
                                                           'http://www.perlfoundation.org/artistic_license_2_0'
                                                         ],
                                            'repository' => {
                                                              'url' => 'https://github.com/damil/MsOffice-Word-Surgeon'
                                                            }
                                          },
                           'version' => '2.05',
                           'x_serialization_backend' => 'JSON::PP version 4.05'
                         },
          'meta_json_is_parsable' => 1,
          'meta_json_spec_version' => 2,
          'meta_yml' => {
                          'abstract' => 'tamper with the guts of Microsoft docx documents, with regexes',
                          'author' => [
                                        'DAMI <dami@cpan.org>'
                                      ],
                          'build_requires' => {
                                                'Test::More' => '0'
                                              },
                          'configure_requires' => {
                                                    'Module::Build' => '0.4004'
                                                  },
                          'dynamic_config' => '1',
                          'generated_by' => 'Module::Build version 0.4232, CPAN::Meta::Converter version 2.150010',
                          'license' => 'artistic_2',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'MsOffice-Word-Surgeon',
                          'provides' => {
                                          'MsOffice::Word::Surgeon' => {
                                                                         'file' => 'lib/MsOffice/Word/Surgeon.pm',
                                                                         'version' => '2.05'
                                                                       },
                                          'MsOffice::Word::Surgeon::PackagePart' => {
                                                                                      'file' => 'lib/MsOffice/Word/Surgeon/PackagePart.pm',
                                                                                      'version' => '2.05'
                                                                                    },
                                          'MsOffice::Word::Surgeon::Revision' => {
                                                                                   'file' => 'lib/MsOffice/Word/Surgeon/Revision.pm',
                                                                                   'version' => '2.05'
                                                                                 },
                                          'MsOffice::Word::Surgeon::Run' => {
                                                                              'file' => 'lib/MsOffice/Word/Surgeon/Run.pm',
                                                                              'version' => '2.05'
                                                                            },
                                          'MsOffice::Word::Surgeon::Text' => {
                                                                               'file' => 'lib/MsOffice/Word/Surgeon/Text.pm',
                                                                               'version' => '2.05'
                                                                             },
                                          'MsOffice::Word::Surgeon::Utils' => {
                                                                                'file' => 'lib/MsOffice/Word/Surgeon/Utils.pm',
                                                                                'version' => '2.05'
                                                                              }
                                        },
                          'requires' => {
                                          'Archive::Zip' => '0',
                                          'Carp::Clan' => '0',
                                          'Encode' => '0',
                                          'Exporter' => '0',
                                          'List::Util' => '0',
                                          'Moose' => '0',
                                          'MooseX::StrictConstructor' => '0',
                                          'POSIX' => '0',
                                          'XML::LibXML' => '0',
                                          'namespace::clean' => '0',
                                          'perl' => 'v5.24.0'
                                        },
                          'resources' => {
                                           'license' => 'http://www.perlfoundation.org/artistic_license_2_0',
                                           'repository' => 'https://github.com/damil/MsOffice-Word-Surgeon'
                                         },
                          'version' => '2.05',
                          'x_serialization_backend' => 'CPAN::Meta::YAML version 0.018'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/MsOffice/Word/Surgeon.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MsOffice::Word::Surgeon'
                         },
                         {
                           'file' => 'lib/MsOffice/Word/Surgeon/PackagePart.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MsOffice::Word::Surgeon::PackagePart'
                         },
                         {
                           'file' => 'lib/MsOffice/Word/Surgeon/Revision.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MsOffice::Word::Surgeon::Revision'
                         },
                         {
                           'file' => 'lib/MsOffice/Word/Surgeon/Run.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MsOffice::Word::Surgeon::Run'
                         },
                         {
                           'file' => 'lib/MsOffice/Word/Surgeon/Text.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MsOffice::Word::Surgeon::Text'
                         },
                         {
                           'file' => 'lib/MsOffice/Word/Surgeon/Utils.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'MsOffice::Word::Surgeon::Utils'
                         }
                       ],
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/MsOffice-Word-Surgeon.tar.gz',
          'prereq' => [
                        {
                          'is_prereq' => 1,
                          'requires' => 'Archive::Zip',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Carp::Clan',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Encode',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Exporter',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'List::Util',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'Module::Build',
                          'type' => 'configure_requires',
                          'version' => '0.4004'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Moose',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'MooseX::StrictConstructor',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'POSIX',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'XML::LibXML',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'namespace::clean',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => 'v5.24.0'
                        }
                      ],
          'released' => 1709583473,
          'size_packed' => 65891,
          'size_unpacked' => 119908,
          'test_files' => [
                            't/msoffice-word-surgeon.t',
                            't/reveal_bookmarks.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'requires' => {
                                                       'Module::Build' => '0.4004',
                                                       'strict' => '0',
                                                       'warnings' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'noes' => {
                                                 'warnings' => '0'
                                               },
                                     'requires' => {
                                                     'Archive::Zip' => '0',
                                                     'Carp' => '0',
                                                     'Carp::Clan' => '0',
                                                     'Encode' => '0',
                                                     'Exporter' => '0',
                                                     'List::Util' => '0',
                                                     'Moose' => '0',
                                                     'Moose::Util::TypeConstraints' => '0',
                                                     'MooseX::StrictConstructor' => '0',
                                                     'POSIX' => '0',
                                                     'XML::LibXML' => '0',
                                                     'constant' => '0',
                                                     'namespace::clean' => '0',
                                                     'perl' => 'v5.24.0',
                                                     'strict' => '0',
                                                     'warnings' => '0'
                                                   }
                                   },
                      'test' => {
                                  'requires' => {
                                                  'Test::More' => '0.88',
                                                  'strict' => '0',
                                                  'warnings' => '0'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'MsOffice-Word-Surgeon'
        };
1;
