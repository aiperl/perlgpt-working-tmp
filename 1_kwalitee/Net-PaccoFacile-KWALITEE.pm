$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'Net::PaccoFacile' => 'Perl library with MINIMAL interface to use PaccoFacile API.'
                                },
          'author' => '',
          'dir_lib' => 'lib',
          'dir_t' => 't',
          'dirs' => 3,
          'dirs_array' => [
                            'lib/Net',
                            'lib',
                            't'
                          ],
          'dist' => 'Net-PaccoFacile',
          'dynamic_config' => 0,
          'error' => {
                       'extracts_nicely' => 'expected Net-PaccoFacile but got Net-PaccoFacile-0.1.0'
                     },
          'extension' => 'tar.gz',
          'external_license_file' => 'LICENSE',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_dist_ini' => 'dist.ini',
          'file_license' => 'LICENSE',
          'file_makefile_pl' => 'Makefile.PL',
          'file_manifest' => 'MANIFEST',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README,README.md',
          'files' => 9,
          'files_array' => [
                             'LICENSE',
                             'MANIFEST',
                             'META.yml',
                             'Makefile.PL',
                             'README',
                             'README.md',
                             'dist.ini',
                             'lib/Net/PaccoFacile.pm',
                             't/00basic.t'
                           ],
          'files_hash' => {
                            'LICENSE' => {
                                           'mtime' => 1709309739,
                                           'size' => 9045
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1709309739,
                                            'size' => 180
                                          },
                            'META.yml' => {
                                            'mtime' => 1709309739,
                                            'size' => 676
                                          },
                            'Makefile.PL' => {
                                               'mtime' => 1709309739,
                                               'requires' => {
                                                               'ExtUtils::MakeMaker' => '0',
                                                               'perl' => '5.036000',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 1211
                                             },
                            'README' => {
                                          'mtime' => 1709309739,
                                          'size' => 349
                                        },
                            'README.md' => {
                                             'mtime' => 1709309739,
                                             'size' => 114
                                           },
                            'dist.ini' => {
                                            'mtime' => 1709309739,
                                            'size' => 345
                                          },
                            'lib/Net/PaccoFacile.pm' => {
                                                          'license' => 'Artistic_2_0',
                                                          'module' => 'Net::PaccoFacile',
                                                          'mtime' => 1709309739,
                                                          'requires' => {
                                                                          'Carp' => '0',
                                                                          'List::Util' => '0',
                                                                          'Mojo::UserAgent' => '0',
                                                                          'Mojo::Util' => '0',
                                                                          'Moo' => '0',
                                                                          'namespace::clean' => '0',
                                                                          'perl' => 'v5.36.0',
                                                                          'version' => '0'
                                                                        },
                                                          'size' => 4056
                                                        },
                            't/00basic.t' => {
                                               'mtime' => 1709309739,
                                               'requires' => {
                                                               'Net::PaccoFacile' => '0',
                                                               'Test::Exception' => '0',
                                                               'Test::More' => '0.88'
                                                             },
                                               'size' => 197
                                             }
                          },
          'got_prereq_from' => 'META.yml',
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 0,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 0,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 1,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 29,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 0,
                          'meta_yml_has_repository_resource' => 0,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 1,
                          'use_warnings' => 1
                        },
          'latest_mtime' => 1709309739,
          'license' => 'artistic_2 defined in META.yml defined in LICENSE',
          'license_file' => 'lib/Net/PaccoFacile.pm',
          'license_from_yaml' => 'artistic_2',
          'license_in_pod' => 1,
          'license_type' => 'Artistic_2_0',
          'licenses' => {
                          'Artistic_2_0' => [
                                              'lib/Net/PaccoFacile.pm'
                                            ]
                        },
          'manifest_matches_dist' => 1,
          'meta_yml' => {
                          'abstract' => 'Perl library with MINIMAL interface to use PaccoFacile API.',
                          'author' => [
                                        'Michele Beltrame <mb@blendgroup.it>'
                                      ],
                          'build_requires' => {},
                          'configure_requires' => {
                                                    'ExtUtils::MakeMaker' => '0'
                                                  },
                          'dynamic_config' => '0',
                          'generated_by' => 'Dist::Zilla version 6.030, CPAN::Meta::Converter version 2.150010',
                          'license' => 'artistic_2',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'Net-PaccoFacile',
                          'requires' => {
                                          'Mojolicious' => '9.28',
                                          'Moo' => '2',
                                          'Test::Exception' => '0',
                                          'namespace::clean' => '0.27',
                                          'perl' => 'v5.36.0',
                                          'version' => '0'
                                        },
                          'version' => '0.1.0',
                          'x_generated_by_perl' => 'v5.38.0',
                          'x_serialization_backend' => 'YAML::Tiny version 1.74',
                          'x_spdx_expression' => 'Artistic-2.0'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/Net/PaccoFacile.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Net::PaccoFacile'
                         }
                       ],
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/Net-PaccoFacile.tar.gz',
          'prereq' => [
                        {
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Mojolicious',
                          'type' => 'runtime_requires',
                          'version' => '9.28'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Moo',
                          'type' => 'runtime_requires',
                          'version' => '2'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Test::Exception',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'namespace::clean',
                          'type' => 'runtime_requires',
                          'version' => '0.27'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => 'v5.36.0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'version',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        }
                      ],
          'released' => 1709583479,
          'size_packed' => 6272,
          'size_unpacked' => 16173,
          'test_files' => [
                            't/00basic.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'requires' => {
                                                       'ExtUtils::MakeMaker' => '0',
                                                       'perl' => '5.036000',
                                                       'strict' => '0',
                                                       'warnings' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'requires' => {
                                                     'Carp' => '0',
                                                     'List::Util' => '0',
                                                     'Mojo::UserAgent' => '0',
                                                     'Mojo::Util' => '0',
                                                     'Moo' => '0',
                                                     'namespace::clean' => '0',
                                                     'perl' => 'v5.36.0',
                                                     'version' => '0'
                                                   }
                                   },
                      'test' => {
                                  'requires' => {
                                                  'Test::Exception' => '0',
                                                  'Test::More' => '0.88'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'Net-PaccoFacile'
        };
1;
