$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'Net::Async::Blockchain' => 'base for blockchain subscription clients.',
                                  'Net::Async::Blockchain::BTC' => 'Bitcoin based subscription.',
                                  'Net::Async::Blockchain::Client::RPC' => 'Async RPC Client.',
                                  'Net::Async::Blockchain::Client::RPC::BTC' => 'Async BTC RPC Client.',
                                  'Net::Async::Blockchain::Client::RPC::ETH' => 'Async ETH RPC Client.',
                                  'Net::Async::Blockchain::Client::RPC::Omni' => 'Async Omnicore RPC Client.',
                                  'Net::Async::Blockchain::Client::Websocket' => 'Async websocket Client.',
                                  'Net::Async::Blockchain::Client::ZMQ' => 'Async ZMQ Client.',
                                  'Net::Async::Blockchain::ETH' => 'Ethereum based subscription.',
                                  'Net::Async::Blockchain::Transaction' => 'Transaction abstraction.'
                                },
          'author' => '',
          'dir_lib' => 'lib',
          'dir_t' => 't',
          'dir_xt' => 'xt',
          'dirs' => 12,
          'dirs_array' => [
                            'examples',
                            'lib/Net/Async/Blockchain/Client/RPC',
                            'lib/Net/Async/Blockchain/Client',
                            'lib/Net/Async/Blockchain',
                            'lib/Net/Async',
                            'lib/Net',
                            'lib',
                            't/rc',
                            't',
                            'xt/author',
                            'xt/release',
                            'xt'
                          ],
          'dist' => 'Net-Async-Blockchain',
          'dynamic_config' => 0,
          'error' => {
                       'extracts_nicely' => 'expected Net-Async-Blockchain but got Net-Async-Blockchain-0.004'
                     },
          'extension' => 'tar.gz',
          'external_license_file' => 'LICENSE',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_changelog' => 'Changes',
          'file_cpanfile' => 'cpanfile',
          'file_dist_ini' => 'dist.ini',
          'file_license' => 'LICENSE',
          'file_makefile_pl' => 'Makefile.PL',
          'file_manifest' => 'MANIFEST',
          'file_meta_json' => 'META.json',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README',
          'files' => 31,
          'files_array' => [
                             'Changes',
                             'LICENSE',
                             'MANIFEST',
                             'META.json',
                             'META.yml',
                             'Makefile.PL',
                             'README',
                             'cpanfile',
                             'dist.ini',
                             'examples/btc_subscription.pl',
                             'examples/eth_subscription.pl',
                             'examples/merge_subscription.pl',
                             'lib/Net/Async/Blockchain/BTC.pm',
                             'lib/Net/Async/Blockchain/BTC.pod',
                             'lib/Net/Async/Blockchain/Client/RPC/BTC.pm',
                             'lib/Net/Async/Blockchain/Client/RPC/BTC.pod',
                             'lib/Net/Async/Blockchain/Client/RPC/ETH.pm',
                             'lib/Net/Async/Blockchain/Client/RPC/ETH.pod',
                             'lib/Net/Async/Blockchain/Client/RPC/Omni.pm',
                             'lib/Net/Async/Blockchain/Client/RPC/Omni.pod',
                             'lib/Net/Async/Blockchain/Client/RPC.pm',
                             'lib/Net/Async/Blockchain/Client/RPC.pod',
                             'lib/Net/Async/Blockchain/Client/Websocket.pm',
                             'lib/Net/Async/Blockchain/Client/Websocket.pod',
                             'lib/Net/Async/Blockchain/Client/ZMQ.pm',
                             'lib/Net/Async/Blockchain/Client/ZMQ.pod',
                             'lib/Net/Async/Blockchain/ETH.pm',
                             'lib/Net/Async/Blockchain/ETH.pod',
                             'lib/Net/Async/Blockchain/Transaction.pm',
                             'lib/Net/Async/Blockchain.pm',
                             'lib/Net/Async/Blockchain.pod'
                           ],
          'files_hash' => {
                            'Changes' => {
                                           'mtime' => 1709527776,
                                           'size' => 674
                                         },
                            'LICENSE' => {
                                           'mtime' => 1709527776,
                                           'size' => 18362
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1709527776,
                                            'size' => 1400
                                          },
                            'META.json' => {
                                             'mtime' => 1709527776,
                                             'size' => 6227
                                           },
                            'META.yml' => {
                                            'mtime' => 1709527776,
                                            'size' => 3753
                                          },
                            'Makefile.PL' => {
                                               'mtime' => 1709527776,
                                               'requires' => {
                                                               'ExtUtils::MakeMaker' => '7.64',
                                                               'perl' => '5.024',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 2978
                                             },
                            'README' => {
                                          'mtime' => 1709527776,
                                          'size' => 1393
                                        },
                            'cpanfile' => {
                                            'mtime' => 1709527776,
                                            'size' => 740
                                          },
                            'dist.ini' => {
                                            'mtime' => 1709527776,
                                            'size' => 322
                                          },
                            'examples/btc_subscription.pl' => {
                                                                'mtime' => 1709527776,
                                                                'size' => 697
                                                              },
                            'examples/eth_subscription.pl' => {
                                                                'mtime' => 1709527776,
                                                                'size' => 411
                                                              },
                            'examples/merge_subscription.pl' => {
                                                                  'mtime' => 1709527776,
                                                                  'size' => 650
                                                                },
                            'lib/Net/Async/Blockchain.pm' => {
                                                               'module' => 'Net::Async::Blockchain',
                                                               'mtime' => 1709527776,
                                                               'noes' => {
                                                                           'indirect' => '0'
                                                                         },
                                                               'requires' => {
                                                                               'IO::Async::Notifier' => '0',
                                                                               'parent' => '0',
                                                                               'strict' => '0',
                                                                               'warnings' => '0'
                                                                             },
                                                               'size' => 2394
                                                             },
                            'lib/Net/Async/Blockchain.pod' => {
                                                                'mtime' => 1709527776,
                                                                'size' => 2846
                                                              },
                            'lib/Net/Async/Blockchain/BTC.pm' => {
                                                                   'module' => 'Net::Async::Blockchain::BTC',
                                                                   'mtime' => 1709527776,
                                                                   'noes' => {
                                                                               'indirect' => '0'
                                                                             },
                                                                   'requires' => {
                                                                                   'Future::AsyncAwait' => '0',
                                                                                   'Net::Async::Blockchain' => '0',
                                                                                   'Net::Async::Blockchain::Client::ZMQ' => '0',
                                                                                   'curry' => '0',
                                                                                   'parent' => '0',
                                                                                   'strict' => '0',
                                                                                   'warnings' => '0'
                                                                                 },
                                                                   'size' => 2191
                                                                 },
                            'lib/Net/Async/Blockchain/BTC.pod' => {
                                                                    'mtime' => 1709527776,
                                                                    'size' => 2746
                                                                  },
                            'lib/Net/Async/Blockchain/Client/RPC.pm' => {
                                                                          'module' => 'Net::Async::Blockchain::Client::RPC',
                                                                          'mtime' => 1709527776,
                                                                          'noes' => {
                                                                                      'indirect' => '0'
                                                                                    },
                                                                          'requires' => {
                                                                                          'Future::AsyncAwait' => '0',
                                                                                          'IO::Async::Notifier' => '0',
                                                                                          'JSON::MaybeUTF8' => '0',
                                                                                          'Net::Async::HTTP' => '0',
                                                                                          'constant' => '0',
                                                                                          'parent' => '0',
                                                                                          'strict' => '0',
                                                                                          'warnings' => '0'
                                                                                        },
                                                                          'size' => 3334
                                                                        },
                            'lib/Net/Async/Blockchain/Client/RPC.pod' => {
                                                                           'mtime' => 1709527776,
                                                                           'size' => 2550
                                                                         },
                            'lib/Net/Async/Blockchain/Client/RPC/BTC.pm' => {
                                                                              'module' => 'Net::Async::Blockchain::Client::RPC::BTC',
                                                                              'mtime' => 1709527776,
                                                                              'noes' => {
                                                                                          'indirect' => '0'
                                                                                        },
                                                                              'requires' => {
                                                                                              'Net::Async::Blockchain::Client::RPC' => '0',
                                                                                              'parent' => '0',
                                                                                              'strict' => '0',
                                                                                              'warnings' => '0'
                                                                                            },
                                                                              'size' => 3904
                                                                            },
                            'lib/Net/Async/Blockchain/Client/RPC/BTC.pod' => {
                                                                               'mtime' => 1709527776,
                                                                               'size' => 4555
                                                                             },
                            'lib/Net/Async/Blockchain/Client/RPC/ETH.pm' => {
                                                                              'module' => 'Net::Async::Blockchain::Client::RPC::ETH',
                                                                              'mtime' => 1709527776,
                                                                              'noes' => {
                                                                                          'indirect' => '0'
                                                                                        },
                                                                              'requires' => {
                                                                                              'Net::Async::Blockchain::Client::RPC' => '0',
                                                                                              'parent' => '0',
                                                                                              'strict' => '0',
                                                                                              'warnings' => '0'
                                                                                            },
                                                                              'size' => 2192
                                                                            },
                            'lib/Net/Async/Blockchain/Client/RPC/ETH.pod' => {
                                                                               'mtime' => 1709527776,
                                                                               'size' => 3099
                                                                             },
                            'lib/Net/Async/Blockchain/Client/RPC/Omni.pm' => {
                                                                               'module' => 'Net::Async::Blockchain::Client::RPC::Omni',
                                                                               'mtime' => 1709527776,
                                                                               'noes' => {
                                                                                           'indirect' => '0'
                                                                                         },
                                                                               'requires' => {
                                                                                               'Net::Async::Blockchain::Client::RPC::BTC' => '0',
                                                                                               'parent' => '0',
                                                                                               'strict' => '0',
                                                                                               'warnings' => '0'
                                                                                             },
                                                                               'size' => 1475
                                                                             },
                            'lib/Net/Async/Blockchain/Client/RPC/Omni.pod' => {
                                                                                'mtime' => 1709527776,
                                                                                'size' => 3670
                                                                              },
                            'lib/Net/Async/Blockchain/Client/Websocket.pm' => {
                                                                                'module' => 'Net::Async::Blockchain::Client::Websocket',
                                                                                'mtime' => 1709527776,
                                                                                'noes' => {
                                                                                            'indirect' => '0'
                                                                                          },
                                                                                'requires' => {
                                                                                                'Future::AsyncAwait' => '0',
                                                                                                'IO::Async::Notifier' => '0',
                                                                                                'JSON::MaybeUTF8' => '0',
                                                                                                'Net::Async::WebSocket::Client' => '0',
                                                                                                'Protocol::WebSocket::Request' => '0',
                                                                                                'Ryu::Async' => '0',
                                                                                                'URI' => '0',
                                                                                                'curry' => '0',
                                                                                                'parent' => '0',
                                                                                                'strict' => '0',
                                                                                                'warnings' => '0'
                                                                                              },
                                                                                'size' => 4841
                                                                              },
                            'lib/Net/Async/Blockchain/Client/Websocket.pod' => {
                                                                                 'mtime' => 1709527776,
                                                                                 'size' => 3062
                                                                               },
                            'lib/Net/Async/Blockchain/Client/ZMQ.pm' => {
                                                                          'module' => 'Net::Async::Blockchain::Client::ZMQ',
                                                                          'mtime' => 1709527776,
                                                                          'noes' => {
                                                                                      'indirect' => '0'
                                                                                    },
                                                                          'requires' => {
                                                                                          'IO::Async::Handle' => '0',
                                                                                          'IO::Async::Notifier' => '0',
                                                                                          'Ryu::Async' => '0',
                                                                                          'Socket' => '0',
                                                                                          'ZMQ::Constants' => '0',
                                                                                          'ZMQ::LibZMQ3' => '0',
                                                                                          'constant' => '0',
                                                                                          'curry' => '0',
                                                                                          'parent' => '0',
                                                                                          'strict' => '0',
                                                                                          'warnings' => '0'
                                                                                        },
                                                                          'size' => 5870
                                                                        },
                            'lib/Net/Async/Blockchain/Client/ZMQ.pod' => {
                                                                           'mtime' => 1709527776,
                                                                           'size' => 3313
                                                                         },
                            'lib/Net/Async/Blockchain/ETH.pm' => {
                                                                   'module' => 'Net::Async::Blockchain::ETH',
                                                                   'mtime' => 1709527776,
                                                                   'noes' => {
                                                                               'indirect' => '0'
                                                                             },
                                                                   'requires' => {
                                                                                   'Future::AsyncAwait' => '0',
                                                                                   'Net::Async::Blockchain' => '0',
                                                                                   'Net::Async::Blockchain::Client::Websocket' => '0',
                                                                                   'curry' => '0',
                                                                                   'parent' => '0',
                                                                                   'strict' => '0',
                                                                                   'warnings' => '0'
                                                                                 },
                                                                   'size' => 3282
                                                                 },
                            'lib/Net/Async/Blockchain/ETH.pod' => {
                                                                    'mtime' => 1709527776,
                                                                    'size' => 2915
                                                                  },
                            'lib/Net/Async/Blockchain/Transaction.pm' => {
                                                                           'module' => 'Net::Async::Blockchain::Transaction',
                                                                           'mtime' => 1709527776,
                                                                           'noes' => {
                                                                                       'indirect' => '0'
                                                                                     },
                                                                           'requires' => {
                                                                                           'strict' => '0',
                                                                                           'warnings' => '0'
                                                                                         },
                                                                           'size' => 2440
                                                                         },
                            't/00-check-deps.t' => {
                                                     'mtime' => 1709527776,
                                                     'no_index' => 1,
                                                     'requires' => {
                                                                     'Test::CheckDeps' => '0.010',
                                                                     'Test::More' => '0.94',
                                                                     'strict' => '0',
                                                                     'warnings' => '0'
                                                                   },
                                                     'size' => 283
                                                   },
                            't/00-compile.t' => {
                                                  'mtime' => 1709527776,
                                                  'no_index' => 1,
                                                  'requires' => {
                                                                  'File::Spec' => '0',
                                                                  'IO::Handle' => '0',
                                                                  'IPC::Open3' => '0',
                                                                  'Test::More' => '0',
                                                                  'perl' => '5.006',
                                                                  'strict' => '0',
                                                                  'warnings' => '0'
                                                                },
                                                  'size' => 1830,
                                                  'suggests' => {
                                                                  'blib' => '1.01'
                                                                }
                                                },
                            't/00-report-prereqs.dd' => {
                                                          'mtime' => 1709527776,
                                                          'no_index' => 1,
                                                          'size' => 4350
                                                        },
                            't/00-report-prereqs.t' => {
                                                         'mtime' => 1709527776,
                                                         'no_index' => 1,
                                                         'requires' => {
                                                                         'ExtUtils::MakeMaker' => '0',
                                                                         'File::Spec' => '0',
                                                                         'Test::More' => '0',
                                                                         'strict' => '0',
                                                                         'warnings' => '0'
                                                                       },
                                                         'size' => 5893
                                                       },
                            't/btc.t' => {
                                           'mtime' => 1709527776,
                                           'no_index' => 1,
                                           'requires' => {
                                                           'IO::Async::Loop' => '0',
                                                           'IO::Async::Test' => '0',
                                                           'Net::Async::Blockchain::BTC' => '0',
                                                           'Test::Fatal' => '0',
                                                           'Test::More' => '0.88',
                                                           'Test::TCP' => '0',
                                                           'ZMQ::Constants' => '0',
                                                           'ZMQ::LibZMQ3' => '0',
                                                           'strict' => '0',
                                                           'warnings' => '0'
                                                         },
                                           'size' => 2282
                                         },
                            't/eth.t' => {
                                           'mtime' => 1709527776,
                                           'no_index' => 1,
                                           'requires' => {
                                                           'Future::AsyncAwait' => '0',
                                                           'IO::Async::Loop' => '0',
                                                           'IO::Async::Test' => '0',
                                                           'Net::Async::Blockchain::ETH' => '0',
                                                           'Ryu::Async' => '0',
                                                           'Test::Fatal' => '0',
                                                           'Test::MockModule' => '0',
                                                           'Test::More' => '0.88',
                                                           'strict' => '0',
                                                           'warnings' => '0'
                                                         },
                                           'size' => 2231
                                         },
                            't/rc/perlcriticrc' => {
                                                     'mtime' => 1709527776,
                                                     'no_index' => 1,
                                                     'size' => 868
                                                   },
                            't/rc/perltidyrc' => {
                                                   'mtime' => 1709527776,
                                                   'no_index' => 1,
                                                   'size' => 1434
                                                 },
                            't/rpc.t' => {
                                           'mtime' => 1709527776,
                                           'no_index' => 1,
                                           'noes' => {
                                                       'warnings' => '0'
                                                     },
                                           'requires' => {
                                                           'IO::Async::Loop' => '0',
                                                           'IO::Async::Test' => '0',
                                                           'Test::Fatal' => '0',
                                                           'Test::More' => '0.88',
                                                           'strict' => '0',
                                                           'warnings' => '0'
                                                         },
                                           'size' => 1594
                                         },
                            't/websocket.t' => {
                                                 'mtime' => 1709527776,
                                                 'no_index' => 1,
                                                 'requires' => {
                                                                 'IO::Async::Loop' => '0',
                                                                 'IO::Async::Test' => '0',
                                                                 'JSON::MaybeUTF8' => '0',
                                                                 'Net::Async::Blockchain::Client::Websocket' => '0',
                                                                 'Net::Async::WebSocket::Server' => '0',
                                                                 'Test::Fatal' => '0',
                                                                 'Test::More' => '0.88',
                                                                 'strict' => '0',
                                                                 'warnings' => '0'
                                                               },
                                                 'size' => 1337
                                               },
                            't/zmq.t' => {
                                           'mtime' => 1709527776,
                                           'no_index' => 1,
                                           'requires' => {
                                                           'IO::Async::Loop' => '0',
                                                           'IO::Async::Test' => '0',
                                                           'Test::Fatal' => '0',
                                                           'Test::More' => '0',
                                                           'Test::TCP' => '0',
                                                           'strict' => '0',
                                                           'warnings' => '0'
                                                         },
                                           'size' => 1519
                                         },
                            'xt/author/critic.t' => {
                                                      'mtime' => 1709527776,
                                                      'no_index' => 1,
                                                      'size' => 137
                                                    },
                            'xt/author/distmeta.t' => {
                                                        'mtime' => 1709527776,
                                                        'no_index' => 1,
                                                        'size' => 147
                                                      },
                            'xt/author/eol.t' => {
                                                   'mtime' => 1709527776,
                                                   'no_index' => 1,
                                                   'size' => 1661
                                                 },
                            'xt/author/minimum-version.t' => {
                                                               'mtime' => 1709527776,
                                                               'no_index' => 1,
                                                               'size' => 106
                                                             },
                            'xt/author/mojibake.t' => {
                                                        'mtime' => 1709527776,
                                                        'no_index' => 1,
                                                        'size' => 105
                                                      },
                            'xt/author/no-tabs.t' => {
                                                       'mtime' => 1709527776,
                                                       'no_index' => 1,
                                                       'size' => 1635
                                                     },
                            'xt/author/pod-syntax.t' => {
                                                          'mtime' => 1709527776,
                                                          'no_index' => 1,
                                                          'size' => 170
                                                        },
                            'xt/author/portability.t' => {
                                                           'mtime' => 1709527776,
                                                           'no_index' => 1,
                                                           'size' => 183
                                                         },
                            'xt/author/test-version.t' => {
                                                            'mtime' => 1709527776,
                                                            'no_index' => 1,
                                                            'size' => 415
                                                          },
                            'xt/release/common_spelling.t' => {
                                                                'mtime' => 1709527776,
                                                                'no_index' => 1,
                                                                'size' => 240
                                                              },
                            'xt/release/cpan-changes.t' => {
                                                             'mtime' => 1709527776,
                                                             'no_index' => 1,
                                                             'size' => 228
                                                           }
                          },
          'got_prereq_from' => 'META.yml',
          'ignored_files_array' => [
                                     't/00-check-deps.t',
                                     't/00-compile.t',
                                     't/00-report-prereqs.dd',
                                     't/00-report-prereqs.t',
                                     't/btc.t',
                                     't/eth.t',
                                     't/rc/perlcriticrc',
                                     't/rc/perltidyrc',
                                     't/rpc.t',
                                     't/websocket.t',
                                     't/zmq.t',
                                     'xt/author/critic.t',
                                     'xt/author/distmeta.t',
                                     'xt/author/eol.t',
                                     'xt/author/minimum-version.t',
                                     'xt/author/mojibake.t',
                                     'xt/author/no-tabs.t',
                                     'xt/author/pod-syntax.t',
                                     'xt/author/portability.t',
                                     'xt/author/test-version.t',
                                     'xt/release/common_spelling.t',
                                     'xt/release/cpan-changes.t'
                                   ],
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 0,
                          'has_license_in_source_file' => 0,
                          'has_manifest' => 1,
                          'has_meta_json' => 1,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 1,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 31,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 1,
                          'meta_yml_has_repository_resource' => 1,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 1,
                          'use_warnings' => 1
                        },
          'latest_mtime' => 1709527776,
          'license' => 'perl defined in META.yml defined in LICENSE',
          'license_from_yaml' => 'perl',
          'manifest_matches_dist' => 1,
          'meta_json' => {
                           'abstract' => 'base for blockchain subscription clients.',
                           'author' => [
                                         'DERIV <DERIV@cpan.org>'
                                       ],
                           'dynamic_config' => 0,
                           'generated_by' => 'Dist::Zilla version 6.029, CPAN::Meta::Converter version 2.150010',
                           'license' => [
                                          'perl_5'
                                        ],
                           'meta-spec' => {
                                            'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                            'version' => 2
                                          },
                           'name' => 'Net-Async-Blockchain',
                           'no_index' => {
                                           'directory' => [
                                                            'eg',
                                                            'share',
                                                            'shares',
                                                            't',
                                                            'xt'
                                                          ]
                                         },
                           'prereqs' => {
                                          'configure' => {
                                                           'requires' => {
                                                                           'ExtUtils::MakeMaker' => '7.64'
                                                                         }
                                                         },
                                          'develop' => {
                                                         'requires' => {
                                                                         'Devel::Cover' => '1.23',
                                                                         'Devel::Cover::Report::Codecov' => '0.14',
                                                                         'Dist::Zilla' => '5',
                                                                         'Dist::Zilla::Plugin::PerlTidy' => '0',
                                                                         'Dist::Zilla::Plugin::Test::Perl::Critic' => '0',
                                                                         'Dist::Zilla::PluginBundle::Author::DERIV' => '0',
                                                                         'Software::License::Perl_5' => '0',
                                                                         'Test::CPAN::Changes' => '0.19',
                                                                         'Test::CPAN::Meta' => '0',
                                                                         'Test::EOL' => '0',
                                                                         'Test::MinimumVersion' => '0',
                                                                         'Test::Mojibake' => '0',
                                                                         'Test::More' => '0.96',
                                                                         'Test::NoTabs' => '0',
                                                                         'Test::Perl::Critic' => '0',
                                                                         'Test::Pod' => '1.41',
                                                                         'Test::Portability::Files' => '0',
                                                                         'Test::Version' => '1'
                                                                       }
                                                       },
                                          'runtime' => {
                                                         'requires' => {
                                                                         'Digest::Keccak' => '0',
                                                                         'Future::AsyncAwait' => '0.23',
                                                                         'IO::Async::Handle' => '0',
                                                                         'IO::Async::Notifier' => '0',
                                                                         'IO::Async::SSL' => '0',
                                                                         'IO::Async::Timer::Periodic' => '0.72',
                                                                         'JSON::MaybeUTF8' => '1.002',
                                                                         'Math::BigInt' => '1.999814',
                                                                         'Net::Async::HTTP' => '0.43',
                                                                         'Net::Async::WebSocket' => '0.13',
                                                                         'Net::Async::WebSocket::Client' => '0',
                                                                         'Protocol::WebSocket::Request' => '0',
                                                                         'Ryu::Async' => '0.011',
                                                                         'Socket' => '0',
                                                                         'Syntax::Keyword::Try' => '0.09',
                                                                         'URI' => '0',
                                                                         'ZMQ::Constants' => '0',
                                                                         'ZMQ::LibZMQ3' => '1.19',
                                                                         'constant' => '0',
                                                                         'curry' => '0',
                                                                         'indirect' => '0.37',
                                                                         'parent' => '0',
                                                                         'perl' => '5.024',
                                                                         'strict' => '0',
                                                                         'warnings' => '0'
                                                                       }
                                                       },
                                          'test' => {
                                                      'recommends' => {
                                                                        'CPAN::Meta' => '2.120900'
                                                                      },
                                                      'requires' => {
                                                                      'ExtUtils::MakeMaker' => '0',
                                                                      'File::Spec' => '0',
                                                                      'IO::Async::Loop' => '0',
                                                                      'IO::Async::Test' => '0',
                                                                      'IO::Handle' => '0',
                                                                      'IPC::Open3' => '0',
                                                                      'Net::Async::WebSocket::Server' => '0',
                                                                      'Test::CheckDeps' => '0.010',
                                                                      'Test::Fatal' => '0',
                                                                      'Test::MockModule' => '0',
                                                                      'Test::More' => '0.98',
                                                                      'Test::TCP' => '0',
                                                                      'perl' => '5.024'
                                                                    }
                                                    }
                                        },
                           'provides' => {
                                           'Net::Async::Blockchain' => {
                                                                         'file' => 'lib/Net/Async/Blockchain.pm',
                                                                         'version' => '0.004'
                                                                       },
                                           'Net::Async::Blockchain::BTC' => {
                                                                              'file' => 'lib/Net/Async/Blockchain/BTC.pm',
                                                                              'version' => '0.004'
                                                                            },
                                           'Net::Async::Blockchain::Client::RPC' => {
                                                                                      'file' => 'lib/Net/Async/Blockchain/Client/RPC.pm',
                                                                                      'version' => '0.004'
                                                                                    },
                                           'Net::Async::Blockchain::Client::RPC::BTC' => {
                                                                                           'file' => 'lib/Net/Async/Blockchain/Client/RPC/BTC.pm',
                                                                                           'version' => '0.004'
                                                                                         },
                                           'Net::Async::Blockchain::Client::RPC::ETH' => {
                                                                                           'file' => 'lib/Net/Async/Blockchain/Client/RPC/ETH.pm',
                                                                                           'version' => '0.004'
                                                                                         },
                                           'Net::Async::Blockchain::Client::RPC::Omni' => {
                                                                                            'file' => 'lib/Net/Async/Blockchain/Client/RPC/Omni.pm',
                                                                                            'version' => '0.004'
                                                                                          },
                                           'Net::Async::Blockchain::Client::Websocket' => {
                                                                                            'file' => 'lib/Net/Async/Blockchain/Client/Websocket.pm',
                                                                                            'version' => '0.004'
                                                                                          },
                                           'Net::Async::Blockchain::Client::ZMQ' => {
                                                                                      'file' => 'lib/Net/Async/Blockchain/Client/ZMQ.pm',
                                                                                      'version' => '0.004'
                                                                                    },
                                           'Net::Async::Blockchain::ETH' => {
                                                                              'file' => 'lib/Net/Async/Blockchain/ETH.pm',
                                                                              'version' => '0.004'
                                                                            },
                                           'Net::Async::Blockchain::Transaction' => {
                                                                                      'file' => 'lib/Net/Async/Blockchain/Transaction.pm',
                                                                                      'version' => '0.004'
                                                                                    }
                                         },
                           'release_status' => 'stable',
                           'resources' => {
                                            'bugtracker' => {
                                                              'web' => 'https://github.com/binary-com/perl-Net-Async-Blockchain/issues'
                                                            },
                                            'homepage' => 'https://github.com/binary-com/perl-Net-Async-Blockchain',
                                            'repository' => {
                                                              'type' => 'git',
                                                              'url' => 'https://github.com/binary-com/perl-Net-Async-Blockchain.git',
                                                              'web' => 'https://github.com/binary-com/perl-Net-Async-Blockchain'
                                                            }
                                          },
                           'version' => '0.004',
                           'x_authority' => 'cpan:DERIV',
                           'x_contributors' => [
                                                 'Reginaldo Costa <53163145+reginaldo-binary@users.noreply.github.com>',
                                                 'Mohanad Zarzour <51309057+Binary-Mohanad@users.noreply.github.com>',
                                                 'Tanya Sahni <54654631+binary-tanya@users.noreply.github.com>',
                                                 'Arshad <arshad@binary.com>',
                                                 'Reginaldo Costa <regcostajr@gmail.com>',
                                                 'Zak B. Elep <zakame@zakame.net>',
                                                 'Arshad Munir <55420460+arshad-binary@users.noreply.github.com>',
                                                 'chylli-binary <52912308+chylli-binary@users.noreply.github.com>',
                                                 'kathleen-deriv <122329285+lim-deriv@users.noreply.github.com>',
                                                 'Arshad Munir <55420460+arshad-deriv@users.noreply.github.com>',
                                                 'mukesh-deriv <85932084+mukesh-deriv@users.noreply.github.com>',
                                                 'nihal-deriv <105702884+nihal-deriv@users.noreply.github.com>',
                                                 'Paul Evans <leonerd@leonerd.org.uk>',
                                                 'Reginaldo Costa <53163145+reginaldo-deriv@users.noreply.github.com>',
                                                 'Tom Molesworth <tom@binary.com>'
                                               ],
                           'x_generated_by_perl' => 'v5.26.2',
                           'x_serialization_backend' => 'Cpanel::JSON::XS version 4.32',
                           'x_spdx_expression' => 'Artistic-1.0-Perl OR GPL-1.0-or-later'
                         },
          'meta_json_is_parsable' => 1,
          'meta_json_spec_version' => 2,
          'meta_yml' => {
                          'abstract' => 'base for blockchain subscription clients.',
                          'author' => [
                                        'DERIV <DERIV@cpan.org>'
                                      ],
                          'build_requires' => {
                                                'ExtUtils::MakeMaker' => '0',
                                                'File::Spec' => '0',
                                                'IO::Async::Loop' => '0',
                                                'IO::Async::Test' => '0',
                                                'IO::Handle' => '0',
                                                'IPC::Open3' => '0',
                                                'Net::Async::WebSocket::Server' => '0',
                                                'Test::CheckDeps' => '0.010',
                                                'Test::Fatal' => '0',
                                                'Test::MockModule' => '0',
                                                'Test::More' => '0.98',
                                                'Test::TCP' => '0',
                                                'perl' => '5.024'
                                              },
                          'configure_requires' => {
                                                    'ExtUtils::MakeMaker' => '7.64'
                                                  },
                          'dynamic_config' => '0',
                          'generated_by' => 'Dist::Zilla version 6.029, CPAN::Meta::Converter version 2.150010',
                          'license' => 'perl',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'Net-Async-Blockchain',
                          'no_index' => {
                                          'directory' => [
                                                           'eg',
                                                           'share',
                                                           'shares',
                                                           't',
                                                           'xt'
                                                         ]
                                        },
                          'provides' => {
                                          'Net::Async::Blockchain' => {
                                                                        'file' => 'lib/Net/Async/Blockchain.pm',
                                                                        'version' => '0.004'
                                                                      },
                                          'Net::Async::Blockchain::BTC' => {
                                                                             'file' => 'lib/Net/Async/Blockchain/BTC.pm',
                                                                             'version' => '0.004'
                                                                           },
                                          'Net::Async::Blockchain::Client::RPC' => {
                                                                                     'file' => 'lib/Net/Async/Blockchain/Client/RPC.pm',
                                                                                     'version' => '0.004'
                                                                                   },
                                          'Net::Async::Blockchain::Client::RPC::BTC' => {
                                                                                          'file' => 'lib/Net/Async/Blockchain/Client/RPC/BTC.pm',
                                                                                          'version' => '0.004'
                                                                                        },
                                          'Net::Async::Blockchain::Client::RPC::ETH' => {
                                                                                          'file' => 'lib/Net/Async/Blockchain/Client/RPC/ETH.pm',
                                                                                          'version' => '0.004'
                                                                                        },
                                          'Net::Async::Blockchain::Client::RPC::Omni' => {
                                                                                           'file' => 'lib/Net/Async/Blockchain/Client/RPC/Omni.pm',
                                                                                           'version' => '0.004'
                                                                                         },
                                          'Net::Async::Blockchain::Client::Websocket' => {
                                                                                           'file' => 'lib/Net/Async/Blockchain/Client/Websocket.pm',
                                                                                           'version' => '0.004'
                                                                                         },
                                          'Net::Async::Blockchain::Client::ZMQ' => {
                                                                                     'file' => 'lib/Net/Async/Blockchain/Client/ZMQ.pm',
                                                                                     'version' => '0.004'
                                                                                   },
                                          'Net::Async::Blockchain::ETH' => {
                                                                             'file' => 'lib/Net/Async/Blockchain/ETH.pm',
                                                                             'version' => '0.004'
                                                                           },
                                          'Net::Async::Blockchain::Transaction' => {
                                                                                     'file' => 'lib/Net/Async/Blockchain/Transaction.pm',
                                                                                     'version' => '0.004'
                                                                                   }
                                        },
                          'requires' => {
                                          'Digest::Keccak' => '0',
                                          'Future::AsyncAwait' => '0.23',
                                          'IO::Async::Handle' => '0',
                                          'IO::Async::Notifier' => '0',
                                          'IO::Async::SSL' => '0',
                                          'IO::Async::Timer::Periodic' => '0.72',
                                          'JSON::MaybeUTF8' => '1.002',
                                          'Math::BigInt' => '1.999814',
                                          'Net::Async::HTTP' => '0.43',
                                          'Net::Async::WebSocket' => '0.13',
                                          'Net::Async::WebSocket::Client' => '0',
                                          'Protocol::WebSocket::Request' => '0',
                                          'Ryu::Async' => '0.011',
                                          'Socket' => '0',
                                          'Syntax::Keyword::Try' => '0.09',
                                          'URI' => '0',
                                          'ZMQ::Constants' => '0',
                                          'ZMQ::LibZMQ3' => '1.19',
                                          'constant' => '0',
                                          'curry' => '0',
                                          'indirect' => '0.37',
                                          'parent' => '0',
                                          'perl' => '5.024',
                                          'strict' => '0',
                                          'warnings' => '0'
                                        },
                          'resources' => {
                                           'bugtracker' => 'https://github.com/binary-com/perl-Net-Async-Blockchain/issues',
                                           'homepage' => 'https://github.com/binary-com/perl-Net-Async-Blockchain',
                                           'repository' => 'https://github.com/binary-com/perl-Net-Async-Blockchain.git'
                                         },
                          'version' => '0.004',
                          'x_authority' => 'cpan:DERIV',
                          'x_contributors' => [
                                                'Reginaldo Costa <53163145+reginaldo-binary@users.noreply.github.com>',
                                                'Mohanad Zarzour <51309057+Binary-Mohanad@users.noreply.github.com>',
                                                'Tanya Sahni <54654631+binary-tanya@users.noreply.github.com>',
                                                'Arshad <arshad@binary.com>',
                                                'Reginaldo Costa <regcostajr@gmail.com>',
                                                'Zak B. Elep <zakame@zakame.net>',
                                                'Arshad Munir <55420460+arshad-binary@users.noreply.github.com>',
                                                'chylli-binary <52912308+chylli-binary@users.noreply.github.com>',
                                                'kathleen-deriv <122329285+lim-deriv@users.noreply.github.com>',
                                                'Arshad Munir <55420460+arshad-deriv@users.noreply.github.com>',
                                                'mukesh-deriv <85932084+mukesh-deriv@users.noreply.github.com>',
                                                'nihal-deriv <105702884+nihal-deriv@users.noreply.github.com>',
                                                'Paul Evans <leonerd@leonerd.org.uk>',
                                                'Reginaldo Costa <53163145+reginaldo-deriv@users.noreply.github.com>',
                                                'Tom Molesworth <tom@binary.com>'
                                              ],
                          'x_generated_by_perl' => 'v5.26.2',
                          'x_serialization_backend' => 'YAML::Tiny version 1.73',
                          'x_spdx_expression' => 'Artistic-1.0-Perl OR GPL-1.0-or-later'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/Net/Async/Blockchain.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Net::Async::Blockchain'
                         },
                         {
                           'file' => 'lib/Net/Async/Blockchain/BTC.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Net::Async::Blockchain::BTC'
                         },
                         {
                           'file' => 'lib/Net/Async/Blockchain/Client/RPC.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Net::Async::Blockchain::Client::RPC'
                         },
                         {
                           'file' => 'lib/Net/Async/Blockchain/Client/RPC/BTC.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Net::Async::Blockchain::Client::RPC::BTC'
                         },
                         {
                           'file' => 'lib/Net/Async/Blockchain/Client/RPC/ETH.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Net::Async::Blockchain::Client::RPC::ETH'
                         },
                         {
                           'file' => 'lib/Net/Async/Blockchain/Client/RPC/Omni.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Net::Async::Blockchain::Client::RPC::Omni'
                         },
                         {
                           'file' => 'lib/Net/Async/Blockchain/Client/Websocket.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Net::Async::Blockchain::Client::Websocket'
                         },
                         {
                           'file' => 'lib/Net/Async/Blockchain/Client/ZMQ.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Net::Async::Blockchain::Client::ZMQ'
                         },
                         {
                           'file' => 'lib/Net/Async/Blockchain/ETH.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Net::Async::Blockchain::ETH'
                         },
                         {
                           'file' => 'lib/Net/Async/Blockchain/Transaction.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Net::Async::Blockchain::Transaction'
                         }
                       ],
          'no_index' => '^eg/;^share/;^shares/;^t/;^xt/',
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/Net-Async-Blockchain.tar.gz',
          'prereq' => [
                        {
                          'is_prereq' => 1,
                          'requires' => 'Digest::Keccak',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'configure_requires',
                          'version' => '7.64'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'File::Spec',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Future::AsyncAwait',
                          'type' => 'runtime_requires',
                          'version' => '0.23'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'IO::Async::Handle',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'IO::Async::Loop',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'IO::Async::Notifier',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'IO::Async::SSL',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'IO::Async::Test',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'IO::Async::Timer::Periodic',
                          'type' => 'runtime_requires',
                          'version' => '0.72'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'IO::Handle',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'IPC::Open3',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'JSON::MaybeUTF8',
                          'type' => 'runtime_requires',
                          'version' => '1.002'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Math::BigInt',
                          'type' => 'runtime_requires',
                          'version' => '1.999814'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Net::Async::HTTP',
                          'type' => 'runtime_requires',
                          'version' => '0.43'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Net::Async::WebSocket',
                          'type' => 'runtime_requires',
                          'version' => '0.13'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Net::Async::WebSocket::Client',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Net::Async::WebSocket::Server',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Protocol::WebSocket::Request',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Ryu::Async',
                          'type' => 'runtime_requires',
                          'version' => '0.011'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Socket',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Syntax::Keyword::Try',
                          'type' => 'runtime_requires',
                          'version' => '0.09'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::CheckDeps',
                          'type' => 'build_requires',
                          'version' => '0.010'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::Fatal',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::MockModule',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'build_requires',
                          'version' => '0.98'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::TCP',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'URI',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'ZMQ::Constants',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'ZMQ::LibZMQ3',
                          'type' => 'runtime_requires',
                          'version' => '1.19'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'constant',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'curry',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'indirect',
                          'type' => 'runtime_requires',
                          'version' => '0.37'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'parent',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'build_requires',
                          'version' => '5.024'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => '5.024'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'strict',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'warnings',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        }
                      ],
          'released' => 1709583455,
          'size_packed' => 27705,
          'size_unpacked' => 126934,
          'test_files' => [
                            't/00-check-deps.t',
                            't/00-compile.t',
                            't/00-report-prereqs.t',
                            't/btc.t',
                            't/eth.t',
                            't/rpc.t',
                            't/websocket.t',
                            't/zmq.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'requires' => {
                                                       'ExtUtils::MakeMaker' => '7.64',
                                                       'perl' => '5.024',
                                                       'strict' => '0',
                                                       'warnings' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'noes' => {
                                                 'indirect' => '0'
                                               },
                                     'requires' => {
                                                     'Future::AsyncAwait' => '0',
                                                     'IO::Async::Handle' => '0',
                                                     'IO::Async::Notifier' => '0',
                                                     'JSON::MaybeUTF8' => '0',
                                                     'Net::Async::HTTP' => '0',
                                                     'Net::Async::WebSocket::Client' => '0',
                                                     'Protocol::WebSocket::Request' => '0',
                                                     'Ryu::Async' => '0',
                                                     'Socket' => '0',
                                                     'URI' => '0',
                                                     'ZMQ::Constants' => '0',
                                                     'ZMQ::LibZMQ3' => '0',
                                                     'constant' => '0',
                                                     'curry' => '0',
                                                     'parent' => '0',
                                                     'strict' => '0',
                                                     'warnings' => '0'
                                                   }
                                   },
                      'test' => {
                                  'noes' => {
                                              'warnings' => '0'
                                            },
                                  'requires' => {
                                                  'ExtUtils::MakeMaker' => '0',
                                                  'File::Spec' => '0',
                                                  'Future::AsyncAwait' => '0',
                                                  'IO::Async::Loop' => '0',
                                                  'IO::Async::Test' => '0',
                                                  'IO::Handle' => '0',
                                                  'IPC::Open3' => '0',
                                                  'JSON::MaybeUTF8' => '0',
                                                  'Net::Async::WebSocket::Server' => '0',
                                                  'Ryu::Async' => '0',
                                                  'Test::CheckDeps' => '0.010',
                                                  'Test::Fatal' => '0',
                                                  'Test::MockModule' => '0',
                                                  'Test::More' => '0.94',
                                                  'Test::TCP' => '0',
                                                  'ZMQ::Constants' => '0',
                                                  'ZMQ::LibZMQ3' => '0',
                                                  'perl' => '5.006',
                                                  'strict' => '0',
                                                  'warnings' => '0'
                                                },
                                  'suggests' => {
                                                  'blib' => '1.01'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'Net-Async-Blockchain'
        };
1;
