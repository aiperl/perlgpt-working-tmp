$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'App::CSVUtils::csv_mix_formulas' => 'Mix several formulas/recipes (lists of ingredients and their weights/volumes) into one, and output the combined formula',
                                  'csv-mix-formulas' => 'Mix several formulas/recipes (lists of ingredients and their weights/volumes) into one, and output the combined formula'
                                },
          'author' => '',
          'dir_lib' => 'lib',
          'dir_t' => 't',
          'dirs' => 5,
          'dirs_array' => [
                            'lib/App/CSVUtils',
                            'lib/App',
                            'lib',
                            'script',
                            't'
                          ],
          'dist' => 'App-CSVUtils-csv_mix_formulas',
          'dynamic_config' => 0,
          'error' => {
                       'extracts_nicely' => 'expected App-CSVUtils-csv_mix_formulas but got App-CSVUtils-csv_mix_formulas-0.002'
                     },
          'extension' => 'tar.gz',
          'external_license_file' => 'LICENSE',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_changelog' => 'Changes',
          'file_dist_ini' => 'dist.ini',
          'file_license' => 'LICENSE',
          'file_makefile_pl' => 'Makefile.PL',
          'file_manifest' => 'MANIFEST',
          'file_meta_json' => 'META.json',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README',
          'files' => 16,
          'files_array' => [
                             'Changes',
                             'LICENSE',
                             'MANIFEST',
                             'META.json',
                             'META.yml',
                             'Makefile.PL',
                             'README',
                             'dist.ini',
                             'lib/App/CSVUtils/csv_mix_formulas.pm',
                             'script/csv-mix-formulas',
                             't/00-compile.t',
                             't/author-critic.t',
                             't/author-pod-coverage.t',
                             't/author-pod-syntax.t',
                             't/release-rinci.t',
                             'weaver.ini'
                           ],
          'files_hash' => {
                            'Changes' => {
                                           'mtime' => 1708774251,
                                           'size' => 489
                                         },
                            'LICENSE' => {
                                           'mtime' => 1708774251,
                                           'size' => 19789
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1708774251,
                                            'size' => 322
                                          },
                            'META.json' => {
                                             'mtime' => 1708774251,
                                             'size' => 25067
                                           },
                            'META.yml' => {
                                            'mtime' => 1708774251,
                                            'size' => 16790
                                          },
                            'Makefile.PL' => {
                                               'mtime' => 1708774251,
                                               'requires' => {
                                                               'ExtUtils::MakeMaker' => '0',
                                                               'perl' => '5.010001',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 2514
                                             },
                            'README' => {
                                          'mtime' => 1708774251,
                                          'size' => 10561
                                        },
                            'dist.ini' => {
                                            'mtime' => 1708774251,
                                            'size' => 641
                                          },
                            'lib/App/CSVUtils/csv_mix_formulas.pm' => {
                                                                        'license' => 'Perl_5',
                                                                        'module' => 'App::CSVUtils::csv_mix_formulas',
                                                                        'mtime' => 1708774251,
                                                                        'requires' => {
                                                                                        'App::CSVUtils' => '0',
                                                                                        'List::Util' => '0',
                                                                                        'Log::ger' => '0',
                                                                                        'perl' => '5.010001',
                                                                                        'strict' => '0',
                                                                                        'warnings' => '0'
                                                                                      },
                                                                        'size' => 17842
                                                                      },
                            'script/csv-mix-formulas' => {
                                                           'license' => 'Perl_5',
                                                           'mtime' => 1708774251,
                                                           'size' => 16167
                                                         },
                            't/00-compile.t' => {
                                                  'mtime' => 1708774251,
                                                  'requires' => {
                                                                  'File::Spec' => '0',
                                                                  'IO::Handle' => '0',
                                                                  'IPC::Open3' => '0',
                                                                  'Test::More' => '0',
                                                                  'perl' => '5.006',
                                                                  'strict' => '0',
                                                                  'warnings' => '0'
                                                                },
                                                  'size' => 2785,
                                                  'suggests' => {
                                                                  'blib' => '1.01'
                                                                }
                                                },
                            't/author-critic.t' => {
                                                     'mtime' => 1708774251,
                                                     'recommends' => {
                                                                       'Test::Perl::Critic' => '0',
                                                                       'strict' => '0',
                                                                       'warnings' => '0'
                                                                     },
                                                     'size' => 508
                                                   },
                            't/author-pod-coverage.t' => {
                                                           'mtime' => 1708774251,
                                                           'recommends' => {
                                                                             'Pod::Coverage::TrustPod' => '0',
                                                                             'Test::Pod::Coverage' => '1.08',
                                                                             'strict' => '0',
                                                                             'warnings' => '0'
                                                                           },
                                                           'size' => 375
                                                         },
                            't/author-pod-syntax.t' => {
                                                         'mtime' => 1708774251,
                                                         'recommends' => {
                                                                           'Test::More' => '0',
                                                                           'Test::Pod' => '1.41',
                                                                           'strict' => '0',
                                                                           'warnings' => '0'
                                                                         },
                                                         'size' => 300
                                                       },
                            't/release-rinci.t' => {
                                                     'mtime' => 1708774251,
                                                     'recommends' => {
                                                                       'Test::More' => '0'
                                                                     },
                                                     'size' => 379,
                                                     'suggests' => {
                                                                     'Test::Rinci' => '0.01'
                                                                   }
                                                   },
                            'weaver.ini' => {
                                              'mtime' => 1708774251,
                                              'size' => 21
                                            }
                          },
          'got_prereq_from' => 'META.yml',
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 1,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 1,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 33,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 1,
                          'meta_yml_has_repository_resource' => 1,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 1,
                          'use_warnings' => 1
                        },
          'latest_mtime' => 1708774251,
          'license' => 'perl defined in META.yml defined in LICENSE',
          'license_file' => 'lib/App/CSVUtils/csv_mix_formulas.pm,script/csv-mix-formulas',
          'license_from_yaml' => 'perl',
          'license_in_pod' => 1,
          'license_type' => 'Perl_5',
          'licenses' => {
                          'Perl_5' => [
                                        'lib/App/CSVUtils/csv_mix_formulas.pm',
                                        'script/csv-mix-formulas'
                                      ]
                        },
          'manifest_matches_dist' => 1,
          'meta_json' => {
                           'abstract' => 'Mix several formulas/recipes (lists of ingredients and their weights/volumes) into one, and output the combined formula',
                           'author' => [
                                         'perlancar <perlancar@cpan.org>'
                                       ],
                           'dynamic_config' => 0,
                           'generated_by' => 'Dist::Zilla version 6.031, CPAN::Meta::Converter version 2.150010',
                           'license' => [
                                          'perl_5'
                                        ],
                           'meta-spec' => {
                                            'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                            'version' => 2
                                          },
                           'name' => 'App-CSVUtils-csv_mix_formulas',
                           'prereqs' => {
                                          'configure' => {
                                                           'requires' => {
                                                                           'ExtUtils::MakeMaker' => '0'
                                                                         }
                                                         },
                                          'develop' => {
                                                         'requires' => {
                                                                         'Pod::Coverage::TrustPod' => '0',
                                                                         'Test::Perl::Critic' => '0',
                                                                         'Test::Pod' => '1.41',
                                                                         'Test::Pod::Coverage' => '1.08',
                                                                         'Test::Rinci' => '0.151'
                                                                       },
                                                         'x_spec' => {
                                                                       'Rinci' => 'v1.1.102'
                                                                     }
                                                       },
                                          'runtime' => {
                                                         'requires' => {
                                                                         'App::CSVUtils' => '0.032',
                                                                         'Data::Sah::Compiler::perl::TH::array' => '0.914',
                                                                         'Data::Sah::Compiler::perl::TH::bool' => '0.914',
                                                                         'Data::Sah::Compiler::perl::TH::str' => '0.914',
                                                                         'Data::Sah::Filter::perl::Path::expand_tilde_when_on_unix' => '0',
                                                                         'Data::Sah::Filter::perl::Path::strip_slashes_when_on_unix' => '0',
                                                                         'List::Util' => '1.54',
                                                                         'Log::ger' => '0.038',
                                                                         'Perinci::CmdLine::Any' => '0.154',
                                                                         'Perinci::CmdLine::Lite' => '1.924',
                                                                         'Perinci::Sub::XCompletion::filename' => '0',
                                                                         'Sah::Schema::filename' => '0',
                                                                         'Sah::Schema::true' => '0',
                                                                         'perl' => '5.010001',
                                                                         'strict' => '0',
                                                                         'warnings' => '0'
                                                                       }
                                                       },
                                          'test' => {
                                                      'requires' => {
                                                                      'File::Spec' => '0',
                                                                      'IO::Handle' => '0',
                                                                      'IPC::Open3' => '0',
                                                                      'Test::More' => '0'
                                                                    }
                                                    }
                                        },
                           'provides' => {
                                           'App::CSVUtils::csv_mix_formulas' => {
                                                                                  'file' => 'lib/App/CSVUtils/csv_mix_formulas.pm',
                                                                                  'version' => '0.002'
                                                                                }
                                         },
                           'release_status' => 'stable',
                           'resources' => {
                                            'bugtracker' => {
                                                              'web' => 'https://rt.cpan.org/Public/Dist/Display.html?Name=App-CSVUtils-csv_mix_formulas'
                                                            },
                                            'homepage' => 'https://metacpan.org/release/App-CSVUtils-csv_mix_formulas',
                                            'repository' => {
                                                              'type' => 'git',
                                                              'url' => 'git://github.com/perlancar/perl-App-CSVUtils-csv_mix_formulas.git',
                                                              'web' => 'https://github.com/perlancar/perl-App-CSVUtils-csv_mix_formulas'
                                                            }
                                          },
                           'version' => '0.002',
                           'x_Dist_Zilla' => {
                                               'perl' => {
                                                           'version' => '5.038002'
                                                         },
                                               'plugins' => [
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::GenPericmdScript',
                                                                'name' => 'GenPericmdScript csv-mix-formulas',
                                                                'version' => '0.425'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::InsertExecsList',
                                                                'name' => 'InsertExecsList',
                                                                'version' => '0.032'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::PERLANCAR::CheckPendingRelease',
                                                                'name' => '@Author::PERLANCAR/PERLANCAR::CheckPendingRelease',
                                                                'version' => '0.001'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::GatherDir',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::GatherDir' => {
                                                                                                                    'exclude_filename' => [],
                                                                                                                    'exclude_match' => [],
                                                                                                                    'follow_symlinks' => 0,
                                                                                                                    'include_dotfiles' => 0,
                                                                                                                    'prefix' => '',
                                                                                                                    'prune_directory' => [],
                                                                                                                    'root' => '.'
                                                                                                                  }
                                                                            },
                                                                'name' => '@Author::PERLANCAR/@Filter/GatherDir',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::PruneCruft',
                                                                'name' => '@Author::PERLANCAR/@Filter/PruneCruft',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::ManifestSkip',
                                                                'name' => '@Author::PERLANCAR/@Filter/ManifestSkip',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::MetaYAML',
                                                                'name' => '@Author::PERLANCAR/@Filter/MetaYAML',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::License',
                                                                'name' => '@Author::PERLANCAR/@Filter/License',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::PodCoverageTests',
                                                                'name' => '@Author::PERLANCAR/@Filter/PodCoverageTests',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::PodSyntaxTests',
                                                                'name' => '@Author::PERLANCAR/@Filter/PodSyntaxTests',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::ExtraTests',
                                                                'name' => '@Author::PERLANCAR/@Filter/ExtraTests',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::ExecDir',
                                                                'name' => '@Author::PERLANCAR/@Filter/ExecDir',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::ShareDir',
                                                                'name' => '@Author::PERLANCAR/@Filter/ShareDir',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::MakeMaker',
                                                                'config' => {
                                                                              'Dist::Zilla::Role::TestRunner' => {
                                                                                                                   'default_jobs' => 1
                                                                                                                 }
                                                                            },
                                                                'name' => '@Author::PERLANCAR/@Filter/MakeMaker',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Manifest',
                                                                'name' => '@Author::PERLANCAR/@Filter/Manifest',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::ConfirmRelease',
                                                                'name' => '@Author::PERLANCAR/@Filter/ConfirmRelease',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::ExecDir',
                                                                'name' => '@Author::PERLANCAR/ExecDir script',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::PERLANCAR::BeforeBuild',
                                                                'name' => '@Author::PERLANCAR/PERLANCAR::BeforeBuild',
                                                                'version' => '0.610'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Rinci::AbstractFromMeta',
                                                                'name' => '@Author::PERLANCAR/Rinci::AbstractFromMeta',
                                                                'version' => '0.10'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::PodnameFromFilename',
                                                                'name' => '@Author::PERLANCAR/PodnameFromFilename',
                                                                'version' => '0.02'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::PERLANCAR::EnsurePrereqToSpec',
                                                                'config' => {
                                                                              'Dist::Zilla::Role::ModuleMetadata' => {
                                                                                                                       'Module::Metadata' => '1.000037',
                                                                                                                       'version' => '0.006'
                                                                                                                     }
                                                                            },
                                                                'name' => '@Author::PERLANCAR/PERLANCAR::EnsurePrereqToSpec',
                                                                'version' => '0.064'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::PERLANCAR::MetaResources',
                                                                'name' => '@Author::PERLANCAR/PERLANCAR::MetaResources',
                                                                'version' => '0.043'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::CheckChangeLog',
                                                                'name' => '@Author::PERLANCAR/CheckChangeLog',
                                                                'version' => '0.05'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::CheckMetaResources',
                                                                'name' => '@Author::PERLANCAR/CheckMetaResources',
                                                                'version' => '0.001'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::CheckSelfDependency',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::CheckSelfDependency' => {
                                                                                                                              'finder' => [
                                                                                                                                            ':InstallModules'
                                                                                                                                          ]
                                                                                                                            },
                                                                              'Dist::Zilla::Role::ModuleMetadata' => {
                                                                                                                       'Module::Metadata' => '1.000037',
                                                                                                                       'version' => '0.006'
                                                                                                                     }
                                                                            },
                                                                'name' => '@Author::PERLANCAR/CheckSelfDependency',
                                                                'version' => '0.011'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Git::Contributors',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::Git::Contributors' => {
                                                                                                                            'git_version' => '2.25.1',
                                                                                                                            'include_authors' => 0,
                                                                                                                            'include_releaser' => 1,
                                                                                                                            'order_by' => 'name',
                                                                                                                            'paths' => []
                                                                                                                          }
                                                                            },
                                                                'name' => '@Author::PERLANCAR/Git::Contributors',
                                                                'version' => '0.036'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::CopyrightYearFromGit',
                                                                'name' => '@Author::PERLANCAR/CopyrightYearFromGit',
                                                                'version' => '0.009'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::IfBuilt',
                                                                'name' => '@Author::PERLANCAR/IfBuilt',
                                                                'version' => '0.02'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::MetaJSON',
                                                                'name' => '@Author::PERLANCAR/MetaJSON',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::MetaConfig',
                                                                'name' => '@Author::PERLANCAR/MetaConfig',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::MetaProvides::Package',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::MetaProvides::Package' => {
                                                                                                                                'finder_objects' => [
                                                                                                                                                      {
                                                                                                                                                        'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                                                                                                        'name' => '@Author::PERLANCAR/MetaProvides::Package/AUTOVIV/:InstallModulesPM',
                                                                                                                                                        'version' => '6.031'
                                                                                                                                                      }
                                                                                                                                                    ],
                                                                                                                                'include_underscores' => 0
                                                                                                                              },
                                                                              'Dist::Zilla::Role::MetaProvider::Provider' => {
                                                                                                                               '$Dist::Zilla::Role::MetaProvider::Provider::VERSION' => '2.002004',
                                                                                                                               'inherit_missing' => 1,
                                                                                                                               'inherit_version' => 1,
                                                                                                                               'meta_noindex' => 1
                                                                                                                             },
                                                                              'Dist::Zilla::Role::ModuleMetadata' => {
                                                                                                                       'Module::Metadata' => '1.000037',
                                                                                                                       'version' => '0.006'
                                                                                                                     }
                                                                            },
                                                                'name' => '@Author::PERLANCAR/MetaProvides::Package',
                                                                'version' => '2.004003'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::PERLANCAR::Authority',
                                                                'name' => '@Author::PERLANCAR/PERLANCAR::Authority',
                                                                'version' => '0.001'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::OurDate',
                                                                'name' => '@Author::PERLANCAR/OurDate',
                                                                'version' => '0.040'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::OurDist',
                                                                'name' => '@Author::PERLANCAR/OurDist',
                                                                'version' => '0.02'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::OurPkgVersion',
                                                                'name' => '@Author::PERLANCAR/OurPkgVersion',
                                                                'version' => '0.21'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::PodWeaver',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::PodWeaver' => {
                                                                                                                    'finder' => [
                                                                                                                                  ':InstallModules',
                                                                                                                                  ':PerlExecFiles'
                                                                                                                                ],
                                                                                                                    'plugins' => [
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Plugin::EnsurePod5',
                                                                                                                                     'name' => '@CorePrep/EnsurePod5',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Plugin::H1Nester',
                                                                                                                                     'name' => '@CorePrep/H1Nester',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Name',
                                                                                                                                     'name' => '@Author::PERLANCAR/Name',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Version',
                                                                                                                                     'name' => '@Author::PERLANCAR/Version',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Region',
                                                                                                                                     'name' => '@Author::PERLANCAR/prelude',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Generic',
                                                                                                                                     'name' => 'SYNOPSIS',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Generic',
                                                                                                                                     'name' => 'DESCRIPTION',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Generic',
                                                                                                                                     'name' => 'OVERVIEW',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Collect',
                                                                                                                                     'name' => 'ATTRIBUTES',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Collect',
                                                                                                                                     'name' => 'METHODS',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Collect',
                                                                                                                                     'name' => 'FUNCTIONS',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Leftovers',
                                                                                                                                     'name' => '@Author::PERLANCAR/Leftovers',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Region',
                                                                                                                                     'name' => '@Author::PERLANCAR/postlude',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Completion::GetoptLongComplete',
                                                                                                                                     'name' => '@Author::PERLANCAR/Completion::GetoptLongComplete',
                                                                                                                                     'version' => '0.08'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Completion::GetoptLongSubcommand',
                                                                                                                                     'name' => '@Author::PERLANCAR/Completion::GetoptLongSubcommand',
                                                                                                                                     'version' => '0.04'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Completion::GetoptLongMore',
                                                                                                                                     'name' => '@Author::PERLANCAR/Completion::GetoptLongMore',
                                                                                                                                     'version' => '0.001'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Homepage::DefaultCPAN',
                                                                                                                                     'name' => '@Author::PERLANCAR/Homepage::DefaultCPAN',
                                                                                                                                     'version' => '0.05'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Source::DefaultGitHub',
                                                                                                                                     'name' => '@Author::PERLANCAR/Source::DefaultGitHub',
                                                                                                                                     'version' => '0.07'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Bugs::DefaultRT',
                                                                                                                                     'name' => '@Author::PERLANCAR/Bugs::DefaultRT',
                                                                                                                                     'version' => '0.06'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Authors',
                                                                                                                                     'name' => '@Author::PERLANCAR/Authors',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Contributors',
                                                                                                                                     'name' => '@Author::PERLANCAR/Contributors',
                                                                                                                                     'version' => '0.009'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::PERLANCAR::Contributing',
                                                                                                                                     'name' => '@Author::PERLANCAR/PERLANCAR/Contributing',
                                                                                                                                     'version' => '0.293'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Section::Legal',
                                                                                                                                     'name' => '@Author::PERLANCAR/Legal',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Plugin::Rinci',
                                                                                                                                     'name' => '@Author::PERLANCAR/Rinci',
                                                                                                                                     'version' => '0.786'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Plugin::AppendPrepend',
                                                                                                                                     'name' => '@Author::PERLANCAR/AppendPrepend',
                                                                                                                                     'version' => '0.021'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Plugin::EnsureUniqueSections',
                                                                                                                                     'name' => '@Author::PERLANCAR/EnsureUniqueSections',
                                                                                                                                     'version' => '0.163250'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Plugin::SingleEncoding',
                                                                                                                                     'name' => '@Author::PERLANCAR/SingleEncoding',
                                                                                                                                     'version' => '4.019'
                                                                                                                                   },
                                                                                                                                   {
                                                                                                                                     'class' => 'Pod::Weaver::Plugin::PERLANCAR::SortSections',
                                                                                                                                     'name' => '@Author::PERLANCAR/PERLANCAR::SortSections',
                                                                                                                                     'version' => '0.082'
                                                                                                                                   }
                                                                                                                                 ]
                                                                                                                  }
                                                                            },
                                                                'name' => '@Author::PERLANCAR/PodWeaver',
                                                                'version' => '4.010'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::PruneFiles',
                                                                'name' => '@Author::PERLANCAR/PruneFiles',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Pod2Readme',
                                                                'name' => '@Author::PERLANCAR/Pod2Readme',
                                                                'version' => '0.004'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Rinci::AddPrereqs',
                                                                'name' => '@Author::PERLANCAR/Rinci::AddPrereqs',
                                                                'version' => '0.145'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Rinci::AddToDb',
                                                                'name' => '@Author::PERLANCAR/Rinci::AddToDb',
                                                                'version' => '0.020'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Rinci::EmbedValidator',
                                                                'name' => '@Author::PERLANCAR/Rinci::EmbedValidator',
                                                                'version' => '0.251'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::SetScriptShebang',
                                                                'name' => '@Author::PERLANCAR/SetScriptShebang',
                                                                'version' => '0.01'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Test::Compile',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::Test::Compile' => {
                                                                                                                        'bail_out_on_fail' => 0,
                                                                                                                        'fail_on_warning' => 'author',
                                                                                                                        'fake_home' => 0,
                                                                                                                        'filename' => 't/00-compile.t',
                                                                                                                        'module_finder' => [
                                                                                                                                             ':InstallModules'
                                                                                                                                           ],
                                                                                                                        'needs_display' => 0,
                                                                                                                        'phase' => 'test',
                                                                                                                        'script_finder' => [
                                                                                                                                             ':PerlExecFiles'
                                                                                                                                           ],
                                                                                                                        'skips' => [],
                                                                                                                        'switch' => []
                                                                                                                      }
                                                                            },
                                                                'name' => '@Author::PERLANCAR/Test::Compile',
                                                                'version' => '2.058'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Test::Perl::Critic::Subset',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::Test::Perl::Critic::Subset' => {
                                                                                                                                     'finder' => [
                                                                                                                                                   ':ExecFiles',
                                                                                                                                                   ':InstallModules',
                                                                                                                                                   ':TestFiles'
                                                                                                                                                 ]
                                                                                                                                   }
                                                                            },
                                                                'name' => '@Author::PERLANCAR/Test::Perl::Critic::Subset',
                                                                'version' => '3.001.006'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Test::Rinci',
                                                                'name' => '@Author::PERLANCAR/Test::Rinci',
                                                                'version' => '0.040'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::StaticInstall',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::StaticInstall' => {
                                                                                                                        'dry_run' => 0,
                                                                                                                        'mode' => 'on'
                                                                                                                      }
                                                                            },
                                                                'name' => '@Author::PERLANCAR/StaticInstall',
                                                                'version' => '0.012'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::EnsureSQLSchemaVersionedTest',
                                                                'name' => '@Author::PERLANCAR/EnsureSQLSchemaVersionedTest',
                                                                'version' => '0.03'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Acme::CPANModules::Blacklist',
                                                                'name' => '@Author::PERLANCAR/Acme::CPANModules::Blacklist',
                                                                'version' => '0.002'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Prereqs::EnsureVersion',
                                                                'name' => '@Author::PERLANCAR/Prereqs::EnsureVersion',
                                                                'version' => '0.050'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Prereqs::CheckCircular',
                                                                'name' => '@Author::PERLANCAR/Prereqs::CheckCircular',
                                                                'version' => '0.007'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::UploadToCPAN::WWWPAUSESimple',
                                                                'name' => '@Author::PERLANCAR/UploadToCPAN::WWWPAUSESimple',
                                                                'version' => '0.04'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Prereqs',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::Prereqs' => {
                                                                                                                  'phase' => 'test',
                                                                                                                  'type' => 'requires'
                                                                                                                }
                                                                            },
                                                                'name' => 'TestRequires',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Prereqs',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::Prereqs' => {
                                                                                                                  'phase' => 'runtime',
                                                                                                                  'type' => 'requires'
                                                                                                                }
                                                                            },
                                                                'name' => 'Prereqs',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::Prereqs',
                                                                'config' => {
                                                                              'Dist::Zilla::Plugin::Prereqs' => {
                                                                                                                  'phase' => 'develop',
                                                                                                                  'type' => 'x_spec'
                                                                                                                }
                                                                            },
                                                                'name' => 'DevelopX_spec',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':InstallModules',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':IncModules',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':TestFiles',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':ExtraTestFiles',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':ExecFiles',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':PerlExecFiles',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':ShareFiles',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':MainModule',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':AllFiles',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => ':NoFiles',
                                                                'version' => '6.031'
                                                              },
                                                              {
                                                                'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                'name' => '@Author::PERLANCAR/MetaProvides::Package/AUTOVIV/:InstallModulesPM',
                                                                'version' => '6.031'
                                                              }
                                                            ],
                                               'zilla' => {
                                                            'class' => 'Dist::Zilla::Dist::Builder',
                                                            'config' => {
                                                                          'is_trial' => 0
                                                                        },
                                                            'version' => '6.031'
                                                          }
                                             },
                           'x_authority' => 'cpan:PERLANCAR',
                           'x_generated_by_perl' => 'v5.38.2',
                           'x_serialization_backend' => 'Cpanel::JSON::XS version 4.37',
                           'x_spdx_expression' => 'Artistic-1.0-Perl OR GPL-1.0-or-later',
                           'x_static_install' => 1
                         },
          'meta_json_is_parsable' => 1,
          'meta_json_spec_version' => 2,
          'meta_yml' => {
                          'abstract' => 'Mix several formulas/recipes (lists of ingredients and their weights/volumes) into one, and output the combined formula',
                          'author' => [
                                        'perlancar <perlancar@cpan.org>'
                                      ],
                          'build_requires' => {
                                                'File::Spec' => '0',
                                                'IO::Handle' => '0',
                                                'IPC::Open3' => '0',
                                                'Test::More' => '0'
                                              },
                          'configure_requires' => {
                                                    'ExtUtils::MakeMaker' => '0'
                                                  },
                          'dynamic_config' => '0',
                          'generated_by' => 'Dist::Zilla version 6.031, CPAN::Meta::Converter version 2.150010',
                          'license' => 'perl',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'App-CSVUtils-csv_mix_formulas',
                          'provides' => {
                                          'App::CSVUtils::csv_mix_formulas' => {
                                                                                 'file' => 'lib/App/CSVUtils/csv_mix_formulas.pm',
                                                                                 'version' => '0.002'
                                                                               }
                                        },
                          'requires' => {
                                          'App::CSVUtils' => '0.032',
                                          'Data::Sah::Compiler::perl::TH::array' => '0.914',
                                          'Data::Sah::Compiler::perl::TH::bool' => '0.914',
                                          'Data::Sah::Compiler::perl::TH::str' => '0.914',
                                          'Data::Sah::Filter::perl::Path::expand_tilde_when_on_unix' => '0',
                                          'Data::Sah::Filter::perl::Path::strip_slashes_when_on_unix' => '0',
                                          'List::Util' => '1.54',
                                          'Log::ger' => '0.038',
                                          'Perinci::CmdLine::Any' => '0.154',
                                          'Perinci::CmdLine::Lite' => '1.924',
                                          'Perinci::Sub::XCompletion::filename' => '0',
                                          'Sah::Schema::filename' => '0',
                                          'Sah::Schema::true' => '0',
                                          'perl' => '5.010001',
                                          'strict' => '0',
                                          'warnings' => '0'
                                        },
                          'resources' => {
                                           'bugtracker' => 'https://rt.cpan.org/Public/Dist/Display.html?Name=App-CSVUtils-csv_mix_formulas',
                                           'homepage' => 'https://metacpan.org/release/App-CSVUtils-csv_mix_formulas',
                                           'repository' => 'git://github.com/perlancar/perl-App-CSVUtils-csv_mix_formulas.git'
                                         },
                          'version' => '0.002',
                          'x_Dist_Zilla' => {
                                              'perl' => {
                                                          'version' => '5.038002'
                                                        },
                                              'plugins' => [
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::GenPericmdScript',
                                                               'name' => 'GenPericmdScript csv-mix-formulas',
                                                               'version' => '0.425'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::InsertExecsList',
                                                               'name' => 'InsertExecsList',
                                                               'version' => '0.032'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PERLANCAR::CheckPendingRelease',
                                                               'name' => '@Author::PERLANCAR/PERLANCAR::CheckPendingRelease',
                                                               'version' => '0.001'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::GatherDir',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::GatherDir' => {
                                                                                                                   'exclude_filename' => [],
                                                                                                                   'exclude_match' => [],
                                                                                                                   'follow_symlinks' => '0',
                                                                                                                   'include_dotfiles' => '0',
                                                                                                                   'prefix' => '',
                                                                                                                   'prune_directory' => [],
                                                                                                                   'root' => '.'
                                                                                                                 }
                                                                           },
                                                               'name' => '@Author::PERLANCAR/@Filter/GatherDir',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PruneCruft',
                                                               'name' => '@Author::PERLANCAR/@Filter/PruneCruft',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::ManifestSkip',
                                                               'name' => '@Author::PERLANCAR/@Filter/ManifestSkip',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MetaYAML',
                                                               'name' => '@Author::PERLANCAR/@Filter/MetaYAML',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::License',
                                                               'name' => '@Author::PERLANCAR/@Filter/License',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PodCoverageTests',
                                                               'name' => '@Author::PERLANCAR/@Filter/PodCoverageTests',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PodSyntaxTests',
                                                               'name' => '@Author::PERLANCAR/@Filter/PodSyntaxTests',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::ExtraTests',
                                                               'name' => '@Author::PERLANCAR/@Filter/ExtraTests',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::ExecDir',
                                                               'name' => '@Author::PERLANCAR/@Filter/ExecDir',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::ShareDir',
                                                               'name' => '@Author::PERLANCAR/@Filter/ShareDir',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MakeMaker',
                                                               'config' => {
                                                                             'Dist::Zilla::Role::TestRunner' => {
                                                                                                                  'default_jobs' => '1'
                                                                                                                }
                                                                           },
                                                               'name' => '@Author::PERLANCAR/@Filter/MakeMaker',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Manifest',
                                                               'name' => '@Author::PERLANCAR/@Filter/Manifest',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::ConfirmRelease',
                                                               'name' => '@Author::PERLANCAR/@Filter/ConfirmRelease',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::ExecDir',
                                                               'name' => '@Author::PERLANCAR/ExecDir script',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PERLANCAR::BeforeBuild',
                                                               'name' => '@Author::PERLANCAR/PERLANCAR::BeforeBuild',
                                                               'version' => '0.610'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Rinci::AbstractFromMeta',
                                                               'name' => '@Author::PERLANCAR/Rinci::AbstractFromMeta',
                                                               'version' => '0.10'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PodnameFromFilename',
                                                               'name' => '@Author::PERLANCAR/PodnameFromFilename',
                                                               'version' => '0.02'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PERLANCAR::EnsurePrereqToSpec',
                                                               'config' => {
                                                                             'Dist::Zilla::Role::ModuleMetadata' => {
                                                                                                                      'Module::Metadata' => '1.000037',
                                                                                                                      'version' => '0.006'
                                                                                                                    }
                                                                           },
                                                               'name' => '@Author::PERLANCAR/PERLANCAR::EnsurePrereqToSpec',
                                                               'version' => '0.064'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PERLANCAR::MetaResources',
                                                               'name' => '@Author::PERLANCAR/PERLANCAR::MetaResources',
                                                               'version' => '0.043'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::CheckChangeLog',
                                                               'name' => '@Author::PERLANCAR/CheckChangeLog',
                                                               'version' => '0.05'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::CheckMetaResources',
                                                               'name' => '@Author::PERLANCAR/CheckMetaResources',
                                                               'version' => '0.001'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::CheckSelfDependency',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::CheckSelfDependency' => {
                                                                                                                             'finder' => [
                                                                                                                                           ':InstallModules'
                                                                                                                                         ]
                                                                                                                           },
                                                                             'Dist::Zilla::Role::ModuleMetadata' => {
                                                                                                                      'Module::Metadata' => '1.000037',
                                                                                                                      'version' => '0.006'
                                                                                                                    }
                                                                           },
                                                               'name' => '@Author::PERLANCAR/CheckSelfDependency',
                                                               'version' => '0.011'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Git::Contributors',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Git::Contributors' => {
                                                                                                                           'git_version' => '2.25.1',
                                                                                                                           'include_authors' => '0',
                                                                                                                           'include_releaser' => '1',
                                                                                                                           'order_by' => 'name',
                                                                                                                           'paths' => []
                                                                                                                         }
                                                                           },
                                                               'name' => '@Author::PERLANCAR/Git::Contributors',
                                                               'version' => '0.036'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::CopyrightYearFromGit',
                                                               'name' => '@Author::PERLANCAR/CopyrightYearFromGit',
                                                               'version' => '0.009'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::IfBuilt',
                                                               'name' => '@Author::PERLANCAR/IfBuilt',
                                                               'version' => '0.02'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MetaJSON',
                                                               'name' => '@Author::PERLANCAR/MetaJSON',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MetaConfig',
                                                               'name' => '@Author::PERLANCAR/MetaConfig',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MetaProvides::Package',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::MetaProvides::Package' => {
                                                                                                                               'finder_objects' => [
                                                                                                                                                     {
                                                                                                                                                       'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                                                                                                                       'name' => '@Author::PERLANCAR/MetaProvides::Package/AUTOVIV/:InstallModulesPM',
                                                                                                                                                       'version' => '6.031'
                                                                                                                                                     }
                                                                                                                                                   ],
                                                                                                                               'include_underscores' => '0'
                                                                                                                             },
                                                                             'Dist::Zilla::Role::MetaProvider::Provider' => {
                                                                                                                              '$Dist::Zilla::Role::MetaProvider::Provider::VERSION' => '2.002004',
                                                                                                                              'inherit_missing' => '1',
                                                                                                                              'inherit_version' => '1',
                                                                                                                              'meta_noindex' => '1'
                                                                                                                            },
                                                                             'Dist::Zilla::Role::ModuleMetadata' => {
                                                                                                                      'Module::Metadata' => '1.000037',
                                                                                                                      'version' => '0.006'
                                                                                                                    }
                                                                           },
                                                               'name' => '@Author::PERLANCAR/MetaProvides::Package',
                                                               'version' => '2.004003'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PERLANCAR::Authority',
                                                               'name' => '@Author::PERLANCAR/PERLANCAR::Authority',
                                                               'version' => '0.001'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::OurDate',
                                                               'name' => '@Author::PERLANCAR/OurDate',
                                                               'version' => '0.040'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::OurDist',
                                                               'name' => '@Author::PERLANCAR/OurDist',
                                                               'version' => '0.02'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::OurPkgVersion',
                                                               'name' => '@Author::PERLANCAR/OurPkgVersion',
                                                               'version' => '0.21'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PodWeaver',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::PodWeaver' => {
                                                                                                                   'finder' => [
                                                                                                                                 ':InstallModules',
                                                                                                                                 ':PerlExecFiles'
                                                                                                                               ],
                                                                                                                   'plugins' => [
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Plugin::EnsurePod5',
                                                                                                                                    'name' => '@CorePrep/EnsurePod5',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Plugin::H1Nester',
                                                                                                                                    'name' => '@CorePrep/H1Nester',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Name',
                                                                                                                                    'name' => '@Author::PERLANCAR/Name',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Version',
                                                                                                                                    'name' => '@Author::PERLANCAR/Version',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Region',
                                                                                                                                    'name' => '@Author::PERLANCAR/prelude',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Generic',
                                                                                                                                    'name' => 'SYNOPSIS',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Generic',
                                                                                                                                    'name' => 'DESCRIPTION',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Generic',
                                                                                                                                    'name' => 'OVERVIEW',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Collect',
                                                                                                                                    'name' => 'ATTRIBUTES',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Collect',
                                                                                                                                    'name' => 'METHODS',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Collect',
                                                                                                                                    'name' => 'FUNCTIONS',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Leftovers',
                                                                                                                                    'name' => '@Author::PERLANCAR/Leftovers',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Region',
                                                                                                                                    'name' => '@Author::PERLANCAR/postlude',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Completion::GetoptLongComplete',
                                                                                                                                    'name' => '@Author::PERLANCAR/Completion::GetoptLongComplete',
                                                                                                                                    'version' => '0.08'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Completion::GetoptLongSubcommand',
                                                                                                                                    'name' => '@Author::PERLANCAR/Completion::GetoptLongSubcommand',
                                                                                                                                    'version' => '0.04'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Completion::GetoptLongMore',
                                                                                                                                    'name' => '@Author::PERLANCAR/Completion::GetoptLongMore',
                                                                                                                                    'version' => '0.001'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Homepage::DefaultCPAN',
                                                                                                                                    'name' => '@Author::PERLANCAR/Homepage::DefaultCPAN',
                                                                                                                                    'version' => '0.05'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Source::DefaultGitHub',
                                                                                                                                    'name' => '@Author::PERLANCAR/Source::DefaultGitHub',
                                                                                                                                    'version' => '0.07'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Bugs::DefaultRT',
                                                                                                                                    'name' => '@Author::PERLANCAR/Bugs::DefaultRT',
                                                                                                                                    'version' => '0.06'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Authors',
                                                                                                                                    'name' => '@Author::PERLANCAR/Authors',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Contributors',
                                                                                                                                    'name' => '@Author::PERLANCAR/Contributors',
                                                                                                                                    'version' => '0.009'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::PERLANCAR::Contributing',
                                                                                                                                    'name' => '@Author::PERLANCAR/PERLANCAR/Contributing',
                                                                                                                                    'version' => '0.293'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Legal',
                                                                                                                                    'name' => '@Author::PERLANCAR/Legal',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Plugin::Rinci',
                                                                                                                                    'name' => '@Author::PERLANCAR/Rinci',
                                                                                                                                    'version' => '0.786'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Plugin::AppendPrepend',
                                                                                                                                    'name' => '@Author::PERLANCAR/AppendPrepend',
                                                                                                                                    'version' => '0.021'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Plugin::EnsureUniqueSections',
                                                                                                                                    'name' => '@Author::PERLANCAR/EnsureUniqueSections',
                                                                                                                                    'version' => '0.163250'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Plugin::SingleEncoding',
                                                                                                                                    'name' => '@Author::PERLANCAR/SingleEncoding',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Plugin::PERLANCAR::SortSections',
                                                                                                                                    'name' => '@Author::PERLANCAR/PERLANCAR::SortSections',
                                                                                                                                    'version' => '0.082'
                                                                                                                                  }
                                                                                                                                ]
                                                                                                                 }
                                                                           },
                                                               'name' => '@Author::PERLANCAR/PodWeaver',
                                                               'version' => '4.010'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PruneFiles',
                                                               'name' => '@Author::PERLANCAR/PruneFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Pod2Readme',
                                                               'name' => '@Author::PERLANCAR/Pod2Readme',
                                                               'version' => '0.004'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Rinci::AddPrereqs',
                                                               'name' => '@Author::PERLANCAR/Rinci::AddPrereqs',
                                                               'version' => '0.145'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Rinci::AddToDb',
                                                               'name' => '@Author::PERLANCAR/Rinci::AddToDb',
                                                               'version' => '0.020'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Rinci::EmbedValidator',
                                                               'name' => '@Author::PERLANCAR/Rinci::EmbedValidator',
                                                               'version' => '0.251'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::SetScriptShebang',
                                                               'name' => '@Author::PERLANCAR/SetScriptShebang',
                                                               'version' => '0.01'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::Compile',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Test::Compile' => {
                                                                                                                       'bail_out_on_fail' => '0',
                                                                                                                       'fail_on_warning' => 'author',
                                                                                                                       'fake_home' => '0',
                                                                                                                       'filename' => 't/00-compile.t',
                                                                                                                       'module_finder' => [
                                                                                                                                            ':InstallModules'
                                                                                                                                          ],
                                                                                                                       'needs_display' => '0',
                                                                                                                       'phase' => 'test',
                                                                                                                       'script_finder' => [
                                                                                                                                            ':PerlExecFiles'
                                                                                                                                          ],
                                                                                                                       'skips' => [],
                                                                                                                       'switch' => []
                                                                                                                     }
                                                                           },
                                                               'name' => '@Author::PERLANCAR/Test::Compile',
                                                               'version' => '2.058'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::Perl::Critic::Subset',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Test::Perl::Critic::Subset' => {
                                                                                                                                    'finder' => [
                                                                                                                                                  ':ExecFiles',
                                                                                                                                                  ':InstallModules',
                                                                                                                                                  ':TestFiles'
                                                                                                                                                ]
                                                                                                                                  }
                                                                           },
                                                               'name' => '@Author::PERLANCAR/Test::Perl::Critic::Subset',
                                                               'version' => '3.001.006'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::Rinci',
                                                               'name' => '@Author::PERLANCAR/Test::Rinci',
                                                               'version' => '0.040'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::StaticInstall',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::StaticInstall' => {
                                                                                                                       'dry_run' => '0',
                                                                                                                       'mode' => 'on'
                                                                                                                     }
                                                                           },
                                                               'name' => '@Author::PERLANCAR/StaticInstall',
                                                               'version' => '0.012'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::EnsureSQLSchemaVersionedTest',
                                                               'name' => '@Author::PERLANCAR/EnsureSQLSchemaVersionedTest',
                                                               'version' => '0.03'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Acme::CPANModules::Blacklist',
                                                               'name' => '@Author::PERLANCAR/Acme::CPANModules::Blacklist',
                                                               'version' => '0.002'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Prereqs::EnsureVersion',
                                                               'name' => '@Author::PERLANCAR/Prereqs::EnsureVersion',
                                                               'version' => '0.050'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Prereqs::CheckCircular',
                                                               'name' => '@Author::PERLANCAR/Prereqs::CheckCircular',
                                                               'version' => '0.007'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::UploadToCPAN::WWWPAUSESimple',
                                                               'name' => '@Author::PERLANCAR/UploadToCPAN::WWWPAUSESimple',
                                                               'version' => '0.04'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Prereqs',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Prereqs' => {
                                                                                                                 'phase' => 'test',
                                                                                                                 'type' => 'requires'
                                                                                                               }
                                                                           },
                                                               'name' => 'TestRequires',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Prereqs',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Prereqs' => {
                                                                                                                 'phase' => 'runtime',
                                                                                                                 'type' => 'requires'
                                                                                                               }
                                                                           },
                                                               'name' => 'Prereqs',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Prereqs',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Prereqs' => {
                                                                                                                 'phase' => 'develop',
                                                                                                                 'type' => 'x_spec'
                                                                                                               }
                                                                           },
                                                               'name' => 'DevelopX_spec',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':InstallModules',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':IncModules',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':TestFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':ExtraTestFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':ExecFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':PerlExecFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':ShareFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':MainModule',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':AllFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':NoFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => '@Author::PERLANCAR/MetaProvides::Package/AUTOVIV/:InstallModulesPM',
                                                               'version' => '6.031'
                                                             }
                                                           ],
                                              'zilla' => {
                                                           'class' => 'Dist::Zilla::Dist::Builder',
                                                           'config' => {
                                                                         'is_trial' => '0'
                                                                       },
                                                           'version' => '6.031'
                                                         }
                                            },
                          'x_authority' => 'cpan:PERLANCAR',
                          'x_generated_by_perl' => 'v5.38.2',
                          'x_serialization_backend' => 'YAML::Tiny version 1.74',
                          'x_spdx_expression' => 'Artistic-1.0-Perl OR GPL-1.0-or-later',
                          'x_static_install' => '1'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/App/CSVUtils/csv_mix_formulas.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'App::CSVUtils::csv_mix_formulas'
                         }
                       ],
          'no_pax_headers' => 1,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/App-CSVUtils-csv_mix_formulas.tar.gz',
          'prereq' => [
                        {
                          'is_prereq' => 1,
                          'requires' => 'App::CSVUtils',
                          'type' => 'runtime_requires',
                          'version' => '0.032'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Data::Sah::Compiler::perl::TH::array',
                          'type' => 'runtime_requires',
                          'version' => '0.914'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Data::Sah::Compiler::perl::TH::bool',
                          'type' => 'runtime_requires',
                          'version' => '0.914'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Data::Sah::Compiler::perl::TH::str',
                          'type' => 'runtime_requires',
                          'version' => '0.914'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Data::Sah::Filter::perl::Path::expand_tilde_when_on_unix',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Data::Sah::Filter::perl::Path::strip_slashes_when_on_unix',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'File::Spec',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'IO::Handle',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'IPC::Open3',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'List::Util',
                          'type' => 'runtime_requires',
                          'version' => '1.54'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Log::ger',
                          'type' => 'runtime_requires',
                          'version' => '0.038'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Perinci::CmdLine::Any',
                          'type' => 'runtime_requires',
                          'version' => '0.154'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Perinci::CmdLine::Lite',
                          'type' => 'runtime_requires',
                          'version' => '1.924'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Perinci::Sub::XCompletion::filename',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Sah::Schema::filename',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Sah::Schema::true',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => '5.010001'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'strict',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'warnings',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        }
                      ],
          'released' => 1709583469,
          'size_packed' => 26779,
          'size_unpacked' => 114550,
          'test_files' => [
                            't/00-compile.t',
                            't/author-critic.t',
                            't/author-pod-coverage.t',
                            't/author-pod-syntax.t',
                            't/release-rinci.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'requires' => {
                                                       'ExtUtils::MakeMaker' => '0',
                                                       'perl' => '5.010001',
                                                       'strict' => '0',
                                                       'warnings' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'requires' => {
                                                     'App::CSVUtils' => '0',
                                                     'List::Util' => '0',
                                                     'Log::ger' => '0',
                                                     'perl' => '5.010001',
                                                     'strict' => '0',
                                                     'warnings' => '0'
                                                   }
                                   },
                      'test' => {
                                  'recommends' => {
                                                    'Pod::Coverage::TrustPod' => '0',
                                                    'Test::More' => '0',
                                                    'Test::Perl::Critic' => '0',
                                                    'Test::Pod' => '1.41',
                                                    'Test::Pod::Coverage' => '1.08',
                                                    'strict' => '0',
                                                    'warnings' => '0'
                                                  },
                                  'requires' => {
                                                  'File::Spec' => '0',
                                                  'IO::Handle' => '0',
                                                  'IPC::Open3' => '0',
                                                  'Test::More' => '0',
                                                  'perl' => '5.006',
                                                  'strict' => '0',
                                                  'warnings' => '0'
                                                },
                                  'suggests' => {
                                                  'Test::Rinci' => '0.01',
                                                  'blib' => '1.01'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'App-CSVUtils-csv_mix_formulas'
        };
1;
