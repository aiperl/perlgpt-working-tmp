$distribution_kwalitee = $VAR1 = {
          'abstracts_in_pod' => {
                                  'CGI' => 'Handle Common Gateway Interface requests and responses',
                                  'CGI::Carp' => 'CGI routines for writing to the HTTPD (or other) error log',
                                  'CGI::Cookie' => 'Interface to HTTP Cookies',
                                  'CGI::HTML::Functions' => 'Documentation for CGI.pm Legacy HTML Functionality',
                                  'CGI::Pretty' => 'module to produce nicely formatted HTML code',
                                  'CGI::Push' => 'Simple Interface to Server Push',
                                  'CGI::Util' => 'Internal utilities used by CGI module'
                                },
          'author' => '',
          'dir_lib' => 'lib',
          'dir_t' => 't',
          'dirs' => 10,
          'dirs_array' => [
                            'examples',
                            'lib/CGI/File',
                            'lib/CGI/HTML',
                            'lib/CGI',
                            'lib',
                            't/APR',
                            't/Apache2',
                            't/ModPerl',
                            't/headers',
                            't'
                          ],
          'dist' => 'CGI',
          'dynamic_config' => 1,
          'error' => {
                       'extracts_nicely' => 'expected CGI but got CGI-4.62',
                       'no_pax_headers' => 'CGI-4.62/PaxHeader/LICENSE,CGI-4.62/PaxHeader/LICENSE,CGI-4.62/t/PaxHeader/upload_post_quoted_unquoted.txt,CGI-4.62/t/PaxHeader/upload_post_quoted_unquoted.txt,CGI-4.62/t/PaxHeader/rt_31107.txt,CGI-4.62/t/PaxHeader/rt_31107.txt,CGI-4.62/t/PaxHeader/rt_75628.txt,CGI-4.62/t/PaxHeader/rt_75628.txt',
                       'use_strict' => 'CGI, CGI::Carp, CGI::File::Temp, CGI::Push',
                       'use_warnings' => 'CGI, CGI::Carp, CGI::File::Temp, CGI::Push, CGI::Util'
                     },
          'extension' => 'tar.gz',
          'external_license_file' => 'LICENSE',
          'extractable' => 1,
          'extracts_nicely' => 1,
          'file_changelog' => 'Changes',
          'file_license' => 'LICENSE',
          'file_makefile_pl' => 'Makefile.PL',
          'file_manifest' => 'MANIFEST',
          'file_meta_json' => 'META.json',
          'file_meta_yml' => 'META.yml',
          'file_readme' => 'README.md',
          'files' => 25,
          'files_array' => [
                             'Changes',
                             'LICENSE',
                             'MANIFEST',
                             'META.json',
                             'META.yml',
                             'Makefile.PL',
                             'README.md',
                             'examples/clickable_image.cgi',
                             'examples/cookie.cgi',
                             'examples/crash.cgi',
                             'examples/file_upload.cgi',
                             'examples/mojo_proxy.pl',
                             'examples/wikipedia_example.cgi',
                             'examples/wilogo.gif',
                             'lib/CGI/Carp.pm',
                             'lib/CGI/Cookie.pm',
                             'lib/CGI/File/Temp.pm',
                             'lib/CGI/HTML/Functions.pm',
                             'lib/CGI/HTML/Functions.pod',
                             'lib/CGI/Pretty.pm',
                             'lib/CGI/Push.pm',
                             'lib/CGI/Util.pm',
                             'lib/CGI.pm',
                             'lib/CGI.pod',
                             'lib/Fh.pm'
                           ],
          'files_hash' => {
                            'Changes' => {
                                           'mtime' => 1709300583,
                                           'size' => 96906
                                         },
                            'LICENSE' => {
                                           'mtime' => 1642508284,
                                           'size' => 8644
                                         },
                            'MANIFEST' => {
                                            'mtime' => 1709300768,
                                            'size' => 1778
                                          },
                            'META.json' => {
                                             'mtime' => 1709300768,
                                             'size' => 2071
                                           },
                            'META.yml' => {
                                            'mtime' => 1709300768,
                                            'size' => 1120
                                          },
                            'Makefile.PL' => {
                                               'mtime' => 1709300583,
                                               'requires' => {
                                                               'ExtUtils::MakeMaker' => '0'
                                                             },
                                               'size' => 3562
                                             },
                            'README.md' => {
                                             'mtime' => 1696230788,
                                             'size' => 70155
                                           },
                            'examples/clickable_image.cgi' => {
                                                                'mtime' => 1642508284,
                                                                'size' => 1438
                                                              },
                            'examples/cookie.cgi' => {
                                                       'mtime' => 1642508284,
                                                       'size' => 2806
                                                     },
                            'examples/crash.cgi' => {
                                                      'mtime' => 1642508284,
                                                      'size' => 158
                                                    },
                            'examples/file_upload.cgi' => {
                                                            'mtime' => 1642508284,
                                                            'size' => 2444
                                                          },
                            'examples/mojo_proxy.pl' => {
                                                          'mtime' => 1642508284,
                                                          'size' => 805
                                                        },
                            'examples/wikipedia_example.cgi' => {
                                                                  'mtime' => 1642508284,
                                                                  'size' => 922
                                                                },
                            'examples/wilogo.gif' => {
                                                       'mtime' => 1642508284,
                                                       'size' => 458
                                                     },
                            'lib/CGI.pm' => {
                                              'module' => 'CGI',
                                              'mtime' => 1709300583,
                                              'recommends' => {
                                                                'APR::Pool' => '0',
                                                                'Apache' => '0',
                                                                'Apache2::RequestIO' => '0',
                                                                'Apache2::RequestRec' => '0',
                                                                'Apache2::RequestUtil' => '0',
                                                                'Apache2::Response' => '0',
                                                                'CGI::Cookie' => '0',
                                                                'CGI::File::Temp' => '0',
                                                                'Config' => '0',
                                                                'HTML::Entities' => '0',
                                                                'Text::ParseWords' => '0'
                                                              },
                                              'requires' => {
                                                              'CGI::Util' => '0',
                                                              'Carp' => '0',
                                                              'URI' => '0',
                                                              'perl' => '5.008001'
                                                            },
                                              'size' => 125802,
                                              'suggests' => {
                                                              'Encode' => '0'
                                                            }
                                            },
                            'lib/CGI.pod' => {
                                               'license' => 'Artistic_2_0',
                                               'mtime' => 1683028841,
                                               'size' => 68886
                                             },
                            'lib/CGI/Carp.pm' => {
                                                   'module' => 'CGI::Carp',
                                                   'mtime' => 1709300583,
                                                   'recommends' => {
                                                                     'APR::Pool' => '0',
                                                                     'Apache2::RequestIO' => '0',
                                                                     'Apache2::RequestRec' => '0',
                                                                     'Apache2::RequestUtil' => '0',
                                                                     'Apache2::Response' => '0',
                                                                     'ModPerl::Util' => '0'
                                                                   },
                                                   'requires' => {
                                                                   'Carp' => '0',
                                                                   'Exporter' => '0',
                                                                   'File::Spec' => '0',
                                                                   'perl' => '5.000'
                                                                 },
                                                   'size' => 19027
                                                 },
                            'lib/CGI/Cookie.pm' => {
                                                     'module' => 'CGI::Cookie',
                                                     'mtime' => 1704726774,
                                                     'noes' => {
                                                                 'warnings' => '0'
                                                               },
                                                     'recommends' => {
                                                                       'APR::Table' => '0',
                                                                       'Apache' => '0',
                                                                       'Apache2::RequestUtil' => '0',
                                                                       'CGI' => '0'
                                                                     },
                                                     'requires' => {
                                                                     'CGI::Util' => '0',
                                                                     'overload' => '0',
                                                                     'strict' => '0',
                                                                     'warnings' => '0'
                                                                   },
                                                     'size' => 18475
                                                   },
                            'lib/CGI/File/Temp.pm' => {
                                                        'module' => 'CGI::File::Temp',
                                                        'mtime' => 1709300583,
                                                        'requires' => {
                                                                        'Fh' => '0',
                                                                        'File::Temp' => '0',
                                                                        'overload' => '0',
                                                                        'parent' => '0'
                                                                      },
                                                        'size' => 942
                                                      },
                            'lib/CGI/HTML/Functions.pm' => {
                                                             'module' => 'CGI::HTML::Functions',
                                                             'mtime' => 1642508284,
                                                             'requires' => {
                                                                             'strict' => '0',
                                                                             'warnings' => '0'
                                                                           },
                                                             'size' => 125
                                                           },
                            'lib/CGI/HTML/Functions.pod' => {
                                                              'mtime' => 1642508284,
                                                              'size' => 60528
                                                            },
                            'lib/CGI/Pretty.pm' => {
                                                     'module' => 'CGI::Pretty',
                                                     'mtime' => 1709300583,
                                                     'noes' => {
                                                                 'strict' => '0'
                                                               },
                                                     'requires' => {
                                                                     'CGI' => '0',
                                                                     'strict' => '0',
                                                                     'warnings' => '0'
                                                                   },
                                                     'size' => 1943
                                                   },
                            'lib/CGI/Push.pm' => {
                                                   'module' => 'CGI::Push',
                                                   'mtime' => 1709300583,
                                                   'requires' => {
                                                                   'CGI' => '0',
                                                                   'CGI::Util' => '0'
                                                                 },
                                                   'size' => 10109
                                                 },
                            'lib/CGI/Util.pm' => {
                                                   'module' => 'CGI::Util',
                                                   'mtime' => 1696230767,
                                                   'requires' => {
                                                                   'Exporter' => '0',
                                                                   'parent' => '0',
                                                                   'perl' => '5.008001',
                                                                   'strict' => '0'
                                                                 },
                                                   'size' => 11481
                                                 },
                            'lib/Fh.pm' => {
                                             'module' => 'Fh',
                                             'mtime' => 1709300583,
                                             'requires' => {
                                                             'strict' => '0',
                                                             'warnings' => '0'
                                                           },
                                             'size' => 166
                                           },
                            't/APR/Pool.pm' => {
                                                 'mtime' => 1642508284,
                                                 'no_index' => 1,
                                                 'size' => 23
                                               },
                            't/Apache.pm' => {
                                               'mtime' => 1642508284,
                                               'no_index' => 1,
                                               'size' => 143
                                             },
                            't/Apache2/RequestIO.pm' => {
                                                          'mtime' => 1642508284,
                                                          'no_index' => 1,
                                                          'size' => 32
                                                        },
                            't/Apache2/RequestRec.pm' => {
                                                           'mtime' => 1642508284,
                                                           'no_index' => 1,
                                                           'size' => 33
                                                         },
                            't/Apache2/RequestUtil.pm' => {
                                                            'mtime' => 1642508284,
                                                            'no_index' => 1,
                                                            'size' => 160
                                                          },
                            't/Apache2/Response.pm' => {
                                                         'mtime' => 1642508284,
                                                         'no_index' => 1,
                                                         'size' => 31
                                                       },
                            't/Dump.t' => {
                                            'mtime' => 1642508284,
                                            'no_index' => 1,
                                            'requires' => {
                                                            'CGI' => '0',
                                                            'Test::More' => '0'
                                                          },
                                            'size' => 221
                                          },
                            't/ModPerl/Util.pm' => {
                                                     'mtime' => 1642508284,
                                                     'no_index' => 1,
                                                     'size' => 41
                                                   },
                            't/append_query.t' => {
                                                    'mtime' => 1672731669,
                                                    'no_index' => 1,
                                                    'requires' => {
                                                                    'CGI' => '0',
                                                                    'Config' => '0',
                                                                    'Test::More' => '0',
                                                                    'strict' => '0',
                                                                    'warnings' => '0'
                                                                  },
                                                    'size' => 2325
                                                  },
                            't/arbitrary_handles.t' => {
                                                         'mtime' => 1642508284,
                                                         'no_index' => 1,
                                                         'requires' => {
                                                                         'CGI' => '0',
                                                                         'IO::File' => '0',
                                                                         'Test::More' => '0',
                                                                         'strict' => '0',
                                                                         'warnings' => '0'
                                                                       },
                                                         'size' => 620
                                                       },
                            't/autoescape.t' => {
                                                  'mtime' => 1642508284,
                                                  'no_index' => 1,
                                                  'requires' => {
                                                                  'CGI' => '0',
                                                                  'Test::More' => '0',
                                                                  'strict' => '0',
                                                                  'warnings' => '0'
                                                                },
                                                  'size' => 6249
                                                },
                            't/can.t' => {
                                           'mtime' => 1642508284,
                                           'no_index' => 1,
                                           'requires' => {
                                                           'Test::More' => '0'
                                                         },
                                           'size' => 112
                                         },
                            't/carp.t' => {
                                            'mtime' => 1642508284,
                                            'no_index' => 1,
                                            'requires' => {
                                                            'Apache' => '0',
                                                            'CGI::Carp' => '0',
                                                            'Cwd' => '0',
                                                            'File::Temp' => '0',
                                                            'FileHandle' => '0',
                                                            'FindBin' => '0',
                                                            'IO::Handle' => '0',
                                                            'Test::More' => '0',
                                                            'lib' => '0',
                                                            'overload' => '0',
                                                            'strict' => '0'
                                                          },
                                            'size' => 13522
                                          },
                            't/cgi.t' => {
                                           'mtime' => 1672731669,
                                           'no_index' => 1,
                                           'requires' => {
                                                           'CGI' => '0',
                                                           'Test::More' => '0',
                                                           'strict' => '0',
                                                           'warnings' => '0'
                                                         },
                                           'size' => 1661
                                         },
                            't/charset.t' => {
                                               'mtime' => 1642508284,
                                               'no_index' => 1,
                                               'requires' => {
                                                               'CGI' => '0',
                                                               'Test::More' => '0',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 722
                                             },
                            't/checkbox_group.t' => {
                                                      'mtime' => 1642508284,
                                                      'no_index' => 1,
                                                      'requires' => {
                                                                      'CGI' => '0',
                                                                      'Test::More' => '0'
                                                                    },
                                                      'size' => 992
                                                    },
                            't/command_line.t' => {
                                                    'mtime' => 1672731669,
                                                    'no_index' => 1,
                                                    'requires' => {
                                                                    'Config' => '0',
                                                                    'File::Temp' => '0',
                                                                    'Test::More' => '0.88',
                                                                    'strict' => '0',
                                                                    'warnings' => '0'
                                                                  },
                                                    'size' => 3128
                                                  },
                            't/compiles_pod.t' => {
                                                    'mtime' => 1642508284,
                                                    'no_index' => 1,
                                                    'requires' => {
                                                                    'File::Find' => '0',
                                                                    'Test::More' => '0',
                                                                    'strict' => '0',
                                                                    'warnings' => '0'
                                                                  },
                                                    'size' => 1192,
                                                    'suggests' => {
                                                                    'Test::Pod' => '0',
                                                                    'Test::Pod::Coverage' => '0'
                                                                  }
                                                  },
                            't/cookie.t' => {
                                              'mtime' => 1704726774,
                                              'no_index' => 1,
                                              'requires' => {
                                                              'CGI::Cookie' => '0',
                                                              'CGI::Util' => '0',
                                                              'POSIX' => '0',
                                                              'Test::More' => '0',
                                                              'strict' => '0'
                                                            },
                                              'size' => 18748
                                            },
                            't/delete.t' => {
                                              'mtime' => 1642508284,
                                              'no_index' => 1,
                                              'requires' => {
                                                              'CGI' => '0',
                                                              'Config' => '0',
                                                              'Test::More' => '0.88',
                                                              'strict' => '0',
                                                              'warnings' => '0'
                                                            },
                                              'size' => 2166
                                            },
                            't/end_form.t' => {
                                                'mtime' => 1642508284,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'Test::More' => '0',
                                                                'strict' => '0',
                                                                'warnings' => '0'
                                                              },
                                                'size' => 136
                                              },
                            't/form.t' => {
                                            'mtime' => 1642508284,
                                            'no_index' => 1,
                                            'requires' => {
                                                            'CGI' => '0',
                                                            'Test::More' => '0'
                                                          },
                                            'size' => 9042
                                          },
                            't/function.t' => {
                                                'mtime' => 1642508284,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'CGI' => '0',
                                                                'Config' => '0'
                                                              },
                                                'size' => 5046
                                              },
                            't/gh-155.t' => {
                                              'mtime' => 1642508284,
                                              'no_index' => 1,
                                              'requires' => {
                                                              'CGI' => '0',
                                                              'Test::More' => '0.88',
                                                              'strict' => '0',
                                                              'warnings' => '0'
                                                            },
                                              'size' => 400
                                            },
                            't/headers.t' => {
                                               'mtime' => 1642508284,
                                               'no_index' => 1,
                                               'requires' => {
                                                               'CGI' => '0',
                                                               'Test::More' => '0',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 2018
                                             },
                            't/headers/attachment.t' => {
                                                          'mtime' => 1642508284,
                                                          'no_index' => 1,
                                                          'requires' => {
                                                                          'CGI' => '0',
                                                                          'Test::More' => '0.88',
                                                                          'strict' => '0'
                                                                        },
                                                          'size' => 608
                                                        },
                            't/headers/charset.t' => {
                                                       'mtime' => 1642508284,
                                                       'no_index' => 1,
                                                       'requires' => {
                                                                       'CGI' => '0',
                                                                       'Test::More' => '0.88',
                                                                       'strict' => '0'
                                                                     },
                                                       'size' => 449
                                                     },
                            't/headers/cookie.t' => {
                                                      'mtime' => 1642508284,
                                                      'no_index' => 1,
                                                      'requires' => {
                                                                      'CGI' => '0',
                                                                      'Test::More' => '0.88',
                                                                      'strict' => '0'
                                                                    },
                                                      'size' => 976
                                                    },
                            't/headers/default.t' => {
                                                       'mtime' => 1642508284,
                                                       'no_index' => 1,
                                                       'requires' => {
                                                                       'CGI' => '0',
                                                                       'Test::More' => '0.88',
                                                                       'strict' => '0'
                                                                     },
                                                       'size' => 246
                                                     },
                            't/headers/nph.t' => {
                                                   'mtime' => 1642508284,
                                                   'no_index' => 1,
                                                   'requires' => {
                                                                   'CGI' => '0',
                                                                   'Test::More' => '0.88',
                                                                   'strict' => '0'
                                                                 },
                                                   'size' => 607
                                                 },
                            't/headers/p3p.t' => {
                                                   'mtime' => 1642508284,
                                                   'no_index' => 1,
                                                   'requires' => {
                                                                   'CGI' => '0',
                                                                   'Test::More' => '0.88',
                                                                   'strict' => '0'
                                                                 },
                                                   'size' => 927
                                                 },
                            't/headers/target.t' => {
                                                      'mtime' => 1642508284,
                                                      'no_index' => 1,
                                                      'requires' => {
                                                                      'CGI' => '0',
                                                                      'Test::More' => '0.88',
                                                                      'strict' => '0'
                                                                    },
                                                      'size' => 555
                                                    },
                            't/headers/type.t' => {
                                                    'mtime' => 1642508284,
                                                    'no_index' => 1,
                                                    'requires' => {
                                                                    'CGI' => '0',
                                                                    'Test::More' => '0.88',
                                                                    'strict' => '0'
                                                                  },
                                                    'size' => 2513
                                                  },
                            't/hidden.t' => {
                                              'mtime' => 1642508284,
                                              'no_index' => 1,
                                              'requires' => {
                                                              'CGI' => '0',
                                                              'Test::More' => '0'
                                                            },
                                              'size' => 2191
                                            },
                            't/html.t' => {
                                            'mtime' => 1642508284,
                                            'no_index' => 1,
                                            'recommends' => {
                                                              'utf8' => '0'
                                                            },
                                            'requires' => {
                                                            'CGI' => '0',
                                                            'Test::More' => '0'
                                                          },
                                            'size' => 6448
                                          },
                            't/html_functions.t' => {
                                                      'mtime' => 1642508284,
                                                      'no_index' => 1,
                                                      'requires' => {
                                                                      'CGI' => '0',
                                                                      'Test::More' => '0',
                                                                      'strict' => '0',
                                                                      'warnings' => '0'
                                                                    },
                                                      'size' => 1059
                                                    },
                            't/http.t' => {
                                            'mtime' => 1642508284,
                                            'no_index' => 1,
                                            'requires' => {
                                                            'CGI' => '0',
                                                            'Test::More' => '0',
                                                            'lib' => '0'
                                                          },
                                            'size' => 1470
                                          },
                            't/init.t' => {
                                            'mtime' => 1642508284,
                                            'no_index' => 1,
                                            'requires' => {
                                                            'CGI' => '0',
                                                            'Test::More' => '0',
                                                            'strict' => '0'
                                                          },
                                            'size' => 227
                                          },
                            't/init_test.txt' => {
                                                   'mtime' => 1642508284,
                                                   'no_index' => 1,
                                                   'size' => 12
                                                 },
                            't/multipart_globals.t' => {
                                                         'mtime' => 1642508284,
                                                         'no_index' => 1,
                                                         'requires' => {
                                                                         'CGI' => '0',
                                                                         'Test::More' => '0'
                                                                       },
                                                         'size' => 764
                                                       },
                            't/multipart_init.t' => {
                                                      'mtime' => 1642508284,
                                                      'no_index' => 1,
                                                      'requires' => {
                                                                      'CGI' => '0',
                                                                      'Test::More' => '0'
                                                                    },
                                                      'size' => 935
                                                    },
                            't/multipart_start.t' => {
                                                       'mtime' => 1642508284,
                                                       'no_index' => 1,
                                                       'requires' => {
                                                                       'CGI' => '0',
                                                                       'Test::More' => '0',
                                                                       'strict' => '0',
                                                                       'warnings' => '0'
                                                                     },
                                                       'size' => 719
                                                     },
                            't/no_tabindex.t' => {
                                                   'mtime' => 1642508284,
                                                   'no_index' => 1,
                                                   'requires' => {
                                                                   'CGI' => '0',
                                                                   'Test::More' => '0'
                                                                 },
                                                   'size' => 4178
                                                 },
                            't/param_fetch.t' => {
                                                   'mtime' => 1642508284,
                                                   'no_index' => 1,
                                                   'requires' => {
                                                                   'CGI' => '0',
                                                                   'Test::More' => '0'
                                                                 },
                                                   'size' => 838
                                                 },
                            't/param_list_context.t' => {
                                                          'mtime' => 1672731669,
                                                          'no_index' => 1,
                                                          'requires' => {
                                                                          'CGI' => '0',
                                                                          'Test::More' => '0',
                                                                          'Test::Warn' => '0',
                                                                          'strict' => '0',
                                                                          'warnings' => '0'
                                                                        },
                                                          'size' => 1177
                                                        },
                            't/popup_menu.t' => {
                                                  'mtime' => 1642508284,
                                                  'no_index' => 1,
                                                  'requires' => {
                                                                  'CGI' => '0',
                                                                  'Test::More' => '0'
                                                                },
                                                  'size' => 803
                                                },
                            't/postdata.t' => {
                                                'mtime' => 1642508284,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'CGI' => '0',
                                                                'Test::More' => '0',
                                                                'strict' => '0'
                                                              },
                                                'size' => 4085
                                              },
                            't/pretty.t' => {
                                              'mtime' => 1642508284,
                                              'no_index' => 1,
                                              'requires' => {
                                                              'CGI::Pretty' => '0',
                                                              'Test::More' => '0',
                                                              'strict' => '0'
                                                            },
                                              'size' => 708
                                            },
                            't/push.t' => {
                                            'mtime' => 1642508284,
                                            'no_index' => 1,
                                            'requires' => {
                                                            'Test::More' => '0'
                                                          },
                                            'size' => 1778
                                          },
                            't/query_string.t' => {
                                                    'mtime' => 1642508284,
                                                    'no_index' => 1,
                                                    'requires' => {
                                                                    'CGI' => '0',
                                                                    'Test::More' => '0'
                                                                  },
                                                    'size' => 308
                                                  },
                            't/redirect_query_string.t' => {
                                                             'mtime' => 1642508284,
                                                             'no_index' => 1,
                                                             'noes' => {
                                                                         'warnings' => '0'
                                                                       },
                                                             'requires' => {
                                                                             'CGI' => '0',
                                                                             'Test::More' => '0',
                                                                             'strict' => '0',
                                                                             'warnings' => '0'
                                                                           },
                                                             'size' => 1759
                                                           },
                            't/request.t' => {
                                               'mtime' => 1672731669,
                                               'no_index' => 1,
                                               'requires' => {
                                                               'CGI' => '0',
                                                               'Config' => '0',
                                                               'Test::More' => '0',
                                                               'strict' => '0',
                                                               'warnings' => '0'
                                                             },
                                               'size' => 6093
                                             },
                            't/rt-31107.t' => {
                                                'mtime' => 1642508284,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'CGI' => '0',
                                                                'Test::More' => '0',
                                                                'strict' => '0'
                                                              },
                                                'size' => 952
                                              },
                            't/rt-52469.t' => {
                                                'mtime' => 1642508284,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'CGI' => '0',
                                                                'Test::More' => '0',
                                                                'strict' => '0',
                                                                'warnings' => '0'
                                                              },
                                                'size' => 311
                                              },
                            't/rt-57524.t' => {
                                                'mtime' => 1642508284,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'CGI' => '0',
                                                                'Test::More' => '0',
                                                                'strict' => '0',
                                                                'warnings' => '0'
                                                              },
                                                'size' => 450
                                              },
                            't/rt-75628.t' => {
                                                'mtime' => 1642508284,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'CGI' => '0',
                                                                'Test::More' => '0',
                                                                'strict' => '0'
                                                              },
                                                'size' => 439
                                              },
                            't/rt-84767.t' => {
                                                'mtime' => 1642508284,
                                                'no_index' => 1,
                                                'requires' => {
                                                                'CGI::Carp' => '0',
                                                                'FindBin' => '0',
                                                                'Test::More' => '0',
                                                                'strict' => '0',
                                                                'warnings' => '0'
                                                              },
                                                'size' => 387
                                              },
                            't/rt_31107.txt' => {
                                                  'mtime' => 1642508284,
                                                  'no_index' => 1,
                                                  'size' => 1222
                                                },
                            't/rt_75628.txt' => {
                                                  'mtime' => 1642508284,
                                                  'no_index' => 1,
                                                  'size' => 792
                                                },
                            't/save_read_roundtrip.t' => {
                                                           'mtime' => 1642508284,
                                                           'no_index' => 1,
                                                           'requires' => {
                                                                           'CGI' => '0',
                                                                           'IO::File' => '0',
                                                                           'Test::More' => '0',
                                                                           'strict' => '0',
                                                                           'warnings' => '0'
                                                                         },
                                                           'size' => 613
                                                         },
                            't/sorted.t' => {
                                              'mtime' => 1642508284,
                                              'no_index' => 1,
                                              'requires' => {
                                                              'CGI' => '0',
                                                              'Test::More' => '0',
                                                              'strict' => '0'
                                                            },
                                              'size' => 1445
                                            },
                            't/start_end_asterisk.t' => {
                                                          'mtime' => 1642508284,
                                                          'no_index' => 1,
                                                          'requires' => {
                                                                          'CGI' => '0',
                                                                          'Test::More' => '0',
                                                                          'lib' => '0',
                                                                          'strict' => '0'
                                                                        },
                                                          'size' => 2931
                                                        },
                            't/start_end_end.t' => {
                                                     'mtime' => 1642508284,
                                                     'no_index' => 1,
                                                     'requires' => {
                                                                     'CGI' => '0',
                                                                     'Test::More' => '0',
                                                                     'lib' => '0',
                                                                     'strict' => '0'
                                                                   },
                                                     'size' => 2976
                                                   },
                            't/start_end_start.t' => {
                                                       'mtime' => 1642508284,
                                                       'no_index' => 1,
                                                       'requires' => {
                                                                       'CGI' => '0',
                                                                       'Test::More' => '0',
                                                                       'lib' => '0',
                                                                       'strict' => '0'
                                                                     },
                                                       'size' => 3006
                                                     },
                            't/unescapeHTML.t' => {
                                                    'mtime' => 1642508284,
                                                    'no_index' => 1,
                                                    'requires' => {
                                                                    'CGI' => '0',
                                                                    'Test::More' => '0'
                                                                  },
                                                    'size' => 1052
                                                  },
                            't/upload.t' => {
                                              'mtime' => 1642508284,
                                              'no_index' => 1,
                                              'requires' => {
                                                              'CGI' => '0',
                                                              'Test::More' => '0',
                                                              'strict' => '0'
                                                            },
                                              'size' => 6528
                                            },
                            't/uploadInfo.t' => {
                                                  'mtime' => 1642508284,
                                                  'no_index' => 1,
                                                  'requires' => {
                                                                  'CGI' => '0',
                                                                  'Test::More' => '0',
                                                                  'strict' => '0'
                                                                },
                                                  'size' => 3823
                                                },
                            't/upload_post_quoted_unquoted.txt' => {
                                                                     'mtime' => 1642508284,
                                                                     'no_index' => 1,
                                                                     'size' => 662
                                                                   },
                            't/upload_post_text.txt' => {
                                                          'mtime' => 1642508284,
                                                          'no_index' => 1,
                                                          'size' => 3284
                                                        },
                            't/upload_quoted_unquoted.t' => {
                                                              'mtime' => 1642508284,
                                                              'no_index' => 1,
                                                              'requires' => {
                                                                              'CGI' => '0',
                                                                              'Test::More' => '0',
                                                                              'strict' => '0',
                                                                              'utf8' => '0'
                                                                            },
                                                              'size' => 2073
                                                            },
                            't/url.t' => {
                                           'mtime' => 1709300583,
                                           'no_index' => 1,
                                           'requires' => {
                                                           'CGI' => '0',
                                                           'Test::More' => '0.88',
                                                           'strict' => '0',
                                                           'utf8' => '0',
                                                           'warnings' => '0'
                                                         },
                                           'size' => 5533
                                         },
                            't/user_agent.t' => {
                                                  'mtime' => 1642508284,
                                                  'no_index' => 1,
                                                  'requires' => {
                                                                  'CGI' => '0',
                                                                  'Test::More' => '0'
                                                                },
                                                  'size' => 362
                                                },
                            't/utf8.t' => {
                                            'mtime' => 1642508284,
                                            'no_index' => 1,
                                            'noes' => {
                                                        'warnings' => '0'
                                                      },
                                            'requires' => {
                                                            'Encode' => '0',
                                                            'Test::More' => '0',
                                                            'strict' => '0',
                                                            'utf8' => '0',
                                                            'warnings' => '0'
                                                          },
                                            'size' => 721
                                          },
                            't/util-58.t' => {
                                               'mtime' => 1642508284,
                                               'no_index' => 1,
                                               'requires' => {
                                                               'Test::More' => '0'
                                                             },
                                               'size' => 1058
                                             },
                            't/util.t' => {
                                            'mtime' => 1672731669,
                                            'no_index' => 1,
                                            'requires' => {
                                                            'Config' => '0',
                                                            'Test::More' => '0'
                                                          },
                                            'size' => 2153
                                          }
                          },
          'got_prereq_from' => 'META.yml',
          'ignored_files_array' => [
                                     't/APR/Pool.pm',
                                     't/Apache.pm',
                                     't/Apache2/RequestIO.pm',
                                     't/Apache2/RequestRec.pm',
                                     't/Apache2/RequestUtil.pm',
                                     't/Apache2/Response.pm',
                                     't/Dump.t',
                                     't/ModPerl/Util.pm',
                                     't/append_query.t',
                                     't/arbitrary_handles.t',
                                     't/autoescape.t',
                                     't/can.t',
                                     't/carp.t',
                                     't/cgi.t',
                                     't/charset.t',
                                     't/checkbox_group.t',
                                     't/command_line.t',
                                     't/compiles_pod.t',
                                     't/cookie.t',
                                     't/delete.t',
                                     't/end_form.t',
                                     't/form.t',
                                     't/function.t',
                                     't/gh-155.t',
                                     't/headers.t',
                                     't/headers/attachment.t',
                                     't/headers/charset.t',
                                     't/headers/cookie.t',
                                     't/headers/default.t',
                                     't/headers/nph.t',
                                     't/headers/p3p.t',
                                     't/headers/target.t',
                                     't/headers/type.t',
                                     't/hidden.t',
                                     't/html.t',
                                     't/html_functions.t',
                                     't/http.t',
                                     't/init.t',
                                     't/init_test.txt',
                                     't/multipart_globals.t',
                                     't/multipart_init.t',
                                     't/multipart_start.t',
                                     't/no_tabindex.t',
                                     't/param_fetch.t',
                                     't/param_list_context.t',
                                     't/popup_menu.t',
                                     't/postdata.t',
                                     't/pretty.t',
                                     't/push.t',
                                     't/query_string.t',
                                     't/redirect_query_string.t',
                                     't/request.t',
                                     't/rt-31107.t',
                                     't/rt-52469.t',
                                     't/rt-57524.t',
                                     't/rt-75628.t',
                                     't/rt-84767.t',
                                     't/rt_31107.txt',
                                     't/rt_75628.txt',
                                     't/save_read_roundtrip.t',
                                     't/sorted.t',
                                     't/start_end_asterisk.t',
                                     't/start_end_end.t',
                                     't/start_end_start.t',
                                     't/unescapeHTML.t',
                                     't/upload.t',
                                     't/uploadInfo.t',
                                     't/upload_post_quoted_unquoted.txt',
                                     't/upload_post_text.txt',
                                     't/upload_quoted_unquoted.t',
                                     't/url.t',
                                     't/user_agent.t',
                                     't/utf8.t',
                                     't/util-58.t',
                                     't/util.t'
                                   ],
          'kwalitee' => {
                          'has_abstract_in_pod' => 1,
                          'has_buildtool' => 1,
                          'has_changelog' => 1,
                          'has_human_readable_license' => 1,
                          'has_known_license_in_source_file' => 1,
                          'has_license_in_source_file' => 1,
                          'has_manifest' => 1,
                          'has_meta_json' => 1,
                          'has_meta_yml' => 1,
                          'has_readme' => 1,
                          'has_separate_license_file' => 1,
                          'has_tests' => 1,
                          'has_tests_in_t_dir' => 1,
                          'kwalitee' => 30,
                          'manifest_matches_dist' => 1,
                          'meta_json_conforms_to_known_spec' => 1,
                          'meta_json_is_parsable' => 1,
                          'meta_yml_conforms_to_known_spec' => 1,
                          'meta_yml_declares_perl_version' => 1,
                          'meta_yml_has_license' => 1,
                          'meta_yml_has_provides' => 0,
                          'meta_yml_has_repository_resource' => 1,
                          'meta_yml_is_parsable' => 1,
                          'no_abstract_stub_in_pod' => 1,
                          'no_broken_auto_install' => 1,
                          'no_broken_module_install' => 1,
                          'no_files_to_be_skipped' => 1,
                          'no_maniskip_error' => 1,
                          'no_missing_files_in_provides' => 1,
                          'no_stdin_for_prompting' => 1,
                          'no_symlinks' => 1,
                          'proper_libs' => 1,
                          'use_strict' => 0,
                          'use_warnings' => 0
                        },
          'latest_mtime' => 1709300768,
          'license' => 'artistic_2 defined in META.yml defined in LICENSE',
          'license_file' => 'lib/CGI.pod',
          'license_from_yaml' => 'artistic_2',
          'license_in_pod' => 1,
          'license_type' => 'Artistic_2_0',
          'licenses' => {
                          'Artistic_2_0' => [
                                              'lib/CGI.pod'
                                            ]
                        },
          'manifest_matches_dist' => 1,
          'meta_json' => {
                           'abstract' => 'Handle Common Gateway Interface requests and responses',
                           'author' => [
                                         'unknown'
                                       ],
                           'dynamic_config' => 1,
                           'generated_by' => 'ExtUtils::MakeMaker version 7.62, CPAN::Meta::Converter version 2.150010',
                           'license' => [
                                          'artistic_2'
                                        ],
                           'meta-spec' => {
                                            'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                            'version' => 2
                                          },
                           'name' => 'CGI',
                           'no_index' => {
                                           'directory' => [
                                                            't',
                                                            'inc',
                                                            't'
                                                          ]
                                         },
                           'prereqs' => {
                                          'build' => {
                                                       'requires' => {
                                                                       'ExtUtils::MakeMaker' => '0'
                                                                     }
                                                     },
                                          'configure' => {
                                                           'requires' => {
                                                                           'ExtUtils::MakeMaker' => '0'
                                                                         }
                                                         },
                                          'runtime' => {
                                                         'requires' => {
                                                                         'Carp' => '0',
                                                                         'Config' => '0',
                                                                         'Encode' => '0',
                                                                         'Exporter' => '0',
                                                                         'File::Spec' => '0.82',
                                                                         'File::Temp' => '0.17',
                                                                         'HTML::Entities' => '3.69',
                                                                         'URI' => '1.76',
                                                                         'if' => '0',
                                                                         'overload' => '0',
                                                                         'parent' => '0.225',
                                                                         'perl' => '5.008001',
                                                                         'strict' => '0',
                                                                         'utf8' => '0',
                                                                         'warnings' => '0'
                                                                       }
                                                       },
                                          'test' => {
                                                      'requires' => {
                                                                      'Cwd' => '0',
                                                                      'File::Find' => '0',
                                                                      'IO::File' => '0',
                                                                      'IO::Handle' => '0',
                                                                      'POSIX' => '0',
                                                                      'Test::More' => '0.98',
                                                                      'Test::NoWarnings' => '0',
                                                                      'Test::Warn' => '0.3'
                                                                    }
                                                    }
                                        },
                           'release_status' => 'stable',
                           'resources' => {
                                            'bugtracker' => {
                                                              'web' => 'https://github.com/leejo/CGI.pm/issues'
                                                            },
                                            'homepage' => 'https://metacpan.org/module/CGI',
                                            'license' => [
                                                           'http://dev.perl.org/licenses/'
                                                         ],
                                            'repository' => {
                                                              'type' => 'git',
                                                              'url' => 'https://github.com/leejo/CGI.pm',
                                                              'web' => 'https://github.com/leejo/CGI.pm'
                                                            }
                                          },
                           'version' => '4.62',
                           'x_serialization_backend' => 'JSON::PP version 4.06'
                         },
          'meta_json_is_parsable' => 1,
          'meta_json_spec_version' => 2,
          'meta_yml' => {
                          'abstract' => 'Handle Common Gateway Interface requests and responses',
                          'author' => [
                                        'unknown'
                                      ],
                          'build_requires' => {
                                                'Cwd' => '0',
                                                'ExtUtils::MakeMaker' => '0',
                                                'File::Find' => '0',
                                                'IO::File' => '0',
                                                'IO::Handle' => '0',
                                                'POSIX' => '0',
                                                'Test::More' => '0.98',
                                                'Test::NoWarnings' => '0',
                                                'Test::Warn' => '0.3'
                                              },
                          'configure_requires' => {
                                                    'ExtUtils::MakeMaker' => '0'
                                                  },
                          'dynamic_config' => '1',
                          'generated_by' => 'ExtUtils::MakeMaker version 7.62, CPAN::Meta::Converter version 2.150010',
                          'license' => 'artistic_2',
                          'meta-spec' => {
                                           'url' => 'http://module-build.sourceforge.net/META-spec-v1.4.html',
                                           'version' => '1.4'
                                         },
                          'name' => 'CGI',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc',
                                                           't'
                                                         ]
                                        },
                          'requires' => {
                                          'Carp' => '0',
                                          'Config' => '0',
                                          'Encode' => '0',
                                          'Exporter' => '0',
                                          'File::Spec' => '0.82',
                                          'File::Temp' => '0.17',
                                          'HTML::Entities' => '3.69',
                                          'URI' => '1.76',
                                          'if' => '0',
                                          'overload' => '0',
                                          'parent' => '0.225',
                                          'perl' => '5.008001',
                                          'strict' => '0',
                                          'utf8' => '0',
                                          'warnings' => '0'
                                        },
                          'resources' => {
                                           'bugtracker' => 'https://github.com/leejo/CGI.pm/issues',
                                           'homepage' => 'https://metacpan.org/module/CGI',
                                           'license' => 'http://dev.perl.org/licenses/',
                                           'repository' => 'https://github.com/leejo/CGI.pm'
                                         },
                          'version' => '4.62',
                          'x_serialization_backend' => 'CPAN::Meta::YAML version 0.018'
                        },
          'meta_yml_is_parsable' => 1,
          'meta_yml_spec_version' => '1.4',
          'modules' => [
                         {
                           'file' => 'lib/CGI.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'CGI'
                         },
                         {
                           'file' => 'lib/CGI/Carp.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'CGI::Carp'
                         },
                         {
                           'file' => 'lib/CGI/Cookie.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'CGI::Cookie'
                         },
                         {
                           'file' => 'lib/CGI/File/Temp.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'CGI::File::Temp'
                         },
                         {
                           'file' => 'lib/CGI/HTML/Functions.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'CGI::HTML::Functions'
                         },
                         {
                           'file' => 'lib/CGI/Pretty.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'CGI::Pretty'
                         },
                         {
                           'file' => 'lib/CGI/Push.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'CGI::Push'
                         },
                         {
                           'file' => 'lib/CGI/Util.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'CGI::Util'
                         },
                         {
                           'file' => 'lib/Fh.pm',
                           'in_basedir' => 0,
                           'in_lib' => 1,
                           'module' => 'Fh'
                         }
                       ],
          'no_index' => '^inc/;^t/;^t/',
          'no_pax_headers' => 0,
          'package' => '/home/wbraswell/perlgpt_working/0_metacpan/CGI.tar.gz',
          'prereq' => [
                        {
                          'is_prereq' => 1,
                          'requires' => 'Carp',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Config',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Cwd',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Encode',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'Exporter',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'requires' => 'ExtUtils::MakeMaker',
                          'type' => 'configure_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'File::Find',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'File::Spec',
                          'type' => 'runtime_requires',
                          'version' => '0.82'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'File::Temp',
                          'type' => 'runtime_requires',
                          'version' => '0.17'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'HTML::Entities',
                          'type' => 'runtime_requires',
                          'version' => '3.69'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'IO::File',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'IO::Handle',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'POSIX',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::More',
                          'type' => 'build_requires',
                          'version' => '0.98'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::NoWarnings',
                          'type' => 'build_requires',
                          'version' => '0'
                        },
                        {
                          'is_build_prereq' => 1,
                          'requires' => 'Test::Warn',
                          'type' => 'build_requires',
                          'version' => '0.3'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'URI',
                          'type' => 'runtime_requires',
                          'version' => '1.76'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'if',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'overload',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'parent',
                          'type' => 'runtime_requires',
                          'version' => '0.225'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'perl',
                          'type' => 'runtime_requires',
                          'version' => '5.008001'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'strict',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'utf8',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        },
                        {
                          'is_prereq' => 1,
                          'requires' => 'warnings',
                          'type' => 'runtime_requires',
                          'version' => '0'
                        }
                      ],
          'released' => 1709583486,
          'size_packed' => 208782,
          'size_unpacked' => 664720,
          'test_files' => [
                            't/Dump.t',
                            't/append_query.t',
                            't/arbitrary_handles.t',
                            't/autoescape.t',
                            't/can.t',
                            't/carp.t',
                            't/cgi.t',
                            't/charset.t',
                            't/checkbox_group.t',
                            't/command_line.t',
                            't/compiles_pod.t',
                            't/cookie.t',
                            't/delete.t',
                            't/end_form.t',
                            't/form.t',
                            't/function.t',
                            't/gh-155.t',
                            't/headers.t',
                            't/headers/attachment.t',
                            't/headers/charset.t',
                            't/headers/cookie.t',
                            't/headers/default.t',
                            't/headers/nph.t',
                            't/headers/p3p.t',
                            't/headers/target.t',
                            't/headers/type.t',
                            't/hidden.t',
                            't/html.t',
                            't/html_functions.t',
                            't/http.t',
                            't/init.t',
                            't/multipart_globals.t',
                            't/multipart_init.t',
                            't/multipart_start.t',
                            't/no_tabindex.t',
                            't/param_fetch.t',
                            't/param_list_context.t',
                            't/popup_menu.t',
                            't/postdata.t',
                            't/pretty.t',
                            't/push.t',
                            't/query_string.t',
                            't/redirect_query_string.t',
                            't/request.t',
                            't/rt-31107.t',
                            't/rt-52469.t',
                            't/rt-57524.t',
                            't/rt-75628.t',
                            't/rt-84767.t',
                            't/save_read_roundtrip.t',
                            't/sorted.t',
                            't/start_end_asterisk.t',
                            't/start_end_end.t',
                            't/start_end_start.t',
                            't/unescapeHTML.t',
                            't/upload.t',
                            't/uploadInfo.t',
                            't/upload_quoted_unquoted.t',
                            't/url.t',
                            't/user_agent.t',
                            't/utf8.t',
                            't/util-58.t',
                            't/util.t'
                          ],
          'uses' => {
                      'configure' => {
                                       'requires' => {
                                                       'ExtUtils::MakeMaker' => '0'
                                                     }
                                     },
                      'runtime' => {
                                     'noes' => {
                                                 'strict' => '0',
                                                 'warnings' => '0'
                                               },
                                     'recommends' => {
                                                       'APR::Pool' => '0',
                                                       'APR::Table' => '0',
                                                       'Apache' => '0',
                                                       'Apache2::RequestIO' => '0',
                                                       'Apache2::RequestRec' => '0',
                                                       'Apache2::RequestUtil' => '0',
                                                       'Apache2::Response' => '0',
                                                       'Config' => '0',
                                                       'HTML::Entities' => '0',
                                                       'ModPerl::Util' => '0',
                                                       'Text::ParseWords' => '0'
                                                     },
                                     'requires' => {
                                                     'Carp' => '0',
                                                     'Exporter' => '0',
                                                     'File::Spec' => '0',
                                                     'File::Temp' => '0',
                                                     'URI' => '0',
                                                     'overload' => '0',
                                                     'parent' => '0',
                                                     'perl' => '5.008001',
                                                     'strict' => '0',
                                                     'warnings' => '0'
                                                   },
                                     'suggests' => {
                                                     'Encode' => '0'
                                                   }
                                   },
                      'test' => {
                                  'noes' => {
                                              'warnings' => '0'
                                            },
                                  'recommends' => {
                                                    'utf8' => '0'
                                                  },
                                  'requires' => {
                                                  'Config' => '0',
                                                  'Cwd' => '0',
                                                  'Encode' => '0',
                                                  'File::Find' => '0',
                                                  'File::Temp' => '0',
                                                  'FileHandle' => '0',
                                                  'FindBin' => '0',
                                                  'IO::File' => '0',
                                                  'IO::Handle' => '0',
                                                  'POSIX' => '0',
                                                  'Test::More' => '0.88',
                                                  'Test::Warn' => '0',
                                                  'lib' => '0',
                                                  'overload' => '0',
                                                  'strict' => '0',
                                                  'utf8' => '0',
                                                  'warnings' => '0'
                                                },
                                  'suggests' => {
                                                  'Test::Pod' => '0',
                                                  'Test::Pod::Coverage' => '0'
                                                }
                                }
                    },
          'version' => undef,
          'vname' => 'CGI'
        };
1;
