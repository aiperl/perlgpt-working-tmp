$distribution = $VAR1 = {
          '_id' => 'KQ_8smcu2UfiaYYz8MyR2UTYl10',
          '_type' => 'release',
          'abstract' => 'ESLIF is Extended ScanLess InterFace',
          'archive' => 'MarpaX-ESLIF-6.0.33.4.tar.gz',
          'author' => 'JDDPAUSE',
          'changes_file' => 'Changes',
          'checksum_md5' => 'c2b2b08418b01811b2d50fa60b0e1ae6',
          'checksum_sha256' => '9780e548c95537855a20d421e8557bd856c005a0312d0f15a8572700dc542029',
          'date' => '2024-03-01T06:11:32',
          'dependency' => [
                            {
                              'module' => 'Archive::Tar',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Cwd',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'IO::Handle',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Basename',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Perl::OSType',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Config::AutoConf',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Path',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Find',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'POSIX',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Temp',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Spec',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Probe::Perl',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::chdir',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ExtUtils::Constant',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '5.006'
                            },
                            {
                              'module' => 'Carp',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Config',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Try::Tiny',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ExtUtils::CBuilder',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0.280224'
                            },
                            {
                              'module' => 'File::Copy',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Which',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ExtUtils::CppGuess',
                              'phase' => 'configure',
                              'relationship' => 'suggests',
                              'version' => '0.26'
                            },
                            {
                              'module' => 'JSON::MaybeXS',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.004000'
                            },
                            {
                              'module' => 'warnings',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'namespace::clean',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'parent',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'overload',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Encode',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '5.010'
                            },
                            {
                              'module' => 'Carp',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'strict',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'XSLoader',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Devel::GlobalDestruction',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Math::BigInt',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'constant',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Math::BigFloat',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'CPAN::Meta',
                              'phase' => 'test',
                              'relationship' => 'recommends',
                              'version' => '2.120900'
                            },
                            {
                              'module' => 'utf8',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'IO::Handle',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'POSIX',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Deep',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Spec',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Log::Any',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'diagnostics',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '5.010'
                            },
                            {
                              'module' => 'Try::Tiny',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Safe::Isa',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'open',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'threads',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'IPC::Open3',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More::UTF8',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Data::Dumper',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'threads::shared',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Deep::NoTest',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Log::Any::Adapter',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::DynamicPrereqs',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Pod::Weaver::PluginBundle::RJBS',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Software::License::Perl_5',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::Portability',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::OurPkgVersion',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::CPANFile',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '5'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.96'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::CPAN::Meta::JSON',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::CPAN::Meta::JSON',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.16'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Prereqs::AuthorDeps',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::GitHub::Meta',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Pod::Coverage',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1.08'
                            },
                            {
                              'module' => 'Test::CPAN::Meta',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::Pod::Coverage::Configurable',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::Synopsis',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Perl::Critic',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::MojibakeTests',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::PluginBundle::Starter::Git',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::PodWeaver',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::MinimumPerl',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Authority',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Pod::Coverage::TrustPod',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::MetaTests',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::ChangelogFromGit::CPAN::Changes',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::Version',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::MinimumVersion',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::AutoMetaResources',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Git::Contributors',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Prereqs',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Git::NextVersion',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::Kwalitee',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Pod',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1.41'
                            },
                            {
                              'module' => 'Test::Mojibake',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Portability::Files',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Synopsis',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::AutoPrereqs',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Kwalitee',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1.21'
                            },
                            {
                              'module' => 'Test::NoTabs',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Version',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::GatherFile',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::CPAN::Changes',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.19'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::MinimumVersion',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::NoTabs',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::Perl::Critic',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::CPAN::Changes',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            }
                          ],
          'distribution' => 'MarpaX-ESLIF',
          'download_url' => 'https://cpan.metacpan.org/authors/id/J/JD/JDDPAUSE/MarpaX-ESLIF-6.0.33.4.tar.gz',
          'id' => 'KQ_8smcu2UfiaYYz8MyR2UTYl10',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'MarpaX::ESLIF',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'ESLIF is Extended ScanLess InterFace',
                          'author' => [
                                        'Jean-Damien Durand <jeandamiendurand@free.fr>'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'Dist::Zilla version 6.031, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'MarpaX-ESLIF',
                          'no_index' => {
                                          'directory' => [
                                                           'eg',
                                                           'examples',
                                                           'inc',
                                                           'share',
                                                           't',
                                                           'xt',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'configure' => {
                                                          'requires' => {
                                                                          'Archive::Tar' => '0',
                                                                          'Carp' => '0',
                                                                          'Config' => '0',
                                                                          'Config::AutoConf' => '0',
                                                                          'Cwd' => '0',
                                                                          'ExtUtils::CBuilder' => '0.280224',
                                                                          'ExtUtils::Constant' => '0',
                                                                          'ExtUtils::MakeMaker' => '0',
                                                                          'File::Basename' => '0',
                                                                          'File::Copy' => '0',
                                                                          'File::Find' => '0',
                                                                          'File::Path' => '0',
                                                                          'File::Spec' => '0',
                                                                          'File::Temp' => '0',
                                                                          'File::Which' => '0',
                                                                          'File::chdir' => '0',
                                                                          'IO::Handle' => '0',
                                                                          'POSIX' => '0',
                                                                          'Perl::OSType' => '0',
                                                                          'Probe::Perl' => '0',
                                                                          'Try::Tiny' => '0',
                                                                          'perl' => '5.006'
                                                                        },
                                                          'suggests' => {
                                                                          'ExtUtils::CppGuess' => '0.26'
                                                                        }
                                                        },
                                         'develop' => {
                                                        'requires' => {
                                                                        'Dist::Zilla' => '5',
                                                                        'Dist::Zilla::Plugin::Authority' => '0',
                                                                        'Dist::Zilla::Plugin::AutoMetaResources' => '0',
                                                                        'Dist::Zilla::Plugin::AutoPrereqs' => '0',
                                                                        'Dist::Zilla::Plugin::CPANFile' => '0',
                                                                        'Dist::Zilla::Plugin::ChangelogFromGit::CPAN::Changes' => '0',
                                                                        'Dist::Zilla::Plugin::DynamicPrereqs' => '0',
                                                                        'Dist::Zilla::Plugin::GatherFile' => '0',
                                                                        'Dist::Zilla::Plugin::Git::Contributors' => '0',
                                                                        'Dist::Zilla::Plugin::Git::NextVersion' => '0',
                                                                        'Dist::Zilla::Plugin::GitHub::Meta' => '0',
                                                                        'Dist::Zilla::Plugin::MetaTests' => '0',
                                                                        'Dist::Zilla::Plugin::MinimumPerl' => '0',
                                                                        'Dist::Zilla::Plugin::MojibakeTests' => '0',
                                                                        'Dist::Zilla::Plugin::OurPkgVersion' => '0',
                                                                        'Dist::Zilla::Plugin::PodWeaver' => '0',
                                                                        'Dist::Zilla::Plugin::Prereqs' => '0',
                                                                        'Dist::Zilla::Plugin::Prereqs::AuthorDeps' => '0',
                                                                        'Dist::Zilla::Plugin::Test::CPAN::Changes' => '0',
                                                                        'Dist::Zilla::Plugin::Test::CPAN::Meta::JSON' => '0',
                                                                        'Dist::Zilla::Plugin::Test::Kwalitee' => '0',
                                                                        'Dist::Zilla::Plugin::Test::MinimumVersion' => '0',
                                                                        'Dist::Zilla::Plugin::Test::NoTabs' => '0',
                                                                        'Dist::Zilla::Plugin::Test::Perl::Critic' => '0',
                                                                        'Dist::Zilla::Plugin::Test::Pod::Coverage::Configurable' => '0',
                                                                        'Dist::Zilla::Plugin::Test::Portability' => '0',
                                                                        'Dist::Zilla::Plugin::Test::Synopsis' => '0',
                                                                        'Dist::Zilla::Plugin::Test::Version' => '0',
                                                                        'Dist::Zilla::PluginBundle::Starter::Git' => '0',
                                                                        'Pod::Coverage::TrustPod' => '0',
                                                                        'Pod::Weaver::PluginBundle::RJBS' => '0',
                                                                        'Software::License::Perl_5' => '0',
                                                                        'Test::CPAN::Changes' => '0.19',
                                                                        'Test::CPAN::Meta' => '0',
                                                                        'Test::CPAN::Meta::JSON' => '0.16',
                                                                        'Test::Kwalitee' => '1.21',
                                                                        'Test::MinimumVersion' => '0',
                                                                        'Test::Mojibake' => '0',
                                                                        'Test::More' => '0.96',
                                                                        'Test::NoTabs' => '0',
                                                                        'Test::Perl::Critic' => '0',
                                                                        'Test::Pod' => '1.41',
                                                                        'Test::Pod::Coverage' => '1.08',
                                                                        'Test::Portability::Files' => '0',
                                                                        'Test::Synopsis' => '0',
                                                                        'Test::Version' => '1'
                                                                      }
                                                      },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'Carp' => '0',
                                                                        'Devel::GlobalDestruction' => '0',
                                                                        'Encode' => '0',
                                                                        'JSON::MaybeXS' => '1.004000',
                                                                        'Math::BigFloat' => '0',
                                                                        'Math::BigInt' => '0',
                                                                        'XSLoader' => '0',
                                                                        'constant' => '0',
                                                                        'namespace::clean' => '0',
                                                                        'overload' => '0',
                                                                        'parent' => '0',
                                                                        'perl' => '5.010',
                                                                        'strict' => '0',
                                                                        'warnings' => '0'
                                                                      }
                                                      },
                                         'test' => {
                                                     'recommends' => {
                                                                       'CPAN::Meta' => '2.120900'
                                                                     },
                                                     'requires' => {
                                                                     'Data::Dumper' => '0',
                                                                     'ExtUtils::MakeMaker' => '0',
                                                                     'File::Spec' => '0',
                                                                     'IO::Handle' => '0',
                                                                     'IPC::Open3' => '0',
                                                                     'Log::Any' => '0',
                                                                     'Log::Any::Adapter' => '0',
                                                                     'POSIX' => '0',
                                                                     'Safe::Isa' => '0',
                                                                     'Test::Deep' => '0',
                                                                     'Test::Deep::NoTest' => '0',
                                                                     'Test::More' => '0',
                                                                     'Test::More::UTF8' => '0',
                                                                     'Try::Tiny' => '0',
                                                                     'diagnostics' => '0',
                                                                     'open' => '0',
                                                                     'perl' => '5.010',
                                                                     'threads' => '0',
                                                                     'threads::shared' => '0',
                                                                     'utf8' => '0'
                                                                   }
                                                   }
                                       },
                          'provides' => {
                                          'MarpaX::ESLIF' => {
                                                               'file' => 'lib/MarpaX/ESLIF.pm',
                                                               'version' => 'v6.0.33.4'
                                                             },
                                          'MarpaX::ESLIF::Base' => {
                                                                     'file' => 'lib/MarpaX/ESLIF/Base.pm',
                                                                     'version' => 'v6.0.33.4'
                                                                   },
                                          'MarpaX::ESLIF::Event::Type' => {
                                                                            'file' => 'lib/MarpaX/ESLIF/Event/Type.pm',
                                                                            'version' => 'v6.0.33.4'
                                                                          },
                                          'MarpaX::ESLIF::Grammar' => {
                                                                        'file' => 'lib/MarpaX/ESLIF/Grammar.pm',
                                                                        'version' => 'v6.0.33.4'
                                                                      },
                                          'MarpaX::ESLIF::Grammar::Properties' => {
                                                                                    'file' => 'lib/MarpaX/ESLIF/Grammar/Properties.pm',
                                                                                    'version' => 'v6.0.33.4'
                                                                                  },
                                          'MarpaX::ESLIF::Grammar::Rule::Properties' => {
                                                                                          'file' => 'lib/MarpaX/ESLIF/Grammar/Rule/Properties.pm',
                                                                                          'version' => 'v6.0.33.4'
                                                                                        },
                                          'MarpaX::ESLIF::Grammar::Symbol::Properties' => {
                                                                                            'file' => 'lib/MarpaX/ESLIF/Grammar/Symbol/Properties.pm',
                                                                                            'version' => 'v6.0.33.4'
                                                                                          },
                                          'MarpaX::ESLIF::JSON' => {
                                                                     'file' => 'lib/MarpaX/ESLIF/JSON.pm',
                                                                     'version' => 'v6.0.33.4'
                                                                   },
                                          'MarpaX::ESLIF::JSON::Decoder' => {
                                                                              'file' => 'lib/MarpaX/ESLIF/JSON/Decoder.pm',
                                                                              'version' => 'v6.0.33.4'
                                                                            },
                                          'MarpaX::ESLIF::JSON::Decoder::RecognizerInterface' => {
                                                                                                   'file' => 'lib/MarpaX/ESLIF/JSON/Decoder/RecognizerInterface.pm',
                                                                                                   'version' => 'v6.0.33.4'
                                                                                                 },
                                          'MarpaX::ESLIF::JSON::Encoder' => {
                                                                              'file' => 'lib/MarpaX/ESLIF/JSON/Encoder.pm',
                                                                              'version' => 'v6.0.33.4'
                                                                            },
                                          'MarpaX::ESLIF::Logger::Level' => {
                                                                              'file' => 'lib/MarpaX/ESLIF/Logger/Level.pm',
                                                                              'version' => 'v6.0.33.4'
                                                                            },
                                          'MarpaX::ESLIF::Recognizer' => {
                                                                           'file' => 'lib/MarpaX/ESLIF/Recognizer.pm',
                                                                           'version' => 'v6.0.33.4'
                                                                         },
                                          'MarpaX::ESLIF::RegexCallout' => {
                                                                             'file' => 'lib/MarpaX/ESLIF/RegexCallout.pm',
                                                                             'version' => 'v6.0.33.4'
                                                                           },
                                          'MarpaX::ESLIF::Rule::PropertyBitSet' => {
                                                                                     'file' => 'lib/MarpaX/ESLIF/Rule/PropertyBitSet.pm',
                                                                                     'version' => 'v6.0.33.4'
                                                                                   },
                                          'MarpaX::ESLIF::String' => {
                                                                       'file' => 'lib/MarpaX/ESLIF/String.pm',
                                                                       'version' => 'v6.0.33.4'
                                                                     },
                                          'MarpaX::ESLIF::Symbol' => {
                                                                       'file' => 'lib/MarpaX/ESLIF/Symbol.pm',
                                                                       'version' => 'v6.0.33.4'
                                                                     },
                                          'MarpaX::ESLIF::Symbol::EventBitSet' => {
                                                                                    'file' => 'lib/MarpaX/ESLIF/Symbol/EventBitSet.pm',
                                                                                    'version' => 'v6.0.33.4'
                                                                                  },
                                          'MarpaX::ESLIF::Symbol::PropertyBitSet' => {
                                                                                       'file' => 'lib/MarpaX/ESLIF/Symbol/PropertyBitSet.pm',
                                                                                       'version' => 'v6.0.33.4'
                                                                                     },
                                          'MarpaX::ESLIF::Symbol::Type' => {
                                                                             'file' => 'lib/MarpaX/ESLIF/Symbol/Type.pm',
                                                                             'version' => 'v6.0.33.4'
                                                                           },
                                          'MarpaX::ESLIF::Value' => {
                                                                      'file' => 'lib/MarpaX/ESLIF/Value.pm',
                                                                      'version' => 'v6.0.33.4'
                                                                    },
                                          'MarpaX::ESLIF::Value::Type' => {
                                                                            'file' => 'lib/MarpaX/ESLIF/Value/Type.pm',
                                                                            'version' => 'v6.0.33.4'
                                                                          }
                                        },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/jddurand/c-marpaESLIFPerl/issues'
                                                           },
                                           'homepage' => 'https://metacpan.org/release/MarpaX-ESLIF',
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'git://github.com/jddurand/c-marpaESLIFPerl.git',
                                                             'web' => 'https://github.com/jddurand/c-marpaESLIFPerl'
                                                           }
                                         },
                          'version' => '6.0.33.4',
                          'x_authority' => 'cpan:JDDPAUSE',
                          'x_generated_by_perl' => 'v5.26.1',
                          'x_serialization_backend' => 'Cpanel::JSON::XS version 4.11',
                          'x_spdx_expression' => 'Artistic-1.0-Perl OR GPL-1.0-or-later'
                        },
          'name' => 'MarpaX-ESLIF-6.0.33.4',
          'provides' => [
                          'MarpaX::ESLIF',
                          'MarpaX::ESLIF::Base',
                          'MarpaX::ESLIF::Event::Type',
                          'MarpaX::ESLIF::Grammar',
                          'MarpaX::ESLIF::Grammar::Properties',
                          'MarpaX::ESLIF::Grammar::Rule::Properties',
                          'MarpaX::ESLIF::Grammar::Symbol::Properties',
                          'MarpaX::ESLIF::JSON',
                          'MarpaX::ESLIF::JSON::Decoder',
                          'MarpaX::ESLIF::JSON::Decoder::RecognizerInterface',
                          'MarpaX::ESLIF::JSON::Encoder',
                          'MarpaX::ESLIF::Logger::Level',
                          'MarpaX::ESLIF::Recognizer',
                          'MarpaX::ESLIF::RegexCallout',
                          'MarpaX::ESLIF::Rule::PropertyBitSet',
                          'MarpaX::ESLIF::String',
                          'MarpaX::ESLIF::Symbol',
                          'MarpaX::ESLIF::Symbol::EventBitSet',
                          'MarpaX::ESLIF::Symbol::PropertyBitSet',
                          'MarpaX::ESLIF::Symbol::Type',
                          'MarpaX::ESLIF::Value',
                          'MarpaX::ESLIF::Value::Type'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/jddurand/c-marpaESLIFPerl/issues'
                                           },
                           'homepage' => 'https://metacpan.org/release/MarpaX-ESLIF',
                           'repository' => {
                                             'type' => 'git',
                                             'url' => 'git://github.com/jddurand/c-marpaESLIFPerl.git',
                                             'web' => 'https://github.com/jddurand/c-marpaESLIFPerl'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709273492,
                      'size' => 13604470
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 1,
                       'pass' => 99,
                       'unknown' => 0
                     },
          'version' => '6.0.33.4',
          'version_numified' => '6.000033004'
        };
1;
