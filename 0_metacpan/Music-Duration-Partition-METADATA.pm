$distribution = $VAR1 = {
          '_id' => 'E3ThMtpbNtcggH5S7cm9lhoYle8',
          '_type' => 'release',
          'abstract' => 'Partition a musical duration into rhythmic phrases',
          'archive' => 'Music-Duration-Partition-0.0820.tar.gz',
          'author' => 'GENE',
          'changes_file' => 'Changes',
          'checksum_md5' => '492ddbb373afc9270f4abfac57b53213',
          'checksum_sha256' => '085a1af33c3f175e95ab0de77b892204f783bcceec25185101343e3b2a3acb05',
          'date' => '2024-03-01T07:41:30',
          'dependency' => [
                            {
                              'module' => 'IO::Handle',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '5.006'
                            },
                            {
                              'module' => 'strict',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'warnings',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Spec',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Exception',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'IPC::Open3',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::NoTabs',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Pod::Coverage::TrustPod',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Pod::Coverage',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1.08'
                            },
                            {
                              'module' => 'Test::Synopsis',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Pod',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1.41'
                            },
                            {
                              'module' => 'Test::EOL',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Spelling',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.12'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.88'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '5.006'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'namespace::clean',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'constant',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'List::Util',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '5.006'
                            },
                            {
                              'module' => 'Moo',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '2'
                            },
                            {
                              'module' => 'strictures',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '2'
                            },
                            {
                              'module' => 'MIDI::Simple',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Math::Random::Discrete',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'MIDI::Drummer::Tiny',
                              'phase' => 'runtime',
                              'relationship' => 'suggests',
                              'version' => '0'
                            },
                            {
                              'module' => 'MIDI::Util',
                              'phase' => 'runtime',
                              'relationship' => 'suggests',
                              'version' => '0'
                            },
                            {
                              'module' => 'Data::Dumper::Compact',
                              'phase' => 'runtime',
                              'relationship' => 'suggests',
                              'version' => '0'
                            },
                            {
                              'module' => 'Music::Voss',
                              'phase' => 'runtime',
                              'relationship' => 'suggests',
                              'version' => '0'
                            },
                            {
                              'module' => 'Music::VoiceGen',
                              'phase' => 'runtime',
                              'relationship' => 'suggests',
                              'version' => '0'
                            },
                            {
                              'module' => 'Music::Scales',
                              'phase' => 'runtime',
                              'relationship' => 'suggests',
                              'version' => '0'
                            }
                          ],
          'distribution' => 'Music-Duration-Partition',
          'download_url' => 'https://cpan.metacpan.org/authors/id/G/GE/GENE/Music-Duration-Partition-0.0820.tar.gz',
          'id' => 'E3ThMtpbNtcggH5S7cm9lhoYle8',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'Music::Duration::Partition',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Partition a musical duration into rhythmic phrases',
                          'author' => [
                                        'Gene Boggs <gene@cpan.org>'
                                      ],
                          'dynamic_config' => 0,
                          'generated_by' => 'Dist::Zilla version 6.031, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Music-Duration-Partition',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'configure' => {
                                                          'requires' => {
                                                                          'ExtUtils::MakeMaker' => '0',
                                                                          'perl' => '5.006'
                                                                        }
                                                        },
                                         'develop' => {
                                                        'requires' => {
                                                                        'Pod::Coverage::TrustPod' => '0',
                                                                        'Test::EOL' => '0',
                                                                        'Test::More' => '0.88',
                                                                        'Test::NoTabs' => '0',
                                                                        'Test::Pod' => '1.41',
                                                                        'Test::Pod::Coverage' => '1.08',
                                                                        'Test::Spelling' => '0.12',
                                                                        'Test::Synopsis' => '0'
                                                                      }
                                                      },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'List::Util' => '0',
                                                                        'MIDI::Simple' => '0',
                                                                        'Math::Random::Discrete' => '0',
                                                                        'Moo' => '2',
                                                                        'constant' => '0',
                                                                        'namespace::clean' => '0',
                                                                        'perl' => '5.006',
                                                                        'strictures' => '2'
                                                                      },
                                                        'suggests' => {
                                                                        'Data::Dumper::Compact' => '0',
                                                                        'MIDI::Drummer::Tiny' => '0',
                                                                        'MIDI::Util' => '0',
                                                                        'Music::Scales' => '0',
                                                                        'Music::VoiceGen' => '0',
                                                                        'Music::Voss' => '0'
                                                                      }
                                                      },
                                         'test' => {
                                                     'requires' => {
                                                                     'File::Spec' => '0',
                                                                     'IO::Handle' => '0',
                                                                     'IPC::Open3' => '0',
                                                                     'Test::Exception' => '0',
                                                                     'Test::More' => '0',
                                                                     'perl' => '5.006',
                                                                     'strict' => '0',
                                                                     'warnings' => '0'
                                                                   }
                                                   }
                                       },
                          'release_status' => 'stable',
                          'resources' => {
                                           'homepage' => 'https://github.com/ology/Music-Duration-Partition',
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'https://github.com/ology/Music-Duration-Partition.git',
                                                             'web' => 'https://github.com/ology/Music-Duration-Partition'
                                                           }
                                         },
                          'version' => '0.0820',
                          'x_Dist_Zilla' => {
                                              'perl' => {
                                                          'version' => '5.032001'
                                                        },
                                              'plugins' => [
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::VersionFromModule',
                                                               'name' => 'VersionFromModule',
                                                               'version' => '0.08'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MinimumPerl',
                                                               'name' => 'MinimumPerl',
                                                               'version' => '1.006'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::AutoPrereqs',
                                                               'name' => 'AutoPrereqs',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::CheckChangesHasContent',
                                                               'name' => 'CheckChangesHasContent',
                                                               'version' => '0.011'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::NextRelease',
                                                               'name' => 'NextRelease',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Authority',
                                                               'name' => 'Authority',
                                                               'version' => '1.009'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PkgVersion',
                                                               'name' => 'PkgVersion',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PodWeaver',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::PodWeaver' => {
                                                                                                                   'finder' => [
                                                                                                                                 ':InstallModules',
                                                                                                                                 ':PerlExecFiles'
                                                                                                                               ],
                                                                                                                   'plugins' => [
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Plugin::EnsurePod5',
                                                                                                                                    'name' => '@CorePrep/EnsurePod5',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Plugin::H1Nester',
                                                                                                                                    'name' => '@CorePrep/H1Nester',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Plugin::SingleEncoding',
                                                                                                                                    'name' => '@Default/SingleEncoding',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Name',
                                                                                                                                    'name' => '@Default/Name',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Version',
                                                                                                                                    'name' => '@Default/Version',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Region',
                                                                                                                                    'name' => '@Default/prelude',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Generic',
                                                                                                                                    'name' => 'SYNOPSIS',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Generic',
                                                                                                                                    'name' => 'DESCRIPTION',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Generic',
                                                                                                                                    'name' => 'OVERVIEW',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Collect',
                                                                                                                                    'name' => 'ATTRIBUTES',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Collect',
                                                                                                                                    'name' => 'METHODS',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Collect',
                                                                                                                                    'name' => 'FUNCTIONS',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Leftovers',
                                                                                                                                    'name' => '@Default/Leftovers',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Region',
                                                                                                                                    'name' => '@Default/postlude',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Authors',
                                                                                                                                    'name' => '@Default/Authors',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Legal',
                                                                                                                                    'name' => '@Default/Legal',
                                                                                                                                    'version' => '4.017'
                                                                                                                                  }
                                                                                                                                ]
                                                                                                                 }
                                                                           },
                                                               'name' => 'PodWeaver',
                                                               'version' => '4.010'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PodCoverageTests',
                                                               'name' => 'PodCoverageTests',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PodSyntaxTests',
                                                               'name' => 'PodSyntaxTests',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::NoTabs',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Test::NoTabs' => {
                                                                                                                      'filename' => 'xt/author/no-tabs.t',
                                                                                                                      'finder' => [
                                                                                                                                    ':InstallModules',
                                                                                                                                    ':ExecFiles',
                                                                                                                                    ':TestFiles'
                                                                                                                                  ]
                                                                                                                    }
                                                                           },
                                                               'name' => 'Test::NoTabs',
                                                               'version' => '0.15'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::EOL',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Test::EOL' => {
                                                                                                                   'filename' => 'xt/author/eol.t',
                                                                                                                   'finder' => [
                                                                                                                                 ':ExecFiles',
                                                                                                                                 ':InstallModules',
                                                                                                                                 ':TestFiles'
                                                                                                                               ],
                                                                                                                   'trailing_whitespace' => 1
                                                                                                                 }
                                                                           },
                                                               'name' => 'Test::EOL',
                                                               'version' => '0.19'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::Compile',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Test::Compile' => {
                                                                                                                       'bail_out_on_fail' => 0,
                                                                                                                       'fail_on_warning' => 'author',
                                                                                                                       'fake_home' => 0,
                                                                                                                       'filename' => 't/00-compile.t',
                                                                                                                       'module_finder' => [
                                                                                                                                            ':InstallModules'
                                                                                                                                          ],
                                                                                                                       'needs_display' => 0,
                                                                                                                       'phase' => 'test',
                                                                                                                       'script_finder' => [
                                                                                                                                            ':PerlExecFiles'
                                                                                                                                          ],
                                                                                                                       'skips' => [],
                                                                                                                       'switch' => []
                                                                                                                     }
                                                                           },
                                                               'name' => 'Test::Compile',
                                                               'version' => '2.058'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::Synopsis',
                                                               'name' => 'Test::Synopsis',
                                                               'version' => '2.000007'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MetaConfig',
                                                               'name' => 'MetaConfig',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MetaJSON',
                                                               'name' => 'MetaJSON',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::GithubMeta',
                                                               'name' => 'GithubMeta',
                                                               'version' => '0.58'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::PodSpelling',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Test::PodSpelling' => {
                                                                                                                           'directories' => [
                                                                                                                                              'bin',
                                                                                                                                              'lib'
                                                                                                                                            ],
                                                                                                                           'spell_cmd' => 'aspell list',
                                                                                                                           'stopwords' => [
                                                                                                                                            'Instantiation',
                                                                                                                                            'VLC',
                                                                                                                                            'Voss',
                                                                                                                                            'durations',
                                                                                                                                            'hn',
                                                                                                                                            'homebrew',
                                                                                                                                            'qn',
                                                                                                                                            'th',
                                                                                                                                            'wn'
                                                                                                                                          ],
                                                                                                                           'wordlist' => 'Pod::Wordlist'
                                                                                                                         }
                                                                           },
                                                               'name' => 'Test::PodSpelling',
                                                               'version' => '2.007005'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Prereqs',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Prereqs' => {
                                                                                                                 'phase' => 'runtime',
                                                                                                                 'type' => 'requires'
                                                                                                               }
                                                                           },
                                                               'name' => 'Prereqs',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Prereqs',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Prereqs' => {
                                                                                                                 'phase' => 'runtime',
                                                                                                                 'type' => 'suggests'
                                                                                                               }
                                                                           },
                                                               'name' => 'Suggests',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::GatherDir',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::GatherDir' => {
                                                                                                                   'exclude_filename' => [],
                                                                                                                   'exclude_match' => [
                                                                                                                                        '\\.mid',
                                                                                                                                        'site'
                                                                                                                                      ],
                                                                                                                   'follow_symlinks' => 0,
                                                                                                                   'include_dotfiles' => 0,
                                                                                                                   'prefix' => '',
                                                                                                                   'prune_directory' => [],
                                                                                                                   'root' => '.'
                                                                                                                 }
                                                                           },
                                                               'name' => 'GatherDir',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PruneCruft',
                                                               'name' => 'PruneCruft',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::ManifestSkip',
                                                               'name' => 'ManifestSkip',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MetaYAML',
                                                               'name' => 'MetaYAML',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Readme',
                                                               'name' => 'Readme',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::ExtraTests',
                                                               'name' => 'ExtraTests',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::ExecDir',
                                                               'name' => 'ExecDir',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::ShareDir',
                                                               'name' => 'ShareDir',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MakeMaker',
                                                               'config' => {
                                                                             'Dist::Zilla::Role::TestRunner' => {
                                                                                                                  'default_jobs' => 1
                                                                                                                }
                                                                           },
                                                               'name' => 'MakeMaker',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Manifest',
                                                               'name' => 'Manifest',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::TestRelease',
                                                               'name' => 'TestRelease',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::ConfirmRelease',
                                                               'name' => 'ConfirmRelease',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::UploadToCPAN',
                                                               'name' => 'UploadToCPAN',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::CopyFilesFromBuild',
                                                               'name' => 'CopyFilesFromBuild',
                                                               'version' => '0.170880'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':InstallModules',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':IncModules',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':TestFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':ExtraTestFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':ExecFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':PerlExecFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':ShareFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':MainModule',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':AllFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':NoFiles',
                                                               'version' => '6.031'
                                                             }
                                                           ],
                                              'zilla' => {
                                                           'class' => 'Dist::Zilla::Dist::Builder',
                                                           'config' => {
                                                                         'is_trial' => 0
                                                                       },
                                                           'version' => '6.031'
                                                         }
                                            },
                          'x_authority' => 'cpan:GENE',
                          'x_generated_by_perl' => 'v5.32.1',
                          'x_serialization_backend' => 'Cpanel::JSON::XS version 4.26',
                          'x_spdx_expression' => 'Artistic-1.0-Perl OR GPL-1.0-or-later'
                        },
          'name' => 'Music-Duration-Partition-0.0820',
          'provides' => [
                          'Music::Duration::Partition'
                        ],
          'resources' => {
                           'homepage' => 'https://github.com/ology/Music-Duration-Partition',
                           'repository' => {
                                             'type' => 'git',
                                             'url' => 'https://github.com/ology/Music-Duration-Partition.git',
                                             'web' => 'https://github.com/ology/Music-Duration-Partition'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709278890,
                      'size' => 21162
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 0,
                       'pass' => 99,
                       'unknown' => 0
                     },
          'version' => '0.0820',
          'version_numified' => '0.082'
        };
1;
