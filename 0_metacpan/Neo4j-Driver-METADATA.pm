$distribution = $VAR1 = {
          '_id' => 'aNfGWul1Zp9O9EfL42BeoKoVJ2E',
          '_type' => 'release',
          'abstract' => 'Neo4j community graph database driver for Bolt and HTTP',
          'archive' => 'Neo4j-Driver-0.46.tar.gz',
          'author' => 'AJNN',
          'changes_file' => 'Changes',
          'checksum_md5' => '86b8561514919e20e9227001461c0cc7',
          'checksum_sha256' => '57e05d1840f7053c1b6c2b90bb811afe0af13023598fdd2f68777a2eedc4df69',
          'date' => '2024-03-03T12:41:46',
          'dependency' => [
                            {
                              'module' => 'Neo4j::Types',
                              'phase' => 'runtime',
                              'relationship' => 'recommends',
                              'version' => '2.00'
                            },
                            {
                              'module' => 'Time::Piece',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.20'
                            },
                            {
                              'module' => 'LWP::UserAgent',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '6.04'
                            },
                            {
                              'module' => 'Neo4j::Types',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.00'
                            },
                            {
                              'module' => 'Try::Tiny',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '5.010'
                            },
                            {
                              'module' => 'parent',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'JSON::MaybeXS',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.003003'
                            },
                            {
                              'module' => 'URI',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.31'
                            },
                            {
                              'module' => 'Neo4j::Error',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Cpanel::JSON::XS',
                              'phase' => 'runtime',
                              'relationship' => 'suggests',
                              'version' => '4.38'
                            },
                            {
                              'module' => 'LWP::Protocol::https',
                              'phase' => 'runtime',
                              'relationship' => 'suggests',
                              'version' => '0'
                            },
                            {
                              'module' => 'JSON::PP',
                              'phase' => 'runtime',
                              'relationship' => 'suggests',
                              'version' => '4.11'
                            },
                            {
                              'module' => 'Neo4j::Bolt',
                              'phase' => 'runtime',
                              'relationship' => 'suggests',
                              'version' => '0.02'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'REST::Neo4p',
                              'phase' => 'develop',
                              'relationship' => 'suggests',
                              'version' => '0'
                            },
                            {
                              'module' => 'Neo4j::Cypher::Abstract',
                              'phase' => 'develop',
                              'relationship' => 'suggests',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::MinimumVersion',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Warnings',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.010'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.94'
                            },
                            {
                              'module' => 'Test::Exception',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Pod',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1.41'
                            },
                            {
                              'module' => 'Path::Tiny',
                              'phase' => 'develop',
                              'relationship' => 'recommends',
                              'version' => '0.011'
                            },
                            {
                              'module' => 'Test::Warnings',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0.010'
                            },
                            {
                              'module' => 'HTTP::Response',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Mock::Quick',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Exception',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Neo4j::Types',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0.03'
                            },
                            {
                              'module' => 'HTTP::Headers',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Neo4j::Types',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '1.00'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0.94'
                            }
                          ],
          'distribution' => 'Neo4j-Driver',
          'download_url' => 'https://cpan.metacpan.org/authors/id/A/AJ/AJNN/Neo4j-Driver-0.46.tar.gz',
          'id' => 'aNfGWul1Zp9O9EfL42BeoKoVJ2E',
          'license' => [
                         'artistic_2'
                       ],
          'main_module' => 'Neo4j::Driver',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Neo4j community graph database driver for Bolt and HTTP',
                          'author' => [
                                        'Arne Johannessen <ajnn@cpan.org>'
                                      ],
                          'dynamic_config' => 0,
                          'generated_by' => 'Dist::Zilla version 6.031, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'artistic_2'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Neo4j-Driver',
                          'no_index' => {
                                          'directory' => [
                                                           't/lib',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'configure' => {
                                                          'requires' => {
                                                                          'ExtUtils::MakeMaker' => '0'
                                                                        }
                                                        },
                                         'develop' => {
                                                        'recommends' => {
                                                                          'Path::Tiny' => '0.011'
                                                                        },
                                                        'requires' => {
                                                                        'Test::Exception' => '0',
                                                                        'Test::MinimumVersion' => '0',
                                                                        'Test::More' => '0.94',
                                                                        'Test::Pod' => '1.41',
                                                                        'Test::Warnings' => '0.010'
                                                                      },
                                                        'suggests' => {
                                                                        'Neo4j::Cypher::Abstract' => '0',
                                                                        'REST::Neo4p' => '0'
                                                                      }
                                                      },
                                         'runtime' => {
                                                        'recommends' => {
                                                                          'Neo4j::Types' => '2.00'
                                                                        },
                                                        'requires' => {
                                                                        'JSON::MaybeXS' => '1.003003',
                                                                        'LWP::UserAgent' => '6.04',
                                                                        'Neo4j::Error' => '0',
                                                                        'Neo4j::Types' => '1.00',
                                                                        'Time::Piece' => '1.20',
                                                                        'Try::Tiny' => '0',
                                                                        'URI' => '1.31',
                                                                        'parent' => '0',
                                                                        'perl' => '5.010'
                                                                      },
                                                        'suggests' => {
                                                                        'Cpanel::JSON::XS' => '4.38',
                                                                        'JSON::PP' => '4.11',
                                                                        'LWP::Protocol::https' => '0',
                                                                        'Neo4j::Bolt' => '0.02'
                                                                      }
                                                      },
                                         'test' => {
                                                     'requires' => {
                                                                     'HTTP::Headers' => '0',
                                                                     'HTTP::Response' => '0',
                                                                     'Mock::Quick' => '0',
                                                                     'Neo4j::Types' => '1.00',
                                                                     'Test::Exception' => '0',
                                                                     'Test::More' => '0.94',
                                                                     'Test::Neo4j::Types' => '0.03',
                                                                     'Test::Warnings' => '0.010'
                                                                   }
                                                   }
                                       },
                          'provides' => {
                                          'Neo4j::Driver' => {
                                                               'file' => 'lib/Neo4j/Driver.pm',
                                                               'version' => '0.46'
                                                             },
                                          'Neo4j::Driver::Events' => {
                                                                       'file' => 'lib/Neo4j/Driver/Events.pm',
                                                                       'version' => '0.46'
                                                                     },
                                          'Neo4j::Driver::Net::Bolt' => {
                                                                          'file' => 'lib/Neo4j/Driver/Net/Bolt.pm',
                                                                          'version' => '0.46'
                                                                        },
                                          'Neo4j::Driver::Net::HTTP' => {
                                                                          'file' => 'lib/Neo4j/Driver/Net/HTTP.pm',
                                                                          'version' => '0.46'
                                                                        },
                                          'Neo4j::Driver::Net::HTTP::LWP' => {
                                                                               'file' => 'lib/Neo4j/Driver/Net/HTTP/LWP.pm',
                                                                               'version' => '0.46'
                                                                             },
                                          'Neo4j::Driver::Plugin' => {
                                                                       'file' => 'lib/Neo4j/Driver/Plugin.pm',
                                                                       'version' => '0.46'
                                                                     },
                                          'Neo4j::Driver::Record' => {
                                                                       'file' => 'lib/Neo4j/Driver/Record.pm',
                                                                       'version' => '0.46'
                                                                     },
                                          'Neo4j::Driver::Result' => {
                                                                       'file' => 'lib/Neo4j/Driver/Result.pm',
                                                                       'version' => '0.46'
                                                                     },
                                          'Neo4j::Driver::Result::Bolt' => {
                                                                             'file' => 'lib/Neo4j/Driver/Result/Bolt.pm',
                                                                             'version' => '0.46'
                                                                           },
                                          'Neo4j::Driver::Result::JSON' => {
                                                                             'file' => 'lib/Neo4j/Driver/Result/JSON.pm',
                                                                             'version' => '0.46'
                                                                           },
                                          'Neo4j::Driver::Result::Jolt' => {
                                                                             'file' => 'lib/Neo4j/Driver/Result/Jolt.pm',
                                                                             'version' => '0.46'
                                                                           },
                                          'Neo4j::Driver::Result::Text' => {
                                                                             'file' => 'lib/Neo4j/Driver/Result/Text.pm',
                                                                             'version' => '0.46'
                                                                           },
                                          'Neo4j::Driver::ResultColumns' => {
                                                                              'file' => 'lib/Neo4j/Driver/ResultColumns.pm',
                                                                              'version' => '0.46'
                                                                            },
                                          'Neo4j::Driver::ResultSummary' => {
                                                                              'file' => 'lib/Neo4j/Driver/ResultSummary.pm',
                                                                              'version' => '0.46'
                                                                            },
                                          'Neo4j::Driver::ServerInfo' => {
                                                                           'file' => 'lib/Neo4j/Driver/ServerInfo.pm',
                                                                           'version' => '0.46'
                                                                         },
                                          'Neo4j::Driver::Session' => {
                                                                        'file' => 'lib/Neo4j/Driver/Session.pm',
                                                                        'version' => '0.46'
                                                                      },
                                          'Neo4j::Driver::StatementResult' => {
                                                                                'file' => 'lib/Neo4j/Driver/StatementResult.pm',
                                                                                'version' => '0.46'
                                                                              },
                                          'Neo4j::Driver::SummaryCounters' => {
                                                                                'file' => 'lib/Neo4j/Driver/SummaryCounters.pm',
                                                                                'version' => '0.46'
                                                                              },
                                          'Neo4j::Driver::Transaction' => {
                                                                            'file' => 'lib/Neo4j/Driver/Transaction.pm',
                                                                            'version' => '0.46'
                                                                          },
                                          'Neo4j::Driver::Type::Bytes' => {
                                                                            'file' => 'lib/Neo4j/Driver/Type/Bytes.pm',
                                                                            'version' => '0.46'
                                                                          },
                                          'Neo4j::Driver::Type::DateTime' => {
                                                                               'file' => 'lib/Neo4j/Driver/Type/DateTime.pm',
                                                                               'version' => '0.46'
                                                                             },
                                          'Neo4j::Driver::Type::Duration' => {
                                                                               'file' => 'lib/Neo4j/Driver/Type/Duration.pm',
                                                                               'version' => '0.46'
                                                                             },
                                          'Neo4j::Driver::Type::Node' => {
                                                                           'file' => 'lib/Neo4j/Driver/Type/Node.pm',
                                                                           'version' => '0.46'
                                                                         },
                                          'Neo4j::Driver::Type::Path' => {
                                                                           'file' => 'lib/Neo4j/Driver/Type/Path.pm',
                                                                           'version' => '0.46'
                                                                         },
                                          'Neo4j::Driver::Type::Point' => {
                                                                            'file' => 'lib/Neo4j/Driver/Type/Point.pm',
                                                                            'version' => '0.46'
                                                                          },
                                          'Neo4j::Driver::Type::Relationship' => {
                                                                                   'file' => 'lib/Neo4j/Driver/Type/Relationship.pm',
                                                                                   'version' => '0.46'
                                                                                 },
                                          'Neo4j::Driver::Type::Temporal' => {
                                                                               'file' => 'lib/Neo4j/Driver/Type/Temporal.pm',
                                                                               'version' => '0.46'
                                                                             },
                                          'URI::neo4j' => {
                                                            'file' => 'lib/URI/neo4j.pm',
                                                            'version' => '0.46'
                                                          }
                                        },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/johannessen/neo4j-driver-perl/issues'
                                                           },
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'https://github.com/johannessen/neo4j-driver-perl.git',
                                                             'web' => 'https://github.com/johannessen/neo4j-driver-perl'
                                                           }
                                         },
                          'version' => '0.46',
                          'x_generated_by_perl' => 'v5.38.0',
                          'x_serialization_backend' => 'Cpanel::JSON::XS version 4.37',
                          'x_spdx_expression' => 'Artistic-2.0'
                        },
          'name' => 'Neo4j-Driver-0.46',
          'provides' => [
                          'Neo4j::Driver',
                          'Neo4j::Driver::Events',
                          'Neo4j::Driver::Net::Bolt',
                          'Neo4j::Driver::Net::HTTP',
                          'Neo4j::Driver::Net::HTTP::LWP',
                          'Neo4j::Driver::Plugin',
                          'Neo4j::Driver::Record',
                          'Neo4j::Driver::Result',
                          'Neo4j::Driver::Result::Bolt',
                          'Neo4j::Driver::Result::JSON',
                          'Neo4j::Driver::Result::Jolt',
                          'Neo4j::Driver::Result::Text',
                          'Neo4j::Driver::ResultColumns',
                          'Neo4j::Driver::ResultSummary',
                          'Neo4j::Driver::ServerInfo',
                          'Neo4j::Driver::Session',
                          'Neo4j::Driver::StatementResult',
                          'Neo4j::Driver::SummaryCounters',
                          'Neo4j::Driver::Transaction',
                          'Neo4j::Driver::Type::Bytes',
                          'Neo4j::Driver::Type::DateTime',
                          'Neo4j::Driver::Type::Duration',
                          'Neo4j::Driver::Type::Node',
                          'Neo4j::Driver::Type::Path',
                          'Neo4j::Driver::Type::Point',
                          'Neo4j::Driver::Type::Relationship',
                          'Neo4j::Driver::Type::Temporal',
                          'URI::neo4j'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/johannessen/neo4j-driver-perl/issues'
                                           },
                           'repository' => {
                                             'type' => 'git',
                                             'url' => 'https://github.com/johannessen/neo4j-driver-perl.git',
                                             'web' => 'https://github.com/johannessen/neo4j-driver-perl'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709469706,
                      'size' => 128660
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 1,
                       'pass' => 93,
                       'unknown' => 0
                     },
          'version' => '0.46',
          'version_numified' => '0.46'
        };
1;
