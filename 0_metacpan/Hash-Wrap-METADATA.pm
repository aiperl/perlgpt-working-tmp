$distribution = $VAR1 = {
          '_id' => 'azcCYFSg6n6Ikl4IAXzCff0LQFU',
          '_type' => 'release',
          'abstract' => 'create on-the-fly objects from hashes',
          'archive' => 'Hash-Wrap-1.01.tar.gz',
          'author' => 'DJERIUS',
          'changes_file' => 'Changes',
          'checksum_md5' => '63461e6d58b984b5610379cdd44ed3e5',
          'checksum_sha256' => '521947164b1157a0dc1499eed074ca47f12bba44e64c2e010d20942e52416632',
          'date' => '2024-03-01T15:49:35',
          'dependency' => [
                            {
                              'module' => 'Dist::Zilla::Plugin::ReadmeAnyFromPod',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::PodSpelling',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::PodWeaver',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.88'
                            },
                            {
                              'module' => 'Test::Pod::Coverage',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1.08'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::CheckMetaResources',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::InsertExample',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::NoBreakpoints',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::Compile',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::PluginBundle::Filter',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::TrailingSpace',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.0203'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::EnsurePrereqsInstalled',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::RunExtraTests',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::InsertCopyright',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::MetaNoIndex',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::GatherDir::Template',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::NoTabs',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::NextRelease',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::Fixme',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::ModuleBuildTiny',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::EnsureChangesHasContent',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Perl::Critic',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::CPAN::Changes',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::MetaResources',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::PodCoverageTests',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::DistManifest',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Prereqs',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::UnusedVars',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Software::License::GPL_3',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::Version',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::BumpVersionAfterRelease',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Pod::Weaver::Section::GenerateSection',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::TrailingSpace',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::RewriteVersion',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::DistManifest',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '5'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::CPAN::Meta::JSON',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Pod::Weaver::Plugin::StopWords',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::PodSyntaxTests',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Pod::Weaver::Section::Contributors',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::ReportPrereqs',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Readme::Brief',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::MetaJSON',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Pod::Coverage::TrustPod',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Regenerate',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::CPAN::Changes',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.19'
                            },
                            {
                              'module' => 'Test::Vars',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.015'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::CleanNamespaces',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::PluginBundle::Basic',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::NoTabs',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Version',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::MetaProvides::Package',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::Perl::Critic',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Prereqs::AuthorDeps',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::NoBreakpoints',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.15'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::GatherDir',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::CPAN::Meta::JSON',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.16'
                            },
                            {
                              'module' => 'Test::CleanNamespaces',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.15'
                            },
                            {
                              'module' => 'Test::Pod',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1.41'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::CopyFilesFromRelease',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Spelling',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.12'
                            },
                            {
                              'module' => 'CPAN::Meta',
                              'phase' => 'test',
                              'relationship' => 'recommends',
                              'version' => '2.120900'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Spec',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'IO::Handle',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'IPC::Open3',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test2::V0',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Module::Build::Tiny',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0.034'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '5.010'
                            },
                            {
                              'module' => 'Scalar::Util',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            }
                          ],
          'distribution' => 'Hash-Wrap',
          'download_url' => 'https://cpan.metacpan.org/authors/id/D/DJ/DJERIUS/Hash-Wrap-1.01.tar.gz',
          'id' => 'azcCYFSg6n6Ikl4IAXzCff0LQFU',
          'license' => [
                         'gpl_3'
                       ],
          'main_module' => 'Hash::Wrap',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'create on-the-fly objects from hashes',
                          'author' => [
                                        'Diab Jerius <djerius@cpan.org>'
                                      ],
                          'dynamic_config' => 0,
                          'generated_by' => 'Dist::Zilla version 6.030, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'gpl_3'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Hash-Wrap',
                          'no_index' => {
                                          'directory' => [
                                                           'eg',
                                                           'examples',
                                                           'inc',
                                                           'share',
                                                           't',
                                                           'xt',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'configure' => {
                                                          'requires' => {
                                                                          'Module::Build::Tiny' => '0.034'
                                                                        }
                                                        },
                                         'develop' => {
                                                        'requires' => {
                                                                        'Dist::Zilla' => '5',
                                                                        'Dist::Zilla::Plugin::BumpVersionAfterRelease' => '0',
                                                                        'Dist::Zilla::Plugin::CheckMetaResources' => '0',
                                                                        'Dist::Zilla::Plugin::CopyFilesFromRelease' => '0',
                                                                        'Dist::Zilla::Plugin::EnsureChangesHasContent' => '0',
                                                                        'Dist::Zilla::Plugin::EnsurePrereqsInstalled' => '0',
                                                                        'Dist::Zilla::Plugin::GatherDir' => '0',
                                                                        'Dist::Zilla::Plugin::GatherDir::Template' => '0',
                                                                        'Dist::Zilla::Plugin::InsertCopyright' => '0',
                                                                        'Dist::Zilla::Plugin::InsertExample' => '0',
                                                                        'Dist::Zilla::Plugin::MetaJSON' => '0',
                                                                        'Dist::Zilla::Plugin::MetaNoIndex' => '0',
                                                                        'Dist::Zilla::Plugin::MetaProvides::Package' => '0',
                                                                        'Dist::Zilla::Plugin::MetaResources' => '0',
                                                                        'Dist::Zilla::Plugin::ModuleBuildTiny' => '0',
                                                                        'Dist::Zilla::Plugin::NextRelease' => '0',
                                                                        'Dist::Zilla::Plugin::PodCoverageTests' => '0',
                                                                        'Dist::Zilla::Plugin::PodSyntaxTests' => '0',
                                                                        'Dist::Zilla::Plugin::PodWeaver' => '0',
                                                                        'Dist::Zilla::Plugin::Prereqs' => '0',
                                                                        'Dist::Zilla::Plugin::Prereqs::AuthorDeps' => '0',
                                                                        'Dist::Zilla::Plugin::Readme::Brief' => '0',
                                                                        'Dist::Zilla::Plugin::ReadmeAnyFromPod' => '0',
                                                                        'Dist::Zilla::Plugin::Regenerate' => '0',
                                                                        'Dist::Zilla::Plugin::RewriteVersion' => '0',
                                                                        'Dist::Zilla::Plugin::RunExtraTests' => '0',
                                                                        'Dist::Zilla::Plugin::Test::CPAN::Changes' => '0',
                                                                        'Dist::Zilla::Plugin::Test::CPAN::Meta::JSON' => '0',
                                                                        'Dist::Zilla::Plugin::Test::CleanNamespaces' => '0',
                                                                        'Dist::Zilla::Plugin::Test::Compile' => '0',
                                                                        'Dist::Zilla::Plugin::Test::DistManifest' => '0',
                                                                        'Dist::Zilla::Plugin::Test::Fixme' => '0',
                                                                        'Dist::Zilla::Plugin::Test::NoBreakpoints' => '0',
                                                                        'Dist::Zilla::Plugin::Test::NoTabs' => '0',
                                                                        'Dist::Zilla::Plugin::Test::Perl::Critic' => '0',
                                                                        'Dist::Zilla::Plugin::Test::PodSpelling' => '0',
                                                                        'Dist::Zilla::Plugin::Test::ReportPrereqs' => '0',
                                                                        'Dist::Zilla::Plugin::Test::TrailingSpace' => '0',
                                                                        'Dist::Zilla::Plugin::Test::UnusedVars' => '0',
                                                                        'Dist::Zilla::Plugin::Test::Version' => '0',
                                                                        'Dist::Zilla::PluginBundle::Basic' => '0',
                                                                        'Dist::Zilla::PluginBundle::Filter' => '0',
                                                                        'Pod::Coverage::TrustPod' => '0',
                                                                        'Pod::Weaver::Plugin::StopWords' => '0',
                                                                        'Pod::Weaver::Section::Contributors' => '0',
                                                                        'Pod::Weaver::Section::GenerateSection' => '0',
                                                                        'Software::License::GPL_3' => '0',
                                                                        'Test::CPAN::Changes' => '0.19',
                                                                        'Test::CPAN::Meta::JSON' => '0.16',
                                                                        'Test::CleanNamespaces' => '0.15',
                                                                        'Test::DistManifest' => '0',
                                                                        'Test::More' => '0.88',
                                                                        'Test::NoBreakpoints' => '0.15',
                                                                        'Test::NoTabs' => '0',
                                                                        'Test::Perl::Critic' => '0',
                                                                        'Test::Pod' => '1.41',
                                                                        'Test::Pod::Coverage' => '1.08',
                                                                        'Test::Spelling' => '0.12',
                                                                        'Test::TrailingSpace' => '0.0203',
                                                                        'Test::Vars' => '0.015',
                                                                        'Test::Version' => '1'
                                                                      }
                                                      },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'Scalar::Util' => '0',
                                                                        'perl' => '5.010'
                                                                      }
                                                      },
                                         'test' => {
                                                     'recommends' => {
                                                                       'CPAN::Meta' => '2.120900'
                                                                     },
                                                     'requires' => {
                                                                     'ExtUtils::MakeMaker' => '0',
                                                                     'File::Spec' => '0',
                                                                     'IO::Handle' => '0',
                                                                     'IPC::Open3' => '0',
                                                                     'Test2::V0' => '0',
                                                                     'Test::More' => '0'
                                                                   }
                                                   }
                                       },
                          'provides' => {
                                          'Hash::Wrap' => {
                                                            'file' => 'lib/Hash/Wrap.pm',
                                                            'version' => '1.01'
                                                          }
                                        },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'mailto' => 'bug-hash-wrap@rt.cpan.org',
                                                             'web' => 'https://rt.cpan.org/Public/Dist/Display.html?Name=Hash-Wrap'
                                                           },
                                           'repository' => {
                                                             'url' => 'https://gitlab.com/djerius/hash-wrap.git',
                                                             'web' => 'https://gitlab.com/djerius/hash-wrap'
                                                           }
                                         },
                          'version' => '1.01',
                          'x_generated_by_perl' => 'v5.28.3',
                          'x_serialization_backend' => 'Cpanel::JSON::XS version 4.36',
                          'x_spdx_expression' => 'GPL-3.0-only'
                        },
          'name' => 'Hash-Wrap-1.01',
          'provides' => [
                          'Hash::Wrap'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'mailto' => 'bug-hash-wrap@rt.cpan.org',
                                             'web' => 'https://rt.cpan.org/Public/Dist/Display.html?Name=Hash-Wrap'
                                           },
                           'repository' => {
                                             'url' => 'https://gitlab.com/djerius/hash-wrap.git',
                                             'web' => 'https://gitlab.com/djerius/hash-wrap'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709308175,
                      'size' => 45637
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 1,
                       'pass' => 102,
                       'unknown' => 0
                     },
          'version' => '1.01',
          'version_numified' => '1.01'
        };
1;
