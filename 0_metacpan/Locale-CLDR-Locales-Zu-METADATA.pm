$distribution = $VAR1 = {
          '_id' => 'fRiMfFRxNkbJ_Wia_z7__VprZVE',
          '_type' => 'release',
          'abstract' => 'Locale::CLDR - Data Package ( Perl localization data for Zulu )',
          'archive' => 'Locale-CLDR-Locales-Zu-v0.44.1.tar.gz',
          'author' => 'JGNI',
          'changes_file' => '',
          'checksum_md5' => 'a36e57b4621b129f7b1a5ef304c237f1',
          'checksum_sha256' => 'c5be8a64329c83b1177aee04601b7884232618209252e878616940d116b53238',
          'date' => '2024-03-01T06:27:59',
          'dependency' => [
                            {
                              'module' => 'Module::Build',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0.40'
                            },
                            {
                              'module' => 'version',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.95'
                            },
                            {
                              'module' => 'MooX::ClassAttribute',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.011'
                            },
                            {
                              'module' => 'Locale::CLDR',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => 'v0.44.1'
                            },
                            {
                              'module' => 'Type::Tiny',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'DateTime',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.72'
                            },
                            {
                              'module' => 'Moo',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '2'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => 'v5.10.1'
                            },
                            {
                              'module' => 'ok',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Exception',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0.98'
                            }
                          ],
          'distribution' => 'Locale-CLDR-Locales-Zu',
          'download_url' => 'https://cpan.metacpan.org/authors/id/J/JG/JGNI/Locale-CLDR-Locales-Zu-v0.44.1.tar.gz',
          'id' => 'fRiMfFRxNkbJ_Wia_z7__VprZVE',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'Locale::CLDR::Locales::Zu',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Locale::CLDR - Data Package ( Perl localization data for Zulu )',
                          'author' => [
                                        'John Imrie <john.imrie1@gmail.com>'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'Module::Build version 0.4231, CPAN::Meta::Converter version 2.150010',
                          'keywords' => [
                                          'locale',
                                          'CLDR',
                                          'locale-data-pack'
                                        ],
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Locale-CLDR-Locales-Zu',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'Test::Exception' => '0',
                                                                      'Test::More' => '0.98',
                                                                      'ok' => '0'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'Module::Build' => '0.40'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'DateTime' => '0.72',
                                                                        'Locale::CLDR' => 'v0.44.1',
                                                                        'Moo' => '2',
                                                                        'MooX::ClassAttribute' => '0.011',
                                                                        'Type::Tiny' => '0',
                                                                        'perl' => 'v5.10.1',
                                                                        'version' => '0.95'
                                                                      }
                                                      }
                                       },
                          'provides' => {
                                          'Locale::CLDR::Locales::Zu' => {
                                                                           'file' => 'lib/Locale/CLDR/Locales/Zu.pm',
                                                                           'version' => 'v0.44.1'
                                                                         },
                                          'Locale::CLDR::Locales::Zu::Latn' => {
                                                                                 'file' => 'lib/Locale/CLDR/Locales/Zu/Latn.pm',
                                                                                 'version' => 'v0.44.1'
                                                                               },
                                          'Locale::CLDR::Locales::Zu::Latn::Za' => {
                                                                                     'file' => 'lib/Locale/CLDR/Locales/Zu/Latn/Za.pm',
                                                                                     'version' => 'v0.44.1'
                                                                                   }
                                        },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/ThePilgrim/perlcldr/issues'
                                                           },
                                           'homepage' => 'https://github.com/ThePilgrim/perlcldr',
                                           'repository' => {
                                                             'url' => 'https://github.com/ThePilgrim/perlcldr.git'
                                                           }
                                         },
                          'version' => 'v0.44.1',
                          'x_serialization_backend' => 'JSON::PP version 4.06'
                        },
          'name' => 'Locale-CLDR-Locales-Zu-v0.44.1',
          'provides' => [
                          'Locale::CLDR::Locales::Zu',
                          'Locale::CLDR::Locales::Zu::Latn',
                          'Locale::CLDR::Locales::Zu::Latn::Za'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/ThePilgrim/perlcldr/issues'
                                           },
                           'homepage' => 'https://github.com/ThePilgrim/perlcldr',
                           'repository' => {
                                             'url' => 'https://github.com/ThePilgrim/perlcldr.git'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709274479,
                      'size' => 39184
                    },
          'status' => 'latest',
          'version' => 'v0.44.1',
          'version_numified' => '0.044001'
        };
1;
