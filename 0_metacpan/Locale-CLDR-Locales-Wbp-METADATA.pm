$distribution = $VAR1 = {
          '_id' => 's1_vgyxZo5cxWmPyLyS133Res6o',
          '_type' => 'release',
          'abstract' => 'Locale::CLDR - Data Package ( Perl localization data for Warlpiri )',
          'archive' => 'Locale-CLDR-Locales-Wbp-v0.44.1.tar.gz',
          'author' => 'JGNI',
          'changes_file' => '',
          'checksum_md5' => '97072a8b583e6d1b01ebbb77401b1c24',
          'checksum_sha256' => '3fe2c06a575d34199e78966f38c07338bc725f939e714b32041cb37ea4798a7a',
          'date' => '2024-03-01T06:15:29',
          'dependency' => [
                            {
                              'module' => 'Module::Build',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0.40'
                            },
                            {
                              'module' => 'version',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.95'
                            },
                            {
                              'module' => 'DateTime',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.72'
                            },
                            {
                              'module' => 'Locale::CLDR',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => 'v0.44.1'
                            },
                            {
                              'module' => 'MooX::ClassAttribute',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.011'
                            },
                            {
                              'module' => 'Type::Tiny',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => 'v5.10.1'
                            },
                            {
                              'module' => 'Moo',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '2'
                            },
                            {
                              'module' => 'ok',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0.98'
                            },
                            {
                              'module' => 'Test::Exception',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            }
                          ],
          'distribution' => 'Locale-CLDR-Locales-Wbp',
          'download_url' => 'https://cpan.metacpan.org/authors/id/J/JG/JGNI/Locale-CLDR-Locales-Wbp-v0.44.1.tar.gz',
          'id' => 's1_vgyxZo5cxWmPyLyS133Res6o',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'Locale::CLDR::Locales::Wbp',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Locale::CLDR - Data Package ( Perl localization data for Warlpiri )',
                          'author' => [
                                        'John Imrie <john.imrie1@gmail.com>'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'Module::Build version 0.4231, CPAN::Meta::Converter version 2.150005',
                          'keywords' => [
                                          'locale',
                                          'CLDR',
                                          'locale-data-pack'
                                        ],
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Locale-CLDR-Locales-Wbp',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'Test::Exception' => '0',
                                                                      'Test::More' => '0.98',
                                                                      'ok' => '0'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'Module::Build' => '0.40'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'DateTime' => '0.72',
                                                                        'Locale::CLDR' => 'v0.44.1',
                                                                        'Moo' => '2',
                                                                        'MooX::ClassAttribute' => '0.011',
                                                                        'Type::Tiny' => '0',
                                                                        'perl' => 'v5.10.1',
                                                                        'version' => '0.95'
                                                                      }
                                                      }
                                       },
                          'provides' => {
                                          'Locale::CLDR::Locales::Wbp' => {
                                                                            'file' => 'lib/Locale/CLDR/Locales/Wbp.pm',
                                                                            'version' => 'v0.44.1'
                                                                          },
                                          'Locale::CLDR::Locales::Wbp::Latn' => {
                                                                                  'file' => 'lib/Locale/CLDR/Locales/Wbp/Latn.pm',
                                                                                  'version' => 'v0.44.1'
                                                                                },
                                          'Locale::CLDR::Locales::Wbp::Latn::Au' => {
                                                                                      'file' => 'lib/Locale/CLDR/Locales/Wbp/Latn/Au.pm',
                                                                                      'version' => 'v0.44.1'
                                                                                    }
                                        },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/ThePilgrim/perlcldr/issues'
                                                           },
                                           'homepage' => 'https://github.com/ThePilgrim/perlcldr',
                                           'repository' => {
                                                             'url' => 'https://github.com/ThePilgrim/perlcldr.git'
                                                           }
                                         },
                          'version' => 'v0.44.1',
                          'x_serialization_backend' => 'JSON::PP version 4.06'
                        },
          'name' => 'Locale-CLDR-Locales-Wbp-v0.44.1',
          'provides' => [
                          'Locale::CLDR::Locales::Wbp',
                          'Locale::CLDR::Locales::Wbp::Latn',
                          'Locale::CLDR::Locales::Wbp::Latn::Au'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/ThePilgrim/perlcldr/issues'
                                           },
                           'homepage' => 'https://github.com/ThePilgrim/perlcldr',
                           'repository' => {
                                             'url' => 'https://github.com/ThePilgrim/perlcldr.git'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709273729,
                      'size' => 3757
                    },
          'status' => 'latest',
          'version' => 'v0.44.1',
          'version_numified' => '0.044001'
        };
1;
