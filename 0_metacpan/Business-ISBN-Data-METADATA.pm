$distribution = $VAR1 = {
          '_id' => '6XqaA5jQY1o3F_AGZCVY5eNVVoA',
          '_type' => 'release',
          'abstract' => 'data pack for Business::ISBN',
          'archive' => 'Business-ISBN-Data-20240302.001.tar.gz',
          'author' => 'BRIANDFOY',
          'changes_file' => 'Changes',
          'checksum_md5' => 'be3229dee1ec8e0240f01f63b4478621',
          'checksum_sha256' => 'd874af55a1279874ad0a0ed88defb8011534ba270226be11a8629374ae29381f',
          'date' => '2024-03-02T23:34:25',
          'dependency' => [
                            {
                              'module' => 'File::Spec::Functions',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '6.64'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '1'
                            },
                            {
                              'module' => 'Carp',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Basename',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Spec::Functions',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '5.008'
                            }
                          ],
          'distribution' => 'Business-ISBN-Data',
          'download_url' => 'https://cpan.metacpan.org/authors/id/B/BR/BRIANDFOY/Business-ISBN-Data-20240302.001.tar.gz',
          'id' => '6XqaA5jQY1o3F_AGZCVY5eNVVoA',
          'license' => [
                         'artistic_2'
                       ],
          'main_module' => 'Business::ISBN::Data',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'data pack for Business::ISBN',
                          'author' => [
                                        'brian d foy <briandfoy@pobox.com>'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'ExtUtils::MakeMaker version 7.70, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'artistic_2'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Business-ISBN-Data',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {}
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'ExtUtils::MakeMaker' => '6.64',
                                                                          'File::Spec::Functions' => '0'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'Carp' => '0',
                                                                        'File::Basename' => '0',
                                                                        'File::Spec::Functions' => '0',
                                                                        'perl' => '5.008'
                                                                      }
                                                      },
                                         'test' => {
                                                     'requires' => {
                                                                     'Test::More' => '1'
                                                                   }
                                                   }
                                       },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/briandfoy/business-isbn-data/issues'
                                                           },
                                           'homepage' => 'https://github.com/briandfoy/business-isbn-data',
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'https://github.com/briandfoy/business-isbn-data',
                                                             'web' => 'https://github.com/briandfoy/business-isbn-data'
                                                           }
                                         },
                          'version' => '20240302.001',
                          'x_serialization_backend' => 'JSON::PP version 4.16'
                        },
          'name' => 'Business-ISBN-Data-20240302.001',
          'provides' => [
                          'Business::ISBN::Data'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/briandfoy/business-isbn-data/issues'
                                           },
                           'homepage' => 'https://github.com/briandfoy/business-isbn-data',
                           'repository' => {
                                             'type' => 'git',
                                             'url' => 'https://github.com/briandfoy/business-isbn-data',
                                             'web' => 'https://github.com/briandfoy/business-isbn-data'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709422465,
                      'size' => 34005
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 0,
                       'pass' => 116,
                       'unknown' => 0
                     },
          'version' => '20240302.001',
          'version_numified' => '20240302.001'
        };
1;
