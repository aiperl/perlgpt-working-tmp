$distribution = $VAR1 = {
          '_id' => 'lKc7JqWRVaVcLwNnuIRpXI2GiL4',
          '_type' => 'release',
          'abstract' => 'Mail Analyzing Interface for bounce mails.',
          'archive' => 'Sisimai-v5.0.1.tar.gz',
          'author' => 'AKXLIX',
          'changes_file' => 'ChangeLog.md',
          'checksum_md5' => 'ffaa659b648ab341a916c05098eb082b',
          'checksum_sha256' => '3f0bbb16463daf296820bbda2c5a26d1e9641be4bbcb09cafda3a177ca85f91a',
          'date' => '2024-03-03T09:06:26',
          'dependency' => [
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '5.010001'
                            },
                            {
                              'module' => 'Class::Accessor::Lite',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.05'
                            },
                            {
                              'module' => 'Time::Piece',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.29'
                            },
                            {
                              'module' => 'Time::Local',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.19'
                            },
                            {
                              'module' => 'JSON',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '2.90'
                            },
                            {
                              'module' => 'Module::Load',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.32'
                            },
                            {
                              'module' => 'Module::Build::Tiny',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0.035'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0.98'
                            },
                            {
                              'module' => 'Test::UsedModules',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.03'
                            },
                            {
                              'module' => 'Test::MinimumVersion::Fast',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.04'
                            },
                            {
                              'module' => 'Test::PAUSE::Permissions',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.07'
                            },
                            {
                              'module' => 'Test::CPAN::Meta',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Pod',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1.41'
                            },
                            {
                              'module' => 'Test::Spellunker',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => 'v0.2.7'
                            }
                          ],
          'distribution' => 'Sisimai',
          'download_url' => 'https://cpan.metacpan.org/authors/id/A/AK/AKXLIX/Sisimai-v5.0.1.tar.gz',
          'id' => 'lKc7JqWRVaVcLwNnuIRpXI2GiL4',
          'license' => [
                         'freebsd'
                       ],
          'main_module' => 'Sisimai',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Mail Analyzing Interface for bounce mails.',
                          'author' => [
                                        'azumakuniyuki'
                                      ],
                          'dynamic_config' => 0,
                          'generated_by' => 'Minilla/v3.1.8, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'freebsd'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Sisimai',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'share',
                                                           'eg',
                                                           'examples',
                                                           'author',
                                                           'builder',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'configure' => {
                                                          'requires' => {
                                                                          'Module::Build::Tiny' => '0.035'
                                                                        }
                                                        },
                                         'develop' => {
                                                        'requires' => {
                                                                        'Test::CPAN::Meta' => '0',
                                                                        'Test::MinimumVersion::Fast' => '0.04',
                                                                        'Test::PAUSE::Permissions' => '0.07',
                                                                        'Test::Pod' => '1.41',
                                                                        'Test::Spellunker' => 'v0.2.7',
                                                                        'Test::UsedModules' => '0.03'
                                                                      }
                                                      },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'Class::Accessor::Lite' => '0.05',
                                                                        'JSON' => '2.90',
                                                                        'Module::Load' => '0.32',
                                                                        'Time::Local' => '1.19',
                                                                        'Time::Piece' => '1.29',
                                                                        'perl' => '5.010001'
                                                                      }
                                                      },
                                         'test' => {
                                                     'requires' => {
                                                                     'Test::More' => '0.98'
                                                                   }
                                                   }
                                       },
                          'provides' => {
                                          'Sisimai' => {
                                                         'file' => 'lib/Sisimai.pm',
                                                         'version' => 'v5.0.1'
                                                       },
                                          'Sisimai::ARF' => {
                                                              'file' => 'lib/Sisimai/ARF.pm'
                                                            },
                                          'Sisimai::Address' => {
                                                                  'file' => 'lib/Sisimai/Address.pm'
                                                                },
                                          'Sisimai::DateTime' => {
                                                                   'file' => 'lib/Sisimai/DateTime.pm'
                                                                 },
                                          'Sisimai::Fact' => {
                                                               'file' => 'lib/Sisimai/Fact.pm'
                                                             },
                                          'Sisimai::Fact::JSON' => {
                                                                     'file' => 'lib/Sisimai/Fact/JSON.pm'
                                                                   },
                                          'Sisimai::Fact::YAML' => {
                                                                     'file' => 'lib/Sisimai/Fact/YAML.pm'
                                                                   },
                                          'Sisimai::Lhost' => {
                                                                'file' => 'lib/Sisimai/Lhost.pm'
                                                              },
                                          'Sisimai::Lhost::Activehunter' => {
                                                                              'file' => 'lib/Sisimai/Lhost/Activehunter.pm'
                                                                            },
                                          'Sisimai::Lhost::Amavis' => {
                                                                        'file' => 'lib/Sisimai/Lhost/Amavis.pm'
                                                                      },
                                          'Sisimai::Lhost::AmazonSES' => {
                                                                           'file' => 'lib/Sisimai/Lhost/AmazonSES.pm'
                                                                         },
                                          'Sisimai::Lhost::AmazonWorkMail' => {
                                                                                'file' => 'lib/Sisimai/Lhost/AmazonWorkMail.pm'
                                                                              },
                                          'Sisimai::Lhost::Aol' => {
                                                                     'file' => 'lib/Sisimai/Lhost/Aol.pm'
                                                                   },
                                          'Sisimai::Lhost::ApacheJames' => {
                                                                             'file' => 'lib/Sisimai/Lhost/ApacheJames.pm'
                                                                           },
                                          'Sisimai::Lhost::Barracuda' => {
                                                                           'file' => 'lib/Sisimai/Lhost/Barracuda.pm'
                                                                         },
                                          'Sisimai::Lhost::Bigfoot' => {
                                                                         'file' => 'lib/Sisimai/Lhost/Bigfoot.pm'
                                                                       },
                                          'Sisimai::Lhost::Biglobe' => {
                                                                         'file' => 'lib/Sisimai/Lhost/Biglobe.pm'
                                                                       },
                                          'Sisimai::Lhost::Courier' => {
                                                                         'file' => 'lib/Sisimai/Lhost/Courier.pm'
                                                                       },
                                          'Sisimai::Lhost::Domino' => {
                                                                        'file' => 'lib/Sisimai/Lhost/Domino.pm'
                                                                      },
                                          'Sisimai::Lhost::EZweb' => {
                                                                       'file' => 'lib/Sisimai/Lhost/EZweb.pm'
                                                                     },
                                          'Sisimai::Lhost::EinsUndEins' => {
                                                                             'file' => 'lib/Sisimai/Lhost/EinsUndEins.pm'
                                                                           },
                                          'Sisimai::Lhost::Exchange2003' => {
                                                                              'file' => 'lib/Sisimai/Lhost/Exchange2003.pm'
                                                                            },
                                          'Sisimai::Lhost::Exchange2007' => {
                                                                              'file' => 'lib/Sisimai/Lhost/Exchange2007.pm'
                                                                            },
                                          'Sisimai::Lhost::Exim' => {
                                                                      'file' => 'lib/Sisimai/Lhost/Exim.pm'
                                                                    },
                                          'Sisimai::Lhost::FML' => {
                                                                     'file' => 'lib/Sisimai/Lhost/FML.pm'
                                                                   },
                                          'Sisimai::Lhost::Facebook' => {
                                                                          'file' => 'lib/Sisimai/Lhost/Facebook.pm'
                                                                        },
                                          'Sisimai::Lhost::GMX' => {
                                                                     'file' => 'lib/Sisimai/Lhost/GMX.pm'
                                                                   },
                                          'Sisimai::Lhost::GSuite' => {
                                                                        'file' => 'lib/Sisimai/Lhost/GSuite.pm'
                                                                      },
                                          'Sisimai::Lhost::Gmail' => {
                                                                       'file' => 'lib/Sisimai/Lhost/Gmail.pm'
                                                                     },
                                          'Sisimai::Lhost::GoogleGroups' => {
                                                                              'file' => 'lib/Sisimai/Lhost/GoogleGroups.pm'
                                                                            },
                                          'Sisimai::Lhost::IMailServer' => {
                                                                             'file' => 'lib/Sisimai/Lhost/IMailServer.pm'
                                                                           },
                                          'Sisimai::Lhost::InterScanMSS' => {
                                                                              'file' => 'lib/Sisimai/Lhost/InterScanMSS.pm'
                                                                            },
                                          'Sisimai::Lhost::KDDI' => {
                                                                      'file' => 'lib/Sisimai/Lhost/KDDI.pm'
                                                                    },
                                          'Sisimai::Lhost::MXLogic' => {
                                                                         'file' => 'lib/Sisimai/Lhost/MXLogic.pm'
                                                                       },
                                          'Sisimai::Lhost::MailFoundry' => {
                                                                             'file' => 'lib/Sisimai/Lhost/MailFoundry.pm'
                                                                           },
                                          'Sisimai::Lhost::MailMarshalSMTP' => {
                                                                                 'file' => 'lib/Sisimai/Lhost/MailMarshalSMTP.pm'
                                                                               },
                                          'Sisimai::Lhost::MailRu' => {
                                                                        'file' => 'lib/Sisimai/Lhost/MailRu.pm'
                                                                      },
                                          'Sisimai::Lhost::McAfee' => {
                                                                        'file' => 'lib/Sisimai/Lhost/McAfee.pm'
                                                                      },
                                          'Sisimai::Lhost::MessageLabs' => {
                                                                             'file' => 'lib/Sisimai/Lhost/MessageLabs.pm'
                                                                           },
                                          'Sisimai::Lhost::MessagingServer' => {
                                                                                 'file' => 'lib/Sisimai/Lhost/MessagingServer.pm'
                                                                               },
                                          'Sisimai::Lhost::Notes' => {
                                                                       'file' => 'lib/Sisimai/Lhost/Notes.pm'
                                                                     },
                                          'Sisimai::Lhost::Office365' => {
                                                                           'file' => 'lib/Sisimai/Lhost/Office365.pm'
                                                                         },
                                          'Sisimai::Lhost::OpenSMTPD' => {
                                                                           'file' => 'lib/Sisimai/Lhost/OpenSMTPD.pm'
                                                                         },
                                          'Sisimai::Lhost::Outlook' => {
                                                                         'file' => 'lib/Sisimai/Lhost/Outlook.pm'
                                                                       },
                                          'Sisimai::Lhost::Postfix' => {
                                                                         'file' => 'lib/Sisimai/Lhost/Postfix.pm'
                                                                       },
                                          'Sisimai::Lhost::PowerMTA' => {
                                                                          'file' => 'lib/Sisimai/Lhost/PowerMTA.pm'
                                                                        },
                                          'Sisimai::Lhost::ReceivingSES' => {
                                                                              'file' => 'lib/Sisimai/Lhost/ReceivingSES.pm'
                                                                            },
                                          'Sisimai::Lhost::SendGrid' => {
                                                                          'file' => 'lib/Sisimai/Lhost/SendGrid.pm'
                                                                        },
                                          'Sisimai::Lhost::Sendmail' => {
                                                                          'file' => 'lib/Sisimai/Lhost/Sendmail.pm'
                                                                        },
                                          'Sisimai::Lhost::SurfControl' => {
                                                                             'file' => 'lib/Sisimai/Lhost/SurfControl.pm'
                                                                           },
                                          'Sisimai::Lhost::V5sendmail' => {
                                                                            'file' => 'lib/Sisimai/Lhost/V5sendmail.pm'
                                                                          },
                                          'Sisimai::Lhost::Verizon' => {
                                                                         'file' => 'lib/Sisimai/Lhost/Verizon.pm'
                                                                       },
                                          'Sisimai::Lhost::X1' => {
                                                                    'file' => 'lib/Sisimai/Lhost/X1.pm'
                                                                  },
                                          'Sisimai::Lhost::X2' => {
                                                                    'file' => 'lib/Sisimai/Lhost/X2.pm'
                                                                  },
                                          'Sisimai::Lhost::X3' => {
                                                                    'file' => 'lib/Sisimai/Lhost/X3.pm'
                                                                  },
                                          'Sisimai::Lhost::X4' => {
                                                                    'file' => 'lib/Sisimai/Lhost/X4.pm'
                                                                  },
                                          'Sisimai::Lhost::X5' => {
                                                                    'file' => 'lib/Sisimai/Lhost/X5.pm'
                                                                  },
                                          'Sisimai::Lhost::X6' => {
                                                                    'file' => 'lib/Sisimai/Lhost/X6.pm'
                                                                  },
                                          'Sisimai::Lhost::Yahoo' => {
                                                                       'file' => 'lib/Sisimai/Lhost/Yahoo.pm'
                                                                     },
                                          'Sisimai::Lhost::Yandex' => {
                                                                        'file' => 'lib/Sisimai/Lhost/Yandex.pm'
                                                                      },
                                          'Sisimai::Lhost::Zoho' => {
                                                                      'file' => 'lib/Sisimai/Lhost/Zoho.pm'
                                                                    },
                                          'Sisimai::Lhost::mFILTER' => {
                                                                         'file' => 'lib/Sisimai/Lhost/mFILTER.pm'
                                                                       },
                                          'Sisimai::Lhost::qmail' => {
                                                                       'file' => 'lib/Sisimai/Lhost/qmail.pm'
                                                                     },
                                          'Sisimai::MDA' => {
                                                              'file' => 'lib/Sisimai/MDA.pm'
                                                            },
                                          'Sisimai::Mail' => {
                                                               'file' => 'lib/Sisimai/Mail.pm'
                                                             },
                                          'Sisimai::Mail::Maildir' => {
                                                                        'file' => 'lib/Sisimai/Mail/Maildir.pm'
                                                                      },
                                          'Sisimai::Mail::Mbox' => {
                                                                     'file' => 'lib/Sisimai/Mail/Mbox.pm'
                                                                   },
                                          'Sisimai::Mail::Memory' => {
                                                                       'file' => 'lib/Sisimai/Mail/Memory.pm'
                                                                     },
                                          'Sisimai::Mail::STDIN' => {
                                                                      'file' => 'lib/Sisimai/Mail/STDIN.pm'
                                                                    },
                                          'Sisimai::Message' => {
                                                                  'file' => 'lib/Sisimai/Message.pm'
                                                                },
                                          'Sisimai::Order' => {
                                                                'file' => 'lib/Sisimai/Order.pm'
                                                              },
                                          'Sisimai::RFC1894' => {
                                                                  'file' => 'lib/Sisimai/RFC1894.pm'
                                                                },
                                          'Sisimai::RFC2045' => {
                                                                  'file' => 'lib/Sisimai/RFC2045.pm'
                                                                },
                                          'Sisimai::RFC3464' => {
                                                                  'file' => 'lib/Sisimai/RFC3464.pm'
                                                                },
                                          'Sisimai::RFC3834' => {
                                                                  'file' => 'lib/Sisimai/RFC3834.pm'
                                                                },
                                          'Sisimai::RFC5322' => {
                                                                  'file' => 'lib/Sisimai/RFC5322.pm'
                                                                },
                                          'Sisimai::RFC5965' => {
                                                                  'file' => 'lib/Sisimai/RFC5965.pm'
                                                                },
                                          'Sisimai::Reason' => {
                                                                 'file' => 'lib/Sisimai/Reason.pm'
                                                               },
                                          'Sisimai::Reason::AuthFailure' => {
                                                                              'file' => 'lib/Sisimai/Reason/AuthFailure.pm'
                                                                            },
                                          'Sisimai::Reason::BadReputation' => {
                                                                                'file' => 'lib/Sisimai/Reason/BadReputation.pm'
                                                                              },
                                          'Sisimai::Reason::Blocked' => {
                                                                          'file' => 'lib/Sisimai/Reason/Blocked.pm'
                                                                        },
                                          'Sisimai::Reason::ContentError' => {
                                                                               'file' => 'lib/Sisimai/Reason/ContentError.pm'
                                                                             },
                                          'Sisimai::Reason::Delivered' => {
                                                                            'file' => 'lib/Sisimai/Reason/Delivered.pm'
                                                                          },
                                          'Sisimai::Reason::ExceedLimit' => {
                                                                              'file' => 'lib/Sisimai/Reason/ExceedLimit.pm'
                                                                            },
                                          'Sisimai::Reason::Expired' => {
                                                                          'file' => 'lib/Sisimai/Reason/Expired.pm'
                                                                        },
                                          'Sisimai::Reason::Feedback' => {
                                                                           'file' => 'lib/Sisimai/Reason/Feedback.pm'
                                                                         },
                                          'Sisimai::Reason::Filtered' => {
                                                                           'file' => 'lib/Sisimai/Reason/Filtered.pm'
                                                                         },
                                          'Sisimai::Reason::HasMoved' => {
                                                                           'file' => 'lib/Sisimai/Reason/HasMoved.pm'
                                                                         },
                                          'Sisimai::Reason::HostUnknown' => {
                                                                              'file' => 'lib/Sisimai/Reason/HostUnknown.pm'
                                                                            },
                                          'Sisimai::Reason::MailboxFull' => {
                                                                              'file' => 'lib/Sisimai/Reason/MailboxFull.pm'
                                                                            },
                                          'Sisimai::Reason::MailerError' => {
                                                                              'file' => 'lib/Sisimai/Reason/MailerError.pm'
                                                                            },
                                          'Sisimai::Reason::MesgTooBig' => {
                                                                             'file' => 'lib/Sisimai/Reason/MesgTooBig.pm'
                                                                           },
                                          'Sisimai::Reason::NetworkError' => {
                                                                               'file' => 'lib/Sisimai/Reason/NetworkError.pm'
                                                                             },
                                          'Sisimai::Reason::NoRelaying' => {
                                                                             'file' => 'lib/Sisimai/Reason/NoRelaying.pm'
                                                                           },
                                          'Sisimai::Reason::NotAccept' => {
                                                                            'file' => 'lib/Sisimai/Reason/NotAccept.pm'
                                                                          },
                                          'Sisimai::Reason::NotCompliantRFC' => {
                                                                                  'file' => 'lib/Sisimai/Reason/NotCompliantRFC.pm'
                                                                                },
                                          'Sisimai::Reason::OnHold' => {
                                                                         'file' => 'lib/Sisimai/Reason/OnHold.pm'
                                                                       },
                                          'Sisimai::Reason::PolicyViolation' => {
                                                                                  'file' => 'lib/Sisimai/Reason/PolicyViolation.pm'
                                                                                },
                                          'Sisimai::Reason::Rejected' => {
                                                                           'file' => 'lib/Sisimai/Reason/Rejected.pm'
                                                                         },
                                          'Sisimai::Reason::RequirePTR' => {
                                                                             'file' => 'lib/Sisimai/Reason/RequirePTR.pm'
                                                                           },
                                          'Sisimai::Reason::SecurityError' => {
                                                                                'file' => 'lib/Sisimai/Reason/SecurityError.pm'
                                                                              },
                                          'Sisimai::Reason::SpamDetected' => {
                                                                               'file' => 'lib/Sisimai/Reason/SpamDetected.pm'
                                                                             },
                                          'Sisimai::Reason::Speeding' => {
                                                                           'file' => 'lib/Sisimai/Reason/Speeding.pm'
                                                                         },
                                          'Sisimai::Reason::Suspend' => {
                                                                          'file' => 'lib/Sisimai/Reason/Suspend.pm'
                                                                        },
                                          'Sisimai::Reason::SyntaxError' => {
                                                                              'file' => 'lib/Sisimai/Reason/SyntaxError.pm'
                                                                            },
                                          'Sisimai::Reason::SystemError' => {
                                                                              'file' => 'lib/Sisimai/Reason/SystemError.pm'
                                                                            },
                                          'Sisimai::Reason::SystemFull' => {
                                                                             'file' => 'lib/Sisimai/Reason/SystemFull.pm'
                                                                           },
                                          'Sisimai::Reason::TooManyConn' => {
                                                                              'file' => 'lib/Sisimai/Reason/TooManyConn.pm'
                                                                            },
                                          'Sisimai::Reason::Undefined' => {
                                                                            'file' => 'lib/Sisimai/Reason/Undefined.pm'
                                                                          },
                                          'Sisimai::Reason::UserUnknown' => {
                                                                              'file' => 'lib/Sisimai/Reason/UserUnknown.pm'
                                                                            },
                                          'Sisimai::Reason::Vacation' => {
                                                                           'file' => 'lib/Sisimai/Reason/Vacation.pm'
                                                                         },
                                          'Sisimai::Reason::VirusDetected' => {
                                                                                'file' => 'lib/Sisimai/Reason/VirusDetected.pm'
                                                                              },
                                          'Sisimai::Rhost' => {
                                                                'file' => 'lib/Sisimai/Rhost.pm'
                                                              },
                                          'Sisimai::Rhost::Cox' => {
                                                                     'file' => 'lib/Sisimai/Rhost/Cox.pm'
                                                                   },
                                          'Sisimai::Rhost::FrancePTT' => {
                                                                           'file' => 'lib/Sisimai/Rhost/FrancePTT.pm'
                                                                         },
                                          'Sisimai::Rhost::GoDaddy' => {
                                                                         'file' => 'lib/Sisimai/Rhost/GoDaddy.pm'
                                                                       },
                                          'Sisimai::Rhost::Google' => {
                                                                        'file' => 'lib/Sisimai/Rhost/Google.pm'
                                                                      },
                                          'Sisimai::Rhost::IUA' => {
                                                                     'file' => 'lib/Sisimai/Rhost/IUA.pm'
                                                                   },
                                          'Sisimai::Rhost::KDDI' => {
                                                                      'file' => 'lib/Sisimai/Rhost/KDDI.pm'
                                                                    },
                                          'Sisimai::Rhost::Microsoft' => {
                                                                           'file' => 'lib/Sisimai/Rhost/Microsoft.pm'
                                                                         },
                                          'Sisimai::Rhost::Mimecast' => {
                                                                          'file' => 'lib/Sisimai/Rhost/Mimecast.pm'
                                                                        },
                                          'Sisimai::Rhost::NTTDOCOMO' => {
                                                                           'file' => 'lib/Sisimai/Rhost/NTTDOCOMO.pm'
                                                                         },
                                          'Sisimai::Rhost::Spectrum' => {
                                                                          'file' => 'lib/Sisimai/Rhost/Spectrum.pm'
                                                                        },
                                          'Sisimai::Rhost::Tencent' => {
                                                                         'file' => 'lib/Sisimai/Rhost/Tencent.pm'
                                                                       },
                                          'Sisimai::SMTP' => {
                                                               'file' => 'lib/Sisimai/SMTP.pm'
                                                             },
                                          'Sisimai::SMTP::Command' => {
                                                                        'file' => 'lib/Sisimai/SMTP/Command.pm'
                                                                      },
                                          'Sisimai::SMTP::Error' => {
                                                                      'file' => 'lib/Sisimai/SMTP/Error.pm'
                                                                    },
                                          'Sisimai::SMTP::Reply' => {
                                                                      'file' => 'lib/Sisimai/SMTP/Reply.pm'
                                                                    },
                                          'Sisimai::SMTP::Status' => {
                                                                       'file' => 'lib/Sisimai/SMTP/Status.pm'
                                                                     },
                                          'Sisimai::SMTP::Transcript' => {
                                                                           'file' => 'lib/Sisimai/SMTP/Transcript.pm'
                                                                         },
                                          'Sisimai::String' => {
                                                                 'file' => 'lib/Sisimai/String.pm'
                                                               },
                                          'Sisimai::Time' => {
                                                               'file' => 'lib/Sisimai/Time.pm'
                                                             }
                                        },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/sisimai/p5-sisimai/issues'
                                                           },
                                           'homepage' => 'https://libsisimai.org/',
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'git://github.com/sisimai/p5-sisimai.git',
                                                             'web' => 'https://github.com/sisimai/p5-sisimai'
                                                           }
                                         },
                          'version' => 'v5.0.1',
                          'x_contributors' => [
                                                '0xcdcdcdcd <0xcdcdcdcd@gmail.com>',
                                                'Adrian Yee <adrian@gt.net>',
                                                'Alexandre Derumier <aderumier@odiso.com>',
                                                'Jon Jensen <jon@endpoint.com>',
                                                'Jonathan Leroy <jonathan@inikup.com>',
                                                'Mutsutoshi Yoshimoto <negachov@gmail.com>',
                                                'Stefan Hornburg (Racke) <racke@linuxia.de>',
                                                'Valentin Henon <valentin.henon@gmail.com>',
                                                'Xavier <yadd@debian.org>',
                                                'Xavier Guimard <x.guimard@free.fr>',
                                                'azuma, kuniyuki <azumakuniyuki@users.noreply.github.com>',
                                                'azumakuniyuki <azumakuniyuki+github.com@gmail.com>',
                                                'azumakuniyuki <azumakuniyuki@gmail.com>',
                                                'azumakuniyuki <perl.org@azumakuniyuki.org>',
                                                'batarian71 <36008612+batarian71@users.noreply.github.com>',
                                                'jcbf <underspell@gmail.com>',
                                                'root <root@bwt-1.squaremx.com>'
                                              ],
                          'x_serialization_backend' => 'JSON::PP version 4.02',
                          'x_static_install' => 1
                        },
          'name' => 'Sisimai-v5.0.1',
          'provides' => [
                          'Sisimai',
                          'Sisimai::ARF',
                          'Sisimai::Address',
                          'Sisimai::DateTime',
                          'Sisimai::Fact',
                          'Sisimai::Fact::JSON',
                          'Sisimai::Fact::YAML',
                          'Sisimai::Lhost',
                          'Sisimai::Lhost::Activehunter',
                          'Sisimai::Lhost::Amavis',
                          'Sisimai::Lhost::AmazonSES',
                          'Sisimai::Lhost::AmazonWorkMail',
                          'Sisimai::Lhost::Aol',
                          'Sisimai::Lhost::ApacheJames',
                          'Sisimai::Lhost::Barracuda',
                          'Sisimai::Lhost::Bigfoot',
                          'Sisimai::Lhost::Biglobe',
                          'Sisimai::Lhost::Courier',
                          'Sisimai::Lhost::Domino',
                          'Sisimai::Lhost::EZweb',
                          'Sisimai::Lhost::EinsUndEins',
                          'Sisimai::Lhost::Exchange2003',
                          'Sisimai::Lhost::Exchange2007',
                          'Sisimai::Lhost::Exim',
                          'Sisimai::Lhost::FML',
                          'Sisimai::Lhost::Facebook',
                          'Sisimai::Lhost::GMX',
                          'Sisimai::Lhost::GSuite',
                          'Sisimai::Lhost::Gmail',
                          'Sisimai::Lhost::GoogleGroups',
                          'Sisimai::Lhost::IMailServer',
                          'Sisimai::Lhost::InterScanMSS',
                          'Sisimai::Lhost::KDDI',
                          'Sisimai::Lhost::MXLogic',
                          'Sisimai::Lhost::MailFoundry',
                          'Sisimai::Lhost::MailMarshalSMTP',
                          'Sisimai::Lhost::MailRu',
                          'Sisimai::Lhost::McAfee',
                          'Sisimai::Lhost::MessageLabs',
                          'Sisimai::Lhost::MessagingServer',
                          'Sisimai::Lhost::Notes',
                          'Sisimai::Lhost::Office365',
                          'Sisimai::Lhost::OpenSMTPD',
                          'Sisimai::Lhost::Outlook',
                          'Sisimai::Lhost::Postfix',
                          'Sisimai::Lhost::PowerMTA',
                          'Sisimai::Lhost::ReceivingSES',
                          'Sisimai::Lhost::SendGrid',
                          'Sisimai::Lhost::Sendmail',
                          'Sisimai::Lhost::SurfControl',
                          'Sisimai::Lhost::V5sendmail',
                          'Sisimai::Lhost::Verizon',
                          'Sisimai::Lhost::X1',
                          'Sisimai::Lhost::X2',
                          'Sisimai::Lhost::X3',
                          'Sisimai::Lhost::X4',
                          'Sisimai::Lhost::X5',
                          'Sisimai::Lhost::X6',
                          'Sisimai::Lhost::Yahoo',
                          'Sisimai::Lhost::Yandex',
                          'Sisimai::Lhost::Zoho',
                          'Sisimai::Lhost::mFILTER',
                          'Sisimai::Lhost::qmail',
                          'Sisimai::MDA',
                          'Sisimai::Mail',
                          'Sisimai::Mail::Maildir',
                          'Sisimai::Mail::Mbox',
                          'Sisimai::Mail::Memory',
                          'Sisimai::Mail::STDIN',
                          'Sisimai::Message',
                          'Sisimai::Order',
                          'Sisimai::RFC1894',
                          'Sisimai::RFC2045',
                          'Sisimai::RFC3464',
                          'Sisimai::RFC3834',
                          'Sisimai::RFC5322',
                          'Sisimai::RFC5965',
                          'Sisimai::Reason',
                          'Sisimai::Reason::AuthFailure',
                          'Sisimai::Reason::BadReputation',
                          'Sisimai::Reason::Blocked',
                          'Sisimai::Reason::ContentError',
                          'Sisimai::Reason::Delivered',
                          'Sisimai::Reason::ExceedLimit',
                          'Sisimai::Reason::Expired',
                          'Sisimai::Reason::Feedback',
                          'Sisimai::Reason::Filtered',
                          'Sisimai::Reason::HasMoved',
                          'Sisimai::Reason::HostUnknown',
                          'Sisimai::Reason::MailboxFull',
                          'Sisimai::Reason::MailerError',
                          'Sisimai::Reason::MesgTooBig',
                          'Sisimai::Reason::NetworkError',
                          'Sisimai::Reason::NoRelaying',
                          'Sisimai::Reason::NotAccept',
                          'Sisimai::Reason::NotCompliantRFC',
                          'Sisimai::Reason::OnHold',
                          'Sisimai::Reason::PolicyViolation',
                          'Sisimai::Reason::Rejected',
                          'Sisimai::Reason::RequirePTR',
                          'Sisimai::Reason::SecurityError',
                          'Sisimai::Reason::SpamDetected',
                          'Sisimai::Reason::Speeding',
                          'Sisimai::Reason::Suspend',
                          'Sisimai::Reason::SyntaxError',
                          'Sisimai::Reason::SystemError',
                          'Sisimai::Reason::SystemFull',
                          'Sisimai::Reason::TooManyConn',
                          'Sisimai::Reason::Undefined',
                          'Sisimai::Reason::UserUnknown',
                          'Sisimai::Reason::Vacation',
                          'Sisimai::Reason::VirusDetected',
                          'Sisimai::Rhost',
                          'Sisimai::Rhost::Cox',
                          'Sisimai::Rhost::FrancePTT',
                          'Sisimai::Rhost::GoDaddy',
                          'Sisimai::Rhost::Google',
                          'Sisimai::Rhost::IUA',
                          'Sisimai::Rhost::KDDI',
                          'Sisimai::Rhost::Microsoft',
                          'Sisimai::Rhost::Mimecast',
                          'Sisimai::Rhost::NTTDOCOMO',
                          'Sisimai::Rhost::Spectrum',
                          'Sisimai::Rhost::Tencent',
                          'Sisimai::SMTP',
                          'Sisimai::SMTP::Command',
                          'Sisimai::SMTP::Error',
                          'Sisimai::SMTP::Reply',
                          'Sisimai::SMTP::Status',
                          'Sisimai::SMTP::Transcript',
                          'Sisimai::String',
                          'Sisimai::Time'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/sisimai/p5-sisimai/issues'
                                           },
                           'homepage' => 'https://libsisimai.org/',
                           'repository' => {
                                             'type' => 'git',
                                             'url' => 'git://github.com/sisimai/p5-sisimai.git',
                                             'web' => 'https://github.com/sisimai/p5-sisimai'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709456786,
                      'size' => 1004813
                    },
          'status' => 'latest',
          'version' => 'v5.0.1',
          'version_numified' => '5.000001'
        };
1;
