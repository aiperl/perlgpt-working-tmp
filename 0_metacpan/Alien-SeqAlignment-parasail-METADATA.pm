$distribution = $VAR1 = {
          '_id' => 'vmioeCecMxCvNeDkGgVSJGjDzro',
          '_type' => 'release',
          'abstract' => 'Provide the static, dynamic libraries and CLI of the parasail aligner',
          'archive' => 'Alien-SeqAlignment-parasail-0.03.tar.gz',
          'author' => 'CHRISARG',
          'changes_file' => 'Changes',
          'checksum_md5' => 'f2ee3eeaabae6d02df9ccf51e85ffe67',
          'checksum_sha256' => '65ba52d8a875d8b575ab3402ad4636914bf7f11665b7222e992a850802818ad0',
          'date' => '2024-03-03T05:40:08',
          'dependency' => [
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '6.52'
                            },
                            {
                              'module' => 'Alien::Build::MM',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0.32'
                            },
                            {
                              'module' => 'Alien::Build',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0.32'
                            },
                            {
                              'module' => 'parent',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'strict',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Alien::Base',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'warnings',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Perl::Critic',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'English',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'IO::Handle',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test2::V0',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Pod',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1.41'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '5.006'
                            },
                            {
                              'module' => 'File::Spec',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'IPC::Open3',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'CPAN::Meta',
                              'phase' => 'test',
                              'relationship' => 'recommends',
                              'version' => '2.120900'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '6.52'
                            },
                            {
                              'module' => 'Test2::V0',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Alien::Diag',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Spec',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Alien',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Alien::Build::MM',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0.32'
                            },
                            {
                              'module' => 'Alien::Build',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '1.40'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '6.52'
                            }
                          ],
          'distribution' => 'Alien-SeqAlignment-parasail',
          'download_url' => 'https://cpan.metacpan.org/authors/id/C/CH/CHRISARG/Alien-SeqAlignment-parasail-0.03.tar.gz',
          'id' => 'vmioeCecMxCvNeDkGgVSJGjDzro',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'Alien::SeqAlignment::parasail',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Provide the static, dynamic libraries and CLI of the parasail aligner',
                          'author' => [
                                        'Christos Argyropoulos <chrisarg@gmail.com>'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'Dist::Zilla version 6.030, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Alien-SeqAlignment-parasail',
                          'no_index' => {
                                          'directory' => [
                                                           'eg',
                                                           'examples',
                                                           'inc',
                                                           'share',
                                                           't',
                                                           'xt',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'Alien::Build' => '0.32',
                                                                      'Alien::Build::MM' => '0.32',
                                                                      'ExtUtils::MakeMaker' => '6.52'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'Alien::Build' => '1.40',
                                                                          'Alien::Build::MM' => '0.32',
                                                                          'ExtUtils::MakeMaker' => '6.52'
                                                                        }
                                                        },
                                         'develop' => {
                                                        'requires' => {
                                                                        'English' => '0',
                                                                        'File::Spec' => '0',
                                                                        'IO::Handle' => '0',
                                                                        'IPC::Open3' => '0',
                                                                        'Test2::V0' => '0',
                                                                        'Test::More' => '0',
                                                                        'Test::Perl::Critic' => '0',
                                                                        'Test::Pod' => '1.41',
                                                                        'perl' => '5.006'
                                                                      }
                                                      },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'Alien::Base' => '0',
                                                                        'parent' => '0',
                                                                        'strict' => '0',
                                                                        'warnings' => '0'
                                                                      }
                                                      },
                                         'test' => {
                                                     'recommends' => {
                                                                       'CPAN::Meta' => '2.120900'
                                                                     },
                                                     'requires' => {
                                                                     'ExtUtils::MakeMaker' => '6.52',
                                                                     'File::Spec' => '0',
                                                                     'Test2::V0' => '0',
                                                                     'Test::Alien' => '0',
                                                                     'Test::Alien::Diag' => '0',
                                                                     'Test::More' => '0'
                                                                   }
                                                   }
                                       },
                          'provides' => {
                                          'Alien::SeqAlignment::parasail' => {
                                                                               'file' => 'lib/Alien/SeqAlignment/parasail.pm',
                                                                               'version' => '0.03'
                                                                             }
                                        },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/chrisarg/Alien-SeqAlignment-parasail/issues'
                                                           },
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'git://github.com/chrisarg/Alien-SeqAlignment-parasail.git',
                                                             'web' => 'https://github.com/chrisarg/Alien-SeqAlignment-parasail'
                                                           }
                                         },
                          'version' => '0.03',
                          'x_alienfile' => {
                                             'generated_by' => 'Dist::Zilla::Plugin::AlienBuild version 0.32',
                                             'requires' => {
                                                             'share' => {
                                                                          'Alien::Autotools' => '0',
                                                                          'Alien::Build::Plugin::Gather::IsolateDynamic' => '0.48',
                                                                          'Config' => '0',
                                                                          'HTTP::Tiny' => '0.044',
                                                                          'IO::Socket::SSL' => '1.56',
                                                                          'Mozilla::CA' => '0',
                                                                          'Net::SSLeay' => '1.49',
                                                                          'URI' => '0'
                                                                        },
                                                             'system' => {}
                                                           }
                                           },
                          'x_generated_by_perl' => 'v5.38.0',
                          'x_serialization_backend' => 'Cpanel::JSON::XS version 4.37',
                          'x_spdx_expression' => 'Artistic-1.0-Perl OR GPL-1.0-or-later'
                        },
          'name' => 'Alien-SeqAlignment-parasail-0.03',
          'provides' => [
                          'Alien::SeqAlignment::parasail'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/chrisarg/Alien-SeqAlignment-parasail/issues'
                                           },
                           'repository' => {
                                             'type' => 'git',
                                             'url' => 'git://github.com/chrisarg/Alien-SeqAlignment-parasail.git',
                                             'web' => 'https://github.com/chrisarg/Alien-SeqAlignment-parasail'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709444408,
                      'size' => 17057
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 0,
                       'pass' => 6,
                       'unknown' => 33
                     },
          'version' => '0.03',
          'version_numified' => '0.03'
        };
1;
