$distribution = $VAR1 = {
          '_id' => '95X3ZqOKJwzrt1c9YpV972YpcAA',
          '_type' => 'release',
          'abstract' => 'libvirt Perl API',
          'archive' => 'Sys-Virt-v10.1.0.tar.gz',
          'author' => 'DANBERR',
          'changes_file' => 'Changes',
          'checksum_md5' => '6d9a60215aa3cea44cd67a0cc5ea1ee1',
          'checksum_sha256' => '478f1a2f7ebe39c87e3e75512fdade7da1642f80af08b47a7c3c508814d116be',
          'date' => '2024-03-01T17:53:25',
          'dependency' => [
                            {
                              'module' => 'Test::Pod',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ExtUtils::CBuilder',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'XML::XPath::XMLParser',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::CPAN::Changes',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Sys::Hostname',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Time::HiRes',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Pod::Coverage',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'XML::XPath',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Module::Build',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => 'v5.16.0'
                            }
                          ],
          'distribution' => 'Sys-Virt',
          'download_url' => 'https://cpan.metacpan.org/authors/id/D/DA/DANBERR/Sys-Virt-v10.1.0.tar.gz',
          'id' => '95X3ZqOKJwzrt1c9YpV972YpcAA',
          'license' => [
                         'gpl_1'
                       ],
          'main_module' => 'Sys::Virt',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'libvirt Perl API',
                          'author' => [
                                        'Daniel Berrange <dan@berrange.com>'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'Module::Build version 0.4232, CPAN::Meta::Converter version 2.150005',
                          'license' => [
                                         'gpl_1'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Sys-Virt',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'ExtUtils::CBuilder' => '0',
                                                                      'Sys::Hostname' => '0',
                                                                      'Test::CPAN::Changes' => '0',
                                                                      'Test::More' => '0',
                                                                      'Test::Pod' => '0',
                                                                      'Test::Pod::Coverage' => '0',
                                                                      'Time::HiRes' => '0',
                                                                      'XML::XPath' => '0',
                                                                      'XML::XPath::XMLParser' => '0'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'Module::Build' => '0'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'perl' => 'v5.16.0'
                                                                      }
                                                      }
                                       },
                          'provides' => {
                                          'Sys::Virt' => {
                                                           'file' => 'lib/Sys/Virt.pm',
                                                           'version' => 'v10.1.0'
                                                         },
                                          'Sys::Virt::Domain' => {
                                                                   'file' => 'lib/Sys/Virt/Domain.pm'
                                                                 },
                                          'Sys::Virt::DomainCheckpoint' => {
                                                                             'file' => 'lib/Sys/Virt/DomainCheckpoint.pm'
                                                                           },
                                          'Sys::Virt::DomainSnapshot' => {
                                                                           'file' => 'lib/Sys/Virt/DomainSnapshot.pm'
                                                                         },
                                          'Sys::Virt::Error' => {
                                                                  'file' => 'lib/Sys/Virt/Error.pm'
                                                                },
                                          'Sys::Virt::Event' => {
                                                                  'file' => 'lib/Sys/Virt/Event.pm'
                                                                },
                                          'Sys::Virt::EventImpl' => {
                                                                      'file' => 'lib/Sys/Virt/EventImpl.pm'
                                                                    },
                                          'Sys::Virt::Interface' => {
                                                                      'file' => 'lib/Sys/Virt/Interface.pm'
                                                                    },
                                          'Sys::Virt::NWFilter' => {
                                                                     'file' => 'lib/Sys/Virt/NWFilter.pm'
                                                                   },
                                          'Sys::Virt::NWFilterBinding' => {
                                                                            'file' => 'lib/Sys/Virt/NWFilterBinding.pm'
                                                                          },
                                          'Sys::Virt::Network' => {
                                                                    'file' => 'lib/Sys/Virt/Network.pm'
                                                                  },
                                          'Sys::Virt::NetworkPort' => {
                                                                        'file' => 'lib/Sys/Virt/NetworkPort.pm'
                                                                      },
                                          'Sys::Virt::NodeDevice' => {
                                                                       'file' => 'lib/Sys/Virt/NodeDevice.pm'
                                                                     },
                                          'Sys::Virt::Secret' => {
                                                                   'file' => 'lib/Sys/Virt/Secret.pm'
                                                                 },
                                          'Sys::Virt::StoragePool' => {
                                                                        'file' => 'lib/Sys/Virt/StoragePool.pm'
                                                                      },
                                          'Sys::Virt::StorageVol' => {
                                                                       'file' => 'lib/Sys/Virt/StorageVol.pm'
                                                                     },
                                          'Sys::Virt::Stream' => {
                                                                   'file' => 'lib/Sys/Virt/Stream.pm'
                                                                 }
                                        },
                          'release_status' => 'stable',
                          'resources' => {
                                           'homepage' => 'http://libvirt.org/',
                                           'license' => [
                                                          'http://www.gnu.org/licenses/gpl.html'
                                                        ],
                                           'repository' => {
                                                             'url' => 'https://gitlab.com/libvirt/libvirt-perl'
                                                           },
                                           'x_MailingList' => 'http://www.redhat.com/mailman/listinfo/libvir-list'
                                         },
                          'version' => 'v10.1.0',
                          'x_serialization_backend' => 'JSON::PP version 4.16'
                        },
          'name' => 'Sys-Virt-v10.1.0',
          'provides' => [
                          'Sys::Virt',
                          'Sys::Virt::Domain',
                          'Sys::Virt::DomainCheckpoint',
                          'Sys::Virt::DomainSnapshot',
                          'Sys::Virt::Error',
                          'Sys::Virt::Event',
                          'Sys::Virt::EventImpl',
                          'Sys::Virt::Interface',
                          'Sys::Virt::NWFilter',
                          'Sys::Virt::NWFilterBinding',
                          'Sys::Virt::Network',
                          'Sys::Virt::NetworkPort',
                          'Sys::Virt::NodeDevice',
                          'Sys::Virt::Secret',
                          'Sys::Virt::StoragePool',
                          'Sys::Virt::StorageVol',
                          'Sys::Virt::Stream'
                        ],
          'resources' => {
                           'homepage' => 'http://libvirt.org/',
                           'license' => [
                                          'http://www.gnu.org/licenses/gpl.html'
                                        ],
                           'repository' => {
                                             'url' => 'https://gitlab.com/libvirt/libvirt-perl'
                                           }
                         },
          'stat' => {
                      'mode' => 33204,
                      'mtime' => 1709315605,
                      'size' => 175345
                    },
          'status' => 'latest',
          'version' => 'v10.1.0',
          'version_numified' => '10.001'
        };
1;
