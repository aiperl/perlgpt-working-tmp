$distribution = $VAR1 = {
          '_id' => 'GOaJDgpP5tqtnf3yjBQSnV1ylnw',
          '_type' => 'release',
          'abstract' => 'spawn a local HTTP server for testing',
          'archive' => 'Test-HTTP-LocalServer-0.76.tar.gz',
          'author' => 'CORION',
          'changes_file' => 'Changes',
          'checksum_md5' => '23552703814fd58b49a56f5b0b0bdcc0',
          'checksum_sha256' => 'b3c9c2dde0085c49c34fa6bf6aef9e615b95a5bf578b467c8fef7d1ac5320a1f',
          'date' => '2024-03-03T18:45:19',
          'dependency' => [
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'HTTP::Daemon',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '6.05'
                            },
                            {
                              'module' => 'File::Temp',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Cwd',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Socket',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'HTTP::Response',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Basename',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Spec',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'CGI',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'URI::URL',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Carp',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Getopt::Long',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'IO::Socket::INET',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'HTTP::Tiny',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'URI',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'IO::Socket::IP',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.25'
                            },
                            {
                              'module' => 'Time::HiRes',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '5.008'
                            },
                            {
                              'module' => 'HTTP::Request::AsCGI',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Copy',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Basename',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Path',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Find',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            }
                          ],
          'distribution' => 'Test-HTTP-LocalServer',
          'download_url' => 'https://cpan.metacpan.org/authors/id/C/CO/CORION/Test-HTTP-LocalServer-0.76.tar.gz',
          'id' => 'GOaJDgpP5tqtnf3yjBQSnV1ylnw',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'Test::HTTP::LocalServer',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'spawn a local HTTP server for testing',
                          'author' => [
                                        'Max Maischein <corion@cpan.org>'
                                      ],
                          'dynamic_config' => 0,
                          'generated_by' => 'ExtUtils::MakeMaker version 7.64, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Test-HTTP-LocalServer',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'ExtUtils::MakeMaker' => '0',
                                                                      'File::Basename' => '0',
                                                                      'File::Copy' => '0',
                                                                      'File::Find' => '0',
                                                                      'File::Path' => '0'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'ExtUtils::MakeMaker' => '0'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'CGI' => '0',
                                                                        'Carp' => '0',
                                                                        'Cwd' => '0',
                                                                        'File::Basename' => '0',
                                                                        'File::Spec' => '0',
                                                                        'File::Temp' => '0',
                                                                        'Getopt::Long' => '0',
                                                                        'HTTP::Daemon' => '6.05',
                                                                        'HTTP::Request::AsCGI' => '0',
                                                                        'HTTP::Response' => '0',
                                                                        'HTTP::Tiny' => '0',
                                                                        'IO::Socket::INET' => '0',
                                                                        'IO::Socket::IP' => '0.25',
                                                                        'Socket' => '0',
                                                                        'Time::HiRes' => '0',
                                                                        'URI' => '0',
                                                                        'URI::URL' => '0',
                                                                        'perl' => '5.008'
                                                                      }
                                                      },
                                         'test' => {
                                                     'requires' => {
                                                                     'Test::More' => '0'
                                                                   }
                                                   }
                                       },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/Corion/Test-HTTP-LocalServer/issues'
                                                           },
                                           'license' => [
                                                          'https://dev.perl.org/licenses/'
                                                        ],
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'git://github.com/Corion/Test-HTTP-LocalServer.git',
                                                             'web' => 'https://github.com/Corion/Test-HTTP-LocalServer'
                                                           }
                                         },
                          'version' => '0.76',
                          'x_serialization_backend' => 'JSON::PP version 4.07',
                          'x_static_install' => 0
                        },
          'name' => 'Test-HTTP-LocalServer-0.76',
          'provides' => [
                          'Test::HTTP::LocalServer'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/Corion/Test-HTTP-LocalServer/issues'
                                           },
                           'license' => [
                                          'https://dev.perl.org/licenses/'
                                        ],
                           'repository' => {
                                             'type' => 'git',
                                             'url' => 'git://github.com/Corion/Test-HTTP-LocalServer.git',
                                             'web' => 'https://github.com/Corion/Test-HTTP-LocalServer'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709491519,
                      'size' => 31255
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 0,
                       'pass' => 61,
                       'unknown' => 0
                     },
          'version' => '0.76',
          'version_numified' => '0.76'
        };
1;
