$distribution = $VAR1 = {
          '_id' => 'eLLim_04f49n0Xrz935BDcyTuhI',
          '_type' => 'release',
          'abstract' => 'Perl Archive Toolkit',
          'archive' => 'PAR-1.020.tar.gz',
          'author' => 'RSCHUPP',
          'changes_file' => 'Changes',
          'checksum_md5' => '62a8d843d726ee046a59bf01961d46f7',
          'checksum_sha256' => '6654537fe98eb7dafc341aae550507b8a834a9dff7893afec1aafe95ad1fb769',
          'date' => '2024-03-04T10:49:29',
          'dependency' => [
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Digest::SHA',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '5.45'
                            },
                            {
                              'module' => 'AutoLoader',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '5.66_02'
                            },
                            {
                              'module' => 'Archive::Zip',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.00'
                            },
                            {
                              'module' => 'Compress::Zlib',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.30'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '5.008009'
                            },
                            {
                              'module' => 'File::Temp',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.05'
                            },
                            {
                              'module' => 'PAR::Dist',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.32'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            }
                          ],
          'distribution' => 'PAR',
          'download_url' => 'https://cpan.metacpan.org/authors/id/R/RS/RSCHUPP/PAR-1.020.tar.gz',
          'id' => 'eLLim_04f49n0Xrz935BDcyTuhI',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'PAR',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Perl Archive Toolkit',
                          'author' => [
                                        'Audrey Tang <cpan@audreyt.org>'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'ExtUtils::MakeMaker version 7.70, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'PAR',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'ExtUtils::MakeMaker' => '0'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'ExtUtils::MakeMaker' => '0'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'Archive::Zip' => '1.00',
                                                                        'AutoLoader' => '5.66_02',
                                                                        'Compress::Zlib' => '1.30',
                                                                        'Digest::SHA' => '5.45',
                                                                        'File::Temp' => '0.05',
                                                                        'PAR::Dist' => '0.32',
                                                                        'perl' => '5.008009'
                                                                      }
                                                      },
                                         'test' => {
                                                     'requires' => {
                                                                     'Test::More' => '0'
                                                                   }
                                                   }
                                       },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/rschupp/PAR/issues'
                                                           },
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'git://github.com/rschupp/PAR.git',
                                                             'web' => 'https://github.com/rschupp/PAR'
                                                           },
                                           'x_MailingList' => 'mailto:par@perl.org'
                                         },
                          'version' => '1.020',
                          'x_serialization_backend' => 'JSON::PP version 4.16'
                        },
          'name' => 'PAR-1.020',
          'provides' => [
                          'PAR',
                          'PAR::Heavy',
                          'PAR::SetupProgname',
                          'PAR::SetupTemp'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/rschupp/PAR/issues'
                                           },
                           'repository' => {
                                             'type' => 'git',
                                             'url' => 'git://github.com/rschupp/PAR.git',
                                             'web' => 'https://github.com/rschupp/PAR'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709549369,
                      'size' => 66318
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 0,
                       'pass' => 56,
                       'unknown' => 0
                     },
          'version' => '1.020',
          'version_numified' => '1.02'
        };
1;
