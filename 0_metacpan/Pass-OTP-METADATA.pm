$distribution = $VAR1 = {
          '_id' => 'uYijeHi8pF98fEX8wRGhBDzY9kU',
          '_type' => 'release',
          'abstract' => 'Perl implementation of HOTP / TOTP algorithms',
          'archive' => 'Pass-OTP-1.7.tar.gz',
          'author' => 'JBAIER',
          'changes_file' => 'Changes',
          'checksum_md5' => '952ef9b2d1aabf2a32852a0aa30821c9',
          'checksum_sha256' => 'ead0598b94d766c877713104a0f2854e8b2092bcbd9be66ee50decddef8cf878',
          'date' => '2024-03-02T13:11:36',
          'dependency' => [
                            {
                              'module' => 'Test::More',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Digest::HMAC',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Digest::SHA',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Math::BigInt',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.999806'
                            },
                            {
                              'module' => 'Convert::Base32',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '5.014'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            }
                          ],
          'distribution' => 'Pass-OTP',
          'download_url' => 'https://cpan.metacpan.org/authors/id/J/JB/JBAIER/Pass-OTP-1.7.tar.gz',
          'id' => 'uYijeHi8pF98fEX8wRGhBDzY9kU',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'Pass::OTP',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Perl implementation of HOTP / TOTP algorithms',
                          'author' => [
                                        'Jan Baier <jan.baier@amagical.net>'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'ExtUtils::MakeMaker version 7.70, CPAN::Meta::Converter version 2.150010, CPAN::Meta::Converter version 2.150005',
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Pass-OTP',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'ExtUtils::MakeMaker' => '0'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'ExtUtils::MakeMaker' => '0'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'Convert::Base32' => '0',
                                                                        'Digest::HMAC' => '0',
                                                                        'Digest::SHA' => '0',
                                                                        'Math::BigInt' => '1.999806',
                                                                        'perl' => '5.014'
                                                                      }
                                                      },
                                         'test' => {
                                                     'requires' => {
                                                                     'Test::More' => '0'
                                                                   }
                                                   }
                                       },
                          'release_status' => 'stable',
                          'version' => '1.7',
                          'x_serialization_backend' => 'JSON::PP version 4.16'
                        },
          'name' => 'Pass-OTP-1.7',
          'provides' => [
                          'Pass::OTP',
                          'Pass::OTP::URI'
                        ],
          'resources' => {},
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709385096,
                      'size' => 6796
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 2,
                       'pass' => 93,
                       'unknown' => 0
                     },
          'version' => '1.7',
          'version_numified' => '1.7'
        };
1;
