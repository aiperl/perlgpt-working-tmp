$distribution = $VAR1 = {
          '_id' => 'RaMdaQsYZ4nGHeerkJ_xKdPc1c0',
          '_type' => 'release',
          'abstract' => 'Spanish holidays',
          'archive' => 'Date-Holidays-ES-0.03.tar.gz',
          'author' => 'JONASBN',
          'changes_file' => 'CHANGELOG.md',
          'checksum_md5' => 'f863d7bc75b7f0cb0c3eab9e2aa25b5f',
          'checksum_sha256' => 'fcab6b793893540779c12e7f8718cbf65342f705771d326fa33be4c8f5f675d1',
          'date' => '2024-03-03T13:57:16',
          'dependency' => [
                            {
                              'module' => 'IO::Handle',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'IPC::Open3',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0.4234'
                            },
                            {
                              'module' => 'File::Spec',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Module::Build',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0.30'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0.4234'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Module::Build',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0.30'
                            },
                            {
                              'module' => 'Time::JulianDay',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'DateTime',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Date::Holidays::Super',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Date::Easter',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Perl::Critic',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Pod::Coverage::TrustPod',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Pod::Coverage',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1.08'
                            },
                            {
                              'module' => 'Test::Kwalitee',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1.21'
                            },
                            {
                              'module' => 'Test::Pod',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1.41'
                            },
                            {
                              'module' => 'Test::CPAN::Meta::JSON',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.16'
                            }
                          ],
          'distribution' => 'Date-Holidays-ES',
          'download_url' => 'https://cpan.metacpan.org/authors/id/J/JO/JONASBN/Date-Holidays-ES-0.03.tar.gz',
          'id' => 'RaMdaQsYZ4nGHeerkJ_xKdPc1c0',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'Date::Holidays::ES',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Spanish holidays',
                          'author' => [
                                        'Jonas B. <jonasbn@cpan.org>'
                                      ],
                          'dynamic_config' => 0,
                          'generated_by' => 'Dist::Zilla version 6.031, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Date-Holidays-ES',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'Module::Build' => '0.30',
                                                                      'Test::More' => '0.4234'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'ExtUtils::MakeMaker' => '0',
                                                                          'Module::Build' => '0.30'
                                                                        }
                                                        },
                                         'develop' => {
                                                        'requires' => {
                                                                        'Pod::Coverage::TrustPod' => '0',
                                                                        'Test::CPAN::Meta::JSON' => '0.16',
                                                                        'Test::Kwalitee' => '1.21',
                                                                        'Test::Perl::Critic' => '0',
                                                                        'Test::Pod' => '1.41',
                                                                        'Test::Pod::Coverage' => '1.08'
                                                                      }
                                                      },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'Date::Easter' => '0',
                                                                        'Date::Holidays::Super' => '0',
                                                                        'DateTime' => '0',
                                                                        'Time::JulianDay' => '0'
                                                                      }
                                                      },
                                         'test' => {
                                                     'requires' => {
                                                                     'File::Spec' => '0',
                                                                     'IO::Handle' => '0',
                                                                     'IPC::Open3' => '0',
                                                                     'Test::More' => '0.4234'
                                                                   }
                                                   }
                                       },
                          'provides' => {
                                          'Date::Holidays::ES' => {
                                                                    'file' => 'lib/Date/Holidays/ES.pm',
                                                                    'version' => '0.03'
                                                                  }
                                        },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/jonasbn/Date-Holidays-ES/issues'
                                                           },
                                           'homepage' => 'https://github.com/jonasbn/Date-Holidays-ES',
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'https://github.com/jonasbn/Date-Holidays-ES.git',
                                                             'web' => 'https://github.com/jonasbn/Date-Holidays-ES'
                                                           }
                                         },
                          'version' => '0.03',
                          'x_generated_by_perl' => 'v5.38.0',
                          'x_serialization_backend' => 'Cpanel::JSON::XS version 4.37',
                          'x_spdx_expression' => 'Artistic-1.0-Perl OR GPL-1.0-or-later'
                        },
          'name' => 'Date-Holidays-ES-0.03',
          'provides' => [
                          'Date::Holidays::ES'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/jonasbn/Date-Holidays-ES/issues'
                                           },
                           'homepage' => 'https://github.com/jonasbn/Date-Holidays-ES',
                           'repository' => {
                                             'type' => 'git',
                                             'url' => 'https://github.com/jonasbn/Date-Holidays-ES.git',
                                             'web' => 'https://github.com/jonasbn/Date-Holidays-ES'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709474236,
                      'size' => 14272
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 0,
                       'pass' => 59,
                       'unknown' => 0
                     },
          'version' => '0.03',
          'version_numified' => '0.03'
        };
1;
