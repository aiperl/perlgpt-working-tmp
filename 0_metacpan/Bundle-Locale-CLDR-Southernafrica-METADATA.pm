$distribution = $VAR1 = {
          '_id' => 'nf84mQMyTKKngRFOcKXelnm0oh4',
          '_type' => 'release',
          'abstract' => 'Locale::CLDR - Data Package (  )',
          'archive' => 'Bundle-Locale-CLDR-Southernafrica-0.44.1.tar.gz',
          'author' => 'JGNI',
          'changes_file' => '',
          'checksum_md5' => '0a4a70aae32923d94e567c3a7bc0a730',
          'checksum_sha256' => '4c25c67ed2cc4effbf9b0bd29d69f910dd59b10c776af25b5d6f0bffa759970a',
          'date' => '2024-03-01T06:52:10',
          'dependency' => [
                            {
                              'module' => 'Module::Build',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0.40'
                            },
                            {
                              'module' => 'DateTime',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.72'
                            },
                            {
                              'module' => 'Type::Tiny',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => 'v5.10.1'
                            },
                            {
                              'module' => 'Moo',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '2'
                            },
                            {
                              'module' => 'MooX::ClassAttribute',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.011'
                            },
                            {
                              'module' => 'version',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.95'
                            },
                            {
                              'module' => 'ok',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Exception',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0.98'
                            }
                          ],
          'distribution' => 'Bundle-Locale-CLDR-Southernafrica',
          'download_url' => 'https://cpan.metacpan.org/authors/id/J/JG/JGNI/Bundle-Locale-CLDR-Southernafrica-0.44.1.tar.gz',
          'id' => 'nf84mQMyTKKngRFOcKXelnm0oh4',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'Bundle::Locale::CLDR::Southernafrica',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Locale::CLDR - Data Package (  )',
                          'author' => [
                                        'John Imrie <john.imrie1@gmail.com>'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'Module::Build version 0.4231, CPAN::Meta::Converter version 2.150010',
                          'keywords' => [
                                          'locale',
                                          'CLDR',
                                          'locale-data-pack'
                                        ],
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Bundle-Locale-CLDR-Southernafrica',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'Test::Exception' => '0',
                                                                      'Test::More' => '0.98',
                                                                      'ok' => '0'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'Module::Build' => '0.40'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'DateTime' => '0.72',
                                                                        'Moo' => '2',
                                                                        'MooX::ClassAttribute' => '0.011',
                                                                        'Type::Tiny' => '0',
                                                                        'perl' => 'v5.10.1',
                                                                        'version' => '0.95'
                                                                      }
                                                      }
                                       },
                          'provides' => {
                                          'Bundle::Locale::CLDR::Southernafrica' => {
                                                                                      'file' => 'lib/Bundle/Locale/CLDR/Southernafrica.pm',
                                                                                      'version' => 'v0.44.1'
                                                                                    }
                                        },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/ThePilgrim/perlcldr/issues'
                                                           },
                                           'homepage' => 'https://github.com/ThePilgrim/perlcldr',
                                           'repository' => {
                                                             'url' => 'https://github.com/ThePilgrim/perlcldr.git'
                                                           }
                                         },
                          'version' => 'v0.44.1',
                          'x_serialization_backend' => 'JSON::PP version 4.06'
                        },
          'name' => 'Bundle-Locale-CLDR-Southernafrica-0.44.1',
          'provides' => [
                          'Bundle::Locale::CLDR::Southernafrica'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/ThePilgrim/perlcldr/issues'
                                           },
                           'homepage' => 'https://github.com/ThePilgrim/perlcldr',
                           'repository' => {
                                             'url' => 'https://github.com/ThePilgrim/perlcldr.git'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709275930,
                      'size' => 2156
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 1,
                       'pass' => 0,
                       'unknown' => 84
                     },
          'version' => 'v0.44.1',
          'version_numified' => '0.044001'
        };
1;
