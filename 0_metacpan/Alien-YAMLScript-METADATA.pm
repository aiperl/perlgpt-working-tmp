$distribution = $VAR1 = {
          '_id' => 'arbYquHbMjCrZ8LP4pFsHU9XxKo',
          '_type' => 'release',
          'abstract' => 'Program in YAML',
          'archive' => 'Alien-YAMLScript-0.1.39.tar.gz',
          'author' => 'INGY',
          'changes_file' => 'Changes',
          'checksum_md5' => 'beb5539af36b9ee23469b5375280592a',
          'checksum_sha256' => 'e9064159f9562a90c42b6d860853768d75ade8e4f75b1554f27297cea59d9b9b',
          'date' => '2024-03-02T16:32:44',
          'dependency' => [
                            {
                              'module' => 'Alien::Build::MM',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0.32'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '6.52'
                            },
                            {
                              'module' => 'Alien::Build',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0.32'
                            },
                            {
                              'module' => 'Test::Pod',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1.41'
                            },
                            {
                              'module' => 'Test2::V0',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0.000159'
                            },
                            {
                              'module' => 'Test::Alien',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '2.8'
                            },
                            {
                              'module' => 'Alien::Build::Plugin::Download::GitHub',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0.10'
                            },
                            {
                              'module' => 'Alien::Build::MM',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0.32'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '6.52'
                            },
                            {
                              'module' => 'Alien::Build',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0.32'
                            },
                            {
                              'module' => 'Cpanel::JSON::XS',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '4.37'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => 'v5.16.0'
                            }
                          ],
          'distribution' => 'Alien-YAMLScript',
          'download_url' => 'https://cpan.metacpan.org/authors/id/I/IN/INGY/Alien-YAMLScript-0.1.39.tar.gz',
          'id' => 'arbYquHbMjCrZ8LP4pFsHU9XxKo',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'Alien::YAMLScript',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Program in YAML',
                          'author' => [
                                        "Ingy d\x{f6}t Net <ingy\@ingy.net>"
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'Dist::Zilla version 6.030, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Alien-YAMLScript',
                          'no_index' => {
                                          'directory' => [
                                                           'example',
                                                           'inc',
                                                           't',
                                                           'xt',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'Alien::Build' => '0.32',
                                                                      'Alien::Build::MM' => '0.32',
                                                                      'ExtUtils::MakeMaker' => '6.52'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'Alien::Build' => '0.32',
                                                                          'Alien::Build::MM' => '0.32',
                                                                          'Alien::Build::Plugin::Download::GitHub' => '0.10',
                                                                          'ExtUtils::MakeMaker' => '6.52'
                                                                        }
                                                        },
                                         'develop' => {
                                                        'requires' => {
                                                                        'Test::Pod' => '1.41'
                                                                      }
                                                      },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'Cpanel::JSON::XS' => '4.37',
                                                                        'perl' => 'v5.16.0'
                                                                      }
                                                      },
                                         'test' => {
                                                     'requires' => {
                                                                     'Test2::V0' => '0.000159',
                                                                     'Test::Alien' => '2.8'
                                                                   }
                                                   }
                                       },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/ingydotnet/yamlscript/issues'
                                                           },
                                           'homepage' => 'https://github.com/ingydotnet/yamlscript',
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'https://github.com/ingydotnet/yamlscript.git',
                                                             'web' => 'https://github.com/ingydotnet/yamlscript'
                                                           }
                                         },
                          'version' => '0.1.39',
                          'x_alienfile' => {
                                             'generated_by' => 'Dist::Zilla::Plugin::AlienBuild version 0.32',
                                             'requires' => {
                                                             'share' => {
                                                                          'Alien::Build::Plugin::Download::Negotiate' => '1.30',
                                                                          'Config' => '0',
                                                                          'HTTP::Tiny' => '0.044',
                                                                          'IO::Socket::SSL' => '1.56',
                                                                          'Mojo::DOM' => '0',
                                                                          'Mojolicious' => '7.00',
                                                                          'Mozilla::CA' => '0',
                                                                          'Net::SSLeay' => '1.49',
                                                                          'URI' => '0',
                                                                          'URI::Escape' => '0'
                                                                        },
                                                             'system' => {}
                                                           }
                                           },
                          'x_generated_by_perl' => 'v5.28.0',
                          'x_serialization_backend' => 'Cpanel::JSON::XS version 4.37',
                          'x_spdx_expression' => 'Artistic-1.0-Perl OR GPL-1.0-or-later'
                        },
          'name' => 'Alien-YAMLScript-0.1.39',
          'provides' => [
                          'Alien::YAMLScript'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/ingydotnet/yamlscript/issues'
                                           },
                           'homepage' => 'https://github.com/ingydotnet/yamlscript',
                           'repository' => {
                                             'type' => 'git',
                                             'url' => 'https://github.com/ingydotnet/yamlscript.git',
                                             'web' => 'https://github.com/ingydotnet/yamlscript'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709397164,
                      'size' => 11621
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 15,
                       'na' => 3,
                       'pass' => 35,
                       'unknown' => 34
                     },
          'version' => '0.1.39',
          'version_numified' => '0.001039'
        };
1;
