$distribution = $VAR1 = {
          '_id' => 'TGldJruCVJ0bp2MXTM224ReJo28',
          '_type' => 'release',
          'abstract' => 'Mo CSS utilities.',
          'archive' => 'Mo-utils-CSS-0.05.tar.gz',
          'author' => 'SKIM',
          'changes_file' => 'Changes',
          'checksum_md5' => 'ddcf2fa162b078f7fb2143f64aa1cc31',
          'checksum_sha256' => '4969b6266f3ab44b61660a7be9e5cf1ad77c20c65d3948411f2137f36a238ea7',
          'date' => '2024-03-04T10:09:03',
          'dependency' => [
                            {
                              'module' => 'English',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '6.59'
                            },
                            {
                              'module' => 'Readonly',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::NoWarnings',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Error::Pure::Utils',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Exporter',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => 'v5.6.2'
                            },
                            {
                              'module' => 'List::Util',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.33'
                            },
                            {
                              'module' => 'Readonly',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Error::Pure',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.15'
                            },
                            {
                              'module' => 'Mo::utils',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.06'
                            },
                            {
                              'module' => 'Graphics::ColorNames::CSS',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '6.59'
                            }
                          ],
          'distribution' => 'Mo-utils-CSS',
          'download_url' => 'https://cpan.metacpan.org/authors/id/S/SK/SKIM/Mo-utils-CSS-0.05.tar.gz',
          'id' => 'TGldJruCVJ0bp2MXTM224ReJo28',
          'license' => [
                         'bsd'
                       ],
          'main_module' => 'Mo::utils::CSS',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Mo CSS utilities.',
                          'author' => [
                                        'Michal Josef Spacek <skim@cpan.org>'
                                      ],
                          'dynamic_config' => '1',
                          'generated_by' => 'Module::Install version 1.21, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'bsd'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Mo-utils-CSS',
                          'no_index' => {
                                          'directory' => [
                                                           'examples',
                                                           'inc',
                                                           't',
                                                           'xt',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'English' => '0',
                                                                      'Error::Pure::Utils' => '0',
                                                                      'ExtUtils::MakeMaker' => '6.59',
                                                                      'Readonly' => '0',
                                                                      'Test::More' => '0',
                                                                      'Test::NoWarnings' => '0'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'ExtUtils::MakeMaker' => '6.59'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'Error::Pure' => '0.15',
                                                                        'Exporter' => '0',
                                                                        'Graphics::ColorNames::CSS' => '0',
                                                                        'List::Util' => '1.33',
                                                                        'Mo::utils' => '0.06',
                                                                        'Readonly' => '0',
                                                                        'perl' => 'v5.6.2'
                                                                      }
                                                      }
                                       },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/michal-josef-spacek/Mo-utils-CSS/issues'
                                                           },
                                           'homepage' => 'https://github.com/michal-josef-spacek/Mo-utils-CSS',
                                           'license' => [
                                                          'http://opensource.org/licenses/bsd-license.php'
                                                        ],
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'git://github.com/michal-josef-spacek/Mo-utils-CSS'
                                                           }
                                         },
                          'version' => '0.05'
                        },
          'name' => 'Mo-utils-CSS-0.05',
          'provides' => [
                          'Mo::utils::CSS'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/michal-josef-spacek/Mo-utils-CSS/issues'
                                           },
                           'homepage' => 'https://github.com/michal-josef-spacek/Mo-utils-CSS',
                           'license' => [
                                          'http://opensource.org/licenses/bsd-license.php'
                                        ],
                           'repository' => {
                                             'url' => 'git://github.com/michal-josef-spacek/Mo-utils-CSS'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709546943,
                      'size' => 29315
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 0,
                       'pass' => 57,
                       'unknown' => 0
                     },
          'version' => '0.05',
          'version_numified' => '0.05'
        };
1;
