$distribution = $VAR1 = {
          '_id' => 'TpWSYBGLfNmsNPqzSFQAbCveiAU',
          '_type' => 'release',
          'abstract' => 'Build and Install the CFITSIO library',
          'archive' => 'Alien-CFITSIO-v4.4.0.0.tar.gz',
          'author' => 'DJERIUS',
          'changes_file' => 'Changes',
          'checksum_md5' => '660f5da00911c65efc52a2c8794fb7a3',
          'checksum_sha256' => 'c399d69ec3e04ef90cde96443319ab49db20f1efbf6b24ae02943553db97ca69',
          'date' => '2024-03-04T17:44:50',
          'dependency' => [
                            {
                              'module' => 'Alien::Build',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '2.3901'
                            },
                            {
                              'module' => 'Alien::Build::MM',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0.32'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '6.52'
                            },
                            {
                              'module' => 'Sort::Versions',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Alien::Build::Plugin::PkgConfig::Negotiate',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '1.53'
                            },
                            {
                              'module' => 'Alien::Build',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0.32'
                            },
                            {
                              'module' => 'Alien::Build::MM',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0.32'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '6.52'
                            },
                            {
                              'module' => 'Test::Alien',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '2.3901'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '5.006'
                            },
                            {
                              'module' => 'Package::Stash',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Spec',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'IO::Handle',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '6.52'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'IPC::Open3',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test2::V0',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'CPAN::Meta',
                              'phase' => 'test',
                              'relationship' => 'recommends',
                              'version' => '2.120900'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::Compile',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Software::License::GPL_3',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::AutoPrereqs',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Regenerate',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::RewriteVersion',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::TrailingSpace',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::NoBreakpoints',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::CopyFilesFromRelease',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Readme::Brief',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Spelling',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.12'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::InsertExample',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::PodWeaver',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::EnsurePrereqsInstalled',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::RunExtraTests',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::GatherDir',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::PluginBundle::Filter',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::DistManifest',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::PodSpelling',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::UnusedVars',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Pod::Weaver::Section::GenerateSection',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::CPAN::Changes',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.19'
                            },
                            {
                              'module' => 'Test::CPAN::Meta::JSON',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.16'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Prereqs::AuthorDeps',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::NoTabs',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::GatherDir::Template',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::FileFinder::ByName',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::Version',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::CleanNamespaces',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.15'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Encoding',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Pod::Coverage',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1.08'
                            },
                            {
                              'module' => 'Alien::Build',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '2.3901'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::InsertCopyright',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::MetaNoIndex',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::NoBreakpoints',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.15'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::ReadmeAnyFromPod',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::TrailingSpace',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.0203'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::MetaProvides::Package',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Pod::Weaver::Plugin::StopWords',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '5'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::PodCoverageTests',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Pod::Coverage::TrustPod',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::PodSyntaxTests',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Perl::Critic',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::CleanNamespaces',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Prereqs',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::BumpVersionAfterRelease',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::MetaJSON',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::CPAN::Changes',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Version',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '2.00'
                            },
                            {
                              'module' => 'Pod::Weaver::Section::Contributors',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::PluginBundle::Basic',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::MetaResources',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Pod::Weaver::Section::SeeAlso',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Pod::Wordlist',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::EnsureChangesHasContent',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Pod',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1.41'
                            },
                            {
                              'module' => 'Alien::Base',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::NextRelease',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::CheckMetaResources',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::NoTabs',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Templates',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::ReportPrereqs',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::CPAN::Meta::JSON',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.96'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::Perl::Critic',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::DistManifest',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::AlienBuild',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Vars',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'warnings',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'base',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Alien::Base',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'constant',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'strict',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            }
                          ],
          'distribution' => 'Alien-CFITSIO',
          'download_url' => 'https://cpan.metacpan.org/authors/id/D/DJ/DJERIUS/Alien-CFITSIO-v4.4.0.0.tar.gz',
          'id' => 'TpWSYBGLfNmsNPqzSFQAbCveiAU',
          'license' => [
                         'gpl_3'
                       ],
          'main_module' => 'Alien::CFITSIO',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Build and Install the CFITSIO library',
                          'author' => [
                                        'Diab Jerius <djerius@cpan.org>'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'Dist::Zilla version 6.031, CPAN::Meta::Converter version 2.150010, CPAN::Meta::Converter version 2.150005',
                          'license' => [
                                         'gpl_3'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Alien-CFITSIO',
                          'no_index' => {
                                          'directory' => [
                                                           'eg',
                                                           'examples',
                                                           'inc',
                                                           'share',
                                                           't',
                                                           'xt',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'Alien::Build' => '0.32',
                                                                      'Alien::Build::MM' => '0.32',
                                                                      'ExtUtils::MakeMaker' => '6.52'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'Alien::Build' => '2.3901',
                                                                          'Alien::Build::MM' => '0.32',
                                                                          'Alien::Build::Plugin::PkgConfig::Negotiate' => '1.53',
                                                                          'ExtUtils::MakeMaker' => '6.52',
                                                                          'Sort::Versions' => '0'
                                                                        }
                                                        },
                                         'develop' => {
                                                        'requires' => {
                                                                        'Alien::Base' => '0',
                                                                        'Alien::Build' => '2.3901',
                                                                        'Dist::Zilla' => '5',
                                                                        'Dist::Zilla::Plugin::AlienBuild' => '0',
                                                                        'Dist::Zilla::Plugin::AutoPrereqs' => '0',
                                                                        'Dist::Zilla::Plugin::BumpVersionAfterRelease' => '0',
                                                                        'Dist::Zilla::Plugin::CheckMetaResources' => '0',
                                                                        'Dist::Zilla::Plugin::CopyFilesFromRelease' => '0',
                                                                        'Dist::Zilla::Plugin::Encoding' => '0',
                                                                        'Dist::Zilla::Plugin::EnsureChangesHasContent' => '0',
                                                                        'Dist::Zilla::Plugin::EnsurePrereqsInstalled' => '0',
                                                                        'Dist::Zilla::Plugin::FileFinder::ByName' => '0',
                                                                        'Dist::Zilla::Plugin::GatherDir' => '0',
                                                                        'Dist::Zilla::Plugin::GatherDir::Template' => '0',
                                                                        'Dist::Zilla::Plugin::InsertCopyright' => '0',
                                                                        'Dist::Zilla::Plugin::InsertExample' => '0',
                                                                        'Dist::Zilla::Plugin::MetaJSON' => '0',
                                                                        'Dist::Zilla::Plugin::MetaNoIndex' => '0',
                                                                        'Dist::Zilla::Plugin::MetaProvides::Package' => '0',
                                                                        'Dist::Zilla::Plugin::MetaResources' => '0',
                                                                        'Dist::Zilla::Plugin::NextRelease' => '0',
                                                                        'Dist::Zilla::Plugin::PodCoverageTests' => '0',
                                                                        'Dist::Zilla::Plugin::PodSyntaxTests' => '0',
                                                                        'Dist::Zilla::Plugin::PodWeaver' => '0',
                                                                        'Dist::Zilla::Plugin::Prereqs' => '0',
                                                                        'Dist::Zilla::Plugin::Prereqs::AuthorDeps' => '0',
                                                                        'Dist::Zilla::Plugin::Readme::Brief' => '0',
                                                                        'Dist::Zilla::Plugin::ReadmeAnyFromPod' => '0',
                                                                        'Dist::Zilla::Plugin::Regenerate' => '0',
                                                                        'Dist::Zilla::Plugin::RewriteVersion' => '0',
                                                                        'Dist::Zilla::Plugin::RunExtraTests' => '0',
                                                                        'Dist::Zilla::Plugin::Templates' => '0',
                                                                        'Dist::Zilla::Plugin::Test::CPAN::Changes' => '0',
                                                                        'Dist::Zilla::Plugin::Test::CPAN::Meta::JSON' => '0',
                                                                        'Dist::Zilla::Plugin::Test::CleanNamespaces' => '0',
                                                                        'Dist::Zilla::Plugin::Test::Compile' => '0',
                                                                        'Dist::Zilla::Plugin::Test::DistManifest' => '0',
                                                                        'Dist::Zilla::Plugin::Test::NoBreakpoints' => '0',
                                                                        'Dist::Zilla::Plugin::Test::NoTabs' => '0',
                                                                        'Dist::Zilla::Plugin::Test::Perl::Critic' => '0',
                                                                        'Dist::Zilla::Plugin::Test::PodSpelling' => '0',
                                                                        'Dist::Zilla::Plugin::Test::ReportPrereqs' => '0',
                                                                        'Dist::Zilla::Plugin::Test::TrailingSpace' => '0',
                                                                        'Dist::Zilla::Plugin::Test::UnusedVars' => '0',
                                                                        'Dist::Zilla::Plugin::Test::Version' => '0',
                                                                        'Dist::Zilla::PluginBundle::Basic' => '0',
                                                                        'Dist::Zilla::PluginBundle::Filter' => '0',
                                                                        'Pod::Coverage::TrustPod' => '0',
                                                                        'Pod::Weaver::Plugin::StopWords' => '0',
                                                                        'Pod::Weaver::Section::Contributors' => '0',
                                                                        'Pod::Weaver::Section::GenerateSection' => '0',
                                                                        'Pod::Weaver::Section::SeeAlso' => '0',
                                                                        'Pod::Wordlist' => '0',
                                                                        'Software::License::GPL_3' => '0',
                                                                        'Test::CPAN::Changes' => '0.19',
                                                                        'Test::CPAN::Meta::JSON' => '0.16',
                                                                        'Test::CleanNamespaces' => '0.15',
                                                                        'Test::DistManifest' => '0',
                                                                        'Test::More' => '0.96',
                                                                        'Test::NoBreakpoints' => '0.15',
                                                                        'Test::NoTabs' => '0',
                                                                        'Test::Perl::Critic' => '0',
                                                                        'Test::Pod' => '1.41',
                                                                        'Test::Pod::Coverage' => '1.08',
                                                                        'Test::Spelling' => '0.12',
                                                                        'Test::TrailingSpace' => '0.0203',
                                                                        'Test::Vars' => '0',
                                                                        'Test::Version' => '2.00'
                                                                      }
                                                      },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'Alien::Base' => '0',
                                                                        'base' => '0',
                                                                        'constant' => '0',
                                                                        'strict' => '0',
                                                                        'warnings' => '0'
                                                                      }
                                                      },
                                         'test' => {
                                                     'recommends' => {
                                                                       'CPAN::Meta' => '2.120900'
                                                                     },
                                                     'requires' => {
                                                                     'ExtUtils::MakeMaker' => '6.52',
                                                                     'File::Spec' => '0',
                                                                     'IO::Handle' => '0',
                                                                     'IPC::Open3' => '0',
                                                                     'Package::Stash' => '0',
                                                                     'Test2::V0' => '0',
                                                                     'Test::Alien' => '2.3901',
                                                                     'Test::More' => '0',
                                                                     'perl' => '5.006'
                                                                   }
                                                   }
                                       },
                          'provides' => {
                                          'Alien::CFITSIO' => {
                                                                'file' => 'lib/Alien/CFITSIO.pm',
                                                                'version' => 'v4.4.0.0'
                                                              }
                                        },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'mailto' => 'bug-alien-cfitsio@rt.cpan.org',
                                                             'web' => 'https://rt.cpan.org/Public/Dist/Display.html?Name=Alien-CFITSIO'
                                                           },
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'https://gitlab.com/djerius/alien-cfitsio.git',
                                                             'web' => 'https://gitlab.com/djerius/alien-cfitsio'
                                                           }
                                         },
                          'version' => 'v4.4.0.0',
                          'x_alienfile' => {
                                             'generated_by' => 'Dist::Zilla::Plugin::AlienBuild version 0.32',
                                             'requires' => {
                                                             'share' => {
                                                                          'Alien::Build::Plugin::Gather::IsolateDynamic' => '0.48',
                                                                          'Archive::Tar' => '0',
                                                                          'Config' => '0',
                                                                          'HTTP::Tiny' => '0.044',
                                                                          'IO::Socket::SSL' => '1.56',
                                                                          'IO::Zlib' => '0',
                                                                          'Mojo::DOM58' => '1.00',
                                                                          'Mozilla::CA' => '0',
                                                                          'Net::SSLeay' => '1.49',
                                                                          'Sort::Versions' => '0',
                                                                          'URI' => '0',
                                                                          'URI::Escape' => '0'
                                                                        },
                                                             'system' => {}
                                                           }
                                           },
                          'x_generated_by_perl' => 'v5.38.2',
                          'x_serialization_backend' => 'Cpanel::JSON::XS version 4.37',
                          'x_spdx_expression' => 'GPL-3.0-only'
                        },
          'name' => 'Alien-CFITSIO-v4.4.0.0',
          'provides' => [
                          'Alien::CFITSIO'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'mailto' => 'bug-alien-cfitsio@rt.cpan.org',
                                             'web' => 'https://rt.cpan.org/Public/Dist/Display.html?Name=Alien-CFITSIO'
                                           },
                           'repository' => {
                                             'type' => 'git',
                                             'url' => 'https://gitlab.com/djerius/alien-cfitsio.git',
                                             'web' => 'https://gitlab.com/djerius/alien-cfitsio'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709574290,
                      'size' => 28048
                    },
          'status' => 'latest',
          'version' => 'v4.4.0.0',
          'version_numified' => '4.004'
        };
1;
