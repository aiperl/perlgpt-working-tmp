$distribution = $VAR1 = {
          '_id' => 'Lee5v0n8hyBh2uN_oqqu3009J_A',
          '_type' => 'release',
          'abstract' => 'CPAN.pm plugin for installing external dependencies',
          'archive' => 'CPAN-Plugin-Sysdeps-0.75.tar.gz',
          'author' => 'SREZIC',
          'changes_file' => 'Changes',
          'checksum_md5' => 'c31d9ab731385db2a1e1b47c069d3e5d',
          'checksum_sha256' => '60e7c923a409c3cb4cabd83c9e7599e4f7208cff4543e2936a11fa18629bde7e',
          'date' => '2024-03-03T17:03:04',
          'dependency' => [
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '5.006'
                            },
                            {
                              'module' => 'List::Util',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            }
                          ],
          'distribution' => 'CPAN-Plugin-Sysdeps',
          'download_url' => 'https://cpan.metacpan.org/authors/id/S/SR/SREZIC/CPAN-Plugin-Sysdeps-0.75.tar.gz',
          'id' => 'Lee5v0n8hyBh2uN_oqqu3009J_A',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'CPAN::Plugin::Sysdeps',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'CPAN.pm plugin for installing external dependencies',
                          'author' => [
                                        'Slaven Rezic <srezic@cpan.org>'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'ExtUtils::MakeMaker version 7.64, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'CPAN-Plugin-Sysdeps',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'ExtUtils::MakeMaker' => '0'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'ExtUtils::MakeMaker' => '0'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'List::Util' => '0',
                                                                        'Test::More' => '0',
                                                                        'perl' => '5.006'
                                                                      }
                                                      }
                                       },
                          'release_status' => 'stable',
                          'resources' => {
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'git://github.com/eserte/cpan-plugin-sysdeps.git'
                                                           }
                                         },
                          'version' => '0.75',
                          'x_serialization_backend' => 'JSON::PP version 4.07'
                        },
          'name' => 'CPAN-Plugin-Sysdeps-0.75',
          'provides' => [
                          'CPAN::Plugin::Sysdeps',
                          'CPAN::Plugin::Sysdeps::Mapping'
                        ],
          'resources' => {
                           'repository' => {
                                             'type' => 'git',
                                             'url' => 'git://github.com/eserte/cpan-plugin-sysdeps.git'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709485384,
                      'size' => 41845
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 1,
                       'na' => 0,
                       'pass' => 91,
                       'unknown' => 0
                     },
          'version' => '0.75',
          'version_numified' => '0.75'
        };
1;
