$distribution = $VAR1 = {
          '_id' => '_XrQ4dbnZgBnZMGmiZNub7nw0P4',
          '_type' => 'release',
          'abstract' => 'Validate Japanese zip-codes',
          'archive' => 'Number-ZipCode-JP-0.20240229.tar.gz',
          'author' => 'TANIGUCHI',
          'changes_file' => 'Changes',
          'checksum_md5' => '95fa9e60e5a8f320fcc3012db16cf6c5',
          'checksum_sha256' => '07b39278260f9880c79f5adc5707b824e8c78c59080e5aa6ad54d1c701a1e25c',
          'date' => '2024-03-01T07:59:19',
          'dependency' => [
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'UNIVERSAL::require',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            }
                          ],
          'distribution' => 'Number-ZipCode-JP',
          'download_url' => 'https://cpan.metacpan.org/authors/id/T/TA/TANIGUCHI/Number-ZipCode-JP-0.20240229.tar.gz',
          'id' => '_XrQ4dbnZgBnZMGmiZNub7nw0P4',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'Number::ZipCode::JP',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Validate Japanese zip-codes',
                          'author' => [
                                        'unknown'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'ExtUtils::MakeMaker version 7.58, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Number-ZipCode-JP',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'ExtUtils::MakeMaker' => '0'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'ExtUtils::MakeMaker' => '0'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'Test::More' => '0',
                                                                        'UNIVERSAL::require' => '0'
                                                                      }
                                                      }
                                       },
                          'release_status' => 'stable',
                          'resources' => {
                                           'X_twitter' => 'https://twitter.com/nipotan'
                                         },
                          'version' => '0.20240229',
                          'x_serialization_backend' => 'JSON::PP version 2.97001'
                        },
          'name' => 'Number-ZipCode-JP-0.20240229',
          'provides' => [
                          'Number::ZipCode::JP',
                          'Number::ZipCode::JP::Table',
                          'Number::ZipCode::JP::Table::Area',
                          'Number::ZipCode::JP::Table::Company'
                        ],
          'resources' => {},
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709279959,
                      'size' => 1499731
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 0,
                       'pass' => 97,
                       'unknown' => 0
                     },
          'version' => '0.20240229',
          'version_numified' => '0.20240229'
        };
1;
