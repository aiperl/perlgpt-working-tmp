$distribution = $VAR1 = {
          '_id' => '_Rie6HD1C8rwc3e0eRBmmwMsHUs',
          '_type' => 'release',
          'abstract' => 'Audit CPAN distributions for known vulnerabilities',
          'archive' => 'CPAN-Audit-20240302.001.tar.gz',
          'author' => 'BDFOY',
          'changes_file' => 'Changes',
          'checksum_md5' => 'cba877e9e53baa4170fba61f1b74e084',
          'checksum_sha256' => '19cac14ddd8a39d04df656d2b8231d943c554f2dc609440a458f89bca0ad99af',
          'date' => '2024-03-03T00:42:56',
          'dependency' => [
                            {
                              'module' => 'Capture::Tiny',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Temp',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0.98'
                            },
                            {
                              'module' => 'HTTP::Tiny',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'JSON',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'IO::Interactive',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '5.010'
                            },
                            {
                              'module' => 'CPAN::DistnameInfo',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Module::CoreList',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '5.20181020'
                            },
                            {
                              'module' => 'Pod::Usage',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.69'
                            },
                            {
                              'module' => 'Module::CPANfile',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'PerlIO::gzip',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Module::Extract::VERSION',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '6.64'
                            },
                            {
                              'module' => 'File::Spec::Functions',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'YAML::Tiny',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            }
                          ],
          'distribution' => 'CPAN-Audit',
          'download_url' => 'https://cpan.metacpan.org/authors/id/B/BD/BDFOY/CPAN-Audit-20240302.001.tar.gz',
          'id' => '_Rie6HD1C8rwc3e0eRBmmwMsHUs',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'CPAN::Audit',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Audit CPAN distributions for known vulnerabilities',
                          'author' => [
                                        'Viacheslav Tykhanovskyi <viacheslav.t@gmail.com>'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'ExtUtils::MakeMaker version 7.70, CPAN::Meta::Converter version 2.150010, CPAN::Meta::Converter version 2.150005',
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'CPAN-Audit',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'YAML::Tiny' => '0'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'ExtUtils::MakeMaker' => '6.64',
                                                                          'File::Spec::Functions' => '0'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'CPAN::DistnameInfo' => '0',
                                                                        'IO::Interactive' => '0',
                                                                        'JSON' => '0',
                                                                        'Module::CPANfile' => '0',
                                                                        'Module::CoreList' => '5.20181020',
                                                                        'Module::Extract::VERSION' => '0',
                                                                        'PerlIO::gzip' => '0',
                                                                        'Pod::Usage' => '1.69',
                                                                        'perl' => '5.010'
                                                                      }
                                                      },
                                         'test' => {
                                                     'requires' => {
                                                                     'Capture::Tiny' => '0',
                                                                     'File::Temp' => '0',
                                                                     'HTTP::Tiny' => '0',
                                                                     'Test::More' => '0.98'
                                                                   }
                                                   }
                                       },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/briandfoy/cpan-audit/issues'
                                                           },
                                           'homepage' => 'https://github.com/briandfoy/cpan-audit',
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'https://github.com/briandfoy/cpan-audit',
                                                             'web' => 'https://github.com/briandfoy/cpan-audit'
                                                           }
                                         },
                          'version' => '20240302.001',
                          'x_serialization_backend' => 'JSON::PP version 4.16'
                        },
          'name' => 'CPAN-Audit-20240302.001',
          'provides' => [
                          'CPAN::Audit',
                          'CPAN::Audit::DB',
                          'CPAN::Audit::Discover',
                          'CPAN::Audit::Discover::Cpanfile',
                          'CPAN::Audit::Discover::CpanfileSnapshot',
                          'CPAN::Audit::Filter',
                          'CPAN::Audit::FreshnessCheck',
                          'CPAN::Audit::Installed',
                          'CPAN::Audit::Query',
                          'CPAN::Audit::Version'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/briandfoy/cpan-audit/issues'
                                           },
                           'homepage' => 'https://github.com/briandfoy/cpan-audit',
                           'repository' => {
                                             'type' => 'git',
                                             'url' => 'https://github.com/briandfoy/cpan-audit',
                                             'web' => 'https://github.com/briandfoy/cpan-audit'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709426576,
                      'size' => 702493
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 2,
                       'pass' => 125,
                       'unknown' => 0
                     },
          'version' => '20240302.001',
          'version_numified' => '20240302.001'
        };
1;
