$distribution = $VAR1 = {
          '_id' => 'iEoj5lJS4I0HK_kWVg9cwdEinao',
          '_type' => 'release',
          'abstract' => 'Automate the Firefox browser with the Marionette protocol',
          'archive' => 'Firefox-Marionette-1.53.tar.gz',
          'author' => 'DDICK',
          'changes_file' => 'Changes',
          'checksum_md5' => '3b7c1130eb31eee1a06facfc98c330f9',
          'checksum_sha256' => 'eae6efe7492f57322448b449003fc3998a04a7264c91a50075b5f1d166b78e71',
          'date' => '2024-03-03T10:58:22',
          'dependency' => [
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Time::Local',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Config',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Time::HiRes',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'URI',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.61'
                            },
                            {
                              'module' => 'MIME::Base64',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '3.11'
                            },
                            {
                              'module' => 'Term::ReadKey',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'FileHandle',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'IO::Handle',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Text::CSV_XS',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.35'
                            },
                            {
                              'module' => 'Scalar::Util',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Find',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'overload',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'DirHandle',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'URI::URL',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Socket',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'POSIX',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Crypt::URandom',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Exporter',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'JSON',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Fcntl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Archive::Zip',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'English',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Spec',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'IPC::Open3',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.03'
                            },
                            {
                              'module' => 'Encode',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Config::INI::Reader',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Temp',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'URI::data',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'XML::Parser',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Pod::Simple::Text',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'parent',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'URI::Escape',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '5.006'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'HTTP::Daemon',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'IO::Socket::SSL',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Crypt::PasswdMD5',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::CheckManifest',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0.9'
                            },
                            {
                              'module' => 'PDF::API2',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '2.036'
                            },
                            {
                              'module' => 'Test::Pod::Coverage',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '1.04'
                            },
                            {
                              'module' => 'HTTP::Status',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Compress::Zlib',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Pod',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '1.41'
                            },
                            {
                              'module' => 'Cwd',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::HomeDir',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'HTTP::Response',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Digest::SHA',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'IO::Socket::IP',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            }
                          ],
          'distribution' => 'Firefox-Marionette',
          'download_url' => 'https://cpan.metacpan.org/authors/id/D/DD/DDICK/Firefox-Marionette-1.53.tar.gz',
          'id' => 'iEoj5lJS4I0HK_kWVg9cwdEinao',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'Firefox::Marionette',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Automate the Firefox browser with the Marionette protocol',
                          'author' => [
                                        'David Dick <ddick@cpan.org>'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'ExtUtils::MakeMaker version 7.70, CPAN::Meta::Converter version 2.150010, CPAN::Meta::Converter version 2.150005',
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Firefox-Marionette',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'Compress::Zlib' => '0',
                                                                      'Crypt::PasswdMD5' => '0',
                                                                      'Cwd' => '0',
                                                                      'Digest::SHA' => '0',
                                                                      'File::HomeDir' => '0',
                                                                      'HTTP::Daemon' => '0',
                                                                      'HTTP::Response' => '0',
                                                                      'HTTP::Status' => '0',
                                                                      'IO::Socket::IP' => '0',
                                                                      'IO::Socket::SSL' => '0',
                                                                      'PDF::API2' => '2.036',
                                                                      'Test::CheckManifest' => '0.9',
                                                                      'Test::More' => '0',
                                                                      'Test::Pod' => '1.41',
                                                                      'Test::Pod::Coverage' => '1.04'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'ExtUtils::MakeMaker' => '0'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'Archive::Zip' => '0',
                                                                        'Config' => '0',
                                                                        'Config::INI::Reader' => '0',
                                                                        'Crypt::URandom' => '0',
                                                                        'DirHandle' => '0',
                                                                        'Encode' => '0',
                                                                        'English' => '0',
                                                                        'Exporter' => '0',
                                                                        'Fcntl' => '0',
                                                                        'File::Find' => '0',
                                                                        'File::Spec' => '0',
                                                                        'File::Temp' => '0',
                                                                        'FileHandle' => '0',
                                                                        'IO::Handle' => '0',
                                                                        'IPC::Open3' => '1.03',
                                                                        'JSON' => '0',
                                                                        'MIME::Base64' => '3.11',
                                                                        'POSIX' => '0',
                                                                        'Pod::Simple::Text' => '0',
                                                                        'Scalar::Util' => '0',
                                                                        'Socket' => '0',
                                                                        'Term::ReadKey' => '0',
                                                                        'Text::CSV_XS' => '1.35',
                                                                        'Time::HiRes' => '0',
                                                                        'Time::Local' => '0',
                                                                        'URI' => '1.61',
                                                                        'URI::Escape' => '0',
                                                                        'URI::URL' => '0',
                                                                        'URI::data' => '0',
                                                                        'XML::Parser' => '0',
                                                                        'overload' => '0',
                                                                        'parent' => '0',
                                                                        'perl' => '5.006'
                                                                      }
                                                      }
                                       },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/david-dick/firefox-marionette/issues'
                                                           },
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'https://github.com/david-dick/firefox-marionette',
                                                             'web' => 'https://github.com/david-dick/firefox-marionette'
                                                           }
                                         },
                          'version' => '1.53',
                          'x_serialization_backend' => 'JSON::PP version 4.16'
                        },
          'name' => 'Firefox-Marionette-1.53',
          'provides' => [
                          'Firefox::Marionette',
                          'Firefox::Marionette::Bookmark',
                          'Firefox::Marionette::Buttons',
                          'Firefox::Marionette::Cache',
                          'Firefox::Marionette::Capabilities',
                          'Firefox::Marionette::Certificate',
                          'Firefox::Marionette::Cookie',
                          'Firefox::Marionette::Display',
                          'Firefox::Marionette::Element',
                          'Firefox::Marionette::Element::Rect',
                          'Firefox::Marionette::Exception',
                          'Firefox::Marionette::Exception::InsecureCertificate',
                          'Firefox::Marionette::Exception::NoSuchAlert',
                          'Firefox::Marionette::Exception::NotFound',
                          'Firefox::Marionette::Exception::Response',
                          'Firefox::Marionette::Exception::StaleElement',
                          'Firefox::Marionette::Extension::HarExportTrigger',
                          'Firefox::Marionette::Extension::Stealth',
                          'Firefox::Marionette::GeoLocation',
                          'Firefox::Marionette::Image',
                          'Firefox::Marionette::Keys',
                          'Firefox::Marionette::Link',
                          'Firefox::Marionette::LocalObject',
                          'Firefox::Marionette::Login',
                          'Firefox::Marionette::Profile',
                          'Firefox::Marionette::Proxy',
                          'Firefox::Marionette::Response',
                          'Firefox::Marionette::ShadowRoot',
                          'Firefox::Marionette::Timeouts',
                          'Firefox::Marionette::UpdateStatus',
                          'Firefox::Marionette::WebAuthn::Authenticator',
                          'Firefox::Marionette::WebAuthn::Credential',
                          'Firefox::Marionette::WebFrame',
                          'Firefox::Marionette::WebWindow',
                          'Firefox::Marionette::Window::Rect',
                          'Waterfox::Marionette',
                          'Waterfox::Marionette::Profile'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/david-dick/firefox-marionette/issues'
                                           },
                           'repository' => {
                                             'type' => 'git',
                                             'url' => 'https://github.com/david-dick/firefox-marionette',
                                             'web' => 'https://github.com/david-dick/firefox-marionette'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709463502,
                      'size' => 398221
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 0,
                       'pass' => 30,
                       'unknown' => 43
                     },
          'version' => '1.53',
          'version_numified' => '1.53'
        };
1;
