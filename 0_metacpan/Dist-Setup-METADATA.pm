$distribution = $VAR1 = {
          '_id' => 'hU86jLyEqi4W5wvcT3eaDlu8Gak',
          '_type' => 'release',
          'abstract' => 'Simple opinionated tool to set up a Perl distribution directory.',
          'archive' => 'Dist-Setup/Dist-Setup-0.10.tar.gz',
          'author' => 'MATHIAS',
          'changes_file' => 'Changes',
          'checksum_md5' => '384e52e3db07c5ef932f8a297d077535',
          'checksum_sha256' => 'c4a8b8b265ea8a78173c167dbcc626e94d0532f8f6970c6701053350aa62d06f',
          'date' => '2024-03-01T22:58:23',
          'dependency' => [
                            {
                              'module' => 'perl',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => 'v5.26.0'
                            },
                            {
                              'module' => 'File::ShareDir::Install',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker::CPANfile',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0.09'
                            },
                            {
                              'module' => 'Devel::Cover',
                              'phase' => 'develop',
                              'relationship' => 'recommends',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test2::Tools::PerlCritic',
                              'phase' => 'test',
                              'relationship' => 'suggests',
                              'version' => '0'
                            },
                            {
                              'module' => 'IPC::Run3',
                              'phase' => 'test',
                              'relationship' => 'suggests',
                              'version' => '0'
                            },
                            {
                              'module' => 'Perl::Tidy',
                              'phase' => 'test',
                              'relationship' => 'suggests',
                              'version' => '20220613'
                            },
                            {
                              'module' => 'Test::Pod',
                              'phase' => 'test',
                              'relationship' => 'recommends',
                              'version' => '1.22'
                            },
                            {
                              'module' => 'Test2::V0',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Readonly',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::CPANfile',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::ShareDir',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Eval::Safe',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Template',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '5.026'
                            }
                          ],
          'distribution' => 'Dist-Setup',
          'download_url' => 'https://cpan.metacpan.org/authors/id/M/MA/MATHIAS/Dist-Setup/Dist-Setup-0.10.tar.gz',
          'id' => 'hU86jLyEqi4W5wvcT3eaDlu8Gak',
          'license' => [
                         'mit'
                       ],
          'main_module' => 'Dist::Setup',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Simple opinionated tool to set up a Perl distribution directory.',
                          'author' => [
                                        'Mathias Kende <mathias@cpan.org>'
                                      ],
                          'dynamic_config' => 0,
                          'generated_by' => 'ExtUtils::MakeMaker version 7.44, CPAN::Meta::Converter version 2.150010, CPAN::Meta::Converter version 2.150005',
                          'license' => [
                                         'mit'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Dist-Setup',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc',
                                                           'local',
                                                           'vendor',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'ExtUtils::MakeMaker' => '0'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'ExtUtils::MakeMaker::CPANfile' => '0.09',
                                                                          'File::ShareDir::Install' => '0',
                                                                          'perl' => 'v5.26.0'
                                                                        }
                                                        },
                                         'develop' => {
                                                        'recommends' => {
                                                                          'Devel::Cover' => '0'
                                                                        }
                                                      },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'Eval::Safe' => '0',
                                                                        'File::ShareDir' => '0',
                                                                        'Template' => '0',
                                                                        'perl' => '5.026'
                                                                      }
                                                      },
                                         'test' => {
                                                     'recommends' => {
                                                                       'Test::Pod' => '1.22'
                                                                     },
                                                     'requires' => {
                                                                     'Readonly' => '0',
                                                                     'Test2::V0' => '0',
                                                                     'Test::CPANfile' => '0',
                                                                     'Test::More' => '0'
                                                                   },
                                                     'suggests' => {
                                                                     'IPC::Run3' => '0',
                                                                     'Perl::Tidy' => '20220613',
                                                                     'Test2::Tools::PerlCritic' => '0'
                                                                   }
                                                   }
                                       },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/mkende/perl_dist_setup/issues'
                                                           },
                                           'repository' => {
                                                             'type' => 'git',
                                                             'web' => 'https://github.com/mkende/perl_dist_setup'
                                                           }
                                         },
                          'version' => '0.10',
                          'x_serialization_backend' => 'JSON::PP version 4.04'
                        },
          'name' => 'Dist-Setup-0.10',
          'provides' => [
                          'Dist::Setup'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/mkende/perl_dist_setup/issues'
                                           },
                           'repository' => {
                                             'type' => 'git',
                                             'web' => 'https://github.com/mkende/perl_dist_setup'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709333903,
                      'size' => 16958
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 0,
                       'pass' => 60,
                       'unknown' => 0
                     },
          'version' => '0.10',
          'version_numified' => '0.1'
        };
1;
