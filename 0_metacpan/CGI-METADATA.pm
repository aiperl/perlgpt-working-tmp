$distribution = $VAR1 = {
          '_id' => 'LxqoJcsc8Y9CiOENg1AQf6jC4Xg',
          '_type' => 'release',
          'abstract' => 'Handle Common Gateway Interface requests and responses',
          'archive' => 'CGI-4.62.tar.gz',
          'author' => 'LEEJO',
          'changes_file' => 'Changes',
          'checksum_md5' => 'bbdc494f0fdd5d116ab71be3befba30f',
          'checksum_sha256' => 'b3619821c7aeba3fe1235985fc18f515049a2422ebfc680799cc30007faa80c5',
          'date' => '2024-03-01T13:46:49',
          'dependency' => [
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0.98'
                            },
                            {
                              'module' => 'File::Find',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Cwd',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'IO::File',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Warn',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0.3'
                            },
                            {
                              'module' => 'IO::Handle',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'POSIX',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::NoWarnings',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Temp',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.17'
                            },
                            {
                              'module' => 'utf8',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Carp',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Encode',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'URI',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.76'
                            },
                            {
                              'module' => 'HTML::Entities',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '3.69'
                            },
                            {
                              'module' => 'Config',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '5.008001'
                            },
                            {
                              'module' => 'warnings',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'overload',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'parent',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.225'
                            },
                            {
                              'module' => 'File::Spec',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.82'
                            },
                            {
                              'module' => 'if',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'strict',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Exporter',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            }
                          ],
          'distribution' => 'CGI',
          'download_url' => 'https://cpan.metacpan.org/authors/id/L/LE/LEEJO/CGI-4.62.tar.gz',
          'id' => 'LxqoJcsc8Y9CiOENg1AQf6jC4Xg',
          'license' => [
                         'artistic_2'
                       ],
          'main_module' => 'CGI',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Handle Common Gateway Interface requests and responses',
                          'author' => [
                                        'unknown'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'ExtUtils::MakeMaker version 7.62, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'artistic_2'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'CGI',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc',
                                                           't',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'ExtUtils::MakeMaker' => '0'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'ExtUtils::MakeMaker' => '0'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'Carp' => '0',
                                                                        'Config' => '0',
                                                                        'Encode' => '0',
                                                                        'Exporter' => '0',
                                                                        'File::Spec' => '0.82',
                                                                        'File::Temp' => '0.17',
                                                                        'HTML::Entities' => '3.69',
                                                                        'URI' => '1.76',
                                                                        'if' => '0',
                                                                        'overload' => '0',
                                                                        'parent' => '0.225',
                                                                        'perl' => '5.008001',
                                                                        'strict' => '0',
                                                                        'utf8' => '0',
                                                                        'warnings' => '0'
                                                                      }
                                                      },
                                         'test' => {
                                                     'requires' => {
                                                                     'Cwd' => '0',
                                                                     'File::Find' => '0',
                                                                     'IO::File' => '0',
                                                                     'IO::Handle' => '0',
                                                                     'POSIX' => '0',
                                                                     'Test::More' => '0.98',
                                                                     'Test::NoWarnings' => '0',
                                                                     'Test::Warn' => '0.3'
                                                                   }
                                                   }
                                       },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/leejo/CGI.pm/issues'
                                                           },
                                           'homepage' => 'https://metacpan.org/module/CGI',
                                           'license' => [
                                                          'http://dev.perl.org/licenses/'
                                                        ],
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'https://github.com/leejo/CGI.pm',
                                                             'web' => 'https://github.com/leejo/CGI.pm'
                                                           }
                                         },
                          'version' => '4.62',
                          'x_serialization_backend' => 'JSON::PP version 4.06'
                        },
          'name' => 'CGI-4.62',
          'provides' => [
                          'CGI',
                          'CGI::Carp',
                          'CGI::Cookie',
                          'CGI::File::Temp',
                          'CGI::HTML::Functions',
                          'CGI::MultipartBuffer',
                          'CGI::Pretty',
                          'CGI::Push',
                          'CGI::Util',
                          'Fh'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/leejo/CGI.pm/issues'
                                           },
                           'homepage' => 'https://metacpan.org/module/CGI',
                           'license' => [
                                          'http://dev.perl.org/licenses/'
                                        ],
                           'repository' => {
                                             'type' => 'git',
                                             'url' => 'https://github.com/leejo/CGI.pm',
                                             'web' => 'https://github.com/leejo/CGI.pm'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709300809,
                      'size' => 208782
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 15,
                       'na' => 0,
                       'pass' => 110,
                       'unknown' => 0
                     },
          'version' => '4.62',
          'version_numified' => '4.62'
        };
1;
