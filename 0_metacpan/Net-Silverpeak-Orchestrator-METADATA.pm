$distribution = $VAR1 = {
          '_id' => 'OzFH5uC_JCSotETtguspBJdVzf8',
          '_type' => 'release',
          'abstract' => 'Silverpeak Orchestrator REST API client library',
          'archive' => 'Net-Silverpeak-Orchestrator-0.010000.tar.gz',
          'author' => 'ABRAXXA',
          'changes_file' => 'Changes',
          'checksum_md5' => 'f7e5ecdb7666f6e808351ef44be75f3b',
          'checksum_sha256' => '451256de117c2000cdd7c6ab81e9fee60fc0695c4dd593baf955f5bc8a2db7dd',
          'date' => '2024-03-04T13:29:04',
          'dependency' => [
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'List::Util',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.55'
                            },
                            {
                              'module' => 'Role::REST::Client',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.23'
                            },
                            {
                              'module' => 'Carp::Clan',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '6.07'
                            },
                            {
                              'module' => 'JSON',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '2.97001'
                            },
                            {
                              'module' => 'Type::Tiny',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.004002'
                            },
                            {
                              'module' => 'HTTP::CookieJar',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.010'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '5.024000'
                            },
                            {
                              'module' => 'HTTP::Tiny',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.088'
                            },
                            {
                              'module' => 'Moo',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '2.003004'
                            },
                            {
                              'module' => 'IO::Handle',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Spec',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'IPC::Open3',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test2::V0',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0.000135'
                            },
                            {
                              'module' => 'Test::CPAN::Changes',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.19'
                            },
                            {
                              'module' => 'Test::Pod::Coverage',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1.08'
                            },
                            {
                              'module' => 'Test::Kwalitee',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1.21'
                            },
                            {
                              'module' => 'Test::Pod',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1.41'
                            },
                            {
                              'module' => 'Test::EOL',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Perl::Critic',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Synopsis',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Pod::LinkCheck',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::DistManifest',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Vars',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Mojibake',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::CPAN::Meta',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::NoTabs',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.88'
                            },
                            {
                              'module' => 'Test::MinimumVersion',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::CPAN::Meta::JSON',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.16'
                            },
                            {
                              'module' => 'Test::Portability::Files',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Version',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1'
                            },
                            {
                              'module' => 'Pod::Coverage::TrustPod',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            }
                          ],
          'distribution' => 'Net-Silverpeak-Orchestrator',
          'download_url' => 'https://cpan.metacpan.org/authors/id/A/AB/ABRAXXA/Net-Silverpeak-Orchestrator-0.010000.tar.gz',
          'id' => 'OzFH5uC_JCSotETtguspBJdVzf8',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'Net::Silverpeak::Orchestrator',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Silverpeak Orchestrator REST API client library',
                          'author' => [
                                        'Alexander Hartmaier <abraxxa@cpan.org>'
                                      ],
                          'dynamic_config' => 0,
                          'generated_by' => 'Dist::Zilla version 6.031, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Net-Silverpeak-Orchestrator',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'configure' => {
                                                          'requires' => {
                                                                          'ExtUtils::MakeMaker' => '0'
                                                                        }
                                                        },
                                         'develop' => {
                                                        'requires' => {
                                                                        'Pod::Coverage::TrustPod' => '0',
                                                                        'Test::CPAN::Changes' => '0.19',
                                                                        'Test::CPAN::Meta' => '0',
                                                                        'Test::CPAN::Meta::JSON' => '0.16',
                                                                        'Test::DistManifest' => '0',
                                                                        'Test::EOL' => '0',
                                                                        'Test::Kwalitee' => '1.21',
                                                                        'Test::MinimumVersion' => '0',
                                                                        'Test::Mojibake' => '0',
                                                                        'Test::More' => '0.88',
                                                                        'Test::NoTabs' => '0',
                                                                        'Test::Perl::Critic' => '0',
                                                                        'Test::Pod' => '1.41',
                                                                        'Test::Pod::Coverage' => '1.08',
                                                                        'Test::Pod::LinkCheck' => '0',
                                                                        'Test::Portability::Files' => '0',
                                                                        'Test::Synopsis' => '0',
                                                                        'Test::Vars' => '0',
                                                                        'Test::Version' => '1'
                                                                      }
                                                      },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'Carp::Clan' => '6.07',
                                                                        'HTTP::CookieJar' => '0.010',
                                                                        'HTTP::Tiny' => '0.088',
                                                                        'JSON' => '2.97001',
                                                                        'List::Util' => '1.55',
                                                                        'Moo' => '2.003004',
                                                                        'Role::REST::Client' => '0.23',
                                                                        'Type::Tiny' => '1.004002',
                                                                        'perl' => '5.024000'
                                                                      }
                                                      },
                                         'test' => {
                                                     'requires' => {
                                                                     'File::Spec' => '0',
                                                                     'IO::Handle' => '0',
                                                                     'IPC::Open3' => '0',
                                                                     'Test2::V0' => '0.000135',
                                                                     'Test::More' => '0'
                                                                   }
                                                   }
                                       },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/abraxxa/Net-Silverpeak-Orchestrator/issues'
                                                           },
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'https://github.com/abraxxa/Net-Silverpeak-Orchestrator.git',
                                                             'web' => 'https://github.com/abraxxa/Net-Silverpeak-Orchestrator'
                                                           }
                                         },
                          'version' => '0.010000',
                          'x_Dist_Zilla' => {
                                              'perl' => {
                                                          'version' => '5.038002'
                                                        },
                                              'plugins' => [
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::GatherDir',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::GatherDir' => {
                                                                                                                   'exclude_filename' => [],
                                                                                                                   'exclude_match' => [],
                                                                                                                   'follow_symlinks' => 0,
                                                                                                                   'include_dotfiles' => 0,
                                                                                                                   'prefix' => '',
                                                                                                                   'prune_directory' => [],
                                                                                                                   'root' => '.'
                                                                                                                 }
                                                                           },
                                                               'name' => '@Basic/GatherDir',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PruneCruft',
                                                               'name' => '@Basic/PruneCruft',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::ManifestSkip',
                                                               'name' => '@Basic/ManifestSkip',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MetaYAML',
                                                               'name' => '@Basic/MetaYAML',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::License',
                                                               'name' => '@Basic/License',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Readme',
                                                               'name' => '@Basic/Readme',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::ExtraTests',
                                                               'name' => '@Basic/ExtraTests',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::ExecDir',
                                                               'name' => '@Basic/ExecDir',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::ShareDir',
                                                               'name' => '@Basic/ShareDir',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MakeMaker',
                                                               'config' => {
                                                                             'Dist::Zilla::Role::TestRunner' => {
                                                                                                                  'default_jobs' => 1
                                                                                                                }
                                                                           },
                                                               'name' => '@Basic/MakeMaker',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Manifest',
                                                               'name' => '@Basic/Manifest',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::TestRelease',
                                                               'name' => '@Basic/TestRelease',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::ConfirmRelease',
                                                               'name' => '@Basic/ConfirmRelease',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::UploadToCPAN',
                                                               'name' => '@Basic/UploadToCPAN',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PodWeaver',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::PodWeaver' => {
                                                                                                                   'finder' => [
                                                                                                                                 ':InstallModules',
                                                                                                                                 ':PerlExecFiles'
                                                                                                                               ],
                                                                                                                   'plugins' => [
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Plugin::EnsurePod5',
                                                                                                                                    'name' => '@CorePrep/EnsurePod5',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Plugin::H1Nester',
                                                                                                                                    'name' => '@CorePrep/H1Nester',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Plugin::SingleEncoding',
                                                                                                                                    'name' => '@Default/SingleEncoding',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Name',
                                                                                                                                    'name' => '@Default/Name',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Version',
                                                                                                                                    'name' => '@Default/Version',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Region',
                                                                                                                                    'name' => '@Default/prelude',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Generic',
                                                                                                                                    'name' => 'SYNOPSIS',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Generic',
                                                                                                                                    'name' => 'DESCRIPTION',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Generic',
                                                                                                                                    'name' => 'OVERVIEW',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Collect',
                                                                                                                                    'name' => 'ATTRIBUTES',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Collect',
                                                                                                                                    'name' => 'METHODS',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Collect',
                                                                                                                                    'name' => 'FUNCTIONS',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Leftovers',
                                                                                                                                    'name' => '@Default/Leftovers',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Region',
                                                                                                                                    'name' => '@Default/postlude',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Authors',
                                                                                                                                    'name' => '@Default/Authors',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  },
                                                                                                                                  {
                                                                                                                                    'class' => 'Pod::Weaver::Section::Legal',
                                                                                                                                    'name' => '@Default/Legal',
                                                                                                                                    'version' => '4.019'
                                                                                                                                  }
                                                                                                                                ]
                                                                                                                 }
                                                                           },
                                                               'name' => 'PodWeaver',
                                                               'version' => '4.010'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PkgVersion',
                                                               'name' => 'PkgVersion',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::NextRelease',
                                                               'name' => 'NextRelease',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MetaConfig',
                                                               'name' => 'MetaConfig',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MetaJSON',
                                                               'name' => 'MetaJSON',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MetaResources',
                                                               'name' => 'MetaResources',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::CPAN::Changes',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Test::CPAN::Changes' => {
                                                                                                                             'changelog' => 'Changes'
                                                                                                                           }
                                                                           },
                                                               'name' => '@TestingMania/Test::CPAN::Changes',
                                                               'version' => '0.012'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PodCoverageTests',
                                                               'name' => '@TestingMania/PodCoverageTests',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::Synopsis',
                                                               'name' => '@TestingMania/Test::Synopsis',
                                                               'version' => '2.000007'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::Version',
                                                               'name' => '@TestingMania/Test::Version',
                                                               'version' => '1.09'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::CPAN::Meta::JSON',
                                                               'name' => '@TestingMania/Test::CPAN::Meta::JSON',
                                                               'version' => '0.004'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::Kwalitee',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Test::Kwalitee' => {
                                                                                                                        'filename' => 'xt/release/kwalitee.t',
                                                                                                                        'skiptest' => []
                                                                                                                      }
                                                                           },
                                                               'name' => '@TestingMania/Test::Kwalitee',
                                                               'version' => '2.12'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::Portability',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Test::Portability' => {
                                                                                                                           'options' => ''
                                                                                                                         }
                                                                           },
                                                               'name' => '@TestingMania/Test::Portability',
                                                               'version' => '2.001001'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::Perl::Critic',
                                                               'name' => '@TestingMania/Test::Perl::Critic',
                                                               'version' => '3.001'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MetaTests',
                                                               'name' => '@TestingMania/MetaTests',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::Pod::LinkCheck',
                                                               'name' => '@TestingMania/Test::Pod::LinkCheck',
                                                               'version' => '1.004'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::DistManifest',
                                                               'name' => '@TestingMania/Test::DistManifest',
                                                               'version' => '2.000006'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::MinimumVersion',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Test::MinimumVersion' => {
                                                                                                                              'max_target_perl' => undef
                                                                                                                            }
                                                                           },
                                                               'name' => '@TestingMania/Test::MinimumVersion',
                                                               'version' => '2.000010'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::PodSyntaxTests',
                                                               'name' => '@TestingMania/PodSyntaxTests',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::MojibakeTests',
                                                               'name' => '@TestingMania/MojibakeTests',
                                                               'version' => '0.8'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::Compile',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Test::Compile' => {
                                                                                                                       'bail_out_on_fail' => 0,
                                                                                                                       'fail_on_warning' => 'author',
                                                                                                                       'fake_home' => 0,
                                                                                                                       'filename' => 't/00-compile.t',
                                                                                                                       'module_finder' => [
                                                                                                                                            ':InstallModules'
                                                                                                                                          ],
                                                                                                                       'needs_display' => 0,
                                                                                                                       'phase' => 'test',
                                                                                                                       'script_finder' => [
                                                                                                                                            ':PerlExecFiles'
                                                                                                                                          ],
                                                                                                                       'skips' => [],
                                                                                                                       'switch' => []
                                                                                                                     }
                                                                           },
                                                               'name' => '@TestingMania/Test::Compile',
                                                               'version' => '2.058'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::NoTabs',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Test::NoTabs' => {
                                                                                                                      'filename' => 'xt/author/no-tabs.t',
                                                                                                                      'finder' => [
                                                                                                                                    ':InstallModules',
                                                                                                                                    ':ExecFiles',
                                                                                                                                    ':TestFiles'
                                                                                                                                  ]
                                                                                                                    }
                                                                           },
                                                               'name' => '@TestingMania/Test::NoTabs',
                                                               'version' => '0.15'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::UnusedVars',
                                                               'name' => '@TestingMania/Test::UnusedVars',
                                                               'version' => '2.001001'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Test::EOL',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Test::EOL' => {
                                                                                                                   'filename' => 'xt/author/eol.t',
                                                                                                                   'finder' => [
                                                                                                                                 ':ExecFiles',
                                                                                                                                 ':InstallModules',
                                                                                                                                 ':TestFiles'
                                                                                                                               ],
                                                                                                                   'trailing_whitespace' => 1
                                                                                                                 }
                                                                           },
                                                               'name' => '@TestingMania/Test::EOL',
                                                               'version' => '0.19'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Git::Check',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Git::Check' => {
                                                                                                                    'untracked_files' => 'die'
                                                                                                                  },
                                                                             'Dist::Zilla::Role::Git::DirtyFiles' => {
                                                                                                                       'allow_dirty' => [
                                                                                                                                          'Changes',
                                                                                                                                          'dist.ini'
                                                                                                                                        ],
                                                                                                                       'allow_dirty_match' => [],
                                                                                                                       'changelog' => 'Changes'
                                                                                                                     },
                                                                             'Dist::Zilla::Role::Git::Repo' => {
                                                                                                                 'git_version' => '2.39.2',
                                                                                                                 'repo_root' => '.'
                                                                                                               }
                                                                           },
                                                               'name' => '@Git/Check',
                                                               'version' => '2.049'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Git::Commit',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Git::Commit' => {
                                                                                                                     'add_files_in' => [],
                                                                                                                     'commit_msg' => 'version %v%n%n%c',
                                                                                                                     'signoff' => 0
                                                                                                                   },
                                                                             'Dist::Zilla::Role::Git::DirtyFiles' => {
                                                                                                                       'allow_dirty' => [
                                                                                                                                          'Changes',
                                                                                                                                          'dist.ini'
                                                                                                                                        ],
                                                                                                                       'allow_dirty_match' => [],
                                                                                                                       'changelog' => 'Changes'
                                                                                                                     },
                                                                             'Dist::Zilla::Role::Git::Repo' => {
                                                                                                                 'git_version' => '2.39.2',
                                                                                                                 'repo_root' => '.'
                                                                                                               },
                                                                             'Dist::Zilla::Role::Git::StringFormatter' => {
                                                                                                                            'time_zone' => 'local'
                                                                                                                          }
                                                                           },
                                                               'name' => '@Git/Commit',
                                                               'version' => '2.049'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Git::Tag',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Git::Tag' => {
                                                                                                                  'branch' => undef,
                                                                                                                  'changelog' => 'Changes',
                                                                                                                  'signed' => 0,
                                                                                                                  'tag' => '0.010000',
                                                                                                                  'tag_format' => '%v',
                                                                                                                  'tag_message' => '%v'
                                                                                                                },
                                                                             'Dist::Zilla::Role::Git::Repo' => {
                                                                                                                 'git_version' => '2.39.2',
                                                                                                                 'repo_root' => '.'
                                                                                                               },
                                                                             'Dist::Zilla::Role::Git::StringFormatter' => {
                                                                                                                            'time_zone' => 'local'
                                                                                                                          }
                                                                           },
                                                               'name' => '@Git/Tag',
                                                               'version' => '2.049'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Git::Push',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Git::Push' => {
                                                                                                                   'push_to' => [
                                                                                                                                  'origin'
                                                                                                                                ],
                                                                                                                   'remotes_must_exist' => 1
                                                                                                                 },
                                                                             'Dist::Zilla::Role::Git::Repo' => {
                                                                                                                 'git_version' => '2.39.2',
                                                                                                                 'repo_root' => '.'
                                                                                                               }
                                                                           },
                                                               'name' => '@Git/Push',
                                                               'version' => '2.049'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Prereqs',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Prereqs' => {
                                                                                                                 'phase' => 'runtime',
                                                                                                                 'type' => 'requires'
                                                                                                               }
                                                                           },
                                                               'name' => 'Prereqs',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::Prereqs',
                                                               'config' => {
                                                                             'Dist::Zilla::Plugin::Prereqs' => {
                                                                                                                 'phase' => 'test',
                                                                                                                 'type' => 'requires'
                                                                                                               }
                                                                           },
                                                               'name' => 'TestRequires',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':InstallModules',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':IncModules',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':TestFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':ExtraTestFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':ExecFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':PerlExecFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':ShareFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':MainModule',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':AllFiles',
                                                               'version' => '6.031'
                                                             },
                                                             {
                                                               'class' => 'Dist::Zilla::Plugin::FinderCode',
                                                               'name' => ':NoFiles',
                                                               'version' => '6.031'
                                                             }
                                                           ],
                                              'zilla' => {
                                                           'class' => 'Dist::Zilla::Dist::Builder',
                                                           'config' => {
                                                                         'is_trial' => 0
                                                                       },
                                                           'version' => '6.031'
                                                         }
                                            },
                          'x_generated_by_perl' => 'v5.38.2',
                          'x_serialization_backend' => 'Cpanel::JSON::XS version 4.37',
                          'x_spdx_expression' => 'Artistic-1.0-Perl OR GPL-1.0-or-later'
                        },
          'name' => 'Net-Silverpeak-Orchestrator-0.010000',
          'provides' => [
                          'Net::Silverpeak::Orchestrator'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/abraxxa/Net-Silverpeak-Orchestrator/issues'
                                           },
                           'repository' => {
                                             'type' => 'git',
                                             'url' => 'https://github.com/abraxxa/Net-Silverpeak-Orchestrator.git',
                                             'web' => 'https://github.com/abraxxa/Net-Silverpeak-Orchestrator'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709558944,
                      'size' => 22725
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 5,
                       'pass' => 32,
                       'unknown' => 0
                     },
          'version' => '0.010000',
          'version_numified' => '0.01'
        };
1;
