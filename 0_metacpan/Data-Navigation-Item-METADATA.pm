$distribution = $VAR1 = {
          '_id' => 'JIDeA23lHzyYRiiHykh6e27DtS4',
          '_type' => 'release',
          'abstract' => 'Data object for navigation item.',
          'archive' => 'Data-Navigation-Item-0.01.tar.gz',
          'author' => 'SKIM',
          'changes_file' => 'Changes',
          'checksum_md5' => '1da73711cada600f3c29416757999e43',
          'checksum_sha256' => '9d38f840f0c8ffefcd2b445abd2d598179391d02b8935da4675eb3b8da60c7fc',
          'date' => '2024-03-04T11:53:10',
          'dependency' => [
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '6.36'
                            },
                            {
                              'module' => 'English',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::NoWarnings',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Error::Pure::Utils',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '6.36'
                            },
                            {
                              'module' => 'Mo::utils',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.09'
                            },
                            {
                              'module' => 'Mo::utils::URI',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Mo',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => 'v5.8.0'
                            },
                            {
                              'module' => 'Mo::utils::CSS',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.02'
                            }
                          ],
          'distribution' => 'Data-Navigation-Item',
          'download_url' => 'https://cpan.metacpan.org/authors/id/S/SK/SKIM/Data-Navigation-Item-0.01.tar.gz',
          'id' => 'JIDeA23lHzyYRiiHykh6e27DtS4',
          'license' => [
                         'bsd'
                       ],
          'main_module' => 'Data::Navigation::Item',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Data object for navigation item.',
                          'author' => [
                                        'Michal Josef Spacek <skim@cpan.org>'
                                      ],
                          'dynamic_config' => '1',
                          'generated_by' => 'Module::Install version 1.21, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'bsd'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Data-Navigation-Item',
                          'no_index' => {
                                          'directory' => [
                                                           'examples',
                                                           'inc',
                                                           't',
                                                           'xt',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'English' => '0',
                                                                      'Error::Pure::Utils' => '0',
                                                                      'ExtUtils::MakeMaker' => '6.36',
                                                                      'Test::More' => '0',
                                                                      'Test::NoWarnings' => '0'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'ExtUtils::MakeMaker' => '6.36'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'Mo' => '0',
                                                                        'Mo::utils' => '0.09',
                                                                        'Mo::utils::CSS' => '0.02',
                                                                        'Mo::utils::URI' => '0',
                                                                        'perl' => 'v5.8.0'
                                                                      }
                                                      }
                                       },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/michal-josef-spacek/Data-Navigation-Item/issues'
                                                           },
                                           'homepage' => 'https://github.com/michal-josef-spacek/Data-Navigation-Item',
                                           'license' => [
                                                          'http://opensource.org/licenses/bsd-license.php'
                                                        ],
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'git://github.com/michal-josef-spacek/Data-Navigation-Item'
                                                           }
                                         },
                          'version' => '0.01'
                        },
          'name' => 'Data-Navigation-Item-0.01',
          'provides' => [
                          'Data::Navigation::Item'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/michal-josef-spacek/Data-Navigation-Item/issues'
                                           },
                           'homepage' => 'https://github.com/michal-josef-spacek/Data-Navigation-Item',
                           'license' => [
                                          'http://opensource.org/licenses/bsd-license.php'
                                        ],
                           'repository' => {
                                             'url' => 'git://github.com/michal-josef-spacek/Data-Navigation-Item'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709553190,
                      'size' => 26564
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 0,
                       'pass' => 57,
                       'unknown' => 0
                     },
          'version' => '0.01',
          'version_numified' => '0.01'
        };
1;
