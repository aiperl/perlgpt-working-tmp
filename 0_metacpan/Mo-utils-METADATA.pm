$distribution = $VAR1 = {
          '_id' => 'RXvw3Ixvx7Fq8dEZppqo_STWwRI',
          '_type' => 'release',
          'abstract' => 'Mo utilities.',
          'archive' => 'Mo-utils-0.23.tar.gz',
          'author' => 'SKIM',
          'changes_file' => 'Changes',
          'checksum_md5' => '7a9815d475870f82acfa42a62c383ac7',
          'checksum_sha256' => 'fd03e18cdabcb1a09f305572b36c94a19bca2090f39eb8fa06c68ef6bec811a7',
          'date' => '2024-03-04T10:53:39',
          'dependency' => [
                            {
                              'module' => 'Scalar::Util',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Error::Pure',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.15'
                            },
                            {
                              'module' => 'List::Util',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.33'
                            },
                            {
                              'module' => 'Readonly',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => 'v5.6.2'
                            },
                            {
                              'module' => 'Exporter',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Error::Pure::Utils',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::MockObject',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '6.59'
                            },
                            {
                              'module' => 'English',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::NoWarnings',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '6.59'
                            }
                          ],
          'distribution' => 'Mo-utils',
          'download_url' => 'https://cpan.metacpan.org/authors/id/S/SK/SKIM/Mo-utils-0.23.tar.gz',
          'id' => 'RXvw3Ixvx7Fq8dEZppqo_STWwRI',
          'license' => [
                         'bsd'
                       ],
          'main_module' => 'Mo::utils',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Mo utilities.',
                          'author' => [
                                        'Michal Josef Spacek <skim@cpan.org>'
                                      ],
                          'dynamic_config' => '1',
                          'generated_by' => 'Module::Install version 1.21, CPAN::Meta::Converter version 2.150005',
                          'license' => [
                                         'bsd'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Mo-utils',
                          'no_index' => {
                                          'directory' => [
                                                           'examples',
                                                           'inc',
                                                           't',
                                                           'xt',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'English' => '0',
                                                                      'Error::Pure::Utils' => '0',
                                                                      'ExtUtils::MakeMaker' => '6.59',
                                                                      'Test::MockObject' => '0',
                                                                      'Test::More' => '0',
                                                                      'Test::NoWarnings' => '0'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'ExtUtils::MakeMaker' => '6.59'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'Error::Pure' => '0.15',
                                                                        'Exporter' => '0',
                                                                        'List::Util' => '1.33',
                                                                        'Readonly' => '0',
                                                                        'Scalar::Util' => '0',
                                                                        'perl' => 'v5.6.2'
                                                                      }
                                                      }
                                       },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/michal-josef-spacek/Mo-utils/issues'
                                                           },
                                           'homepage' => 'https://github.com/michal-josef-spacek/Mo-utils',
                                           'license' => [
                                                          'http://opensource.org/licenses/bsd-license.php'
                                                        ],
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'git://github.com/michal-josef-spacek/Mo-utils'
                                                           }
                                         },
                          'version' => '0.23'
                        },
          'name' => 'Mo-utils-0.23',
          'provides' => [
                          'Mo::utils'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/michal-josef-spacek/Mo-utils/issues'
                                           },
                           'homepage' => 'https://github.com/michal-josef-spacek/Mo-utils',
                           'license' => [
                                          'http://opensource.org/licenses/bsd-license.php'
                                        ],
                           'repository' => {
                                             'url' => 'git://github.com/michal-josef-spacek/Mo-utils'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709549619,
                      'size' => 37762
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 0,
                       'pass' => 57,
                       'unknown' => 0
                     },
          'version' => '0.23',
          'version_numified' => '0.23'
        };
1;
