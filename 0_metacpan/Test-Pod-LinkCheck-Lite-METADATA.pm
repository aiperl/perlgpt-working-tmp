$distribution = $VAR1 = {
          '_id' => 'vcBuhPqON51pUMy50csuchagLuE',
          '_type' => 'release',
          'abstract' => 'Test POD links',
          'archive' => 'Test-Pod-LinkCheck-Lite-0.013.tar.gz',
          'author' => 'WYANT',
          'changes_file' => 'Changes',
          'checksum_md5' => 'a8885a33bee16208ac15496eb7f1d7ec',
          'checksum_sha256' => 'd718c23fefefdbd01a4428164c12f7c3622b9422838d3e895f4763a30918350d',
          'date' => '2024-03-03T13:38:11',
          'dependency' => [
                            {
                              'module' => 'Pod::Simple',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'constant',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Pod::Perldoc',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'B::Keywords',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Pod::Simple::PullParser',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Pod::Simple::LinkSection',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'HTTP::Tiny',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'strict',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '5.008'
                            },
                            {
                              'module' => 'Storable',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Exporter',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Spec',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'IPC::Cmd',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Module::Load::Conditional',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'warnings',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'utf8',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Carp',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Find',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Builder',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'warnings',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'strict',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'lib',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0.88'
                            }
                          ],
          'distribution' => 'Test-Pod-LinkCheck-Lite',
          'download_url' => 'https://cpan.metacpan.org/authors/id/W/WY/WYANT/Test-Pod-LinkCheck-Lite-0.013.tar.gz',
          'id' => 'vcBuhPqON51pUMy50csuchagLuE',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'Test::Pod::LinkCheck::Lite',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Test POD links',
                          'author' => [
                                        'Thomas R. Wyant, III F<wyant at cpan dot org>'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'Module::Build version 0.4234, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Test-Pod-LinkCheck-Lite',
                          'no_index' => {
                                          'directory' => [
                                                           'inc',
                                                           't',
                                                           'xt',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ],
                                          'package' => [
                                                         'My_Parser'
                                                       ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'Test::More' => '0.88'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'lib' => '0',
                                                                          'strict' => '0',
                                                                          'warnings' => '0'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'B::Keywords' => '0',
                                                                        'Carp' => '0',
                                                                        'Exporter' => '0',
                                                                        'File::Find' => '0',
                                                                        'File::Spec' => '0',
                                                                        'HTTP::Tiny' => '0',
                                                                        'IPC::Cmd' => '0',
                                                                        'Module::Load::Conditional' => '0',
                                                                        'Pod::Perldoc' => '0',
                                                                        'Pod::Simple' => '0',
                                                                        'Pod::Simple::LinkSection' => '0',
                                                                        'Pod::Simple::PullParser' => '0',
                                                                        'Storable' => '0',
                                                                        'Test::Builder' => '0',
                                                                        'constant' => '0',
                                                                        'perl' => '5.008',
                                                                        'strict' => '0',
                                                                        'utf8' => '0',
                                                                        'warnings' => '0'
                                                                      }
                                                      }
                                       },
                          'provides' => {
                                          'Test::Pod::LinkCheck::Lite' => {
                                                                            'file' => 'lib/Test/Pod/LinkCheck/Lite.pm',
                                                                            'version' => '0.013'
                                                                          }
                                        },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'mailto' => 'wyant@cpan.org',
                                                             'web' => 'https://rt.cpan.org/Public/Dist/Display.html?Name=Test-Pod-LinkCheck-Lite'
                                                           },
                                           'license' => [
                                                          'http://dev.perl.org/licenses/'
                                                        ],
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'git://github.com/trwyant/perl-Test-Pod-LinkCheck-Lite.git',
                                                             'web' => 'https://github.com/trwyant/perl-Test-Pod-LinkCheck-Lite'
                                                           }
                                         },
                          'version' => '0.013',
                          'x_serialization_backend' => 'JSON::PP version 4.16'
                        },
          'name' => 'Test-Pod-LinkCheck-Lite-0.013',
          'provides' => [
                          'Test::Pod::LinkCheck::Lite'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'mailto' => 'wyant@cpan.org',
                                             'web' => 'https://rt.cpan.org/Public/Dist/Display.html?Name=Test-Pod-LinkCheck-Lite'
                                           },
                           'license' => [
                                          'http://dev.perl.org/licenses/'
                                        ],
                           'repository' => {
                                             'type' => 'git',
                                             'url' => 'git://github.com/trwyant/perl-Test-Pod-LinkCheck-Lite.git',
                                             'web' => 'https://github.com/trwyant/perl-Test-Pod-LinkCheck-Lite'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709473091,
                      'size' => 56928
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 0,
                       'pass' => 63,
                       'unknown' => 0
                     },
          'version' => '0.013',
          'version_numified' => '0.013'
        };
1;
