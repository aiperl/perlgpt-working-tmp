$distribution = $VAR1 = {
          '_id' => 'ZH1jbdVVLAQ5B0dWrVIul0mk1sI',
          '_type' => 'release',
          'abstract' => 'translation support module for greple',
          'archive' => 'App-Greple-xlate-0.31.tar.gz',
          'author' => 'UTASHIRO',
          'changes_file' => 'Changes',
          'checksum_md5' => '3b0e2a539813244245aaed9d49a3c8b9',
          'checksum_sha256' => '6886e6c6fcebb5b1c8658c9c6d4cfb8b468cbc48ee4c7b4a572cae70f3ea7483',
          'date' => '2024-03-01T10:34:06',
          'dependency' => [
                            {
                              'module' => 'Module::Build::Tiny',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0.035'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0.98'
                            },
                            {
                              'module' => 'Clipboard',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'List::Util',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.56'
                            },
                            {
                              'module' => 'Hash::Util',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'JSON',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => 'v5.14.0'
                            },
                            {
                              'module' => 'App::sdif',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '4.29'
                            },
                            {
                              'module' => 'App::Greple::msdoc',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.05'
                            },
                            {
                              'module' => 'File::Share',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Text::ANSI::Fold',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '2.2104'
                            },
                            {
                              'module' => 'App::optex::textconv',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.04'
                            },
                            {
                              'module' => 'App::Greple',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '9.0902'
                            },
                            {
                              'module' => 'App::Greple::xp',
                              'phase' => 'develop',
                              'relationship' => 'recommends',
                              'version' => '0.04'
                            },
                            {
                              'module' => 'App::Greple::subst::desumasu',
                              'phase' => 'develop',
                              'relationship' => 'recommends',
                              'version' => '0'
                            },
                            {
                              'module' => 'Pod::Markdown',
                              'phase' => 'develop',
                              'relationship' => 'recommends',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Pod',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1.41'
                            },
                            {
                              'module' => 'Test::CPAN::Meta',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Spellunker',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => 'v0.2.7'
                            },
                            {
                              'module' => 'Test::MinimumVersion::Fast',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.04'
                            },
                            {
                              'module' => 'Test::PAUSE::Permissions',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.07'
                            }
                          ],
          'distribution' => 'App-Greple-xlate',
          'download_url' => 'https://cpan.metacpan.org/authors/id/U/UT/UTASHIRO/App-Greple-xlate-0.31.tar.gz',
          'id' => 'ZH1jbdVVLAQ5B0dWrVIul0mk1sI',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'App::Greple::xlate',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'translation support module for greple',
                          'author' => [
                                        'Kazumasa Utashiro'
                                      ],
                          'dynamic_config' => 0,
                          'generated_by' => 'Minilla/v3.1.23, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'App-Greple-xlate',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'share',
                                                           'eg',
                                                           'examples',
                                                           'author',
                                                           'builder',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'configure' => {
                                                          'requires' => {
                                                                          'Module::Build::Tiny' => '0.035'
                                                                        }
                                                        },
                                         'develop' => {
                                                        'recommends' => {
                                                                          'App::Greple::subst::desumasu' => '0',
                                                                          'App::Greple::xp' => '0.04',
                                                                          'Pod::Markdown' => '0'
                                                                        },
                                                        'requires' => {
                                                                        'Test::CPAN::Meta' => '0',
                                                                        'Test::MinimumVersion::Fast' => '0.04',
                                                                        'Test::PAUSE::Permissions' => '0.07',
                                                                        'Test::Pod' => '1.41',
                                                                        'Test::Spellunker' => 'v0.2.7'
                                                                      }
                                                      },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'App::Greple' => '9.0902',
                                                                        'App::Greple::msdoc' => '1.05',
                                                                        'App::optex::textconv' => '1.04',
                                                                        'App::sdif' => '4.29',
                                                                        'Clipboard' => '0',
                                                                        'File::Share' => '0',
                                                                        'Hash::Util' => '0',
                                                                        'JSON' => '0',
                                                                        'List::Util' => '1.56',
                                                                        'Text::ANSI::Fold' => '2.2104',
                                                                        'perl' => 'v5.14.0'
                                                                      }
                                                      },
                                         'test' => {
                                                     'requires' => {
                                                                     'Test::More' => '0.98'
                                                                   }
                                                   }
                                       },
                          'provides' => {
                                          'App::Greple::xlate' => {
                                                                    'file' => 'lib/App/Greple/xlate.pm',
                                                                    'version' => '0.31'
                                                                  },
                                          'App::Greple::xlate::Cache' => {
                                                                           'file' => 'lib/App/Greple/xlate/Cache.pm'
                                                                         },
                                          'App::Greple::xlate::Lang' => {
                                                                          'file' => 'lib/App/Greple/xlate/Lang.pm'
                                                                        },
                                          'App::Greple::xlate::deepl' => {
                                                                           'file' => 'lib/App/Greple/xlate/deepl.pm',
                                                                           'version' => '0.31'
                                                                         },
                                          'App::Greple::xlate::gpt3' => {
                                                                          'file' => 'lib/App/Greple/xlate/gpt3.pm',
                                                                          'version' => '0.31'
                                                                        },
                                          'App::Greple::xlate::gpt4' => {
                                                                          'file' => 'lib/App/Greple/xlate/gpt4.pm',
                                                                          'version' => '0.31'
                                                                        }
                                        },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/kaz-utashiro/App-Greple-xlate/issues'
                                                           },
                                           'homepage' => 'https://github.com/kaz-utashiro/App-Greple-xlate',
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'https://github.com/kaz-utashiro/App-Greple-xlate.git',
                                                             'web' => 'https://github.com/kaz-utashiro/App-Greple-xlate'
                                                           }
                                         },
                          'version' => '0.31',
                          'x_authority' => 'cpan:UTASHIRO',
                          'x_contributors' => [
                                                'Kazumasa Utashiro <kaz@utashiro.com>'
                                              ],
                          'x_serialization_backend' => 'JSON::PP version 4.12',
                          'x_static_install' => 1
                        },
          'name' => 'App-Greple-xlate-0.31',
          'provides' => [
                          'App::Greple::xlate',
                          'App::Greple::xlate::Cache',
                          'App::Greple::xlate::Lang',
                          'App::Greple::xlate::deepl',
                          'App::Greple::xlate::gpt3',
                          'App::Greple::xlate::gpt4'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/kaz-utashiro/App-Greple-xlate/issues'
                                           },
                           'homepage' => 'https://github.com/kaz-utashiro/App-Greple-xlate',
                           'repository' => {
                                             'type' => 'git',
                                             'url' => 'https://github.com/kaz-utashiro/App-Greple-xlate.git',
                                             'web' => 'https://github.com/kaz-utashiro/App-Greple-xlate'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709289246,
                      'size' => 63038
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 2,
                       'pass' => 78,
                       'unknown' => 0
                     },
          'version' => '0.31',
          'version_numified' => '0.31'
        };
1;
