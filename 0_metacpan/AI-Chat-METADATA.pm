$distribution = $VAR1 = {
          '_id' => 'FtLAESno_JC7q__JLQ6QXaoBZ7A',
          '_type' => 'release',
          'abstract' => 'Interact with AI Chat APIs',
          'archive' => 'AI-Chat-0.2.tar.gz',
          'author' => 'BOD',
          'changes_file' => 'Changes',
          'checksum_md5' => '306f1d6b88db5f06d419a6453055bb06',
          'checksum_sha256' => '623dbbc83bc0de72a230b3348c6295e58008f3f31b73b64d208ba6193304dc2b',
          'date' => '2024-03-03T21:14:58',
          'dependency' => [
                            {
                              'module' => 'Test::More',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '5.010'
                            },
                            {
                              'module' => 'JSON::PP',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '2.00'
                            },
                            {
                              'module' => 'Carp',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.50'
                            },
                            {
                              'module' => 'HTTP::Tiny',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.014'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            }
                          ],
          'distribution' => 'AI-Chat',
          'download_url' => 'https://cpan.metacpan.org/authors/id/B/BO/BOD/AI-Chat-0.2.tar.gz',
          'id' => 'FtLAESno_JC7q__JLQ6QXaoBZ7A',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'AI::Chat',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Interact with AI Chat APIs',
                          'author' => [
                                        'Ian Boddison <bod@cpan.org>'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'ExtUtils::MakeMaker version 7.58, CPAN::Meta::Converter version 2.150010, CPAN::Meta::Converter version 2.150005',
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'AI-Chat',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'ExtUtils::MakeMaker' => '0'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'ExtUtils::MakeMaker' => '0'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'Carp' => '1.50',
                                                                        'HTTP::Tiny' => '0.014',
                                                                        'JSON::PP' => '2.00',
                                                                        'perl' => '5.010'
                                                                      }
                                                      },
                                         'test' => {
                                                     'requires' => {
                                                                     'Test::More' => '0'
                                                                   }
                                                   }
                                       },
                          'release_status' => 'stable',
                          'version' => '0.2',
                          'x_serialization_backend' => 'JSON::PP version 4.06'
                        },
          'name' => 'AI-Chat-0.2',
          'provides' => [
                          'AI::Chat'
                        ],
          'resources' => {},
          'stat' => {
                      'mode' => 33204,
                      'mtime' => 1709500498,
                      'size' => 5773
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 1,
                       'pass' => 61,
                       'unknown' => 0
                     },
          'version' => '0.2',
          'version_numified' => '0.2'
        };
1;
