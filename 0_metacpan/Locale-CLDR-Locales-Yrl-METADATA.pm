$distribution = $VAR1 = {
          '_id' => 'seaoPcF80msMsqaA7OPCkTW_tEc',
          '_type' => 'release',
          'abstract' => 'Locale::CLDR - Data Package ( Perl localization data for Nheengatu )',
          'archive' => 'Locale-CLDR-Locales-Yrl-v0.44.1.tar.gz',
          'author' => 'JGNI',
          'changes_file' => '',
          'checksum_md5' => '4a6311b655c7499c57008a48af552373',
          'checksum_sha256' => '8f387991ad8313642694bfb4f8f478d9bfa6ed41a49ed87e6bef233692d5d8a2',
          'date' => '2024-03-01T06:23:26',
          'dependency' => [
                            {
                              'module' => 'version',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.95'
                            },
                            {
                              'module' => 'Locale::CLDR',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => 'v0.44.1'
                            },
                            {
                              'module' => 'DateTime',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.72'
                            },
                            {
                              'module' => 'MooX::ClassAttribute',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.011'
                            },
                            {
                              'module' => 'Type::Tiny',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => 'v5.10.1'
                            },
                            {
                              'module' => 'Moo',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '2'
                            },
                            {
                              'module' => 'ok',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0.98'
                            },
                            {
                              'module' => 'Test::Exception',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Module::Build',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0.40'
                            }
                          ],
          'distribution' => 'Locale-CLDR-Locales-Yrl',
          'download_url' => 'https://cpan.metacpan.org/authors/id/J/JG/JGNI/Locale-CLDR-Locales-Yrl-v0.44.1.tar.gz',
          'id' => 'seaoPcF80msMsqaA7OPCkTW_tEc',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'Locale::CLDR::Locales::Yrl',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Locale::CLDR - Data Package ( Perl localization data for Nheengatu )',
                          'author' => [
                                        'John Imrie <john.imrie1@gmail.com>'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'Module::Build version 0.4231, CPAN::Meta::Converter version 2.150005',
                          'keywords' => [
                                          'locale',
                                          'CLDR',
                                          'locale-data-pack'
                                        ],
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Locale-CLDR-Locales-Yrl',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'Test::Exception' => '0',
                                                                      'Test::More' => '0.98',
                                                                      'ok' => '0'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'Module::Build' => '0.40'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'DateTime' => '0.72',
                                                                        'Locale::CLDR' => 'v0.44.1',
                                                                        'Moo' => '2',
                                                                        'MooX::ClassAttribute' => '0.011',
                                                                        'Type::Tiny' => '0',
                                                                        'perl' => 'v5.10.1',
                                                                        'version' => '0.95'
                                                                      }
                                                      }
                                       },
                          'provides' => {
                                          'Locale::CLDR::Locales::Yrl' => {
                                                                            'file' => 'lib/Locale/CLDR/Locales/Yrl.pm',
                                                                            'version' => 'v0.44.1'
                                                                          },
                                          'Locale::CLDR::Locales::Yrl::Latn' => {
                                                                                  'file' => 'lib/Locale/CLDR/Locales/Yrl/Latn.pm',
                                                                                  'version' => 'v0.44.1'
                                                                                },
                                          'Locale::CLDR::Locales::Yrl::Latn::Br' => {
                                                                                      'file' => 'lib/Locale/CLDR/Locales/Yrl/Latn/Br.pm',
                                                                                      'version' => 'v0.44.1'
                                                                                    },
                                          'Locale::CLDR::Locales::Yrl::Latn::Co' => {
                                                                                      'file' => 'lib/Locale/CLDR/Locales/Yrl/Latn/Co.pm',
                                                                                      'version' => 'v0.44.1'
                                                                                    },
                                          'Locale::CLDR::Locales::Yrl::Latn::Ve' => {
                                                                                      'file' => 'lib/Locale/CLDR/Locales/Yrl/Latn/Ve.pm',
                                                                                      'version' => 'v0.44.1'
                                                                                    }
                                        },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/ThePilgrim/perlcldr/issues'
                                                           },
                                           'homepage' => 'https://github.com/ThePilgrim/perlcldr',
                                           'repository' => {
                                                             'url' => 'https://github.com/ThePilgrim/perlcldr.git'
                                                           }
                                         },
                          'version' => 'v0.44.1',
                          'x_serialization_backend' => 'JSON::PP version 4.06'
                        },
          'name' => 'Locale-CLDR-Locales-Yrl-v0.44.1',
          'provides' => [
                          'Locale::CLDR::Locales::Yrl',
                          'Locale::CLDR::Locales::Yrl::Latn',
                          'Locale::CLDR::Locales::Yrl::Latn::Br',
                          'Locale::CLDR::Locales::Yrl::Latn::Co',
                          'Locale::CLDR::Locales::Yrl::Latn::Ve'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/ThePilgrim/perlcldr/issues'
                                           },
                           'homepage' => 'https://github.com/ThePilgrim/perlcldr',
                           'repository' => {
                                             'url' => 'https://github.com/ThePilgrim/perlcldr.git'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709274206,
                      'size' => 52000
                    },
          'status' => 'latest',
          'version' => 'v0.44.1',
          'version_numified' => '0.044001'
        };
1;
