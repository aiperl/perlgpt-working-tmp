$distribution = $VAR1 = {
          '_id' => 'mXS4RjrV3lbuWlFYow_Mp2SXR9U',
          '_type' => 'release',
          'abstract' => 'Interface to Deutsche Bahn Wagon Order API.',
          'archive' => 'Travel-Status-DE-DBWagenreihung-0.10.tar.gz',
          'author' => 'DERF',
          'changes_file' => 'Changelog',
          'checksum_md5' => 'e37e91b5058c25327117aa7b7ced582f',
          'checksum_sha256' => '86fa26d2d257685ab296a5cfe16375d85226995795f62d6cf792203a53be4004',
          'date' => '2024-03-04T05:08:28',
          'dependency' => [
                            {
                              'module' => 'LWP::UserAgent',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Class::Accessor',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Getopt::Long',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Travel::Status::DE::IRIS',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.2'
                            },
                            {
                              'module' => 'List::Util',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => 'v5.20.0'
                            },
                            {
                              'module' => 'Carp',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'JSON',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Pod',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Compile',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Module::Build',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0.4'
                            }
                          ],
          'distribution' => 'Travel-Status-DE-DBWagenreihung',
          'download_url' => 'https://cpan.metacpan.org/authors/id/D/DE/DERF/Travel-Status-DE-DBWagenreihung-0.10.tar.gz',
          'id' => 'mXS4RjrV3lbuWlFYow_Mp2SXR9U',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'Travel::Status::DE::DBWagenreihung',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Interface to Deutsche Bahn Wagon Order API.',
                          'author' => [
                                        'Copyright (C) 2018-2019 by Birte Kristina Friesel E<lt>derf@finalrewind.orgE<gt>'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'Module::Build version 0.4234, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Travel-Status-DE-DBWagenreihung',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'Test::Compile' => '0',
                                                                      'Test::More' => '0',
                                                                      'Test::Pod' => '0'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'Module::Build' => '0.4'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'Carp' => '0',
                                                                        'Class::Accessor' => '0',
                                                                        'Getopt::Long' => '0',
                                                                        'JSON' => '0',
                                                                        'LWP::UserAgent' => '0',
                                                                        'List::Util' => '0',
                                                                        'Travel::Status::DE::IRIS' => '1.2',
                                                                        'perl' => 'v5.20.0'
                                                                      }
                                                      }
                                       },
                          'provides' => {
                                          'Travel::Status::DE::DBWagenreihung' => {
                                                                                    'file' => 'lib/Travel/Status/DE/DBWagenreihung.pm',
                                                                                    'version' => '0.10'
                                                                                  },
                                          'Travel::Status::DE::DBWagenreihung::Section' => {
                                                                                             'file' => 'lib/Travel/Status/DE/DBWagenreihung/Section.pm',
                                                                                             'version' => '0.10'
                                                                                           },
                                          'Travel::Status::DE::DBWagenreihung::Wagon' => {
                                                                                           'file' => 'lib/Travel/Status/DE/DBWagenreihung/Wagon.pm',
                                                                                           'version' => '0.10'
                                                                                         }
                                        },
                          'release_status' => 'stable',
                          'resources' => {
                                           'license' => [
                                                          'http://dev.perl.org/licenses/'
                                                        ],
                                           'repository' => {
                                                             'url' => 'https://github.com/derf/Travel-Status-DE-DBWagenreihung'
                                                           }
                                         },
                          'version' => '0.10',
                          'x_serialization_backend' => 'JSON::PP version 4.16'
                        },
          'name' => 'Travel-Status-DE-DBWagenreihung-0.10',
          'provides' => [
                          'Travel::Status::DE::DBWagenreihung',
                          'Travel::Status::DE::DBWagenreihung::Section',
                          'Travel::Status::DE::DBWagenreihung::Wagon'
                        ],
          'resources' => {
                           'license' => [
                                          'http://dev.perl.org/licenses/'
                                        ],
                           'repository' => {
                                             'url' => 'https://github.com/derf/Travel-Status-DE-DBWagenreihung'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709528908,
                      'size' => 29818
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 7,
                       'pass' => 0,
                       'unknown' => 51
                     },
          'version' => '0.10',
          'version_numified' => '0.1'
        };
1;
