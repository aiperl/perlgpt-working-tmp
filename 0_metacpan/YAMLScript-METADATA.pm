$distribution = $VAR1 = {
          '_id' => 'BM7PPBGq6G5ocnXM1mkBa47IPA4',
          '_type' => 'release',
          'abstract' => 'Program in YAML',
          'archive' => 'YAMLScript-0.1.39.tar.gz',
          'author' => 'INGY',
          'changes_file' => 'Changes',
          'checksum_md5' => '7b6794047eccbde9905cb6ca5e1fb99d',
          'checksum_sha256' => 'f0e10cc33d6bb7a0a3937403d88c389a19468d3511356b044804fe3552bc6abf',
          'date' => '2024-03-02T16:32:55',
          'dependency' => [
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Cpanel::JSON::XS',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '4.37'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => 'v5.16.0'
                            },
                            {
                              'module' => 'FFI::CheckLib',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.31'
                            },
                            {
                              'module' => 'Alien::YAMLScript',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => 'v0.1.39'
                            },
                            {
                              'module' => 'FFI::Platypus',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '2.08'
                            },
                            {
                              'module' => 'Test2::V0',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0.000159'
                            },
                            {
                              'module' => 'Test::Pod',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1.41'
                            }
                          ],
          'distribution' => 'YAMLScript',
          'download_url' => 'https://cpan.metacpan.org/authors/id/I/IN/INGY/YAMLScript-0.1.39.tar.gz',
          'id' => 'BM7PPBGq6G5ocnXM1mkBa47IPA4',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'YAMLScript',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Program in YAML',
                          'author' => [
                                        "Ingy d\x{f6}t Net <ingy\@ingy.net>"
                                      ],
                          'dynamic_config' => 0,
                          'generated_by' => 'Dist::Zilla version 6.030, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'YAMLScript',
                          'no_index' => {
                                          'directory' => [
                                                           'example',
                                                           'inc',
                                                           't',
                                                           'xt',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'configure' => {
                                                          'requires' => {
                                                                          'ExtUtils::MakeMaker' => '0'
                                                                        }
                                                        },
                                         'develop' => {
                                                        'requires' => {
                                                                        'Test::Pod' => '1.41'
                                                                      }
                                                      },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'Alien::YAMLScript' => 'v0.1.39',
                                                                        'Cpanel::JSON::XS' => '4.37',
                                                                        'FFI::CheckLib' => '0.31',
                                                                        'FFI::Platypus' => '2.08',
                                                                        'perl' => 'v5.16.0'
                                                                      }
                                                      },
                                         'test' => {
                                                     'requires' => {
                                                                     'Test2::V0' => '0.000159'
                                                                   }
                                                   }
                                       },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/ingydotnet/yamlscript/issues'
                                                           },
                                           'homepage' => 'https://github.com/ingydotnet/yamlscript',
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'https://github.com/ingydotnet/yamlscript.git',
                                                             'web' => 'https://github.com/ingydotnet/yamlscript'
                                                           }
                                         },
                          'version' => '0.1.39',
                          'x_generated_by_perl' => 'v5.28.0',
                          'x_serialization_backend' => 'Cpanel::JSON::XS version 4.37',
                          'x_spdx_expression' => 'Artistic-1.0-Perl OR GPL-1.0-or-later'
                        },
          'name' => 'YAMLScript-0.1.39',
          'provides' => [
                          'YAMLScript',
                          'YAMLScript::Alien'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/ingydotnet/yamlscript/issues'
                                           },
                           'homepage' => 'https://github.com/ingydotnet/yamlscript',
                           'repository' => {
                                             'type' => 'git',
                                             'url' => 'https://github.com/ingydotnet/yamlscript.git',
                                             'web' => 'https://github.com/ingydotnet/yamlscript'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709397175,
                      'size' => 13445
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 22,
                       'na' => 4,
                       'pass' => 5,
                       'unknown' => 0
                     },
          'version' => '0.1.39',
          'version_numified' => '0.001039'
        };
1;
