$distribution = $VAR1 = {
          '_id' => 'QMeWjBj0Xjo2t1i0eS1Dko6SACQ',
          '_type' => 'release',
          'abstract' => 'An effort to make creating and using custom web components easier',
          'archive' => 'Mojolicious-Plugin-WebComponent-0.06.tar.gz',
          'author' => 'RES',
          'changes_file' => 'Changes',
          'checksum_md5' => '57a1d55496bc7822810783c49d6ee83c',
          'checksum_sha256' => 'fad8644a2e606c1107918eb67b4c92f67f3a7cb423a5fb287b452538b56da9d0',
          'date' => '2024-03-01T13:48:15',
          'dependency' => [
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => 'v5.20.0'
                            },
                            {
                              'module' => 'Mojolicious',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Module::Build::Tiny',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0.035'
                            },
                            {
                              'module' => 'Test::Pod',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1.41'
                            },
                            {
                              'module' => 'Test::CPAN::Meta',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::MinimumVersion::Fast',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.04'
                            },
                            {
                              'module' => 'Test::Spellunker',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => 'v0.2.7'
                            },
                            {
                              'module' => 'Test::PAUSE::Permissions',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.07'
                            }
                          ],
          'distribution' => 'Mojolicious-Plugin-WebComponent',
          'download_url' => 'https://cpan.metacpan.org/authors/id/R/RE/RES/Mojolicious-Plugin-WebComponent-0.06.tar.gz',
          'id' => 'QMeWjBj0Xjo2t1i0eS1Dko6SACQ',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'Mojolicious::Plugin::WebComponent',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'An effort to make creating and using custom web components easier',
                          'author' => [
                                        'Russell Shingleton <reshingleton@gmail.com>'
                                      ],
                          'dynamic_config' => 0,
                          'generated_by' => 'Minilla/v3.1.23, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Mojolicious-Plugin-WebComponent',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'share',
                                                           'eg',
                                                           'examples',
                                                           'author',
                                                           'builder',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'configure' => {
                                                          'requires' => {
                                                                          'Module::Build::Tiny' => '0.035'
                                                                        }
                                                        },
                                         'develop' => {
                                                        'requires' => {
                                                                        'Test::CPAN::Meta' => '0',
                                                                        'Test::MinimumVersion::Fast' => '0.04',
                                                                        'Test::PAUSE::Permissions' => '0.07',
                                                                        'Test::Pod' => '1.41',
                                                                        'Test::Spellunker' => 'v0.2.7'
                                                                      }
                                                      },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'Mojolicious' => '0',
                                                                        'perl' => 'v5.20.0'
                                                                      }
                                                      }
                                       },
                          'provides' => {
                                          'Mojolicious::Plugin::WebComponent' => {
                                                                                   'file' => 'lib/Mojolicious/Plugin/WebComponent.pm',
                                                                                   'version' => '0.06'
                                                                                 }
                                        },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/rshingleton/Mojolicious-Plugin-WebComponent/issues'
                                                           },
                                           'homepage' => 'https://github.com/rshingleton/Mojolicious-Plugin-WebComponent',
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'https://github.com/rshingleton/Mojolicious-Plugin-WebComponent.git',
                                                             'web' => 'https://github.com/rshingleton/Mojolicious-Plugin-WebComponent'
                                                           }
                                         },
                          'version' => '0.06',
                          'x_contributors' => [
                                                'shingler <shingler@oclc.org>'
                                              ],
                          'x_serialization_backend' => 'JSON::PP version 4.07',
                          'x_static_install' => 1
                        },
          'name' => 'Mojolicious-Plugin-WebComponent-0.06',
          'provides' => [
                          'Mojolicious::Plugin::WebComponent'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/rshingleton/Mojolicious-Plugin-WebComponent/issues'
                                           },
                           'homepage' => 'https://github.com/rshingleton/Mojolicious-Plugin-WebComponent',
                           'repository' => {
                                             'type' => 'git',
                                             'url' => 'https://github.com/rshingleton/Mojolicious-Plugin-WebComponent.git',
                                             'web' => 'https://github.com/rshingleton/Mojolicious-Plugin-WebComponent'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709300895,
                      'size' => 15223
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 6,
                       'na' => 7,
                       'pass' => 74,
                       'unknown' => 0
                     },
          'version' => '0.06',
          'version_numified' => '0.06'
        };
1;
