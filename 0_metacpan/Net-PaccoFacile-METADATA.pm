$distribution = $VAR1 = {
          '_id' => 'lPc5zETM_c2Cw3iOZ1_LsP8sFgA',
          '_type' => 'release',
          'abstract' => 'Perl library with MINIMAL interface to use PaccoFacile API.',
          'archive' => 'Net-PaccoFacile-0.1.0.tar.gz',
          'author' => 'ARTHAS',
          'changes_file' => '',
          'checksum_md5' => 'e20ae0ff72c807f202fed13e27592295',
          'checksum_sha256' => '99aa553d66ea4806a31711094213ca187cb8edffc86a254bfe9483e26c5169a7',
          'date' => '2024-03-01T16:16:12',
          'dependency' => [
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => 'v5.36.0'
                            },
                            {
                              'module' => 'Mojolicious',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '9.28'
                            },
                            {
                              'module' => 'Test::Exception',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Moo',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '2'
                            },
                            {
                              'module' => 'version',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'namespace::clean',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.27'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            }
                          ],
          'distribution' => 'Net-PaccoFacile',
          'download_url' => 'https://cpan.metacpan.org/authors/id/A/AR/ARTHAS/Net-PaccoFacile-0.1.0.tar.gz',
          'id' => 'lPc5zETM_c2Cw3iOZ1_LsP8sFgA',
          'license' => [
                         'artistic_2'
                       ],
          'main_module' => 'Net::PaccoFacile',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Perl library with MINIMAL interface to use PaccoFacile API.',
                          'author' => [
                                        'Michele Beltrame <mb@blendgroup.it>'
                                      ],
                          'dynamic_config' => '0',
                          'generated_by' => 'Dist::Zilla version 6.030, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'artistic_2'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Net-PaccoFacile',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {}
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'ExtUtils::MakeMaker' => '0'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'Mojolicious' => '9.28',
                                                                        'Moo' => '2',
                                                                        'Test::Exception' => '0',
                                                                        'namespace::clean' => '0.27',
                                                                        'perl' => 'v5.36.0',
                                                                        'version' => '0'
                                                                      }
                                                      }
                                       },
                          'release_status' => 'stable',
                          'version' => '0.1.0',
                          'x_generated_by_perl' => 'v5.38.0',
                          'x_serialization_backend' => 'YAML::Tiny version 1.74',
                          'x_spdx_expression' => 'Artistic-2.0'
                        },
          'name' => 'Net-PaccoFacile-0.1.0',
          'provides' => [
                          'Net::PaccoFacile'
                        ],
          'resources' => {},
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709309772,
                      'size' => 6272
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 43,
                       'pass' => 25,
                       'unknown' => 0
                     },
          'version' => '0.1.0',
          'version_numified' => '0.001'
        };
1;
