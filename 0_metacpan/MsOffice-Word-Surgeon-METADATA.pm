$distribution = $VAR1 = {
          '_id' => '8yYZP884_qlj_rYEvxStkDavkSk',
          '_type' => 'release',
          'abstract' => 'tamper with the guts of Microsoft docx documents, with regexes',
          'archive' => 'MsOffice-Word-Surgeon-2.05.tar.gz',
          'author' => 'DAMI',
          'changes_file' => 'Changes',
          'checksum_md5' => '9f40230c48a88b031574453bc8c46735',
          'checksum_sha256' => 'c10170875af773ba488f29bc5949c7c84dc54a094d4833191f87c745c2a66eac',
          'date' => '2024-03-02T22:18:41',
          'dependency' => [
                            {
                              'module' => 'Encode',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'POSIX',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'MooseX::StrictConstructor',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Archive::Zip',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Carp::Clan',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => 'v5.24.0'
                            },
                            {
                              'module' => 'Exporter',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'List::Util',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'XML::LibXML',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Moose',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'namespace::clean',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Module::Build',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0.4004'
                            }
                          ],
          'distribution' => 'MsOffice-Word-Surgeon',
          'download_url' => 'https://cpan.metacpan.org/authors/id/D/DA/DAMI/MsOffice-Word-Surgeon-2.05.tar.gz',
          'id' => '8yYZP884_qlj_rYEvxStkDavkSk',
          'license' => [
                         'artistic_2'
                       ],
          'main_module' => 'MsOffice::Word::Surgeon',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'tamper with the guts of Microsoft docx documents, with regexes',
                          'author' => [
                                        'DAMI <dami@cpan.org>'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'Module::Build version 0.4232, CPAN::Meta::Converter version 2.150005',
                          'license' => [
                                         'artistic_2'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'MsOffice-Word-Surgeon',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'configure' => {
                                                          'requires' => {
                                                                          'Module::Build' => '0.4004'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'Archive::Zip' => '0',
                                                                        'Carp::Clan' => '0',
                                                                        'Encode' => '0',
                                                                        'Exporter' => '0',
                                                                        'List::Util' => '0',
                                                                        'Moose' => '0',
                                                                        'MooseX::StrictConstructor' => '0',
                                                                        'POSIX' => '0',
                                                                        'XML::LibXML' => '0',
                                                                        'namespace::clean' => '0',
                                                                        'perl' => 'v5.24.0'
                                                                      }
                                                      },
                                         'test' => {
                                                     'requires' => {
                                                                     'Test::More' => '0'
                                                                   }
                                                   }
                                       },
                          'provides' => {
                                          'MsOffice::Word::Surgeon' => {
                                                                         'file' => 'lib/MsOffice/Word/Surgeon.pm',
                                                                         'version' => '2.05'
                                                                       },
                                          'MsOffice::Word::Surgeon::PackagePart' => {
                                                                                      'file' => 'lib/MsOffice/Word/Surgeon/PackagePart.pm',
                                                                                      'version' => '2.05'
                                                                                    },
                                          'MsOffice::Word::Surgeon::Revision' => {
                                                                                   'file' => 'lib/MsOffice/Word/Surgeon/Revision.pm',
                                                                                   'version' => '2.05'
                                                                                 },
                                          'MsOffice::Word::Surgeon::Run' => {
                                                                              'file' => 'lib/MsOffice/Word/Surgeon/Run.pm',
                                                                              'version' => '2.05'
                                                                            },
                                          'MsOffice::Word::Surgeon::Text' => {
                                                                               'file' => 'lib/MsOffice/Word/Surgeon/Text.pm',
                                                                               'version' => '2.05'
                                                                             },
                                          'MsOffice::Word::Surgeon::Utils' => {
                                                                                'file' => 'lib/MsOffice/Word/Surgeon/Utils.pm',
                                                                                'version' => '2.05'
                                                                              }
                                        },
                          'release_status' => 'stable',
                          'resources' => {
                                           'license' => [
                                                          'http://www.perlfoundation.org/artistic_license_2_0'
                                                        ],
                                           'repository' => {
                                                             'url' => 'https://github.com/damil/MsOffice-Word-Surgeon'
                                                           }
                                         },
                          'version' => '2.05',
                          'x_serialization_backend' => 'JSON::PP version 4.05'
                        },
          'name' => 'MsOffice-Word-Surgeon-2.05',
          'provides' => [
                          'MsOffice::Word::Surgeon',
                          'MsOffice::Word::Surgeon::PackagePart',
                          'MsOffice::Word::Surgeon::Revision',
                          'MsOffice::Word::Surgeon::Run',
                          'MsOffice::Word::Surgeon::Text',
                          'MsOffice::Word::Surgeon::Utils'
                        ],
          'resources' => {
                           'license' => [
                                          'http://www.perlfoundation.org/artistic_license_2_0'
                                        ],
                           'repository' => {
                                             'url' => 'https://github.com/damil/MsOffice-Word-Surgeon'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709417921,
                      'size' => 65891
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 23,
                       'pass' => 67,
                       'unknown' => 0
                     },
          'version' => '2.05',
          'version_numified' => '2.05'
        };
1;
