$distribution = $VAR1 = {
          '_id' => 'k4P0DabsBcNGLuCkFqQOzs2ZJ2U',
          '_type' => 'release',
          'abstract' => 'S/MIME message signing, verification, encryption and decryption',
          'archive' => 'Crypt-SMIME-0.29.tar.gz',
          'author' => 'MIKAGE',
          'changes_file' => 'Changes',
          'checksum_md5' => '745e9d8422ae69676808955f60b78a36',
          'checksum_sha256' => 'ca83070673e94e6d1b2b51bb8ff85fbe7e25b5279149bc35cb40b74a23025845',
          'date' => '2024-03-04T09:26:07',
          'dependency' => [
                            {
                              'module' => 'ExtUtils::Constant',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0.23'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ExtUtils::PkgConfig',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ExtUtils::CChecker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ExtUtils::PkgConfig',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Exception',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'XSLoader',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            }
                          ],
          'distribution' => 'Crypt-SMIME',
          'download_url' => 'https://cpan.metacpan.org/authors/id/M/MI/MIKAGE/Crypt-SMIME-0.29.tar.gz',
          'id' => 'k4P0DabsBcNGLuCkFqQOzs2ZJ2U',
          'license' => [
                         'unknown'
                       ],
          'main_module' => 'Crypt::SMIME',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'S/MIME message signing, verification, encryption and decryption',
                          'author' => [
                                        'Ymirlink <tl@tripletail.jp>'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'ExtUtils::MakeMaker version 7.64, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'unknown'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Crypt-SMIME',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'ExtUtils::PkgConfig' => '0',
                                                                      'Test::Exception' => '0',
                                                                      'Test::More' => '0'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'ExtUtils::CChecker' => '0',
                                                                          'ExtUtils::Constant' => '0.23',
                                                                          'ExtUtils::MakeMaker' => '0',
                                                                          'ExtUtils::PkgConfig' => '0'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'XSLoader' => '0'
                                                                      }
                                                      }
                                       },
                          'release_status' => 'stable',
                          'version' => '0.29',
                          'x_serialization_backend' => 'JSON::PP version 4.07'
                        },
          'name' => 'Crypt-SMIME-0.29',
          'provides' => [
                          'Crypt::SMIME'
                        ],
          'resources' => {},
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709544367,
                      'size' => 27963
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 0,
                       'pass' => 56,
                       'unknown' => 1
                     },
          'version' => '0.29',
          'version_numified' => '0.29'
        };
1;
