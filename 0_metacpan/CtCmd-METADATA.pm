$distribution = $VAR1 = {
          '_id' => 'f9lWmrAKNbLH_unws0agwFmcnhg',
          '_type' => 'release',
          'abstract' => 'Perl extension for IBM Rational ClearCase',
          'archive' => 'CtCmd-1.17.tar.gz',
          'author' => 'RATL',
          'changes_file' => 'ChangeLog',
          'checksum_md5' => '3bccc5176569bdf19b59701bc39c0e50',
          'checksum_sha256' => '27764b8f7a6afdea0a33ed823365976a6d72a3b87008d971308b4f254a94becb',
          'date' => '2024-03-01T18:25:24',
          'dependency' => [
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            }
                          ],
          'distribution' => 'CtCmd',
          'download_url' => 'https://cpan.metacpan.org/authors/id/R/RA/RATL/CtCmd-1.17.tar.gz',
          'id' => 'f9lWmrAKNbLH_unws0agwFmcnhg',
          'license' => [
                         'unknown'
                       ],
          'main_module' => 'ClearCase::CtCmd',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'unknown',
                          'author' => [
                                        'unknown'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'ExtUtils::MakeMaker version 7.24, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'unknown'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'ClearCase-CtCmd',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'ExtUtils::MakeMaker' => '0'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'ExtUtils::MakeMaker' => '0'
                                                                        }
                                                        }
                                       },
                          'release_status' => 'stable',
                          'version' => '1.17',
                          'x_serialization_backend' => 'JSON::PP version 2.27400_02'
                        },
          'name' => 'CtCmd-1.17',
          'provides' => [
                          'CC::Activity',
                          'CC::AdminVob',
                          'CC::Baseline',
                          'CC::BaselineDiff',
                          'CC::CC',
                          'CC::CompHlink',
                          'CC::Component',
                          'CC::CompositeDiff',
                          'CC::DiffBl',
                          'CC::Element',
                          'CC::File',
                          'CC::Folder',
                          'CC::Project',
                          'CC::Stream',
                          'CC::UCMObject',
                          'CC::Version',
                          'CC::View',
                          'CC::Vob',
                          'CC::VobObject',
                          'ClearCase::CtCmd'
                        ],
          'resources' => {},
          'stat' => {
                      'mode' => 33204,
                      'mtime' => 1709317524,
                      'size' => 39715
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 0,
                       'pass' => 0,
                       'unknown' => 54
                     },
          'version' => '1.17',
          'version_numified' => '1.17'
        };
1;
