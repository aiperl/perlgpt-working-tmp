$distribution = $VAR1 = {
          '_id' => 'kFYh_00yUeYWEzjUHU_dQ6pFmqs',
          '_type' => 'release',
          'abstract' => 'Locale::CLDR - Data Package (  )',
          'archive' => 'Bundle-Locale-CLDR-Subsaharanafrica-0.44.1.tar.gz',
          'author' => 'JGNI',
          'changes_file' => '',
          'checksum_md5' => '35c8ad0d0afb6f723bd075b3d745efb4',
          'checksum_sha256' => 'f216d90aad69da7c4f4d4df9dd318309be23c739836a9dd28b39195f6e18eab5',
          'date' => '2024-03-01T06:50:47',
          'dependency' => [
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => 'v5.10.1'
                            },
                            {
                              'module' => 'Moo',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '2'
                            },
                            {
                              'module' => 'version',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.95'
                            },
                            {
                              'module' => 'DateTime',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.72'
                            },
                            {
                              'module' => 'Type::Tiny',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'MooX::ClassAttribute',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.011'
                            },
                            {
                              'module' => 'ok',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0.98'
                            },
                            {
                              'module' => 'Test::Exception',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Module::Build',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0.40'
                            }
                          ],
          'distribution' => 'Bundle-Locale-CLDR-Subsaharanafrica',
          'download_url' => 'https://cpan.metacpan.org/authors/id/J/JG/JGNI/Bundle-Locale-CLDR-Subsaharanafrica-0.44.1.tar.gz',
          'id' => 'kFYh_00yUeYWEzjUHU_dQ6pFmqs',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'Bundle::Locale::CLDR::Subsaharanafrica',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Locale::CLDR - Data Package (  )',
                          'author' => [
                                        'John Imrie <john.imrie1@gmail.com>'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'Module::Build version 0.4231, CPAN::Meta::Converter version 2.150005',
                          'keywords' => [
                                          'locale',
                                          'CLDR',
                                          'locale-data-pack'
                                        ],
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Bundle-Locale-CLDR-Subsaharanafrica',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'Test::Exception' => '0',
                                                                      'Test::More' => '0.98',
                                                                      'ok' => '0'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'Module::Build' => '0.40'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'DateTime' => '0.72',
                                                                        'Moo' => '2',
                                                                        'MooX::ClassAttribute' => '0.011',
                                                                        'Type::Tiny' => '0',
                                                                        'perl' => 'v5.10.1',
                                                                        'version' => '0.95'
                                                                      }
                                                      }
                                       },
                          'provides' => {
                                          'Bundle::Locale::CLDR::Subsaharanafrica' => {
                                                                                        'file' => 'lib/Bundle/Locale/CLDR/Subsaharanafrica.pm',
                                                                                        'version' => 'v0.44.1'
                                                                                      }
                                        },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/ThePilgrim/perlcldr/issues'
                                                           },
                                           'homepage' => 'https://github.com/ThePilgrim/perlcldr',
                                           'repository' => {
                                                             'url' => 'https://github.com/ThePilgrim/perlcldr.git'
                                                           }
                                         },
                          'version' => 'v0.44.1',
                          'x_serialization_backend' => 'JSON::PP version 4.06'
                        },
          'name' => 'Bundle-Locale-CLDR-Subsaharanafrica-0.44.1',
          'provides' => [
                          'Bundle::Locale::CLDR::Subsaharanafrica'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/ThePilgrim/perlcldr/issues'
                                           },
                           'homepage' => 'https://github.com/ThePilgrim/perlcldr',
                           'repository' => {
                                             'url' => 'https://github.com/ThePilgrim/perlcldr.git'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709275847,
                      'size' => 1984
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 1,
                       'pass' => 0,
                       'unknown' => 84
                     },
          'version' => 'v0.44.1',
          'version_numified' => '0.044001'
        };
1;
