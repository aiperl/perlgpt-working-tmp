$distribution = $VAR1 = {
          '_id' => 'JgGa47UEvPsuLyOU1MOqj_U9Sy8',
          '_type' => 'release',
          'abstract' => 'Locale::CLDR - Data Package ( Perl localization data for Soga )',
          'archive' => 'Locale-CLDR-Locales-Xog-v0.44.1.tar.gz',
          'author' => 'JGNI',
          'changes_file' => '',
          'checksum_md5' => 'd2348dda0ad59d0768a3185f018ded5b',
          'checksum_sha256' => '3b57563010b9e5cca899899f3dd6dce03f22e0b9f409f453f34d071533ee5f5b',
          'date' => '2024-03-01T06:19:50',
          'dependency' => [
                            {
                              'module' => 'Test::More',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0.98'
                            },
                            {
                              'module' => 'ok',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Exception',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Locale::CLDR',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => 'v0.44.1'
                            },
                            {
                              'module' => 'MooX::ClassAttribute',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.011'
                            },
                            {
                              'module' => 'version',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.95'
                            },
                            {
                              'module' => 'Moo',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '2'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => 'v5.10.1'
                            },
                            {
                              'module' => 'DateTime',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.72'
                            },
                            {
                              'module' => 'Type::Tiny',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Module::Build',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0.40'
                            }
                          ],
          'distribution' => 'Locale-CLDR-Locales-Xog',
          'download_url' => 'https://cpan.metacpan.org/authors/id/J/JG/JGNI/Locale-CLDR-Locales-Xog-v0.44.1.tar.gz',
          'id' => 'JgGa47UEvPsuLyOU1MOqj_U9Sy8',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'Locale::CLDR::Locales::Xog',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Locale::CLDR - Data Package ( Perl localization data for Soga )',
                          'author' => [
                                        'John Imrie <john.imrie1@gmail.com>'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'Module::Build version 0.4231, CPAN::Meta::Converter version 2.150010',
                          'keywords' => [
                                          'locale',
                                          'CLDR',
                                          'locale-data-pack'
                                        ],
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Locale-CLDR-Locales-Xog',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'Test::Exception' => '0',
                                                                      'Test::More' => '0.98',
                                                                      'ok' => '0'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'Module::Build' => '0.40'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'DateTime' => '0.72',
                                                                        'Locale::CLDR' => 'v0.44.1',
                                                                        'Moo' => '2',
                                                                        'MooX::ClassAttribute' => '0.011',
                                                                        'Type::Tiny' => '0',
                                                                        'perl' => 'v5.10.1',
                                                                        'version' => '0.95'
                                                                      }
                                                      }
                                       },
                          'provides' => {
                                          'Locale::CLDR::Locales::Xog' => {
                                                                            'file' => 'lib/Locale/CLDR/Locales/Xog.pm',
                                                                            'version' => 'v0.44.1'
                                                                          },
                                          'Locale::CLDR::Locales::Xog::Latn' => {
                                                                                  'file' => 'lib/Locale/CLDR/Locales/Xog/Latn.pm',
                                                                                  'version' => 'v0.44.1'
                                                                                },
                                          'Locale::CLDR::Locales::Xog::Latn::Ug' => {
                                                                                      'file' => 'lib/Locale/CLDR/Locales/Xog/Latn/Ug.pm',
                                                                                      'version' => 'v0.44.1'
                                                                                    }
                                        },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/ThePilgrim/perlcldr/issues'
                                                           },
                                           'homepage' => 'https://github.com/ThePilgrim/perlcldr',
                                           'repository' => {
                                                             'url' => 'https://github.com/ThePilgrim/perlcldr.git'
                                                           }
                                         },
                          'version' => 'v0.44.1',
                          'x_serialization_backend' => 'JSON::PP version 4.06'
                        },
          'name' => 'Locale-CLDR-Locales-Xog-v0.44.1',
          'provides' => [
                          'Locale::CLDR::Locales::Xog',
                          'Locale::CLDR::Locales::Xog::Latn',
                          'Locale::CLDR::Locales::Xog::Latn::Ug'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/ThePilgrim/perlcldr/issues'
                                           },
                           'homepage' => 'https://github.com/ThePilgrim/perlcldr',
                           'repository' => {
                                             'url' => 'https://github.com/ThePilgrim/perlcldr.git'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709273990,
                      'size' => 8632
                    },
          'status' => 'latest',
          'version' => 'v0.44.1',
          'version_numified' => '0.044001'
        };
1;
