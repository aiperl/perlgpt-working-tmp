$distribution = $VAR1 = {
          '_id' => '_R3N5ckpk191uzMgqP3eCnrY_Yg',
          '_type' => 'release',
          'abstract' => 'Easy to use REPL with existing lexical support and DWIM tab completion.',
          'archive' => 'Runtime-Debugger-0.14.tar.gz',
          'author' => 'TIMKA',
          'changes_file' => 'Changes',
          'checksum_md5' => 'de1eb859c3e9b9130f3063821a20d665',
          'checksum_sha256' => '1f78dd8b302b4aa046c7870460936b36a40c63c30638192d437d2facd29827fe',
          'date' => '2024-03-04T13:49:24',
          'dependency' => [
                            {
                              'module' => 'Test::More',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Module::Build',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0.4004'
                            },
                            {
                              'module' => 'Class::Tiny',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.008'
                            },
                            {
                              'module' => 'Term::ReadLine::Gnu',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.44'
                            },
                            {
                              'module' => 'PadWalker',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '2.5'
                            },
                            {
                              'module' => 'Data::Printer',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.002001'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '5.014'
                            }
                          ],
          'distribution' => 'Runtime-Debugger',
          'download_url' => 'https://cpan.metacpan.org/authors/id/T/TI/TIMKA/Runtime-Debugger-0.14.tar.gz',
          'id' => '_R3N5ckpk191uzMgqP3eCnrY_Yg',
          'license' => [
                         'artistic_2'
                       ],
          'main_module' => 'Runtime::Debugger',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Easy to use REPL with existing lexical support and DWIM tab completion.',
                          'author' => [
                                        'Tim Potapov <tim.potapov[AT]gmail.com>'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'Module::Build version 0.4234, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'artistic_2'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Runtime-Debugger',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'configure' => {
                                                          'requires' => {
                                                                          'Module::Build' => '0.4004'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'Class::Tiny' => '1.008',
                                                                        'Data::Printer' => '1.002001',
                                                                        'PadWalker' => '2.5',
                                                                        'Term::ReadLine::Gnu' => '1.44',
                                                                        'perl' => '5.014'
                                                                      }
                                                      },
                                         'test' => {
                                                     'requires' => {
                                                                     'Test::More' => '0'
                                                                   }
                                                   }
                                       },
                          'provides' => {
                                          'Runtime::Debugger' => {
                                                                   'file' => 'lib/Runtime/Debugger.pm',
                                                                   'version' => '0.14'
                                                                 }
                                        },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/poti1/runtime-debugger/issues'
                                                           },
                                           'license' => [
                                                          'http://opensource.org/licenses/artistic-license-2.0.php'
                                                        ],
                                           'repository' => {
                                                             'url' => 'https://github.com/poti1/runtime-debugger'
                                                           }
                                         },
                          'version' => '0.14',
                          'x_serialization_backend' => 'JSON::PP version 4.04'
                        },
          'name' => 'Runtime-Debugger-0.14',
          'provides' => [
                          'Runtime::Debugger'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/poti1/runtime-debugger/issues'
                                           },
                           'license' => [
                                          'http://opensource.org/licenses/artistic-license-2.0.php'
                                        ],
                           'repository' => {
                                             'url' => 'https://github.com/poti1/runtime-debugger'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709560164,
                      'size' => 19788
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 0,
                       'pass' => 37,
                       'unknown' => 0
                     },
          'version' => '0.14',
          'version_numified' => '0.14'
        };
1;
