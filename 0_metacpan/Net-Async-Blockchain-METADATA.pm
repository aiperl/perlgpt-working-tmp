$distribution = $VAR1 = {
          '_id' => 'lmOznmoRbSM5MG8vCTw6hT25DeA',
          '_type' => 'release',
          'abstract' => 'base for blockchain subscription clients.',
          'archive' => 'Net-Async-Blockchain-0.004.tar.gz',
          'author' => 'DERIV',
          'changes_file' => 'Changes',
          'checksum_md5' => 'a75564f42d6fc7197e196e9472dd3d6d',
          'checksum_sha256' => '4ff6760d6cd475a359b8a967c55add10c5c87d0df5eb303fb8601c5072847ae0',
          'date' => '2024-03-04T04:51:34',
          'dependency' => [
                            {
                              'module' => 'constant',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Protocol::WebSocket::Request',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Net::Async::WebSocket::Client',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Ryu::Async',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.011'
                            },
                            {
                              'module' => 'IO::Async::Timer::Periodic',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.72'
                            },
                            {
                              'module' => 'warnings',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '5.024'
                            },
                            {
                              'module' => 'Net::Async::HTTP',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.43'
                            },
                            {
                              'module' => 'JSON::MaybeUTF8',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.002'
                            },
                            {
                              'module' => 'Net::Async::WebSocket',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.13'
                            },
                            {
                              'module' => 'IO::Async::Handle',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Digest::Keccak',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'ZMQ::Constants',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'curry',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'IO::Async::SSL',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Syntax::Keyword::Try',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.09'
                            },
                            {
                              'module' => 'indirect',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.37'
                            },
                            {
                              'module' => 'Future::AsyncAwait',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.23'
                            },
                            {
                              'module' => 'ZMQ::LibZMQ3',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.19'
                            },
                            {
                              'module' => 'IO::Async::Notifier',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'parent',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'URI',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'strict',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Socket',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Math::BigInt',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.999814'
                            },
                            {
                              'module' => 'Test::Mojibake',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Devel::Cover::Report::Codecov',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.14'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::PerlTidy',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Version',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1'
                            },
                            {
                              'module' => 'Test::CPAN::Changes',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.19'
                            },
                            {
                              'module' => 'Software::License::Perl_5',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla::Plugin::Test::Perl::Critic',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0.96'
                            },
                            {
                              'module' => 'Test::Portability::Files',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::NoTabs',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::EOL',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Devel::Cover',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1.23'
                            },
                            {
                              'module' => 'Test::CPAN::Meta',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Perl::Critic',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Pod',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '1.41'
                            },
                            {
                              'module' => 'Dist::Zilla::PluginBundle::Author::DERIV',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::MinimumVersion',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Dist::Zilla',
                              'phase' => 'develop',
                              'relationship' => 'requires',
                              'version' => '5'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'IO::Async::Loop',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::MockModule',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::CheckDeps',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0.010'
                            },
                            {
                              'module' => 'IO::Handle',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'IPC::Open3',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'File::Spec',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Net::Async::WebSocket::Server',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0.98'
                            },
                            {
                              'module' => 'Test::Fatal',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '5.024'
                            },
                            {
                              'module' => 'IO::Async::Test',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::TCP',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'CPAN::Meta',
                              'phase' => 'test',
                              'relationship' => 'recommends',
                              'version' => '2.120900'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '7.64'
                            }
                          ],
          'distribution' => 'Net-Async-Blockchain',
          'download_url' => 'https://cpan.metacpan.org/authors/id/D/DE/DERIV/Net-Async-Blockchain-0.004.tar.gz',
          'id' => 'lmOznmoRbSM5MG8vCTw6hT25DeA',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'Net::Async::Blockchain',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'base for blockchain subscription clients.',
                          'author' => [
                                        'DERIV <DERIV@cpan.org>'
                                      ],
                          'dynamic_config' => 0,
                          'generated_by' => 'Dist::Zilla version 6.029, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'Net-Async-Blockchain',
                          'no_index' => {
                                          'directory' => [
                                                           'eg',
                                                           'share',
                                                           'shares',
                                                           't',
                                                           'xt',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'configure' => {
                                                          'requires' => {
                                                                          'ExtUtils::MakeMaker' => '7.64'
                                                                        }
                                                        },
                                         'develop' => {
                                                        'requires' => {
                                                                        'Devel::Cover' => '1.23',
                                                                        'Devel::Cover::Report::Codecov' => '0.14',
                                                                        'Dist::Zilla' => '5',
                                                                        'Dist::Zilla::Plugin::PerlTidy' => '0',
                                                                        'Dist::Zilla::Plugin::Test::Perl::Critic' => '0',
                                                                        'Dist::Zilla::PluginBundle::Author::DERIV' => '0',
                                                                        'Software::License::Perl_5' => '0',
                                                                        'Test::CPAN::Changes' => '0.19',
                                                                        'Test::CPAN::Meta' => '0',
                                                                        'Test::EOL' => '0',
                                                                        'Test::MinimumVersion' => '0',
                                                                        'Test::Mojibake' => '0',
                                                                        'Test::More' => '0.96',
                                                                        'Test::NoTabs' => '0',
                                                                        'Test::Perl::Critic' => '0',
                                                                        'Test::Pod' => '1.41',
                                                                        'Test::Portability::Files' => '0',
                                                                        'Test::Version' => '1'
                                                                      }
                                                      },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'Digest::Keccak' => '0',
                                                                        'Future::AsyncAwait' => '0.23',
                                                                        'IO::Async::Handle' => '0',
                                                                        'IO::Async::Notifier' => '0',
                                                                        'IO::Async::SSL' => '0',
                                                                        'IO::Async::Timer::Periodic' => '0.72',
                                                                        'JSON::MaybeUTF8' => '1.002',
                                                                        'Math::BigInt' => '1.999814',
                                                                        'Net::Async::HTTP' => '0.43',
                                                                        'Net::Async::WebSocket' => '0.13',
                                                                        'Net::Async::WebSocket::Client' => '0',
                                                                        'Protocol::WebSocket::Request' => '0',
                                                                        'Ryu::Async' => '0.011',
                                                                        'Socket' => '0',
                                                                        'Syntax::Keyword::Try' => '0.09',
                                                                        'URI' => '0',
                                                                        'ZMQ::Constants' => '0',
                                                                        'ZMQ::LibZMQ3' => '1.19',
                                                                        'constant' => '0',
                                                                        'curry' => '0',
                                                                        'indirect' => '0.37',
                                                                        'parent' => '0',
                                                                        'perl' => '5.024',
                                                                        'strict' => '0',
                                                                        'warnings' => '0'
                                                                      }
                                                      },
                                         'test' => {
                                                     'recommends' => {
                                                                       'CPAN::Meta' => '2.120900'
                                                                     },
                                                     'requires' => {
                                                                     'ExtUtils::MakeMaker' => '0',
                                                                     'File::Spec' => '0',
                                                                     'IO::Async::Loop' => '0',
                                                                     'IO::Async::Test' => '0',
                                                                     'IO::Handle' => '0',
                                                                     'IPC::Open3' => '0',
                                                                     'Net::Async::WebSocket::Server' => '0',
                                                                     'Test::CheckDeps' => '0.010',
                                                                     'Test::Fatal' => '0',
                                                                     'Test::MockModule' => '0',
                                                                     'Test::More' => '0.98',
                                                                     'Test::TCP' => '0',
                                                                     'perl' => '5.024'
                                                                   }
                                                   }
                                       },
                          'provides' => {
                                          'Net::Async::Blockchain' => {
                                                                        'file' => 'lib/Net/Async/Blockchain.pm',
                                                                        'version' => '0.004'
                                                                      },
                                          'Net::Async::Blockchain::BTC' => {
                                                                             'file' => 'lib/Net/Async/Blockchain/BTC.pm',
                                                                             'version' => '0.004'
                                                                           },
                                          'Net::Async::Blockchain::Client::RPC' => {
                                                                                     'file' => 'lib/Net/Async/Blockchain/Client/RPC.pm',
                                                                                     'version' => '0.004'
                                                                                   },
                                          'Net::Async::Blockchain::Client::RPC::BTC' => {
                                                                                          'file' => 'lib/Net/Async/Blockchain/Client/RPC/BTC.pm',
                                                                                          'version' => '0.004'
                                                                                        },
                                          'Net::Async::Blockchain::Client::RPC::ETH' => {
                                                                                          'file' => 'lib/Net/Async/Blockchain/Client/RPC/ETH.pm',
                                                                                          'version' => '0.004'
                                                                                        },
                                          'Net::Async::Blockchain::Client::RPC::Omni' => {
                                                                                           'file' => 'lib/Net/Async/Blockchain/Client/RPC/Omni.pm',
                                                                                           'version' => '0.004'
                                                                                         },
                                          'Net::Async::Blockchain::Client::Websocket' => {
                                                                                           'file' => 'lib/Net/Async/Blockchain/Client/Websocket.pm',
                                                                                           'version' => '0.004'
                                                                                         },
                                          'Net::Async::Blockchain::Client::ZMQ' => {
                                                                                     'file' => 'lib/Net/Async/Blockchain/Client/ZMQ.pm',
                                                                                     'version' => '0.004'
                                                                                   },
                                          'Net::Async::Blockchain::ETH' => {
                                                                             'file' => 'lib/Net/Async/Blockchain/ETH.pm',
                                                                             'version' => '0.004'
                                                                           },
                                          'Net::Async::Blockchain::Transaction' => {
                                                                                     'file' => 'lib/Net/Async/Blockchain/Transaction.pm',
                                                                                     'version' => '0.004'
                                                                                   }
                                        },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/binary-com/perl-Net-Async-Blockchain/issues'
                                                           },
                                           'homepage' => 'https://github.com/binary-com/perl-Net-Async-Blockchain',
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'https://github.com/binary-com/perl-Net-Async-Blockchain.git',
                                                             'web' => 'https://github.com/binary-com/perl-Net-Async-Blockchain'
                                                           }
                                         },
                          'version' => '0.004',
                          'x_authority' => 'cpan:DERIV',
                          'x_contributors' => [
                                                'Reginaldo Costa <53163145+reginaldo-binary@users.noreply.github.com>',
                                                'Mohanad Zarzour <51309057+Binary-Mohanad@users.noreply.github.com>',
                                                'Tanya Sahni <54654631+binary-tanya@users.noreply.github.com>',
                                                'Arshad <arshad@binary.com>',
                                                'Reginaldo Costa <regcostajr@gmail.com>',
                                                'Zak B. Elep <zakame@zakame.net>',
                                                'Arshad Munir <55420460+arshad-binary@users.noreply.github.com>',
                                                'chylli-binary <52912308+chylli-binary@users.noreply.github.com>',
                                                'kathleen-deriv <122329285+lim-deriv@users.noreply.github.com>',
                                                'Arshad Munir <55420460+arshad-deriv@users.noreply.github.com>',
                                                'mukesh-deriv <85932084+mukesh-deriv@users.noreply.github.com>',
                                                'nihal-deriv <105702884+nihal-deriv@users.noreply.github.com>',
                                                'Paul Evans <leonerd@leonerd.org.uk>',
                                                'Reginaldo Costa <53163145+reginaldo-deriv@users.noreply.github.com>',
                                                'Tom Molesworth <tom@binary.com>'
                                              ],
                          'x_generated_by_perl' => 'v5.26.2',
                          'x_serialization_backend' => 'Cpanel::JSON::XS version 4.32',
                          'x_spdx_expression' => 'Artistic-1.0-Perl OR GPL-1.0-or-later'
                        },
          'name' => 'Net-Async-Blockchain-0.004',
          'provides' => [
                          'Net::Async::Blockchain',
                          'Net::Async::Blockchain::BTC',
                          'Net::Async::Blockchain::Client::RPC',
                          'Net::Async::Blockchain::Client::RPC::BTC',
                          'Net::Async::Blockchain::Client::RPC::ETH',
                          'Net::Async::Blockchain::Client::RPC::Omni',
                          'Net::Async::Blockchain::Client::Websocket',
                          'Net::Async::Blockchain::Client::ZMQ',
                          'Net::Async::Blockchain::ETH',
                          'Net::Async::Blockchain::Transaction'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/binary-com/perl-Net-Async-Blockchain/issues'
                                           },
                           'homepage' => 'https://github.com/binary-com/perl-Net-Async-Blockchain',
                           'repository' => {
                                             'type' => 'git',
                                             'url' => 'https://github.com/binary-com/perl-Net-Async-Blockchain.git',
                                             'web' => 'https://github.com/binary-com/perl-Net-Async-Blockchain'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709527894,
                      'size' => 27705
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 19,
                       'pass' => 16,
                       'unknown' => 0
                     },
          'version' => '0.004',
          'version_numified' => '0.004'
        };
1;
