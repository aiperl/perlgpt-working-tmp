$distribution = $VAR1 = {
          '_id' => 'J_9nrWXvFA_1MfJg8ow0XnKQC9E',
          '_type' => 'release',
          'abstract' => 'SPVM Language',
          'archive' => 'SPVM-0.989082.tar.gz',
          'author' => 'KIMOTO',
          'changes_file' => 'Changes',
          'checksum_md5' => '55d1181dc21233ba35c3f7497575663b',
          'checksum_sha256' => '98d518353c5968116f87ed6db48d013cad102e298dcd265a6075dc729e3e7cb7',
          'date' => '2024-03-01T06:43:46',
          'dependency' => [
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0.92'
                            },
                            {
                              'module' => 'ExtUtils::CBuilder',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.280236'
                            },
                            {
                              'module' => 'Time::Piece',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.12'
                            },
                            {
                              'module' => 'JSON::PP',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '2.27105'
                            },
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            }
                          ],
          'distribution' => 'SPVM',
          'download_url' => 'https://cpan.metacpan.org/authors/id/K/KI/KIMOTO/SPVM-0.989082.tar.gz',
          'id' => 'J_9nrWXvFA_1MfJg8ow0XnKQC9E',
          'license' => [
                         'mit'
                       ],
          'main_module' => 'SPVM',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'SPVM Language',
                          'author' => [
                                        'Yuki Kimoto<kimoto.yuki@gmail.com>'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'ExtUtils::MakeMaker version 7.64, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'mit'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'SPVM',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'ExtUtils::MakeMaker' => '0'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'ExtUtils::MakeMaker' => '0'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'ExtUtils::CBuilder' => '0.280236',
                                                                        'JSON::PP' => '2.27105',
                                                                        'Time::Piece' => '1.12'
                                                                      }
                                                      },
                                         'test' => {
                                                     'requires' => {
                                                                     'Test::More' => '0.92'
                                                                   }
                                                   }
                                       },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/yuki-kimoto/SPVM/issues'
                                                           },
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'https://github.com/yuki-kimoto/SPVM.git',
                                                             'web' => 'https://github.com/yuki-kimoto/SPVM'
                                                           }
                                         },
                          'version' => '0.989082',
                          'x_serialization_backend' => 'JSON::PP version 4.07'
                        },
          'name' => 'SPVM-0.989082',
          'provides' => [
                          'SPVM',
                          'SPVM::Address',
                          'SPVM::Array',
                          'SPVM::BlessedObject',
                          'SPVM::BlessedObject::Array',
                          'SPVM::BlessedObject::Class',
                          'SPVM::BlessedObject::String',
                          'SPVM::Bool',
                          'SPVM::Builder',
                          'SPVM::Builder::API',
                          'SPVM::Builder::CC',
                          'SPVM::Builder::CompileInfo',
                          'SPVM::Builder::Compiler',
                          'SPVM::Builder::Config',
                          'SPVM::Builder::Config::Exe',
                          'SPVM::Builder::Env',
                          'SPVM::Builder::Exe',
                          'SPVM::Builder::LibInfo',
                          'SPVM::Builder::LinkInfo',
                          'SPVM::Builder::ObjectFileInfo',
                          'SPVM::Builder::Resource',
                          'SPVM::Builder::Runtime',
                          'SPVM::Builder::Stack',
                          'SPVM::Builder::Util',
                          'SPVM::Builder::Util::API',
                          'SPVM::Byte',
                          'SPVM::ByteList',
                          'SPVM::Callback',
                          'SPVM::Cloneable',
                          'SPVM::Cloner',
                          'SPVM::CommandInfo',
                          'SPVM::Comparator',
                          'SPVM::Comparator::Double',
                          'SPVM::Comparator::Float',
                          'SPVM::Comparator::Int',
                          'SPVM::Comparator::Long',
                          'SPVM::Comparator::String',
                          'SPVM::Dist',
                          'SPVM::Double',
                          'SPVM::DoubleList',
                          'SPVM::Env',
                          'SPVM::EqualityChecker',
                          'SPVM::EqualityChecker::Address',
                          'SPVM::Error',
                          'SPVM::Error::Compile',
                          'SPVM::Error::NotSupported',
                          'SPVM::Error::System',
                          'SPVM::Error::Unicode::InvalidUTF8',
                          'SPVM::ExchangeAPI',
                          'SPVM::ExchangeAPI::Class',
                          'SPVM::ExchangeAPI::Error',
                          'SPVM::Float',
                          'SPVM::FloatList',
                          'SPVM::Fn',
                          'SPVM::Fn::Resource',
                          'SPVM::Format',
                          'SPVM::Global',
                          'SPVM::Hash',
                          'SPVM::Hash::Entry',
                          'SPVM::Immutable::ByteList',
                          'SPVM::Immutable::DoubleList',
                          'SPVM::Immutable::FloatList',
                          'SPVM::Immutable::IntList',
                          'SPVM::Immutable::LongList',
                          'SPVM::Immutable::ShortList',
                          'SPVM::Immutable::StringList',
                          'SPVM::Int',
                          'SPVM::IntList',
                          'SPVM::List',
                          'SPVM::Long',
                          'SPVM::LongList',
                          'SPVM::Native',
                          'SPVM::Native::Arg',
                          'SPVM::Native::BasicType',
                          'SPVM::Native::ClassFile',
                          'SPVM::Native::ClassVar',
                          'SPVM::Native::Compiler',
                          'SPVM::Native::Env',
                          'SPVM::Native::Field',
                          'SPVM::Native::Method',
                          'SPVM::Native::MethodCall',
                          'SPVM::Native::Runtime',
                          'SPVM::Native::Stack',
                          'SPVM::Point',
                          'SPVM::Point3D',
                          'SPVM::Runtime',
                          'SPVM::Scope::Guard',
                          'SPVM::Short',
                          'SPVM::ShortList',
                          'SPVM::Sort',
                          'SPVM::Stack',
                          'SPVM::StringBuffer',
                          'SPVM::StringList',
                          'SPVM::Stringable',
                          'SPVM::Stringer',
                          'SPVM::Sync::Mutex'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/yuki-kimoto/SPVM/issues'
                                           },
                           'repository' => {
                                             'type' => 'git',
                                             'url' => 'https://github.com/yuki-kimoto/SPVM.git',
                                             'web' => 'https://github.com/yuki-kimoto/SPVM'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709275426,
                      'size' => 1116543
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 24,
                       'na' => 0,
                       'pass' => 72,
                       'unknown' => 1
                     },
          'version' => '0.989082',
          'version_numified' => '0.989082'
        };
1;
