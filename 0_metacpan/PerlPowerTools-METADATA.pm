$distribution = $VAR1 = {
          '_id' => 'DYTVOKjabQOX3M__nv2SJCQR5rc',
          '_type' => 'release',
          'abstract' => 'BSD utilities written in pure Perl',
          'archive' => 'PerlPowerTools-1.044.tar.gz',
          'author' => 'BRIANDFOY',
          'changes_file' => 'Changes',
          'checksum_md5' => 'c1cbe61ebebcf0eb1f58460435fa6973',
          'checksum_sha256' => 'cba3adfd2da4bb4355eb0c3c4c3dc9a94a493772bad1584691c36b4f93439060',
          'date' => '2024-03-03T09:13:52',
          'dependency' => [
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '6.64'
                            },
                            {
                              'module' => 'File::Spec::Functions',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'App::a2p',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '5.008'
                            },
                            {
                              'module' => 'MIME::Parser',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'App::find2perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::Warnings',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'IPC::Run3',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '1'
                            },
                            {
                              'module' => 'Test::Pod',
                              'phase' => 'test',
                              'relationship' => 'requires',
                              'version' => '0'
                            }
                          ],
          'distribution' => 'PerlPowerTools',
          'download_url' => 'https://cpan.metacpan.org/authors/id/B/BR/BRIANDFOY/PerlPowerTools-1.044.tar.gz',
          'id' => 'DYTVOKjabQOX3M__nv2SJCQR5rc',
          'license' => [
                         'perl_5'
                       ],
          'main_module' => 'PerlPowerTools',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'BSD utilities written in pure Perl',
                          'author' => [
                                        'brian d foy <briandfoy@pobox.com>'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'ExtUtils::MakeMaker version 7.70, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'perl_5'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'PerlPowerTools',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {}
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'ExtUtils::MakeMaker' => '6.64',
                                                                          'File::Spec::Functions' => '0'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'App::a2p' => '0',
                                                                        'App::find2perl' => '0',
                                                                        'MIME::Parser' => '0',
                                                                        'perl' => '5.008'
                                                                      }
                                                      },
                                         'test' => {
                                                     'requires' => {
                                                                     'IPC::Run3' => '0',
                                                                     'Test::More' => '1',
                                                                     'Test::Pod' => '0',
                                                                     'Test::Warnings' => '0'
                                                                   }
                                                   }
                                       },
                          'release_status' => 'stable',
                          'resources' => {
                                           'bugtracker' => {
                                                             'web' => 'https://github.com/briandfoy/PerlPowerTools/issues'
                                                           },
                                           'homepage' => 'https://www.perlpowertools.com/',
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'https://github.com/briandfoy/PerlPowerTools',
                                                             'web' => 'https://github.com/briandfoy/PerlPowerTools'
                                                           }
                                         },
                          'version' => '1.044',
                          'x_serialization_backend' => 'JSON::PP version 4.16'
                        },
          'name' => 'PerlPowerTools-1.044',
          'provides' => [
                          'PerlPowerTools',
                          'PerlPowerTools::SymbolicMode',
                          'ppt'
                        ],
          'resources' => {
                           'bugtracker' => {
                                             'web' => 'https://github.com/briandfoy/PerlPowerTools/issues'
                                           },
                           'homepage' => 'https://www.perlpowertools.com/',
                           'repository' => {
                                             'type' => 'git',
                                             'url' => 'https://github.com/briandfoy/PerlPowerTools',
                                             'web' => 'https://github.com/briandfoy/PerlPowerTools'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709457232,
                      'size' => 400948
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 2,
                       'na' => 0,
                       'pass' => 93,
                       'unknown' => 0
                     },
          'version' => '1.044',
          'version_numified' => '1.044'
        };
1;
