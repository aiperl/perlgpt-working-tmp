$distribution = $VAR1 = {
          '_id' => '_UH89uJNZ19QoUOvRsDWRuejjGY',
          '_type' => 'release',
          'abstract' => 'Interface to Google JSON/Atom Custom Search.',
          'archive' => 'WWW-Google-CustomSearch-0.40.tar.gz',
          'author' => 'MANWAR',
          'changes_file' => 'Changes',
          'checksum_md5' => '47da9c3e7b1bc706092145a6ad37d2bd',
          'checksum_sha256' => 'd8265c3e1994a2b6a350a554d9e7cf329e9dbc751091dfe3c9d05f8559a14834',
          'date' => '2024-03-01T16:25:25',
          'dependency' => [
                            {
                              'module' => 'ExtUtils::MakeMaker',
                              'phase' => 'configure',
                              'relationship' => 'requires',
                              'version' => '0'
                            },
                            {
                              'module' => 'namespace::autoclean',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.28'
                            },
                            {
                              'module' => 'Moo',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '2.000000'
                            },
                            {
                              'module' => 'perl',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '5.006'
                            },
                            {
                              'module' => 'WWW::Google::UserAgent',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '0.23'
                            },
                            {
                              'module' => 'URI',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.65'
                            },
                            {
                              'module' => 'Type::Tiny',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '1.000005'
                            },
                            {
                              'module' => 'JSON',
                              'phase' => 'runtime',
                              'relationship' => 'requires',
                              'version' => '2.53'
                            },
                            {
                              'module' => 'Test::More',
                              'phase' => 'build',
                              'relationship' => 'requires',
                              'version' => '0'
                            }
                          ],
          'distribution' => 'WWW-Google-CustomSearch',
          'download_url' => 'https://cpan.metacpan.org/authors/id/M/MA/MANWAR/WWW-Google-CustomSearch-0.40.tar.gz',
          'id' => '_UH89uJNZ19QoUOvRsDWRuejjGY',
          'license' => [
                         'artistic_2'
                       ],
          'main_module' => 'WWW::Google::CustomSearch',
          'maturity' => 'released',
          'metadata' => {
                          'abstract' => 'Interface to Google JSON/Atom Custom Search.',
                          'author' => [
                                        'Mohammad S Anwar <mohammad.anwar@yahoo.com>'
                                      ],
                          'dynamic_config' => 1,
                          'generated_by' => 'ExtUtils::MakeMaker version 7.34, CPAN::Meta::Converter version 2.150010',
                          'license' => [
                                         'artistic_2'
                                       ],
                          'meta-spec' => {
                                           'url' => 'http://search.cpan.org/perldoc?CPAN::Meta::Spec',
                                           'version' => 2
                                         },
                          'name' => 'WWW-Google-CustomSearch',
                          'no_index' => {
                                          'directory' => [
                                                           't',
                                                           'inc',
                                                           't',
                                                           'xt',
                                                           'inc',
                                                           'local',
                                                           'perl5',
                                                           'fatlib',
                                                           'example',
                                                           'blib',
                                                           'examples',
                                                           'eg'
                                                         ]
                                        },
                          'prereqs' => {
                                         'build' => {
                                                      'requires' => {
                                                                      'Test::More' => '0'
                                                                    }
                                                    },
                                         'configure' => {
                                                          'requires' => {
                                                                          'ExtUtils::MakeMaker' => '0'
                                                                        }
                                                        },
                                         'runtime' => {
                                                        'requires' => {
                                                                        'JSON' => '2.53',
                                                                        'Moo' => '2.000000',
                                                                        'Type::Tiny' => '1.000005',
                                                                        'URI' => '1.65',
                                                                        'WWW::Google::UserAgent' => '0.23',
                                                                        'namespace::autoclean' => '0.28',
                                                                        'perl' => '5.006'
                                                                      }
                                                      }
                                       },
                          'provides' => {
                                          'WWW::Google::CustomSearch' => {
                                                                           'file' => 'lib/WWW/Google/CustomSearch.pm',
                                                                           'version' => '0.40'
                                                                         },
                                          'WWW::Google::CustomSearch::Item' => {
                                                                                 'file' => 'lib/WWW/Google/CustomSearch/Item.pm',
                                                                                 'version' => '0.40'
                                                                               },
                                          'WWW::Google::CustomSearch::Page' => {
                                                                                 'file' => 'lib/WWW/Google/CustomSearch/Page.pm',
                                                                                 'version' => '0.40'
                                                                               },
                                          'WWW::Google::CustomSearch::Params' => {
                                                                                   'file' => 'lib/WWW/Google/CustomSearch/Params.pm',
                                                                                   'version' => '0.40'
                                                                                 },
                                          'WWW::Google::CustomSearch::Request' => {
                                                                                    'file' => 'lib/WWW/Google/CustomSearch/Request.pm',
                                                                                    'version' => '0.40'
                                                                                  },
                                          'WWW::Google::CustomSearch::Result' => {
                                                                                   'file' => 'lib/WWW/Google/CustomSearch/Result.pm',
                                                                                   'version' => '0.40'
                                                                                 }
                                        },
                          'release_status' => 'stable',
                          'resources' => {
                                           'repository' => {
                                                             'type' => 'git',
                                                             'url' => 'https://github.com/manwar/WWW-Google-CustomSearch.git',
                                                             'web' => 'https://github.com/manwar/WWW-Google-CustomSearch'
                                                           }
                                         },
                          'version' => '0.40',
                          'x_serialization_backend' => 'JSON::PP version 4.02'
                        },
          'name' => 'WWW-Google-CustomSearch-0.40',
          'provides' => [
                          'WWW::Google::CustomSearch',
                          'WWW::Google::CustomSearch::Item',
                          'WWW::Google::CustomSearch::Page',
                          'WWW::Google::CustomSearch::Params',
                          'WWW::Google::CustomSearch::Request',
                          'WWW::Google::CustomSearch::Result'
                        ],
          'resources' => {
                           'repository' => {
                                             'type' => 'git',
                                             'url' => 'https://github.com/manwar/WWW-Google-CustomSearch.git',
                                             'web' => 'https://github.com/manwar/WWW-Google-CustomSearch'
                                           }
                         },
          'stat' => {
                      'mode' => 33188,
                      'mtime' => 1709310325,
                      'size' => 27658
                    },
          'status' => 'latest',
          'tests' => {
                       'fail' => 0,
                       'na' => 0,
                       'pass' => 94,
                       'unknown' => 0
                     },
          'version' => '0.40',
          'version_numified' => '0.4'
        };
1;
