$distributions_rejected_count = 72;
$distributions_rejected_names = $VAR1 = [
          'Acme-CPANModules-LoremIpsum',
          'Acme-CPANModules-OpeningFileInApp',
          'Alien-CFITSIO',
          'Alien-SeqAlignment-parasail',
          'Alien-YAMLScript',
          'Bundle-Locale-CLDR-Africa',
          'Bundle-Locale-CLDR-Americas',
          'Bundle-Locale-CLDR-Asia',
          'Bundle-Locale-CLDR-Australasia',
          'Bundle-Locale-CLDR-Caribbean',
          'Bundle-Locale-CLDR-Centralamerica',
          'Bundle-Locale-CLDR-Centralasia',
          'Bundle-Locale-CLDR-Easternafrica',
          'Bundle-Locale-CLDR-Easternasia',
          'Bundle-Locale-CLDR-Easterneurope',
          'Bundle-Locale-CLDR-Europe',
          'Bundle-Locale-CLDR-Europeanunion',
          'Bundle-Locale-CLDR-Eurozone',
          'Bundle-Locale-CLDR-Everything',
          'Bundle-Locale-CLDR-Latinamerica',
          'Bundle-Locale-CLDR-Melanesia',
          'Bundle-Locale-CLDR-Micronesianregion',
          'Bundle-Locale-CLDR-Middleafrica',
          'Bundle-Locale-CLDR-Northamerica',
          'Bundle-Locale-CLDR-Northernafrica',
          'Bundle-Locale-CLDR-Northernamerica',
          'Bundle-Locale-CLDR-Northerneurope',
          'Bundle-Locale-CLDR-Oceania',
          'Bundle-Locale-CLDR-Outlyingoceania',
          'Bundle-Locale-CLDR-Polynesia',
          'Bundle-Locale-CLDR-Southamerica',
          'Bundle-Locale-CLDR-Southeastasia',
          'Bundle-Locale-CLDR-Southernafrica',
          'Bundle-Locale-CLDR-Southernasia',
          'Bundle-Locale-CLDR-Southerneurope',
          'Bundle-Locale-CLDR-Subsaharanafrica',
          'Bundle-Locale-CLDR-Unitednations',
          'Bundle-Locale-CLDR-Westernafrica',
          'Bundle-Locale-CLDR-Westernasia',
          'Bundle-Locale-CLDR-Westerneurope',
          'Bundle-Locale-CLDR-World',
          'Business-ISBN-Data',
          'CGI',
          'CPAN-Audit',
          'Crypt-SMIME',
          'CtCmd',
          'Dist-Setup',
          'Locale-CLDR-Locales-Vo',
          'Locale-CLDR-Locales-Vun',
          'Locale-CLDR-Locales-Wa',
          'Locale-CLDR-Locales-Wae',
          'Locale-CLDR-Locales-Wal',
          'Locale-CLDR-Locales-Wbp',
          'Locale-CLDR-Locales-Wo',
          'Locale-CLDR-Locales-Xh',
          'Locale-CLDR-Locales-Xnr',
          'Locale-CLDR-Locales-Xog',
          'Locale-CLDR-Locales-Yav',
          'Locale-CLDR-Locales-Yi',
          'Locale-CLDR-Locales-Yo',
          'Locale-CLDR-Locales-Yrl',
          'Locale-CLDR-Locales-Yue',
          'Locale-CLDR-Locales-Za',
          'Locale-CLDR-Locales-Zgh',
          'Locale-CLDR-Locales-Zu',
          'PerlPowerTools',
          'SPVM',
          'Sisimai',
          'Sys-Virt',
          'Test-HTTP-LocalServer',
          'Travel-Status-DE-DBWagenreihung',
          'YAMLScript'
        ];

$distributions_rejected = $VAR1 = {
          'Acme-CPANModules-LoremIpsum' => {
                                             'filtered' => 'FILTERING: rejected distribution namespace \'Acme\', Acme-CPANModules-LoremIpsum'
                                           },
          'Acme-CPANModules-OpeningFileInApp' => {
                                                   'filtered' => 'FILTERING: rejected distribution namespace \'Acme\', Acme-CPANModules-OpeningFileInApp'
                                                 },
          'Alien-CFITSIO' => {
                               'filtered' => 'FILTERING: missing or incomplete CPAN Testers data, Alien-CFITSIO'
                             },
          'Alien-SeqAlignment-parasail' => {
                                             'filtered' => 'FILTERING: too few CPAN Testers, Alien-SeqAlignment-parasail'
                                           },
          'Alien-YAMLScript' => {
                                  'filtered' => 'FILTERING: too many failing CPAN Testers, Alien-YAMLScript'
                                },
          'Bundle-Locale-CLDR-Africa' => {
                                           'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Africa'
                                         },
          'Bundle-Locale-CLDR-Americas' => {
                                             'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Americas'
                                           },
          'Bundle-Locale-CLDR-Asia' => {
                                         'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Asia'
                                       },
          'Bundle-Locale-CLDR-Australasia' => {
                                                'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Australasia'
                                              },
          'Bundle-Locale-CLDR-Caribbean' => {
                                              'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Caribbean'
                                            },
          'Bundle-Locale-CLDR-Centralamerica' => {
                                                   'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Centralamerica'
                                                 },
          'Bundle-Locale-CLDR-Centralasia' => {
                                                'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Centralasia'
                                              },
          'Bundle-Locale-CLDR-Easternafrica' => {
                                                  'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Easternafrica'
                                                },
          'Bundle-Locale-CLDR-Easternasia' => {
                                                'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Easternasia'
                                              },
          'Bundle-Locale-CLDR-Easterneurope' => {
                                                  'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Easterneurope'
                                                },
          'Bundle-Locale-CLDR-Europe' => {
                                           'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Europe'
                                         },
          'Bundle-Locale-CLDR-Europeanunion' => {
                                                  'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Europeanunion'
                                                },
          'Bundle-Locale-CLDR-Eurozone' => {
                                             'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Eurozone'
                                           },
          'Bundle-Locale-CLDR-Everything' => {
                                               'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Everything'
                                             },
          'Bundle-Locale-CLDR-Latinamerica' => {
                                                 'filtered' => 'FILTERING: missing or incomplete CPAN Testers data, Bundle-Locale-CLDR-Latinamerica'
                                               },
          'Bundle-Locale-CLDR-Melanesia' => {
                                              'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Melanesia'
                                            },
          'Bundle-Locale-CLDR-Micronesianregion' => {
                                                      'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Micronesianregion'
                                                    },
          'Bundle-Locale-CLDR-Middleafrica' => {
                                                 'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Middleafrica'
                                               },
          'Bundle-Locale-CLDR-Northamerica' => {
                                                 'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Northamerica'
                                               },
          'Bundle-Locale-CLDR-Northernafrica' => {
                                                   'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Northernafrica'
                                                 },
          'Bundle-Locale-CLDR-Northernamerica' => {
                                                    'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Northernamerica'
                                                  },
          'Bundle-Locale-CLDR-Northerneurope' => {
                                                   'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Northerneurope'
                                                 },
          'Bundle-Locale-CLDR-Oceania' => {
                                            'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Oceania'
                                          },
          'Bundle-Locale-CLDR-Outlyingoceania' => {
                                                    'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Outlyingoceania'
                                                  },
          'Bundle-Locale-CLDR-Polynesia' => {
                                              'filtered' => 'FILTERING: missing or incomplete CPAN Testers data, Bundle-Locale-CLDR-Polynesia'
                                            },
          'Bundle-Locale-CLDR-Southamerica' => {
                                                 'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Southamerica'
                                               },
          'Bundle-Locale-CLDR-Southeastasia' => {
                                                  'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Southeastasia'
                                                },
          'Bundle-Locale-CLDR-Southernafrica' => {
                                                   'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Southernafrica'
                                                 },
          'Bundle-Locale-CLDR-Southernasia' => {
                                                 'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Southernasia'
                                               },
          'Bundle-Locale-CLDR-Southerneurope' => {
                                                   'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Southerneurope'
                                                 },
          'Bundle-Locale-CLDR-Subsaharanafrica' => {
                                                     'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Subsaharanafrica'
                                                   },
          'Bundle-Locale-CLDR-Unitednations' => {
                                                  'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Unitednations'
                                                },
          'Bundle-Locale-CLDR-Westernafrica' => {
                                                  'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Westernafrica'
                                                },
          'Bundle-Locale-CLDR-Westernasia' => {
                                                'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Westernasia'
                                              },
          'Bundle-Locale-CLDR-Westerneurope' => {
                                                  'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-Westerneurope'
                                                },
          'Bundle-Locale-CLDR-World' => {
                                          'filtered' => 'FILTERING: too few CPAN Testers, Bundle-Locale-CLDR-World'
                                        },
          'Business-ISBN-Data' => {
                                    'filtered' => 'FILTERING: bad individual CPANTS Kwalitee score \'use_strict\', Business-ISBN-Data'
                                  },
          'CGI' => {
                     'filtered' => 'FILTERING: bad individual CPANTS Kwalitee score \'use_strict\', CGI'
                   },
          'CPAN-Audit' => {
                            'filtered' => 'FILTERING: bad individual CPANTS Kwalitee score \'use_strict\', CPAN-Audit'
                          },
          'Crypt-SMIME' => {
                             'filtered' => 'FILTERING: rejected all distribution licenses (\'unknown\'), Crypt-SMIME'
                           },
          'CtCmd' => {
                       'filtered' => 'FILTERING: too few CPAN Testers, CtCmd'
                     },
          'Dist-Setup' => {
                            'filtered' => 'FILTERING: bad individual CPANTS Kwalitee score \'has_abstract_in_pod\', Dist-Setup'
                          },
          'Locale-CLDR-Locales-Vo' => {
                                        'filtered' => 'FILTERING: missing or incomplete CPAN Testers data, Locale-CLDR-Locales-Vo'
                                      },
          'Locale-CLDR-Locales-Vun' => {
                                         'filtered' => 'FILTERING: missing or incomplete CPAN Testers data, Locale-CLDR-Locales-Vun'
                                       },
          'Locale-CLDR-Locales-Wa' => {
                                        'filtered' => 'FILTERING: missing or incomplete CPAN Testers data, Locale-CLDR-Locales-Wa'
                                      },
          'Locale-CLDR-Locales-Wae' => {
                                         'filtered' => 'FILTERING: missing or incomplete CPAN Testers data, Locale-CLDR-Locales-Wae'
                                       },
          'Locale-CLDR-Locales-Wal' => {
                                         'filtered' => 'FILTERING: missing or incomplete CPAN Testers data, Locale-CLDR-Locales-Wal'
                                       },
          'Locale-CLDR-Locales-Wbp' => {
                                         'filtered' => 'FILTERING: missing or incomplete CPAN Testers data, Locale-CLDR-Locales-Wbp'
                                       },
          'Locale-CLDR-Locales-Wo' => {
                                        'filtered' => 'FILTERING: missing or incomplete CPAN Testers data, Locale-CLDR-Locales-Wo'
                                      },
          'Locale-CLDR-Locales-Xh' => {
                                        'filtered' => 'FILTERING: missing or incomplete CPAN Testers data, Locale-CLDR-Locales-Xh'
                                      },
          'Locale-CLDR-Locales-Xnr' => {
                                         'filtered' => 'FILTERING: missing or incomplete CPAN Testers data, Locale-CLDR-Locales-Xnr'
                                       },
          'Locale-CLDR-Locales-Xog' => {
                                         'filtered' => 'FILTERING: missing or incomplete CPAN Testers data, Locale-CLDR-Locales-Xog'
                                       },
          'Locale-CLDR-Locales-Yav' => {
                                         'filtered' => 'FILTERING: missing or incomplete CPAN Testers data, Locale-CLDR-Locales-Yav'
                                       },
          'Locale-CLDR-Locales-Yi' => {
                                        'filtered' => 'FILTERING: missing or incomplete CPAN Testers data, Locale-CLDR-Locales-Yi'
                                      },
          'Locale-CLDR-Locales-Yo' => {
                                        'filtered' => 'FILTERING: missing or incomplete CPAN Testers data, Locale-CLDR-Locales-Yo'
                                      },
          'Locale-CLDR-Locales-Yrl' => {
                                         'filtered' => 'FILTERING: missing or incomplete CPAN Testers data, Locale-CLDR-Locales-Yrl'
                                       },
          'Locale-CLDR-Locales-Yue' => {
                                         'filtered' => 'FILTERING: missing or incomplete CPAN Testers data, Locale-CLDR-Locales-Yue'
                                       },
          'Locale-CLDR-Locales-Za' => {
                                        'filtered' => 'FILTERING: missing or incomplete CPAN Testers data, Locale-CLDR-Locales-Za'
                                      },
          'Locale-CLDR-Locales-Zgh' => {
                                         'filtered' => 'FILTERING: missing or incomplete CPAN Testers data, Locale-CLDR-Locales-Zgh'
                                       },
          'Locale-CLDR-Locales-Zu' => {
                                        'filtered' => 'FILTERING: missing or incomplete CPAN Testers data, Locale-CLDR-Locales-Zu'
                                      },
          'PerlPowerTools' => {
                                'filtered' => 'FILTERING: bad individual CPANTS Kwalitee score \'use_strict\', PerlPowerTools'
                              },
          'SPVM' => {
                      'filtered' => 'FILTERING: bad individual CPANTS Kwalitee score \'use_strict\', SPVM'
                    },
          'Sisimai' => {
                         'filtered' => 'FILTERING: missing or incomplete CPAN Testers data, Sisimai'
                       },
          'Sys-Virt' => {
                          'filtered' => 'FILTERING: missing or incomplete CPAN Testers data, Sys-Virt'
                        },
          'Test-HTTP-LocalServer' => {
                                       'filtered' => 'FILTERING: bad individual CPANTS Kwalitee score \'use_warnings\', Test-HTTP-LocalServer'
                                     },
          'Travel-Status-DE-DBWagenreihung' => {
                                                 'filtered' => 'FILTERING: too few CPAN Testers, Travel-Status-DE-DBWagenreihung'
                                               },
          'YAMLScript' => {
                            'filtered' => 'FILTERING: too many failing CPAN Testers, YAMLScript'
                          }
        };
1;
